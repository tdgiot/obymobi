﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Media'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MediaEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MediaEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection;
		private bool	_alwaysFetchAnnouncementCollection, _alreadyFetchedAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.MediaCultureCollection	_mediaCultureCollection;
		private bool	_alwaysFetchMediaCultureCollection, _alreadyFetchedMediaCultureCollection;
		private Obymobi.Data.CollectionClasses.MediaLanguageCollection	_mediaLanguageCollection;
		private bool	_alwaysFetchMediaLanguageCollection, _alreadyFetchedMediaLanguageCollection;
		private Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection	_mediaRatioTypeMediaCollection;
		private bool	_alwaysFetchMediaRatioTypeMediaCollection, _alreadyFetchedMediaRatioTypeMediaCollection;
		private Obymobi.Data.CollectionClasses.MediaRelationshipCollection	_mediaRelationshipCollection;
		private bool	_alwaysFetchMediaRelationshipCollection, _alreadyFetchedMediaRelationshipCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageTemplateCollection	_messageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCollection, _alreadyFetchedMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemCollection	_uIScheduleItemCollection;
		private bool	_alwaysFetchUIScheduleItemCollection, _alreadyFetchedUIScheduleItemCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAnnouncement;
		private bool	_alwaysFetchCategoryCollectionViaAnnouncement, _alreadyFetchedCategoryCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAnnouncement_;
		private bool	_alwaysFetchCategoryCollectionViaAnnouncement_, _alreadyFetchedCategoryCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessage;
		private bool	_alwaysFetchCategoryCollectionViaMessage, _alreadyFetchedCategoryCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessageTemplate;
		private bool	_alwaysFetchCategoryCollectionViaMessageTemplate, _alreadyFetchedCategoryCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaMessage;
		private bool	_alwaysFetchClientCollectionViaMessage, _alreadyFetchedClientCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAnnouncement;
		private bool	_alwaysFetchCompanyCollectionViaAnnouncement, _alreadyFetchedCompanyCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMessage;
		private bool	_alwaysFetchCompanyCollectionViaMessage, _alreadyFetchedCompanyCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMessageTemplate;
		private bool	_alwaysFetchCompanyCollectionViaMessageTemplate, _alreadyFetchedCompanyCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaMessage;
		private bool	_alwaysFetchCustomerCollectionViaMessage, _alreadyFetchedCustomerCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessage;
		private bool	_alwaysFetchDeliverypointCollectionViaMessage, _alreadyFetchedDeliverypointCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAnnouncement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAnnouncement, _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement, _alreadyFetchedEntertainmentCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessageTemplate;
		private bool	_alwaysFetchEntertainmentCollectionViaMessageTemplate, _alreadyFetchedEntertainmentCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement, _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAnnouncement_;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_, _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaMessage;
		private bool	_alwaysFetchOrderCollectionViaMessage, _alreadyFetchedOrderCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement;
		private bool	_alwaysFetchProductCollectionViaAnnouncement, _alreadyFetchedProductCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMessageTemplate;
		private bool	_alwaysFetchProductCollectionViaMessageTemplate, _alreadyFetchedProductCollectionViaMessageTemplate;
		private AdvertisementEntity _advertisementEntity;
		private bool	_alwaysFetchAdvertisementEntity, _alreadyFetchedAdvertisementEntity, _advertisementEntityReturnsNewIfNotFound;
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationoptionEntity _alterationoptionEntity;
		private bool	_alwaysFetchAlterationoptionEntity, _alreadyFetchedAlterationoptionEntity, _alterationoptionEntityReturnsNewIfNotFound;
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;
		private CarouselItemEntity _carouselItemEntity;
		private bool	_alwaysFetchCarouselItemEntity, _alreadyFetchedCarouselItemEntity, _carouselItemEntityReturnsNewIfNotFound;
		private LandingPageEntity _landingPageEntity;
		private bool	_alwaysFetchLandingPageEntity, _alreadyFetchedLandingPageEntity, _landingPageEntityReturnsNewIfNotFound;
		private WidgetActionBannerEntity _widgetActionBannerEntity;
		private bool	_alwaysFetchWidgetActionBannerEntity, _alreadyFetchedWidgetActionBannerEntity, _widgetActionBannerEntityReturnsNewIfNotFound;
		private WidgetHeroEntity _widgetHeroEntity;
		private bool	_alwaysFetchWidgetHeroEntity, _alreadyFetchedWidgetHeroEntity, _widgetHeroEntityReturnsNewIfNotFound;
		private AttachmentEntity _attachmentEntity;
		private bool	_alwaysFetchAttachmentEntity, _alreadyFetchedAttachmentEntity, _attachmentEntityReturnsNewIfNotFound;
		private CategoryEntity _actionCategoryEntity;
		private bool	_alwaysFetchActionCategoryEntity, _alreadyFetchedActionCategoryEntity, _actionCategoryEntityReturnsNewIfNotFound;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private EntertainmentEntity _actionEntertainmentEntity;
		private bool	_alwaysFetchActionEntertainmentEntity, _alreadyFetchedActionEntertainmentEntity, _actionEntertainmentEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private EntertainmentcategoryEntity _actionEntertainmentcategoryEntity;
		private bool	_alwaysFetchActionEntertainmentcategoryEntity, _alreadyFetchedActionEntertainmentcategoryEntity, _actionEntertainmentcategoryEntityReturnsNewIfNotFound;
		private GenericcategoryEntity _genericcategoryEntity;
		private bool	_alwaysFetchGenericcategoryEntity, _alreadyFetchedGenericcategoryEntity, _genericcategoryEntityReturnsNewIfNotFound;
		private GenericproductEntity _genericproductEntity;
		private bool	_alwaysFetchGenericproductEntity, _alreadyFetchedGenericproductEntity, _genericproductEntityReturnsNewIfNotFound;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity_;
		private bool	_alwaysFetchPageEntity_, _alreadyFetchedPageEntity_, _pageEntity_ReturnsNewIfNotFound;
		private PageElementEntity _pageElementEntity;
		private bool	_alwaysFetchPageElementEntity, _alreadyFetchedPageElementEntity, _pageElementEntityReturnsNewIfNotFound;
		private PageTemplateEntity _pageTemplateEntity;
		private bool	_alwaysFetchPageTemplateEntity, _alreadyFetchedPageTemplateEntity, _pageTemplateEntityReturnsNewIfNotFound;
		private PageTemplateElementEntity _pageTemplateElementEntity;
		private bool	_alwaysFetchPageTemplateElementEntity, _alreadyFetchedPageTemplateElementEntity, _pageTemplateElementEntityReturnsNewIfNotFound;
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;
		private ProductEntity _actionProductEntity;
		private bool	_alwaysFetchActionProductEntity, _alreadyFetchedActionProductEntity, _actionProductEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductgroupEntity _productgroupEntity;
		private bool	_alwaysFetchProductgroupEntity, _alreadyFetchedProductgroupEntity, _productgroupEntityReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;
		private RoomControlSectionItemEntity _roomControlSectionItemEntity;
		private bool	_alwaysFetchRoomControlSectionItemEntity, _alreadyFetchedRoomControlSectionItemEntity, _roomControlSectionItemEntityReturnsNewIfNotFound;
		private RoutestephandlerEntity _routestephandlerEntity;
		private bool	_alwaysFetchRoutestephandlerEntity, _alreadyFetchedRoutestephandlerEntity, _routestephandlerEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity_;
		private bool	_alwaysFetchSiteEntity_, _alreadyFetchedSiteEntity_, _siteEntity_ReturnsNewIfNotFound;
		private StationEntity _stationEntity;
		private bool	_alwaysFetchStationEntity, _alreadyFetchedStationEntity, _stationEntityReturnsNewIfNotFound;
		private SurveyEntity _surveyEntity;
		private bool	_alwaysFetchSurveyEntity, _alreadyFetchedSurveyEntity, _surveyEntityReturnsNewIfNotFound;
		private SurveyPageEntity _surveyPageEntity;
		private bool	_alwaysFetchSurveyPageEntity, _alreadyFetchedSurveyPageEntity, _surveyPageEntityReturnsNewIfNotFound;
		private UIFooterItemEntity _uIFooterItemEntity;
		private bool	_alwaysFetchUIFooterItemEntity, _alreadyFetchedUIFooterItemEntity, _uIFooterItemEntityReturnsNewIfNotFound;
		private UIThemeEntity _uIThemeEntity;
		private bool	_alwaysFetchUIThemeEntity, _alreadyFetchedUIThemeEntity, _uIThemeEntityReturnsNewIfNotFound;
		private UIWidgetEntity _uIWidgetEntity;
		private bool	_alwaysFetchUIWidgetEntity, _alreadyFetchedUIWidgetEntity, _uIWidgetEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementEntity</summary>
			public static readonly string AdvertisementEntity = "AdvertisementEntity";
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationoptionEntity</summary>
			public static readonly string AlterationoptionEntity = "AlterationoptionEntity";
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name CarouselItemEntity</summary>
			public static readonly string CarouselItemEntity = "CarouselItemEntity";
			/// <summary>Member name LandingPageEntity</summary>
			public static readonly string LandingPageEntity = "LandingPageEntity";
			/// <summary>Member name WidgetActionBannerEntity</summary>
			public static readonly string WidgetActionBannerEntity = "WidgetActionBannerEntity";
			/// <summary>Member name WidgetHeroEntity</summary>
			public static readonly string WidgetHeroEntity = "WidgetHeroEntity";
			/// <summary>Member name AttachmentEntity</summary>
			public static readonly string AttachmentEntity = "AttachmentEntity";
			/// <summary>Member name ActionCategoryEntity</summary>
			public static readonly string ActionCategoryEntity = "ActionCategoryEntity";
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name ActionEntertainmentEntity</summary>
			public static readonly string ActionEntertainmentEntity = "ActionEntertainmentEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name ActionEntertainmentcategoryEntity</summary>
			public static readonly string ActionEntertainmentcategoryEntity = "ActionEntertainmentcategoryEntity";
			/// <summary>Member name GenericcategoryEntity</summary>
			public static readonly string GenericcategoryEntity = "GenericcategoryEntity";
			/// <summary>Member name GenericproductEntity</summary>
			public static readonly string GenericproductEntity = "GenericproductEntity";
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name PageEntity_</summary>
			public static readonly string PageEntity_ = "PageEntity_";
			/// <summary>Member name PageElementEntity</summary>
			public static readonly string PageElementEntity = "PageElementEntity";
			/// <summary>Member name PageTemplateEntity</summary>
			public static readonly string PageTemplateEntity = "PageTemplateEntity";
			/// <summary>Member name PageTemplateElementEntity</summary>
			public static readonly string PageTemplateElementEntity = "PageTemplateElementEntity";
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
			/// <summary>Member name ActionProductEntity</summary>
			public static readonly string ActionProductEntity = "ActionProductEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductgroupEntity</summary>
			public static readonly string ProductgroupEntity = "ProductgroupEntity";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name RoomControlSectionItemEntity</summary>
			public static readonly string RoomControlSectionItemEntity = "RoomControlSectionItemEntity";
			/// <summary>Member name RoutestephandlerEntity</summary>
			public static readonly string RoutestephandlerEntity = "RoutestephandlerEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name SiteEntity_</summary>
			public static readonly string SiteEntity_ = "SiteEntity_";
			/// <summary>Member name StationEntity</summary>
			public static readonly string StationEntity = "StationEntity";
			/// <summary>Member name SurveyEntity</summary>
			public static readonly string SurveyEntity = "SurveyEntity";
			/// <summary>Member name SurveyPageEntity</summary>
			public static readonly string SurveyPageEntity = "SurveyPageEntity";
			/// <summary>Member name UIFooterItemEntity</summary>
			public static readonly string UIFooterItemEntity = "UIFooterItemEntity";
			/// <summary>Member name UIThemeEntity</summary>
			public static readonly string UIThemeEntity = "UIThemeEntity";
			/// <summary>Member name UIWidgetEntity</summary>
			public static readonly string UIWidgetEntity = "UIWidgetEntity";
			/// <summary>Member name AnnouncementCollection</summary>
			public static readonly string AnnouncementCollection = "AnnouncementCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaCultureCollection</summary>
			public static readonly string MediaCultureCollection = "MediaCultureCollection";
			/// <summary>Member name MediaLanguageCollection</summary>
			public static readonly string MediaLanguageCollection = "MediaLanguageCollection";
			/// <summary>Member name MediaRatioTypeMediaCollection</summary>
			public static readonly string MediaRatioTypeMediaCollection = "MediaRatioTypeMediaCollection";
			/// <summary>Member name MediaRelationshipCollection</summary>
			public static readonly string MediaRelationshipCollection = "MediaRelationshipCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageTemplateCollection</summary>
			public static readonly string MessageTemplateCollection = "MessageTemplateCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
			/// <summary>Member name UIScheduleItemCollection</summary>
			public static readonly string UIScheduleItemCollection = "UIScheduleItemCollection";
			/// <summary>Member name CategoryCollectionViaAnnouncement</summary>
			public static readonly string CategoryCollectionViaAnnouncement = "CategoryCollectionViaAnnouncement";
			/// <summary>Member name CategoryCollectionViaAnnouncement_</summary>
			public static readonly string CategoryCollectionViaAnnouncement_ = "CategoryCollectionViaAnnouncement_";
			/// <summary>Member name CategoryCollectionViaMessage</summary>
			public static readonly string CategoryCollectionViaMessage = "CategoryCollectionViaMessage";
			/// <summary>Member name CategoryCollectionViaMessageTemplate</summary>
			public static readonly string CategoryCollectionViaMessageTemplate = "CategoryCollectionViaMessageTemplate";
			/// <summary>Member name ClientCollectionViaMessage</summary>
			public static readonly string ClientCollectionViaMessage = "ClientCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaAnnouncement</summary>
			public static readonly string CompanyCollectionViaAnnouncement = "CompanyCollectionViaAnnouncement";
			/// <summary>Member name CompanyCollectionViaMessage</summary>
			public static readonly string CompanyCollectionViaMessage = "CompanyCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaMessageTemplate</summary>
			public static readonly string CompanyCollectionViaMessageTemplate = "CompanyCollectionViaMessageTemplate";
			/// <summary>Member name CustomerCollectionViaMessage</summary>
			public static readonly string CustomerCollectionViaMessage = "CustomerCollectionViaMessage";
			/// <summary>Member name DeliverypointCollectionViaMessage</summary>
			public static readonly string DeliverypointCollectionViaMessage = "DeliverypointCollectionViaMessage";
			/// <summary>Member name DeliverypointgroupCollectionViaAnnouncement</summary>
			public static readonly string DeliverypointgroupCollectionViaAnnouncement = "DeliverypointgroupCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement = "EntertainmentCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name EntertainmentCollectionViaMessageTemplate</summary>
			public static readonly string EntertainmentCollectionViaMessageTemplate = "EntertainmentCollectionViaMessageTemplate";
			/// <summary>Member name EntertainmentcategoryCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAnnouncement = "EntertainmentcategoryCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentcategoryCollectionViaAnnouncement_</summary>
			public static readonly string EntertainmentcategoryCollectionViaAnnouncement_ = "EntertainmentcategoryCollectionViaAnnouncement_";
			/// <summary>Member name OrderCollectionViaMessage</summary>
			public static readonly string OrderCollectionViaMessage = "OrderCollectionViaMessage";
			/// <summary>Member name ProductCollectionViaAnnouncement</summary>
			public static readonly string ProductCollectionViaAnnouncement = "ProductCollectionViaAnnouncement";
			/// <summary>Member name ProductCollectionViaMessageTemplate</summary>
			public static readonly string ProductCollectionViaMessageTemplate = "ProductCollectionViaMessageTemplate";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MediaEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MediaEntityBase() :base("MediaEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		protected MediaEntityBase(System.Int32 mediaId):base("MediaEntity")
		{
			InitClassFetch(mediaId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MediaEntityBase(System.Int32 mediaId, IPrefetchPath prefetchPathToUse): base("MediaEntity")
		{
			InitClassFetch(mediaId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="validator">The custom validator object for this MediaEntity</param>
		protected MediaEntityBase(System.Int32 mediaId, IValidator validator):base("MediaEntity")
		{
			InitClassFetch(mediaId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_announcementCollection = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection = info.GetBoolean("_alwaysFetchAnnouncementCollection");
			_alreadyFetchedAnnouncementCollection = info.GetBoolean("_alreadyFetchedAnnouncementCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_mediaCultureCollection = (Obymobi.Data.CollectionClasses.MediaCultureCollection)info.GetValue("_mediaCultureCollection", typeof(Obymobi.Data.CollectionClasses.MediaCultureCollection));
			_alwaysFetchMediaCultureCollection = info.GetBoolean("_alwaysFetchMediaCultureCollection");
			_alreadyFetchedMediaCultureCollection = info.GetBoolean("_alreadyFetchedMediaCultureCollection");

			_mediaLanguageCollection = (Obymobi.Data.CollectionClasses.MediaLanguageCollection)info.GetValue("_mediaLanguageCollection", typeof(Obymobi.Data.CollectionClasses.MediaLanguageCollection));
			_alwaysFetchMediaLanguageCollection = info.GetBoolean("_alwaysFetchMediaLanguageCollection");
			_alreadyFetchedMediaLanguageCollection = info.GetBoolean("_alreadyFetchedMediaLanguageCollection");

			_mediaRatioTypeMediaCollection = (Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection)info.GetValue("_mediaRatioTypeMediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection));
			_alwaysFetchMediaRatioTypeMediaCollection = info.GetBoolean("_alwaysFetchMediaRatioTypeMediaCollection");
			_alreadyFetchedMediaRatioTypeMediaCollection = info.GetBoolean("_alreadyFetchedMediaRatioTypeMediaCollection");

			_mediaRelationshipCollection = (Obymobi.Data.CollectionClasses.MediaRelationshipCollection)info.GetValue("_mediaRelationshipCollection", typeof(Obymobi.Data.CollectionClasses.MediaRelationshipCollection));
			_alwaysFetchMediaRelationshipCollection = info.GetBoolean("_alwaysFetchMediaRelationshipCollection");
			_alreadyFetchedMediaRelationshipCollection = info.GetBoolean("_alreadyFetchedMediaRelationshipCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCollection)info.GetValue("_messageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCollection));
			_alwaysFetchMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");

			_uIScheduleItemCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemCollection)info.GetValue("_uIScheduleItemCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemCollection));
			_alwaysFetchUIScheduleItemCollection = info.GetBoolean("_alwaysFetchUIScheduleItemCollection");
			_alreadyFetchedUIScheduleItemCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemCollection");
			_categoryCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAnnouncement");
			_alreadyFetchedCategoryCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAnnouncement");

			_categoryCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaAnnouncement_");
			_alreadyFetchedCategoryCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAnnouncement_");

			_categoryCollectionViaMessage = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessage = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessage");
			_alreadyFetchedCategoryCollectionViaMessage = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessage");

			_categoryCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessageTemplate");
			_alreadyFetchedCategoryCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessageTemplate");

			_clientCollectionViaMessage = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaMessage = info.GetBoolean("_alwaysFetchClientCollectionViaMessage");
			_alreadyFetchedClientCollectionViaMessage = info.GetBoolean("_alreadyFetchedClientCollectionViaMessage");

			_companyCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchCompanyCollectionViaAnnouncement");
			_alreadyFetchedCompanyCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAnnouncement");

			_companyCollectionViaMessage = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMessage = info.GetBoolean("_alwaysFetchCompanyCollectionViaMessage");
			_alreadyFetchedCompanyCollectionViaMessage = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMessage");

			_companyCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchCompanyCollectionViaMessageTemplate");
			_alreadyFetchedCompanyCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMessageTemplate");

			_customerCollectionViaMessage = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaMessage = info.GetBoolean("_alwaysFetchCustomerCollectionViaMessage");
			_alreadyFetchedCustomerCollectionViaMessage = info.GetBoolean("_alreadyFetchedCustomerCollectionViaMessage");

			_deliverypointCollectionViaMessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessage");
			_alreadyFetchedDeliverypointCollectionViaMessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessage");

			_deliverypointgroupCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement");
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement");

			_entertainmentCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_entertainmentCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessageTemplate");
			_alreadyFetchedEntertainmentCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessageTemplate");

			_entertainmentcategoryCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement");

			_entertainmentcategoryCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_");
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_");

			_orderCollectionViaMessage = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaMessage = info.GetBoolean("_alwaysFetchOrderCollectionViaMessage");
			_alreadyFetchedOrderCollectionViaMessage = info.GetBoolean("_alreadyFetchedOrderCollectionViaMessage");

			_productCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement");
			_alreadyFetchedProductCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement");

			_productCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchProductCollectionViaMessageTemplate");
			_alreadyFetchedProductCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedProductCollectionViaMessageTemplate");
			_advertisementEntity = (AdvertisementEntity)info.GetValue("_advertisementEntity", typeof(AdvertisementEntity));
			if(_advertisementEntity!=null)
			{
				_advertisementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementEntity = info.GetBoolean("_alwaysFetchAdvertisementEntity");
			_alreadyFetchedAdvertisementEntity = info.GetBoolean("_alreadyFetchedAdvertisementEntity");

			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationoptionEntity = (AlterationoptionEntity)info.GetValue("_alterationoptionEntity", typeof(AlterationoptionEntity));
			if(_alterationoptionEntity!=null)
			{
				_alterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_alterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationoptionEntity = info.GetBoolean("_alwaysFetchAlterationoptionEntity");
			_alreadyFetchedAlterationoptionEntity = info.GetBoolean("_alreadyFetchedAlterationoptionEntity");

			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");

			_carouselItemEntity = (CarouselItemEntity)info.GetValue("_carouselItemEntity", typeof(CarouselItemEntity));
			if(_carouselItemEntity!=null)
			{
				_carouselItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_carouselItemEntityReturnsNewIfNotFound = info.GetBoolean("_carouselItemEntityReturnsNewIfNotFound");
			_alwaysFetchCarouselItemEntity = info.GetBoolean("_alwaysFetchCarouselItemEntity");
			_alreadyFetchedCarouselItemEntity = info.GetBoolean("_alreadyFetchedCarouselItemEntity");

			_landingPageEntity = (LandingPageEntity)info.GetValue("_landingPageEntity", typeof(LandingPageEntity));
			if(_landingPageEntity!=null)
			{
				_landingPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_landingPageEntityReturnsNewIfNotFound = info.GetBoolean("_landingPageEntityReturnsNewIfNotFound");
			_alwaysFetchLandingPageEntity = info.GetBoolean("_alwaysFetchLandingPageEntity");
			_alreadyFetchedLandingPageEntity = info.GetBoolean("_alreadyFetchedLandingPageEntity");

			_widgetActionBannerEntity = (WidgetActionBannerEntity)info.GetValue("_widgetActionBannerEntity", typeof(WidgetActionBannerEntity));
			if(_widgetActionBannerEntity!=null)
			{
				_widgetActionBannerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetActionBannerEntityReturnsNewIfNotFound = info.GetBoolean("_widgetActionBannerEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetActionBannerEntity = info.GetBoolean("_alwaysFetchWidgetActionBannerEntity");
			_alreadyFetchedWidgetActionBannerEntity = info.GetBoolean("_alreadyFetchedWidgetActionBannerEntity");

			_widgetHeroEntity = (WidgetHeroEntity)info.GetValue("_widgetHeroEntity", typeof(WidgetHeroEntity));
			if(_widgetHeroEntity!=null)
			{
				_widgetHeroEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetHeroEntityReturnsNewIfNotFound = info.GetBoolean("_widgetHeroEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetHeroEntity = info.GetBoolean("_alwaysFetchWidgetHeroEntity");
			_alreadyFetchedWidgetHeroEntity = info.GetBoolean("_alreadyFetchedWidgetHeroEntity");

			_attachmentEntity = (AttachmentEntity)info.GetValue("_attachmentEntity", typeof(AttachmentEntity));
			if(_attachmentEntity!=null)
			{
				_attachmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attachmentEntityReturnsNewIfNotFound = info.GetBoolean("_attachmentEntityReturnsNewIfNotFound");
			_alwaysFetchAttachmentEntity = info.GetBoolean("_alwaysFetchAttachmentEntity");
			_alreadyFetchedAttachmentEntity = info.GetBoolean("_alreadyFetchedAttachmentEntity");

			_actionCategoryEntity = (CategoryEntity)info.GetValue("_actionCategoryEntity", typeof(CategoryEntity));
			if(_actionCategoryEntity!=null)
			{
				_actionCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_actionCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchActionCategoryEntity = info.GetBoolean("_alwaysFetchActionCategoryEntity");
			_alreadyFetchedActionCategoryEntity = info.GetBoolean("_alreadyFetchedActionCategoryEntity");

			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_actionEntertainmentEntity = (EntertainmentEntity)info.GetValue("_actionEntertainmentEntity", typeof(EntertainmentEntity));
			if(_actionEntertainmentEntity!=null)
			{
				_actionEntertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionEntertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_actionEntertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchActionEntertainmentEntity = info.GetBoolean("_alwaysFetchActionEntertainmentEntity");
			_alreadyFetchedActionEntertainmentEntity = info.GetBoolean("_alreadyFetchedActionEntertainmentEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_actionEntertainmentcategoryEntity = (EntertainmentcategoryEntity)info.GetValue("_actionEntertainmentcategoryEntity", typeof(EntertainmentcategoryEntity));
			if(_actionEntertainmentcategoryEntity!=null)
			{
				_actionEntertainmentcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionEntertainmentcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_actionEntertainmentcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchActionEntertainmentcategoryEntity = info.GetBoolean("_alwaysFetchActionEntertainmentcategoryEntity");
			_alreadyFetchedActionEntertainmentcategoryEntity = info.GetBoolean("_alreadyFetchedActionEntertainmentcategoryEntity");

			_genericcategoryEntity = (GenericcategoryEntity)info.GetValue("_genericcategoryEntity", typeof(GenericcategoryEntity));
			if(_genericcategoryEntity!=null)
			{
				_genericcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_genericcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchGenericcategoryEntity = info.GetBoolean("_alwaysFetchGenericcategoryEntity");
			_alreadyFetchedGenericcategoryEntity = info.GetBoolean("_alreadyFetchedGenericcategoryEntity");

			_genericproductEntity = (GenericproductEntity)info.GetValue("_genericproductEntity", typeof(GenericproductEntity));
			if(_genericproductEntity!=null)
			{
				_genericproductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericproductEntityReturnsNewIfNotFound = info.GetBoolean("_genericproductEntityReturnsNewIfNotFound");
			_alwaysFetchGenericproductEntity = info.GetBoolean("_alwaysFetchGenericproductEntity");
			_alreadyFetchedGenericproductEntity = info.GetBoolean("_alreadyFetchedGenericproductEntity");

			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_pageEntity_ = (PageEntity)info.GetValue("_pageEntity_", typeof(PageEntity));
			if(_pageEntity_!=null)
			{
				_pageEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntity_ReturnsNewIfNotFound = info.GetBoolean("_pageEntity_ReturnsNewIfNotFound");
			_alwaysFetchPageEntity_ = info.GetBoolean("_alwaysFetchPageEntity_");
			_alreadyFetchedPageEntity_ = info.GetBoolean("_alreadyFetchedPageEntity_");

			_pageElementEntity = (PageElementEntity)info.GetValue("_pageElementEntity", typeof(PageElementEntity));
			if(_pageElementEntity!=null)
			{
				_pageElementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageElementEntityReturnsNewIfNotFound = info.GetBoolean("_pageElementEntityReturnsNewIfNotFound");
			_alwaysFetchPageElementEntity = info.GetBoolean("_alwaysFetchPageElementEntity");
			_alreadyFetchedPageElementEntity = info.GetBoolean("_alreadyFetchedPageElementEntity");

			_pageTemplateEntity = (PageTemplateEntity)info.GetValue("_pageTemplateEntity", typeof(PageTemplateEntity));
			if(_pageTemplateEntity!=null)
			{
				_pageTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_pageTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchPageTemplateEntity = info.GetBoolean("_alwaysFetchPageTemplateEntity");
			_alreadyFetchedPageTemplateEntity = info.GetBoolean("_alreadyFetchedPageTemplateEntity");

			_pageTemplateElementEntity = (PageTemplateElementEntity)info.GetValue("_pageTemplateElementEntity", typeof(PageTemplateElementEntity));
			if(_pageTemplateElementEntity!=null)
			{
				_pageTemplateElementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageTemplateElementEntityReturnsNewIfNotFound = info.GetBoolean("_pageTemplateElementEntityReturnsNewIfNotFound");
			_alwaysFetchPageTemplateElementEntity = info.GetBoolean("_alwaysFetchPageTemplateElementEntity");
			_alreadyFetchedPageTemplateElementEntity = info.GetBoolean("_alreadyFetchedPageTemplateElementEntity");

			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");

			_actionProductEntity = (ProductEntity)info.GetValue("_actionProductEntity", typeof(ProductEntity));
			if(_actionProductEntity!=null)
			{
				_actionProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionProductEntityReturnsNewIfNotFound = info.GetBoolean("_actionProductEntityReturnsNewIfNotFound");
			_alwaysFetchActionProductEntity = info.GetBoolean("_alwaysFetchActionProductEntity");
			_alreadyFetchedActionProductEntity = info.GetBoolean("_alreadyFetchedActionProductEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productgroupEntity = (ProductgroupEntity)info.GetValue("_productgroupEntity", typeof(ProductgroupEntity));
			if(_productgroupEntity!=null)
			{
				_productgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productgroupEntityReturnsNewIfNotFound = info.GetBoolean("_productgroupEntityReturnsNewIfNotFound");
			_alwaysFetchProductgroupEntity = info.GetBoolean("_alwaysFetchProductgroupEntity");
			_alreadyFetchedProductgroupEntity = info.GetBoolean("_alreadyFetchedProductgroupEntity");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");

			_roomControlSectionItemEntity = (RoomControlSectionItemEntity)info.GetValue("_roomControlSectionItemEntity", typeof(RoomControlSectionItemEntity));
			if(_roomControlSectionItemEntity!=null)
			{
				_roomControlSectionItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionItemEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionItemEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionItemEntity = info.GetBoolean("_alwaysFetchRoomControlSectionItemEntity");
			_alreadyFetchedRoomControlSectionItemEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionItemEntity");

			_routestephandlerEntity = (RoutestephandlerEntity)info.GetValue("_routestephandlerEntity", typeof(RoutestephandlerEntity));
			if(_routestephandlerEntity!=null)
			{
				_routestephandlerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routestephandlerEntityReturnsNewIfNotFound = info.GetBoolean("_routestephandlerEntityReturnsNewIfNotFound");
			_alwaysFetchRoutestephandlerEntity = info.GetBoolean("_alwaysFetchRoutestephandlerEntity");
			_alreadyFetchedRoutestephandlerEntity = info.GetBoolean("_alreadyFetchedRoutestephandlerEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");

			_siteEntity_ = (SiteEntity)info.GetValue("_siteEntity_", typeof(SiteEntity));
			if(_siteEntity_!=null)
			{
				_siteEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntity_ReturnsNewIfNotFound = info.GetBoolean("_siteEntity_ReturnsNewIfNotFound");
			_alwaysFetchSiteEntity_ = info.GetBoolean("_alwaysFetchSiteEntity_");
			_alreadyFetchedSiteEntity_ = info.GetBoolean("_alreadyFetchedSiteEntity_");

			_stationEntity = (StationEntity)info.GetValue("_stationEntity", typeof(StationEntity));
			if(_stationEntity!=null)
			{
				_stationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_stationEntityReturnsNewIfNotFound = info.GetBoolean("_stationEntityReturnsNewIfNotFound");
			_alwaysFetchStationEntity = info.GetBoolean("_alwaysFetchStationEntity");
			_alreadyFetchedStationEntity = info.GetBoolean("_alreadyFetchedStationEntity");

			_surveyEntity = (SurveyEntity)info.GetValue("_surveyEntity", typeof(SurveyEntity));
			if(_surveyEntity!=null)
			{
				_surveyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyEntityReturnsNewIfNotFound = info.GetBoolean("_surveyEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyEntity = info.GetBoolean("_alwaysFetchSurveyEntity");
			_alreadyFetchedSurveyEntity = info.GetBoolean("_alreadyFetchedSurveyEntity");

			_surveyPageEntity = (SurveyPageEntity)info.GetValue("_surveyPageEntity", typeof(SurveyPageEntity));
			if(_surveyPageEntity!=null)
			{
				_surveyPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyPageEntityReturnsNewIfNotFound = info.GetBoolean("_surveyPageEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyPageEntity = info.GetBoolean("_alwaysFetchSurveyPageEntity");
			_alreadyFetchedSurveyPageEntity = info.GetBoolean("_alreadyFetchedSurveyPageEntity");

			_uIFooterItemEntity = (UIFooterItemEntity)info.GetValue("_uIFooterItemEntity", typeof(UIFooterItemEntity));
			if(_uIFooterItemEntity!=null)
			{
				_uIFooterItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIFooterItemEntityReturnsNewIfNotFound = info.GetBoolean("_uIFooterItemEntityReturnsNewIfNotFound");
			_alwaysFetchUIFooterItemEntity = info.GetBoolean("_alwaysFetchUIFooterItemEntity");
			_alreadyFetchedUIFooterItemEntity = info.GetBoolean("_alreadyFetchedUIFooterItemEntity");

			_uIThemeEntity = (UIThemeEntity)info.GetValue("_uIThemeEntity", typeof(UIThemeEntity));
			if(_uIThemeEntity!=null)
			{
				_uIThemeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIThemeEntityReturnsNewIfNotFound = info.GetBoolean("_uIThemeEntityReturnsNewIfNotFound");
			_alwaysFetchUIThemeEntity = info.GetBoolean("_alwaysFetchUIThemeEntity");
			_alreadyFetchedUIThemeEntity = info.GetBoolean("_alreadyFetchedUIThemeEntity");

			_uIWidgetEntity = (UIWidgetEntity)info.GetValue("_uIWidgetEntity", typeof(UIWidgetEntity));
			if(_uIWidgetEntity!=null)
			{
				_uIWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_uIWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchUIWidgetEntity = info.GetBoolean("_alwaysFetchUIWidgetEntity");
			_alreadyFetchedUIWidgetEntity = info.GetBoolean("_alreadyFetchedUIWidgetEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MediaFieldIndex)fieldIndex)
			{
				case MediaFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case MediaFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case MediaFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case MediaFieldIndex.AdvertisementId:
					DesetupSyncAdvertisementEntity(true, false);
					_alreadyFetchedAdvertisementEntity = false;
					break;
				case MediaFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case MediaFieldIndex.AlterationoptionId:
					DesetupSyncAlterationoptionEntity(true, false);
					_alreadyFetchedAlterationoptionEntity = false;
					break;
				case MediaFieldIndex.GenericproductId:
					DesetupSyncGenericproductEntity(true, false);
					_alreadyFetchedGenericproductEntity = false;
					break;
				case MediaFieldIndex.GenericcategoryId:
					DesetupSyncGenericcategoryEntity(true, false);
					_alreadyFetchedGenericcategoryEntity = false;
					break;
				case MediaFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case MediaFieldIndex.SurveyId:
					DesetupSyncSurveyEntity(true, false);
					_alreadyFetchedSurveyEntity = false;
					break;
				case MediaFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case MediaFieldIndex.SurveyPageId:
					DesetupSyncSurveyPageEntity(true, false);
					_alreadyFetchedSurveyPageEntity = false;
					break;
				case MediaFieldIndex.RoutestephandlerId:
					DesetupSyncRoutestephandlerEntity(true, false);
					_alreadyFetchedRoutestephandlerEntity = false;
					break;
				case MediaFieldIndex.PageElementId:
					DesetupSyncPageElementEntity(true, false);
					_alreadyFetchedPageElementEntity = false;
					break;
				case MediaFieldIndex.PageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case MediaFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				case MediaFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case MediaFieldIndex.ActionEntertainmentId:
					DesetupSyncActionEntertainmentEntity(true, false);
					_alreadyFetchedActionEntertainmentEntity = false;
					break;
				case MediaFieldIndex.ActionProductId:
					DesetupSyncActionProductEntity(true, false);
					_alreadyFetchedActionProductEntity = false;
					break;
				case MediaFieldIndex.ActionCategoryId:
					DesetupSyncActionCategoryEntity(true, false);
					_alreadyFetchedActionCategoryEntity = false;
					break;
				case MediaFieldIndex.ActionEntertainmentcategoryId:
					DesetupSyncActionEntertainmentcategoryEntity(true, false);
					_alreadyFetchedActionEntertainmentcategoryEntity = false;
					break;
				case MediaFieldIndex.AgnosticMediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case MediaFieldIndex.AttachmentId:
					DesetupSyncAttachmentEntity(true, false);
					_alreadyFetchedAttachmentEntity = false;
					break;
				case MediaFieldIndex.ActionPageId:
					DesetupSyncPageEntity_(true, false);
					_alreadyFetchedPageEntity_ = false;
					break;
				case MediaFieldIndex.ActionSiteId:
					DesetupSyncSiteEntity_(true, false);
					_alreadyFetchedSiteEntity_ = false;
					break;
				case MediaFieldIndex.PageTemplateElementId:
					DesetupSyncPageTemplateElementEntity(true, false);
					_alreadyFetchedPageTemplateElementEntity = false;
					break;
				case MediaFieldIndex.PageTemplateId:
					DesetupSyncPageTemplateEntity(true, false);
					_alreadyFetchedPageTemplateEntity = false;
					break;
				case MediaFieldIndex.UIWidgetId:
					DesetupSyncUIWidgetEntity(true, false);
					_alreadyFetchedUIWidgetEntity = false;
					break;
				case MediaFieldIndex.UIThemeId:
					DesetupSyncUIThemeEntity(true, false);
					_alreadyFetchedUIThemeEntity = false;
					break;
				case MediaFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				case MediaFieldIndex.RoomControlSectionItemId:
					DesetupSyncRoomControlSectionItemEntity(true, false);
					_alreadyFetchedRoomControlSectionItemEntity = false;
					break;
				case MediaFieldIndex.StationId:
					DesetupSyncStationEntity(true, false);
					_alreadyFetchedStationEntity = false;
					break;
				case MediaFieldIndex.UIFooterItemId:
					DesetupSyncUIFooterItemEntity(true, false);
					_alreadyFetchedUIFooterItemEntity = false;
					break;
				case MediaFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case MediaFieldIndex.ProductgroupId:
					DesetupSyncProductgroupEntity(true, false);
					_alreadyFetchedProductgroupEntity = false;
					break;
				case MediaFieldIndex.LandingPageId:
					DesetupSyncLandingPageEntity(true, false);
					_alreadyFetchedLandingPageEntity = false;
					break;
				case MediaFieldIndex.CarouselItemId:
					DesetupSyncCarouselItemEntity(true, false);
					_alreadyFetchedCarouselItemEntity = false;
					break;
				case MediaFieldIndex.WidgetId:
					DesetupSyncWidgetActionBannerEntity(true, false);
					_alreadyFetchedWidgetActionBannerEntity = false;
					DesetupSyncWidgetHeroEntity(true, false);
					_alreadyFetchedWidgetHeroEntity = false;
					break;
				case MediaFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAnnouncementCollection = (_announcementCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedMediaCultureCollection = (_mediaCultureCollection.Count > 0);
			_alreadyFetchedMediaLanguageCollection = (_mediaLanguageCollection.Count > 0);
			_alreadyFetchedMediaRatioTypeMediaCollection = (_mediaRatioTypeMediaCollection.Count > 0);
			_alreadyFetchedMediaRelationshipCollection = (_mediaRelationshipCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageTemplateCollection = (_messageTemplateCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedUIScheduleItemCollection = (_uIScheduleItemCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaAnnouncement = (_categoryCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedCategoryCollectionViaAnnouncement_ = (_categoryCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessage = (_categoryCollectionViaMessage.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessageTemplate = (_categoryCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedClientCollectionViaMessage = (_clientCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaAnnouncement = (_companyCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedCompanyCollectionViaMessage = (_companyCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaMessageTemplate = (_companyCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedCustomerCollectionViaMessage = (_customerCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessage = (_deliverypointCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = (_deliverypointgroupCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = (_entertainmentCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessageTemplate = (_entertainmentCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = (_entertainmentcategoryCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = (_entertainmentcategoryCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedOrderCollectionViaMessage = (_orderCollectionViaMessage.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement = (_productCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedProductCollectionViaMessageTemplate = (_productCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedAdvertisementEntity = (_advertisementEntity != null);
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationoptionEntity = (_alterationoptionEntity != null);
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
			_alreadyFetchedCarouselItemEntity = (_carouselItemEntity != null);
			_alreadyFetchedLandingPageEntity = (_landingPageEntity != null);
			_alreadyFetchedWidgetActionBannerEntity = (_widgetActionBannerEntity != null);
			_alreadyFetchedWidgetHeroEntity = (_widgetHeroEntity != null);
			_alreadyFetchedAttachmentEntity = (_attachmentEntity != null);
			_alreadyFetchedActionCategoryEntity = (_actionCategoryEntity != null);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedActionEntertainmentEntity = (_actionEntertainmentEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedActionEntertainmentcategoryEntity = (_actionEntertainmentcategoryEntity != null);
			_alreadyFetchedGenericcategoryEntity = (_genericcategoryEntity != null);
			_alreadyFetchedGenericproductEntity = (_genericproductEntity != null);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedPageEntity_ = (_pageEntity_ != null);
			_alreadyFetchedPageElementEntity = (_pageElementEntity != null);
			_alreadyFetchedPageTemplateEntity = (_pageTemplateEntity != null);
			_alreadyFetchedPageTemplateElementEntity = (_pageTemplateElementEntity != null);
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
			_alreadyFetchedActionProductEntity = (_actionProductEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductgroupEntity = (_productgroupEntity != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
			_alreadyFetchedRoomControlSectionItemEntity = (_roomControlSectionItemEntity != null);
			_alreadyFetchedRoutestephandlerEntity = (_routestephandlerEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
			_alreadyFetchedSiteEntity_ = (_siteEntity_ != null);
			_alreadyFetchedStationEntity = (_stationEntity != null);
			_alreadyFetchedSurveyEntity = (_surveyEntity != null);
			_alreadyFetchedSurveyPageEntity = (_surveyPageEntity != null);
			_alreadyFetchedUIFooterItemEntity = (_uIFooterItemEntity != null);
			_alreadyFetchedUIThemeEntity = (_uIThemeEntity != null);
			_alreadyFetchedUIWidgetEntity = (_uIWidgetEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementEntity":
					toReturn.Add(Relations.AdvertisementEntityUsingAdvertisementId);
					break;
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationoptionEntity":
					toReturn.Add(Relations.AlterationoptionEntityUsingAlterationoptionId);
					break;
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "CarouselItemEntity":
					toReturn.Add(Relations.CarouselItemEntityUsingCarouselItemId);
					break;
				case "LandingPageEntity":
					toReturn.Add(Relations.LandingPageEntityUsingLandingPageId);
					break;
				case "WidgetActionBannerEntity":
					toReturn.Add(Relations.WidgetActionBannerEntityUsingWidgetId);
					break;
				case "WidgetHeroEntity":
					toReturn.Add(Relations.WidgetHeroEntityUsingWidgetId);
					break;
				case "AttachmentEntity":
					toReturn.Add(Relations.AttachmentEntityUsingAttachmentId);
					break;
				case "ActionCategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingActionCategoryId);
					break;
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "ActionEntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingActionEntertainmentId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "ActionEntertainmentcategoryEntity":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId);
					break;
				case "GenericcategoryEntity":
					toReturn.Add(Relations.GenericcategoryEntityUsingGenericcategoryId);
					break;
				case "GenericproductEntity":
					toReturn.Add(Relations.GenericproductEntityUsingGenericproductId);
					break;
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaIdAgnosticMediaId);
					break;
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageId);
					break;
				case "PageEntity_":
					toReturn.Add(Relations.PageEntityUsingActionPageId);
					break;
				case "PageElementEntity":
					toReturn.Add(Relations.PageElementEntityUsingPageElementId);
					break;
				case "PageTemplateEntity":
					toReturn.Add(Relations.PageTemplateEntityUsingPageTemplateId);
					break;
				case "PageTemplateElementEntity":
					toReturn.Add(Relations.PageTemplateElementEntityUsingPageTemplateElementId);
					break;
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				case "ActionProductEntity":
					toReturn.Add(Relations.ProductEntityUsingActionProductId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductgroupEntity":
					toReturn.Add(Relations.ProductgroupEntityUsingProductgroupId);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "RoomControlSectionItemEntity":
					toReturn.Add(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
					break;
				case "RoutestephandlerEntity":
					toReturn.Add(Relations.RoutestephandlerEntityUsingRoutestephandlerId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "SiteEntity_":
					toReturn.Add(Relations.SiteEntityUsingActionSiteId);
					break;
				case "StationEntity":
					toReturn.Add(Relations.StationEntityUsingStationId);
					break;
				case "SurveyEntity":
					toReturn.Add(Relations.SurveyEntityUsingSurveyId);
					break;
				case "SurveyPageEntity":
					toReturn.Add(Relations.SurveyPageEntityUsingSurveyPageId);
					break;
				case "UIFooterItemEntity":
					toReturn.Add(Relations.UIFooterItemEntityUsingUIFooterItemId);
					break;
				case "UIThemeEntity":
					toReturn.Add(Relations.UIThemeEntityUsingUIThemeId);
					break;
				case "UIWidgetEntity":
					toReturn.Add(Relations.UIWidgetEntityUsingUIWidgetId);
					break;
				case "AnnouncementCollection":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingAgnosticMediaId);
					break;
				case "MediaCultureCollection":
					toReturn.Add(Relations.MediaCultureEntityUsingMediaId);
					break;
				case "MediaLanguageCollection":
					toReturn.Add(Relations.MediaLanguageEntityUsingMediaId);
					break;
				case "MediaRatioTypeMediaCollection":
					toReturn.Add(Relations.MediaRatioTypeMediaEntityUsingMediaId);
					break;
				case "MediaRelationshipCollection":
					toReturn.Add(Relations.MediaRelationshipEntityUsingMediaId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingMediaId);
					break;
				case "MessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateEntityUsingMediaId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingMediaId);
					break;
				case "UIScheduleItemCollection":
					toReturn.Add(Relations.UIScheduleItemEntityUsingMediaId);
					break;
				case "CategoryCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnNoCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnYesCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingMediaId, "MediaEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.CategoryEntityUsingCategoryId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.ClientEntityUsingClientId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CompanyEntityUsingCompanyId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CompanyEntityUsingCompanyId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingMediaId, "MediaEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.CompanyEntityUsingCompanyId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CustomerEntityUsingCustomerId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingMediaId, "MediaEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.EntertainmentEntityUsingEntertainmentId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingMediaId, "MediaEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.OrderEntityUsingOrderId, "Message_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingMediaId, "MediaEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingMediaId, "MediaEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.ProductEntityUsingProductId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_announcementCollection", (!this.MarkedForDeletion?_announcementCollection:null));
			info.AddValue("_alwaysFetchAnnouncementCollection", _alwaysFetchAnnouncementCollection);
			info.AddValue("_alreadyFetchedAnnouncementCollection", _alreadyFetchedAnnouncementCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_mediaCultureCollection", (!this.MarkedForDeletion?_mediaCultureCollection:null));
			info.AddValue("_alwaysFetchMediaCultureCollection", _alwaysFetchMediaCultureCollection);
			info.AddValue("_alreadyFetchedMediaCultureCollection", _alreadyFetchedMediaCultureCollection);
			info.AddValue("_mediaLanguageCollection", (!this.MarkedForDeletion?_mediaLanguageCollection:null));
			info.AddValue("_alwaysFetchMediaLanguageCollection", _alwaysFetchMediaLanguageCollection);
			info.AddValue("_alreadyFetchedMediaLanguageCollection", _alreadyFetchedMediaLanguageCollection);
			info.AddValue("_mediaRatioTypeMediaCollection", (!this.MarkedForDeletion?_mediaRatioTypeMediaCollection:null));
			info.AddValue("_alwaysFetchMediaRatioTypeMediaCollection", _alwaysFetchMediaRatioTypeMediaCollection);
			info.AddValue("_alreadyFetchedMediaRatioTypeMediaCollection", _alreadyFetchedMediaRatioTypeMediaCollection);
			info.AddValue("_mediaRelationshipCollection", (!this.MarkedForDeletion?_mediaRelationshipCollection:null));
			info.AddValue("_alwaysFetchMediaRelationshipCollection", _alwaysFetchMediaRelationshipCollection);
			info.AddValue("_alreadyFetchedMediaRelationshipCollection", _alreadyFetchedMediaRelationshipCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCollection", _alwaysFetchMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCollection", _alreadyFetchedMessageTemplateCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_uIScheduleItemCollection", (!this.MarkedForDeletion?_uIScheduleItemCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemCollection", _alwaysFetchUIScheduleItemCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemCollection", _alreadyFetchedUIScheduleItemCollection);
			info.AddValue("_categoryCollectionViaAnnouncement", (!this.MarkedForDeletion?_categoryCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAnnouncement", _alwaysFetchCategoryCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAnnouncement", _alreadyFetchedCategoryCollectionViaAnnouncement);
			info.AddValue("_categoryCollectionViaAnnouncement_", (!this.MarkedForDeletion?_categoryCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAnnouncement_", _alwaysFetchCategoryCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAnnouncement_", _alreadyFetchedCategoryCollectionViaAnnouncement_);
			info.AddValue("_categoryCollectionViaMessage", (!this.MarkedForDeletion?_categoryCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessage", _alwaysFetchCategoryCollectionViaMessage);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessage", _alreadyFetchedCategoryCollectionViaMessage);
			info.AddValue("_categoryCollectionViaMessageTemplate", (!this.MarkedForDeletion?_categoryCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessageTemplate", _alwaysFetchCategoryCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessageTemplate", _alreadyFetchedCategoryCollectionViaMessageTemplate);
			info.AddValue("_clientCollectionViaMessage", (!this.MarkedForDeletion?_clientCollectionViaMessage:null));
			info.AddValue("_alwaysFetchClientCollectionViaMessage", _alwaysFetchClientCollectionViaMessage);
			info.AddValue("_alreadyFetchedClientCollectionViaMessage", _alreadyFetchedClientCollectionViaMessage);
			info.AddValue("_companyCollectionViaAnnouncement", (!this.MarkedForDeletion?_companyCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAnnouncement", _alwaysFetchCompanyCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAnnouncement", _alreadyFetchedCompanyCollectionViaAnnouncement);
			info.AddValue("_companyCollectionViaMessage", (!this.MarkedForDeletion?_companyCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMessage", _alwaysFetchCompanyCollectionViaMessage);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMessage", _alreadyFetchedCompanyCollectionViaMessage);
			info.AddValue("_companyCollectionViaMessageTemplate", (!this.MarkedForDeletion?_companyCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMessageTemplate", _alwaysFetchCompanyCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMessageTemplate", _alreadyFetchedCompanyCollectionViaMessageTemplate);
			info.AddValue("_customerCollectionViaMessage", (!this.MarkedForDeletion?_customerCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaMessage", _alwaysFetchCustomerCollectionViaMessage);
			info.AddValue("_alreadyFetchedCustomerCollectionViaMessage", _alreadyFetchedCustomerCollectionViaMessage);
			info.AddValue("_deliverypointCollectionViaMessage", (!this.MarkedForDeletion?_deliverypointCollectionViaMessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessage", _alwaysFetchDeliverypointCollectionViaMessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessage", _alreadyFetchedDeliverypointCollectionViaMessage);
			info.AddValue("_deliverypointgroupCollectionViaAnnouncement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement", _alwaysFetchDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement", _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_entertainmentCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement", _alwaysFetchEntertainmentCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement", _alreadyFetchedEntertainmentCollectionViaAnnouncement);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_entertainmentCollectionViaMessageTemplate", (!this.MarkedForDeletion?_entertainmentCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessageTemplate", _alwaysFetchEntertainmentCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessageTemplate", _alreadyFetchedEntertainmentCollectionViaMessageTemplate);
			info.AddValue("_entertainmentcategoryCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement", _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement", _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement);
			info.AddValue("_entertainmentcategoryCollectionViaAnnouncement_", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_", _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_", _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_);
			info.AddValue("_orderCollectionViaMessage", (!this.MarkedForDeletion?_orderCollectionViaMessage:null));
			info.AddValue("_alwaysFetchOrderCollectionViaMessage", _alwaysFetchOrderCollectionViaMessage);
			info.AddValue("_alreadyFetchedOrderCollectionViaMessage", _alreadyFetchedOrderCollectionViaMessage);
			info.AddValue("_productCollectionViaAnnouncement", (!this.MarkedForDeletion?_productCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement", _alwaysFetchProductCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement", _alreadyFetchedProductCollectionViaAnnouncement);
			info.AddValue("_productCollectionViaMessageTemplate", (!this.MarkedForDeletion?_productCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchProductCollectionViaMessageTemplate", _alwaysFetchProductCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedProductCollectionViaMessageTemplate", _alreadyFetchedProductCollectionViaMessageTemplate);
			info.AddValue("_advertisementEntity", (!this.MarkedForDeletion?_advertisementEntity:null));
			info.AddValue("_advertisementEntityReturnsNewIfNotFound", _advertisementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementEntity", _alwaysFetchAdvertisementEntity);
			info.AddValue("_alreadyFetchedAdvertisementEntity", _alreadyFetchedAdvertisementEntity);
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationoptionEntity", (!this.MarkedForDeletion?_alterationoptionEntity:null));
			info.AddValue("_alterationoptionEntityReturnsNewIfNotFound", _alterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationoptionEntity", _alwaysFetchAlterationoptionEntity);
			info.AddValue("_alreadyFetchedAlterationoptionEntity", _alreadyFetchedAlterationoptionEntity);
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);
			info.AddValue("_carouselItemEntity", (!this.MarkedForDeletion?_carouselItemEntity:null));
			info.AddValue("_carouselItemEntityReturnsNewIfNotFound", _carouselItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCarouselItemEntity", _alwaysFetchCarouselItemEntity);
			info.AddValue("_alreadyFetchedCarouselItemEntity", _alreadyFetchedCarouselItemEntity);
			info.AddValue("_landingPageEntity", (!this.MarkedForDeletion?_landingPageEntity:null));
			info.AddValue("_landingPageEntityReturnsNewIfNotFound", _landingPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLandingPageEntity", _alwaysFetchLandingPageEntity);
			info.AddValue("_alreadyFetchedLandingPageEntity", _alreadyFetchedLandingPageEntity);
			info.AddValue("_widgetActionBannerEntity", (!this.MarkedForDeletion?_widgetActionBannerEntity:null));
			info.AddValue("_widgetActionBannerEntityReturnsNewIfNotFound", _widgetActionBannerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetActionBannerEntity", _alwaysFetchWidgetActionBannerEntity);
			info.AddValue("_alreadyFetchedWidgetActionBannerEntity", _alreadyFetchedWidgetActionBannerEntity);
			info.AddValue("_widgetHeroEntity", (!this.MarkedForDeletion?_widgetHeroEntity:null));
			info.AddValue("_widgetHeroEntityReturnsNewIfNotFound", _widgetHeroEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetHeroEntity", _alwaysFetchWidgetHeroEntity);
			info.AddValue("_alreadyFetchedWidgetHeroEntity", _alreadyFetchedWidgetHeroEntity);
			info.AddValue("_attachmentEntity", (!this.MarkedForDeletion?_attachmentEntity:null));
			info.AddValue("_attachmentEntityReturnsNewIfNotFound", _attachmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttachmentEntity", _alwaysFetchAttachmentEntity);
			info.AddValue("_alreadyFetchedAttachmentEntity", _alreadyFetchedAttachmentEntity);
			info.AddValue("_actionCategoryEntity", (!this.MarkedForDeletion?_actionCategoryEntity:null));
			info.AddValue("_actionCategoryEntityReturnsNewIfNotFound", _actionCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionCategoryEntity", _alwaysFetchActionCategoryEntity);
			info.AddValue("_alreadyFetchedActionCategoryEntity", _alreadyFetchedActionCategoryEntity);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_actionEntertainmentEntity", (!this.MarkedForDeletion?_actionEntertainmentEntity:null));
			info.AddValue("_actionEntertainmentEntityReturnsNewIfNotFound", _actionEntertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionEntertainmentEntity", _alwaysFetchActionEntertainmentEntity);
			info.AddValue("_alreadyFetchedActionEntertainmentEntity", _alreadyFetchedActionEntertainmentEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_actionEntertainmentcategoryEntity", (!this.MarkedForDeletion?_actionEntertainmentcategoryEntity:null));
			info.AddValue("_actionEntertainmentcategoryEntityReturnsNewIfNotFound", _actionEntertainmentcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionEntertainmentcategoryEntity", _alwaysFetchActionEntertainmentcategoryEntity);
			info.AddValue("_alreadyFetchedActionEntertainmentcategoryEntity", _alreadyFetchedActionEntertainmentcategoryEntity);
			info.AddValue("_genericcategoryEntity", (!this.MarkedForDeletion?_genericcategoryEntity:null));
			info.AddValue("_genericcategoryEntityReturnsNewIfNotFound", _genericcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericcategoryEntity", _alwaysFetchGenericcategoryEntity);
			info.AddValue("_alreadyFetchedGenericcategoryEntity", _alreadyFetchedGenericcategoryEntity);
			info.AddValue("_genericproductEntity", (!this.MarkedForDeletion?_genericproductEntity:null));
			info.AddValue("_genericproductEntityReturnsNewIfNotFound", _genericproductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericproductEntity", _alwaysFetchGenericproductEntity);
			info.AddValue("_alreadyFetchedGenericproductEntity", _alreadyFetchedGenericproductEntity);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_pageEntity_", (!this.MarkedForDeletion?_pageEntity_:null));
			info.AddValue("_pageEntity_ReturnsNewIfNotFound", _pageEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity_", _alwaysFetchPageEntity_);
			info.AddValue("_alreadyFetchedPageEntity_", _alreadyFetchedPageEntity_);
			info.AddValue("_pageElementEntity", (!this.MarkedForDeletion?_pageElementEntity:null));
			info.AddValue("_pageElementEntityReturnsNewIfNotFound", _pageElementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageElementEntity", _alwaysFetchPageElementEntity);
			info.AddValue("_alreadyFetchedPageElementEntity", _alreadyFetchedPageElementEntity);
			info.AddValue("_pageTemplateEntity", (!this.MarkedForDeletion?_pageTemplateEntity:null));
			info.AddValue("_pageTemplateEntityReturnsNewIfNotFound", _pageTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageTemplateEntity", _alwaysFetchPageTemplateEntity);
			info.AddValue("_alreadyFetchedPageTemplateEntity", _alreadyFetchedPageTemplateEntity);
			info.AddValue("_pageTemplateElementEntity", (!this.MarkedForDeletion?_pageTemplateElementEntity:null));
			info.AddValue("_pageTemplateElementEntityReturnsNewIfNotFound", _pageTemplateElementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageTemplateElementEntity", _alwaysFetchPageTemplateElementEntity);
			info.AddValue("_alreadyFetchedPageTemplateElementEntity", _alreadyFetchedPageTemplateElementEntity);
			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);
			info.AddValue("_actionProductEntity", (!this.MarkedForDeletion?_actionProductEntity:null));
			info.AddValue("_actionProductEntityReturnsNewIfNotFound", _actionProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionProductEntity", _alwaysFetchActionProductEntity);
			info.AddValue("_alreadyFetchedActionProductEntity", _alreadyFetchedActionProductEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productgroupEntity", (!this.MarkedForDeletion?_productgroupEntity:null));
			info.AddValue("_productgroupEntityReturnsNewIfNotFound", _productgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductgroupEntity", _alwaysFetchProductgroupEntity);
			info.AddValue("_alreadyFetchedProductgroupEntity", _alreadyFetchedProductgroupEntity);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);
			info.AddValue("_roomControlSectionItemEntity", (!this.MarkedForDeletion?_roomControlSectionItemEntity:null));
			info.AddValue("_roomControlSectionItemEntityReturnsNewIfNotFound", _roomControlSectionItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionItemEntity", _alwaysFetchRoomControlSectionItemEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionItemEntity", _alreadyFetchedRoomControlSectionItemEntity);
			info.AddValue("_routestephandlerEntity", (!this.MarkedForDeletion?_routestephandlerEntity:null));
			info.AddValue("_routestephandlerEntityReturnsNewIfNotFound", _routestephandlerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoutestephandlerEntity", _alwaysFetchRoutestephandlerEntity);
			info.AddValue("_alreadyFetchedRoutestephandlerEntity", _alreadyFetchedRoutestephandlerEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);
			info.AddValue("_siteEntity_", (!this.MarkedForDeletion?_siteEntity_:null));
			info.AddValue("_siteEntity_ReturnsNewIfNotFound", _siteEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity_", _alwaysFetchSiteEntity_);
			info.AddValue("_alreadyFetchedSiteEntity_", _alreadyFetchedSiteEntity_);
			info.AddValue("_stationEntity", (!this.MarkedForDeletion?_stationEntity:null));
			info.AddValue("_stationEntityReturnsNewIfNotFound", _stationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchStationEntity", _alwaysFetchStationEntity);
			info.AddValue("_alreadyFetchedStationEntity", _alreadyFetchedStationEntity);
			info.AddValue("_surveyEntity", (!this.MarkedForDeletion?_surveyEntity:null));
			info.AddValue("_surveyEntityReturnsNewIfNotFound", _surveyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyEntity", _alwaysFetchSurveyEntity);
			info.AddValue("_alreadyFetchedSurveyEntity", _alreadyFetchedSurveyEntity);
			info.AddValue("_surveyPageEntity", (!this.MarkedForDeletion?_surveyPageEntity:null));
			info.AddValue("_surveyPageEntityReturnsNewIfNotFound", _surveyPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyPageEntity", _alwaysFetchSurveyPageEntity);
			info.AddValue("_alreadyFetchedSurveyPageEntity", _alreadyFetchedSurveyPageEntity);
			info.AddValue("_uIFooterItemEntity", (!this.MarkedForDeletion?_uIFooterItemEntity:null));
			info.AddValue("_uIFooterItemEntityReturnsNewIfNotFound", _uIFooterItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIFooterItemEntity", _alwaysFetchUIFooterItemEntity);
			info.AddValue("_alreadyFetchedUIFooterItemEntity", _alreadyFetchedUIFooterItemEntity);
			info.AddValue("_uIThemeEntity", (!this.MarkedForDeletion?_uIThemeEntity:null));
			info.AddValue("_uIThemeEntityReturnsNewIfNotFound", _uIThemeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIThemeEntity", _alwaysFetchUIThemeEntity);
			info.AddValue("_alreadyFetchedUIThemeEntity", _alreadyFetchedUIThemeEntity);
			info.AddValue("_uIWidgetEntity", (!this.MarkedForDeletion?_uIWidgetEntity:null));
			info.AddValue("_uIWidgetEntityReturnsNewIfNotFound", _uIWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIWidgetEntity", _alwaysFetchUIWidgetEntity);
			info.AddValue("_alreadyFetchedUIWidgetEntity", _alreadyFetchedUIWidgetEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementEntity":
					_alreadyFetchedAdvertisementEntity = true;
					this.AdvertisementEntity = (AdvertisementEntity)entity;
					break;
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationoptionEntity":
					_alreadyFetchedAlterationoptionEntity = true;
					this.AlterationoptionEntity = (AlterationoptionEntity)entity;
					break;
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "CarouselItemEntity":
					_alreadyFetchedCarouselItemEntity = true;
					this.CarouselItemEntity = (CarouselItemEntity)entity;
					break;
				case "LandingPageEntity":
					_alreadyFetchedLandingPageEntity = true;
					this.LandingPageEntity = (LandingPageEntity)entity;
					break;
				case "WidgetActionBannerEntity":
					_alreadyFetchedWidgetActionBannerEntity = true;
					this.WidgetActionBannerEntity = (WidgetActionBannerEntity)entity;
					break;
				case "WidgetHeroEntity":
					_alreadyFetchedWidgetHeroEntity = true;
					this.WidgetHeroEntity = (WidgetHeroEntity)entity;
					break;
				case "AttachmentEntity":
					_alreadyFetchedAttachmentEntity = true;
					this.AttachmentEntity = (AttachmentEntity)entity;
					break;
				case "ActionCategoryEntity":
					_alreadyFetchedActionCategoryEntity = true;
					this.ActionCategoryEntity = (CategoryEntity)entity;
					break;
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "ActionEntertainmentEntity":
					_alreadyFetchedActionEntertainmentEntity = true;
					this.ActionEntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "ActionEntertainmentcategoryEntity":
					_alreadyFetchedActionEntertainmentcategoryEntity = true;
					this.ActionEntertainmentcategoryEntity = (EntertainmentcategoryEntity)entity;
					break;
				case "GenericcategoryEntity":
					_alreadyFetchedGenericcategoryEntity = true;
					this.GenericcategoryEntity = (GenericcategoryEntity)entity;
					break;
				case "GenericproductEntity":
					_alreadyFetchedGenericproductEntity = true;
					this.GenericproductEntity = (GenericproductEntity)entity;
					break;
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "PageEntity_":
					_alreadyFetchedPageEntity_ = true;
					this.PageEntity_ = (PageEntity)entity;
					break;
				case "PageElementEntity":
					_alreadyFetchedPageElementEntity = true;
					this.PageElementEntity = (PageElementEntity)entity;
					break;
				case "PageTemplateEntity":
					_alreadyFetchedPageTemplateEntity = true;
					this.PageTemplateEntity = (PageTemplateEntity)entity;
					break;
				case "PageTemplateElementEntity":
					_alreadyFetchedPageTemplateElementEntity = true;
					this.PageTemplateElementEntity = (PageTemplateElementEntity)entity;
					break;
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				case "ActionProductEntity":
					_alreadyFetchedActionProductEntity = true;
					this.ActionProductEntity = (ProductEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductgroupEntity":
					_alreadyFetchedProductgroupEntity = true;
					this.ProductgroupEntity = (ProductgroupEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "RoomControlSectionItemEntity":
					_alreadyFetchedRoomControlSectionItemEntity = true;
					this.RoomControlSectionItemEntity = (RoomControlSectionItemEntity)entity;
					break;
				case "RoutestephandlerEntity":
					_alreadyFetchedRoutestephandlerEntity = true;
					this.RoutestephandlerEntity = (RoutestephandlerEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "SiteEntity_":
					_alreadyFetchedSiteEntity_ = true;
					this.SiteEntity_ = (SiteEntity)entity;
					break;
				case "StationEntity":
					_alreadyFetchedStationEntity = true;
					this.StationEntity = (StationEntity)entity;
					break;
				case "SurveyEntity":
					_alreadyFetchedSurveyEntity = true;
					this.SurveyEntity = (SurveyEntity)entity;
					break;
				case "SurveyPageEntity":
					_alreadyFetchedSurveyPageEntity = true;
					this.SurveyPageEntity = (SurveyPageEntity)entity;
					break;
				case "UIFooterItemEntity":
					_alreadyFetchedUIFooterItemEntity = true;
					this.UIFooterItemEntity = (UIFooterItemEntity)entity;
					break;
				case "UIThemeEntity":
					_alreadyFetchedUIThemeEntity = true;
					this.UIThemeEntity = (UIThemeEntity)entity;
					break;
				case "UIWidgetEntity":
					_alreadyFetchedUIWidgetEntity = true;
					this.UIWidgetEntity = (UIWidgetEntity)entity;
					break;
				case "AnnouncementCollection":
					_alreadyFetchedAnnouncementCollection = true;
					if(entity!=null)
					{
						this.AnnouncementCollection.Add((AnnouncementEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "MediaCultureCollection":
					_alreadyFetchedMediaCultureCollection = true;
					if(entity!=null)
					{
						this.MediaCultureCollection.Add((MediaCultureEntity)entity);
					}
					break;
				case "MediaLanguageCollection":
					_alreadyFetchedMediaLanguageCollection = true;
					if(entity!=null)
					{
						this.MediaLanguageCollection.Add((MediaLanguageEntity)entity);
					}
					break;
				case "MediaRatioTypeMediaCollection":
					_alreadyFetchedMediaRatioTypeMediaCollection = true;
					if(entity!=null)
					{
						this.MediaRatioTypeMediaCollection.Add((MediaRatioTypeMediaEntity)entity);
					}
					break;
				case "MediaRelationshipCollection":
					_alreadyFetchedMediaRelationshipCollection = true;
					if(entity!=null)
					{
						this.MediaRelationshipCollection.Add((MediaRelationshipEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageTemplateCollection":
					_alreadyFetchedMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCollection.Add((MessageTemplateEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				case "UIScheduleItemCollection":
					_alreadyFetchedUIScheduleItemCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemCollection.Add((UIScheduleItemEntity)entity);
					}
					break;
				case "CategoryCollectionViaAnnouncement":
					_alreadyFetchedCategoryCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAnnouncement.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaAnnouncement_":
					_alreadyFetchedCategoryCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAnnouncement_.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessage":
					_alreadyFetchedCategoryCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessage.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessageTemplate":
					_alreadyFetchedCategoryCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessageTemplate.Add((CategoryEntity)entity);
					}
					break;
				case "ClientCollectionViaMessage":
					_alreadyFetchedClientCollectionViaMessage = true;
					if(entity!=null)
					{
						this.ClientCollectionViaMessage.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaAnnouncement":
					_alreadyFetchedCompanyCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAnnouncement.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMessage":
					_alreadyFetchedCompanyCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMessage.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMessageTemplate":
					_alreadyFetchedCompanyCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMessageTemplate.Add((CompanyEntity)entity);
					}
					break;
				case "CustomerCollectionViaMessage":
					_alreadyFetchedCustomerCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaMessage.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessage":
					_alreadyFetchedDeliverypointCollectionViaMessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAnnouncement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessageTemplate":
					_alreadyFetchedEntertainmentCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessageTemplate.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAnnouncement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement_":
					_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAnnouncement_.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "OrderCollectionViaMessage":
					_alreadyFetchedOrderCollectionViaMessage = true;
					if(entity!=null)
					{
						this.OrderCollectionViaMessage.Add((OrderEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement":
					_alreadyFetchedProductCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMessageTemplate":
					_alreadyFetchedProductCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMessageTemplate.Add((ProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					SetupSyncAdvertisementEntity(relatedEntity);
					break;
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationoptionEntity":
					SetupSyncAlterationoptionEntity(relatedEntity);
					break;
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "CarouselItemEntity":
					SetupSyncCarouselItemEntity(relatedEntity);
					break;
				case "LandingPageEntity":
					SetupSyncLandingPageEntity(relatedEntity);
					break;
				case "WidgetActionBannerEntity":
					SetupSyncWidgetActionBannerEntity(relatedEntity);
					break;
				case "WidgetHeroEntity":
					SetupSyncWidgetHeroEntity(relatedEntity);
					break;
				case "AttachmentEntity":
					SetupSyncAttachmentEntity(relatedEntity);
					break;
				case "ActionCategoryEntity":
					SetupSyncActionCategoryEntity(relatedEntity);
					break;
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "ActionEntertainmentEntity":
					SetupSyncActionEntertainmentEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "ActionEntertainmentcategoryEntity":
					SetupSyncActionEntertainmentcategoryEntity(relatedEntity);
					break;
				case "GenericcategoryEntity":
					SetupSyncGenericcategoryEntity(relatedEntity);
					break;
				case "GenericproductEntity":
					SetupSyncGenericproductEntity(relatedEntity);
					break;
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "PageEntity_":
					SetupSyncPageEntity_(relatedEntity);
					break;
				case "PageElementEntity":
					SetupSyncPageElementEntity(relatedEntity);
					break;
				case "PageTemplateEntity":
					SetupSyncPageTemplateEntity(relatedEntity);
					break;
				case "PageTemplateElementEntity":
					SetupSyncPageTemplateElementEntity(relatedEntity);
					break;
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				case "ActionProductEntity":
					SetupSyncActionProductEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductgroupEntity":
					SetupSyncProductgroupEntity(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "RoomControlSectionItemEntity":
					SetupSyncRoomControlSectionItemEntity(relatedEntity);
					break;
				case "RoutestephandlerEntity":
					SetupSyncRoutestephandlerEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "SiteEntity_":
					SetupSyncSiteEntity_(relatedEntity);
					break;
				case "StationEntity":
					SetupSyncStationEntity(relatedEntity);
					break;
				case "SurveyEntity":
					SetupSyncSurveyEntity(relatedEntity);
					break;
				case "SurveyPageEntity":
					SetupSyncSurveyPageEntity(relatedEntity);
					break;
				case "UIFooterItemEntity":
					SetupSyncUIFooterItemEntity(relatedEntity);
					break;
				case "UIThemeEntity":
					SetupSyncUIThemeEntity(relatedEntity);
					break;
				case "UIWidgetEntity":
					SetupSyncUIWidgetEntity(relatedEntity);
					break;
				case "AnnouncementCollection":
					_announcementCollection.Add((AnnouncementEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "MediaCultureCollection":
					_mediaCultureCollection.Add((MediaCultureEntity)relatedEntity);
					break;
				case "MediaLanguageCollection":
					_mediaLanguageCollection.Add((MediaLanguageEntity)relatedEntity);
					break;
				case "MediaRatioTypeMediaCollection":
					_mediaRatioTypeMediaCollection.Add((MediaRatioTypeMediaEntity)relatedEntity);
					break;
				case "MediaRelationshipCollection":
					_mediaRelationshipCollection.Add((MediaRelationshipEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageTemplateCollection":
					_messageTemplateCollection.Add((MessageTemplateEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				case "UIScheduleItemCollection":
					_uIScheduleItemCollection.Add((UIScheduleItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					DesetupSyncAdvertisementEntity(false, true);
					break;
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationoptionEntity":
					DesetupSyncAlterationoptionEntity(false, true);
					break;
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "CarouselItemEntity":
					DesetupSyncCarouselItemEntity(false, true);
					break;
				case "LandingPageEntity":
					DesetupSyncLandingPageEntity(false, true);
					break;
				case "WidgetActionBannerEntity":
					DesetupSyncWidgetActionBannerEntity(false, true);
					break;
				case "WidgetHeroEntity":
					DesetupSyncWidgetHeroEntity(false, true);
					break;
				case "AttachmentEntity":
					DesetupSyncAttachmentEntity(false, true);
					break;
				case "ActionCategoryEntity":
					DesetupSyncActionCategoryEntity(false, true);
					break;
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "ActionEntertainmentEntity":
					DesetupSyncActionEntertainmentEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "ActionEntertainmentcategoryEntity":
					DesetupSyncActionEntertainmentcategoryEntity(false, true);
					break;
				case "GenericcategoryEntity":
					DesetupSyncGenericcategoryEntity(false, true);
					break;
				case "GenericproductEntity":
					DesetupSyncGenericproductEntity(false, true);
					break;
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "PageEntity_":
					DesetupSyncPageEntity_(false, true);
					break;
				case "PageElementEntity":
					DesetupSyncPageElementEntity(false, true);
					break;
				case "PageTemplateEntity":
					DesetupSyncPageTemplateEntity(false, true);
					break;
				case "PageTemplateElementEntity":
					DesetupSyncPageTemplateElementEntity(false, true);
					break;
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				case "ActionProductEntity":
					DesetupSyncActionProductEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductgroupEntity":
					DesetupSyncProductgroupEntity(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "RoomControlSectionItemEntity":
					DesetupSyncRoomControlSectionItemEntity(false, true);
					break;
				case "RoutestephandlerEntity":
					DesetupSyncRoutestephandlerEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "SiteEntity_":
					DesetupSyncSiteEntity_(false, true);
					break;
				case "StationEntity":
					DesetupSyncStationEntity(false, true);
					break;
				case "SurveyEntity":
					DesetupSyncSurveyEntity(false, true);
					break;
				case "SurveyPageEntity":
					DesetupSyncSurveyPageEntity(false, true);
					break;
				case "UIFooterItemEntity":
					DesetupSyncUIFooterItemEntity(false, true);
					break;
				case "UIThemeEntity":
					DesetupSyncUIThemeEntity(false, true);
					break;
				case "UIWidgetEntity":
					DesetupSyncUIWidgetEntity(false, true);
					break;
				case "AnnouncementCollection":
					this.PerformRelatedEntityRemoval(_announcementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCultureCollection":
					this.PerformRelatedEntityRemoval(_mediaCultureCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaLanguageCollection":
					this.PerformRelatedEntityRemoval(_mediaLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaRatioTypeMediaCollection":
					this.PerformRelatedEntityRemoval(_mediaRatioTypeMediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaRelationshipCollection":
					this.PerformRelatedEntityRemoval(_mediaRelationshipCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_advertisementEntity!=null)
			{
				toReturn.Add(_advertisementEntity);
			}
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationoptionEntity!=null)
			{
				toReturn.Add(_alterationoptionEntity);
			}
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			if(_carouselItemEntity!=null)
			{
				toReturn.Add(_carouselItemEntity);
			}
			if(_landingPageEntity!=null)
			{
				toReturn.Add(_landingPageEntity);
			}
			if(_widgetActionBannerEntity!=null)
			{
				toReturn.Add(_widgetActionBannerEntity);
			}
			if(_widgetHeroEntity!=null)
			{
				toReturn.Add(_widgetHeroEntity);
			}
			if(_attachmentEntity!=null)
			{
				toReturn.Add(_attachmentEntity);
			}
			if(_actionCategoryEntity!=null)
			{
				toReturn.Add(_actionCategoryEntity);
			}
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_actionEntertainmentEntity!=null)
			{
				toReturn.Add(_actionEntertainmentEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_actionEntertainmentcategoryEntity!=null)
			{
				toReturn.Add(_actionEntertainmentcategoryEntity);
			}
			if(_genericcategoryEntity!=null)
			{
				toReturn.Add(_genericcategoryEntity);
			}
			if(_genericproductEntity!=null)
			{
				toReturn.Add(_genericproductEntity);
			}
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_pageEntity_!=null)
			{
				toReturn.Add(_pageEntity_);
			}
			if(_pageElementEntity!=null)
			{
				toReturn.Add(_pageElementEntity);
			}
			if(_pageTemplateEntity!=null)
			{
				toReturn.Add(_pageTemplateEntity);
			}
			if(_pageTemplateElementEntity!=null)
			{
				toReturn.Add(_pageTemplateElementEntity);
			}
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			if(_actionProductEntity!=null)
			{
				toReturn.Add(_actionProductEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productgroupEntity!=null)
			{
				toReturn.Add(_productgroupEntity);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			if(_roomControlSectionItemEntity!=null)
			{
				toReturn.Add(_roomControlSectionItemEntity);
			}
			if(_routestephandlerEntity!=null)
			{
				toReturn.Add(_routestephandlerEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			if(_siteEntity_!=null)
			{
				toReturn.Add(_siteEntity_);
			}
			if(_stationEntity!=null)
			{
				toReturn.Add(_stationEntity);
			}
			if(_surveyEntity!=null)
			{
				toReturn.Add(_surveyEntity);
			}
			if(_surveyPageEntity!=null)
			{
				toReturn.Add(_surveyPageEntity);
			}
			if(_uIFooterItemEntity!=null)
			{
				toReturn.Add(_uIFooterItemEntity);
			}
			if(_uIThemeEntity!=null)
			{
				toReturn.Add(_uIThemeEntity);
			}
			if(_uIWidgetEntity!=null)
			{
				toReturn.Add(_uIWidgetEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_announcementCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_mediaCultureCollection);
			toReturn.Add(_mediaLanguageCollection);
			toReturn.Add(_mediaRatioTypeMediaCollection);
			toReturn.Add(_mediaRelationshipCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageTemplateCollection);
			toReturn.Add(_scheduledMessageCollection);
			toReturn.Add(_uIScheduleItemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaId)
		{
			return FetchUsingPK(mediaId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(mediaId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(mediaId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(mediaId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MediaId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MediaRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection || forceFetch || _alwaysFetchAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection);
				_announcementCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, filter);
				_announcementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection = true;
			}
			return _announcementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection'. These settings will be taken into account
		/// when the property AnnouncementCollection is requested or GetMultiAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection.SortClauses=sortClauses;
			_announcementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCultureCollection GetMultiMediaCultureCollection(bool forceFetch)
		{
			return GetMultiMediaCultureCollection(forceFetch, _mediaCultureCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCultureCollection GetMultiMediaCultureCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCultureCollection(forceFetch, _mediaCultureCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCultureCollection GetMultiMediaCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCultureCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCultureCollection GetMultiMediaCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCultureCollection || forceFetch || _alwaysFetchMediaCultureCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCultureCollection);
				_mediaCultureCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCultureCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCultureCollection.GetMultiManyToOne(this, filter);
				_mediaCultureCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCultureCollection = true;
			}
			return _mediaCultureCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCultureCollection'. These settings will be taken into account
		/// when the property MediaCultureCollection is requested or GetMultiMediaCultureCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCultureCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCultureCollection.SortClauses=sortClauses;
			_mediaCultureCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch)
		{
			return GetMultiMediaLanguageCollection(forceFetch, _mediaLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaLanguageCollection(forceFetch, _mediaLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaLanguageCollection || forceFetch || _alwaysFetchMediaLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaLanguageCollection);
				_mediaLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaLanguageCollection.GetMultiManyToOne(null, this, filter);
				_mediaLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaLanguageCollection = true;
			}
			return _mediaLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaLanguageCollection'. These settings will be taken into account
		/// when the property MediaLanguageCollection is requested or GetMultiMediaLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaLanguageCollection.SortClauses=sortClauses;
			_mediaLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaRatioTypeMediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaRatioTypeMediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection GetMultiMediaRatioTypeMediaCollection(bool forceFetch)
		{
			return GetMultiMediaRatioTypeMediaCollection(forceFetch, _mediaRatioTypeMediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaRatioTypeMediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaRatioTypeMediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection GetMultiMediaRatioTypeMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaRatioTypeMediaCollection(forceFetch, _mediaRatioTypeMediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaRatioTypeMediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection GetMultiMediaRatioTypeMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaRatioTypeMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaRatioTypeMediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection GetMultiMediaRatioTypeMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaRatioTypeMediaCollection || forceFetch || _alwaysFetchMediaRatioTypeMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaRatioTypeMediaCollection);
				_mediaRatioTypeMediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaRatioTypeMediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaRatioTypeMediaCollection.GetMultiManyToOne(this, filter);
				_mediaRatioTypeMediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaRatioTypeMediaCollection = true;
			}
			return _mediaRatioTypeMediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaRatioTypeMediaCollection'. These settings will be taken into account
		/// when the property MediaRatioTypeMediaCollection is requested or GetMultiMediaRatioTypeMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaRatioTypeMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaRatioTypeMediaCollection.SortClauses=sortClauses;
			_mediaRatioTypeMediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaRelationshipEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaRelationshipEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaRelationshipCollection GetMultiMediaRelationshipCollection(bool forceFetch)
		{
			return GetMultiMediaRelationshipCollection(forceFetch, _mediaRelationshipCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaRelationshipEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaRelationshipEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaRelationshipCollection GetMultiMediaRelationshipCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaRelationshipCollection(forceFetch, _mediaRelationshipCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaRelationshipEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaRelationshipCollection GetMultiMediaRelationshipCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaRelationshipCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaRelationshipEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaRelationshipCollection GetMultiMediaRelationshipCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaRelationshipCollection || forceFetch || _alwaysFetchMediaRelationshipCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaRelationshipCollection);
				_mediaRelationshipCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaRelationshipCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaRelationshipCollection.GetMultiManyToOne(this, null, filter);
				_mediaRelationshipCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaRelationshipCollection = true;
			}
			return _mediaRelationshipCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaRelationshipCollection'. These settings will be taken into account
		/// when the property MediaRelationshipCollection is requested or GetMultiMediaRelationshipCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaRelationshipCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaRelationshipCollection.SortClauses=sortClauses;
			_mediaRelationshipCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCollection);
				_messageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_messageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCollection = true;
			}
			return _messageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCollection is requested or GetMultiMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemCollection || forceFetch || _alwaysFetchUIScheduleItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemCollection);
				_uIScheduleItemCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_uIScheduleItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemCollection = true;
			}
			return _uIScheduleItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemCollection'. These settings will be taken into account
		/// when the property UIScheduleItemCollection is requested or GetMultiUIScheduleItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemCollection.SortClauses=sortClauses;
			_uIScheduleItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAnnouncement(forceFetch, _categoryCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAnnouncement || forceFetch || _alwaysFetchCategoryCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_categoryCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAnnouncement"));
				_categoryCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAnnouncement = true;
			}
			return _categoryCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAnnouncement is requested or GetMultiCategoryCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAnnouncement.SortClauses=sortClauses;
			_categoryCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAnnouncement_(forceFetch, _categoryCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAnnouncement_ || forceFetch || _alwaysFetchCategoryCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_categoryCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAnnouncement_"));
				_categoryCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAnnouncement_ = true;
			}
			return _categoryCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property CategoryCollectionViaAnnouncement_ is requested or GetMultiCategoryCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAnnouncement_.SortClauses=sortClauses;
			_categoryCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessage(forceFetch, _categoryCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessage || forceFetch || _alwaysFetchCategoryCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessage.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessage"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessage = true;
			}
			return _categoryCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessage'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessage is requested or GetMultiCategoryCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessage.SortClauses=sortClauses;
			_categoryCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessageTemplate(forceFetch, _categoryCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessageTemplate || forceFetch || _alwaysFetchCategoryCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_categoryCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessageTemplate"));
				_categoryCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessageTemplate = true;
			}
			return _categoryCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessageTemplate is requested or GetMultiCategoryCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessageTemplate.SortClauses=sortClauses;
			_categoryCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch)
		{
			return GetMultiClientCollectionViaMessage(forceFetch, _clientCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaMessage || forceFetch || _alwaysFetchClientCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaMessage.GetMulti(filter, GetRelationsForField("ClientCollectionViaMessage"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaMessage = true;
			}
			return _clientCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaMessage'. These settings will be taken into account
		/// when the property ClientCollectionViaMessage is requested or GetMultiClientCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaMessage.SortClauses=sortClauses;
			_clientCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAnnouncement(forceFetch, _companyCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAnnouncement || forceFetch || _alwaysFetchCompanyCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_companyCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAnnouncement"));
				_companyCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAnnouncement = true;
			}
			return _companyCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property CompanyCollectionViaAnnouncement is requested or GetMultiCompanyCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAnnouncement.SortClauses=sortClauses;
			_companyCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMessage(forceFetch, _companyCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMessage || forceFetch || _alwaysFetchCompanyCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMessage.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMessage"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMessage = true;
			}
			return _companyCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMessage'. These settings will be taken into account
		/// when the property CompanyCollectionViaMessage is requested or GetMultiCompanyCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMessage.SortClauses=sortClauses;
			_companyCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMessageTemplate(forceFetch, _companyCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMessageTemplate || forceFetch || _alwaysFetchCompanyCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_companyCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMessageTemplate"));
				_companyCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMessageTemplate = true;
			}
			return _companyCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property CompanyCollectionViaMessageTemplate is requested or GetMultiCompanyCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMessageTemplate.SortClauses=sortClauses;
			_companyCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaMessage(forceFetch, _customerCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaMessage || forceFetch || _alwaysFetchCustomerCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaMessage.GetMulti(filter, GetRelationsForField("CustomerCollectionViaMessage"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaMessage = true;
			}
			return _customerCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaMessage'. These settings will be taken into account
		/// when the property CustomerCollectionViaMessage is requested or GetMultiCustomerCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaMessage.SortClauses=sortClauses;
			_customerCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessage(forceFetch, _deliverypointCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessage || forceFetch || _alwaysFetchDeliverypointCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessage"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessage = true;
			}
			return _deliverypointCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessage is requested or GetMultiDeliverypointCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessage.SortClauses=sortClauses;
			_deliverypointCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAnnouncement(forceFetch, _deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
			}
			return _deliverypointgroupCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAnnouncement is requested or GetMultiDeliverypointgroupCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAnnouncement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement(forceFetch, _entertainmentCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
			}
			return _entertainmentCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement is requested or GetMultiEntertainmentCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessageTemplate(forceFetch, _entertainmentCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessageTemplate || forceFetch || _alwaysFetchEntertainmentCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_entertainmentCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessageTemplate"));
				_entertainmentCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessageTemplate = true;
			}
			return _entertainmentCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessageTemplate is requested or GetMultiEntertainmentCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessageTemplate.SortClauses=sortClauses;
			_entertainmentCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAnnouncement(forceFetch, _entertainmentcategoryCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_entertainmentcategoryCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement"));
				_entertainmentcategoryCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = true;
			}
			return _entertainmentcategoryCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAnnouncement is requested or GetMultiEntertainmentcategoryCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAnnouncement_(forceFetch, _entertainmentcategoryCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_entertainmentcategoryCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement_"));
				_entertainmentcategoryCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = true;
			}
			return _entertainmentcategoryCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAnnouncement_ is requested or GetMultiEntertainmentcategoryCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAnnouncement_.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch)
		{
			return GetMultiOrderCollectionViaMessage(forceFetch, _orderCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaMessage || forceFetch || _alwaysFetchOrderCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaMessage.GetMulti(filter, GetRelationsForField("OrderCollectionViaMessage"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaMessage = true;
			}
			return _orderCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaMessage'. These settings will be taken into account
		/// when the property OrderCollectionViaMessage is requested or GetMultiOrderCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaMessage.SortClauses=sortClauses;
			_orderCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement(forceFetch, _productCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement || forceFetch || _alwaysFetchProductCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement = true;
			}
			return _productCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement is requested or GetMultiProductCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement.SortClauses=sortClauses;
			_productCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiProductCollectionViaMessageTemplate(forceFetch, _productCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMessageTemplate || forceFetch || _alwaysFetchProductCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MediaFields.MediaId, ComparisonOperator.Equal, this.MediaId, "MediaEntity__"));
				_productCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("ProductCollectionViaMessageTemplate"));
				_productCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMessageTemplate = true;
			}
			return _productCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property ProductCollectionViaMessageTemplate is requested or GetMultiProductCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMessageTemplate.SortClauses=sortClauses;
			_productCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public AdvertisementEntity GetSingleAdvertisementEntity()
		{
			return GetSingleAdvertisementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public virtual AdvertisementEntity GetSingleAdvertisementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementEntity || forceFetch || _alwaysFetchAdvertisementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementEntityUsingAdvertisementId);
				AdvertisementEntity newEntity = new AdvertisementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementEntity = newEntity;
				_alreadyFetchedAdvertisementEntity = fetchResult;
			}
			return _advertisementEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public AlterationoptionEntity GetSingleAlterationoptionEntity()
		{
			return GetSingleAlterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public virtual AlterationoptionEntity GetSingleAlterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationoptionEntity || forceFetch || _alwaysFetchAlterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationoptionEntityUsingAlterationoptionId);
				AlterationoptionEntity newEntity = new AlterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationoptionEntity = newEntity;
				_alreadyFetchedAlterationoptionEntity = fetchResult;
			}
			return _alterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CarouselItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CarouselItemEntity' which is related to this entity.</returns>
		public CarouselItemEntity GetSingleCarouselItemEntity()
		{
			return GetSingleCarouselItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CarouselItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CarouselItemEntity' which is related to this entity.</returns>
		public virtual CarouselItemEntity GetSingleCarouselItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCarouselItemEntity || forceFetch || _alwaysFetchCarouselItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CarouselItemEntityUsingCarouselItemId);
				CarouselItemEntity newEntity = new CarouselItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CarouselItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CarouselItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_carouselItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CarouselItemEntity = newEntity;
				_alreadyFetchedCarouselItemEntity = fetchResult;
			}
			return _carouselItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public LandingPageEntity GetSingleLandingPageEntity()
		{
			return GetSingleLandingPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public virtual LandingPageEntity GetSingleLandingPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLandingPageEntity || forceFetch || _alwaysFetchLandingPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LandingPageEntityUsingLandingPageId);
				LandingPageEntity newEntity = new LandingPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LandingPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LandingPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_landingPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LandingPageEntity = newEntity;
				_alreadyFetchedLandingPageEntity = fetchResult;
			}
			return _landingPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetActionBannerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetActionBannerEntity' which is related to this entity.</returns>
		public WidgetActionBannerEntity GetSingleWidgetActionBannerEntity()
		{
			return GetSingleWidgetActionBannerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetActionBannerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetActionBannerEntity' which is related to this entity.</returns>
		public virtual WidgetActionBannerEntity GetSingleWidgetActionBannerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetActionBannerEntity || forceFetch || _alwaysFetchWidgetActionBannerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetActionBannerEntityUsingWidgetId);
				WidgetActionBannerEntity newEntity = (WidgetActionBannerEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetActionBannerEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetActionBannerEntity.FetchPolymorphic(this.Transaction, this.WidgetId.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetActionBannerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetActionBannerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetActionBannerEntity = newEntity;
				_alreadyFetchedWidgetActionBannerEntity = fetchResult;
			}
			return _widgetActionBannerEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetHeroEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetHeroEntity' which is related to this entity.</returns>
		public WidgetHeroEntity GetSingleWidgetHeroEntity()
		{
			return GetSingleWidgetHeroEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetHeroEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetHeroEntity' which is related to this entity.</returns>
		public virtual WidgetHeroEntity GetSingleWidgetHeroEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetHeroEntity || forceFetch || _alwaysFetchWidgetHeroEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetHeroEntityUsingWidgetId);
				WidgetHeroEntity newEntity = (WidgetHeroEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetHeroEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetHeroEntity.FetchPolymorphic(this.Transaction, this.WidgetId.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetHeroEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetHeroEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetHeroEntity = newEntity;
				_alreadyFetchedWidgetHeroEntity = fetchResult;
			}
			return _widgetHeroEntity;
		}


		/// <summary> Retrieves the related entity of type 'AttachmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttachmentEntity' which is related to this entity.</returns>
		public AttachmentEntity GetSingleAttachmentEntity()
		{
			return GetSingleAttachmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AttachmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttachmentEntity' which is related to this entity.</returns>
		public virtual AttachmentEntity GetSingleAttachmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttachmentEntity || forceFetch || _alwaysFetchAttachmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttachmentEntityUsingAttachmentId);
				AttachmentEntity newEntity = new AttachmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttachmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttachmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attachmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttachmentEntity = newEntity;
				_alreadyFetchedAttachmentEntity = fetchResult;
			}
			return _attachmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleActionCategoryEntity()
		{
			return GetSingleActionCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleActionCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionCategoryEntity || forceFetch || _alwaysFetchActionCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingActionCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionCategoryEntity = newEntity;
				_alreadyFetchedActionCategoryEntity = fetchResult;
			}
			return _actionCategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleActionEntertainmentEntity()
		{
			return GetSingleActionEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleActionEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionEntertainmentEntity || forceFetch || _alwaysFetchActionEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingActionEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionEntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionEntertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionEntertainmentEntity = newEntity;
				_alreadyFetchedActionEntertainmentEntity = fetchResult;
			}
			return _actionEntertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleActionEntertainmentcategoryEntity()
		{
			return GetSingleActionEntertainmentcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleActionEntertainmentcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionEntertainmentcategoryEntity || forceFetch || _alwaysFetchActionEntertainmentcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionEntertainmentcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionEntertainmentcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionEntertainmentcategoryEntity = newEntity;
				_alreadyFetchedActionEntertainmentcategoryEntity = fetchResult;
			}
			return _actionEntertainmentcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public GenericcategoryEntity GetSingleGenericcategoryEntity()
		{
			return GetSingleGenericcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public virtual GenericcategoryEntity GetSingleGenericcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericcategoryEntity || forceFetch || _alwaysFetchGenericcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericcategoryEntityUsingGenericcategoryId);
				GenericcategoryEntity newEntity = new GenericcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericcategoryEntity = newEntity;
				_alreadyFetchedGenericcategoryEntity = fetchResult;
			}
			return _genericcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericproductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericproductEntity' which is related to this entity.</returns>
		public GenericproductEntity GetSingleGenericproductEntity()
		{
			return GetSingleGenericproductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericproductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericproductEntity' which is related to this entity.</returns>
		public virtual GenericproductEntity GetSingleGenericproductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericproductEntity || forceFetch || _alwaysFetchGenericproductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericproductEntityUsingGenericproductId);
				GenericproductEntity newEntity = new GenericproductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericproductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericproductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericproductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericproductEntity = newEntity;
				_alreadyFetchedGenericproductEntity = fetchResult;
			}
			return _genericproductEntity;
		}


		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaIdAgnosticMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AgnosticMediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity_()
		{
			return GetSinglePageEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity_ || forceFetch || _alwaysFetchPageEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingActionPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity_ = newEntity;
				_alreadyFetchedPageEntity_ = fetchResult;
			}
			return _pageEntity_;
		}


		/// <summary> Retrieves the related entity of type 'PageElementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageElementEntity' which is related to this entity.</returns>
		public PageElementEntity GetSinglePageElementEntity()
		{
			return GetSinglePageElementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageElementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageElementEntity' which is related to this entity.</returns>
		public virtual PageElementEntity GetSinglePageElementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageElementEntity || forceFetch || _alwaysFetchPageElementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageElementEntityUsingPageElementId);
				PageElementEntity newEntity = new PageElementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageElementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageElementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageElementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageElementEntity = newEntity;
				_alreadyFetchedPageElementEntity = fetchResult;
			}
			return _pageElementEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public PageTemplateEntity GetSinglePageTemplateEntity()
		{
			return GetSinglePageTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public virtual PageTemplateEntity GetSinglePageTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageTemplateEntity || forceFetch || _alwaysFetchPageTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageTemplateEntityUsingPageTemplateId);
				PageTemplateEntity newEntity = new PageTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageTemplateEntity = newEntity;
				_alreadyFetchedPageTemplateEntity = fetchResult;
			}
			return _pageTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageTemplateElementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageTemplateElementEntity' which is related to this entity.</returns>
		public PageTemplateElementEntity GetSinglePageTemplateElementEntity()
		{
			return GetSinglePageTemplateElementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateElementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageTemplateElementEntity' which is related to this entity.</returns>
		public virtual PageTemplateElementEntity GetSinglePageTemplateElementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageTemplateElementEntity || forceFetch || _alwaysFetchPageTemplateElementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageTemplateElementEntityUsingPageTemplateElementId);
				PageTemplateElementEntity newEntity = new PageTemplateElementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageTemplateElementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageTemplateElementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageTemplateElementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageTemplateElementEntity = newEntity;
				_alreadyFetchedPageTemplateElementEntity = fetchResult;
			}
			return _pageTemplateElementEntity;
		}


		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleActionProductEntity()
		{
			return GetSingleActionProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleActionProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionProductEntity || forceFetch || _alwaysFetchActionProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingActionProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionProductEntity = newEntity;
				_alreadyFetchedActionProductEntity = fetchResult;
			}
			return _actionProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public ProductgroupEntity GetSingleProductgroupEntity()
		{
			return GetSingleProductgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public virtual ProductgroupEntity GetSingleProductgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductgroupEntity || forceFetch || _alwaysFetchProductgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductgroupEntityUsingProductgroupId);
				ProductgroupEntity newEntity = new ProductgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductgroupEntity = newEntity;
				_alreadyFetchedProductgroupEntity = fetchResult;
			}
			return _productgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity()
		{
			return GetSingleRoomControlSectionItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionItemEntity || forceFetch || _alwaysFetchRoomControlSectionItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
				RoomControlSectionItemEntity newEntity = new RoomControlSectionItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionItemEntity = newEntity;
				_alreadyFetchedRoomControlSectionItemEntity = fetchResult;
			}
			return _roomControlSectionItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoutestephandlerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoutestephandlerEntity' which is related to this entity.</returns>
		public RoutestephandlerEntity GetSingleRoutestephandlerEntity()
		{
			return GetSingleRoutestephandlerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoutestephandlerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoutestephandlerEntity' which is related to this entity.</returns>
		public virtual RoutestephandlerEntity GetSingleRoutestephandlerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoutestephandlerEntity || forceFetch || _alwaysFetchRoutestephandlerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoutestephandlerEntityUsingRoutestephandlerId);
				RoutestephandlerEntity newEntity = new RoutestephandlerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoutestephandlerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoutestephandlerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routestephandlerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoutestephandlerEntity = newEntity;
				_alreadyFetchedRoutestephandlerEntity = fetchResult;
			}
			return _routestephandlerEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity_()
		{
			return GetSingleSiteEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity_ || forceFetch || _alwaysFetchSiteEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingActionSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionSiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity_ = newEntity;
				_alreadyFetchedSiteEntity_ = fetchResult;
			}
			return _siteEntity_;
		}


		/// <summary> Retrieves the related entity of type 'StationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StationEntity' which is related to this entity.</returns>
		public StationEntity GetSingleStationEntity()
		{
			return GetSingleStationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'StationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StationEntity' which is related to this entity.</returns>
		public virtual StationEntity GetSingleStationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedStationEntity || forceFetch || _alwaysFetchStationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StationEntityUsingStationId);
				StationEntity newEntity = new StationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (StationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_stationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.StationEntity = newEntity;
				_alreadyFetchedStationEntity = fetchResult;
			}
			return _stationEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public SurveyEntity GetSingleSurveyEntity()
		{
			return GetSingleSurveyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public virtual SurveyEntity GetSingleSurveyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyEntity || forceFetch || _alwaysFetchSurveyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyEntityUsingSurveyId);
				SurveyEntity newEntity = new SurveyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyEntity = newEntity;
				_alreadyFetchedSurveyEntity = fetchResult;
			}
			return _surveyEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyPageEntity' which is related to this entity.</returns>
		public SurveyPageEntity GetSingleSurveyPageEntity()
		{
			return GetSingleSurveyPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyPageEntity' which is related to this entity.</returns>
		public virtual SurveyPageEntity GetSingleSurveyPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyPageEntity || forceFetch || _alwaysFetchSurveyPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyPageEntityUsingSurveyPageId);
				SurveyPageEntity newEntity = new SurveyPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyPageEntity = newEntity;
				_alreadyFetchedSurveyPageEntity = fetchResult;
			}
			return _surveyPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIFooterItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIFooterItemEntity' which is related to this entity.</returns>
		public UIFooterItemEntity GetSingleUIFooterItemEntity()
		{
			return GetSingleUIFooterItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIFooterItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIFooterItemEntity' which is related to this entity.</returns>
		public virtual UIFooterItemEntity GetSingleUIFooterItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIFooterItemEntity || forceFetch || _alwaysFetchUIFooterItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIFooterItemEntityUsingUIFooterItemId);
				UIFooterItemEntity newEntity = new UIFooterItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIFooterItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIFooterItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIFooterItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIFooterItemEntity = newEntity;
				_alreadyFetchedUIFooterItemEntity = fetchResult;
			}
			return _uIFooterItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public UIThemeEntity GetSingleUIThemeEntity()
		{
			return GetSingleUIThemeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public virtual UIThemeEntity GetSingleUIThemeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIThemeEntity || forceFetch || _alwaysFetchUIThemeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIThemeEntityUsingUIThemeId);
				UIThemeEntity newEntity = new UIThemeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIThemeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIThemeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIThemeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIThemeEntity = newEntity;
				_alreadyFetchedUIThemeEntity = fetchResult;
			}
			return _uIThemeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public UIWidgetEntity GetSingleUIWidgetEntity()
		{
			return GetSingleUIWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public virtual UIWidgetEntity GetSingleUIWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIWidgetEntity || forceFetch || _alwaysFetchUIWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIWidgetEntityUsingUIWidgetId);
				UIWidgetEntity newEntity = new UIWidgetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIWidgetId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIWidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIWidgetEntity = newEntity;
				_alreadyFetchedUIWidgetEntity = fetchResult;
			}
			return _uIWidgetEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementEntity", _advertisementEntity);
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationoptionEntity", _alterationoptionEntity);
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("CarouselItemEntity", _carouselItemEntity);
			toReturn.Add("LandingPageEntity", _landingPageEntity);
			toReturn.Add("WidgetActionBannerEntity", _widgetActionBannerEntity);
			toReturn.Add("WidgetHeroEntity", _widgetHeroEntity);
			toReturn.Add("AttachmentEntity", _attachmentEntity);
			toReturn.Add("ActionCategoryEntity", _actionCategoryEntity);
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("ActionEntertainmentEntity", _actionEntertainmentEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("ActionEntertainmentcategoryEntity", _actionEntertainmentcategoryEntity);
			toReturn.Add("GenericcategoryEntity", _genericcategoryEntity);
			toReturn.Add("GenericproductEntity", _genericproductEntity);
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("PageEntity_", _pageEntity_);
			toReturn.Add("PageElementEntity", _pageElementEntity);
			toReturn.Add("PageTemplateEntity", _pageTemplateEntity);
			toReturn.Add("PageTemplateElementEntity", _pageTemplateElementEntity);
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			toReturn.Add("ActionProductEntity", _actionProductEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductgroupEntity", _productgroupEntity);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("RoomControlSectionItemEntity", _roomControlSectionItemEntity);
			toReturn.Add("RoutestephandlerEntity", _routestephandlerEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("SiteEntity_", _siteEntity_);
			toReturn.Add("StationEntity", _stationEntity);
			toReturn.Add("SurveyEntity", _surveyEntity);
			toReturn.Add("SurveyPageEntity", _surveyPageEntity);
			toReturn.Add("UIFooterItemEntity", _uIFooterItemEntity);
			toReturn.Add("UIThemeEntity", _uIThemeEntity);
			toReturn.Add("UIWidgetEntity", _uIWidgetEntity);
			toReturn.Add("AnnouncementCollection", _announcementCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("MediaCultureCollection", _mediaCultureCollection);
			toReturn.Add("MediaLanguageCollection", _mediaLanguageCollection);
			toReturn.Add("MediaRatioTypeMediaCollection", _mediaRatioTypeMediaCollection);
			toReturn.Add("MediaRelationshipCollection", _mediaRelationshipCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageTemplateCollection", _messageTemplateCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			toReturn.Add("UIScheduleItemCollection", _uIScheduleItemCollection);
			toReturn.Add("CategoryCollectionViaAnnouncement", _categoryCollectionViaAnnouncement);
			toReturn.Add("CategoryCollectionViaAnnouncement_", _categoryCollectionViaAnnouncement_);
			toReturn.Add("CategoryCollectionViaMessage", _categoryCollectionViaMessage);
			toReturn.Add("CategoryCollectionViaMessageTemplate", _categoryCollectionViaMessageTemplate);
			toReturn.Add("ClientCollectionViaMessage", _clientCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaAnnouncement", _companyCollectionViaAnnouncement);
			toReturn.Add("CompanyCollectionViaMessage", _companyCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaMessageTemplate", _companyCollectionViaMessageTemplate);
			toReturn.Add("CustomerCollectionViaMessage", _customerCollectionViaMessage);
			toReturn.Add("DeliverypointCollectionViaMessage", _deliverypointCollectionViaMessage);
			toReturn.Add("DeliverypointgroupCollectionViaAnnouncement", _deliverypointgroupCollectionViaAnnouncement);
			toReturn.Add("EntertainmentCollectionViaAnnouncement", _entertainmentCollectionViaAnnouncement);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("EntertainmentCollectionViaMessageTemplate", _entertainmentCollectionViaMessageTemplate);
			toReturn.Add("EntertainmentcategoryCollectionViaAnnouncement", _entertainmentcategoryCollectionViaAnnouncement);
			toReturn.Add("EntertainmentcategoryCollectionViaAnnouncement_", _entertainmentcategoryCollectionViaAnnouncement_);
			toReturn.Add("OrderCollectionViaMessage", _orderCollectionViaMessage);
			toReturn.Add("ProductCollectionViaAnnouncement", _productCollectionViaAnnouncement);
			toReturn.Add("ProductCollectionViaMessageTemplate", _productCollectionViaMessageTemplate);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="validator">The validator object for this MediaEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 mediaId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(mediaId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_announcementCollection = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection.SetContainingEntityInfo(this, "MediaEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "MediaEntity");

			_mediaCultureCollection = new Obymobi.Data.CollectionClasses.MediaCultureCollection();
			_mediaCultureCollection.SetContainingEntityInfo(this, "MediaEntity");

			_mediaLanguageCollection = new Obymobi.Data.CollectionClasses.MediaLanguageCollection();
			_mediaLanguageCollection.SetContainingEntityInfo(this, "MediaEntity");

			_mediaRatioTypeMediaCollection = new Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection();
			_mediaRatioTypeMediaCollection.SetContainingEntityInfo(this, "MediaEntity");

			_mediaRelationshipCollection = new Obymobi.Data.CollectionClasses.MediaRelationshipCollection();
			_mediaRelationshipCollection.SetContainingEntityInfo(this, "MediaEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "MediaEntity");

			_messageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCollection();
			_messageTemplateCollection.SetContainingEntityInfo(this, "MediaEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "MediaEntity");

			_uIScheduleItemCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemCollection();
			_uIScheduleItemCollection.SetContainingEntityInfo(this, "MediaEntity");
			_categoryCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMessage = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientCollectionViaMessage = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMessage = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_customerCollectionViaMessage = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaMessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_orderCollectionViaMessage = new Obymobi.Data.CollectionClasses.OrderCollection();
			_productCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.ProductCollection();
			_advertisementEntityReturnsNewIfNotFound = true;
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationoptionEntityReturnsNewIfNotFound = true;
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			_carouselItemEntityReturnsNewIfNotFound = true;
			_landingPageEntityReturnsNewIfNotFound = true;
			_widgetActionBannerEntityReturnsNewIfNotFound = true;
			_widgetHeroEntityReturnsNewIfNotFound = true;
			_attachmentEntityReturnsNewIfNotFound = true;
			_actionCategoryEntityReturnsNewIfNotFound = true;
			_categoryEntityReturnsNewIfNotFound = true;
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_actionEntertainmentEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_actionEntertainmentcategoryEntityReturnsNewIfNotFound = true;
			_genericcategoryEntityReturnsNewIfNotFound = true;
			_genericproductEntityReturnsNewIfNotFound = true;
			_mediaEntityReturnsNewIfNotFound = true;
			_pageEntityReturnsNewIfNotFound = true;
			_pageEntity_ReturnsNewIfNotFound = true;
			_pageElementEntityReturnsNewIfNotFound = true;
			_pageTemplateEntityReturnsNewIfNotFound = true;
			_pageTemplateElementEntityReturnsNewIfNotFound = true;
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			_actionProductEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_productgroupEntityReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			_roomControlSectionItemEntityReturnsNewIfNotFound = true;
			_routestephandlerEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			_siteEntity_ReturnsNewIfNotFound = true;
			_stationEntityReturnsNewIfNotFound = true;
			_surveyEntityReturnsNewIfNotFound = true;
			_surveyPageEntityReturnsNewIfNotFound = true;
			_uIFooterItemEntityReturnsNewIfNotFound = true;
			_uIThemeEntityReturnsNewIfNotFound = true;
			_uIWidgetEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultItem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilePathRelativeToMediaPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MimeType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SizeKb", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericproductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromSupplier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoutestephandlerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageElementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionUrl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionEntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionEntertainmentcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JpgQuality", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SizeMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ZoomLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaFileMd5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Extension", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AgnosticMediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttachmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionSiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTemplateElementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIThemeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIFooterItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelatedCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastDistributedVersionTicksAmazon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastDistributedVersionTicks", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SizeWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SizeHeight", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LandingPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CarouselItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _advertisementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.AdvertisementId } );		
			_advertisementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementEntity(IEntityCore relatedEntity)
		{
			if(_advertisementEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementEntity(true, true);
				_advertisementEntity = (AdvertisementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, ref _alreadyFetchedAdvertisementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.AlterationoptionId } );		
			_alterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_alterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncAlterationoptionEntity(true, true);
				_alterationoptionEntity = (AlterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, ref _alreadyFetchedAlterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _carouselItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCarouselItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _carouselItemEntity, new PropertyChangedEventHandler( OnCarouselItemEntityPropertyChanged ), "CarouselItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CarouselItemEntityUsingCarouselItemIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.CarouselItemId } );		
			_carouselItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _carouselItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCarouselItemEntity(IEntityCore relatedEntity)
		{
			if(_carouselItemEntity!=relatedEntity)
			{		
				DesetupSyncCarouselItemEntity(true, true);
				_carouselItemEntity = (CarouselItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _carouselItemEntity, new PropertyChangedEventHandler( OnCarouselItemEntityPropertyChanged ), "CarouselItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CarouselItemEntityUsingCarouselItemIdStatic, true, ref _alreadyFetchedCarouselItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCarouselItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _landingPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLandingPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.LandingPageEntityUsingLandingPageIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.LandingPageId } );		
			_landingPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _landingPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLandingPageEntity(IEntityCore relatedEntity)
		{
			if(_landingPageEntity!=relatedEntity)
			{		
				DesetupSyncLandingPageEntity(true, true);
				_landingPageEntity = (LandingPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.LandingPageEntityUsingLandingPageIdStatic, true, ref _alreadyFetchedLandingPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLandingPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetActionBannerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetActionBannerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetActionBannerEntity, new PropertyChangedEventHandler( OnWidgetActionBannerEntityPropertyChanged ), "WidgetActionBannerEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.WidgetActionBannerEntityUsingWidgetIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.WidgetId } );		
			_widgetActionBannerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetActionBannerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetActionBannerEntity(IEntityCore relatedEntity)
		{
			if(_widgetActionBannerEntity!=relatedEntity)
			{		
				DesetupSyncWidgetActionBannerEntity(true, true);
				_widgetActionBannerEntity = (WidgetActionBannerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetActionBannerEntity, new PropertyChangedEventHandler( OnWidgetActionBannerEntityPropertyChanged ), "WidgetActionBannerEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.WidgetActionBannerEntityUsingWidgetIdStatic, true, ref _alreadyFetchedWidgetActionBannerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetActionBannerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetHeroEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetHeroEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetHeroEntity, new PropertyChangedEventHandler( OnWidgetHeroEntityPropertyChanged ), "WidgetHeroEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.WidgetHeroEntityUsingWidgetIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.WidgetId } );		
			_widgetHeroEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetHeroEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetHeroEntity(IEntityCore relatedEntity)
		{
			if(_widgetHeroEntity!=relatedEntity)
			{		
				DesetupSyncWidgetHeroEntity(true, true);
				_widgetHeroEntity = (WidgetHeroEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetHeroEntity, new PropertyChangedEventHandler( OnWidgetHeroEntityPropertyChanged ), "WidgetHeroEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.WidgetHeroEntityUsingWidgetIdStatic, true, ref _alreadyFetchedWidgetHeroEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetHeroEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attachmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttachmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attachmentEntity, new PropertyChangedEventHandler( OnAttachmentEntityPropertyChanged ), "AttachmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AttachmentEntityUsingAttachmentIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.AttachmentId } );		
			_attachmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _attachmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttachmentEntity(IEntityCore relatedEntity)
		{
			if(_attachmentEntity!=relatedEntity)
			{		
				DesetupSyncAttachmentEntity(true, true);
				_attachmentEntity = (AttachmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attachmentEntity, new PropertyChangedEventHandler( OnAttachmentEntityPropertyChanged ), "AttachmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.AttachmentEntityUsingAttachmentIdStatic, true, ref _alreadyFetchedAttachmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttachmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _actionCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionCategoryEntity, new PropertyChangedEventHandler( OnActionCategoryEntityPropertyChanged ), "ActionCategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CategoryEntityUsingActionCategoryIdStatic, true, signalRelatedEntity, "ActionMediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ActionCategoryId } );		
			_actionCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionCategoryEntity(IEntityCore relatedEntity)
		{
			if(_actionCategoryEntity!=relatedEntity)
			{		
				DesetupSyncActionCategoryEntity(true, true);
				_actionCategoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionCategoryEntity, new PropertyChangedEventHandler( OnActionCategoryEntityPropertyChanged ), "ActionCategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CategoryEntityUsingActionCategoryIdStatic, true, ref _alreadyFetchedActionCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _actionEntertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionEntertainmentEntity, new PropertyChangedEventHandler( OnActionEntertainmentEntityPropertyChanged ), "ActionEntertainmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentEntityUsingActionEntertainmentIdStatic, true, signalRelatedEntity, "ActionMediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ActionEntertainmentId } );		
			_actionEntertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionEntertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_actionEntertainmentEntity!=relatedEntity)
			{		
				DesetupSyncActionEntertainmentEntity(true, true);
				_actionEntertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionEntertainmentEntity, new PropertyChangedEventHandler( OnActionEntertainmentEntityPropertyChanged ), "ActionEntertainmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentEntityUsingActionEntertainmentIdStatic, true, ref _alreadyFetchedActionEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _actionEntertainmentcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionEntertainmentcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionEntertainmentcategoryEntity, new PropertyChangedEventHandler( OnActionEntertainmentcategoryEntityPropertyChanged ), "ActionEntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryIdStatic, true, signalRelatedEntity, "ActionMediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ActionEntertainmentcategoryId } );		
			_actionEntertainmentcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionEntertainmentcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionEntertainmentcategoryEntity(IEntityCore relatedEntity)
		{
			if(_actionEntertainmentcategoryEntity!=relatedEntity)
			{		
				DesetupSyncActionEntertainmentcategoryEntity(true, true);
				_actionEntertainmentcategoryEntity = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionEntertainmentcategoryEntity, new PropertyChangedEventHandler( OnActionEntertainmentcategoryEntityPropertyChanged ), "ActionEntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryIdStatic, true, ref _alreadyFetchedActionEntertainmentcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionEntertainmentcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.GenericcategoryId } );		
			_genericcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericcategoryEntity(IEntityCore relatedEntity)
		{
			if(_genericcategoryEntity!=relatedEntity)
			{		
				DesetupSyncGenericcategoryEntity(true, true);
				_genericcategoryEntity = (GenericcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, ref _alreadyFetchedGenericcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericproductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericproductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericproductEntity, new PropertyChangedEventHandler( OnGenericproductEntityPropertyChanged ), "GenericproductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.GenericproductEntityUsingGenericproductIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.GenericproductId } );		
			_genericproductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericproductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericproductEntity(IEntityCore relatedEntity)
		{
			if(_genericproductEntity!=relatedEntity)
			{		
				DesetupSyncGenericproductEntity(true, true);
				_genericproductEntity = (GenericproductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericproductEntity, new PropertyChangedEventHandler( OnGenericproductEntityPropertyChanged ), "GenericproductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.GenericproductEntityUsingGenericproductIdStatic, true, ref _alreadyFetchedGenericproductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericproductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.MediaEntityUsingMediaIdAgnosticMediaIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.AgnosticMediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.MediaEntityUsingMediaIdAgnosticMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageEntityUsingPageIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.PageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageEntityUsingPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity_, new PropertyChangedEventHandler( OnPageEntity_PropertyChanged ), "PageEntity_", Obymobi.Data.RelationClasses.StaticMediaRelations.PageEntityUsingActionPageIdStatic, true, signalRelatedEntity, "MediaCollection_", resetFKFields, new int[] { (int)MediaFieldIndex.ActionPageId } );		
			_pageEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity_(IEntityCore relatedEntity)
		{
			if(_pageEntity_!=relatedEntity)
			{		
				DesetupSyncPageEntity_(true, true);
				_pageEntity_ = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity_, new PropertyChangedEventHandler( OnPageEntity_PropertyChanged ), "PageEntity_", Obymobi.Data.RelationClasses.StaticMediaRelations.PageEntityUsingActionPageIdStatic, true, ref _alreadyFetchedPageEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageElementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageElementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageElementEntity, new PropertyChangedEventHandler( OnPageElementEntityPropertyChanged ), "PageElementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageElementEntityUsingPageElementIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.PageElementId } );		
			_pageElementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageElementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageElementEntity(IEntityCore relatedEntity)
		{
			if(_pageElementEntity!=relatedEntity)
			{		
				DesetupSyncPageElementEntity(true, true);
				_pageElementEntity = (PageElementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageElementEntity, new PropertyChangedEventHandler( OnPageElementEntityPropertyChanged ), "PageElementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageElementEntityUsingPageElementIdStatic, true, ref _alreadyFetchedPageElementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageElementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.PageTemplateId } );		
			_pageTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageTemplateEntity(IEntityCore relatedEntity)
		{
			if(_pageTemplateEntity!=relatedEntity)
			{		
				DesetupSyncPageTemplateEntity(true, true);
				_pageTemplateEntity = (PageTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, ref _alreadyFetchedPageTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageTemplateElementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageTemplateElementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageTemplateElementEntity, new PropertyChangedEventHandler( OnPageTemplateElementEntityPropertyChanged ), "PageTemplateElementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageTemplateElementEntityUsingPageTemplateElementIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.PageTemplateElementId } );		
			_pageTemplateElementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageTemplateElementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageTemplateElementEntity(IEntityCore relatedEntity)
		{
			if(_pageTemplateElementEntity!=relatedEntity)
			{		
				DesetupSyncPageTemplateElementEntity(true, true);
				_pageTemplateElementEntity = (PageTemplateElementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageTemplateElementEntity, new PropertyChangedEventHandler( OnPageTemplateElementEntityPropertyChanged ), "PageTemplateElementEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PageTemplateElementEntityUsingPageTemplateElementIdStatic, true, ref _alreadyFetchedPageTemplateElementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageTemplateElementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.PointOfInterestId } );		
			_pointOfInterestEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{		
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _actionProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionProductEntity, new PropertyChangedEventHandler( OnActionProductEntityPropertyChanged ), "ActionProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductEntityUsingActionProductIdStatic, true, signalRelatedEntity, "ActionMediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ActionProductId } );		
			_actionProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionProductEntity(IEntityCore relatedEntity)
		{
			if(_actionProductEntity!=relatedEntity)
			{		
				DesetupSyncActionProductEntity(true, true);
				_actionProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionProductEntity, new PropertyChangedEventHandler( OnActionProductEntityPropertyChanged ), "ActionProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductEntityUsingActionProductIdStatic, true, ref _alreadyFetchedActionProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductgroupEntityUsingProductgroupIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.ProductgroupId } );		
			_productgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductgroupEntity(IEntityCore relatedEntity)
		{
			if(_productgroupEntity!=relatedEntity)
			{		
				DesetupSyncProductgroupEntity(true, true);
				_productgroupEntity = (ProductgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.ProductgroupEntityUsingProductgroupIdStatic, true, ref _alreadyFetchedProductgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.RoomControlSectionItemId } );		
			_roomControlSectionItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionItemEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionItemEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionItemEntity(true, true);
				_roomControlSectionItemEntity = (RoomControlSectionItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, ref _alreadyFetchedRoomControlSectionItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routestephandlerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoutestephandlerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routestephandlerEntity, new PropertyChangedEventHandler( OnRoutestephandlerEntityPropertyChanged ), "RoutestephandlerEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoutestephandlerEntityUsingRoutestephandlerIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.RoutestephandlerId } );		
			_routestephandlerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routestephandlerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoutestephandlerEntity(IEntityCore relatedEntity)
		{
			if(_routestephandlerEntity!=relatedEntity)
			{		
				DesetupSyncRoutestephandlerEntity(true, true);
				_routestephandlerEntity = (RoutestephandlerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routestephandlerEntity, new PropertyChangedEventHandler( OnRoutestephandlerEntityPropertyChanged ), "RoutestephandlerEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.RoutestephandlerEntityUsingRoutestephandlerIdStatic, true, ref _alreadyFetchedRoutestephandlerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoutestephandlerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity_, new PropertyChangedEventHandler( OnSiteEntity_PropertyChanged ), "SiteEntity_", Obymobi.Data.RelationClasses.StaticMediaRelations.SiteEntityUsingActionSiteIdStatic, true, signalRelatedEntity, "MediaCollection_", resetFKFields, new int[] { (int)MediaFieldIndex.ActionSiteId } );		
			_siteEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity_(IEntityCore relatedEntity)
		{
			if(_siteEntity_!=relatedEntity)
			{		
				DesetupSyncSiteEntity_(true, true);
				_siteEntity_ = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity_, new PropertyChangedEventHandler( OnSiteEntity_PropertyChanged ), "SiteEntity_", Obymobi.Data.RelationClasses.StaticMediaRelations.SiteEntityUsingActionSiteIdStatic, true, ref _alreadyFetchedSiteEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _stationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncStationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _stationEntity, new PropertyChangedEventHandler( OnStationEntityPropertyChanged ), "StationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.StationEntityUsingStationIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.StationId } );		
			_stationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _stationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncStationEntity(IEntityCore relatedEntity)
		{
			if(_stationEntity!=relatedEntity)
			{		
				DesetupSyncStationEntity(true, true);
				_stationEntity = (StationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _stationEntity, new PropertyChangedEventHandler( OnStationEntityPropertyChanged ), "StationEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.StationEntityUsingStationIdStatic, true, ref _alreadyFetchedStationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _surveyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyEntity, new PropertyChangedEventHandler( OnSurveyEntityPropertyChanged ), "SurveyEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SurveyEntityUsingSurveyIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.SurveyId } );		
			_surveyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyEntity(IEntityCore relatedEntity)
		{
			if(_surveyEntity!=relatedEntity)
			{		
				DesetupSyncSurveyEntity(true, true);
				_surveyEntity = (SurveyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyEntity, new PropertyChangedEventHandler( OnSurveyEntityPropertyChanged ), "SurveyEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SurveyEntityUsingSurveyIdStatic, true, ref _alreadyFetchedSurveyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _surveyPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyPageEntity, new PropertyChangedEventHandler( OnSurveyPageEntityPropertyChanged ), "SurveyPageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SurveyPageEntityUsingSurveyPageIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.SurveyPageId } );		
			_surveyPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyPageEntity(IEntityCore relatedEntity)
		{
			if(_surveyPageEntity!=relatedEntity)
			{		
				DesetupSyncSurveyPageEntity(true, true);
				_surveyPageEntity = (SurveyPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyPageEntity, new PropertyChangedEventHandler( OnSurveyPageEntityPropertyChanged ), "SurveyPageEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.SurveyPageEntityUsingSurveyPageIdStatic, true, ref _alreadyFetchedSurveyPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIFooterItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIFooterItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIFooterItemEntity, new PropertyChangedEventHandler( OnUIFooterItemEntityPropertyChanged ), "UIFooterItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIFooterItemEntityUsingUIFooterItemIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.UIFooterItemId } );		
			_uIFooterItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIFooterItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIFooterItemEntity(IEntityCore relatedEntity)
		{
			if(_uIFooterItemEntity!=relatedEntity)
			{		
				DesetupSyncUIFooterItemEntity(true, true);
				_uIFooterItemEntity = (UIFooterItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIFooterItemEntity, new PropertyChangedEventHandler( OnUIFooterItemEntityPropertyChanged ), "UIFooterItemEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIFooterItemEntityUsingUIFooterItemIdStatic, true, ref _alreadyFetchedUIFooterItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIFooterItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIThemeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIThemeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIThemeEntityUsingUIThemeIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.UIThemeId } );		
			_uIThemeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIThemeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIThemeEntity(IEntityCore relatedEntity)
		{
			if(_uIThemeEntity!=relatedEntity)
			{		
				DesetupSyncUIThemeEntity(true, true);
				_uIThemeEntity = (UIThemeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIThemeEntityUsingUIThemeIdStatic, true, ref _alreadyFetchedUIThemeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIThemeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, signalRelatedEntity, "MediaCollection", resetFKFields, new int[] { (int)MediaFieldIndex.UIWidgetId } );		
			_uIWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIWidgetEntity(IEntityCore relatedEntity)
		{
			if(_uIWidgetEntity!=relatedEntity)
			{		
				DesetupSyncUIWidgetEntity(true, true);
				_uIWidgetEntity = (UIWidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticMediaRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, ref _alreadyFetchedUIWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 mediaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MediaFieldIndex.MediaId].ForcedCurrentValueWrite(mediaId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMediaDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MediaEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MediaRelations Relations
		{
			get	{ return new MediaRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaCulture' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCultureCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCultureCollection(), (IEntityRelation)GetRelationsForField("MediaCultureCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaCultureEntity, 0, null, null, null, "MediaCultureCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaLanguageCollection(), (IEntityRelation)GetRelationsForField("MediaLanguageCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaLanguageEntity, 0, null, null, null, "MediaLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaRatioTypeMedia' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaRatioTypeMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection(), (IEntityRelation)GetRelationsForField("MediaRatioTypeMediaCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, 0, null, null, null, "MediaRatioTypeMediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaRelationship' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaRelationshipCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaRelationshipCollection(), (IEntityRelation)GetRelationsForField("MediaRelationshipCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaRelationshipEntity, 0, null, null, null, "MediaRelationshipCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MessageTemplateEntity, 0, null, null, null, "MessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemCollection")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.UIScheduleItemEntity, 0, null, null, null, "UIScheduleItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAnnouncement"), "CategoryCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAnnouncement_"), "CategoryCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessage"), "CategoryCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessageTemplate"), "CategoryCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaMessage"), "ClientCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAnnouncement"), "CompanyCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMessage"), "CompanyCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMessageTemplate"), "CompanyCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaMessage"), "CustomerCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessage"), "DeliverypointCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"), "DeliverypointgroupCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement"), "EntertainmentCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessageTemplate"), "EntertainmentCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement"), "EntertainmentcategoryCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement_"), "EntertainmentcategoryCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaMessage"), "OrderCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement"), "ProductCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingMediaId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMessageTemplate"), "ProductCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CarouselItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCarouselItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CarouselItemCollection(), (IEntityRelation)GetRelationsForField("CarouselItemEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CarouselItemEntity, 0, null, null, null, "CarouselItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetActionBanner'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetActionBannerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetActionBannerCollection(), (IEntityRelation)GetRelationsForField("WidgetActionBannerEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.WidgetActionBannerEntity, 0, null, null, null, "WidgetActionBannerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetHero'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetHeroEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetHeroCollection(), (IEntityRelation)GetRelationsForField("WidgetHeroEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.WidgetHeroEntity, 0, null, null, null, "WidgetHeroEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attachment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentCollection(), (IEntityRelation)GetRelationsForField("AttachmentEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.AttachmentEntity, 0, null, null, null, "AttachmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("ActionCategoryEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "ActionCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("ActionEntertainmentEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "ActionEntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionEntertainmentcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("ActionEntertainmentcategoryEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "ActionEntertainmentcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity_")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageElement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageElementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageElementCollection(), (IEntityRelation)GetRelationsForField("PageElementEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PageElementEntity, 0, null, null, null, "PageElementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplateElement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateElementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateElementCollection(), (IEntityRelation)GetRelationsForField("PageTemplateElementEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PageTemplateElementEntity, 0, null, null, null, "PageTemplateElementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ActionProductEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ActionProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Productgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductgroupCollection(), (IEntityRelation)GetRelationsForField("ProductgroupEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.ProductgroupEntity, 0, null, null, null, "ProductgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, 0, null, null, null, "RoomControlSectionItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity_")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Station'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.StationCollection(), (IEntityRelation)GetRelationsForField("StationEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.StationEntity, 0, null, null, null, "StationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), (IEntityRelation)GetRelationsForField("SurveyEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, null, "SurveyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), (IEntityRelation)GetRelationsForField("SurveyPageEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, null, "SurveyPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIFooterItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIFooterItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIFooterItemCollection(), (IEntityRelation)GetRelationsForField("UIFooterItemEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.UIFooterItemEntity, 0, null, null, null, "UIFooterItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITheme'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeCollection(), (IEntityRelation)GetRelationsForField("UIThemeEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.UIThemeEntity, 0, null, null, null, "UIThemeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetEntity")[0], (int)Obymobi.Data.EntityType.MediaEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MediaId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MediaId
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.MediaId, true); }
			set	{ SetValue((int)MediaFieldIndex.MediaId, value, true); }
		}

		/// <summary> The MediaType property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MediaType
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.MediaType, true); }
			set	{ SetValue((int)MediaFieldIndex.MediaType, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SortOrder
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SortOrder, false); }
			set	{ SetValue((int)MediaFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The DefaultItem property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."DefaultItem"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DefaultItem
		{
			get { return (System.Boolean)GetValue((int)MediaFieldIndex.DefaultItem, true); }
			set	{ SetValue((int)MediaFieldIndex.DefaultItem, value, true); }
		}

		/// <summary> The Name property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Name, true); }
			set	{ SetValue((int)MediaFieldIndex.Name, value, true); }
		}

		/// <summary> The FilePathRelativeToMediaPath property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."FilePathRelativeToMediaPath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FilePathRelativeToMediaPath
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.FilePathRelativeToMediaPath, true); }
			set	{ SetValue((int)MediaFieldIndex.FilePathRelativeToMediaPath, value, true); }
		}

		/// <summary> The Description property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Description, true); }
			set	{ SetValue((int)MediaFieldIndex.Description, value, true); }
		}

		/// <summary> The MimeType property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MimeType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MimeType
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.MimeType, true); }
			set	{ SetValue((int)MediaFieldIndex.MimeType, value, true); }
		}

		/// <summary> The SizeKb property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeKb"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeKb
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeKb, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeKb, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CompanyId, false); }
			set	{ SetValue((int)MediaFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ProductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ProductId, false); }
			set	{ SetValue((int)MediaFieldIndex.ProductId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The AdvertisementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AdvertisementId, false); }
			set	{ SetValue((int)MediaFieldIndex.AdvertisementId, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The AlterationoptionId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)MediaFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The GenericproductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."GenericproductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.GenericproductId, false); }
			set	{ SetValue((int)MediaFieldIndex.GenericproductId, value, true); }
		}

		/// <summary> The GenericcategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."GenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.GenericcategoryId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)MediaFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The SurveyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SurveyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SurveyId, false); }
			set	{ SetValue((int)MediaFieldIndex.SurveyId, value, true); }
		}

		/// <summary> The FromSupplier property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."FromSupplier"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FromSupplier
		{
			get { return (System.Boolean)GetValue((int)MediaFieldIndex.FromSupplier, true); }
			set	{ SetValue((int)MediaFieldIndex.FromSupplier, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MediaFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MediaFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The AlterationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AlterationId, false); }
			set	{ SetValue((int)MediaFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The SurveyPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SurveyPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SurveyPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.SurveyPageId, value, true); }
		}

		/// <summary> The RoutestephandlerId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoutestephandlerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoutestephandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoutestephandlerId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoutestephandlerId, value, true); }
		}

		/// <summary> The PageElementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageElementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageElementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageElementId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageElementId, value, true); }
		}

		/// <summary> The PageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageId, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)MediaFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The SiteId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SiteId, false); }
			set	{ SetValue((int)MediaFieldIndex.SiteId, value, true); }
		}

		/// <summary> The ActionUrl property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionUrl"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionUrl
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.ActionUrl, true); }
			set	{ SetValue((int)MediaFieldIndex.ActionUrl, value, true); }
		}

		/// <summary> The ActionEntertainmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionEntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionEntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionEntertainmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionEntertainmentId, value, true); }
		}

		/// <summary> The ActionProductId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionProductId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionProductId, value, true); }
		}

		/// <summary> The ActionCategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionCategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionCategoryId, value, true); }
		}

		/// <summary> The ActionEntertainmentcategoryId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionEntertainmentcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionEntertainmentcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionEntertainmentcategoryId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionEntertainmentcategoryId, value, true); }
		}

		/// <summary> The JpgQuality property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."JpgQuality"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> JpgQuality
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.JpgQuality, false); }
			set	{ SetValue((int)MediaFieldIndex.JpgQuality, value, true); }
		}

		/// <summary> The SizeMode property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MediaSizeMode SizeMode
		{
			get { return (Obymobi.Enums.MediaSizeMode)GetValue((int)MediaFieldIndex.SizeMode, true); }
			set	{ SetValue((int)MediaFieldIndex.SizeMode, value, true); }
		}

		/// <summary> The ZoomLevel property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ZoomLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ZoomLevel
		{
			get { return (System.Int32)GetValue((int)MediaFieldIndex.ZoomLevel, true); }
			set	{ SetValue((int)MediaFieldIndex.ZoomLevel, value, true); }
		}

		/// <summary> The MediaFileMd5 property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."MediaFileMd5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MediaFileMd5
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.MediaFileMd5, true); }
			set	{ SetValue((int)MediaFieldIndex.MediaFileMd5, value, true); }
		}

		/// <summary> The Extension property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."Extension"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Extension
		{
			get { return (System.String)GetValue((int)MediaFieldIndex.Extension, true); }
			set	{ SetValue((int)MediaFieldIndex.Extension, value, true); }
		}

		/// <summary> The AgnosticMediaId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AgnosticMediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AgnosticMediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AgnosticMediaId, false); }
			set	{ SetValue((int)MediaFieldIndex.AgnosticMediaId, value, true); }
		}

		/// <summary> The AttachmentId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."AttachmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AttachmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.AttachmentId, false); }
			set	{ SetValue((int)MediaFieldIndex.AttachmentId, value, true); }
		}

		/// <summary> The ActionPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionPageId, value, true); }
		}

		/// <summary> The ActionSiteId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ActionSiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionSiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ActionSiteId, false); }
			set	{ SetValue((int)MediaFieldIndex.ActionSiteId, value, true); }
		}

		/// <summary> The PageTemplateElementId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageTemplateElementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateElementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageTemplateElementId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageTemplateElementId, value, true); }
		}

		/// <summary> The PageTemplateId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."PageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.PageTemplateId, false); }
			set	{ SetValue((int)MediaFieldIndex.PageTemplateId, value, true); }
		}

		/// <summary> The UIWidgetId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIWidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIWidgetId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIWidgetId, value, true); }
		}

		/// <summary> The UIThemeId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIThemeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIThemeId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The RoomControlSectionItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RoomControlSectionItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RoomControlSectionItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.RoomControlSectionItemId, value, true); }
		}

		/// <summary> The StationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."StationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.StationId, false); }
			set	{ SetValue((int)MediaFieldIndex.StationId, value, true); }
		}

		/// <summary> The UIFooterItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UIFooterItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIFooterItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.UIFooterItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.UIFooterItemId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MediaFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MediaFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The RelatedCompanyId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."RelatedCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RelatedCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.RelatedCompanyId, false); }
			set	{ SetValue((int)MediaFieldIndex.RelatedCompanyId, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)MediaFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The LastDistributedVersionTicksAmazon property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LastDistributedVersionTicksAmazon"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAmazon
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaFieldIndex.LastDistributedVersionTicksAmazon, false); }
			set	{ SetValue((int)MediaFieldIndex.LastDistributedVersionTicksAmazon, value, true); }
		}

		/// <summary> The LastDistributedVersionTicks property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LastDistributedVersionTicks"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicks
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaFieldIndex.LastDistributedVersionTicks, false); }
			set	{ SetValue((int)MediaFieldIndex.LastDistributedVersionTicks, value, true); }
		}

		/// <summary> The SizeWidth property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeWidth"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeWidth
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeWidth, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeWidth, value, true); }
		}

		/// <summary> The SizeHeight property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."SizeHeight"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SizeHeight
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.SizeHeight, false); }
			set	{ SetValue((int)MediaFieldIndex.SizeHeight, value, true); }
		}

		/// <summary> The ProductgroupId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ProductgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ProductgroupId, false); }
			set	{ SetValue((int)MediaFieldIndex.ProductgroupId, value, true); }
		}

		/// <summary> The LandingPageId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."LandingPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)MediaFieldIndex.LandingPageId, value, true); }
		}

		/// <summary> The CarouselItemId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."CarouselItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CarouselItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.CarouselItemId, false); }
			set	{ SetValue((int)MediaFieldIndex.CarouselItemId, value, true); }
		}

		/// <summary> The WidgetId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.WidgetId, false); }
			set	{ SetValue((int)MediaFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity Media<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Media"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ApplicationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaFieldIndex.ApplicationConfigurationId, false); }
			set	{ SetValue((int)MediaFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection
		{
			get	{ return GetMultiAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection. When set to true, AnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection
		{
			get	{ return _alwaysFetchAnnouncementCollection; }
			set	{ _alwaysFetchAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection already has been fetched. Setting this property to false when AnnouncementCollection has been fetched
		/// will clear the AnnouncementCollection collection well. Setting this property to true while AnnouncementCollection hasn't been fetched disables lazy loading for AnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection
		{
			get { return _alreadyFetchedAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection && !value && (_announcementCollection != null))
				{
					_announcementCollection.Clear();
				}
				_alreadyFetchedAnnouncementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaCultureEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCultureCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCultureCollection MediaCultureCollection
		{
			get	{ return GetMultiMediaCultureCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCultureCollection. When set to true, MediaCultureCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCultureCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCultureCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCultureCollection
		{
			get	{ return _alwaysFetchMediaCultureCollection; }
			set	{ _alwaysFetchMediaCultureCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCultureCollection already has been fetched. Setting this property to false when MediaCultureCollection has been fetched
		/// will clear the MediaCultureCollection collection well. Setting this property to true while MediaCultureCollection hasn't been fetched disables lazy loading for MediaCultureCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCultureCollection
		{
			get { return _alreadyFetchedMediaCultureCollection;}
			set 
			{
				if(_alreadyFetchedMediaCultureCollection && !value && (_mediaCultureCollection != null))
				{
					_mediaCultureCollection.Clear();
				}
				_alreadyFetchedMediaCultureCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaLanguageCollection MediaLanguageCollection
		{
			get	{ return GetMultiMediaLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaLanguageCollection. When set to true, MediaLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaLanguageCollection
		{
			get	{ return _alwaysFetchMediaLanguageCollection; }
			set	{ _alwaysFetchMediaLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaLanguageCollection already has been fetched. Setting this property to false when MediaLanguageCollection has been fetched
		/// will clear the MediaLanguageCollection collection well. Setting this property to true while MediaLanguageCollection hasn't been fetched disables lazy loading for MediaLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaLanguageCollection
		{
			get { return _alreadyFetchedMediaLanguageCollection;}
			set 
			{
				if(_alreadyFetchedMediaLanguageCollection && !value && (_mediaLanguageCollection != null))
				{
					_mediaLanguageCollection.Clear();
				}
				_alreadyFetchedMediaLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaRatioTypeMediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaRatioTypeMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection MediaRatioTypeMediaCollection
		{
			get	{ return GetMultiMediaRatioTypeMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaRatioTypeMediaCollection. When set to true, MediaRatioTypeMediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaRatioTypeMediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaRatioTypeMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaRatioTypeMediaCollection
		{
			get	{ return _alwaysFetchMediaRatioTypeMediaCollection; }
			set	{ _alwaysFetchMediaRatioTypeMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaRatioTypeMediaCollection already has been fetched. Setting this property to false when MediaRatioTypeMediaCollection has been fetched
		/// will clear the MediaRatioTypeMediaCollection collection well. Setting this property to true while MediaRatioTypeMediaCollection hasn't been fetched disables lazy loading for MediaRatioTypeMediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaRatioTypeMediaCollection
		{
			get { return _alreadyFetchedMediaRatioTypeMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaRatioTypeMediaCollection && !value && (_mediaRatioTypeMediaCollection != null))
				{
					_mediaRatioTypeMediaCollection.Clear();
				}
				_alreadyFetchedMediaRatioTypeMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaRelationshipEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaRelationshipCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaRelationshipCollection MediaRelationshipCollection
		{
			get	{ return GetMultiMediaRelationshipCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaRelationshipCollection. When set to true, MediaRelationshipCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaRelationshipCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaRelationshipCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaRelationshipCollection
		{
			get	{ return _alwaysFetchMediaRelationshipCollection; }
			set	{ _alwaysFetchMediaRelationshipCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaRelationshipCollection already has been fetched. Setting this property to false when MediaRelationshipCollection has been fetched
		/// will clear the MediaRelationshipCollection collection well. Setting this property to true while MediaRelationshipCollection hasn't been fetched disables lazy loading for MediaRelationshipCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaRelationshipCollection
		{
			get { return _alreadyFetchedMediaRelationshipCollection;}
			set 
			{
				if(_alreadyFetchedMediaRelationshipCollection && !value && (_mediaRelationshipCollection != null))
				{
					_mediaRelationshipCollection.Clear();
				}
				_alreadyFetchedMediaRelationshipCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection MessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCollection. When set to true, MessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCollection collection well. Setting this property to true while MessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCollection && !value && (_messageTemplateCollection != null))
				{
					_messageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection UIScheduleItemCollection
		{
			get	{ return GetMultiUIScheduleItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemCollection. When set to true, UIScheduleItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemCollection
		{
			get	{ return _alwaysFetchUIScheduleItemCollection; }
			set	{ _alwaysFetchUIScheduleItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemCollection already has been fetched. Setting this property to false when UIScheduleItemCollection has been fetched
		/// will clear the UIScheduleItemCollection collection well. Setting this property to true while UIScheduleItemCollection hasn't been fetched disables lazy loading for UIScheduleItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemCollection
		{
			get { return _alreadyFetchedUIScheduleItemCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemCollection && !value && (_uIScheduleItemCollection != null))
				{
					_uIScheduleItemCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAnnouncement
		{
			get { return GetMultiCategoryCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAnnouncement. When set to true, CategoryCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAnnouncement; }
			set	{ _alwaysFetchCategoryCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAnnouncement already has been fetched. Setting this property to false when CategoryCollectionViaAnnouncement has been fetched
		/// will clear the CategoryCollectionViaAnnouncement collection well. Setting this property to true while CategoryCollectionViaAnnouncement hasn't been fetched disables lazy loading for CategoryCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAnnouncement
		{
			get { return _alreadyFetchedCategoryCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAnnouncement && !value && (_categoryCollectionViaAnnouncement != null))
				{
					_categoryCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAnnouncement_
		{
			get { return GetMultiCategoryCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAnnouncement_. When set to true, CategoryCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchCategoryCollectionViaAnnouncement_; }
			set	{ _alwaysFetchCategoryCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAnnouncement_ already has been fetched. Setting this property to false when CategoryCollectionViaAnnouncement_ has been fetched
		/// will clear the CategoryCollectionViaAnnouncement_ collection well. Setting this property to true while CategoryCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for CategoryCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedCategoryCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAnnouncement_ && !value && (_categoryCollectionViaAnnouncement_ != null))
				{
					_categoryCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessage
		{
			get { return GetMultiCategoryCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessage. When set to true, CategoryCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessage
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessage; }
			set	{ _alwaysFetchCategoryCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessage already has been fetched. Setting this property to false when CategoryCollectionViaMessage has been fetched
		/// will clear the CategoryCollectionViaMessage collection well. Setting this property to true while CategoryCollectionViaMessage hasn't been fetched disables lazy loading for CategoryCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessage
		{
			get { return _alreadyFetchedCategoryCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessage && !value && (_categoryCollectionViaMessage != null))
				{
					_categoryCollectionViaMessage.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessageTemplate
		{
			get { return GetMultiCategoryCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessageTemplate. When set to true, CategoryCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessageTemplate; }
			set	{ _alwaysFetchCategoryCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessageTemplate already has been fetched. Setting this property to false when CategoryCollectionViaMessageTemplate has been fetched
		/// will clear the CategoryCollectionViaMessageTemplate collection well. Setting this property to true while CategoryCollectionViaMessageTemplate hasn't been fetched disables lazy loading for CategoryCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedCategoryCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessageTemplate && !value && (_categoryCollectionViaMessageTemplate != null))
				{
					_categoryCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaMessage
		{
			get { return GetMultiClientCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaMessage. When set to true, ClientCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaMessage
		{
			get	{ return _alwaysFetchClientCollectionViaMessage; }
			set	{ _alwaysFetchClientCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaMessage already has been fetched. Setting this property to false when ClientCollectionViaMessage has been fetched
		/// will clear the ClientCollectionViaMessage collection well. Setting this property to true while ClientCollectionViaMessage hasn't been fetched disables lazy loading for ClientCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaMessage
		{
			get { return _alreadyFetchedClientCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaMessage && !value && (_clientCollectionViaMessage != null))
				{
					_clientCollectionViaMessage.Clear();
				}
				_alreadyFetchedClientCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAnnouncement
		{
			get { return GetMultiCompanyCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAnnouncement. When set to true, CompanyCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchCompanyCollectionViaAnnouncement; }
			set	{ _alwaysFetchCompanyCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAnnouncement already has been fetched. Setting this property to false when CompanyCollectionViaAnnouncement has been fetched
		/// will clear the CompanyCollectionViaAnnouncement collection well. Setting this property to true while CompanyCollectionViaAnnouncement hasn't been fetched disables lazy loading for CompanyCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAnnouncement
		{
			get { return _alreadyFetchedCompanyCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAnnouncement && !value && (_companyCollectionViaAnnouncement != null))
				{
					_companyCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMessage
		{
			get { return GetMultiCompanyCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMessage. When set to true, CompanyCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMessage
		{
			get	{ return _alwaysFetchCompanyCollectionViaMessage; }
			set	{ _alwaysFetchCompanyCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMessage already has been fetched. Setting this property to false when CompanyCollectionViaMessage has been fetched
		/// will clear the CompanyCollectionViaMessage collection well. Setting this property to true while CompanyCollectionViaMessage hasn't been fetched disables lazy loading for CompanyCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMessage
		{
			get { return _alreadyFetchedCompanyCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMessage && !value && (_companyCollectionViaMessage != null))
				{
					_companyCollectionViaMessage.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMessageTemplate
		{
			get { return GetMultiCompanyCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMessageTemplate. When set to true, CompanyCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchCompanyCollectionViaMessageTemplate; }
			set	{ _alwaysFetchCompanyCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMessageTemplate already has been fetched. Setting this property to false when CompanyCollectionViaMessageTemplate has been fetched
		/// will clear the CompanyCollectionViaMessageTemplate collection well. Setting this property to true while CompanyCollectionViaMessageTemplate hasn't been fetched disables lazy loading for CompanyCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedCompanyCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMessageTemplate && !value && (_companyCollectionViaMessageTemplate != null))
				{
					_companyCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaMessage
		{
			get { return GetMultiCustomerCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaMessage. When set to true, CustomerCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaMessage
		{
			get	{ return _alwaysFetchCustomerCollectionViaMessage; }
			set	{ _alwaysFetchCustomerCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaMessage already has been fetched. Setting this property to false when CustomerCollectionViaMessage has been fetched
		/// will clear the CustomerCollectionViaMessage collection well. Setting this property to true while CustomerCollectionViaMessage hasn't been fetched disables lazy loading for CustomerCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaMessage
		{
			get { return _alreadyFetchedCustomerCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaMessage && !value && (_customerCollectionViaMessage != null))
				{
					_customerCollectionViaMessage.Clear();
				}
				_alreadyFetchedCustomerCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessage
		{
			get { return GetMultiDeliverypointCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessage. When set to true, DeliverypointCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessage already has been fetched. Setting this property to false when DeliverypointCollectionViaMessage has been fetched
		/// will clear the DeliverypointCollectionViaMessage collection well. Setting this property to true while DeliverypointCollectionViaMessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessage && !value && (_deliverypointCollectionViaMessage != null))
				{
					_deliverypointCollectionViaMessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAnnouncement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAnnouncement. When set to true, DeliverypointgroupCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAnnouncement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAnnouncement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAnnouncement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAnnouncement collection well. Setting this property to true while DeliverypointgroupCollectionViaAnnouncement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAnnouncement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement && !value && (_deliverypointgroupCollectionViaAnnouncement != null))
				{
					_deliverypointgroupCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement. When set to true, EntertainmentCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement && !value && (_entertainmentCollectionViaAnnouncement != null))
				{
					_entertainmentCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessageTemplate
		{
			get { return GetMultiEntertainmentCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessageTemplate. When set to true, EntertainmentCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessageTemplate; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessageTemplate already has been fetched. Setting this property to false when EntertainmentCollectionViaMessageTemplate has been fetched
		/// will clear the EntertainmentCollectionViaMessageTemplate collection well. Setting this property to true while EntertainmentCollectionViaMessageTemplate hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessageTemplate && !value && (_entertainmentCollectionViaMessageTemplate != null))
				{
					_entertainmentCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAnnouncement. When set to true, EntertainmentcategoryCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement && !value && (_entertainmentcategoryCollectionViaAnnouncement != null))
				{
					_entertainmentcategoryCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAnnouncement_
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAnnouncement_. When set to true, EntertainmentcategoryCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAnnouncement_ already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAnnouncement_ has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAnnouncement_ collection well. Setting this property to true while EntertainmentcategoryCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ && !value && (_entertainmentcategoryCollectionViaAnnouncement_ != null))
				{
					_entertainmentcategoryCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaMessage
		{
			get { return GetMultiOrderCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaMessage. When set to true, OrderCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaMessage
		{
			get	{ return _alwaysFetchOrderCollectionViaMessage; }
			set	{ _alwaysFetchOrderCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaMessage already has been fetched. Setting this property to false when OrderCollectionViaMessage has been fetched
		/// will clear the OrderCollectionViaMessage collection well. Setting this property to true while OrderCollectionViaMessage hasn't been fetched disables lazy loading for OrderCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaMessage
		{
			get { return _alreadyFetchedOrderCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaMessage && !value && (_orderCollectionViaMessage != null))
				{
					_orderCollectionViaMessage.Clear();
				}
				_alreadyFetchedOrderCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement
		{
			get { return GetMultiProductCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement. When set to true, ProductCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement has been fetched
		/// will clear the ProductCollectionViaAnnouncement collection well. Setting this property to true while ProductCollectionViaAnnouncement hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement && !value && (_productCollectionViaAnnouncement != null))
				{
					_productCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMessageTemplate
		{
			get { return GetMultiProductCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMessageTemplate. When set to true, ProductCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchProductCollectionViaMessageTemplate; }
			set	{ _alwaysFetchProductCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMessageTemplate already has been fetched. Setting this property to false when ProductCollectionViaMessageTemplate has been fetched
		/// will clear the ProductCollectionViaMessageTemplate collection well. Setting this property to true while ProductCollectionViaMessageTemplate hasn't been fetched disables lazy loading for ProductCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedProductCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMessageTemplate && !value && (_productCollectionViaMessageTemplate != null))
				{
					_productCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedProductCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementEntity AdvertisementEntity
		{
			get	{ return GetSingleAdvertisementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "AdvertisementEntity", _advertisementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementEntity. When set to true, AdvertisementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementEntity
		{
			get	{ return _alwaysFetchAdvertisementEntity; }
			set	{ _alwaysFetchAdvertisementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementEntity already has been fetched. Setting this property to false when AdvertisementEntity has been fetched
		/// will set AdvertisementEntity to null as well. Setting this property to true while AdvertisementEntity hasn't been fetched disables lazy loading for AdvertisementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementEntity
		{
			get { return _alreadyFetchedAdvertisementEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementEntity && !value)
				{
					this.AdvertisementEntity = null;
				}
				_alreadyFetchedAdvertisementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementEntity is not found
		/// in the database. When set to true, AdvertisementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementEntityReturnsNewIfNotFound; }
			set { _advertisementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationoptionEntity AlterationoptionEntity
		{
			get	{ return GetSingleAlterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "AlterationoptionEntity", _alterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionEntity. When set to true, AlterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionEntity
		{
			get	{ return _alwaysFetchAlterationoptionEntity; }
			set	{ _alwaysFetchAlterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionEntity already has been fetched. Setting this property to false when AlterationoptionEntity has been fetched
		/// will set AlterationoptionEntity to null as well. Setting this property to true while AlterationoptionEntity hasn't been fetched disables lazy loading for AlterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionEntity
		{
			get { return _alreadyFetchedAlterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedAlterationoptionEntity && !value)
				{
					this.AlterationoptionEntity = null;
				}
				_alreadyFetchedAlterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationoptionEntity is not found
		/// in the database. When set to true, AlterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _alterationoptionEntityReturnsNewIfNotFound; }
			set { _alterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CarouselItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCarouselItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CarouselItemEntity CarouselItemEntity
		{
			get	{ return GetSingleCarouselItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCarouselItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "CarouselItemEntity", _carouselItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CarouselItemEntity. When set to true, CarouselItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CarouselItemEntity is accessed. You can always execute a forced fetch by calling GetSingleCarouselItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCarouselItemEntity
		{
			get	{ return _alwaysFetchCarouselItemEntity; }
			set	{ _alwaysFetchCarouselItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CarouselItemEntity already has been fetched. Setting this property to false when CarouselItemEntity has been fetched
		/// will set CarouselItemEntity to null as well. Setting this property to true while CarouselItemEntity hasn't been fetched disables lazy loading for CarouselItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCarouselItemEntity
		{
			get { return _alreadyFetchedCarouselItemEntity;}
			set 
			{
				if(_alreadyFetchedCarouselItemEntity && !value)
				{
					this.CarouselItemEntity = null;
				}
				_alreadyFetchedCarouselItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CarouselItemEntity is not found
		/// in the database. When set to true, CarouselItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CarouselItemEntityReturnsNewIfNotFound
		{
			get	{ return _carouselItemEntityReturnsNewIfNotFound; }
			set { _carouselItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LandingPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLandingPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LandingPageEntity LandingPageEntity
		{
			get	{ return GetSingleLandingPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLandingPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "LandingPageEntity", _landingPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageEntity. When set to true, LandingPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageEntity is accessed. You can always execute a forced fetch by calling GetSingleLandingPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageEntity
		{
			get	{ return _alwaysFetchLandingPageEntity; }
			set	{ _alwaysFetchLandingPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageEntity already has been fetched. Setting this property to false when LandingPageEntity has been fetched
		/// will set LandingPageEntity to null as well. Setting this property to true while LandingPageEntity hasn't been fetched disables lazy loading for LandingPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageEntity
		{
			get { return _alreadyFetchedLandingPageEntity;}
			set 
			{
				if(_alreadyFetchedLandingPageEntity && !value)
				{
					this.LandingPageEntity = null;
				}
				_alreadyFetchedLandingPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LandingPageEntity is not found
		/// in the database. When set to true, LandingPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LandingPageEntityReturnsNewIfNotFound
		{
			get	{ return _landingPageEntityReturnsNewIfNotFound; }
			set { _landingPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetActionBannerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetActionBannerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetActionBannerEntity WidgetActionBannerEntity
		{
			get	{ return GetSingleWidgetActionBannerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetActionBannerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "WidgetActionBannerEntity", _widgetActionBannerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetActionBannerEntity. When set to true, WidgetActionBannerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetActionBannerEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetActionBannerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetActionBannerEntity
		{
			get	{ return _alwaysFetchWidgetActionBannerEntity; }
			set	{ _alwaysFetchWidgetActionBannerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetActionBannerEntity already has been fetched. Setting this property to false when WidgetActionBannerEntity has been fetched
		/// will set WidgetActionBannerEntity to null as well. Setting this property to true while WidgetActionBannerEntity hasn't been fetched disables lazy loading for WidgetActionBannerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetActionBannerEntity
		{
			get { return _alreadyFetchedWidgetActionBannerEntity;}
			set 
			{
				if(_alreadyFetchedWidgetActionBannerEntity && !value)
				{
					this.WidgetActionBannerEntity = null;
				}
				_alreadyFetchedWidgetActionBannerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetActionBannerEntity is not found
		/// in the database. When set to true, WidgetActionBannerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetActionBannerEntityReturnsNewIfNotFound
		{
			get	{ return _widgetActionBannerEntityReturnsNewIfNotFound; }
			set { _widgetActionBannerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetHeroEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetHeroEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetHeroEntity WidgetHeroEntity
		{
			get	{ return GetSingleWidgetHeroEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetHeroEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "WidgetHeroEntity", _widgetHeroEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetHeroEntity. When set to true, WidgetHeroEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetHeroEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetHeroEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetHeroEntity
		{
			get	{ return _alwaysFetchWidgetHeroEntity; }
			set	{ _alwaysFetchWidgetHeroEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetHeroEntity already has been fetched. Setting this property to false when WidgetHeroEntity has been fetched
		/// will set WidgetHeroEntity to null as well. Setting this property to true while WidgetHeroEntity hasn't been fetched disables lazy loading for WidgetHeroEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetHeroEntity
		{
			get { return _alreadyFetchedWidgetHeroEntity;}
			set 
			{
				if(_alreadyFetchedWidgetHeroEntity && !value)
				{
					this.WidgetHeroEntity = null;
				}
				_alreadyFetchedWidgetHeroEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetHeroEntity is not found
		/// in the database. When set to true, WidgetHeroEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetHeroEntityReturnsNewIfNotFound
		{
			get	{ return _widgetHeroEntityReturnsNewIfNotFound; }
			set { _widgetHeroEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttachmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttachmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AttachmentEntity AttachmentEntity
		{
			get	{ return GetSingleAttachmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttachmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "AttachmentEntity", _attachmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentEntity. When set to true, AttachmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentEntity is accessed. You can always execute a forced fetch by calling GetSingleAttachmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentEntity
		{
			get	{ return _alwaysFetchAttachmentEntity; }
			set	{ _alwaysFetchAttachmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentEntity already has been fetched. Setting this property to false when AttachmentEntity has been fetched
		/// will set AttachmentEntity to null as well. Setting this property to true while AttachmentEntity hasn't been fetched disables lazy loading for AttachmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentEntity
		{
			get { return _alreadyFetchedAttachmentEntity;}
			set 
			{
				if(_alreadyFetchedAttachmentEntity && !value)
				{
					this.AttachmentEntity = null;
				}
				_alreadyFetchedAttachmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttachmentEntity is not found
		/// in the database. When set to true, AttachmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AttachmentEntityReturnsNewIfNotFound
		{
			get	{ return _attachmentEntityReturnsNewIfNotFound; }
			set { _attachmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity ActionCategoryEntity
		{
			get	{ return GetSingleActionCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionMediaCollection", "ActionCategoryEntity", _actionCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionCategoryEntity. When set to true, ActionCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleActionCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionCategoryEntity
		{
			get	{ return _alwaysFetchActionCategoryEntity; }
			set	{ _alwaysFetchActionCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionCategoryEntity already has been fetched. Setting this property to false when ActionCategoryEntity has been fetched
		/// will set ActionCategoryEntity to null as well. Setting this property to true while ActionCategoryEntity hasn't been fetched disables lazy loading for ActionCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionCategoryEntity
		{
			get { return _alreadyFetchedActionCategoryEntity;}
			set 
			{
				if(_alreadyFetchedActionCategoryEntity && !value)
				{
					this.ActionCategoryEntity = null;
				}
				_alreadyFetchedActionCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionCategoryEntity is not found
		/// in the database. When set to true, ActionCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _actionCategoryEntityReturnsNewIfNotFound; }
			set { _actionCategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity ActionEntertainmentEntity
		{
			get	{ return GetSingleActionEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionMediaCollection", "ActionEntertainmentEntity", _actionEntertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionEntertainmentEntity. When set to true, ActionEntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionEntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleActionEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionEntertainmentEntity
		{
			get	{ return _alwaysFetchActionEntertainmentEntity; }
			set	{ _alwaysFetchActionEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionEntertainmentEntity already has been fetched. Setting this property to false when ActionEntertainmentEntity has been fetched
		/// will set ActionEntertainmentEntity to null as well. Setting this property to true while ActionEntertainmentEntity hasn't been fetched disables lazy loading for ActionEntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionEntertainmentEntity
		{
			get { return _alreadyFetchedActionEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedActionEntertainmentEntity && !value)
				{
					this.ActionEntertainmentEntity = null;
				}
				_alreadyFetchedActionEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionEntertainmentEntity is not found
		/// in the database. When set to true, ActionEntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionEntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _actionEntertainmentEntityReturnsNewIfNotFound; }
			set { _actionEntertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionEntertainmentcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity ActionEntertainmentcategoryEntity
		{
			get	{ return GetSingleActionEntertainmentcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionEntertainmentcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionMediaCollection", "ActionEntertainmentcategoryEntity", _actionEntertainmentcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionEntertainmentcategoryEntity. When set to true, ActionEntertainmentcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionEntertainmentcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleActionEntertainmentcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionEntertainmentcategoryEntity
		{
			get	{ return _alwaysFetchActionEntertainmentcategoryEntity; }
			set	{ _alwaysFetchActionEntertainmentcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionEntertainmentcategoryEntity already has been fetched. Setting this property to false when ActionEntertainmentcategoryEntity has been fetched
		/// will set ActionEntertainmentcategoryEntity to null as well. Setting this property to true while ActionEntertainmentcategoryEntity hasn't been fetched disables lazy loading for ActionEntertainmentcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionEntertainmentcategoryEntity
		{
			get { return _alreadyFetchedActionEntertainmentcategoryEntity;}
			set 
			{
				if(_alreadyFetchedActionEntertainmentcategoryEntity && !value)
				{
					this.ActionEntertainmentcategoryEntity = null;
				}
				_alreadyFetchedActionEntertainmentcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionEntertainmentcategoryEntity is not found
		/// in the database. When set to true, ActionEntertainmentcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionEntertainmentcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _actionEntertainmentcategoryEntityReturnsNewIfNotFound; }
			set { _actionEntertainmentcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericcategoryEntity GenericcategoryEntity
		{
			get	{ return GetSingleGenericcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "GenericcategoryEntity", _genericcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryEntity. When set to true, GenericcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryEntity
		{
			get	{ return _alwaysFetchGenericcategoryEntity; }
			set	{ _alwaysFetchGenericcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryEntity already has been fetched. Setting this property to false when GenericcategoryEntity has been fetched
		/// will set GenericcategoryEntity to null as well. Setting this property to true while GenericcategoryEntity hasn't been fetched disables lazy loading for GenericcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryEntity
		{
			get { return _alreadyFetchedGenericcategoryEntity;}
			set 
			{
				if(_alreadyFetchedGenericcategoryEntity && !value)
				{
					this.GenericcategoryEntity = null;
				}
				_alreadyFetchedGenericcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericcategoryEntity is not found
		/// in the database. When set to true, GenericcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _genericcategoryEntityReturnsNewIfNotFound; }
			set { _genericcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericproductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericproductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericproductEntity GenericproductEntity
		{
			get	{ return GetSingleGenericproductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericproductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "GenericproductEntity", _genericproductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductEntity. When set to true, GenericproductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericproductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductEntity
		{
			get	{ return _alwaysFetchGenericproductEntity; }
			set	{ _alwaysFetchGenericproductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductEntity already has been fetched. Setting this property to false when GenericproductEntity has been fetched
		/// will set GenericproductEntity to null as well. Setting this property to true while GenericproductEntity hasn't been fetched disables lazy loading for GenericproductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductEntity
		{
			get { return _alreadyFetchedGenericproductEntity;}
			set 
			{
				if(_alreadyFetchedGenericproductEntity && !value)
				{
					this.GenericproductEntity = null;
				}
				_alreadyFetchedGenericproductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericproductEntity is not found
		/// in the database. When set to true, GenericproductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericproductEntityReturnsNewIfNotFound
		{
			get	{ return _genericproductEntityReturnsNewIfNotFound; }
			set { _genericproductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity_
		{
			get	{ return GetSinglePageEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection_", "PageEntity_", _pageEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity_. When set to true, PageEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity_ is accessed. You can always execute a forced fetch by calling GetSinglePageEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity_
		{
			get	{ return _alwaysFetchPageEntity_; }
			set	{ _alwaysFetchPageEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity_ already has been fetched. Setting this property to false when PageEntity_ has been fetched
		/// will set PageEntity_ to null as well. Setting this property to true while PageEntity_ hasn't been fetched disables lazy loading for PageEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity_
		{
			get { return _alreadyFetchedPageEntity_;}
			set 
			{
				if(_alreadyFetchedPageEntity_ && !value)
				{
					this.PageEntity_ = null;
				}
				_alreadyFetchedPageEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity_ is not found
		/// in the database. When set to true, PageEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntity_ReturnsNewIfNotFound
		{
			get	{ return _pageEntity_ReturnsNewIfNotFound; }
			set { _pageEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageElementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageElementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageElementEntity PageElementEntity
		{
			get	{ return GetSinglePageElementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageElementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "PageElementEntity", _pageElementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageElementEntity. When set to true, PageElementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageElementEntity is accessed. You can always execute a forced fetch by calling GetSinglePageElementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageElementEntity
		{
			get	{ return _alwaysFetchPageElementEntity; }
			set	{ _alwaysFetchPageElementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageElementEntity already has been fetched. Setting this property to false when PageElementEntity has been fetched
		/// will set PageElementEntity to null as well. Setting this property to true while PageElementEntity hasn't been fetched disables lazy loading for PageElementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageElementEntity
		{
			get { return _alreadyFetchedPageElementEntity;}
			set 
			{
				if(_alreadyFetchedPageElementEntity && !value)
				{
					this.PageElementEntity = null;
				}
				_alreadyFetchedPageElementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageElementEntity is not found
		/// in the database. When set to true, PageElementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageElementEntityReturnsNewIfNotFound
		{
			get	{ return _pageElementEntityReturnsNewIfNotFound; }
			set { _pageElementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageTemplateEntity PageTemplateEntity
		{
			get	{ return GetSinglePageTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "PageTemplateEntity", _pageTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateEntity. When set to true, PageTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateEntity is accessed. You can always execute a forced fetch by calling GetSinglePageTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateEntity
		{
			get	{ return _alwaysFetchPageTemplateEntity; }
			set	{ _alwaysFetchPageTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateEntity already has been fetched. Setting this property to false when PageTemplateEntity has been fetched
		/// will set PageTemplateEntity to null as well. Setting this property to true while PageTemplateEntity hasn't been fetched disables lazy loading for PageTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateEntity
		{
			get { return _alreadyFetchedPageTemplateEntity;}
			set 
			{
				if(_alreadyFetchedPageTemplateEntity && !value)
				{
					this.PageTemplateEntity = null;
				}
				_alreadyFetchedPageTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageTemplateEntity is not found
		/// in the database. When set to true, PageTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _pageTemplateEntityReturnsNewIfNotFound; }
			set { _pageTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageTemplateElementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageTemplateElementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageTemplateElementEntity PageTemplateElementEntity
		{
			get	{ return GetSinglePageTemplateElementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageTemplateElementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "PageTemplateElementEntity", _pageTemplateElementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateElementEntity. When set to true, PageTemplateElementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateElementEntity is accessed. You can always execute a forced fetch by calling GetSinglePageTemplateElementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateElementEntity
		{
			get	{ return _alwaysFetchPageTemplateElementEntity; }
			set	{ _alwaysFetchPageTemplateElementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateElementEntity already has been fetched. Setting this property to false when PageTemplateElementEntity has been fetched
		/// will set PageTemplateElementEntity to null as well. Setting this property to true while PageTemplateElementEntity hasn't been fetched disables lazy loading for PageTemplateElementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateElementEntity
		{
			get { return _alreadyFetchedPageTemplateElementEntity;}
			set 
			{
				if(_alreadyFetchedPageTemplateElementEntity && !value)
				{
					this.PageTemplateElementEntity = null;
				}
				_alreadyFetchedPageTemplateElementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageTemplateElementEntity is not found
		/// in the database. When set to true, PageTemplateElementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageTemplateElementEntityReturnsNewIfNotFound
		{
			get	{ return _pageTemplateElementEntityReturnsNewIfNotFound; }
			set { _pageTemplateElementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "PointOfInterestEntity", _pointOfInterestEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set { _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ActionProductEntity
		{
			get	{ return GetSingleActionProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionMediaCollection", "ActionProductEntity", _actionProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionProductEntity. When set to true, ActionProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionProductEntity is accessed. You can always execute a forced fetch by calling GetSingleActionProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionProductEntity
		{
			get	{ return _alwaysFetchActionProductEntity; }
			set	{ _alwaysFetchActionProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionProductEntity already has been fetched. Setting this property to false when ActionProductEntity has been fetched
		/// will set ActionProductEntity to null as well. Setting this property to true while ActionProductEntity hasn't been fetched disables lazy loading for ActionProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionProductEntity
		{
			get { return _alreadyFetchedActionProductEntity;}
			set 
			{
				if(_alreadyFetchedActionProductEntity && !value)
				{
					this.ActionProductEntity = null;
				}
				_alreadyFetchedActionProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionProductEntity is not found
		/// in the database. When set to true, ActionProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionProductEntityReturnsNewIfNotFound
		{
			get	{ return _actionProductEntityReturnsNewIfNotFound; }
			set { _actionProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductgroupEntity ProductgroupEntity
		{
			get	{ return GetSingleProductgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "ProductgroupEntity", _productgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductgroupEntity. When set to true, ProductgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleProductgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductgroupEntity
		{
			get	{ return _alwaysFetchProductgroupEntity; }
			set	{ _alwaysFetchProductgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductgroupEntity already has been fetched. Setting this property to false when ProductgroupEntity has been fetched
		/// will set ProductgroupEntity to null as well. Setting this property to true while ProductgroupEntity hasn't been fetched disables lazy loading for ProductgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductgroupEntity
		{
			get { return _alreadyFetchedProductgroupEntity;}
			set 
			{
				if(_alreadyFetchedProductgroupEntity && !value)
				{
					this.ProductgroupEntity = null;
				}
				_alreadyFetchedProductgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductgroupEntity is not found
		/// in the database. When set to true, ProductgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductgroupEntityReturnsNewIfNotFound
		{
			get	{ return _productgroupEntityReturnsNewIfNotFound; }
			set { _productgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionItemEntity RoomControlSectionItemEntity
		{
			get	{ return GetSingleRoomControlSectionItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "RoomControlSectionItemEntity", _roomControlSectionItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemEntity. When set to true, RoomControlSectionItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemEntity
		{
			get	{ return _alwaysFetchRoomControlSectionItemEntity; }
			set	{ _alwaysFetchRoomControlSectionItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemEntity already has been fetched. Setting this property to false when RoomControlSectionItemEntity has been fetched
		/// will set RoomControlSectionItemEntity to null as well. Setting this property to true while RoomControlSectionItemEntity hasn't been fetched disables lazy loading for RoomControlSectionItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemEntity
		{
			get { return _alreadyFetchedRoomControlSectionItemEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemEntity && !value)
				{
					this.RoomControlSectionItemEntity = null;
				}
				_alreadyFetchedRoomControlSectionItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionItemEntity is not found
		/// in the database. When set to true, RoomControlSectionItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionItemEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionItemEntityReturnsNewIfNotFound; }
			set { _roomControlSectionItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoutestephandlerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoutestephandlerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoutestephandlerEntity RoutestephandlerEntity
		{
			get	{ return GetSingleRoutestephandlerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoutestephandlerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "RoutestephandlerEntity", _routestephandlerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerEntity. When set to true, RoutestephandlerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerEntity is accessed. You can always execute a forced fetch by calling GetSingleRoutestephandlerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerEntity
		{
			get	{ return _alwaysFetchRoutestephandlerEntity; }
			set	{ _alwaysFetchRoutestephandlerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerEntity already has been fetched. Setting this property to false when RoutestephandlerEntity has been fetched
		/// will set RoutestephandlerEntity to null as well. Setting this property to true while RoutestephandlerEntity hasn't been fetched disables lazy loading for RoutestephandlerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerEntity
		{
			get { return _alreadyFetchedRoutestephandlerEntity;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerEntity && !value)
				{
					this.RoutestephandlerEntity = null;
				}
				_alreadyFetchedRoutestephandlerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoutestephandlerEntity is not found
		/// in the database. When set to true, RoutestephandlerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoutestephandlerEntityReturnsNewIfNotFound
		{
			get	{ return _routestephandlerEntityReturnsNewIfNotFound; }
			set { _routestephandlerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity_
		{
			get	{ return GetSingleSiteEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection_", "SiteEntity_", _siteEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity_. When set to true, SiteEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity_ is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity_
		{
			get	{ return _alwaysFetchSiteEntity_; }
			set	{ _alwaysFetchSiteEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity_ already has been fetched. Setting this property to false when SiteEntity_ has been fetched
		/// will set SiteEntity_ to null as well. Setting this property to true while SiteEntity_ hasn't been fetched disables lazy loading for SiteEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity_
		{
			get { return _alreadyFetchedSiteEntity_;}
			set 
			{
				if(_alreadyFetchedSiteEntity_ && !value)
				{
					this.SiteEntity_ = null;
				}
				_alreadyFetchedSiteEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity_ is not found
		/// in the database. When set to true, SiteEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntity_ReturnsNewIfNotFound
		{
			get	{ return _siteEntity_ReturnsNewIfNotFound; }
			set { _siteEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleStationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual StationEntity StationEntity
		{
			get	{ return GetSingleStationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncStationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "StationEntity", _stationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for StationEntity. When set to true, StationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StationEntity is accessed. You can always execute a forced fetch by calling GetSingleStationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStationEntity
		{
			get	{ return _alwaysFetchStationEntity; }
			set	{ _alwaysFetchStationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StationEntity already has been fetched. Setting this property to false when StationEntity has been fetched
		/// will set StationEntity to null as well. Setting this property to true while StationEntity hasn't been fetched disables lazy loading for StationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStationEntity
		{
			get { return _alreadyFetchedStationEntity;}
			set 
			{
				if(_alreadyFetchedStationEntity && !value)
				{
					this.StationEntity = null;
				}
				_alreadyFetchedStationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property StationEntity is not found
		/// in the database. When set to true, StationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool StationEntityReturnsNewIfNotFound
		{
			get	{ return _stationEntityReturnsNewIfNotFound; }
			set { _stationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyEntity SurveyEntity
		{
			get	{ return GetSingleSurveyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "SurveyEntity", _surveyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyEntity. When set to true, SurveyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyEntity
		{
			get	{ return _alwaysFetchSurveyEntity; }
			set	{ _alwaysFetchSurveyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyEntity already has been fetched. Setting this property to false when SurveyEntity has been fetched
		/// will set SurveyEntity to null as well. Setting this property to true while SurveyEntity hasn't been fetched disables lazy loading for SurveyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyEntity
		{
			get { return _alreadyFetchedSurveyEntity;}
			set 
			{
				if(_alreadyFetchedSurveyEntity && !value)
				{
					this.SurveyEntity = null;
				}
				_alreadyFetchedSurveyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyEntity is not found
		/// in the database. When set to true, SurveyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyEntityReturnsNewIfNotFound
		{
			get	{ return _surveyEntityReturnsNewIfNotFound; }
			set { _surveyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyPageEntity SurveyPageEntity
		{
			get	{ return GetSingleSurveyPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "SurveyPageEntity", _surveyPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageEntity. When set to true, SurveyPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageEntity
		{
			get	{ return _alwaysFetchSurveyPageEntity; }
			set	{ _alwaysFetchSurveyPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageEntity already has been fetched. Setting this property to false when SurveyPageEntity has been fetched
		/// will set SurveyPageEntity to null as well. Setting this property to true while SurveyPageEntity hasn't been fetched disables lazy loading for SurveyPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageEntity
		{
			get { return _alreadyFetchedSurveyPageEntity;}
			set 
			{
				if(_alreadyFetchedSurveyPageEntity && !value)
				{
					this.SurveyPageEntity = null;
				}
				_alreadyFetchedSurveyPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyPageEntity is not found
		/// in the database. When set to true, SurveyPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyPageEntityReturnsNewIfNotFound
		{
			get	{ return _surveyPageEntityReturnsNewIfNotFound; }
			set { _surveyPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIFooterItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIFooterItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIFooterItemEntity UIFooterItemEntity
		{
			get	{ return GetSingleUIFooterItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIFooterItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "UIFooterItemEntity", _uIFooterItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIFooterItemEntity. When set to true, UIFooterItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIFooterItemEntity is accessed. You can always execute a forced fetch by calling GetSingleUIFooterItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIFooterItemEntity
		{
			get	{ return _alwaysFetchUIFooterItemEntity; }
			set	{ _alwaysFetchUIFooterItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIFooterItemEntity already has been fetched. Setting this property to false when UIFooterItemEntity has been fetched
		/// will set UIFooterItemEntity to null as well. Setting this property to true while UIFooterItemEntity hasn't been fetched disables lazy loading for UIFooterItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIFooterItemEntity
		{
			get { return _alreadyFetchedUIFooterItemEntity;}
			set 
			{
				if(_alreadyFetchedUIFooterItemEntity && !value)
				{
					this.UIFooterItemEntity = null;
				}
				_alreadyFetchedUIFooterItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIFooterItemEntity is not found
		/// in the database. When set to true, UIFooterItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIFooterItemEntityReturnsNewIfNotFound
		{
			get	{ return _uIFooterItemEntityReturnsNewIfNotFound; }
			set { _uIFooterItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIThemeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIThemeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIThemeEntity UIThemeEntity
		{
			get	{ return GetSingleUIThemeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIThemeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "UIThemeEntity", _uIThemeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeEntity. When set to true, UIThemeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIThemeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeEntity
		{
			get	{ return _alwaysFetchUIThemeEntity; }
			set	{ _alwaysFetchUIThemeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeEntity already has been fetched. Setting this property to false when UIThemeEntity has been fetched
		/// will set UIThemeEntity to null as well. Setting this property to true while UIThemeEntity hasn't been fetched disables lazy loading for UIThemeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeEntity
		{
			get { return _alreadyFetchedUIThemeEntity;}
			set 
			{
				if(_alreadyFetchedUIThemeEntity && !value)
				{
					this.UIThemeEntity = null;
				}
				_alreadyFetchedUIThemeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIThemeEntity is not found
		/// in the database. When set to true, UIThemeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIThemeEntityReturnsNewIfNotFound
		{
			get	{ return _uIThemeEntityReturnsNewIfNotFound; }
			set { _uIThemeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIWidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIWidgetEntity UIWidgetEntity
		{
			get	{ return GetSingleUIWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaCollection", "UIWidgetEntity", _uIWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetEntity. When set to true, UIWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleUIWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetEntity
		{
			get	{ return _alwaysFetchUIWidgetEntity; }
			set	{ _alwaysFetchUIWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetEntity already has been fetched. Setting this property to false when UIWidgetEntity has been fetched
		/// will set UIWidgetEntity to null as well. Setting this property to true while UIWidgetEntity hasn't been fetched disables lazy loading for UIWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetEntity
		{
			get { return _alreadyFetchedUIWidgetEntity;}
			set 
			{
				if(_alreadyFetchedUIWidgetEntity && !value)
				{
					this.UIWidgetEntity = null;
				}
				_alreadyFetchedUIWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIWidgetEntity is not found
		/// in the database. When set to true, UIWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _uIWidgetEntityReturnsNewIfNotFound; }
			set { _uIWidgetEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MediaEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
