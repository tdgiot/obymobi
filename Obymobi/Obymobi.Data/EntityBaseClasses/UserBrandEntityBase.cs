﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UserBrand'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UserBrandEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UserBrandEntity"; }
		}
	
		#region Class Member Declarations
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;
		private UserEntity _userEntity;
		private bool	_alwaysFetchUserEntity, _alreadyFetchedUserEntity, _userEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name UserEntity</summary>
			public static readonly string UserEntity = "UserEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserBrandEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UserBrandEntityBase() :base("UserBrandEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		protected UserBrandEntityBase(System.Int32 userBrandId):base("UserBrandEntity")
		{
			InitClassFetch(userBrandId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UserBrandEntityBase(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse): base("UserBrandEntity")
		{
			InitClassFetch(userBrandId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="validator">The custom validator object for this UserBrandEntity</param>
		protected UserBrandEntityBase(System.Int32 userBrandId, IValidator validator):base("UserBrandEntity")
		{
			InitClassFetch(userBrandId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserBrandEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");

			_userEntity = (UserEntity)info.GetValue("_userEntity", typeof(UserEntity));
			if(_userEntity!=null)
			{
				_userEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userEntityReturnsNewIfNotFound = info.GetBoolean("_userEntityReturnsNewIfNotFound");
			_alwaysFetchUserEntity = info.GetBoolean("_alwaysFetchUserEntity");
			_alreadyFetchedUserEntity = info.GetBoolean("_alreadyFetchedUserEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UserBrandFieldIndex)fieldIndex)
			{
				case UserBrandFieldIndex.UserId:
					DesetupSyncUserEntity(true, false);
					_alreadyFetchedUserEntity = false;
					break;
				case UserBrandFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBrandEntity = (_brandEntity != null);
			_alreadyFetchedUserEntity = (_userEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "UserEntity":
					toReturn.Add(Relations.UserEntityUsingUserId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);
			info.AddValue("_userEntity", (!this.MarkedForDeletion?_userEntity:null));
			info.AddValue("_userEntityReturnsNewIfNotFound", _userEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserEntity", _alwaysFetchUserEntity);
			info.AddValue("_alreadyFetchedUserEntity", _alreadyFetchedUserEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "UserEntity":
					_alreadyFetchedUserEntity = true;
					this.UserEntity = (UserEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "UserEntity":
					SetupSyncUserEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "UserEntity":
					DesetupSyncUserEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			if(_userEntity!=null)
			{
				toReturn.Add(_userEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="userId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="brandId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUserIdBrandId(System.Int32 userId, System.Int32 brandId)
		{
			return FetchUsingUCUserIdBrandId( userId,  brandId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="userId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="brandId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUserIdBrandId(System.Int32 userId, System.Int32 brandId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCUserIdBrandId( userId,  brandId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="userId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="brandId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUserIdBrandId(System.Int32 userId, System.Int32 brandId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCUserIdBrandId( userId,  brandId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="userId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="brandId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUserIdBrandId(System.Int32 userId, System.Int32 brandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((UserBrandDAO)CreateDAOInstance()).FetchUserBrandUsingUCUserIdBrandId(this, this.Transaction, userId, brandId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userBrandId)
		{
			return FetchUsingPK(userBrandId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(userBrandId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(userBrandId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(userBrandId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UserBrandId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserBrandRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId);
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public UserEntity GetSingleUserEntity()
		{
			return GetSingleUserEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public virtual UserEntity GetSingleUserEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserEntity || forceFetch || _alwaysFetchUserEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserEntityUsingUserId);
				UserEntity newEntity = new UserEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserId);
				}
				if(fetchResult)
				{
					newEntity = (UserEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserEntity = newEntity;
				_alreadyFetchedUserEntity = fetchResult;
			}
			return _userEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("UserEntity", _userEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="validator">The validator object for this UserBrandEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 userBrandId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(userBrandId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_brandEntityReturnsNewIfNotFound = true;
			_userEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserBrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Role", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticUserBrandRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "UserBrandCollection", resetFKFields, new int[] { (int)UserBrandFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticUserBrandRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] { "BrandName" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Name":
					this.OnPropertyChanged("BrandName");
					break;
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userEntity, new PropertyChangedEventHandler( OnUserEntityPropertyChanged ), "UserEntity", Obymobi.Data.RelationClasses.StaticUserBrandRelations.UserEntityUsingUserIdStatic, true, signalRelatedEntity, "UserBrandCollection", resetFKFields, new int[] { (int)UserBrandFieldIndex.UserId } );		
			_userEntity = null;
		}
		
		/// <summary> setups the sync logic for member _userEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserEntity(IEntityCore relatedEntity)
		{
			if(_userEntity!=relatedEntity)
			{		
				DesetupSyncUserEntity(true, true);
				_userEntity = (UserEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userEntity, new PropertyChangedEventHandler( OnUserEntityPropertyChanged ), "UserEntity", Obymobi.Data.RelationClasses.StaticUserBrandRelations.UserEntityUsingUserIdStatic, true, ref _alreadyFetchedUserEntity, new string[] { "UserUsername" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Username":
					this.OnPropertyChanged("UserUsername");
					break;
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserBrandFieldIndex.UserBrandId].ForcedCurrentValueWrite(userBrandId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserBrandDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserBrandEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserBrandRelations Relations
		{
			get	{ return new UserBrandRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.UserBrandEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), (IEntityRelation)GetRelationsForField("UserEntity")[0], (int)Obymobi.Data.EntityType.UserBrandEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, null, "UserEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UserBrandId property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."UserBrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UserBrandId
		{
			get { return (System.Int32)GetValue((int)UserBrandFieldIndex.UserBrandId, true); }
			set	{ SetValue((int)UserBrandFieldIndex.UserBrandId, value, true); }
		}

		/// <summary> The UserId property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."UserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UserId
		{
			get { return (System.Int32)GetValue((int)UserBrandFieldIndex.UserId, true); }
			set	{ SetValue((int)UserBrandFieldIndex.UserId, value, true); }
		}

		/// <summary> The BrandId property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BrandId
		{
			get { return (System.Int32)GetValue((int)UserBrandFieldIndex.BrandId, true); }
			set	{ SetValue((int)UserBrandFieldIndex.BrandId, value, true); }
		}

		/// <summary> The Role property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."Role"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.BrandRole Role
		{
			get { return (Obymobi.Enums.BrandRole)GetValue((int)UserBrandFieldIndex.Role, true); }
			set	{ SetValue((int)UserBrandFieldIndex.Role, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)UserBrandFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)UserBrandFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserBrandFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UserBrandFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserBrandFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UserBrandFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UserBrand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UserBrand"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserBrandFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UserBrandFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserBrandCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UserEntity UserEntity
		{
			get	{ return GetSingleUserEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserBrandCollection", "UserEntity", _userEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserEntity. When set to true, UserEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserEntity is accessed. You can always execute a forced fetch by calling GetSingleUserEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserEntity
		{
			get	{ return _alwaysFetchUserEntity; }
			set	{ _alwaysFetchUserEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserEntity already has been fetched. Setting this property to false when UserEntity has been fetched
		/// will set UserEntity to null as well. Setting this property to true while UserEntity hasn't been fetched disables lazy loading for UserEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserEntity
		{
			get { return _alreadyFetchedUserEntity;}
			set 
			{
				if(_alreadyFetchedUserEntity && !value)
				{
					this.UserEntity = null;
				}
				_alreadyFetchedUserEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserEntity is not found
		/// in the database. When set to true, UserEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UserEntityReturnsNewIfNotFound
		{
			get	{ return _userEntityReturnsNewIfNotFound; }
			set { _userEntityReturnsNewIfNotFound = value; }	
		}

 
		/// <summary> Gets the value of the related field this.BrandEntity.Name.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String BrandName
		{
			get
			{
				BrandEntity relatedEntity = this.BrandEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Name;
			}

		}
 
		/// <summary> Gets the value of the related field this.UserEntity.Username.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String UserUsername
		{
			get
			{
				UserEntity relatedEntity = this.UserEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Username;
			}

		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UserBrandEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
