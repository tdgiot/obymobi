﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'OutletOperationalState'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OutletOperationalStateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OutletOperationalStateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.OutletCollection	_outletCollection;
		private bool	_alwaysFetchOutletCollection, _alreadyFetchedOutletCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OutletOperationalStateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OutletOperationalStateEntityBase() :base("OutletOperationalStateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		protected OutletOperationalStateEntityBase(System.Int32 outletOperationalStateId):base("OutletOperationalStateEntity")
		{
			InitClassFetch(outletOperationalStateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OutletOperationalStateEntityBase(System.Int32 outletOperationalStateId, IPrefetchPath prefetchPathToUse): base("OutletOperationalStateEntity")
		{
			InitClassFetch(outletOperationalStateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="validator">The custom validator object for this OutletOperationalStateEntity</param>
		protected OutletOperationalStateEntityBase(System.Int32 outletOperationalStateId, IValidator validator):base("OutletOperationalStateEntity")
		{
			InitClassFetch(outletOperationalStateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletOperationalStateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_outletCollection = (Obymobi.Data.CollectionClasses.OutletCollection)info.GetValue("_outletCollection", typeof(Obymobi.Data.CollectionClasses.OutletCollection));
			_alwaysFetchOutletCollection = info.GetBoolean("_alwaysFetchOutletCollection");
			_alreadyFetchedOutletCollection = info.GetBoolean("_alreadyFetchedOutletCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOutletCollection = (_outletCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OutletCollection":
					toReturn.Add(Relations.OutletEntityUsingOutletOperationalStateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_outletCollection", (!this.MarkedForDeletion?_outletCollection:null));
			info.AddValue("_alwaysFetchOutletCollection", _alwaysFetchOutletCollection);
			info.AddValue("_alreadyFetchedOutletCollection", _alreadyFetchedOutletCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OutletCollection":
					_alreadyFetchedOutletCollection = true;
					if(entity!=null)
					{
						this.OutletCollection.Add((OutletEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OutletCollection":
					_outletCollection.Add((OutletEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OutletCollection":
					this.PerformRelatedEntityRemoval(_outletCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_outletCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletOperationalStateId)
		{
			return FetchUsingPK(outletOperationalStateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletOperationalStateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(outletOperationalStateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletOperationalStateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(outletOperationalStateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletOperationalStateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(outletOperationalStateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OutletOperationalStateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OutletOperationalStateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OutletEntity'</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch)
		{
			return GetMultiOutletCollection(forceFetch, _outletCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OutletEntity'</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOutletCollection(forceFetch, _outletCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOutletCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOutletCollection || forceFetch || _alwaysFetchOutletCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_outletCollection);
				_outletCollection.SuppressClearInGetMulti=!forceFetch;
				_outletCollection.EntityFactoryToUse = entityFactoryToUse;
				_outletCollection.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_outletCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOutletCollection = true;
			}
			return _outletCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OutletCollection'. These settings will be taken into account
		/// when the property OutletCollection is requested or GetMultiOutletCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOutletCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_outletCollection.SortClauses=sortClauses;
			_outletCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OutletCollection", _outletCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="validator">The validator object for this OutletOperationalStateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 outletOperationalStateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(outletOperationalStateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_outletCollection = new Obymobi.Data.CollectionClasses.OutletCollection();
			_outletCollection.SetContainingEntityInfo(this, "OutletOperationalStateEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletOperationalStateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WaitTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderIntakeDisabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="outletOperationalStateId">PK value for OutletOperationalState which data should be fetched into this OutletOperationalState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 outletOperationalStateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OutletOperationalStateFieldIndex.OutletOperationalStateId].ForcedCurrentValueWrite(outletOperationalStateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOutletOperationalStateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OutletOperationalStateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OutletOperationalStateRelations Relations
		{
			get	{ return new OutletOperationalStateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletCollection")[0], (int)Obymobi.Data.EntityType.OutletOperationalStateEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OutletOperationalStateId property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."OutletOperationalStateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletOperationalStateId
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.OutletOperationalStateId, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.OutletOperationalStateId, value, true); }
		}

		/// <summary> The WaitTime property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."WaitTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Time, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.TimeSpan> WaitTime
		{
			get { return (Nullable<System.TimeSpan>)GetValue((int)OutletOperationalStateFieldIndex.WaitTime, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.WaitTime, value, true); }
		}

		/// <summary> The OrderIntakeDisabled property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."OrderIntakeDisabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderIntakeDisabled
		{
			get { return (System.Boolean)GetValue((int)OutletOperationalStateFieldIndex.OrderIntakeDisabled, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.OrderIntakeDisabled, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletOperationalStateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletOperationalStateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity OutletOperationalState<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletOperationalState"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OutletOperationalStateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OutletOperationalStateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOutletCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OutletCollection OutletCollection
		{
			get	{ return GetMultiOutletCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OutletCollection. When set to true, OutletCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOutletCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletCollection
		{
			get	{ return _alwaysFetchOutletCollection; }
			set	{ _alwaysFetchOutletCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletCollection already has been fetched. Setting this property to false when OutletCollection has been fetched
		/// will clear the OutletCollection collection well. Setting this property to true while OutletCollection hasn't been fetched disables lazy loading for OutletCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletCollection
		{
			get { return _alreadyFetchedOutletCollection;}
			set 
			{
				if(_alreadyFetchedOutletCollection && !value && (_outletCollection != null))
				{
					_outletCollection.Clear();
				}
				_alreadyFetchedOutletCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OutletOperationalStateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
