﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'FolioItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class FolioItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "FolioItemEntity"; }
		}
	
		#region Class Member Declarations
		private FolioEntity _folioEntity;
		private bool	_alwaysFetchFolioEntity, _alreadyFetchedFolioEntity, _folioEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FolioEntity</summary>
			public static readonly string FolioEntity = "FolioEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FolioItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected FolioItemEntityBase() :base("FolioItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		protected FolioItemEntityBase(System.Int32 folioItemId):base("FolioItemEntity")
		{
			InitClassFetch(folioItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected FolioItemEntityBase(System.Int32 folioItemId, IPrefetchPath prefetchPathToUse): base("FolioItemEntity")
		{
			InitClassFetch(folioItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="validator">The custom validator object for this FolioItemEntity</param>
		protected FolioItemEntityBase(System.Int32 folioItemId, IValidator validator):base("FolioItemEntity")
		{
			InitClassFetch(folioItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FolioItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_folioEntity = (FolioEntity)info.GetValue("_folioEntity", typeof(FolioEntity));
			if(_folioEntity!=null)
			{
				_folioEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_folioEntityReturnsNewIfNotFound = info.GetBoolean("_folioEntityReturnsNewIfNotFound");
			_alwaysFetchFolioEntity = info.GetBoolean("_alwaysFetchFolioEntity");
			_alreadyFetchedFolioEntity = info.GetBoolean("_alreadyFetchedFolioEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FolioItemFieldIndex)fieldIndex)
			{
				case FolioItemFieldIndex.FolioId:
					DesetupSyncFolioEntity(true, false);
					_alreadyFetchedFolioEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFolioEntity = (_folioEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FolioEntity":
					toReturn.Add(Relations.FolioEntityUsingFolioId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_folioEntity", (!this.MarkedForDeletion?_folioEntity:null));
			info.AddValue("_folioEntityReturnsNewIfNotFound", _folioEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFolioEntity", _alwaysFetchFolioEntity);
			info.AddValue("_alreadyFetchedFolioEntity", _alreadyFetchedFolioEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FolioEntity":
					_alreadyFetchedFolioEntity = true;
					this.FolioEntity = (FolioEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FolioEntity":
					SetupSyncFolioEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FolioEntity":
					DesetupSyncFolioEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_folioEntity!=null)
			{
				toReturn.Add(_folioEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 folioItemId)
		{
			return FetchUsingPK(folioItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 folioItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(folioItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 folioItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(folioItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 folioItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(folioItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FolioItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FolioItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'FolioEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FolioEntity' which is related to this entity.</returns>
		public FolioEntity GetSingleFolioEntity()
		{
			return GetSingleFolioEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'FolioEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FolioEntity' which is related to this entity.</returns>
		public virtual FolioEntity GetSingleFolioEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedFolioEntity || forceFetch || _alwaysFetchFolioEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FolioEntityUsingFolioId);
				FolioEntity newEntity = new FolioEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FolioId);
				}
				if(fetchResult)
				{
					newEntity = (FolioEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_folioEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FolioEntity = newEntity;
				_alreadyFetchedFolioEntity = fetchResult;
			}
			return _folioEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FolioEntity", _folioEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="validator">The validator object for this FolioItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 folioItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(folioItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_folioEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FolioItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FolioId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _folioEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFolioEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _folioEntity, new PropertyChangedEventHandler( OnFolioEntityPropertyChanged ), "FolioEntity", Obymobi.Data.RelationClasses.StaticFolioItemRelations.FolioEntityUsingFolioIdStatic, true, signalRelatedEntity, "FolioItemCollection", resetFKFields, new int[] { (int)FolioItemFieldIndex.FolioId } );		
			_folioEntity = null;
		}
		
		/// <summary> setups the sync logic for member _folioEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFolioEntity(IEntityCore relatedEntity)
		{
			if(_folioEntity!=relatedEntity)
			{		
				DesetupSyncFolioEntity(true, true);
				_folioEntity = (FolioEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _folioEntity, new PropertyChangedEventHandler( OnFolioEntityPropertyChanged ), "FolioEntity", Obymobi.Data.RelationClasses.StaticFolioItemRelations.FolioEntityUsingFolioIdStatic, true, ref _alreadyFetchedFolioEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFolioEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="folioItemId">PK value for FolioItem which data should be fetched into this FolioItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 folioItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FolioItemFieldIndex.FolioItemId].ForcedCurrentValueWrite(folioItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFolioItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FolioItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FolioItemRelations Relations
		{
			get	{ return new FolioItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Folio'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFolioEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.FolioCollection(), (IEntityRelation)GetRelationsForField("FolioEntity")[0], (int)Obymobi.Data.EntityType.FolioItemEntity, (int)Obymobi.Data.EntityType.FolioEntity, 0, null, null, null, "FolioEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FolioItemId property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."FolioItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 FolioItemId
		{
			get { return (System.Int32)GetValue((int)FolioItemFieldIndex.FolioItemId, true); }
			set	{ SetValue((int)FolioItemFieldIndex.FolioItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)FolioItemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)FolioItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The FolioId property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."FolioId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FolioId
		{
			get { return (System.Int32)GetValue((int)FolioItemFieldIndex.FolioId, true); }
			set	{ SetValue((int)FolioItemFieldIndex.FolioId, value, true); }
		}

		/// <summary> The Code property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)FolioItemFieldIndex.Code, true); }
			set	{ SetValue((int)FolioItemFieldIndex.Code, value, true); }
		}

		/// <summary> The Description property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FolioItemFieldIndex.Description, true); }
			set	{ SetValue((int)FolioItemFieldIndex.Description, value, true); }
		}

		/// <summary> The PriceIn property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceIn
		{
			get { return (System.Decimal)GetValue((int)FolioItemFieldIndex.PriceIn, true); }
			set	{ SetValue((int)FolioItemFieldIndex.PriceIn, value, true); }
		}

		/// <summary> The CurrencyCode property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."CurrencyCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)FolioItemFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)FolioItemFieldIndex.CurrencyCode, value, true); }
		}

		/// <summary> The ReferenceId property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."ReferenceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReferenceId
		{
			get { return (System.String)GetValue((int)FolioItemFieldIndex.ReferenceId, true); }
			set	{ SetValue((int)FolioItemFieldIndex.ReferenceId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)FolioItemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)FolioItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)FolioItemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)FolioItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FolioItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)FolioItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity FolioItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FolioItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FolioItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)FolioItemFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'FolioEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFolioEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual FolioEntity FolioEntity
		{
			get	{ return GetSingleFolioEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFolioEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FolioItemCollection", "FolioEntity", _folioEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FolioEntity. When set to true, FolioEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FolioEntity is accessed. You can always execute a forced fetch by calling GetSingleFolioEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFolioEntity
		{
			get	{ return _alwaysFetchFolioEntity; }
			set	{ _alwaysFetchFolioEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FolioEntity already has been fetched. Setting this property to false when FolioEntity has been fetched
		/// will set FolioEntity to null as well. Setting this property to true while FolioEntity hasn't been fetched disables lazy loading for FolioEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFolioEntity
		{
			get { return _alreadyFetchedFolioEntity;}
			set 
			{
				if(_alreadyFetchedFolioEntity && !value)
				{
					this.FolioEntity = null;
				}
				_alreadyFetchedFolioEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FolioEntity is not found
		/// in the database. When set to true, FolioEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool FolioEntityReturnsNewIfNotFound
		{
			get	{ return _folioEntityReturnsNewIfNotFound; }
			set { _folioEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.FolioItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
