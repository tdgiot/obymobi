﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Posproduct'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PosproductEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PosproductEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection	_alterationoptionCollection;
		private bool	_alwaysFetchAlterationoptionCollection, _alreadyFetchedAlterationoptionCollection;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.GenericalterationoptionCollection _genericalterationoptionCollectionViaAlterationoption;
		private bool	_alwaysFetchGenericalterationoptionCollectionViaAlterationoption, _alreadyFetchedGenericalterationoptionCollectionViaAlterationoption;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaProduct;
		private bool	_alwaysFetchRouteCollectionViaProduct, _alreadyFetchedRouteCollectionViaProduct;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name GenericalterationoptionCollectionViaAlterationoption</summary>
			public static readonly string GenericalterationoptionCollectionViaAlterationoption = "GenericalterationoptionCollectionViaAlterationoption";
			/// <summary>Member name RouteCollectionViaProduct</summary>
			public static readonly string RouteCollectionViaProduct = "RouteCollectionViaProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PosproductEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PosproductEntityBase() :base("PosproductEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		protected PosproductEntityBase(System.Int32 posproductId):base("PosproductEntity")
		{
			InitClassFetch(posproductId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PosproductEntityBase(System.Int32 posproductId, IPrefetchPath prefetchPathToUse): base("PosproductEntity")
		{
			InitClassFetch(posproductId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="validator">The custom validator object for this PosproductEntity</param>
		protected PosproductEntityBase(System.Int32 posproductId, IValidator validator):base("PosproductEntity")
		{
			InitClassFetch(posproductId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PosproductEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationoptionCollection = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollection = info.GetBoolean("_alwaysFetchAlterationoptionCollection");
			_alreadyFetchedAlterationoptionCollection = info.GetBoolean("_alreadyFetchedAlterationoptionCollection");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");
			_genericalterationoptionCollectionViaAlterationoption = (Obymobi.Data.CollectionClasses.GenericalterationoptionCollection)info.GetValue("_genericalterationoptionCollectionViaAlterationoption", typeof(Obymobi.Data.CollectionClasses.GenericalterationoptionCollection));
			_alwaysFetchGenericalterationoptionCollectionViaAlterationoption = info.GetBoolean("_alwaysFetchGenericalterationoptionCollectionViaAlterationoption");
			_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption = info.GetBoolean("_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption");

			_routeCollectionViaProduct = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaProduct = info.GetBoolean("_alwaysFetchRouteCollectionViaProduct");
			_alreadyFetchedRouteCollectionViaProduct = info.GetBoolean("_alreadyFetchedRouteCollectionViaProduct");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PosproductFieldIndex)fieldIndex)
			{
				case PosproductFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationoptionCollection = (_alterationoptionCollection.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption = (_genericalterationoptionCollectionViaAlterationoption.Count > 0);
			_alreadyFetchedRouteCollectionViaProduct = (_routeCollectionViaProduct.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "AlterationoptionCollection":
					toReturn.Add(Relations.AlterationoptionEntityUsingPosproductId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingPosproductId);
					break;
				case "GenericalterationoptionCollectionViaAlterationoption":
					toReturn.Add(Relations.AlterationoptionEntityUsingPosproductId, "PosproductEntity__", "Alterationoption_", JoinHint.None);
					toReturn.Add(AlterationoptionEntity.Relations.GenericalterationoptionEntityUsingGenericalterationoptionId, "Alterationoption_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingPosproductId, "PosproductEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.RouteEntityUsingRouteId, "Product_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationoptionCollection", (!this.MarkedForDeletion?_alterationoptionCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionCollection", _alwaysFetchAlterationoptionCollection);
			info.AddValue("_alreadyFetchedAlterationoptionCollection", _alreadyFetchedAlterationoptionCollection);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_genericalterationoptionCollectionViaAlterationoption", (!this.MarkedForDeletion?_genericalterationoptionCollectionViaAlterationoption:null));
			info.AddValue("_alwaysFetchGenericalterationoptionCollectionViaAlterationoption", _alwaysFetchGenericalterationoptionCollectionViaAlterationoption);
			info.AddValue("_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption", _alreadyFetchedGenericalterationoptionCollectionViaAlterationoption);
			info.AddValue("_routeCollectionViaProduct", (!this.MarkedForDeletion?_routeCollectionViaProduct:null));
			info.AddValue("_alwaysFetchRouteCollectionViaProduct", _alwaysFetchRouteCollectionViaProduct);
			info.AddValue("_alreadyFetchedRouteCollectionViaProduct", _alreadyFetchedRouteCollectionViaProduct);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "AlterationoptionCollection":
					_alreadyFetchedAlterationoptionCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionCollection.Add((AlterationoptionEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "GenericalterationoptionCollectionViaAlterationoption":
					_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption = true;
					if(entity!=null)
					{
						this.GenericalterationoptionCollectionViaAlterationoption.Add((GenericalterationoptionEntity)entity);
					}
					break;
				case "RouteCollectionViaProduct":
					_alreadyFetchedRouteCollectionViaProduct = true;
					if(entity!=null)
					{
						this.RouteCollectionViaProduct.Add((RouteEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "AlterationoptionCollection":
					_alterationoptionCollection.Add((AlterationoptionEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "AlterationoptionCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationoptionCollection);
			toReturn.Add(_productCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posproductId)
		{
			return FetchUsingPK(posproductId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posproductId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(posproductId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posproductId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(posproductId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posproductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(posproductId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PosproductId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PosproductRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollection || forceFetch || _alwaysFetchAlterationoptionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollection);
				_alterationoptionCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_alterationoptionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollection = true;
			}
			return _alterationoptionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollection'. These settings will be taken into account
		/// when the property AlterationoptionCollection is requested or GetMultiAlterationoptionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollection.SortClauses=sortClauses;
			_alterationoptionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GetMultiGenericalterationoptionCollectionViaAlterationoption(bool forceFetch)
		{
			return GetMultiGenericalterationoptionCollectionViaAlterationoption(forceFetch, _genericalterationoptionCollectionViaAlterationoption.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GetMultiGenericalterationoptionCollectionViaAlterationoption(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption || forceFetch || _alwaysFetchGenericalterationoptionCollectionViaAlterationoption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationoptionCollectionViaAlterationoption);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PosproductFields.PosproductId, ComparisonOperator.Equal, this.PosproductId, "PosproductEntity__"));
				_genericalterationoptionCollectionViaAlterationoption.SuppressClearInGetMulti=!forceFetch;
				_genericalterationoptionCollectionViaAlterationoption.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationoptionCollectionViaAlterationoption.GetMulti(filter, GetRelationsForField("GenericalterationoptionCollectionViaAlterationoption"));
				_genericalterationoptionCollectionViaAlterationoption.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption = true;
			}
			return _genericalterationoptionCollectionViaAlterationoption;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationoptionCollectionViaAlterationoption'. These settings will be taken into account
		/// when the property GenericalterationoptionCollectionViaAlterationoption is requested or GetMultiGenericalterationoptionCollectionViaAlterationoption is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationoptionCollectionViaAlterationoption(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationoptionCollectionViaAlterationoption.SortClauses=sortClauses;
			_genericalterationoptionCollectionViaAlterationoption.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch)
		{
			return GetMultiRouteCollectionViaProduct(forceFetch, _routeCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaProduct || forceFetch || _alwaysFetchRouteCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PosproductFields.PosproductId, ComparisonOperator.Equal, this.PosproductId, "PosproductEntity__"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaProduct.GetMulti(filter, GetRelationsForField("RouteCollectionViaProduct"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaProduct = true;
			}
			return _routeCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaProduct'. These settings will be taken into account
		/// when the property RouteCollectionViaProduct is requested or GetMultiRouteCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaProduct.SortClauses=sortClauses;
			_routeCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("AlterationoptionCollection", _alterationoptionCollection);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("GenericalterationoptionCollectionViaAlterationoption", _genericalterationoptionCollectionViaAlterationoption);
			toReturn.Add("RouteCollectionViaProduct", _routeCollectionViaProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="validator">The validator object for this PosproductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 posproductId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(posproductId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationoptionCollection = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_alterationoptionCollection.SetContainingEntityInfo(this, "PosproductEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "PosproductEntity");
			_genericalterationoptionCollectionViaAlterationoption = new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection();
			_routeCollectionViaProduct = new Obymobi.Data.CollectionClasses.RouteCollection();
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosproductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalPoscategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatTariff", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronisationBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueCenter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPosproductRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PosproductCollection", resetFKFields, new int[] { (int)PosproductFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPosproductRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 posproductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PosproductFieldIndex.PosproductId].ForcedCurrentValueWrite(posproductId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePosproductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PosproductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PosproductRelations Relations
		{
			get	{ return new PosproductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionCollection")[0], (int)Obymobi.Data.EntityType.PosproductEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.PosproductEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationoptionCollectionViaAlterationoption
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationoptionEntityUsingPosproductId;
				intermediateRelation.SetAliases(string.Empty, "Alterationoption_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PosproductEntity, (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, 0, null, null, GetRelationsForField("GenericalterationoptionCollectionViaAlterationoption"), "GenericalterationoptionCollectionViaAlterationoption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingPosproductId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PosproductEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaProduct"), "RouteCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PosproductEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PosproductId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."PosproductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PosproductId
		{
			get { return (System.Int32)GetValue((int)PosproductFieldIndex.PosproductId, true); }
			set	{ SetValue((int)PosproductFieldIndex.PosproductId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PosproductFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PosproductFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalId
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.ExternalId, true); }
			set	{ SetValue((int)PosproductFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The ExternalPoscategoryId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."ExternalPoscategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalPoscategoryId
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.ExternalPoscategoryId, true); }
			set	{ SetValue((int)PosproductFieldIndex.ExternalPoscategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.Name, true); }
			set	{ SetValue((int)PosproductFieldIndex.Name, value, true); }
		}

		/// <summary> The PriceIn property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceIn
		{
			get { return (System.Decimal)GetValue((int)PosproductFieldIndex.PriceIn, true); }
			set	{ SetValue((int)PosproductFieldIndex.PriceIn, value, true); }
		}

		/// <summary> The VatTariff property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."VatTariff"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VatTariff
		{
			get { return (System.Int32)GetValue((int)PosproductFieldIndex.VatTariff, true); }
			set	{ SetValue((int)PosproductFieldIndex.VatTariff, value, true); }
		}

		/// <summary> The Visible property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)PosproductFieldIndex.Visible, true); }
			set	{ SetValue((int)PosproductFieldIndex.Visible, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)PosproductFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The CreatedInBatchId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."CreatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosproductFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)PosproductFieldIndex.CreatedInBatchId, value, true); }
		}

		/// <summary> The UpdatedInBatchId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."UpdatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosproductFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)PosproductFieldIndex.UpdatedInBatchId, value, true); }
		}

		/// <summary> The SynchronisationBatchId property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."SynchronisationBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosproductFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)PosproductFieldIndex.SynchronisationBatchId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PosproductFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PosproductFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PosproductFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PosproductFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PosproductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PosproductFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PosproductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PosproductFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The RevenueCenter property of the Entity Posproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posproduct"."RevenueCenter"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RevenueCenter
		{
			get { return (System.String)GetValue((int)PosproductFieldIndex.RevenueCenter, true); }
			set	{ SetValue((int)PosproductFieldIndex.RevenueCenter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollection
		{
			get	{ return GetMultiAlterationoptionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollection. When set to true, AlterationoptionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollection
		{
			get	{ return _alwaysFetchAlterationoptionCollection; }
			set	{ _alwaysFetchAlterationoptionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollection already has been fetched. Setting this property to false when AlterationoptionCollection has been fetched
		/// will clear the AlterationoptionCollection collection well. Setting this property to true while AlterationoptionCollection hasn't been fetched disables lazy loading for AlterationoptionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollection
		{
			get { return _alreadyFetchedAlterationoptionCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollection && !value && (_alterationoptionCollection != null))
				{
					_alterationoptionCollection.Clear();
				}
				_alreadyFetchedAlterationoptionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationoptionCollectionViaAlterationoption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GenericalterationoptionCollectionViaAlterationoption
		{
			get { return GetMultiGenericalterationoptionCollectionViaAlterationoption(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationoptionCollectionViaAlterationoption. When set to true, GenericalterationoptionCollectionViaAlterationoption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationoptionCollectionViaAlterationoption is accessed. You can always execute a forced fetch by calling GetMultiGenericalterationoptionCollectionViaAlterationoption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationoptionCollectionViaAlterationoption
		{
			get	{ return _alwaysFetchGenericalterationoptionCollectionViaAlterationoption; }
			set	{ _alwaysFetchGenericalterationoptionCollectionViaAlterationoption = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationoptionCollectionViaAlterationoption already has been fetched. Setting this property to false when GenericalterationoptionCollectionViaAlterationoption has been fetched
		/// will clear the GenericalterationoptionCollectionViaAlterationoption collection well. Setting this property to true while GenericalterationoptionCollectionViaAlterationoption hasn't been fetched disables lazy loading for GenericalterationoptionCollectionViaAlterationoption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationoptionCollectionViaAlterationoption
		{
			get { return _alreadyFetchedGenericalterationoptionCollectionViaAlterationoption;}
			set 
			{
				if(_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption && !value && (_genericalterationoptionCollectionViaAlterationoption != null))
				{
					_genericalterationoptionCollectionViaAlterationoption.Clear();
				}
				_alreadyFetchedGenericalterationoptionCollectionViaAlterationoption = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaProduct
		{
			get { return GetMultiRouteCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaProduct. When set to true, RouteCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaProduct
		{
			get	{ return _alwaysFetchRouteCollectionViaProduct; }
			set	{ _alwaysFetchRouteCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaProduct already has been fetched. Setting this property to false when RouteCollectionViaProduct has been fetched
		/// will clear the RouteCollectionViaProduct collection well. Setting this property to true while RouteCollectionViaProduct hasn't been fetched disables lazy loading for RouteCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaProduct
		{
			get { return _alreadyFetchedRouteCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaProduct && !value && (_routeCollectionViaProduct != null))
				{
					_routeCollectionViaProduct.Clear();
				}
				_alreadyFetchedRouteCollectionViaProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PosproductCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PosproductEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
