﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Messagegroup'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MessagegroupEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MessagegroupEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection	_messagegroupDeliverypointCollection;
		private bool	_alwaysFetchMessagegroupDeliverypointCollection, _alreadyFetchedMessagegroupDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.PmsActionRuleCollection	_pmsActionRuleCollection;
		private bool	_alwaysFetchPmsActionRuleCollection, _alreadyFetchedPmsActionRuleCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection	_scheduledMessageHistoryCollection;
		private bool	_alwaysFetchScheduledMessageHistoryCollection, _alreadyFetchedScheduledMessageHistoryCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection	_uIScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchUIScheduleItemOccurrenceCollection, _alreadyFetchedUIScheduleItemOccurrenceCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessagegroupDeliverypoint;
		private bool	_alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint, _alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name MessagegroupDeliverypointCollection</summary>
			public static readonly string MessagegroupDeliverypointCollection = "MessagegroupDeliverypointCollection";
			/// <summary>Member name PmsActionRuleCollection</summary>
			public static readonly string PmsActionRuleCollection = "PmsActionRuleCollection";
			/// <summary>Member name ScheduledMessageHistoryCollection</summary>
			public static readonly string ScheduledMessageHistoryCollection = "ScheduledMessageHistoryCollection";
			/// <summary>Member name UIScheduleItemOccurrenceCollection</summary>
			public static readonly string UIScheduleItemOccurrenceCollection = "UIScheduleItemOccurrenceCollection";
			/// <summary>Member name DeliverypointCollectionViaMessagegroupDeliverypoint</summary>
			public static readonly string DeliverypointCollectionViaMessagegroupDeliverypoint = "DeliverypointCollectionViaMessagegroupDeliverypoint";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessagegroupEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MessagegroupEntityBase() :base("MessagegroupEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		protected MessagegroupEntityBase(System.Int32 messagegroupId):base("MessagegroupEntity")
		{
			InitClassFetch(messagegroupId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MessagegroupEntityBase(System.Int32 messagegroupId, IPrefetchPath prefetchPathToUse): base("MessagegroupEntity")
		{
			InitClassFetch(messagegroupId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="validator">The custom validator object for this MessagegroupEntity</param>
		protected MessagegroupEntityBase(System.Int32 messagegroupId, IValidator validator):base("MessagegroupEntity")
		{
			InitClassFetch(messagegroupId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessagegroupEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_messagegroupDeliverypointCollection = (Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection)info.GetValue("_messagegroupDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection));
			_alwaysFetchMessagegroupDeliverypointCollection = info.GetBoolean("_alwaysFetchMessagegroupDeliverypointCollection");
			_alreadyFetchedMessagegroupDeliverypointCollection = info.GetBoolean("_alreadyFetchedMessagegroupDeliverypointCollection");

			_pmsActionRuleCollection = (Obymobi.Data.CollectionClasses.PmsActionRuleCollection)info.GetValue("_pmsActionRuleCollection", typeof(Obymobi.Data.CollectionClasses.PmsActionRuleCollection));
			_alwaysFetchPmsActionRuleCollection = info.GetBoolean("_alwaysFetchPmsActionRuleCollection");
			_alreadyFetchedPmsActionRuleCollection = info.GetBoolean("_alreadyFetchedPmsActionRuleCollection");

			_scheduledMessageHistoryCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection)info.GetValue("_scheduledMessageHistoryCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection));
			_alwaysFetchScheduledMessageHistoryCollection = info.GetBoolean("_alwaysFetchScheduledMessageHistoryCollection");
			_alreadyFetchedScheduledMessageHistoryCollection = info.GetBoolean("_alreadyFetchedScheduledMessageHistoryCollection");

			_uIScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection)info.GetValue("_uIScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection));
			_alwaysFetchUIScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchUIScheduleItemOccurrenceCollection");
			_alreadyFetchedUIScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemOccurrenceCollection");
			_deliverypointCollectionViaMessagegroupDeliverypoint = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessagegroupDeliverypoint", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint");
			_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MessagegroupFieldIndex)fieldIndex)
			{
				case MessagegroupFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMessagegroupDeliverypointCollection = (_messagegroupDeliverypointCollection.Count > 0);
			_alreadyFetchedPmsActionRuleCollection = (_pmsActionRuleCollection.Count > 0);
			_alreadyFetchedScheduledMessageHistoryCollection = (_scheduledMessageHistoryCollection.Count > 0);
			_alreadyFetchedUIScheduleItemOccurrenceCollection = (_uIScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint = (_deliverypointCollectionViaMessagegroupDeliverypoint.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "MessagegroupDeliverypointCollection":
					toReturn.Add(Relations.MessagegroupDeliverypointEntityUsingMessagegroupId);
					break;
				case "PmsActionRuleCollection":
					toReturn.Add(Relations.PmsActionRuleEntityUsingMessagegroupId);
					break;
				case "ScheduledMessageHistoryCollection":
					toReturn.Add(Relations.ScheduledMessageHistoryEntityUsingMessagegroupId);
					break;
				case "UIScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingMessagegroupId);
					break;
				case "DeliverypointCollectionViaMessagegroupDeliverypoint":
					toReturn.Add(Relations.MessagegroupDeliverypointEntityUsingMessagegroupId, "MessagegroupEntity__", "MessagegroupDeliverypoint_", JoinHint.None);
					toReturn.Add(MessagegroupDeliverypointEntity.Relations.DeliverypointEntityUsingDeliverypointId, "MessagegroupDeliverypoint_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_messagegroupDeliverypointCollection", (!this.MarkedForDeletion?_messagegroupDeliverypointCollection:null));
			info.AddValue("_alwaysFetchMessagegroupDeliverypointCollection", _alwaysFetchMessagegroupDeliverypointCollection);
			info.AddValue("_alreadyFetchedMessagegroupDeliverypointCollection", _alreadyFetchedMessagegroupDeliverypointCollection);
			info.AddValue("_pmsActionRuleCollection", (!this.MarkedForDeletion?_pmsActionRuleCollection:null));
			info.AddValue("_alwaysFetchPmsActionRuleCollection", _alwaysFetchPmsActionRuleCollection);
			info.AddValue("_alreadyFetchedPmsActionRuleCollection", _alreadyFetchedPmsActionRuleCollection);
			info.AddValue("_scheduledMessageHistoryCollection", (!this.MarkedForDeletion?_scheduledMessageHistoryCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageHistoryCollection", _alwaysFetchScheduledMessageHistoryCollection);
			info.AddValue("_alreadyFetchedScheduledMessageHistoryCollection", _alreadyFetchedScheduledMessageHistoryCollection);
			info.AddValue("_uIScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_uIScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemOccurrenceCollection", _alwaysFetchUIScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemOccurrenceCollection", _alreadyFetchedUIScheduleItemOccurrenceCollection);
			info.AddValue("_deliverypointCollectionViaMessagegroupDeliverypoint", (!this.MarkedForDeletion?_deliverypointCollectionViaMessagegroupDeliverypoint:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint", _alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint", _alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "MessagegroupDeliverypointCollection":
					_alreadyFetchedMessagegroupDeliverypointCollection = true;
					if(entity!=null)
					{
						this.MessagegroupDeliverypointCollection.Add((MessagegroupDeliverypointEntity)entity);
					}
					break;
				case "PmsActionRuleCollection":
					_alreadyFetchedPmsActionRuleCollection = true;
					if(entity!=null)
					{
						this.PmsActionRuleCollection.Add((PmsActionRuleEntity)entity);
					}
					break;
				case "ScheduledMessageHistoryCollection":
					_alreadyFetchedScheduledMessageHistoryCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)entity);
					}
					break;
				case "UIScheduleItemOccurrenceCollection":
					_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessagegroupDeliverypoint":
					_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessagegroupDeliverypoint.Add((DeliverypointEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "MessagegroupDeliverypointCollection":
					_messagegroupDeliverypointCollection.Add((MessagegroupDeliverypointEntity)relatedEntity);
					break;
				case "PmsActionRuleCollection":
					_pmsActionRuleCollection.Add((PmsActionRuleEntity)relatedEntity);
					break;
				case "ScheduledMessageHistoryCollection":
					_scheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)relatedEntity);
					break;
				case "UIScheduleItemOccurrenceCollection":
					_uIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "MessagegroupDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_messagegroupDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PmsActionRuleCollection":
					this.PerformRelatedEntityRemoval(_pmsActionRuleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageHistoryCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_messagegroupDeliverypointCollection);
			toReturn.Add(_pmsActionRuleCollection);
			toReturn.Add(_scheduledMessageHistoryCollection);
			toReturn.Add(_uIScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupId)
		{
			return FetchUsingPK(messagegroupId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messagegroupId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messagegroupId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messagegroupId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessagegroupId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessagegroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessagegroupDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, _messagegroupDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessagegroupDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, _messagegroupDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessagegroupDeliverypointCollection || forceFetch || _alwaysFetchMessagegroupDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messagegroupDeliverypointCollection);
				_messagegroupDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_messagegroupDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_messagegroupDeliverypointCollection.GetMultiManyToOne(null, this, filter);
				_messagegroupDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessagegroupDeliverypointCollection = true;
			}
			return _messagegroupDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessagegroupDeliverypointCollection'. These settings will be taken into account
		/// when the property MessagegroupDeliverypointCollection is requested or GetMultiMessagegroupDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessagegroupDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messagegroupDeliverypointCollection.SortClauses=sortClauses;
			_messagegroupDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PmsActionRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, _pmsActionRuleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PmsActionRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, _pmsActionRuleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPmsActionRuleCollection || forceFetch || _alwaysFetchPmsActionRuleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pmsActionRuleCollection);
				_pmsActionRuleCollection.SuppressClearInGetMulti=!forceFetch;
				_pmsActionRuleCollection.EntityFactoryToUse = entityFactoryToUse;
				_pmsActionRuleCollection.GetMultiManyToOne(null, this, null, null, filter);
				_pmsActionRuleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPmsActionRuleCollection = true;
			}
			return _pmsActionRuleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PmsActionRuleCollection'. These settings will be taken into account
		/// when the property PmsActionRuleCollection is requested or GetMultiPmsActionRuleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPmsActionRuleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pmsActionRuleCollection.SortClauses=sortClauses;
			_pmsActionRuleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageHistoryCollection || forceFetch || _alwaysFetchScheduledMessageHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageHistoryCollection);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageHistoryCollection.GetMultiManyToOne(null, this, null, null, filter);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageHistoryCollection = true;
			}
			return _scheduledMessageHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageHistoryCollection'. These settings will be taken into account
		/// when the property ScheduledMessageHistoryCollection is requested or GetMultiScheduledMessageHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageHistoryCollection.SortClauses=sortClauses;
			_scheduledMessageHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchUIScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemOccurrenceCollection);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemOccurrenceCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
			}
			return _uIScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property UIScheduleItemOccurrenceCollection is requested or GetMultiUIScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_uIScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint(forceFetch, _deliverypointCollectionViaMessagegroupDeliverypoint.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint || forceFetch || _alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessagegroupDeliverypoint);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MessagegroupFields.MessagegroupId, ComparisonOperator.Equal, this.MessagegroupId, "MessagegroupEntity__"));
				_deliverypointCollectionViaMessagegroupDeliverypoint.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessagegroupDeliverypoint.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessagegroupDeliverypoint.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessagegroupDeliverypoint"));
				_deliverypointCollectionViaMessagegroupDeliverypoint.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint = true;
			}
			return _deliverypointCollectionViaMessagegroupDeliverypoint;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessagegroupDeliverypoint'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessagegroupDeliverypoint is requested or GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessagegroupDeliverypoint(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessagegroupDeliverypoint.SortClauses=sortClauses;
			_deliverypointCollectionViaMessagegroupDeliverypoint.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("MessagegroupDeliverypointCollection", _messagegroupDeliverypointCollection);
			toReturn.Add("PmsActionRuleCollection", _pmsActionRuleCollection);
			toReturn.Add("ScheduledMessageHistoryCollection", _scheduledMessageHistoryCollection);
			toReturn.Add("UIScheduleItemOccurrenceCollection", _uIScheduleItemOccurrenceCollection);
			toReturn.Add("DeliverypointCollectionViaMessagegroupDeliverypoint", _deliverypointCollectionViaMessagegroupDeliverypoint);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="validator">The validator object for this MessagegroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 messagegroupId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messagegroupId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_messagegroupDeliverypointCollection = new Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection();
			_messagegroupDeliverypointCollection.SetContainingEntityInfo(this, "MessagegroupEntity");

			_pmsActionRuleCollection = new Obymobi.Data.CollectionClasses.PmsActionRuleCollection();
			_pmsActionRuleCollection.SetContainingEntityInfo(this, "MessagegroupEntity");

			_scheduledMessageHistoryCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection();
			_scheduledMessageHistoryCollection.SetContainingEntityInfo(this, "MessagegroupEntity");

			_uIScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection();
			_uIScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "MessagegroupEntity");
			_deliverypointCollectionViaMessagegroupDeliverypoint = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagegroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessagegroupRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "MessagegroupCollection", resetFKFields, new int[] { (int)MessagegroupFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessagegroupRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messagegroupId">PK value for Messagegroup which data should be fetched into this Messagegroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 messagegroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessagegroupFieldIndex.MessagegroupId].ForcedCurrentValueWrite(messagegroupId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessagegroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessagegroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessagegroupRelations Relations
		{
			get	{ return new MessagegroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessagegroupDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection(), (IEntityRelation)GetRelationsForField("MessagegroupDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.MessagegroupDeliverypointEntity, 0, null, null, null, "MessagegroupDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsActionRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsActionRuleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsActionRuleCollection(), (IEntityRelation)GetRelationsForField("PmsActionRuleCollection")[0], (int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.PmsActionRuleEntity, 0, null, null, null, "PmsActionRuleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessageHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageHistoryCollection")[0], (int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.ScheduledMessageHistoryEntity, 0, null, null, null, "ScheduledMessageHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "UIScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessagegroupDeliverypoint
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessagegroupDeliverypointEntityUsingMessagegroupId;
				intermediateRelation.SetAliases(string.Empty, "MessagegroupDeliverypoint_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessagegroupDeliverypoint"), "DeliverypointCollectionViaMessagegroupDeliverypoint", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.MessagegroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MessagegroupId property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."MessagegroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MessagegroupId
		{
			get { return (System.Int32)GetValue((int)MessagegroupFieldIndex.MessagegroupId, true); }
			set	{ SetValue((int)MessagegroupFieldIndex.MessagegroupId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)MessagegroupFieldIndex.CompanyId, true); }
			set	{ SetValue((int)MessagegroupFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MessagegroupFieldIndex.Name, true); }
			set	{ SetValue((int)MessagegroupFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessagegroupFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MessagegroupFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessagegroupFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)MessagegroupFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessagegroupFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MessagegroupFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessagegroupFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)MessagegroupFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Type property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MessagegroupType Type
		{
			get { return (Obymobi.Enums.MessagegroupType)GetValue((int)MessagegroupFieldIndex.Type, true); }
			set	{ SetValue((int)MessagegroupFieldIndex.Type, value, true); }
		}

		/// <summary> The GroupName property of the Entity Messagegroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Messagegroup"."GroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GroupName
		{
			get { return (System.String)GetValue((int)MessagegroupFieldIndex.GroupName, true); }
			set	{ SetValue((int)MessagegroupFieldIndex.GroupName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessagegroupDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection MessagegroupDeliverypointCollection
		{
			get	{ return GetMultiMessagegroupDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupDeliverypointCollection. When set to true, MessagegroupDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessagegroupDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupDeliverypointCollection
		{
			get	{ return _alwaysFetchMessagegroupDeliverypointCollection; }
			set	{ _alwaysFetchMessagegroupDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupDeliverypointCollection already has been fetched. Setting this property to false when MessagegroupDeliverypointCollection has been fetched
		/// will clear the MessagegroupDeliverypointCollection collection well. Setting this property to true while MessagegroupDeliverypointCollection hasn't been fetched disables lazy loading for MessagegroupDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupDeliverypointCollection
		{
			get { return _alreadyFetchedMessagegroupDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedMessagegroupDeliverypointCollection && !value && (_messagegroupDeliverypointCollection != null))
				{
					_messagegroupDeliverypointCollection.Clear();
				}
				_alreadyFetchedMessagegroupDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPmsActionRuleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PmsActionRuleCollection PmsActionRuleCollection
		{
			get	{ return GetMultiPmsActionRuleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PmsActionRuleCollection. When set to true, PmsActionRuleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsActionRuleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPmsActionRuleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsActionRuleCollection
		{
			get	{ return _alwaysFetchPmsActionRuleCollection; }
			set	{ _alwaysFetchPmsActionRuleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsActionRuleCollection already has been fetched. Setting this property to false when PmsActionRuleCollection has been fetched
		/// will clear the PmsActionRuleCollection collection well. Setting this property to true while PmsActionRuleCollection hasn't been fetched disables lazy loading for PmsActionRuleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsActionRuleCollection
		{
			get { return _alreadyFetchedPmsActionRuleCollection;}
			set 
			{
				if(_alreadyFetchedPmsActionRuleCollection && !value && (_pmsActionRuleCollection != null))
				{
					_pmsActionRuleCollection.Clear();
				}
				_alreadyFetchedPmsActionRuleCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection ScheduledMessageHistoryCollection
		{
			get	{ return GetMultiScheduledMessageHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageHistoryCollection. When set to true, ScheduledMessageHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageHistoryCollection
		{
			get	{ return _alwaysFetchScheduledMessageHistoryCollection; }
			set	{ _alwaysFetchScheduledMessageHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageHistoryCollection already has been fetched. Setting this property to false when ScheduledMessageHistoryCollection has been fetched
		/// will clear the ScheduledMessageHistoryCollection collection well. Setting this property to true while ScheduledMessageHistoryCollection hasn't been fetched disables lazy loading for ScheduledMessageHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageHistoryCollection
		{
			get { return _alreadyFetchedScheduledMessageHistoryCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageHistoryCollection && !value && (_scheduledMessageHistoryCollection != null))
				{
					_scheduledMessageHistoryCollection.Clear();
				}
				_alreadyFetchedScheduledMessageHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection UIScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiUIScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemOccurrenceCollection. When set to true, UIScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchUIScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchUIScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when UIScheduleItemOccurrenceCollection has been fetched
		/// will clear the UIScheduleItemOccurrenceCollection collection well. Setting this property to true while UIScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for UIScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedUIScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemOccurrenceCollection && !value && (_uIScheduleItemOccurrenceCollection != null))
				{
					_uIScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessagegroupDeliverypoint
		{
			get { return GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessagegroupDeliverypoint. When set to true, DeliverypointCollectionViaMessagegroupDeliverypoint is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessagegroupDeliverypoint is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessagegroupDeliverypoint(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessagegroupDeliverypoint = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessagegroupDeliverypoint already has been fetched. Setting this property to false when DeliverypointCollectionViaMessagegroupDeliverypoint has been fetched
		/// will clear the DeliverypointCollectionViaMessagegroupDeliverypoint collection well. Setting this property to true while DeliverypointCollectionViaMessagegroupDeliverypoint hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessagegroupDeliverypoint</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint && !value && (_deliverypointCollectionViaMessagegroupDeliverypoint != null))
				{
					_deliverypointCollectionViaMessagegroupDeliverypoint.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessagegroupDeliverypoint = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessagegroupCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MessagegroupEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
