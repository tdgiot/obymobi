﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SiteLanguage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SiteLanguageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SiteLanguageEntity"; }
		}
	
		#region Class Member Declarations
		private LanguageEntity _languageEntity;
		private bool	_alwaysFetchLanguageEntity, _alreadyFetchedLanguageEntity, _languageEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name LanguageEntity</summary>
			public static readonly string LanguageEntity = "LanguageEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SiteLanguageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SiteLanguageEntityBase() :base("SiteLanguageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		protected SiteLanguageEntityBase(System.Int32 siteLanguageId):base("SiteLanguageEntity")
		{
			InitClassFetch(siteLanguageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SiteLanguageEntityBase(System.Int32 siteLanguageId, IPrefetchPath prefetchPathToUse): base("SiteLanguageEntity")
		{
			InitClassFetch(siteLanguageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="validator">The custom validator object for this SiteLanguageEntity</param>
		protected SiteLanguageEntityBase(System.Int32 siteLanguageId, IValidator validator):base("SiteLanguageEntity")
		{
			InitClassFetch(siteLanguageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteLanguageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_languageEntity = (LanguageEntity)info.GetValue("_languageEntity", typeof(LanguageEntity));
			if(_languageEntity!=null)
			{
				_languageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageEntityReturnsNewIfNotFound = info.GetBoolean("_languageEntityReturnsNewIfNotFound");
			_alwaysFetchLanguageEntity = info.GetBoolean("_alwaysFetchLanguageEntity");
			_alreadyFetchedLanguageEntity = info.GetBoolean("_alreadyFetchedLanguageEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SiteLanguageFieldIndex)fieldIndex)
			{
				case SiteLanguageFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case SiteLanguageFieldIndex.LanguageId:
					DesetupSyncLanguageEntity(true, false);
					_alreadyFetchedLanguageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLanguageEntity = (_languageEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "LanguageEntity":
					toReturn.Add(Relations.LanguageEntityUsingLanguageId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_languageEntity", (!this.MarkedForDeletion?_languageEntity:null));
			info.AddValue("_languageEntityReturnsNewIfNotFound", _languageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguageEntity", _alwaysFetchLanguageEntity);
			info.AddValue("_alreadyFetchedLanguageEntity", _alreadyFetchedLanguageEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "LanguageEntity":
					_alreadyFetchedLanguageEntity = true;
					this.LanguageEntity = (LanguageEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "LanguageEntity":
					SetupSyncLanguageEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "LanguageEntity":
					DesetupSyncLanguageEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_languageEntity!=null)
			{
				toReturn.Add(_languageEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteLanguageId)
		{
			return FetchUsingPK(siteLanguageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteLanguageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(siteLanguageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(siteLanguageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(siteLanguageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SiteLanguageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SiteLanguageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguageEntity()
		{
			return GetSingleLanguageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguageEntity || forceFetch || _alwaysFetchLanguageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageId);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageId);
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LanguageEntity = newEntity;
				_alreadyFetchedLanguageEntity = fetchResult;
			}
			return _languageEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId);
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("LanguageEntity", _languageEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="validator">The validator object for this SiteLanguageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 siteLanguageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(siteLanguageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_languageEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _languageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticSiteLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, signalRelatedEntity, "SiteLanguageCollection", resetFKFields, new int[] { (int)SiteLanguageFieldIndex.LanguageId } );		
			_languageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _languageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguageEntity(IEntityCore relatedEntity)
		{
			if(_languageEntity!=relatedEntity)
			{		
				DesetupSyncLanguageEntity(true, true);
				_languageEntity = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticSiteLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, ref _alreadyFetchedLanguageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticSiteLanguageRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "SiteLanguageCollection", resetFKFields, new int[] { (int)SiteLanguageFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticSiteLanguageRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="siteLanguageId">PK value for SiteLanguage which data should be fetched into this SiteLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 siteLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SiteLanguageFieldIndex.SiteLanguageId].ForcedCurrentValueWrite(siteLanguageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSiteLanguageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SiteLanguageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SiteLanguageRelations Relations
		{
			get	{ return new SiteLanguageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("LanguageEntity")[0], (int)Obymobi.Data.EntityType.SiteLanguageEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, null, "LanguageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.SiteLanguageEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SiteLanguageId property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."SiteLanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SiteLanguageId
		{
			get { return (System.Int32)GetValue((int)SiteLanguageFieldIndex.SiteLanguageId, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.SiteLanguageId, value, true); }
		}

		/// <summary> The SiteId property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteId
		{
			get { return (System.Int32)GetValue((int)SiteLanguageFieldIndex.SiteId, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.SiteId, value, true); }
		}

		/// <summary> The LanguageId property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LanguageId
		{
			get { return (System.Int32)GetValue((int)SiteLanguageFieldIndex.LanguageId, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SiteLanguageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SiteLanguageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Description property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SiteLanguageFieldIndex.Description, true); }
			set	{ SetValue((int)SiteLanguageFieldIndex.Description, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SiteLanguageFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)SiteLanguageFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteLanguageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SiteLanguageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SiteLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteLanguage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteLanguageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SiteLanguageFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LanguageEntity LanguageEntity
		{
			get	{ return GetSingleLanguageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteLanguageCollection", "LanguageEntity", _languageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageEntity. When set to true, LanguageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageEntity is accessed. You can always execute a forced fetch by calling GetSingleLanguageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageEntity
		{
			get	{ return _alwaysFetchLanguageEntity; }
			set	{ _alwaysFetchLanguageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageEntity already has been fetched. Setting this property to false when LanguageEntity has been fetched
		/// will set LanguageEntity to null as well. Setting this property to true while LanguageEntity hasn't been fetched disables lazy loading for LanguageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageEntity
		{
			get { return _alreadyFetchedLanguageEntity;}
			set 
			{
				if(_alreadyFetchedLanguageEntity && !value)
				{
					this.LanguageEntity = null;
				}
				_alreadyFetchedLanguageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LanguageEntity is not found
		/// in the database. When set to true, LanguageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LanguageEntityReturnsNewIfNotFound
		{
			get	{ return _languageEntityReturnsNewIfNotFound; }
			set { _languageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteLanguageCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SiteLanguageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
