﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'RoomControlSection'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoomControlSectionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoomControlSectionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.RoomControlComponentCollection	_roomControlComponentCollection;
		private bool	_alwaysFetchRoomControlComponentCollection, _alreadyFetchedRoomControlComponentCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection	_roomControlSectionItemCollection;
		private bool	_alwaysFetchRoomControlSectionItemCollection, _alreadyFetchedRoomControlSectionItemCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection	_roomControlSectionLanguageCollection;
		private bool	_alwaysFetchRoomControlSectionLanguageCollection, _alreadyFetchedRoomControlSectionLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection;
		private bool	_alwaysFetchRoomControlWidgetCollection, _alreadyFetchedRoomControlWidgetCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private RoomControlAreaEntity _roomControlAreaEntity;
		private bool	_alwaysFetchRoomControlAreaEntity, _alreadyFetchedRoomControlAreaEntity, _roomControlAreaEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RoomControlAreaEntity</summary>
			public static readonly string RoomControlAreaEntity = "RoomControlAreaEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name RoomControlComponentCollection</summary>
			public static readonly string RoomControlComponentCollection = "RoomControlComponentCollection";
			/// <summary>Member name RoomControlSectionItemCollection</summary>
			public static readonly string RoomControlSectionItemCollection = "RoomControlSectionItemCollection";
			/// <summary>Member name RoomControlSectionLanguageCollection</summary>
			public static readonly string RoomControlSectionLanguageCollection = "RoomControlSectionLanguageCollection";
			/// <summary>Member name RoomControlWidgetCollection</summary>
			public static readonly string RoomControlWidgetCollection = "RoomControlWidgetCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoomControlSectionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoomControlSectionEntityBase() :base("RoomControlSectionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		protected RoomControlSectionEntityBase(System.Int32 roomControlSectionId):base("RoomControlSectionEntity")
		{
			InitClassFetch(roomControlSectionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoomControlSectionEntityBase(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse): base("RoomControlSectionEntity")
		{
			InitClassFetch(roomControlSectionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="validator">The custom validator object for this RoomControlSectionEntity</param>
		protected RoomControlSectionEntityBase(System.Int32 roomControlSectionId, IValidator validator):base("RoomControlSectionEntity")
		{
			InitClassFetch(roomControlSectionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlSectionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_roomControlComponentCollection = (Obymobi.Data.CollectionClasses.RoomControlComponentCollection)info.GetValue("_roomControlComponentCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlComponentCollection));
			_alwaysFetchRoomControlComponentCollection = info.GetBoolean("_alwaysFetchRoomControlComponentCollection");
			_alreadyFetchedRoomControlComponentCollection = info.GetBoolean("_alreadyFetchedRoomControlComponentCollection");

			_roomControlSectionItemCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection)info.GetValue("_roomControlSectionItemCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection));
			_alwaysFetchRoomControlSectionItemCollection = info.GetBoolean("_alwaysFetchRoomControlSectionItemCollection");
			_alreadyFetchedRoomControlSectionItemCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionItemCollection");

			_roomControlSectionLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection)info.GetValue("_roomControlSectionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection));
			_alwaysFetchRoomControlSectionLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlSectionLanguageCollection");
			_alreadyFetchedRoomControlSectionLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionLanguageCollection");

			_roomControlWidgetCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection");
			_alreadyFetchedRoomControlWidgetCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_roomControlAreaEntity = (RoomControlAreaEntity)info.GetValue("_roomControlAreaEntity", typeof(RoomControlAreaEntity));
			if(_roomControlAreaEntity!=null)
			{
				_roomControlAreaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlAreaEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlAreaEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlAreaEntity = info.GetBoolean("_alwaysFetchRoomControlAreaEntity");
			_alreadyFetchedRoomControlAreaEntity = info.GetBoolean("_alreadyFetchedRoomControlAreaEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoomControlSectionFieldIndex)fieldIndex)
			{
				case RoomControlSectionFieldIndex.RoomControlAreaId:
					DesetupSyncRoomControlAreaEntity(true, false);
					_alreadyFetchedRoomControlAreaEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedRoomControlComponentCollection = (_roomControlComponentCollection.Count > 0);
			_alreadyFetchedRoomControlSectionItemCollection = (_roomControlSectionItemCollection.Count > 0);
			_alreadyFetchedRoomControlSectionLanguageCollection = (_roomControlSectionLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection = (_roomControlWidgetCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedRoomControlAreaEntity = (_roomControlAreaEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RoomControlAreaEntity":
					toReturn.Add(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingRoomControlSectionId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingRoomControlSectionId);
					break;
				case "RoomControlComponentCollection":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlSectionId);
					break;
				case "RoomControlSectionItemCollection":
					toReturn.Add(Relations.RoomControlSectionItemEntityUsingRoomControlSectionId);
					break;
				case "RoomControlSectionLanguageCollection":
					toReturn.Add(Relations.RoomControlSectionLanguageEntityUsingRoomControlSectionId);
					break;
				case "RoomControlWidgetCollection":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlSectionId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingRoomControlSectionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_roomControlComponentCollection", (!this.MarkedForDeletion?_roomControlComponentCollection:null));
			info.AddValue("_alwaysFetchRoomControlComponentCollection", _alwaysFetchRoomControlComponentCollection);
			info.AddValue("_alreadyFetchedRoomControlComponentCollection", _alreadyFetchedRoomControlComponentCollection);
			info.AddValue("_roomControlSectionItemCollection", (!this.MarkedForDeletion?_roomControlSectionItemCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionItemCollection", _alwaysFetchRoomControlSectionItemCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionItemCollection", _alreadyFetchedRoomControlSectionItemCollection);
			info.AddValue("_roomControlSectionLanguageCollection", (!this.MarkedForDeletion?_roomControlSectionLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionLanguageCollection", _alwaysFetchRoomControlSectionLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionLanguageCollection", _alreadyFetchedRoomControlSectionLanguageCollection);
			info.AddValue("_roomControlWidgetCollection", (!this.MarkedForDeletion?_roomControlWidgetCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection", _alwaysFetchRoomControlWidgetCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection", _alreadyFetchedRoomControlWidgetCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_roomControlAreaEntity", (!this.MarkedForDeletion?_roomControlAreaEntity:null));
			info.AddValue("_roomControlAreaEntityReturnsNewIfNotFound", _roomControlAreaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlAreaEntity", _alwaysFetchRoomControlAreaEntity);
			info.AddValue("_alreadyFetchedRoomControlAreaEntity", _alreadyFetchedRoomControlAreaEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RoomControlAreaEntity":
					_alreadyFetchedRoomControlAreaEntity = true;
					this.RoomControlAreaEntity = (RoomControlAreaEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "RoomControlComponentCollection":
					_alreadyFetchedRoomControlComponentCollection = true;
					if(entity!=null)
					{
						this.RoomControlComponentCollection.Add((RoomControlComponentEntity)entity);
					}
					break;
				case "RoomControlSectionItemCollection":
					_alreadyFetchedRoomControlSectionItemCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionItemCollection.Add((RoomControlSectionItemEntity)entity);
					}
					break;
				case "RoomControlSectionLanguageCollection":
					_alreadyFetchedRoomControlSectionLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionLanguageCollection.Add((RoomControlSectionLanguageEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection":
					_alreadyFetchedRoomControlWidgetCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RoomControlAreaEntity":
					SetupSyncRoomControlAreaEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "RoomControlComponentCollection":
					_roomControlComponentCollection.Add((RoomControlComponentEntity)relatedEntity);
					break;
				case "RoomControlSectionItemCollection":
					_roomControlSectionItemCollection.Add((RoomControlSectionItemEntity)relatedEntity);
					break;
				case "RoomControlSectionLanguageCollection":
					_roomControlSectionLanguageCollection.Add((RoomControlSectionLanguageEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection":
					_roomControlWidgetCollection.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RoomControlAreaEntity":
					DesetupSyncRoomControlAreaEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlComponentCollection":
					this.PerformRelatedEntityRemoval(_roomControlComponentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionItemCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_roomControlAreaEntity!=null)
			{
				toReturn.Add(_roomControlAreaEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_roomControlComponentCollection);
			toReturn.Add(_roomControlSectionItemCollection);
			toReturn.Add(_roomControlSectionLanguageCollection);
			toReturn.Add(_roomControlWidgetCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionId)
		{
			return FetchUsingPK(roomControlSectionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roomControlSectionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roomControlSectionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roomControlSectionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoomControlSectionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoomControlSectionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, _roomControlComponentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, _roomControlComponentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlComponentCollection || forceFetch || _alwaysFetchRoomControlComponentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlComponentCollection);
				_roomControlComponentCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlComponentCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlComponentCollection.GetMultiManyToOne(null, this, filter);
				_roomControlComponentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlComponentCollection = true;
			}
			return _roomControlComponentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlComponentCollection'. These settings will be taken into account
		/// when the property RoomControlComponentCollection is requested or GetMultiRoomControlComponentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlComponentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlComponentCollection.SortClauses=sortClauses;
			_roomControlComponentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, _roomControlSectionItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, _roomControlSectionItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionItemCollection || forceFetch || _alwaysFetchRoomControlSectionItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionItemCollection);
				_roomControlSectionItemCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionItemCollection.GetMultiManyToOne(null, this, null, filter);
				_roomControlSectionItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionItemCollection = true;
			}
			return _roomControlSectionItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionItemCollection'. These settings will be taken into account
		/// when the property RoomControlSectionItemCollection is requested or GetMultiRoomControlSectionItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionItemCollection.SortClauses=sortClauses;
			_roomControlSectionItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, _roomControlSectionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, _roomControlSectionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionLanguageCollection || forceFetch || _alwaysFetchRoomControlSectionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionLanguageCollection);
				_roomControlSectionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionLanguageCollection.GetMultiManyToOne(null, this, filter);
				_roomControlSectionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionLanguageCollection = true;
			}
			return _roomControlSectionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlSectionLanguageCollection is requested or GetMultiRoomControlSectionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionLanguageCollection.SortClauses=sortClauses;
			_roomControlSectionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection || forceFetch || _alwaysFetchRoomControlWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection);
				_roomControlWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_roomControlWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection = true;
			}
			return _roomControlWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection is requested or GetMultiRoomControlWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection.SortClauses=sortClauses;
			_roomControlWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public RoomControlAreaEntity GetSingleRoomControlAreaEntity()
		{
			return GetSingleRoomControlAreaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public virtual RoomControlAreaEntity GetSingleRoomControlAreaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlAreaEntity || forceFetch || _alwaysFetchRoomControlAreaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
				RoomControlAreaEntity newEntity = new RoomControlAreaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlAreaId);
				}
				if(fetchResult)
				{
					newEntity = (RoomControlAreaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlAreaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlAreaEntity = newEntity;
				_alreadyFetchedRoomControlAreaEntity = fetchResult;
			}
			return _roomControlAreaEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RoomControlAreaEntity", _roomControlAreaEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("RoomControlComponentCollection", _roomControlComponentCollection);
			toReturn.Add("RoomControlSectionItemCollection", _roomControlSectionItemCollection);
			toReturn.Add("RoomControlSectionLanguageCollection", _roomControlSectionLanguageCollection);
			toReturn.Add("RoomControlWidgetCollection", _roomControlWidgetCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="validator">The validator object for this RoomControlSectionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roomControlSectionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roomControlSectionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_roomControlComponentCollection = new Obymobi.Data.CollectionClasses.RoomControlComponentCollection();
			_roomControlComponentCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_roomControlSectionItemCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection();
			_roomControlSectionItemCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_roomControlSectionLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection();
			_roomControlSectionLanguageCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_roomControlWidgetCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "RoomControlSectionEntity");
			_roomControlAreaEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlAreaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, signalRelatedEntity, "RoomControlSectionCollection", resetFKFields, new int[] { (int)RoomControlSectionFieldIndex.RoomControlAreaId } );		
			_roomControlAreaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlAreaEntity(IEntityCore relatedEntity)
		{
			if(_roomControlAreaEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlAreaEntity(true, true);
				_roomControlAreaEntity = (RoomControlAreaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, ref _alreadyFetchedRoomControlAreaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlAreaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoomControlSectionFieldIndex.RoomControlSectionId].ForcedCurrentValueWrite(roomControlSectionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlSectionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoomControlSectionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoomControlSectionRelations Relations
		{
			get	{ return new RoomControlSectionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, 0, null, null, null, "RoomControlSectionItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionLanguageCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.RoomControlSectionLanguageEntity, 0, null, null, null, "RoomControlSectionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlArea'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaEntity")[0], (int)Obymobi.Data.EntityType.RoomControlSectionEntity, (int)Obymobi.Data.EntityType.RoomControlAreaEntity, 0, null, null, null, "RoomControlAreaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoomControlSectionId property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoomControlSectionId
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionFieldIndex.RoomControlSectionId, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlAreaId
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionFieldIndex.RoomControlAreaId, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The Name property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.Name, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.Name, value, true); }
		}

		/// <summary> The Type property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RoomControlSectionType Type
		{
			get { return (Obymobi.Enums.RoomControlSectionType)GetValue((int)RoomControlSectionFieldIndex.Type, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.Type, value, true); }
		}

		/// <summary> The SortOrder property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionFieldIndex.SortOrder, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlSectionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlSectionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)RoomControlSectionFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The Visible property of the Entity RoomControlSection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSection"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)RoomControlSectionFieldIndex.Visible, true); }
			set	{ SetValue((int)RoomControlSectionFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlComponentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentCollection RoomControlComponentCollection
		{
			get	{ return GetMultiRoomControlComponentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentCollection. When set to true, RoomControlComponentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlComponentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentCollection
		{
			get	{ return _alwaysFetchRoomControlComponentCollection; }
			set	{ _alwaysFetchRoomControlComponentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentCollection already has been fetched. Setting this property to false when RoomControlComponentCollection has been fetched
		/// will clear the RoomControlComponentCollection collection well. Setting this property to true while RoomControlComponentCollection hasn't been fetched disables lazy loading for RoomControlComponentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentCollection
		{
			get { return _alreadyFetchedRoomControlComponentCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentCollection && !value && (_roomControlComponentCollection != null))
				{
					_roomControlComponentCollection.Clear();
				}
				_alreadyFetchedRoomControlComponentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection RoomControlSectionItemCollection
		{
			get	{ return GetMultiRoomControlSectionItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemCollection. When set to true, RoomControlSectionItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemCollection
		{
			get	{ return _alwaysFetchRoomControlSectionItemCollection; }
			set	{ _alwaysFetchRoomControlSectionItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemCollection already has been fetched. Setting this property to false when RoomControlSectionItemCollection has been fetched
		/// will clear the RoomControlSectionItemCollection collection well. Setting this property to true while RoomControlSectionItemCollection hasn't been fetched disables lazy loading for RoomControlSectionItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemCollection
		{
			get { return _alreadyFetchedRoomControlSectionItemCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemCollection && !value && (_roomControlSectionItemCollection != null))
				{
					_roomControlSectionItemCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection RoomControlSectionLanguageCollection
		{
			get	{ return GetMultiRoomControlSectionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionLanguageCollection. When set to true, RoomControlSectionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlSectionLanguageCollection; }
			set	{ _alwaysFetchRoomControlSectionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionLanguageCollection already has been fetched. Setting this property to false when RoomControlSectionLanguageCollection has been fetched
		/// will clear the RoomControlSectionLanguageCollection collection well. Setting this property to true while RoomControlSectionLanguageCollection hasn't been fetched disables lazy loading for RoomControlSectionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionLanguageCollection
		{
			get { return _alreadyFetchedRoomControlSectionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionLanguageCollection && !value && (_roomControlSectionLanguageCollection != null))
				{
					_roomControlSectionLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection
		{
			get	{ return GetMultiRoomControlWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection. When set to true, RoomControlWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection; }
			set	{ _alwaysFetchRoomControlWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection already has been fetched. Setting this property to false when RoomControlWidgetCollection has been fetched
		/// will clear the RoomControlWidgetCollection collection well. Setting this property to true while RoomControlWidgetCollection hasn't been fetched disables lazy loading for RoomControlWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection
		{
			get { return _alreadyFetchedRoomControlWidgetCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection && !value && (_roomControlWidgetCollection != null))
				{
					_roomControlWidgetCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'RoomControlAreaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlAreaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlAreaEntity RoomControlAreaEntity
		{
			get	{ return GetSingleRoomControlAreaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlAreaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlSectionCollection", "RoomControlAreaEntity", _roomControlAreaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaEntity. When set to true, RoomControlAreaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlAreaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaEntity
		{
			get	{ return _alwaysFetchRoomControlAreaEntity; }
			set	{ _alwaysFetchRoomControlAreaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaEntity already has been fetched. Setting this property to false when RoomControlAreaEntity has been fetched
		/// will set RoomControlAreaEntity to null as well. Setting this property to true while RoomControlAreaEntity hasn't been fetched disables lazy loading for RoomControlAreaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaEntity
		{
			get { return _alreadyFetchedRoomControlAreaEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaEntity && !value)
				{
					this.RoomControlAreaEntity = null;
				}
				_alreadyFetchedRoomControlAreaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlAreaEntity is not found
		/// in the database. When set to true, RoomControlAreaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlAreaEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlAreaEntityReturnsNewIfNotFound; }
			set { _roomControlAreaEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoomControlSectionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
