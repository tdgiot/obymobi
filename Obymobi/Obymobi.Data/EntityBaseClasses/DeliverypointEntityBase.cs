﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Deliverypoint'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeliverypointEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientCollection	_clientCollection;
		private bool	_alwaysFetchClientCollection, _alreadyFetchedClientCollection;
		private Obymobi.Data.CollectionClasses.ClientCollection	_clientCollection_;
		private bool	_alwaysFetchClientCollection_, _alreadyFetchedClientCollection_;
		private Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection	_deliverypointExternalDeliverypointCollection;
		private bool	_alwaysFetchDeliverypointExternalDeliverypointCollection, _alreadyFetchedDeliverypointExternalDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.GameSessionCollection	_gameSessionCollection;
		private bool	_alwaysFetchGameSessionCollection, _alreadyFetchedGameSessionCollection;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection	_icrtouchprintermappingDeliverypointCollection;
		private bool	_alwaysFetchIcrtouchprintermappingDeliverypointCollection, _alreadyFetchedIcrtouchprintermappingDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection	_messagegroupDeliverypointCollection;
		private bool	_alwaysFetchMessagegroupDeliverypointCollection, _alreadyFetchedMessagegroupDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.MessageRecipientCollection	_messageRecipientCollection;
		private bool	_alwaysFetchMessageRecipientCollection, _alreadyFetchedMessageRecipientCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_receivedNetmessageCollection;
		private bool	_alwaysFetchReceivedNetmessageCollection, _alreadyFetchedReceivedNetmessageCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_sentNetmessageCollection;
		private bool	_alwaysFetchSentNetmessageCollection, _alreadyFetchedSentNetmessageCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection_;
		private bool	_alwaysFetchOrderCollection_, _alreadyFetchedOrderCollection_;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection_;
		private bool	_alwaysFetchTerminalCollection_, _alreadyFetchedTerminalCollection_;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection;
		private bool	_alwaysFetchTerminalCollection, _alreadyFetchedTerminalCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessage;
		private bool	_alwaysFetchCategoryCollectionViaMessage, _alreadyFetchedCategoryCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaMessage;
		private bool	_alwaysFetchClientCollectionViaMessage, _alreadyFetchedClientCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaClient;
		private bool	_alwaysFetchCompanyCollectionViaClient, _alreadyFetchedCompanyCollectionViaClient;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaClient_;
		private bool	_alwaysFetchCompanyCollectionViaClient_, _alreadyFetchedCompanyCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMessage;
		private bool	_alwaysFetchCompanyCollectionViaMessage, _alreadyFetchedCompanyCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaMessage;
		private bool	_alwaysFetchCustomerCollectionViaMessage, _alreadyFetchedCustomerCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient;
		private bool	_alwaysFetchDeliverypointCollectionViaClient, _alreadyFetchedDeliverypointCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient_;
		private bool	_alwaysFetchDeliverypointCollectionViaClient_, _alreadyFetchedDeliverypointCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaClient;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaClient, _alreadyFetchedDeliverypointgroupCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaClient_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaClient_, _alreadyFetchedDeliverypointgroupCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage_, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal, _alreadyFetchedDeliverypointgroupCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal_, _alreadyFetchedDeliverypointgroupCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaClient;
		private bool	_alwaysFetchDeviceCollectionViaClient, _alreadyFetchedDeviceCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaClient_;
		private bool	_alwaysFetchDeviceCollectionViaClient_, _alreadyFetchedDeviceCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal;
		private bool	_alwaysFetchDeviceCollectionViaTerminal, _alreadyFetchedDeviceCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal_;
		private bool	_alwaysFetchDeviceCollectionViaTerminal_, _alreadyFetchedDeviceCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal___;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal___, _alreadyFetchedEntertainmentCollectionViaTerminal___;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal____;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal____, _alreadyFetchedEntertainmentCollectionViaTerminal____;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_____;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_____, _alreadyFetchedEntertainmentCollectionViaTerminal_____;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessage;
		private bool	_alwaysFetchMediaCollectionViaMessage, _alreadyFetchedMediaCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.MessagegroupCollection _messagegroupCollectionViaMessagegroupDeliverypoint;
		private bool	_alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint, _alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaMessage;
		private bool	_alwaysFetchOrderCollectionViaMessage, _alreadyFetchedOrderCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrder;
		private bool	_alwaysFetchOrderCollectionViaOrder, _alreadyFetchedOrderCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage, _alreadyFetchedTerminalCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage_;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage_, _alreadyFetchedTerminalCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage__;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage__, _alreadyFetchedTerminalCollectionViaNetmessage__;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage___;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage___, _alreadyFetchedTerminalCollectionViaNetmessage___;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal;
		private bool	_alwaysFetchTerminalCollectionViaTerminal, _alreadyFetchedTerminalCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal_;
		private bool	_alwaysFetchTerminalCollectionViaTerminal_, _alreadyFetchedTerminalCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal;
		private bool	_alwaysFetchUIModeCollectionViaTerminal, _alreadyFetchedUIModeCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal_;
		private bool	_alwaysFetchUIModeCollectionViaTerminal_, _alreadyFetchedUIModeCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaTerminal;
		private bool	_alwaysFetchUserCollectionViaTerminal, _alreadyFetchedUserCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaTerminal_;
		private bool	_alwaysFetchUserCollectionViaTerminal_, _alreadyFetchedUserCollectionViaTerminal_;
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private DeviceEntity _deviceEntity;
		private bool	_alwaysFetchDeviceEntity, _alreadyFetchedDeviceEntity, _deviceEntityReturnsNewIfNotFound;
		private PosdeliverypointEntity _posdeliverypointEntity;
		private bool	_alwaysFetchPosdeliverypointEntity, _alreadyFetchedPosdeliverypointEntity, _posdeliverypointEntityReturnsNewIfNotFound;
		private RoomControlAreaEntity _roomControlAreaEntity;
		private bool	_alwaysFetchRoomControlAreaEntity, _alreadyFetchedRoomControlAreaEntity, _roomControlAreaEntityReturnsNewIfNotFound;
		private RoomControlConfigurationEntity _roomControlConfigurationEntity;
		private bool	_alwaysFetchRoomControlConfigurationEntity, _alreadyFetchedRoomControlConfigurationEntity, _roomControlConfigurationEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name DeviceEntity</summary>
			public static readonly string DeviceEntity = "DeviceEntity";
			/// <summary>Member name PosdeliverypointEntity</summary>
			public static readonly string PosdeliverypointEntity = "PosdeliverypointEntity";
			/// <summary>Member name RoomControlAreaEntity</summary>
			public static readonly string RoomControlAreaEntity = "RoomControlAreaEntity";
			/// <summary>Member name RoomControlConfigurationEntity</summary>
			public static readonly string RoomControlConfigurationEntity = "RoomControlConfigurationEntity";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name ClientCollection_</summary>
			public static readonly string ClientCollection_ = "ClientCollection_";
			/// <summary>Member name DeliverypointExternalDeliverypointCollection</summary>
			public static readonly string DeliverypointExternalDeliverypointCollection = "DeliverypointExternalDeliverypointCollection";
			/// <summary>Member name GameSessionCollection</summary>
			public static readonly string GameSessionCollection = "GameSessionCollection";
			/// <summary>Member name IcrtouchprintermappingDeliverypointCollection</summary>
			public static readonly string IcrtouchprintermappingDeliverypointCollection = "IcrtouchprintermappingDeliverypointCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessagegroupDeliverypointCollection</summary>
			public static readonly string MessagegroupDeliverypointCollection = "MessagegroupDeliverypointCollection";
			/// <summary>Member name MessageRecipientCollection</summary>
			public static readonly string MessageRecipientCollection = "MessageRecipientCollection";
			/// <summary>Member name ReceivedNetmessageCollection</summary>
			public static readonly string ReceivedNetmessageCollection = "ReceivedNetmessageCollection";
			/// <summary>Member name SentNetmessageCollection</summary>
			public static readonly string SentNetmessageCollection = "SentNetmessageCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name OrderCollection_</summary>
			public static readonly string OrderCollection_ = "OrderCollection_";
			/// <summary>Member name TerminalCollection_</summary>
			public static readonly string TerminalCollection_ = "TerminalCollection_";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name CategoryCollectionViaMessage</summary>
			public static readonly string CategoryCollectionViaMessage = "CategoryCollectionViaMessage";
			/// <summary>Member name ClientCollectionViaMessage</summary>
			public static readonly string ClientCollectionViaMessage = "ClientCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaClient</summary>
			public static readonly string CompanyCollectionViaClient = "CompanyCollectionViaClient";
			/// <summary>Member name CompanyCollectionViaClient_</summary>
			public static readonly string CompanyCollectionViaClient_ = "CompanyCollectionViaClient_";
			/// <summary>Member name CompanyCollectionViaMessage</summary>
			public static readonly string CompanyCollectionViaMessage = "CompanyCollectionViaMessage";
			/// <summary>Member name CustomerCollectionViaMessage</summary>
			public static readonly string CustomerCollectionViaMessage = "CustomerCollectionViaMessage";
			/// <summary>Member name DeliverypointCollectionViaClient</summary>
			public static readonly string DeliverypointCollectionViaClient = "DeliverypointCollectionViaClient";
			/// <summary>Member name DeliverypointCollectionViaClient_</summary>
			public static readonly string DeliverypointCollectionViaClient_ = "DeliverypointCollectionViaClient_";
			/// <summary>Member name DeliverypointgroupCollectionViaClient</summary>
			public static readonly string DeliverypointgroupCollectionViaClient = "DeliverypointgroupCollectionViaClient";
			/// <summary>Member name DeliverypointgroupCollectionViaClient_</summary>
			public static readonly string DeliverypointgroupCollectionViaClient_ = "DeliverypointgroupCollectionViaClient_";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage = "DeliverypointgroupCollectionViaNetmessage";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage_</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage_ = "DeliverypointgroupCollectionViaNetmessage_";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal = "DeliverypointgroupCollectionViaTerminal";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal_</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal_ = "DeliverypointgroupCollectionViaTerminal_";
			/// <summary>Member name DeviceCollectionViaClient</summary>
			public static readonly string DeviceCollectionViaClient = "DeviceCollectionViaClient";
			/// <summary>Member name DeviceCollectionViaClient_</summary>
			public static readonly string DeviceCollectionViaClient_ = "DeviceCollectionViaClient_";
			/// <summary>Member name DeviceCollectionViaTerminal</summary>
			public static readonly string DeviceCollectionViaTerminal = "DeviceCollectionViaTerminal";
			/// <summary>Member name DeviceCollectionViaTerminal_</summary>
			public static readonly string DeviceCollectionViaTerminal_ = "DeviceCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name EntertainmentCollectionViaTerminal___</summary>
			public static readonly string EntertainmentCollectionViaTerminal___ = "EntertainmentCollectionViaTerminal___";
			/// <summary>Member name EntertainmentCollectionViaTerminal____</summary>
			public static readonly string EntertainmentCollectionViaTerminal____ = "EntertainmentCollectionViaTerminal____";
			/// <summary>Member name EntertainmentCollectionViaTerminal_____</summary>
			public static readonly string EntertainmentCollectionViaTerminal_____ = "EntertainmentCollectionViaTerminal_____";
			/// <summary>Member name MediaCollectionViaMessage</summary>
			public static readonly string MediaCollectionViaMessage = "MediaCollectionViaMessage";
			/// <summary>Member name MessagegroupCollectionViaMessagegroupDeliverypoint</summary>
			public static readonly string MessagegroupCollectionViaMessagegroupDeliverypoint = "MessagegroupCollectionViaMessagegroupDeliverypoint";
			/// <summary>Member name OrderCollectionViaMessage</summary>
			public static readonly string OrderCollectionViaMessage = "OrderCollectionViaMessage";
			/// <summary>Member name OrderCollectionViaOrder</summary>
			public static readonly string OrderCollectionViaOrder = "OrderCollectionViaOrder";
			/// <summary>Member name TerminalCollectionViaNetmessage</summary>
			public static readonly string TerminalCollectionViaNetmessage = "TerminalCollectionViaNetmessage";
			/// <summary>Member name TerminalCollectionViaNetmessage_</summary>
			public static readonly string TerminalCollectionViaNetmessage_ = "TerminalCollectionViaNetmessage_";
			/// <summary>Member name TerminalCollectionViaNetmessage__</summary>
			public static readonly string TerminalCollectionViaNetmessage__ = "TerminalCollectionViaNetmessage__";
			/// <summary>Member name TerminalCollectionViaNetmessage___</summary>
			public static readonly string TerminalCollectionViaNetmessage___ = "TerminalCollectionViaNetmessage___";
			/// <summary>Member name TerminalCollectionViaTerminal</summary>
			public static readonly string TerminalCollectionViaTerminal = "TerminalCollectionViaTerminal";
			/// <summary>Member name TerminalCollectionViaTerminal_</summary>
			public static readonly string TerminalCollectionViaTerminal_ = "TerminalCollectionViaTerminal_";
			/// <summary>Member name UIModeCollectionViaTerminal</summary>
			public static readonly string UIModeCollectionViaTerminal = "UIModeCollectionViaTerminal";
			/// <summary>Member name UIModeCollectionViaTerminal_</summary>
			public static readonly string UIModeCollectionViaTerminal_ = "UIModeCollectionViaTerminal_";
			/// <summary>Member name UserCollectionViaTerminal</summary>
			public static readonly string UserCollectionViaTerminal = "UserCollectionViaTerminal";
			/// <summary>Member name UserCollectionViaTerminal_</summary>
			public static readonly string UserCollectionViaTerminal_ = "UserCollectionViaTerminal_";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeliverypointEntityBase() :base("DeliverypointEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		protected DeliverypointEntityBase(System.Int32 deliverypointId):base("DeliverypointEntity")
		{
			InitClassFetch(deliverypointId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeliverypointEntityBase(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse): base("DeliverypointEntity")
		{
			InitClassFetch(deliverypointId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointEntity</param>
		protected DeliverypointEntityBase(System.Int32 deliverypointId, IValidator validator):base("DeliverypointEntity")
		{
			InitClassFetch(deliverypointId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientCollection = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollection", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollection = info.GetBoolean("_alwaysFetchClientCollection");
			_alreadyFetchedClientCollection = info.GetBoolean("_alreadyFetchedClientCollection");

			_clientCollection_ = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollection_", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollection_ = info.GetBoolean("_alwaysFetchClientCollection_");
			_alreadyFetchedClientCollection_ = info.GetBoolean("_alreadyFetchedClientCollection_");

			_deliverypointExternalDeliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection)info.GetValue("_deliverypointExternalDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection));
			_alwaysFetchDeliverypointExternalDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointExternalDeliverypointCollection");
			_alreadyFetchedDeliverypointExternalDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointExternalDeliverypointCollection");

			_gameSessionCollection = (Obymobi.Data.CollectionClasses.GameSessionCollection)info.GetValue("_gameSessionCollection", typeof(Obymobi.Data.CollectionClasses.GameSessionCollection));
			_alwaysFetchGameSessionCollection = info.GetBoolean("_alwaysFetchGameSessionCollection");
			_alreadyFetchedGameSessionCollection = info.GetBoolean("_alreadyFetchedGameSessionCollection");

			_icrtouchprintermappingDeliverypointCollection = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection)info.GetValue("_icrtouchprintermappingDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection));
			_alwaysFetchIcrtouchprintermappingDeliverypointCollection = info.GetBoolean("_alwaysFetchIcrtouchprintermappingDeliverypointCollection");
			_alreadyFetchedIcrtouchprintermappingDeliverypointCollection = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingDeliverypointCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messagegroupDeliverypointCollection = (Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection)info.GetValue("_messagegroupDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection));
			_alwaysFetchMessagegroupDeliverypointCollection = info.GetBoolean("_alwaysFetchMessagegroupDeliverypointCollection");
			_alreadyFetchedMessagegroupDeliverypointCollection = info.GetBoolean("_alreadyFetchedMessagegroupDeliverypointCollection");

			_messageRecipientCollection = (Obymobi.Data.CollectionClasses.MessageRecipientCollection)info.GetValue("_messageRecipientCollection", typeof(Obymobi.Data.CollectionClasses.MessageRecipientCollection));
			_alwaysFetchMessageRecipientCollection = info.GetBoolean("_alwaysFetchMessageRecipientCollection");
			_alreadyFetchedMessageRecipientCollection = info.GetBoolean("_alreadyFetchedMessageRecipientCollection");

			_receivedNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_receivedNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchReceivedNetmessageCollection = info.GetBoolean("_alwaysFetchReceivedNetmessageCollection");
			_alreadyFetchedReceivedNetmessageCollection = info.GetBoolean("_alreadyFetchedReceivedNetmessageCollection");

			_sentNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_sentNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchSentNetmessageCollection = info.GetBoolean("_alwaysFetchSentNetmessageCollection");
			_alreadyFetchedSentNetmessageCollection = info.GetBoolean("_alreadyFetchedSentNetmessageCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_orderCollection_ = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection_", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection_ = info.GetBoolean("_alwaysFetchOrderCollection_");
			_alreadyFetchedOrderCollection_ = info.GetBoolean("_alreadyFetchedOrderCollection_");

			_terminalCollection_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection_ = info.GetBoolean("_alwaysFetchTerminalCollection_");
			_alreadyFetchedTerminalCollection_ = info.GetBoolean("_alreadyFetchedTerminalCollection_");

			_terminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection = info.GetBoolean("_alwaysFetchTerminalCollection");
			_alreadyFetchedTerminalCollection = info.GetBoolean("_alreadyFetchedTerminalCollection");
			_categoryCollectionViaMessage = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessage = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessage");
			_alreadyFetchedCategoryCollectionViaMessage = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessage");

			_clientCollectionViaMessage = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaMessage = info.GetBoolean("_alwaysFetchClientCollectionViaMessage");
			_alreadyFetchedClientCollectionViaMessage = info.GetBoolean("_alreadyFetchedClientCollectionViaMessage");

			_companyCollectionViaClient = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaClient = info.GetBoolean("_alwaysFetchCompanyCollectionViaClient");
			_alreadyFetchedCompanyCollectionViaClient = info.GetBoolean("_alreadyFetchedCompanyCollectionViaClient");

			_companyCollectionViaClient_ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaClient_ = info.GetBoolean("_alwaysFetchCompanyCollectionViaClient_");
			_alreadyFetchedCompanyCollectionViaClient_ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaClient_");

			_companyCollectionViaMessage = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMessage = info.GetBoolean("_alwaysFetchCompanyCollectionViaMessage");
			_alreadyFetchedCompanyCollectionViaMessage = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMessage");

			_customerCollectionViaMessage = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaMessage = info.GetBoolean("_alwaysFetchCustomerCollectionViaMessage");
			_alreadyFetchedCustomerCollectionViaMessage = info.GetBoolean("_alreadyFetchedCustomerCollectionViaMessage");

			_deliverypointCollectionViaClient = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient");
			_alreadyFetchedDeliverypointCollectionViaClient = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient");

			_deliverypointCollectionViaClient_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient_");
			_alreadyFetchedDeliverypointCollectionViaClient_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient_");

			_deliverypointgroupCollectionViaClient = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaClient = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaClient");
			_alreadyFetchedDeliverypointgroupCollectionViaClient = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaClient");

			_deliverypointgroupCollectionViaClient_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaClient_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaClient_");
			_alreadyFetchedDeliverypointgroupCollectionViaClient_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaClient_");

			_deliverypointgroupCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage");

			_deliverypointgroupCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_");

			_deliverypointgroupCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal");

			_deliverypointgroupCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal_");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal_");

			_deviceCollectionViaClient = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaClient = info.GetBoolean("_alwaysFetchDeviceCollectionViaClient");
			_alreadyFetchedDeviceCollectionViaClient = info.GetBoolean("_alreadyFetchedDeviceCollectionViaClient");

			_deviceCollectionViaClient_ = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaClient_ = info.GetBoolean("_alwaysFetchDeviceCollectionViaClient_");
			_alreadyFetchedDeviceCollectionViaClient_ = info.GetBoolean("_alreadyFetchedDeviceCollectionViaClient_");

			_deviceCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal");
			_alreadyFetchedDeviceCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal");

			_deviceCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal_");
			_alreadyFetchedDeviceCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal_");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_entertainmentCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal___");
			_alreadyFetchedEntertainmentCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal___");

			_entertainmentCollectionViaTerminal____ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal____", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal____ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal____");
			_alreadyFetchedEntertainmentCollectionViaTerminal____ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal____");

			_entertainmentCollectionViaTerminal_____ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_____", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_____ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_____");
			_alreadyFetchedEntertainmentCollectionViaTerminal_____ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_____");

			_mediaCollectionViaMessage = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessage = info.GetBoolean("_alwaysFetchMediaCollectionViaMessage");
			_alreadyFetchedMediaCollectionViaMessage = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessage");

			_messagegroupCollectionViaMessagegroupDeliverypoint = (Obymobi.Data.CollectionClasses.MessagegroupCollection)info.GetValue("_messagegroupCollectionViaMessagegroupDeliverypoint", typeof(Obymobi.Data.CollectionClasses.MessagegroupCollection));
			_alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint = info.GetBoolean("_alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint");
			_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint = info.GetBoolean("_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint");

			_orderCollectionViaMessage = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaMessage = info.GetBoolean("_alwaysFetchOrderCollectionViaMessage");
			_alreadyFetchedOrderCollectionViaMessage = info.GetBoolean("_alreadyFetchedOrderCollectionViaMessage");

			_orderCollectionViaOrder = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrder = info.GetBoolean("_alwaysFetchOrderCollectionViaOrder");
			_alreadyFetchedOrderCollectionViaOrder = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrder");

			_terminalCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage");
			_alreadyFetchedTerminalCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage");

			_terminalCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage_");
			_alreadyFetchedTerminalCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage_");

			_terminalCollectionViaNetmessage__ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage__", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage__ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage__");
			_alreadyFetchedTerminalCollectionViaNetmessage__ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage__");

			_terminalCollectionViaNetmessage___ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage___", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage___ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage___");
			_alreadyFetchedTerminalCollectionViaNetmessage___ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage___");

			_terminalCollectionViaTerminal = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal");
			_alreadyFetchedTerminalCollectionViaTerminal = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal");

			_terminalCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal_");
			_alreadyFetchedTerminalCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal_");

			_uIModeCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal");
			_alreadyFetchedUIModeCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal");

			_uIModeCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal_");
			_alreadyFetchedUIModeCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal_");

			_userCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaTerminal = info.GetBoolean("_alwaysFetchUserCollectionViaTerminal");
			_alreadyFetchedUserCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUserCollectionViaTerminal");

			_userCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchUserCollectionViaTerminal_");
			_alreadyFetchedUserCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedUserCollectionViaTerminal_");
			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_deviceEntity = (DeviceEntity)info.GetValue("_deviceEntity", typeof(DeviceEntity));
			if(_deviceEntity!=null)
			{
				_deviceEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceEntityReturnsNewIfNotFound = info.GetBoolean("_deviceEntityReturnsNewIfNotFound");
			_alwaysFetchDeviceEntity = info.GetBoolean("_alwaysFetchDeviceEntity");
			_alreadyFetchedDeviceEntity = info.GetBoolean("_alreadyFetchedDeviceEntity");

			_posdeliverypointEntity = (PosdeliverypointEntity)info.GetValue("_posdeliverypointEntity", typeof(PosdeliverypointEntity));
			if(_posdeliverypointEntity!=null)
			{
				_posdeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posdeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_posdeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchPosdeliverypointEntity = info.GetBoolean("_alwaysFetchPosdeliverypointEntity");
			_alreadyFetchedPosdeliverypointEntity = info.GetBoolean("_alreadyFetchedPosdeliverypointEntity");

			_roomControlAreaEntity = (RoomControlAreaEntity)info.GetValue("_roomControlAreaEntity", typeof(RoomControlAreaEntity));
			if(_roomControlAreaEntity!=null)
			{
				_roomControlAreaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlAreaEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlAreaEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlAreaEntity = info.GetBoolean("_alwaysFetchRoomControlAreaEntity");
			_alreadyFetchedRoomControlAreaEntity = info.GetBoolean("_alreadyFetchedRoomControlAreaEntity");

			_roomControlConfigurationEntity = (RoomControlConfigurationEntity)info.GetValue("_roomControlConfigurationEntity", typeof(RoomControlConfigurationEntity));
			if(_roomControlConfigurationEntity!=null)
			{
				_roomControlConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlConfigurationEntity = info.GetBoolean("_alwaysFetchRoomControlConfigurationEntity");
			_alreadyFetchedRoomControlConfigurationEntity = info.GetBoolean("_alreadyFetchedRoomControlConfigurationEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointFieldIndex)fieldIndex)
			{
				case DeliverypointFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case DeliverypointFieldIndex.PosdeliverypointId:
					DesetupSyncPosdeliverypointEntity(true, false);
					_alreadyFetchedPosdeliverypointEntity = false;
					break;
				case DeliverypointFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case DeliverypointFieldIndex.DeviceId:
					DesetupSyncDeviceEntity(true, false);
					_alreadyFetchedDeviceEntity = false;
					break;
				case DeliverypointFieldIndex.RoomControlConfigurationId:
					DesetupSyncRoomControlConfigurationEntity(true, false);
					_alreadyFetchedRoomControlConfigurationEntity = false;
					break;
				case DeliverypointFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case DeliverypointFieldIndex.RoomControlAreaId:
					DesetupSyncRoomControlAreaEntity(true, false);
					_alreadyFetchedRoomControlAreaEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientCollection = (_clientCollection.Count > 0);
			_alreadyFetchedClientCollection_ = (_clientCollection_.Count > 0);
			_alreadyFetchedDeliverypointExternalDeliverypointCollection = (_deliverypointExternalDeliverypointCollection.Count > 0);
			_alreadyFetchedGameSessionCollection = (_gameSessionCollection.Count > 0);
			_alreadyFetchedIcrtouchprintermappingDeliverypointCollection = (_icrtouchprintermappingDeliverypointCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessagegroupDeliverypointCollection = (_messagegroupDeliverypointCollection.Count > 0);
			_alreadyFetchedMessageRecipientCollection = (_messageRecipientCollection.Count > 0);
			_alreadyFetchedReceivedNetmessageCollection = (_receivedNetmessageCollection.Count > 0);
			_alreadyFetchedSentNetmessageCollection = (_sentNetmessageCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedOrderCollection_ = (_orderCollection_.Count > 0);
			_alreadyFetchedTerminalCollection_ = (_terminalCollection_.Count > 0);
			_alreadyFetchedTerminalCollection = (_terminalCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessage = (_categoryCollectionViaMessage.Count > 0);
			_alreadyFetchedClientCollectionViaMessage = (_clientCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaClient = (_companyCollectionViaClient.Count > 0);
			_alreadyFetchedCompanyCollectionViaClient_ = (_companyCollectionViaClient_.Count > 0);
			_alreadyFetchedCompanyCollectionViaMessage = (_companyCollectionViaMessage.Count > 0);
			_alreadyFetchedCustomerCollectionViaMessage = (_customerCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient = (_deliverypointCollectionViaClient.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient_ = (_deliverypointCollectionViaClient_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaClient = (_deliverypointgroupCollectionViaClient.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaClient_ = (_deliverypointgroupCollectionViaClient_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = (_deliverypointgroupCollectionViaNetmessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = (_deliverypointgroupCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = (_deliverypointgroupCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ = (_deliverypointgroupCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeviceCollectionViaClient = (_deviceCollectionViaClient.Count > 0);
			_alreadyFetchedDeviceCollectionViaClient_ = (_deviceCollectionViaClient_.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal = (_deviceCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal_ = (_deviceCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal___ = (_entertainmentCollectionViaTerminal___.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal____ = (_entertainmentCollectionViaTerminal____.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_____ = (_entertainmentCollectionViaTerminal_____.Count > 0);
			_alreadyFetchedMediaCollectionViaMessage = (_mediaCollectionViaMessage.Count > 0);
			_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint = (_messagegroupCollectionViaMessagegroupDeliverypoint.Count > 0);
			_alreadyFetchedOrderCollectionViaMessage = (_orderCollectionViaMessage.Count > 0);
			_alreadyFetchedOrderCollectionViaOrder = (_orderCollectionViaOrder.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage = (_terminalCollectionViaNetmessage.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage_ = (_terminalCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage__ = (_terminalCollectionViaNetmessage__.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage___ = (_terminalCollectionViaNetmessage___.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal = (_terminalCollectionViaTerminal.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal_ = (_terminalCollectionViaTerminal_.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal = (_uIModeCollectionViaTerminal.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal_ = (_uIModeCollectionViaTerminal_.Count > 0);
			_alreadyFetchedUserCollectionViaTerminal = (_userCollectionViaTerminal.Count > 0);
			_alreadyFetchedUserCollectionViaTerminal_ = (_userCollectionViaTerminal_.Count > 0);
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedDeviceEntity = (_deviceEntity != null);
			_alreadyFetchedPosdeliverypointEntity = (_posdeliverypointEntity != null);
			_alreadyFetchedRoomControlAreaEntity = (_roomControlAreaEntity != null);
			_alreadyFetchedRoomControlConfigurationEntity = (_roomControlConfigurationEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "DeviceEntity":
					toReturn.Add(Relations.DeviceEntityUsingDeviceId);
					break;
				case "PosdeliverypointEntity":
					toReturn.Add(Relations.PosdeliverypointEntityUsingPosdeliverypointId);
					break;
				case "RoomControlAreaEntity":
					toReturn.Add(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
					break;
				case "RoomControlConfigurationEntity":
					toReturn.Add(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
					break;
				case "ClientCollection":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId);
					break;
				case "ClientCollection_":
					toReturn.Add(Relations.ClientEntityUsingLastDeliverypointId);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					toReturn.Add(Relations.DeliverypointExternalDeliverypointEntityUsingDeliverypointId);
					break;
				case "GameSessionCollection":
					toReturn.Add(Relations.GameSessionEntityUsingDeliverypointId);
					break;
				case "IcrtouchprintermappingDeliverypointCollection":
					toReturn.Add(Relations.IcrtouchprintermappingDeliverypointEntityUsingDeliverypointId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId);
					break;
				case "MessagegroupDeliverypointCollection":
					toReturn.Add(Relations.MessagegroupDeliverypointEntityUsingDeliverypointId);
					break;
				case "MessageRecipientCollection":
					toReturn.Add(Relations.MessageRecipientEntityUsingDeliverypointId);
					break;
				case "ReceivedNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointId);
					break;
				case "SentNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingSenderDeliverypointId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingDeliverypointId);
					break;
				case "OrderCollection_":
					toReturn.Add(Relations.OrderEntityUsingChargeToDeliverypointId);
					break;
				case "TerminalCollection_":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId);
					break;
				case "TerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId);
					break;
				case "CategoryCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.ClientEntityUsingClientId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId, "Client_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingLastDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId, "Client_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CompanyEntityUsingCompanyId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CustomerEntityUsingCustomerId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingLastDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingLastDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingLastDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingLastDeliverypointId, "DeliverypointEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal____":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_____":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.MediaEntityUsingMediaId, "Message_", string.Empty, JoinHint.None);
					break;
				case "MessagegroupCollectionViaMessagegroupDeliverypoint":
					toReturn.Add(Relations.MessagegroupDeliverypointEntityUsingDeliverypointId, "DeliverypointEntity__", "MessagegroupDeliverypoint_", JoinHint.None);
					toReturn.Add(MessagegroupDeliverypointEntity.Relations.MessagegroupEntityUsingMessagegroupId, "MessagegroupDeliverypoint_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingDeliverypointId, "DeliverypointEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.OrderEntityUsingOrderId, "Message_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingDeliverypointId, "DeliverypointEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.OrderEntityUsingMasterOrderId, "Order_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage__":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage___":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointId, "DeliverypointEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UserCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UserCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingSystemMessagesDeliverypointId, "DeliverypointEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingDeliverypointId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientCollection", (!this.MarkedForDeletion?_clientCollection:null));
			info.AddValue("_alwaysFetchClientCollection", _alwaysFetchClientCollection);
			info.AddValue("_alreadyFetchedClientCollection", _alreadyFetchedClientCollection);
			info.AddValue("_clientCollection_", (!this.MarkedForDeletion?_clientCollection_:null));
			info.AddValue("_alwaysFetchClientCollection_", _alwaysFetchClientCollection_);
			info.AddValue("_alreadyFetchedClientCollection_", _alreadyFetchedClientCollection_);
			info.AddValue("_deliverypointExternalDeliverypointCollection", (!this.MarkedForDeletion?_deliverypointExternalDeliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointExternalDeliverypointCollection", _alwaysFetchDeliverypointExternalDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointExternalDeliverypointCollection", _alreadyFetchedDeliverypointExternalDeliverypointCollection);
			info.AddValue("_gameSessionCollection", (!this.MarkedForDeletion?_gameSessionCollection:null));
			info.AddValue("_alwaysFetchGameSessionCollection", _alwaysFetchGameSessionCollection);
			info.AddValue("_alreadyFetchedGameSessionCollection", _alreadyFetchedGameSessionCollection);
			info.AddValue("_icrtouchprintermappingDeliverypointCollection", (!this.MarkedForDeletion?_icrtouchprintermappingDeliverypointCollection:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingDeliverypointCollection", _alwaysFetchIcrtouchprintermappingDeliverypointCollection);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingDeliverypointCollection", _alreadyFetchedIcrtouchprintermappingDeliverypointCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messagegroupDeliverypointCollection", (!this.MarkedForDeletion?_messagegroupDeliverypointCollection:null));
			info.AddValue("_alwaysFetchMessagegroupDeliverypointCollection", _alwaysFetchMessagegroupDeliverypointCollection);
			info.AddValue("_alreadyFetchedMessagegroupDeliverypointCollection", _alreadyFetchedMessagegroupDeliverypointCollection);
			info.AddValue("_messageRecipientCollection", (!this.MarkedForDeletion?_messageRecipientCollection:null));
			info.AddValue("_alwaysFetchMessageRecipientCollection", _alwaysFetchMessageRecipientCollection);
			info.AddValue("_alreadyFetchedMessageRecipientCollection", _alreadyFetchedMessageRecipientCollection);
			info.AddValue("_receivedNetmessageCollection", (!this.MarkedForDeletion?_receivedNetmessageCollection:null));
			info.AddValue("_alwaysFetchReceivedNetmessageCollection", _alwaysFetchReceivedNetmessageCollection);
			info.AddValue("_alreadyFetchedReceivedNetmessageCollection", _alreadyFetchedReceivedNetmessageCollection);
			info.AddValue("_sentNetmessageCollection", (!this.MarkedForDeletion?_sentNetmessageCollection:null));
			info.AddValue("_alwaysFetchSentNetmessageCollection", _alwaysFetchSentNetmessageCollection);
			info.AddValue("_alreadyFetchedSentNetmessageCollection", _alreadyFetchedSentNetmessageCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_orderCollection_", (!this.MarkedForDeletion?_orderCollection_:null));
			info.AddValue("_alwaysFetchOrderCollection_", _alwaysFetchOrderCollection_);
			info.AddValue("_alreadyFetchedOrderCollection_", _alreadyFetchedOrderCollection_);
			info.AddValue("_terminalCollection_", (!this.MarkedForDeletion?_terminalCollection_:null));
			info.AddValue("_alwaysFetchTerminalCollection_", _alwaysFetchTerminalCollection_);
			info.AddValue("_alreadyFetchedTerminalCollection_", _alreadyFetchedTerminalCollection_);
			info.AddValue("_terminalCollection", (!this.MarkedForDeletion?_terminalCollection:null));
			info.AddValue("_alwaysFetchTerminalCollection", _alwaysFetchTerminalCollection);
			info.AddValue("_alreadyFetchedTerminalCollection", _alreadyFetchedTerminalCollection);
			info.AddValue("_categoryCollectionViaMessage", (!this.MarkedForDeletion?_categoryCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessage", _alwaysFetchCategoryCollectionViaMessage);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessage", _alreadyFetchedCategoryCollectionViaMessage);
			info.AddValue("_clientCollectionViaMessage", (!this.MarkedForDeletion?_clientCollectionViaMessage:null));
			info.AddValue("_alwaysFetchClientCollectionViaMessage", _alwaysFetchClientCollectionViaMessage);
			info.AddValue("_alreadyFetchedClientCollectionViaMessage", _alreadyFetchedClientCollectionViaMessage);
			info.AddValue("_companyCollectionViaClient", (!this.MarkedForDeletion?_companyCollectionViaClient:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaClient", _alwaysFetchCompanyCollectionViaClient);
			info.AddValue("_alreadyFetchedCompanyCollectionViaClient", _alreadyFetchedCompanyCollectionViaClient);
			info.AddValue("_companyCollectionViaClient_", (!this.MarkedForDeletion?_companyCollectionViaClient_:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaClient_", _alwaysFetchCompanyCollectionViaClient_);
			info.AddValue("_alreadyFetchedCompanyCollectionViaClient_", _alreadyFetchedCompanyCollectionViaClient_);
			info.AddValue("_companyCollectionViaMessage", (!this.MarkedForDeletion?_companyCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMessage", _alwaysFetchCompanyCollectionViaMessage);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMessage", _alreadyFetchedCompanyCollectionViaMessage);
			info.AddValue("_customerCollectionViaMessage", (!this.MarkedForDeletion?_customerCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaMessage", _alwaysFetchCustomerCollectionViaMessage);
			info.AddValue("_alreadyFetchedCustomerCollectionViaMessage", _alreadyFetchedCustomerCollectionViaMessage);
			info.AddValue("_deliverypointCollectionViaClient", (!this.MarkedForDeletion?_deliverypointCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient", _alwaysFetchDeliverypointCollectionViaClient);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient", _alreadyFetchedDeliverypointCollectionViaClient);
			info.AddValue("_deliverypointCollectionViaClient_", (!this.MarkedForDeletion?_deliverypointCollectionViaClient_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient_", _alwaysFetchDeliverypointCollectionViaClient_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient_", _alreadyFetchedDeliverypointCollectionViaClient_);
			info.AddValue("_deliverypointgroupCollectionViaClient", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaClient", _alwaysFetchDeliverypointgroupCollectionViaClient);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaClient", _alreadyFetchedDeliverypointgroupCollectionViaClient);
			info.AddValue("_deliverypointgroupCollectionViaClient_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaClient_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaClient_", _alwaysFetchDeliverypointgroupCollectionViaClient_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaClient_", _alreadyFetchedDeliverypointgroupCollectionViaClient_);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage", _alwaysFetchDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_", _alwaysFetchDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_deliverypointgroupCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal", _alwaysFetchDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal", _alreadyFetchedDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_deliverypointgroupCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal_", _alwaysFetchDeliverypointgroupCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal_", _alreadyFetchedDeliverypointgroupCollectionViaTerminal_);
			info.AddValue("_deviceCollectionViaClient", (!this.MarkedForDeletion?_deviceCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaClient", _alwaysFetchDeviceCollectionViaClient);
			info.AddValue("_alreadyFetchedDeviceCollectionViaClient", _alreadyFetchedDeviceCollectionViaClient);
			info.AddValue("_deviceCollectionViaClient_", (!this.MarkedForDeletion?_deviceCollectionViaClient_:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaClient_", _alwaysFetchDeviceCollectionViaClient_);
			info.AddValue("_alreadyFetchedDeviceCollectionViaClient_", _alreadyFetchedDeviceCollectionViaClient_);
			info.AddValue("_deviceCollectionViaTerminal", (!this.MarkedForDeletion?_deviceCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal", _alwaysFetchDeviceCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal", _alreadyFetchedDeviceCollectionViaTerminal);
			info.AddValue("_deviceCollectionViaTerminal_", (!this.MarkedForDeletion?_deviceCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal_", _alwaysFetchDeviceCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal_", _alreadyFetchedDeviceCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_entertainmentCollectionViaTerminal___", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal___", _alwaysFetchEntertainmentCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal___", _alreadyFetchedEntertainmentCollectionViaTerminal___);
			info.AddValue("_entertainmentCollectionViaTerminal____", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal____:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal____", _alwaysFetchEntertainmentCollectionViaTerminal____);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal____", _alreadyFetchedEntertainmentCollectionViaTerminal____);
			info.AddValue("_entertainmentCollectionViaTerminal_____", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_____:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_____", _alwaysFetchEntertainmentCollectionViaTerminal_____);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_____", _alreadyFetchedEntertainmentCollectionViaTerminal_____);
			info.AddValue("_mediaCollectionViaMessage", (!this.MarkedForDeletion?_mediaCollectionViaMessage:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessage", _alwaysFetchMediaCollectionViaMessage);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessage", _alreadyFetchedMediaCollectionViaMessage);
			info.AddValue("_messagegroupCollectionViaMessagegroupDeliverypoint", (!this.MarkedForDeletion?_messagegroupCollectionViaMessagegroupDeliverypoint:null));
			info.AddValue("_alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint", _alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint);
			info.AddValue("_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint", _alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint);
			info.AddValue("_orderCollectionViaMessage", (!this.MarkedForDeletion?_orderCollectionViaMessage:null));
			info.AddValue("_alwaysFetchOrderCollectionViaMessage", _alwaysFetchOrderCollectionViaMessage);
			info.AddValue("_alreadyFetchedOrderCollectionViaMessage", _alreadyFetchedOrderCollectionViaMessage);
			info.AddValue("_orderCollectionViaOrder", (!this.MarkedForDeletion?_orderCollectionViaOrder:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrder", _alwaysFetchOrderCollectionViaOrder);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrder", _alreadyFetchedOrderCollectionViaOrder);
			info.AddValue("_terminalCollectionViaNetmessage", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage", _alwaysFetchTerminalCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage", _alreadyFetchedTerminalCollectionViaNetmessage);
			info.AddValue("_terminalCollectionViaNetmessage_", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage_", _alwaysFetchTerminalCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage_", _alreadyFetchedTerminalCollectionViaNetmessage_);
			info.AddValue("_terminalCollectionViaNetmessage__", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage__:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage__", _alwaysFetchTerminalCollectionViaNetmessage__);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage__", _alreadyFetchedTerminalCollectionViaNetmessage__);
			info.AddValue("_terminalCollectionViaNetmessage___", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage___:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage___", _alwaysFetchTerminalCollectionViaNetmessage___);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage___", _alreadyFetchedTerminalCollectionViaNetmessage___);
			info.AddValue("_terminalCollectionViaTerminal", (!this.MarkedForDeletion?_terminalCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal", _alwaysFetchTerminalCollectionViaTerminal);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal", _alreadyFetchedTerminalCollectionViaTerminal);
			info.AddValue("_terminalCollectionViaTerminal_", (!this.MarkedForDeletion?_terminalCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal_", _alwaysFetchTerminalCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal_", _alreadyFetchedTerminalCollectionViaTerminal_);
			info.AddValue("_uIModeCollectionViaTerminal", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal", _alwaysFetchUIModeCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal", _alreadyFetchedUIModeCollectionViaTerminal);
			info.AddValue("_uIModeCollectionViaTerminal_", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal_", _alwaysFetchUIModeCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal_", _alreadyFetchedUIModeCollectionViaTerminal_);
			info.AddValue("_userCollectionViaTerminal", (!this.MarkedForDeletion?_userCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUserCollectionViaTerminal", _alwaysFetchUserCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUserCollectionViaTerminal", _alreadyFetchedUserCollectionViaTerminal);
			info.AddValue("_userCollectionViaTerminal_", (!this.MarkedForDeletion?_userCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchUserCollectionViaTerminal_", _alwaysFetchUserCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedUserCollectionViaTerminal_", _alreadyFetchedUserCollectionViaTerminal_);
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_deviceEntity", (!this.MarkedForDeletion?_deviceEntity:null));
			info.AddValue("_deviceEntityReturnsNewIfNotFound", _deviceEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceEntity", _alwaysFetchDeviceEntity);
			info.AddValue("_alreadyFetchedDeviceEntity", _alreadyFetchedDeviceEntity);
			info.AddValue("_posdeliverypointEntity", (!this.MarkedForDeletion?_posdeliverypointEntity:null));
			info.AddValue("_posdeliverypointEntityReturnsNewIfNotFound", _posdeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosdeliverypointEntity", _alwaysFetchPosdeliverypointEntity);
			info.AddValue("_alreadyFetchedPosdeliverypointEntity", _alreadyFetchedPosdeliverypointEntity);
			info.AddValue("_roomControlAreaEntity", (!this.MarkedForDeletion?_roomControlAreaEntity:null));
			info.AddValue("_roomControlAreaEntityReturnsNewIfNotFound", _roomControlAreaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlAreaEntity", _alwaysFetchRoomControlAreaEntity);
			info.AddValue("_alreadyFetchedRoomControlAreaEntity", _alreadyFetchedRoomControlAreaEntity);
			info.AddValue("_roomControlConfigurationEntity", (!this.MarkedForDeletion?_roomControlConfigurationEntity:null));
			info.AddValue("_roomControlConfigurationEntityReturnsNewIfNotFound", _roomControlConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlConfigurationEntity", _alwaysFetchRoomControlConfigurationEntity);
			info.AddValue("_alreadyFetchedRoomControlConfigurationEntity", _alreadyFetchedRoomControlConfigurationEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "DeviceEntity":
					_alreadyFetchedDeviceEntity = true;
					this.DeviceEntity = (DeviceEntity)entity;
					break;
				case "PosdeliverypointEntity":
					_alreadyFetchedPosdeliverypointEntity = true;
					this.PosdeliverypointEntity = (PosdeliverypointEntity)entity;
					break;
				case "RoomControlAreaEntity":
					_alreadyFetchedRoomControlAreaEntity = true;
					this.RoomControlAreaEntity = (RoomControlAreaEntity)entity;
					break;
				case "RoomControlConfigurationEntity":
					_alreadyFetchedRoomControlConfigurationEntity = true;
					this.RoomControlConfigurationEntity = (RoomControlConfigurationEntity)entity;
					break;
				case "ClientCollection":
					_alreadyFetchedClientCollection = true;
					if(entity!=null)
					{
						this.ClientCollection.Add((ClientEntity)entity);
					}
					break;
				case "ClientCollection_":
					_alreadyFetchedClientCollection_ = true;
					if(entity!=null)
					{
						this.ClientCollection_.Add((ClientEntity)entity);
					}
					break;
				case "DeliverypointExternalDeliverypointCollection":
					_alreadyFetchedDeliverypointExternalDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointExternalDeliverypointCollection.Add((DeliverypointExternalDeliverypointEntity)entity);
					}
					break;
				case "GameSessionCollection":
					_alreadyFetchedGameSessionCollection = true;
					if(entity!=null)
					{
						this.GameSessionCollection.Add((GameSessionEntity)entity);
					}
					break;
				case "IcrtouchprintermappingDeliverypointCollection":
					_alreadyFetchedIcrtouchprintermappingDeliverypointCollection = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingDeliverypointCollection.Add((IcrtouchprintermappingDeliverypointEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessagegroupDeliverypointCollection":
					_alreadyFetchedMessagegroupDeliverypointCollection = true;
					if(entity!=null)
					{
						this.MessagegroupDeliverypointCollection.Add((MessagegroupDeliverypointEntity)entity);
					}
					break;
				case "MessageRecipientCollection":
					_alreadyFetchedMessageRecipientCollection = true;
					if(entity!=null)
					{
						this.MessageRecipientCollection.Add((MessageRecipientEntity)entity);
					}
					break;
				case "ReceivedNetmessageCollection":
					_alreadyFetchedReceivedNetmessageCollection = true;
					if(entity!=null)
					{
						this.ReceivedNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "SentNetmessageCollection":
					_alreadyFetchedSentNetmessageCollection = true;
					if(entity!=null)
					{
						this.SentNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollection_":
					_alreadyFetchedOrderCollection_ = true;
					if(entity!=null)
					{
						this.OrderCollection_.Add((OrderEntity)entity);
					}
					break;
				case "TerminalCollection_":
					_alreadyFetchedTerminalCollection_ = true;
					if(entity!=null)
					{
						this.TerminalCollection_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollection":
					_alreadyFetchedTerminalCollection = true;
					if(entity!=null)
					{
						this.TerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessage":
					_alreadyFetchedCategoryCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessage.Add((CategoryEntity)entity);
					}
					break;
				case "ClientCollectionViaMessage":
					_alreadyFetchedClientCollectionViaMessage = true;
					if(entity!=null)
					{
						this.ClientCollectionViaMessage.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaClient":
					_alreadyFetchedCompanyCollectionViaClient = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaClient.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaClient_":
					_alreadyFetchedCompanyCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaClient_.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMessage":
					_alreadyFetchedCompanyCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMessage.Add((CompanyEntity)entity);
					}
					break;
				case "CustomerCollectionViaMessage":
					_alreadyFetchedCustomerCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaMessage.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient":
					_alreadyFetchedDeliverypointCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient_":
					_alreadyFetchedDeliverypointCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaClient":
					_alreadyFetchedDeliverypointgroupCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaClient.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaClient_":
					_alreadyFetchedDeliverypointgroupCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaClient_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal_":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeviceCollectionViaClient":
					_alreadyFetchedDeviceCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaClient.Add((DeviceEntity)entity);
					}
					break;
				case "DeviceCollectionViaClient_":
					_alreadyFetchedDeviceCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaClient_.Add((DeviceEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal":
					_alreadyFetchedDeviceCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal.Add((DeviceEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal_":
					_alreadyFetchedDeviceCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal_.Add((DeviceEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal___":
					_alreadyFetchedEntertainmentCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal___.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal____":
					_alreadyFetchedEntertainmentCollectionViaTerminal____ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal____.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_____":
					_alreadyFetchedEntertainmentCollectionViaTerminal_____ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_____.Add((EntertainmentEntity)entity);
					}
					break;
				case "MediaCollectionViaMessage":
					_alreadyFetchedMediaCollectionViaMessage = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessage.Add((MediaEntity)entity);
					}
					break;
				case "MessagegroupCollectionViaMessagegroupDeliverypoint":
					_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint = true;
					if(entity!=null)
					{
						this.MessagegroupCollectionViaMessagegroupDeliverypoint.Add((MessagegroupEntity)entity);
					}
					break;
				case "OrderCollectionViaMessage":
					_alreadyFetchedOrderCollectionViaMessage = true;
					if(entity!=null)
					{
						this.OrderCollectionViaMessage.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrder":
					_alreadyFetchedOrderCollectionViaOrder = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrder.Add((OrderEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage":
					_alreadyFetchedTerminalCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage_":
					_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage__":
					_alreadyFetchedTerminalCollectionViaNetmessage__ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage__.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage___":
					_alreadyFetchedTerminalCollectionViaNetmessage___ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage___.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal":
					_alreadyFetchedTerminalCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal_":
					_alreadyFetchedTerminalCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal_.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal":
					_alreadyFetchedUIModeCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal_":
					_alreadyFetchedUIModeCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal_.Add((UIModeEntity)entity);
					}
					break;
				case "UserCollectionViaTerminal":
					_alreadyFetchedUserCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UserCollectionViaTerminal.Add((UserEntity)entity);
					}
					break;
				case "UserCollectionViaTerminal_":
					_alreadyFetchedUserCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.UserCollectionViaTerminal_.Add((UserEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "DeviceEntity":
					SetupSyncDeviceEntity(relatedEntity);
					break;
				case "PosdeliverypointEntity":
					SetupSyncPosdeliverypointEntity(relatedEntity);
					break;
				case "RoomControlAreaEntity":
					SetupSyncRoomControlAreaEntity(relatedEntity);
					break;
				case "RoomControlConfigurationEntity":
					SetupSyncRoomControlConfigurationEntity(relatedEntity);
					break;
				case "ClientCollection":
					_clientCollection.Add((ClientEntity)relatedEntity);
					break;
				case "ClientCollection_":
					_clientCollection_.Add((ClientEntity)relatedEntity);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					_deliverypointExternalDeliverypointCollection.Add((DeliverypointExternalDeliverypointEntity)relatedEntity);
					break;
				case "GameSessionCollection":
					_gameSessionCollection.Add((GameSessionEntity)relatedEntity);
					break;
				case "IcrtouchprintermappingDeliverypointCollection":
					_icrtouchprintermappingDeliverypointCollection.Add((IcrtouchprintermappingDeliverypointEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessagegroupDeliverypointCollection":
					_messagegroupDeliverypointCollection.Add((MessagegroupDeliverypointEntity)relatedEntity);
					break;
				case "MessageRecipientCollection":
					_messageRecipientCollection.Add((MessageRecipientEntity)relatedEntity);
					break;
				case "ReceivedNetmessageCollection":
					_receivedNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "SentNetmessageCollection":
					_sentNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "OrderCollection_":
					_orderCollection_.Add((OrderEntity)relatedEntity);
					break;
				case "TerminalCollection_":
					_terminalCollection_.Add((TerminalEntity)relatedEntity);
					break;
				case "TerminalCollection":
					_terminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "DeviceEntity":
					DesetupSyncDeviceEntity(false, true);
					break;
				case "PosdeliverypointEntity":
					DesetupSyncPosdeliverypointEntity(false, true);
					break;
				case "RoomControlAreaEntity":
					DesetupSyncRoomControlAreaEntity(false, true);
					break;
				case "RoomControlConfigurationEntity":
					DesetupSyncRoomControlConfigurationEntity(false, true);
					break;
				case "ClientCollection":
					this.PerformRelatedEntityRemoval(_clientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientCollection_":
					this.PerformRelatedEntityRemoval(_clientCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointExternalDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GameSessionCollection":
					this.PerformRelatedEntityRemoval(_gameSessionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "IcrtouchprintermappingDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_icrtouchprintermappingDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessagegroupDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_messagegroupDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageRecipientCollection":
					this.PerformRelatedEntityRemoval(_messageRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceivedNetmessageCollection":
					this.PerformRelatedEntityRemoval(_receivedNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SentNetmessageCollection":
					this.PerformRelatedEntityRemoval(_sentNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection_":
					this.PerformRelatedEntityRemoval(_orderCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection_":
					this.PerformRelatedEntityRemoval(_terminalCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection":
					this.PerformRelatedEntityRemoval(_terminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_deviceEntity!=null)
			{
				toReturn.Add(_deviceEntity);
			}
			if(_posdeliverypointEntity!=null)
			{
				toReturn.Add(_posdeliverypointEntity);
			}
			if(_roomControlAreaEntity!=null)
			{
				toReturn.Add(_roomControlAreaEntity);
			}
			if(_roomControlConfigurationEntity!=null)
			{
				toReturn.Add(_roomControlConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientCollection);
			toReturn.Add(_clientCollection_);
			toReturn.Add(_deliverypointExternalDeliverypointCollection);
			toReturn.Add(_gameSessionCollection);
			toReturn.Add(_icrtouchprintermappingDeliverypointCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messagegroupDeliverypointCollection);
			toReturn.Add(_messageRecipientCollection);
			toReturn.Add(_receivedNetmessageCollection);
			toReturn.Add(_sentNetmessageCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_orderCollection_);
			toReturn.Add(_terminalCollection_);
			toReturn.Add(_terminalCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupIdNumber(System.Int32 deliverypointgroupId, System.String number)
		{
			return FetchUsingUCDeliverypointgroupIdNumber( deliverypointgroupId,  number, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupIdNumber(System.Int32 deliverypointgroupId, System.String number, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCDeliverypointgroupIdNumber( deliverypointgroupId,  number, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupIdNumber(System.Int32 deliverypointgroupId, System.String number, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCDeliverypointgroupIdNumber( deliverypointgroupId,  number, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupIdNumber(System.Int32 deliverypointgroupId, System.String number, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((DeliverypointDAO)CreateDAOInstance()).FetchDeliverypointUsingUCDeliverypointgroupIdNumber(this, this.Transaction, deliverypointgroupId, number, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointId)
		{
			return FetchUsingPK(deliverypointId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deliverypointId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientCollection || forceFetch || _alwaysFetchClientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollection);
				_clientCollection.SuppressClearInGetMulti=!forceFetch;
				_clientCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientCollection.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_clientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollection = true;
			}
			return _clientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollection'. These settings will be taken into account
		/// when the property ClientCollection is requested or GetMultiClientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollection.SortClauses=sortClauses;
			_clientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection_(bool forceFetch)
		{
			return GetMultiClientCollection_(forceFetch, _clientCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientCollection_(forceFetch, _clientCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientCollection_ || forceFetch || _alwaysFetchClientCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollection_);
				_clientCollection_.SuppressClearInGetMulti=!forceFetch;
				_clientCollection_.EntityFactoryToUse = entityFactoryToUse;
				_clientCollection_.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_clientCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollection_ = true;
			}
			return _clientCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollection_'. These settings will be taken into account
		/// when the property ClientCollection_ is requested or GetMultiClientCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollection_.SortClauses=sortClauses;
			_clientCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, _deliverypointExternalDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, _deliverypointExternalDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointExternalDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointExternalDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointExternalDeliverypointCollection);
				_deliverypointExternalDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointExternalDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointExternalDeliverypointCollection.GetMultiManyToOne(this, null, filter);
				_deliverypointExternalDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointExternalDeliverypointCollection = true;
			}
			return _deliverypointExternalDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointExternalDeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointExternalDeliverypointCollection is requested or GetMultiDeliverypointExternalDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointExternalDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointExternalDeliverypointCollection.SortClauses=sortClauses;
			_deliverypointExternalDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GameSessionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch)
		{
			return GetMultiGameSessionCollection(forceFetch, _gameSessionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GameSessionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGameSessionCollection(forceFetch, _gameSessionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGameSessionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGameSessionCollection || forceFetch || _alwaysFetchGameSessionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_gameSessionCollection);
				_gameSessionCollection.SuppressClearInGetMulti=!forceFetch;
				_gameSessionCollection.EntityFactoryToUse = entityFactoryToUse;
				_gameSessionCollection.GetMultiManyToOne(null, this, null, filter);
				_gameSessionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGameSessionCollection = true;
			}
			return _gameSessionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GameSessionCollection'. These settings will be taken into account
		/// when the property GameSessionCollection is requested or GetMultiGameSessionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGameSessionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_gameSessionCollection.SortClauses=sortClauses;
			_gameSessionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection GetMultiIcrtouchprintermappingDeliverypointCollection(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingDeliverypointCollection(forceFetch, _icrtouchprintermappingDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection GetMultiIcrtouchprintermappingDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiIcrtouchprintermappingDeliverypointCollection(forceFetch, _icrtouchprintermappingDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection GetMultiIcrtouchprintermappingDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiIcrtouchprintermappingDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection GetMultiIcrtouchprintermappingDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingDeliverypointCollection || forceFetch || _alwaysFetchIcrtouchprintermappingDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingDeliverypointCollection);
				_icrtouchprintermappingDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingDeliverypointCollection.GetMultiManyToOne(this, null, filter);
				_icrtouchprintermappingDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingDeliverypointCollection = true;
			}
			return _icrtouchprintermappingDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingDeliverypointCollection'. These settings will be taken into account
		/// when the property IcrtouchprintermappingDeliverypointCollection is requested or GetMultiIcrtouchprintermappingDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingDeliverypointCollection.SortClauses=sortClauses;
			_icrtouchprintermappingDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessagegroupDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, _messagegroupDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessagegroupDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, _messagegroupDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessagegroupDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection GetMultiMessagegroupDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessagegroupDeliverypointCollection || forceFetch || _alwaysFetchMessagegroupDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messagegroupDeliverypointCollection);
				_messagegroupDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_messagegroupDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_messagegroupDeliverypointCollection.GetMultiManyToOne(this, null, filter);
				_messagegroupDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessagegroupDeliverypointCollection = true;
			}
			return _messagegroupDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessagegroupDeliverypointCollection'. These settings will be taken into account
		/// when the property MessagegroupDeliverypointCollection is requested or GetMultiMessagegroupDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessagegroupDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messagegroupDeliverypointCollection.SortClauses=sortClauses;
			_messagegroupDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageRecipientCollection || forceFetch || _alwaysFetchMessageRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageRecipientCollection);
				_messageRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_messageRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageRecipientCollection.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_messageRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageRecipientCollection = true;
			}
			return _messageRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageRecipientCollection'. These settings will be taken into account
		/// when the property MessageRecipientCollection is requested or GetMultiMessageRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageRecipientCollection.SortClauses=sortClauses;
			_messageRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceivedNetmessageCollection || forceFetch || _alwaysFetchReceivedNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receivedNetmessageCollection);
				_receivedNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_receivedNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_receivedNetmessageCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, filter);
				_receivedNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceivedNetmessageCollection = true;
			}
			return _receivedNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceivedNetmessageCollection'. These settings will be taken into account
		/// when the property ReceivedNetmessageCollection is requested or GetMultiReceivedNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceivedNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receivedNetmessageCollection.SortClauses=sortClauses;
			_receivedNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSentNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSentNetmessageCollection || forceFetch || _alwaysFetchSentNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sentNetmessageCollection);
				_sentNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_sentNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_sentNetmessageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, filter);
				_sentNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSentNetmessageCollection = true;
			}
			return _sentNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SentNetmessageCollection'. These settings will be taken into account
		/// when the property SentNetmessageCollection is requested or GetMultiSentNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSentNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sentNetmessageCollection.SortClauses=sortClauses;
			_sentNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection_(bool forceFetch)
		{
			return GetMultiOrderCollection_(forceFetch, _orderCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection_(forceFetch, _orderCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection_ || forceFetch || _alwaysFetchOrderCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection_);
				_orderCollection_.SuppressClearInGetMulti=!forceFetch;
				_orderCollection_.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection_.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, filter);
				_orderCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection_ = true;
			}
			return _orderCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection_'. These settings will be taken into account
		/// when the property OrderCollection_ is requested or GetMultiOrderCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection_.SortClauses=sortClauses;
			_orderCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection_(bool forceFetch)
		{
			return GetMultiTerminalCollection_(forceFetch, _terminalCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection_(forceFetch, _terminalCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection_ || forceFetch || _alwaysFetchTerminalCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection_);
				_terminalCollection_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection_.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_terminalCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection_ = true;
			}
			return _terminalCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection_'. These settings will be taken into account
		/// when the property TerminalCollection_ is requested or GetMultiTerminalCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection_.SortClauses=sortClauses;
			_terminalCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection || forceFetch || _alwaysFetchTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection);
				_terminalCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_terminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection = true;
			}
			return _terminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection'. These settings will be taken into account
		/// when the property TerminalCollection is requested or GetMultiTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection.SortClauses=sortClauses;
			_terminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessage(forceFetch, _categoryCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessage || forceFetch || _alwaysFetchCategoryCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessage.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessage"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessage = true;
			}
			return _categoryCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessage'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessage is requested or GetMultiCategoryCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessage.SortClauses=sortClauses;
			_categoryCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch)
		{
			return GetMultiClientCollectionViaMessage(forceFetch, _clientCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaMessage || forceFetch || _alwaysFetchClientCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaMessage.GetMulti(filter, GetRelationsForField("ClientCollectionViaMessage"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaMessage = true;
			}
			return _clientCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaMessage'. These settings will be taken into account
		/// when the property ClientCollectionViaMessage is requested or GetMultiClientCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaMessage.SortClauses=sortClauses;
			_clientCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaClient(forceFetch, _companyCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaClient || forceFetch || _alwaysFetchCompanyCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_companyCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaClient.GetMulti(filter, GetRelationsForField("CompanyCollectionViaClient"));
				_companyCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaClient = true;
			}
			return _companyCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaClient'. These settings will be taken into account
		/// when the property CompanyCollectionViaClient is requested or GetMultiCompanyCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaClient.SortClauses=sortClauses;
			_companyCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient_(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaClient_(forceFetch, _companyCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaClient_ || forceFetch || _alwaysFetchCompanyCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_companyCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaClient_.GetMulti(filter, GetRelationsForField("CompanyCollectionViaClient_"));
				_companyCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaClient_ = true;
			}
			return _companyCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaClient_'. These settings will be taken into account
		/// when the property CompanyCollectionViaClient_ is requested or GetMultiCompanyCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaClient_.SortClauses=sortClauses;
			_companyCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMessage(forceFetch, _companyCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMessage || forceFetch || _alwaysFetchCompanyCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMessage.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMessage"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMessage = true;
			}
			return _companyCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMessage'. These settings will be taken into account
		/// when the property CompanyCollectionViaMessage is requested or GetMultiCompanyCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMessage.SortClauses=sortClauses;
			_companyCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaMessage(forceFetch, _customerCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaMessage || forceFetch || _alwaysFetchCustomerCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaMessage.GetMulti(filter, GetRelationsForField("CustomerCollectionViaMessage"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaMessage = true;
			}
			return _customerCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaMessage'. These settings will be taken into account
		/// when the property CustomerCollectionViaMessage is requested or GetMultiCustomerCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaMessage.SortClauses=sortClauses;
			_customerCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient(forceFetch, _deliverypointCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient || forceFetch || _alwaysFetchDeliverypointCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient = true;
			}
			return _deliverypointCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient is requested or GetMultiDeliverypointCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient.SortClauses=sortClauses;
			_deliverypointCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient_(forceFetch, _deliverypointCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient_ || forceFetch || _alwaysFetchDeliverypointCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient_"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient_ = true;
			}
			return _deliverypointCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient_ is requested or GetMultiDeliverypointCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient_.SortClauses=sortClauses;
			_deliverypointCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaClient(forceFetch, _deliverypointgroupCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaClient || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaClient.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaClient"));
				_deliverypointgroupCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaClient = true;
			}
			return _deliverypointgroupCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaClient'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaClient is requested or GetMultiDeliverypointgroupCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaClient.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaClient_(forceFetch, _deliverypointgroupCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaClient_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaClient_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaClient_"));
				_deliverypointgroupCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaClient_ = true;
			}
			return _deliverypointgroupCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaClient_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaClient_ is requested or GetMultiDeliverypointgroupCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaClient_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage(forceFetch, _deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
			}
			return _deliverypointgroupCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage is requested or GetMultiDeliverypointgroupCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage_(forceFetch, _deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
			}
			return _deliverypointgroupCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage_ is requested or GetMultiDeliverypointgroupCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal(forceFetch, _deliverypointgroupCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
			}
			return _deliverypointgroupCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal is requested or GetMultiDeliverypointgroupCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal_(forceFetch, _deliverypointgroupCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deliverypointgroupCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal_"));
				_deliverypointgroupCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ = true;
			}
			return _deliverypointgroupCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal_ is requested or GetMultiDeliverypointgroupCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaClient(forceFetch, _deviceCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaClient || forceFetch || _alwaysFetchDeviceCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deviceCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaClient.GetMulti(filter, GetRelationsForField("DeviceCollectionViaClient"));
				_deviceCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaClient = true;
			}
			return _deviceCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaClient'. These settings will be taken into account
		/// when the property DeviceCollectionViaClient is requested or GetMultiDeviceCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaClient.SortClauses=sortClauses;
			_deviceCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient_(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaClient_(forceFetch, _deviceCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaClient_ || forceFetch || _alwaysFetchDeviceCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deviceCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaClient_.GetMulti(filter, GetRelationsForField("DeviceCollectionViaClient_"));
				_deviceCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaClient_ = true;
			}
			return _deviceCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaClient_'. These settings will be taken into account
		/// when the property DeviceCollectionViaClient_ is requested or GetMultiDeviceCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaClient_.SortClauses=sortClauses;
			_deviceCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal(forceFetch, _deviceCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal || forceFetch || _alwaysFetchDeviceCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal = true;
			}
			return _deviceCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal is requested or GetMultiDeviceCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal.SortClauses=sortClauses;
			_deviceCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal_(forceFetch, _deviceCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal_ || forceFetch || _alwaysFetchDeviceCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_deviceCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal_"));
				_deviceCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal_ = true;
			}
			return _deviceCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal_ is requested or GetMultiDeviceCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal_.SortClauses=sortClauses;
			_deviceCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal___(forceFetch, _entertainmentCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal___ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal___"));
				_entertainmentCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal___ = true;
			}
			return _entertainmentCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal___'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal___ is requested or GetMultiEntertainmentCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal___.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal____(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal____(forceFetch, _entertainmentCollectionViaTerminal____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal____ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal____.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal____.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal____.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal____"));
				_entertainmentCollectionViaTerminal____.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal____ = true;
			}
			return _entertainmentCollectionViaTerminal____;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal____'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal____ is requested or GetMultiEntertainmentCollectionViaTerminal____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal____.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_____(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_____(forceFetch, _entertainmentCollectionViaTerminal_____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_____ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_entertainmentCollectionViaTerminal_____.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_____.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_____.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_____"));
				_entertainmentCollectionViaTerminal_____.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_____ = true;
			}
			return _entertainmentCollectionViaTerminal_____;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_____'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_____ is requested or GetMultiEntertainmentCollectionViaTerminal_____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_____.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessage(forceFetch, _mediaCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessage || forceFetch || _alwaysFetchMediaCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessage.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessage"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessage = true;
			}
			return _mediaCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessage'. These settings will be taken into account
		/// when the property MediaCollectionViaMessage is requested or GetMultiMediaCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessage.SortClauses=sortClauses;
			_mediaCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessagegroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupCollection GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint(bool forceFetch)
		{
			return GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint(forceFetch, _messagegroupCollectionViaMessagegroupDeliverypoint.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessagegroupCollection GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint || forceFetch || _alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messagegroupCollectionViaMessagegroupDeliverypoint);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_messagegroupCollectionViaMessagegroupDeliverypoint.SuppressClearInGetMulti=!forceFetch;
				_messagegroupCollectionViaMessagegroupDeliverypoint.EntityFactoryToUse = entityFactoryToUse;
				_messagegroupCollectionViaMessagegroupDeliverypoint.GetMulti(filter, GetRelationsForField("MessagegroupCollectionViaMessagegroupDeliverypoint"));
				_messagegroupCollectionViaMessagegroupDeliverypoint.SuppressClearInGetMulti=false;
				_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint = true;
			}
			return _messagegroupCollectionViaMessagegroupDeliverypoint;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessagegroupCollectionViaMessagegroupDeliverypoint'. These settings will be taken into account
		/// when the property MessagegroupCollectionViaMessagegroupDeliverypoint is requested or GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessagegroupCollectionViaMessagegroupDeliverypoint(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messagegroupCollectionViaMessagegroupDeliverypoint.SortClauses=sortClauses;
			_messagegroupCollectionViaMessagegroupDeliverypoint.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch)
		{
			return GetMultiOrderCollectionViaMessage(forceFetch, _orderCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaMessage || forceFetch || _alwaysFetchOrderCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaMessage.GetMulti(filter, GetRelationsForField("OrderCollectionViaMessage"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaMessage = true;
			}
			return _orderCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaMessage'. These settings will be taken into account
		/// when the property OrderCollectionViaMessage is requested or GetMultiOrderCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaMessage.SortClauses=sortClauses;
			_orderCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrder(forceFetch, _orderCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrder || forceFetch || _alwaysFetchOrderCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrder.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrder"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrder = true;
			}
			return _orderCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrder'. These settings will be taken into account
		/// when the property OrderCollectionViaOrder is requested or GetMultiOrderCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrder.SortClauses=sortClauses;
			_orderCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage(forceFetch, _terminalCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage = true;
			}
			return _terminalCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage is requested or GetMultiTerminalCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage_(forceFetch, _terminalCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage_ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage_"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
			}
			return _terminalCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage_ is requested or GetMultiTerminalCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage_.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage__(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage__(forceFetch, _terminalCollectionViaNetmessage__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage__ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaNetmessage__.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage__.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage__.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage__"));
				_terminalCollectionViaNetmessage__.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage__ = true;
			}
			return _terminalCollectionViaNetmessage__;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage__'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage__ is requested or GetMultiTerminalCollectionViaNetmessage__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage__.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage___(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage___(forceFetch, _terminalCollectionViaNetmessage___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage___ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaNetmessage___.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage___.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage___.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage___"));
				_terminalCollectionViaNetmessage___.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage___ = true;
			}
			return _terminalCollectionViaNetmessage___;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage___'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage___ is requested or GetMultiTerminalCollectionViaNetmessage___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage___.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal(forceFetch, _terminalCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal || forceFetch || _alwaysFetchTerminalCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal = true;
			}
			return _terminalCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal is requested or GetMultiTerminalCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal.SortClauses=sortClauses;
			_terminalCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal_(forceFetch, _terminalCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal_ || forceFetch || _alwaysFetchTerminalCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_terminalCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal_"));
				_terminalCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal_ = true;
			}
			return _terminalCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal_'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal_ is requested or GetMultiTerminalCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal_.SortClauses=sortClauses;
			_terminalCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal(forceFetch, _uIModeCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal || forceFetch || _alwaysFetchUIModeCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal = true;
			}
			return _uIModeCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal is requested or GetMultiUIModeCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal_(forceFetch, _uIModeCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal_ || forceFetch || _alwaysFetchUIModeCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_uIModeCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal_"));
				_uIModeCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal_ = true;
			}
			return _uIModeCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal_'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal_ is requested or GetMultiUIModeCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal_.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUserCollectionViaTerminal(forceFetch, _userCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaTerminal || forceFetch || _alwaysFetchUserCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UserCollectionViaTerminal"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaTerminal = true;
			}
			return _userCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaTerminal'. These settings will be taken into account
		/// when the property UserCollectionViaTerminal is requested or GetMultiUserCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaTerminal.SortClauses=sortClauses;
			_userCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiUserCollectionViaTerminal_(forceFetch, _userCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaTerminal_ || forceFetch || _alwaysFetchUserCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointFields.DeliverypointId, ComparisonOperator.Equal, this.DeliverypointId, "DeliverypointEntity__"));
				_userCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("UserCollectionViaTerminal_"));
				_userCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaTerminal_ = true;
			}
			return _userCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaTerminal_'. These settings will be taken into account
		/// when the property UserCollectionViaTerminal_ is requested or GetMultiUserCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaTerminal_.SortClauses=sortClauses;
			_userCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId);
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public DeviceEntity GetSingleDeviceEntity()
		{
			return GetSingleDeviceEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public virtual DeviceEntity GetSingleDeviceEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceEntity || forceFetch || _alwaysFetchDeviceEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceEntityUsingDeviceId);
				DeviceEntity newEntity = new DeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceEntity = newEntity;
				_alreadyFetchedDeviceEntity = fetchResult;
			}
			return _deviceEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosdeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosdeliverypointEntity' which is related to this entity.</returns>
		public PosdeliverypointEntity GetSinglePosdeliverypointEntity()
		{
			return GetSinglePosdeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosdeliverypointEntity' which is related to this entity.</returns>
		public virtual PosdeliverypointEntity GetSinglePosdeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosdeliverypointEntity || forceFetch || _alwaysFetchPosdeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosdeliverypointEntityUsingPosdeliverypointId);
				PosdeliverypointEntity newEntity = new PosdeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosdeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosdeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posdeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosdeliverypointEntity = newEntity;
				_alreadyFetchedPosdeliverypointEntity = fetchResult;
			}
			return _posdeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public RoomControlAreaEntity GetSingleRoomControlAreaEntity()
		{
			return GetSingleRoomControlAreaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public virtual RoomControlAreaEntity GetSingleRoomControlAreaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlAreaEntity || forceFetch || _alwaysFetchRoomControlAreaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
				RoomControlAreaEntity newEntity = new RoomControlAreaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlAreaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlAreaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlAreaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlAreaEntity = newEntity;
				_alreadyFetchedRoomControlAreaEntity = fetchResult;
			}
			return _roomControlAreaEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity()
		{
			return GetSingleRoomControlConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public virtual RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlConfigurationEntity || forceFetch || _alwaysFetchRoomControlConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
				RoomControlConfigurationEntity newEntity = new RoomControlConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlConfigurationEntity = newEntity;
				_alreadyFetchedRoomControlConfigurationEntity = fetchResult;
			}
			return _roomControlConfigurationEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingDeliverypointId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCDeliverypointId(this.DeliverypointId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("DeviceEntity", _deviceEntity);
			toReturn.Add("PosdeliverypointEntity", _posdeliverypointEntity);
			toReturn.Add("RoomControlAreaEntity", _roomControlAreaEntity);
			toReturn.Add("RoomControlConfigurationEntity", _roomControlConfigurationEntity);
			toReturn.Add("ClientCollection", _clientCollection);
			toReturn.Add("ClientCollection_", _clientCollection_);
			toReturn.Add("DeliverypointExternalDeliverypointCollection", _deliverypointExternalDeliverypointCollection);
			toReturn.Add("GameSessionCollection", _gameSessionCollection);
			toReturn.Add("IcrtouchprintermappingDeliverypointCollection", _icrtouchprintermappingDeliverypointCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessagegroupDeliverypointCollection", _messagegroupDeliverypointCollection);
			toReturn.Add("MessageRecipientCollection", _messageRecipientCollection);
			toReturn.Add("ReceivedNetmessageCollection", _receivedNetmessageCollection);
			toReturn.Add("SentNetmessageCollection", _sentNetmessageCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("OrderCollection_", _orderCollection_);
			toReturn.Add("TerminalCollection_", _terminalCollection_);
			toReturn.Add("TerminalCollection", _terminalCollection);
			toReturn.Add("CategoryCollectionViaMessage", _categoryCollectionViaMessage);
			toReturn.Add("ClientCollectionViaMessage", _clientCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaClient", _companyCollectionViaClient);
			toReturn.Add("CompanyCollectionViaClient_", _companyCollectionViaClient_);
			toReturn.Add("CompanyCollectionViaMessage", _companyCollectionViaMessage);
			toReturn.Add("CustomerCollectionViaMessage", _customerCollectionViaMessage);
			toReturn.Add("DeliverypointCollectionViaClient", _deliverypointCollectionViaClient);
			toReturn.Add("DeliverypointCollectionViaClient_", _deliverypointCollectionViaClient_);
			toReturn.Add("DeliverypointgroupCollectionViaClient", _deliverypointgroupCollectionViaClient);
			toReturn.Add("DeliverypointgroupCollectionViaClient_", _deliverypointgroupCollectionViaClient_);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage", _deliverypointgroupCollectionViaNetmessage);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage_", _deliverypointgroupCollectionViaNetmessage_);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal", _deliverypointgroupCollectionViaTerminal);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal_", _deliverypointgroupCollectionViaTerminal_);
			toReturn.Add("DeviceCollectionViaClient", _deviceCollectionViaClient);
			toReturn.Add("DeviceCollectionViaClient_", _deviceCollectionViaClient_);
			toReturn.Add("DeviceCollectionViaTerminal", _deviceCollectionViaTerminal);
			toReturn.Add("DeviceCollectionViaTerminal_", _deviceCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("EntertainmentCollectionViaTerminal___", _entertainmentCollectionViaTerminal___);
			toReturn.Add("EntertainmentCollectionViaTerminal____", _entertainmentCollectionViaTerminal____);
			toReturn.Add("EntertainmentCollectionViaTerminal_____", _entertainmentCollectionViaTerminal_____);
			toReturn.Add("MediaCollectionViaMessage", _mediaCollectionViaMessage);
			toReturn.Add("MessagegroupCollectionViaMessagegroupDeliverypoint", _messagegroupCollectionViaMessagegroupDeliverypoint);
			toReturn.Add("OrderCollectionViaMessage", _orderCollectionViaMessage);
			toReturn.Add("OrderCollectionViaOrder", _orderCollectionViaOrder);
			toReturn.Add("TerminalCollectionViaNetmessage", _terminalCollectionViaNetmessage);
			toReturn.Add("TerminalCollectionViaNetmessage_", _terminalCollectionViaNetmessage_);
			toReturn.Add("TerminalCollectionViaNetmessage__", _terminalCollectionViaNetmessage__);
			toReturn.Add("TerminalCollectionViaNetmessage___", _terminalCollectionViaNetmessage___);
			toReturn.Add("TerminalCollectionViaTerminal", _terminalCollectionViaTerminal);
			toReturn.Add("TerminalCollectionViaTerminal_", _terminalCollectionViaTerminal_);
			toReturn.Add("UIModeCollectionViaTerminal", _uIModeCollectionViaTerminal);
			toReturn.Add("UIModeCollectionViaTerminal_", _uIModeCollectionViaTerminal_);
			toReturn.Add("UserCollectionViaTerminal", _userCollectionViaTerminal);
			toReturn.Add("UserCollectionViaTerminal_", _userCollectionViaTerminal_);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="validator">The validator object for this DeliverypointEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deliverypointId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deliverypointId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientCollection = new Obymobi.Data.CollectionClasses.ClientCollection();
			_clientCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_clientCollection_ = new Obymobi.Data.CollectionClasses.ClientCollection();
			_clientCollection_.SetContainingEntityInfo(this, "LastDeliverypointEntity");

			_deliverypointExternalDeliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection();
			_deliverypointExternalDeliverypointCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_gameSessionCollection = new Obymobi.Data.CollectionClasses.GameSessionCollection();
			_gameSessionCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_icrtouchprintermappingDeliverypointCollection = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection();
			_icrtouchprintermappingDeliverypointCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_messagegroupDeliverypointCollection = new Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection();
			_messagegroupDeliverypointCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_messageRecipientCollection = new Obymobi.Data.CollectionClasses.MessageRecipientCollection();
			_messageRecipientCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_receivedNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_receivedNetmessageCollection.SetContainingEntityInfo(this, "ReceiverDeliverypointEntity");

			_sentNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_sentNetmessageCollection.SetContainingEntityInfo(this, "SenderDeliverypointEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "DeliverypointEntity");

			_orderCollection_ = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection_.SetContainingEntityInfo(this, "DeliverypointEntity_");

			_terminalCollection_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection_.SetContainingEntityInfo(this, "AltSystemMessagesDeliverypointEntity");

			_terminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection.SetContainingEntityInfo(this, "SystemMessagesDeliverypointEntity");
			_categoryCollectionViaMessage = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientCollectionViaMessage = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaClient = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaClient_ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMessage = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_customerCollectionViaMessage = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaClient = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaClient_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaClient = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaClient_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deviceCollectionViaClient = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_deviceCollectionViaClient_ = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_deviceCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_deviceCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal____ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_____ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_mediaCollectionViaMessage = new Obymobi.Data.CollectionClasses.MediaCollection();
			_messagegroupCollectionViaMessagegroupDeliverypoint = new Obymobi.Data.CollectionClasses.MessagegroupCollection();
			_orderCollectionViaMessage = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrder = new Obymobi.Data.CollectionClasses.OrderCollection();
			_terminalCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage__ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage___ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaTerminal = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_userCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UserCollection();
			_userCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.UserCollection();
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_deviceEntityReturnsNewIfNotFound = true;
			_posdeliverypointEntityReturnsNewIfNotFound = true;
			_roomControlAreaEntityReturnsNewIfNotFound = true;
			_roomControlConfigurationEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosdeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControllerIp", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePrinterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePrinterName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotSOSRoomId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnableAnalytics", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControllerSlaveId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControllerType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] { "DeliverypointgroupName" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Name":
					this.OnPropertyChanged("DeliverypointgroupName");
					break;
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.DeviceEntityUsingDeviceIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.DeviceId } );		
			_deviceEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deviceEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceEntity(IEntityCore relatedEntity)
		{
			if(_deviceEntity!=relatedEntity)
			{		
				DesetupSyncDeviceEntity(true, true);
				_deviceEntity = (DeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.DeviceEntityUsingDeviceIdStatic, true, ref _alreadyFetchedDeviceEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posdeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosdeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posdeliverypointEntity, new PropertyChangedEventHandler( OnPosdeliverypointEntityPropertyChanged ), "PosdeliverypointEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.PosdeliverypointEntityUsingPosdeliverypointIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.PosdeliverypointId } );		
			_posdeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posdeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosdeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_posdeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncPosdeliverypointEntity(true, true);
				_posdeliverypointEntity = (PosdeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posdeliverypointEntity, new PropertyChangedEventHandler( OnPosdeliverypointEntityPropertyChanged ), "PosdeliverypointEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.PosdeliverypointEntityUsingPosdeliverypointIdStatic, true, ref _alreadyFetchedPosdeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosdeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlAreaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.RoomControlAreaId } );		
			_roomControlAreaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlAreaEntity(IEntityCore relatedEntity)
		{
			if(_roomControlAreaEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlAreaEntity(true, true);
				_roomControlAreaEntity = (RoomControlAreaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, ref _alreadyFetchedRoomControlAreaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlAreaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, signalRelatedEntity, "DeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointFieldIndex.RoomControlConfigurationId } );		
			_roomControlConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_roomControlConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlConfigurationEntity(true, true);
				_roomControlConfigurationEntity = (RoomControlConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, ref _alreadyFetchedRoomControlConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.TimestampEntityUsingDeliverypointIdStatic, false, signalRelatedEntity, "DeliverypointEntity", false, new int[] { (int)DeliverypointFieldIndex.DeliverypointId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticDeliverypointRelations.TimestampEntityUsingDeliverypointIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeliverypointFieldIndex.DeliverypointId].ForcedCurrentValueWrite(deliverypointId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointRelations Relations
		{
			get	{ return new DeliverypointRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientCollection_")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointExternalDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointExternalDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity, 0, null, null, null, "DeliverypointExternalDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GameSession' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGameSessionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GameSessionCollection(), (IEntityRelation)GetRelationsForField("GameSessionCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.GameSessionEntity, 0, null, null, null, "GameSessionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'IcrtouchprintermappingDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection(), (IEntityRelation)GetRelationsForField("IcrtouchprintermappingDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity, 0, null, null, null, "IcrtouchprintermappingDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessagegroupDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection(), (IEntityRelation)GetRelationsForField("MessagegroupDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.MessagegroupDeliverypointEntity, 0, null, null, null, "MessagegroupDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageRecipientCollection(), (IEntityRelation)GetRelationsForField("MessageRecipientCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.MessageRecipientEntity, 0, null, null, null, "MessageRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceivedNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("ReceivedNetmessageCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "ReceivedNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSentNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("SentNetmessageCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "SentNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection_")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection_")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessage"), "CategoryCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaMessage"), "ClientCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaClient"), "CompanyCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingLastDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaClient_"), "CompanyCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMessage"), "CompanyCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaMessage"), "CustomerCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient"), "DeliverypointCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient_"), "DeliverypointCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaClient"), "DeliverypointgroupCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingLastDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaClient_"), "DeliverypointgroupCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"), "DeliverypointgroupCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"), "DeliverypointgroupCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"), "DeliverypointgroupCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal_"), "DeliverypointgroupCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaClient"), "DeviceCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingLastDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaClient_"), "DeviceCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal"), "DeviceCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal_"), "DeviceCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal___"), "EntertainmentCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal____"), "EntertainmentCollectionViaTerminal____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_____"), "EntertainmentCollectionViaTerminal_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessage"), "MediaCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Messagegroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupCollectionViaMessagegroupDeliverypoint
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessagegroupDeliverypointEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "MessagegroupDeliverypoint_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.MessagegroupEntity, 0, null, null, GetRelationsForField("MessagegroupCollectionViaMessagegroupDeliverypoint"), "MessagegroupCollectionViaMessagegroupDeliverypoint", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaMessage"), "OrderCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrder"), "OrderCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage"), "TerminalCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage_"), "TerminalCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage__"), "TerminalCollectionViaNetmessage__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage___"), "TerminalCollectionViaNetmessage___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal"), "TerminalCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal_"), "TerminalCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal"), "UIModeCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal_"), "UIModeCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAltSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaTerminal"), "UserCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingSystemMessagesDeliverypointId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaTerminal_"), "UserCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("DeviceEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, null, "DeviceEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointCollection(), (IEntityRelation)GetRelationsForField("PosdeliverypointEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.PosdeliverypointEntity, 0, null, null, null, "PosdeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlArea'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.RoomControlAreaEntity, 0, null, null, null, "RoomControlAreaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlConfigurationCollection(), (IEntityRelation)GetRelationsForField("RoomControlConfigurationEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.RoomControlConfigurationEntity, 0, null, null, null, "RoomControlConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.DeliverypointId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The Name property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.Name, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."Number"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Number
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.Number, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.Number, value, true); }
		}

		/// <summary> The PosdeliverypointId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."PosdeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosdeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.PosdeliverypointId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.PosdeliverypointId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.CompanyId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The RoomControllerIp property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerIp"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControllerIp
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.RoomControllerIp, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerIp, value, true); }
		}

		/// <summary> The GooglePrinterId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."GooglePrinterId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.GooglePrinterId, value, true); }
		}

		/// <summary> The GooglePrinterName property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."GooglePrinterName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterName
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.GooglePrinterName, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.GooglePrinterName, value, true); }
		}

		/// <summary> The HotSOSRoomId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."HotSOSRoomId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotSOSRoomId
		{
			get { return (System.String)GetValue((int)DeliverypointFieldIndex.HotSOSRoomId, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.HotSOSRoomId, value, true); }
		}

		/// <summary> The DeviceId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."DeviceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.DeviceId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.DeviceId, value, true); }
		}

		/// <summary> The EnableAnalytics property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."EnableAnalytics"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableAnalytics
		{
			get { return (System.Boolean)GetValue((int)DeliverypointFieldIndex.EnableAnalytics, true); }
			set	{ SetValue((int)DeliverypointFieldIndex.EnableAnalytics, value, true); }
		}

		/// <summary> The RoomControlConfigurationId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControlConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.RoomControlConfigurationId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControlConfigurationId, value, true); }
		}

		/// <summary> The RoomControllerSlaveId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerSlaveId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControllerSlaveId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.RoomControllerSlaveId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerSlaveId, value, true); }
		}

		/// <summary> The RoomControllerType property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControllerType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.RoomControlType> RoomControllerType
		{
			get { return (Nullable<Obymobi.Enums.RoomControlType>)GetValue((int)DeliverypointFieldIndex.RoomControllerType, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControllerType, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity Deliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypoint"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)DeliverypointFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollection
		{
			get	{ return GetMultiClientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollection. When set to true, ClientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollection
		{
			get	{ return _alwaysFetchClientCollection; }
			set	{ _alwaysFetchClientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollection already has been fetched. Setting this property to false when ClientCollection has been fetched
		/// will clear the ClientCollection collection well. Setting this property to true while ClientCollection hasn't been fetched disables lazy loading for ClientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollection
		{
			get { return _alreadyFetchedClientCollection;}
			set 
			{
				if(_alreadyFetchedClientCollection && !value && (_clientCollection != null))
				{
					_clientCollection.Clear();
				}
				_alreadyFetchedClientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollection_
		{
			get	{ return GetMultiClientCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollection_. When set to true, ClientCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiClientCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollection_
		{
			get	{ return _alwaysFetchClientCollection_; }
			set	{ _alwaysFetchClientCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollection_ already has been fetched. Setting this property to false when ClientCollection_ has been fetched
		/// will clear the ClientCollection_ collection well. Setting this property to true while ClientCollection_ hasn't been fetched disables lazy loading for ClientCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollection_
		{
			get { return _alreadyFetchedClientCollection_;}
			set 
			{
				if(_alreadyFetchedClientCollection_ && !value && (_clientCollection_ != null))
				{
					_clientCollection_.Clear();
				}
				_alreadyFetchedClientCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointExternalDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection DeliverypointExternalDeliverypointCollection
		{
			get	{ return GetMultiDeliverypointExternalDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointExternalDeliverypointCollection. When set to true, DeliverypointExternalDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointExternalDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointExternalDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointExternalDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointExternalDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointExternalDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointExternalDeliverypointCollection already has been fetched. Setting this property to false when DeliverypointExternalDeliverypointCollection has been fetched
		/// will clear the DeliverypointExternalDeliverypointCollection collection well. Setting this property to true while DeliverypointExternalDeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointExternalDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointExternalDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointExternalDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointExternalDeliverypointCollection && !value && (_deliverypointExternalDeliverypointCollection != null))
				{
					_deliverypointExternalDeliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointExternalDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGameSessionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GameSessionCollection GameSessionCollection
		{
			get	{ return GetMultiGameSessionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GameSessionCollection. When set to true, GameSessionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GameSessionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGameSessionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGameSessionCollection
		{
			get	{ return _alwaysFetchGameSessionCollection; }
			set	{ _alwaysFetchGameSessionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GameSessionCollection already has been fetched. Setting this property to false when GameSessionCollection has been fetched
		/// will clear the GameSessionCollection collection well. Setting this property to true while GameSessionCollection hasn't been fetched disables lazy loading for GameSessionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGameSessionCollection
		{
			get { return _alreadyFetchedGameSessionCollection;}
			set 
			{
				if(_alreadyFetchedGameSessionCollection && !value && (_gameSessionCollection != null))
				{
					_gameSessionCollection.Clear();
				}
				_alreadyFetchedGameSessionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingDeliverypointCollection IcrtouchprintermappingDeliverypointCollection
		{
			get	{ return GetMultiIcrtouchprintermappingDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingDeliverypointCollection. When set to true, IcrtouchprintermappingDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiIcrtouchprintermappingDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingDeliverypointCollection
		{
			get	{ return _alwaysFetchIcrtouchprintermappingDeliverypointCollection; }
			set	{ _alwaysFetchIcrtouchprintermappingDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingDeliverypointCollection already has been fetched. Setting this property to false when IcrtouchprintermappingDeliverypointCollection has been fetched
		/// will clear the IcrtouchprintermappingDeliverypointCollection collection well. Setting this property to true while IcrtouchprintermappingDeliverypointCollection hasn't been fetched disables lazy loading for IcrtouchprintermappingDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingDeliverypointCollection
		{
			get { return _alreadyFetchedIcrtouchprintermappingDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingDeliverypointCollection && !value && (_icrtouchprintermappingDeliverypointCollection != null))
				{
					_icrtouchprintermappingDeliverypointCollection.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessagegroupDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessagegroupDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessagegroupDeliverypointCollection MessagegroupDeliverypointCollection
		{
			get	{ return GetMultiMessagegroupDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupDeliverypointCollection. When set to true, MessagegroupDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessagegroupDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupDeliverypointCollection
		{
			get	{ return _alwaysFetchMessagegroupDeliverypointCollection; }
			set	{ _alwaysFetchMessagegroupDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupDeliverypointCollection already has been fetched. Setting this property to false when MessagegroupDeliverypointCollection has been fetched
		/// will clear the MessagegroupDeliverypointCollection collection well. Setting this property to true while MessagegroupDeliverypointCollection hasn't been fetched disables lazy loading for MessagegroupDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupDeliverypointCollection
		{
			get { return _alreadyFetchedMessagegroupDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedMessagegroupDeliverypointCollection && !value && (_messagegroupDeliverypointCollection != null))
				{
					_messagegroupDeliverypointCollection.Clear();
				}
				_alreadyFetchedMessagegroupDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection MessageRecipientCollection
		{
			get	{ return GetMultiMessageRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageRecipientCollection. When set to true, MessageRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageRecipientCollection
		{
			get	{ return _alwaysFetchMessageRecipientCollection; }
			set	{ _alwaysFetchMessageRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageRecipientCollection already has been fetched. Setting this property to false when MessageRecipientCollection has been fetched
		/// will clear the MessageRecipientCollection collection well. Setting this property to true while MessageRecipientCollection hasn't been fetched disables lazy loading for MessageRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageRecipientCollection
		{
			get { return _alreadyFetchedMessageRecipientCollection;}
			set 
			{
				if(_alreadyFetchedMessageRecipientCollection && !value && (_messageRecipientCollection != null))
				{
					_messageRecipientCollection.Clear();
				}
				_alreadyFetchedMessageRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceivedNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection ReceivedNetmessageCollection
		{
			get	{ return GetMultiReceivedNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceivedNetmessageCollection. When set to true, ReceivedNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceivedNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceivedNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceivedNetmessageCollection
		{
			get	{ return _alwaysFetchReceivedNetmessageCollection; }
			set	{ _alwaysFetchReceivedNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceivedNetmessageCollection already has been fetched. Setting this property to false when ReceivedNetmessageCollection has been fetched
		/// will clear the ReceivedNetmessageCollection collection well. Setting this property to true while ReceivedNetmessageCollection hasn't been fetched disables lazy loading for ReceivedNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceivedNetmessageCollection
		{
			get { return _alreadyFetchedReceivedNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedReceivedNetmessageCollection && !value && (_receivedNetmessageCollection != null))
				{
					_receivedNetmessageCollection.Clear();
				}
				_alreadyFetchedReceivedNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSentNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection SentNetmessageCollection
		{
			get	{ return GetMultiSentNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SentNetmessageCollection. When set to true, SentNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SentNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSentNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSentNetmessageCollection
		{
			get	{ return _alwaysFetchSentNetmessageCollection; }
			set	{ _alwaysFetchSentNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SentNetmessageCollection already has been fetched. Setting this property to false when SentNetmessageCollection has been fetched
		/// will clear the SentNetmessageCollection collection well. Setting this property to true while SentNetmessageCollection hasn't been fetched disables lazy loading for SentNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSentNetmessageCollection
		{
			get { return _alreadyFetchedSentNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedSentNetmessageCollection && !value && (_sentNetmessageCollection != null))
				{
					_sentNetmessageCollection.Clear();
				}
				_alreadyFetchedSentNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection_
		{
			get	{ return GetMultiOrderCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection_. When set to true, OrderCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection_
		{
			get	{ return _alwaysFetchOrderCollection_; }
			set	{ _alwaysFetchOrderCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection_ already has been fetched. Setting this property to false when OrderCollection_ has been fetched
		/// will clear the OrderCollection_ collection well. Setting this property to true while OrderCollection_ hasn't been fetched disables lazy loading for OrderCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection_
		{
			get { return _alreadyFetchedOrderCollection_;}
			set 
			{
				if(_alreadyFetchedOrderCollection_ && !value && (_orderCollection_ != null))
				{
					_orderCollection_.Clear();
				}
				_alreadyFetchedOrderCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection_
		{
			get	{ return GetMultiTerminalCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection_. When set to true, TerminalCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection_
		{
			get	{ return _alwaysFetchTerminalCollection_; }
			set	{ _alwaysFetchTerminalCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection_ already has been fetched. Setting this property to false when TerminalCollection_ has been fetched
		/// will clear the TerminalCollection_ collection well. Setting this property to true while TerminalCollection_ hasn't been fetched disables lazy loading for TerminalCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection_
		{
			get { return _alreadyFetchedTerminalCollection_;}
			set 
			{
				if(_alreadyFetchedTerminalCollection_ && !value && (_terminalCollection_ != null))
				{
					_terminalCollection_.Clear();
				}
				_alreadyFetchedTerminalCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection
		{
			get	{ return GetMultiTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection. When set to true, TerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection
		{
			get	{ return _alwaysFetchTerminalCollection; }
			set	{ _alwaysFetchTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection already has been fetched. Setting this property to false when TerminalCollection has been fetched
		/// will clear the TerminalCollection collection well. Setting this property to true while TerminalCollection hasn't been fetched disables lazy loading for TerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection
		{
			get { return _alreadyFetchedTerminalCollection;}
			set 
			{
				if(_alreadyFetchedTerminalCollection && !value && (_terminalCollection != null))
				{
					_terminalCollection.Clear();
				}
				_alreadyFetchedTerminalCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessage
		{
			get { return GetMultiCategoryCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessage. When set to true, CategoryCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessage
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessage; }
			set	{ _alwaysFetchCategoryCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessage already has been fetched. Setting this property to false when CategoryCollectionViaMessage has been fetched
		/// will clear the CategoryCollectionViaMessage collection well. Setting this property to true while CategoryCollectionViaMessage hasn't been fetched disables lazy loading for CategoryCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessage
		{
			get { return _alreadyFetchedCategoryCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessage && !value && (_categoryCollectionViaMessage != null))
				{
					_categoryCollectionViaMessage.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaMessage
		{
			get { return GetMultiClientCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaMessage. When set to true, ClientCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaMessage
		{
			get	{ return _alwaysFetchClientCollectionViaMessage; }
			set	{ _alwaysFetchClientCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaMessage already has been fetched. Setting this property to false when ClientCollectionViaMessage has been fetched
		/// will clear the ClientCollectionViaMessage collection well. Setting this property to true while ClientCollectionViaMessage hasn't been fetched disables lazy loading for ClientCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaMessage
		{
			get { return _alreadyFetchedClientCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaMessage && !value && (_clientCollectionViaMessage != null))
				{
					_clientCollectionViaMessage.Clear();
				}
				_alreadyFetchedClientCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaClient
		{
			get { return GetMultiCompanyCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaClient. When set to true, CompanyCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaClient
		{
			get	{ return _alwaysFetchCompanyCollectionViaClient; }
			set	{ _alwaysFetchCompanyCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaClient already has been fetched. Setting this property to false when CompanyCollectionViaClient has been fetched
		/// will clear the CompanyCollectionViaClient collection well. Setting this property to true while CompanyCollectionViaClient hasn't been fetched disables lazy loading for CompanyCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaClient
		{
			get { return _alreadyFetchedCompanyCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaClient && !value && (_companyCollectionViaClient != null))
				{
					_companyCollectionViaClient.Clear();
				}
				_alreadyFetchedCompanyCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaClient_
		{
			get { return GetMultiCompanyCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaClient_. When set to true, CompanyCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaClient_
		{
			get	{ return _alwaysFetchCompanyCollectionViaClient_; }
			set	{ _alwaysFetchCompanyCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaClient_ already has been fetched. Setting this property to false when CompanyCollectionViaClient_ has been fetched
		/// will clear the CompanyCollectionViaClient_ collection well. Setting this property to true while CompanyCollectionViaClient_ hasn't been fetched disables lazy loading for CompanyCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaClient_
		{
			get { return _alreadyFetchedCompanyCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaClient_ && !value && (_companyCollectionViaClient_ != null))
				{
					_companyCollectionViaClient_.Clear();
				}
				_alreadyFetchedCompanyCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMessage
		{
			get { return GetMultiCompanyCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMessage. When set to true, CompanyCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMessage
		{
			get	{ return _alwaysFetchCompanyCollectionViaMessage; }
			set	{ _alwaysFetchCompanyCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMessage already has been fetched. Setting this property to false when CompanyCollectionViaMessage has been fetched
		/// will clear the CompanyCollectionViaMessage collection well. Setting this property to true while CompanyCollectionViaMessage hasn't been fetched disables lazy loading for CompanyCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMessage
		{
			get { return _alreadyFetchedCompanyCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMessage && !value && (_companyCollectionViaMessage != null))
				{
					_companyCollectionViaMessage.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaMessage
		{
			get { return GetMultiCustomerCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaMessage. When set to true, CustomerCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaMessage
		{
			get	{ return _alwaysFetchCustomerCollectionViaMessage; }
			set	{ _alwaysFetchCustomerCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaMessage already has been fetched. Setting this property to false when CustomerCollectionViaMessage has been fetched
		/// will clear the CustomerCollectionViaMessage collection well. Setting this property to true while CustomerCollectionViaMessage hasn't been fetched disables lazy loading for CustomerCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaMessage
		{
			get { return _alreadyFetchedCustomerCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaMessage && !value && (_customerCollectionViaMessage != null))
				{
					_customerCollectionViaMessage.Clear();
				}
				_alreadyFetchedCustomerCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient
		{
			get { return GetMultiDeliverypointCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient. When set to true, DeliverypointCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient already has been fetched. Setting this property to false when DeliverypointCollectionViaClient has been fetched
		/// will clear the DeliverypointCollectionViaClient collection well. Setting this property to true while DeliverypointCollectionViaClient hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient && !value && (_deliverypointCollectionViaClient != null))
				{
					_deliverypointCollectionViaClient.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient_
		{
			get { return GetMultiDeliverypointCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient_. When set to true, DeliverypointCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient_; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient_ already has been fetched. Setting this property to false when DeliverypointCollectionViaClient_ has been fetched
		/// will clear the DeliverypointCollectionViaClient_ collection well. Setting this property to true while DeliverypointCollectionViaClient_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient_ && !value && (_deliverypointCollectionViaClient_ != null))
				{
					_deliverypointCollectionViaClient_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaClient
		{
			get { return GetMultiDeliverypointgroupCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaClient. When set to true, DeliverypointgroupCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaClient
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaClient; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaClient already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaClient has been fetched
		/// will clear the DeliverypointgroupCollectionViaClient collection well. Setting this property to true while DeliverypointgroupCollectionViaClient hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaClient
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaClient && !value && (_deliverypointgroupCollectionViaClient != null))
				{
					_deliverypointgroupCollectionViaClient.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaClient_
		{
			get { return GetMultiDeliverypointgroupCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaClient_. When set to true, DeliverypointgroupCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaClient_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaClient_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaClient_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaClient_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaClient_ collection well. Setting this property to true while DeliverypointgroupCollectionViaClient_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaClient_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaClient_ && !value && (_deliverypointgroupCollectionViaClient_ != null))
				{
					_deliverypointgroupCollectionViaClient_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage. When set to true, DeliverypointgroupCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage && !value && (_deliverypointgroupCollectionViaNetmessage != null))
				{
					_deliverypointgroupCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage_
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage_. When set to true, DeliverypointgroupCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage_ collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ && !value && (_deliverypointgroupCollectionViaNetmessage_ != null))
				{
					_deliverypointgroupCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal. When set to true, DeliverypointgroupCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal && !value && (_deliverypointgroupCollectionViaTerminal != null))
				{
					_deliverypointgroupCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal_. When set to true, DeliverypointgroupCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ && !value && (_deliverypointgroupCollectionViaTerminal_ != null))
				{
					_deliverypointgroupCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaClient
		{
			get { return GetMultiDeviceCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaClient. When set to true, DeviceCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaClient
		{
			get	{ return _alwaysFetchDeviceCollectionViaClient; }
			set	{ _alwaysFetchDeviceCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaClient already has been fetched. Setting this property to false when DeviceCollectionViaClient has been fetched
		/// will clear the DeviceCollectionViaClient collection well. Setting this property to true while DeviceCollectionViaClient hasn't been fetched disables lazy loading for DeviceCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaClient
		{
			get { return _alreadyFetchedDeviceCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaClient && !value && (_deviceCollectionViaClient != null))
				{
					_deviceCollectionViaClient.Clear();
				}
				_alreadyFetchedDeviceCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaClient_
		{
			get { return GetMultiDeviceCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaClient_. When set to true, DeviceCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaClient_
		{
			get	{ return _alwaysFetchDeviceCollectionViaClient_; }
			set	{ _alwaysFetchDeviceCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaClient_ already has been fetched. Setting this property to false when DeviceCollectionViaClient_ has been fetched
		/// will clear the DeviceCollectionViaClient_ collection well. Setting this property to true while DeviceCollectionViaClient_ hasn't been fetched disables lazy loading for DeviceCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaClient_
		{
			get { return _alreadyFetchedDeviceCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaClient_ && !value && (_deviceCollectionViaClient_ != null))
				{
					_deviceCollectionViaClient_.Clear();
				}
				_alreadyFetchedDeviceCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal
		{
			get { return GetMultiDeviceCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal. When set to true, DeviceCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal already has been fetched. Setting this property to false when DeviceCollectionViaTerminal has been fetched
		/// will clear the DeviceCollectionViaTerminal collection well. Setting this property to true while DeviceCollectionViaTerminal hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal && !value && (_deviceCollectionViaTerminal != null))
				{
					_deviceCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal_
		{
			get { return GetMultiDeviceCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal_. When set to true, DeviceCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal_; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal_ already has been fetched. Setting this property to false when DeviceCollectionViaTerminal_ has been fetched
		/// will clear the DeviceCollectionViaTerminal_ collection well. Setting this property to true while DeviceCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal_ && !value && (_deviceCollectionViaTerminal_ != null))
				{
					_deviceCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal___
		{
			get { return GetMultiEntertainmentCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal___. When set to true, EntertainmentCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal___
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal___; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal___ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal___ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal___ collection well. Setting this property to true while EntertainmentCollectionViaTerminal___ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal___
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal___ && !value && (_entertainmentCollectionViaTerminal___ != null))
				{
					_entertainmentCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal____
		{
			get { return GetMultiEntertainmentCollectionViaTerminal____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal____. When set to true, EntertainmentCollectionViaTerminal____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal____ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal____
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal____; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal____ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal____ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal____ collection well. Setting this property to true while EntertainmentCollectionViaTerminal____ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal____
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal____;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal____ && !value && (_entertainmentCollectionViaTerminal____ != null))
				{
					_entertainmentCollectionViaTerminal____.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_____
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_____. When set to true, EntertainmentCollectionViaTerminal_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_____ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_____
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_____; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_____ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_____ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_____ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_____ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_____
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_____;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_____ && !value && (_entertainmentCollectionViaTerminal_____ != null))
				{
					_entertainmentCollectionViaTerminal_____.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessage
		{
			get { return GetMultiMediaCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessage. When set to true, MediaCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessage
		{
			get	{ return _alwaysFetchMediaCollectionViaMessage; }
			set	{ _alwaysFetchMediaCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessage already has been fetched. Setting this property to false when MediaCollectionViaMessage has been fetched
		/// will clear the MediaCollectionViaMessage collection well. Setting this property to true while MediaCollectionViaMessage hasn't been fetched disables lazy loading for MediaCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessage
		{
			get { return _alreadyFetchedMediaCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessage && !value && (_mediaCollectionViaMessage != null))
				{
					_mediaCollectionViaMessage.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MessagegroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessagegroupCollection MessagegroupCollectionViaMessagegroupDeliverypoint
		{
			get { return GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupCollectionViaMessagegroupDeliverypoint. When set to true, MessagegroupCollectionViaMessagegroupDeliverypoint is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupCollectionViaMessagegroupDeliverypoint is accessed. You can always execute a forced fetch by calling GetMultiMessagegroupCollectionViaMessagegroupDeliverypoint(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint
		{
			get	{ return _alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint; }
			set	{ _alwaysFetchMessagegroupCollectionViaMessagegroupDeliverypoint = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupCollectionViaMessagegroupDeliverypoint already has been fetched. Setting this property to false when MessagegroupCollectionViaMessagegroupDeliverypoint has been fetched
		/// will clear the MessagegroupCollectionViaMessagegroupDeliverypoint collection well. Setting this property to true while MessagegroupCollectionViaMessagegroupDeliverypoint hasn't been fetched disables lazy loading for MessagegroupCollectionViaMessagegroupDeliverypoint</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint
		{
			get { return _alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint;}
			set 
			{
				if(_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint && !value && (_messagegroupCollectionViaMessagegroupDeliverypoint != null))
				{
					_messagegroupCollectionViaMessagegroupDeliverypoint.Clear();
				}
				_alreadyFetchedMessagegroupCollectionViaMessagegroupDeliverypoint = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaMessage
		{
			get { return GetMultiOrderCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaMessage. When set to true, OrderCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaMessage
		{
			get	{ return _alwaysFetchOrderCollectionViaMessage; }
			set	{ _alwaysFetchOrderCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaMessage already has been fetched. Setting this property to false when OrderCollectionViaMessage has been fetched
		/// will clear the OrderCollectionViaMessage collection well. Setting this property to true while OrderCollectionViaMessage hasn't been fetched disables lazy loading for OrderCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaMessage
		{
			get { return _alreadyFetchedOrderCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaMessage && !value && (_orderCollectionViaMessage != null))
				{
					_orderCollectionViaMessage.Clear();
				}
				_alreadyFetchedOrderCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrder
		{
			get { return GetMultiOrderCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrder. When set to true, OrderCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrder
		{
			get	{ return _alwaysFetchOrderCollectionViaOrder; }
			set	{ _alwaysFetchOrderCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrder already has been fetched. Setting this property to false when OrderCollectionViaOrder has been fetched
		/// will clear the OrderCollectionViaOrder collection well. Setting this property to true while OrderCollectionViaOrder hasn't been fetched disables lazy loading for OrderCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrder
		{
			get { return _alreadyFetchedOrderCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrder && !value && (_orderCollectionViaOrder != null))
				{
					_orderCollectionViaOrder.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage
		{
			get { return GetMultiTerminalCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage. When set to true, TerminalCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage has been fetched
		/// will clear the TerminalCollectionViaNetmessage collection well. Setting this property to true while TerminalCollectionViaNetmessage hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage && !value && (_terminalCollectionViaNetmessage != null))
				{
					_terminalCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage_
		{
			get { return GetMultiTerminalCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage_. When set to true, TerminalCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage_; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage_ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage_ has been fetched
		/// will clear the TerminalCollectionViaNetmessage_ collection well. Setting this property to true while TerminalCollectionViaNetmessage_ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage_
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage_ && !value && (_terminalCollectionViaNetmessage_ != null))
				{
					_terminalCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage__
		{
			get { return GetMultiTerminalCollectionViaNetmessage__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage__. When set to true, TerminalCollectionViaNetmessage__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage__ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage__
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage__; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage__ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage__ has been fetched
		/// will clear the TerminalCollectionViaNetmessage__ collection well. Setting this property to true while TerminalCollectionViaNetmessage__ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage__
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage__;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage__ && !value && (_terminalCollectionViaNetmessage__ != null))
				{
					_terminalCollectionViaNetmessage__.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage___
		{
			get { return GetMultiTerminalCollectionViaNetmessage___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage___. When set to true, TerminalCollectionViaNetmessage___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage___ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage___
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage___; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage___ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage___ has been fetched
		/// will clear the TerminalCollectionViaNetmessage___ collection well. Setting this property to true while TerminalCollectionViaNetmessage___ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage___
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage___;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage___ && !value && (_terminalCollectionViaNetmessage___ != null))
				{
					_terminalCollectionViaNetmessage___.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal
		{
			get { return GetMultiTerminalCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal. When set to true, TerminalCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal already has been fetched. Setting this property to false when TerminalCollectionViaTerminal has been fetched
		/// will clear the TerminalCollectionViaTerminal collection well. Setting this property to true while TerminalCollectionViaTerminal hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal && !value && (_terminalCollectionViaTerminal != null))
				{
					_terminalCollectionViaTerminal.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal_
		{
			get { return GetMultiTerminalCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal_. When set to true, TerminalCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal_
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal_; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal_ already has been fetched. Setting this property to false when TerminalCollectionViaTerminal_ has been fetched
		/// will clear the TerminalCollectionViaTerminal_ collection well. Setting this property to true while TerminalCollectionViaTerminal_ hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal_
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal_ && !value && (_terminalCollectionViaTerminal_ != null))
				{
					_terminalCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal
		{
			get { return GetMultiUIModeCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal. When set to true, UIModeCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal already has been fetched. Setting this property to false when UIModeCollectionViaTerminal has been fetched
		/// will clear the UIModeCollectionViaTerminal collection well. Setting this property to true while UIModeCollectionViaTerminal hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal && !value && (_uIModeCollectionViaTerminal != null))
				{
					_uIModeCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal_
		{
			get { return GetMultiUIModeCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal_. When set to true, UIModeCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal_
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal_; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal_ already has been fetched. Setting this property to false when UIModeCollectionViaTerminal_ has been fetched
		/// will clear the UIModeCollectionViaTerminal_ collection well. Setting this property to true while UIModeCollectionViaTerminal_ hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal_
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal_ && !value && (_uIModeCollectionViaTerminal_ != null))
				{
					_uIModeCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaTerminal
		{
			get { return GetMultiUserCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaTerminal. When set to true, UserCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaTerminal
		{
			get	{ return _alwaysFetchUserCollectionViaTerminal; }
			set	{ _alwaysFetchUserCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaTerminal already has been fetched. Setting this property to false when UserCollectionViaTerminal has been fetched
		/// will clear the UserCollectionViaTerminal collection well. Setting this property to true while UserCollectionViaTerminal hasn't been fetched disables lazy loading for UserCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaTerminal
		{
			get { return _alreadyFetchedUserCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaTerminal && !value && (_userCollectionViaTerminal != null))
				{
					_userCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUserCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaTerminal_
		{
			get { return GetMultiUserCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaTerminal_. When set to true, UserCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaTerminal_
		{
			get	{ return _alwaysFetchUserCollectionViaTerminal_; }
			set	{ _alwaysFetchUserCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaTerminal_ already has been fetched. Setting this property to false when UserCollectionViaTerminal_ has been fetched
		/// will clear the UserCollectionViaTerminal_ collection well. Setting this property to true while UserCollectionViaTerminal_ hasn't been fetched disables lazy loading for UserCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaTerminal_
		{
			get { return _alreadyFetchedUserCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaTerminal_ && !value && (_userCollectionViaTerminal_ != null))
				{
					_userCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedUserCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeviceEntity DeviceEntity
		{
			get	{ return GetSingleDeviceEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "DeviceEntity", _deviceEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceEntity. When set to true, DeviceEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceEntity is accessed. You can always execute a forced fetch by calling GetSingleDeviceEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceEntity
		{
			get	{ return _alwaysFetchDeviceEntity; }
			set	{ _alwaysFetchDeviceEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceEntity already has been fetched. Setting this property to false when DeviceEntity has been fetched
		/// will set DeviceEntity to null as well. Setting this property to true while DeviceEntity hasn't been fetched disables lazy loading for DeviceEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceEntity
		{
			get { return _alreadyFetchedDeviceEntity;}
			set 
			{
				if(_alreadyFetchedDeviceEntity && !value)
				{
					this.DeviceEntity = null;
				}
				_alreadyFetchedDeviceEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceEntity is not found
		/// in the database. When set to true, DeviceEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeviceEntityReturnsNewIfNotFound
		{
			get	{ return _deviceEntityReturnsNewIfNotFound; }
			set { _deviceEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosdeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosdeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosdeliverypointEntity PosdeliverypointEntity
		{
			get	{ return GetSinglePosdeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosdeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "PosdeliverypointEntity", _posdeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointEntity. When set to true, PosdeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSinglePosdeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointEntity
		{
			get	{ return _alwaysFetchPosdeliverypointEntity; }
			set	{ _alwaysFetchPosdeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointEntity already has been fetched. Setting this property to false when PosdeliverypointEntity has been fetched
		/// will set PosdeliverypointEntity to null as well. Setting this property to true while PosdeliverypointEntity hasn't been fetched disables lazy loading for PosdeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointEntity
		{
			get { return _alreadyFetchedPosdeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointEntity && !value)
				{
					this.PosdeliverypointEntity = null;
				}
				_alreadyFetchedPosdeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosdeliverypointEntity is not found
		/// in the database. When set to true, PosdeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosdeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _posdeliverypointEntityReturnsNewIfNotFound; }
			set { _posdeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlAreaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlAreaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlAreaEntity RoomControlAreaEntity
		{
			get	{ return GetSingleRoomControlAreaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlAreaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "RoomControlAreaEntity", _roomControlAreaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaEntity. When set to true, RoomControlAreaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlAreaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaEntity
		{
			get	{ return _alwaysFetchRoomControlAreaEntity; }
			set	{ _alwaysFetchRoomControlAreaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaEntity already has been fetched. Setting this property to false when RoomControlAreaEntity has been fetched
		/// will set RoomControlAreaEntity to null as well. Setting this property to true while RoomControlAreaEntity hasn't been fetched disables lazy loading for RoomControlAreaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaEntity
		{
			get { return _alreadyFetchedRoomControlAreaEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaEntity && !value)
				{
					this.RoomControlAreaEntity = null;
				}
				_alreadyFetchedRoomControlAreaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlAreaEntity is not found
		/// in the database. When set to true, RoomControlAreaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlAreaEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlAreaEntityReturnsNewIfNotFound; }
			set { _roomControlAreaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlConfigurationEntity RoomControlConfigurationEntity
		{
			get	{ return GetSingleRoomControlConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointCollection", "RoomControlConfigurationEntity", _roomControlConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlConfigurationEntity. When set to true, RoomControlConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlConfigurationEntity
		{
			get	{ return _alwaysFetchRoomControlConfigurationEntity; }
			set	{ _alwaysFetchRoomControlConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlConfigurationEntity already has been fetched. Setting this property to false when RoomControlConfigurationEntity has been fetched
		/// will set RoomControlConfigurationEntity to null as well. Setting this property to true while RoomControlConfigurationEntity hasn't been fetched disables lazy loading for RoomControlConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlConfigurationEntity
		{
			get { return _alreadyFetchedRoomControlConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlConfigurationEntity && !value)
				{
					this.RoomControlConfigurationEntity = null;
				}
				_alreadyFetchedRoomControlConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlConfigurationEntity is not found
		/// in the database. When set to true, RoomControlConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlConfigurationEntityReturnsNewIfNotFound; }
			set { _roomControlConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}
 
		/// <summary> Gets / Sets the value of the related field this.DeliverypointgroupEntity.Name.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String DeliverypointgroupName
		{
			get
			{
				DeliverypointgroupEntity relatedEntity = this.DeliverypointgroupEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Name;
			}
			set
			{
				DeliverypointgroupEntity relatedEntity = this.DeliverypointgroupEntity;
				if(relatedEntity!=null)
				{
					relatedEntity.Name = value;
				}				
			}
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
