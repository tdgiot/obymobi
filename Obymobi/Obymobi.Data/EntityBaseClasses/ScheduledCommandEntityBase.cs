﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ScheduledCommand'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ScheduledCommandEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ScheduledCommandEntity"; }
		}
	
		#region Class Member Declarations
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private ScheduledCommandTaskEntity _scheduledCommandTaskEntity;
		private bool	_alwaysFetchScheduledCommandTaskEntity, _alreadyFetchedScheduledCommandTaskEntity, _scheduledCommandTaskEntityReturnsNewIfNotFound;
		private TerminalEntity _terminalEntity;
		private bool	_alwaysFetchTerminalEntity, _alreadyFetchedTerminalEntity, _terminalEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name ScheduledCommandTaskEntity</summary>
			public static readonly string ScheduledCommandTaskEntity = "ScheduledCommandTaskEntity";
			/// <summary>Member name TerminalEntity</summary>
			public static readonly string TerminalEntity = "TerminalEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ScheduledCommandEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ScheduledCommandEntityBase() :base("ScheduledCommandEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		protected ScheduledCommandEntityBase(System.Int32 scheduledCommandId):base("ScheduledCommandEntity")
		{
			InitClassFetch(scheduledCommandId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ScheduledCommandEntityBase(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse): base("ScheduledCommandEntity")
		{
			InitClassFetch(scheduledCommandId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="validator">The custom validator object for this ScheduledCommandEntity</param>
		protected ScheduledCommandEntityBase(System.Int32 scheduledCommandId, IValidator validator):base("ScheduledCommandEntity")
		{
			InitClassFetch(scheduledCommandId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduledCommandEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_scheduledCommandTaskEntity = (ScheduledCommandTaskEntity)info.GetValue("_scheduledCommandTaskEntity", typeof(ScheduledCommandTaskEntity));
			if(_scheduledCommandTaskEntity!=null)
			{
				_scheduledCommandTaskEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduledCommandTaskEntityReturnsNewIfNotFound = info.GetBoolean("_scheduledCommandTaskEntityReturnsNewIfNotFound");
			_alwaysFetchScheduledCommandTaskEntity = info.GetBoolean("_alwaysFetchScheduledCommandTaskEntity");
			_alreadyFetchedScheduledCommandTaskEntity = info.GetBoolean("_alreadyFetchedScheduledCommandTaskEntity");

			_terminalEntity = (TerminalEntity)info.GetValue("_terminalEntity", typeof(TerminalEntity));
			if(_terminalEntity!=null)
			{
				_terminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalEntityReturnsNewIfNotFound = info.GetBoolean("_terminalEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalEntity = info.GetBoolean("_alwaysFetchTerminalEntity");
			_alreadyFetchedTerminalEntity = info.GetBoolean("_alreadyFetchedTerminalEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ScheduledCommandFieldIndex)fieldIndex)
			{
				case ScheduledCommandFieldIndex.ScheduledCommandTaskId:
					DesetupSyncScheduledCommandTaskEntity(true, false);
					_alreadyFetchedScheduledCommandTaskEntity = false;
					break;
				case ScheduledCommandFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				case ScheduledCommandFieldIndex.TerminalId:
					DesetupSyncTerminalEntity(true, false);
					_alreadyFetchedTerminalEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedScheduledCommandTaskEntity = (_scheduledCommandTaskEntity != null);
			_alreadyFetchedTerminalEntity = (_terminalEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "ScheduledCommandTaskEntity":
					toReturn.Add(Relations.ScheduledCommandTaskEntityUsingScheduledCommandTaskId);
					break;
				case "TerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingTerminalId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);
			info.AddValue("_scheduledCommandTaskEntity", (!this.MarkedForDeletion?_scheduledCommandTaskEntity:null));
			info.AddValue("_scheduledCommandTaskEntityReturnsNewIfNotFound", _scheduledCommandTaskEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduledCommandTaskEntity", _alwaysFetchScheduledCommandTaskEntity);
			info.AddValue("_alreadyFetchedScheduledCommandTaskEntity", _alreadyFetchedScheduledCommandTaskEntity);
			info.AddValue("_terminalEntity", (!this.MarkedForDeletion?_terminalEntity:null));
			info.AddValue("_terminalEntityReturnsNewIfNotFound", _terminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalEntity", _alwaysFetchTerminalEntity);
			info.AddValue("_alreadyFetchedTerminalEntity", _alreadyFetchedTerminalEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "ScheduledCommandTaskEntity":
					_alreadyFetchedScheduledCommandTaskEntity = true;
					this.ScheduledCommandTaskEntity = (ScheduledCommandTaskEntity)entity;
					break;
				case "TerminalEntity":
					_alreadyFetchedTerminalEntity = true;
					this.TerminalEntity = (TerminalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "ScheduledCommandTaskEntity":
					SetupSyncScheduledCommandTaskEntity(relatedEntity);
					break;
				case "TerminalEntity":
					SetupSyncTerminalEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "ScheduledCommandTaskEntity":
					DesetupSyncScheduledCommandTaskEntity(false, true);
					break;
				case "TerminalEntity":
					DesetupSyncTerminalEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_scheduledCommandTaskEntity!=null)
			{
				toReturn.Add(_scheduledCommandTaskEntity);
			}
			if(_terminalEntity!=null)
			{
				toReturn.Add(_terminalEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduledCommandId)
		{
			return FetchUsingPK(scheduledCommandId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(scheduledCommandId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(scheduledCommandId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(scheduledCommandId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ScheduledCommandId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ScheduledCommandRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary> Retrieves the related entity of type 'ScheduledCommandTaskEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduledCommandTaskEntity' which is related to this entity.</returns>
		public ScheduledCommandTaskEntity GetSingleScheduledCommandTaskEntity()
		{
			return GetSingleScheduledCommandTaskEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduledCommandTaskEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduledCommandTaskEntity' which is related to this entity.</returns>
		public virtual ScheduledCommandTaskEntity GetSingleScheduledCommandTaskEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduledCommandTaskEntity || forceFetch || _alwaysFetchScheduledCommandTaskEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduledCommandTaskEntityUsingScheduledCommandTaskId);
				ScheduledCommandTaskEntity newEntity = new ScheduledCommandTaskEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduledCommandTaskId);
				}
				if(fetchResult)
				{
					newEntity = (ScheduledCommandTaskEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduledCommandTaskEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduledCommandTaskEntity = newEntity;
				_alreadyFetchedScheduledCommandTaskEntity = fetchResult;
			}
			return _scheduledCommandTaskEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleTerminalEntity()
		{
			return GetSingleTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalEntity || forceFetch || _alwaysFetchTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalEntity = newEntity;
				_alreadyFetchedTerminalEntity = fetchResult;
			}
			return _terminalEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("ScheduledCommandTaskEntity", _scheduledCommandTaskEntity);
			toReturn.Add("TerminalEntity", _terminalEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="validator">The validator object for this ScheduledCommandEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 scheduledCommandId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(scheduledCommandId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientEntityReturnsNewIfNotFound = true;
			_scheduledCommandTaskEntityReturnsNewIfNotFound = true;
			_terminalEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduledCommandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduledCommandTaskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientCommand", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalCommand", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Sort", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExpiresUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "ScheduledCommandCollection", resetFKFields, new int[] { (int)ScheduledCommandFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _scheduledCommandTaskEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduledCommandTaskEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduledCommandTaskEntity, new PropertyChangedEventHandler( OnScheduledCommandTaskEntityPropertyChanged ), "ScheduledCommandTaskEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.ScheduledCommandTaskEntityUsingScheduledCommandTaskIdStatic, true, signalRelatedEntity, "ScheduledCommandCollection", resetFKFields, new int[] { (int)ScheduledCommandFieldIndex.ScheduledCommandTaskId } );		
			_scheduledCommandTaskEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduledCommandTaskEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduledCommandTaskEntity(IEntityCore relatedEntity)
		{
			if(_scheduledCommandTaskEntity!=relatedEntity)
			{		
				DesetupSyncScheduledCommandTaskEntity(true, true);
				_scheduledCommandTaskEntity = (ScheduledCommandTaskEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduledCommandTaskEntity, new PropertyChangedEventHandler( OnScheduledCommandTaskEntityPropertyChanged ), "ScheduledCommandTaskEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.ScheduledCommandTaskEntityUsingScheduledCommandTaskIdStatic, true, ref _alreadyFetchedScheduledCommandTaskEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduledCommandTaskEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.TerminalEntityUsingTerminalIdStatic, true, signalRelatedEntity, "ScheduledCommandCollection", resetFKFields, new int[] { (int)ScheduledCommandFieldIndex.TerminalId } );		
			_terminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalEntity(IEntityCore relatedEntity)
		{
			if(_terminalEntity!=relatedEntity)
			{		
				DesetupSyncTerminalEntity(true, true);
				_terminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticScheduledCommandRelations.TerminalEntityUsingTerminalIdStatic, true, ref _alreadyFetchedTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ScheduledCommandFieldIndex.ScheduledCommandId].ForcedCurrentValueWrite(scheduledCommandId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateScheduledCommandDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ScheduledCommandEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ScheduledCommandRelations Relations
		{
			get	{ return new ScheduledCommandRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.ScheduledCommandEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledCommandTask'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledCommandTaskEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledCommandTaskCollection(), (IEntityRelation)GetRelationsForField("ScheduledCommandTaskEntity")[0], (int)Obymobi.Data.EntityType.ScheduledCommandEntity, (int)Obymobi.Data.EntityType.ScheduledCommandTaskEntity, 0, null, null, null, "ScheduledCommandTaskEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalEntity")[0], (int)Obymobi.Data.EntityType.ScheduledCommandEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ScheduledCommandId property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ScheduledCommandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ScheduledCommandId
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.ScheduledCommandId, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ScheduledCommandId, value, true); }
		}

		/// <summary> The ScheduledCommandTaskId property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ScheduledCommandTaskId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScheduledCommandTaskId
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.ScheduledCommandTaskId, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ScheduledCommandTaskId, value, true); }
		}

		/// <summary> The ClientCommand property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ClientCommand"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.ClientCommand> ClientCommand
		{
			get { return (Nullable<Obymobi.Enums.ClientCommand>)GetValue((int)ScheduledCommandFieldIndex.ClientCommand, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ClientCommand, value, true); }
		}

		/// <summary> The ClientId property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduledCommandFieldIndex.ClientId, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ClientId, value, true); }
		}

		/// <summary> The TerminalCommand property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."TerminalCommand"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.TerminalCommand> TerminalCommand
		{
			get { return (Nullable<Obymobi.Enums.TerminalCommand>)GetValue((int)ScheduledCommandFieldIndex.TerminalCommand, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.TerminalCommand, value, true); }
		}

		/// <summary> The TerminalId property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduledCommandFieldIndex.TerminalId, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The Status property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ScheduledCommandStatus Status
		{
			get { return (Obymobi.Enums.ScheduledCommandStatus)GetValue((int)ScheduledCommandFieldIndex.Status, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.Status, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Sort property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."Sort"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Sort
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.Sort, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.Sort, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ScheduledCommandFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduledCommandFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduledCommandFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The StartedUTC property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."StartedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduledCommandFieldIndex.StartedUTC, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.StartedUTC, value, true); }
		}

		/// <summary> The ExpiresUTC property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."ExpiresUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExpiresUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduledCommandFieldIndex.ExpiresUTC, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.ExpiresUTC, value, true); }
		}

		/// <summary> The CompletedUTC property of the Entity ScheduledCommand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ScheduledCommand"."CompletedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduledCommandFieldIndex.CompletedUTC, false); }
			set	{ SetValue((int)ScheduledCommandFieldIndex.CompletedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ScheduledCommandCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ScheduledCommandTaskEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduledCommandTaskEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduledCommandTaskEntity ScheduledCommandTaskEntity
		{
			get	{ return GetSingleScheduledCommandTaskEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduledCommandTaskEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ScheduledCommandCollection", "ScheduledCommandTaskEntity", _scheduledCommandTaskEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledCommandTaskEntity. When set to true, ScheduledCommandTaskEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledCommandTaskEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduledCommandTaskEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledCommandTaskEntity
		{
			get	{ return _alwaysFetchScheduledCommandTaskEntity; }
			set	{ _alwaysFetchScheduledCommandTaskEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledCommandTaskEntity already has been fetched. Setting this property to false when ScheduledCommandTaskEntity has been fetched
		/// will set ScheduledCommandTaskEntity to null as well. Setting this property to true while ScheduledCommandTaskEntity hasn't been fetched disables lazy loading for ScheduledCommandTaskEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledCommandTaskEntity
		{
			get { return _alreadyFetchedScheduledCommandTaskEntity;}
			set 
			{
				if(_alreadyFetchedScheduledCommandTaskEntity && !value)
				{
					this.ScheduledCommandTaskEntity = null;
				}
				_alreadyFetchedScheduledCommandTaskEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduledCommandTaskEntity is not found
		/// in the database. When set to true, ScheduledCommandTaskEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduledCommandTaskEntityReturnsNewIfNotFound
		{
			get	{ return _scheduledCommandTaskEntityReturnsNewIfNotFound; }
			set { _scheduledCommandTaskEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity TerminalEntity
		{
			get	{ return GetSingleTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ScheduledCommandCollection", "TerminalEntity", _terminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalEntity. When set to true, TerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalEntity
		{
			get	{ return _alwaysFetchTerminalEntity; }
			set	{ _alwaysFetchTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalEntity already has been fetched. Setting this property to false when TerminalEntity has been fetched
		/// will set TerminalEntity to null as well. Setting this property to true while TerminalEntity hasn't been fetched disables lazy loading for TerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalEntity
		{
			get { return _alreadyFetchedTerminalEntity;}
			set 
			{
				if(_alreadyFetchedTerminalEntity && !value)
				{
					this.TerminalEntity = null;
				}
				_alreadyFetchedTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalEntity is not found
		/// in the database. When set to true, TerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalEntityReturnsNewIfNotFound
		{
			get	{ return _terminalEntityReturnsNewIfNotFound; }
			set { _terminalEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ScheduledCommandEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
