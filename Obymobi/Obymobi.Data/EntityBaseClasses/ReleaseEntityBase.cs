﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Release'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReleaseEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReleaseEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ApplicationCollection	_applicationCollection;
		private bool	_alwaysFetchApplicationCollection, _alreadyFetchedApplicationCollection;
		private Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection	_cloudProcessingTaskCollection;
		private bool	_alwaysFetchCloudProcessingTaskCollection, _alreadyFetchedCloudProcessingTaskCollection;
		private Obymobi.Data.CollectionClasses.CompanyReleaseCollection	_companyReleaseCollection;
		private bool	_alwaysFetchCompanyReleaseCollection, _alreadyFetchedCompanyReleaseCollection;
		private Obymobi.Data.CollectionClasses.ReleaseCollection	_intermediateReleaseCollection;
		private bool	_alwaysFetchIntermediateReleaseCollection, _alreadyFetchedIntermediateReleaseCollection;
		private ApplicationEntity _applicationEntity;
		private bool	_alwaysFetchApplicationEntity, _alreadyFetchedApplicationEntity, _applicationEntityReturnsNewIfNotFound;
		private ReleaseEntity _intermedateReleaseEntity;
		private bool	_alwaysFetchIntermedateReleaseEntity, _alreadyFetchedIntermedateReleaseEntity, _intermedateReleaseEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ApplicationEntity</summary>
			public static readonly string ApplicationEntity = "ApplicationEntity";
			/// <summary>Member name IntermedateReleaseEntity</summary>
			public static readonly string IntermedateReleaseEntity = "IntermedateReleaseEntity";
			/// <summary>Member name ApplicationCollection</summary>
			public static readonly string ApplicationCollection = "ApplicationCollection";
			/// <summary>Member name CloudProcessingTaskCollection</summary>
			public static readonly string CloudProcessingTaskCollection = "CloudProcessingTaskCollection";
			/// <summary>Member name CompanyReleaseCollection</summary>
			public static readonly string CompanyReleaseCollection = "CompanyReleaseCollection";
			/// <summary>Member name IntermediateReleaseCollection</summary>
			public static readonly string IntermediateReleaseCollection = "IntermediateReleaseCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReleaseEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReleaseEntityBase() :base("ReleaseEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		protected ReleaseEntityBase(System.Int32 releaseId):base("ReleaseEntity")
		{
			InitClassFetch(releaseId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReleaseEntityBase(System.Int32 releaseId, IPrefetchPath prefetchPathToUse): base("ReleaseEntity")
		{
			InitClassFetch(releaseId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="validator">The custom validator object for this ReleaseEntity</param>
		protected ReleaseEntityBase(System.Int32 releaseId, IValidator validator):base("ReleaseEntity")
		{
			InitClassFetch(releaseId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReleaseEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_applicationCollection = (Obymobi.Data.CollectionClasses.ApplicationCollection)info.GetValue("_applicationCollection", typeof(Obymobi.Data.CollectionClasses.ApplicationCollection));
			_alwaysFetchApplicationCollection = info.GetBoolean("_alwaysFetchApplicationCollection");
			_alreadyFetchedApplicationCollection = info.GetBoolean("_alreadyFetchedApplicationCollection");

			_cloudProcessingTaskCollection = (Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection)info.GetValue("_cloudProcessingTaskCollection", typeof(Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection));
			_alwaysFetchCloudProcessingTaskCollection = info.GetBoolean("_alwaysFetchCloudProcessingTaskCollection");
			_alreadyFetchedCloudProcessingTaskCollection = info.GetBoolean("_alreadyFetchedCloudProcessingTaskCollection");

			_companyReleaseCollection = (Obymobi.Data.CollectionClasses.CompanyReleaseCollection)info.GetValue("_companyReleaseCollection", typeof(Obymobi.Data.CollectionClasses.CompanyReleaseCollection));
			_alwaysFetchCompanyReleaseCollection = info.GetBoolean("_alwaysFetchCompanyReleaseCollection");
			_alreadyFetchedCompanyReleaseCollection = info.GetBoolean("_alreadyFetchedCompanyReleaseCollection");

			_intermediateReleaseCollection = (Obymobi.Data.CollectionClasses.ReleaseCollection)info.GetValue("_intermediateReleaseCollection", typeof(Obymobi.Data.CollectionClasses.ReleaseCollection));
			_alwaysFetchIntermediateReleaseCollection = info.GetBoolean("_alwaysFetchIntermediateReleaseCollection");
			_alreadyFetchedIntermediateReleaseCollection = info.GetBoolean("_alreadyFetchedIntermediateReleaseCollection");
			_applicationEntity = (ApplicationEntity)info.GetValue("_applicationEntity", typeof(ApplicationEntity));
			if(_applicationEntity!=null)
			{
				_applicationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationEntity = info.GetBoolean("_alwaysFetchApplicationEntity");
			_alreadyFetchedApplicationEntity = info.GetBoolean("_alreadyFetchedApplicationEntity");

			_intermedateReleaseEntity = (ReleaseEntity)info.GetValue("_intermedateReleaseEntity", typeof(ReleaseEntity));
			if(_intermedateReleaseEntity!=null)
			{
				_intermedateReleaseEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_intermedateReleaseEntityReturnsNewIfNotFound = info.GetBoolean("_intermedateReleaseEntityReturnsNewIfNotFound");
			_alwaysFetchIntermedateReleaseEntity = info.GetBoolean("_alwaysFetchIntermedateReleaseEntity");
			_alreadyFetchedIntermedateReleaseEntity = info.GetBoolean("_alreadyFetchedIntermedateReleaseEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReleaseFieldIndex)fieldIndex)
			{
				case ReleaseFieldIndex.ApplicationId:
					DesetupSyncApplicationEntity(true, false);
					_alreadyFetchedApplicationEntity = false;
					break;
				case ReleaseFieldIndex.IntermediateReleaseId:
					DesetupSyncIntermedateReleaseEntity(true, false);
					_alreadyFetchedIntermedateReleaseEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApplicationCollection = (_applicationCollection.Count > 0);
			_alreadyFetchedCloudProcessingTaskCollection = (_cloudProcessingTaskCollection.Count > 0);
			_alreadyFetchedCompanyReleaseCollection = (_companyReleaseCollection.Count > 0);
			_alreadyFetchedIntermediateReleaseCollection = (_intermediateReleaseCollection.Count > 0);
			_alreadyFetchedApplicationEntity = (_applicationEntity != null);
			_alreadyFetchedIntermedateReleaseEntity = (_intermedateReleaseEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ApplicationEntity":
					toReturn.Add(Relations.ApplicationEntityUsingApplicationId);
					break;
				case "IntermedateReleaseEntity":
					toReturn.Add(Relations.ReleaseEntityUsingReleaseIdIntermediateReleaseId);
					break;
				case "ApplicationCollection":
					toReturn.Add(Relations.ApplicationEntityUsingReleaseId);
					break;
				case "CloudProcessingTaskCollection":
					toReturn.Add(Relations.CloudProcessingTaskEntityUsingReleaseId);
					break;
				case "CompanyReleaseCollection":
					toReturn.Add(Relations.CompanyReleaseEntityUsingReleaseId);
					break;
				case "IntermediateReleaseCollection":
					toReturn.Add(Relations.ReleaseEntityUsingIntermediateReleaseId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_applicationCollection", (!this.MarkedForDeletion?_applicationCollection:null));
			info.AddValue("_alwaysFetchApplicationCollection", _alwaysFetchApplicationCollection);
			info.AddValue("_alreadyFetchedApplicationCollection", _alreadyFetchedApplicationCollection);
			info.AddValue("_cloudProcessingTaskCollection", (!this.MarkedForDeletion?_cloudProcessingTaskCollection:null));
			info.AddValue("_alwaysFetchCloudProcessingTaskCollection", _alwaysFetchCloudProcessingTaskCollection);
			info.AddValue("_alreadyFetchedCloudProcessingTaskCollection", _alreadyFetchedCloudProcessingTaskCollection);
			info.AddValue("_companyReleaseCollection", (!this.MarkedForDeletion?_companyReleaseCollection:null));
			info.AddValue("_alwaysFetchCompanyReleaseCollection", _alwaysFetchCompanyReleaseCollection);
			info.AddValue("_alreadyFetchedCompanyReleaseCollection", _alreadyFetchedCompanyReleaseCollection);
			info.AddValue("_intermediateReleaseCollection", (!this.MarkedForDeletion?_intermediateReleaseCollection:null));
			info.AddValue("_alwaysFetchIntermediateReleaseCollection", _alwaysFetchIntermediateReleaseCollection);
			info.AddValue("_alreadyFetchedIntermediateReleaseCollection", _alreadyFetchedIntermediateReleaseCollection);
			info.AddValue("_applicationEntity", (!this.MarkedForDeletion?_applicationEntity:null));
			info.AddValue("_applicationEntityReturnsNewIfNotFound", _applicationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationEntity", _alwaysFetchApplicationEntity);
			info.AddValue("_alreadyFetchedApplicationEntity", _alreadyFetchedApplicationEntity);
			info.AddValue("_intermedateReleaseEntity", (!this.MarkedForDeletion?_intermedateReleaseEntity:null));
			info.AddValue("_intermedateReleaseEntityReturnsNewIfNotFound", _intermedateReleaseEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIntermedateReleaseEntity", _alwaysFetchIntermedateReleaseEntity);
			info.AddValue("_alreadyFetchedIntermedateReleaseEntity", _alreadyFetchedIntermedateReleaseEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ApplicationEntity":
					_alreadyFetchedApplicationEntity = true;
					this.ApplicationEntity = (ApplicationEntity)entity;
					break;
				case "IntermedateReleaseEntity":
					_alreadyFetchedIntermedateReleaseEntity = true;
					this.IntermedateReleaseEntity = (ReleaseEntity)entity;
					break;
				case "ApplicationCollection":
					_alreadyFetchedApplicationCollection = true;
					if(entity!=null)
					{
						this.ApplicationCollection.Add((ApplicationEntity)entity);
					}
					break;
				case "CloudProcessingTaskCollection":
					_alreadyFetchedCloudProcessingTaskCollection = true;
					if(entity!=null)
					{
						this.CloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)entity);
					}
					break;
				case "CompanyReleaseCollection":
					_alreadyFetchedCompanyReleaseCollection = true;
					if(entity!=null)
					{
						this.CompanyReleaseCollection.Add((CompanyReleaseEntity)entity);
					}
					break;
				case "IntermediateReleaseCollection":
					_alreadyFetchedIntermediateReleaseCollection = true;
					if(entity!=null)
					{
						this.IntermediateReleaseCollection.Add((ReleaseEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ApplicationEntity":
					SetupSyncApplicationEntity(relatedEntity);
					break;
				case "IntermedateReleaseEntity":
					SetupSyncIntermedateReleaseEntity(relatedEntity);
					break;
				case "ApplicationCollection":
					_applicationCollection.Add((ApplicationEntity)relatedEntity);
					break;
				case "CloudProcessingTaskCollection":
					_cloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)relatedEntity);
					break;
				case "CompanyReleaseCollection":
					_companyReleaseCollection.Add((CompanyReleaseEntity)relatedEntity);
					break;
				case "IntermediateReleaseCollection":
					_intermediateReleaseCollection.Add((ReleaseEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ApplicationEntity":
					DesetupSyncApplicationEntity(false, true);
					break;
				case "IntermedateReleaseEntity":
					DesetupSyncIntermedateReleaseEntity(false, true);
					break;
				case "ApplicationCollection":
					this.PerformRelatedEntityRemoval(_applicationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CloudProcessingTaskCollection":
					this.PerformRelatedEntityRemoval(_cloudProcessingTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyReleaseCollection":
					this.PerformRelatedEntityRemoval(_companyReleaseCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "IntermediateReleaseCollection":
					this.PerformRelatedEntityRemoval(_intermediateReleaseCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_applicationEntity!=null)
			{
				toReturn.Add(_applicationEntity);
			}
			if(_intermedateReleaseEntity!=null)
			{
				toReturn.Add(_intermedateReleaseEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_applicationCollection);
			toReturn.Add(_cloudProcessingTaskCollection);
			toReturn.Add(_companyReleaseCollection);
			toReturn.Add(_intermediateReleaseCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 releaseId)
		{
			return FetchUsingPK(releaseId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 releaseId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(releaseId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 releaseId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(releaseId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 releaseId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(releaseId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReleaseId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReleaseRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApplicationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApplicationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ApplicationCollection GetMultiApplicationCollection(bool forceFetch)
		{
			return GetMultiApplicationCollection(forceFetch, _applicationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApplicationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApplicationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ApplicationCollection GetMultiApplicationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApplicationCollection(forceFetch, _applicationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApplicationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ApplicationCollection GetMultiApplicationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApplicationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApplicationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ApplicationCollection GetMultiApplicationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApplicationCollection || forceFetch || _alwaysFetchApplicationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_applicationCollection);
				_applicationCollection.SuppressClearInGetMulti=!forceFetch;
				_applicationCollection.EntityFactoryToUse = entityFactoryToUse;
				_applicationCollection.GetMultiManyToOne(this, filter);
				_applicationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedApplicationCollection = true;
			}
			return _applicationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ApplicationCollection'. These settings will be taken into account
		/// when the property ApplicationCollection is requested or GetMultiApplicationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApplicationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_applicationCollection.SortClauses=sortClauses;
			_applicationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCloudProcessingTaskCollection || forceFetch || _alwaysFetchCloudProcessingTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cloudProcessingTaskCollection);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_cloudProcessingTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_cloudProcessingTaskCollection.GetMultiManyToOne(null, null, null, this, null, filter);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCloudProcessingTaskCollection = true;
			}
			return _cloudProcessingTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CloudProcessingTaskCollection'. These settings will be taken into account
		/// when the property CloudProcessingTaskCollection is requested or GetMultiCloudProcessingTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCloudProcessingTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cloudProcessingTaskCollection.SortClauses=sortClauses;
			_cloudProcessingTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyReleaseCollection GetMultiCompanyReleaseCollection(bool forceFetch)
		{
			return GetMultiCompanyReleaseCollection(forceFetch, _companyReleaseCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyReleaseCollection GetMultiCompanyReleaseCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyReleaseCollection(forceFetch, _companyReleaseCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyReleaseCollection GetMultiCompanyReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyReleaseCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyReleaseCollection GetMultiCompanyReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyReleaseCollection || forceFetch || _alwaysFetchCompanyReleaseCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyReleaseCollection);
				_companyReleaseCollection.SuppressClearInGetMulti=!forceFetch;
				_companyReleaseCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyReleaseCollection.GetMultiManyToOne(null, this, filter);
				_companyReleaseCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyReleaseCollection = true;
			}
			return _companyReleaseCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyReleaseCollection'. These settings will be taken into account
		/// when the property CompanyReleaseCollection is requested or GetMultiCompanyReleaseCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyReleaseCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyReleaseCollection.SortClauses=sortClauses;
			_companyReleaseCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiIntermediateReleaseCollection(bool forceFetch)
		{
			return GetMultiIntermediateReleaseCollection(forceFetch, _intermediateReleaseCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiIntermediateReleaseCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiIntermediateReleaseCollection(forceFetch, _intermediateReleaseCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiIntermediateReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiIntermediateReleaseCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiIntermediateReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedIntermediateReleaseCollection || forceFetch || _alwaysFetchIntermediateReleaseCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_intermediateReleaseCollection);
				_intermediateReleaseCollection.SuppressClearInGetMulti=!forceFetch;
				_intermediateReleaseCollection.EntityFactoryToUse = entityFactoryToUse;
				_intermediateReleaseCollection.GetMultiManyToOne(null, this, filter);
				_intermediateReleaseCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedIntermediateReleaseCollection = true;
			}
			return _intermediateReleaseCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'IntermediateReleaseCollection'. These settings will be taken into account
		/// when the property IntermediateReleaseCollection is requested or GetMultiIntermediateReleaseCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIntermediateReleaseCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_intermediateReleaseCollection.SortClauses=sortClauses;
			_intermediateReleaseCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ApplicationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationEntity' which is related to this entity.</returns>
		public ApplicationEntity GetSingleApplicationEntity()
		{
			return GetSingleApplicationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationEntity' which is related to this entity.</returns>
		public virtual ApplicationEntity GetSingleApplicationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationEntity || forceFetch || _alwaysFetchApplicationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationEntityUsingApplicationId);
				ApplicationEntity newEntity = new ApplicationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationId);
				}
				if(fetchResult)
				{
					newEntity = (ApplicationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationEntity = newEntity;
				_alreadyFetchedApplicationEntity = fetchResult;
			}
			return _applicationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public ReleaseEntity GetSingleIntermedateReleaseEntity()
		{
			return GetSingleIntermedateReleaseEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public virtual ReleaseEntity GetSingleIntermedateReleaseEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedIntermedateReleaseEntity || forceFetch || _alwaysFetchIntermedateReleaseEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReleaseEntityUsingReleaseIdIntermediateReleaseId);
				ReleaseEntity newEntity = new ReleaseEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IntermediateReleaseId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReleaseEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_intermedateReleaseEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IntermedateReleaseEntity = newEntity;
				_alreadyFetchedIntermedateReleaseEntity = fetchResult;
			}
			return _intermedateReleaseEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ApplicationEntity", _applicationEntity);
			toReturn.Add("IntermedateReleaseEntity", _intermedateReleaseEntity);
			toReturn.Add("ApplicationCollection", _applicationCollection);
			toReturn.Add("CloudProcessingTaskCollection", _cloudProcessingTaskCollection);
			toReturn.Add("CompanyReleaseCollection", _companyReleaseCollection);
			toReturn.Add("IntermediateReleaseCollection", _intermediateReleaseCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="validator">The validator object for this ReleaseEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 releaseId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(releaseId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_applicationCollection = new Obymobi.Data.CollectionClasses.ApplicationCollection();
			_applicationCollection.SetContainingEntityInfo(this, "ReleaseEntity");

			_cloudProcessingTaskCollection = new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection();
			_cloudProcessingTaskCollection.SetContainingEntityInfo(this, "ReleaseEntity");

			_companyReleaseCollection = new Obymobi.Data.CollectionClasses.CompanyReleaseCollection();
			_companyReleaseCollection.SetContainingEntityInfo(this, "ReleaseEntity");

			_intermediateReleaseCollection = new Obymobi.Data.CollectionClasses.ReleaseCollection();
			_intermediateReleaseCollection.SetContainingEntityInfo(this, "IntermedateReleaseEntity");
			_applicationEntityReturnsNewIfNotFound = true;
			_intermedateReleaseEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Comment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Filename", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("File", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Crc32", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Intermediate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntermediateReleaseId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseGroup", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _applicationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationEntity, new PropertyChangedEventHandler( OnApplicationEntityPropertyChanged ), "ApplicationEntity", Obymobi.Data.RelationClasses.StaticReleaseRelations.ApplicationEntityUsingApplicationIdStatic, true, signalRelatedEntity, "ReleaseCollection", resetFKFields, new int[] { (int)ReleaseFieldIndex.ApplicationId } );		
			_applicationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationEntity(IEntityCore relatedEntity)
		{
			if(_applicationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationEntity(true, true);
				_applicationEntity = (ApplicationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationEntity, new PropertyChangedEventHandler( OnApplicationEntityPropertyChanged ), "ApplicationEntity", Obymobi.Data.RelationClasses.StaticReleaseRelations.ApplicationEntityUsingApplicationIdStatic, true, ref _alreadyFetchedApplicationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _intermedateReleaseEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIntermedateReleaseEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _intermedateReleaseEntity, new PropertyChangedEventHandler( OnIntermedateReleaseEntityPropertyChanged ), "IntermedateReleaseEntity", Obymobi.Data.RelationClasses.StaticReleaseRelations.ReleaseEntityUsingReleaseIdIntermediateReleaseIdStatic, true, signalRelatedEntity, "IntermediateReleaseCollection", resetFKFields, new int[] { (int)ReleaseFieldIndex.IntermediateReleaseId } );		
			_intermedateReleaseEntity = null;
		}
		
		/// <summary> setups the sync logic for member _intermedateReleaseEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIntermedateReleaseEntity(IEntityCore relatedEntity)
		{
			if(_intermedateReleaseEntity!=relatedEntity)
			{		
				DesetupSyncIntermedateReleaseEntity(true, true);
				_intermedateReleaseEntity = (ReleaseEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _intermedateReleaseEntity, new PropertyChangedEventHandler( OnIntermedateReleaseEntityPropertyChanged ), "IntermedateReleaseEntity", Obymobi.Data.RelationClasses.StaticReleaseRelations.ReleaseEntityUsingReleaseIdIntermediateReleaseIdStatic, true, ref _alreadyFetchedIntermedateReleaseEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIntermedateReleaseEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 releaseId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReleaseFieldIndex.ReleaseId].ForcedCurrentValueWrite(releaseId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReleaseDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReleaseEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReleaseRelations Relations
		{
			get	{ return new ReleaseRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Application' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationCollection(), (IEntityRelation)GetRelationsForField("ApplicationCollection")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.ApplicationEntity, 0, null, null, null, "ApplicationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloudProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloudProcessingTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("CloudProcessingTaskCollection")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, 0, null, null, null, "CloudProcessingTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyRelease' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyReleaseCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyReleaseCollection(), (IEntityRelation)GetRelationsForField("CompanyReleaseCollection")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.CompanyReleaseEntity, 0, null, null, null, "CompanyReleaseCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Release' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIntermediateReleaseCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReleaseCollection(), (IEntityRelation)GetRelationsForField("IntermediateReleaseCollection")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.ReleaseEntity, 0, null, null, null, "IntermediateReleaseCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Application'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationCollection(), (IEntityRelation)GetRelationsForField("ApplicationEntity")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.ApplicationEntity, 0, null, null, null, "ApplicationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Release'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIntermedateReleaseEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReleaseCollection(), (IEntityRelation)GetRelationsForField("IntermedateReleaseEntity")[0], (int)Obymobi.Data.EntityType.ReleaseEntity, (int)Obymobi.Data.EntityType.ReleaseEntity, 0, null, null, null, "IntermedateReleaseEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReleaseId property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."ReleaseId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReleaseId
		{
			get { return (System.Int32)GetValue((int)ReleaseFieldIndex.ReleaseId, true); }
			set	{ SetValue((int)ReleaseFieldIndex.ReleaseId, value, true); }
		}

		/// <summary> The ApplicationId property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."ApplicationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationId
		{
			get { return (System.Int32)GetValue((int)ReleaseFieldIndex.ApplicationId, true); }
			set	{ SetValue((int)ReleaseFieldIndex.ApplicationId, value, true); }
		}

		/// <summary> The Version property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Version
		{
			get { return (System.String)GetValue((int)ReleaseFieldIndex.Version, true); }
			set	{ SetValue((int)ReleaseFieldIndex.Version, value, true); }
		}

		/// <summary> The Comment property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."Comment"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Comment
		{
			get { return (System.String)GetValue((int)ReleaseFieldIndex.Comment, true); }
			set	{ SetValue((int)ReleaseFieldIndex.Comment, value, true); }
		}

		/// <summary> The Filename property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."Filename"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Filename
		{
			get { return (System.String)GetValue((int)ReleaseFieldIndex.Filename, true); }
			set	{ SetValue((int)ReleaseFieldIndex.Filename, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReleaseFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReleaseFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReleaseFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReleaseFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The File property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."File"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarBinary, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] File
		{
			get { return (System.Byte[])GetValue((int)ReleaseFieldIndex.File, true); }
			set	{ SetValue((int)ReleaseFieldIndex.File, value, true); }
		}

		/// <summary> The Crc32 property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."Crc32"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Crc32
		{
			get { return (System.String)GetValue((int)ReleaseFieldIndex.Crc32, true); }
			set	{ SetValue((int)ReleaseFieldIndex.Crc32, value, true); }
		}

		/// <summary> The Intermediate property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."Intermediate"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Intermediate
		{
			get { return (System.Boolean)GetValue((int)ReleaseFieldIndex.Intermediate, true); }
			set	{ SetValue((int)ReleaseFieldIndex.Intermediate, value, true); }
		}

		/// <summary> The IntermediateReleaseId property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."IntermediateReleaseId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntermediateReleaseId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReleaseFieldIndex.IntermediateReleaseId, false); }
			set	{ SetValue((int)ReleaseFieldIndex.IntermediateReleaseId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReleaseFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReleaseFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReleaseFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReleaseFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ReleaseGroup property of the Entity Release<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Release"."ReleaseGroup"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReleaseGroup
		{
			get { return (System.Int32)GetValue((int)ReleaseFieldIndex.ReleaseGroup, true); }
			set	{ SetValue((int)ReleaseFieldIndex.ReleaseGroup, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApplicationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApplicationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ApplicationCollection ApplicationCollection
		{
			get	{ return GetMultiApplicationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationCollection. When set to true, ApplicationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiApplicationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationCollection
		{
			get	{ return _alwaysFetchApplicationCollection; }
			set	{ _alwaysFetchApplicationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationCollection already has been fetched. Setting this property to false when ApplicationCollection has been fetched
		/// will clear the ApplicationCollection collection well. Setting this property to true while ApplicationCollection hasn't been fetched disables lazy loading for ApplicationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationCollection
		{
			get { return _alreadyFetchedApplicationCollection;}
			set 
			{
				if(_alreadyFetchedApplicationCollection && !value && (_applicationCollection != null))
				{
					_applicationCollection.Clear();
				}
				_alreadyFetchedApplicationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCloudProcessingTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection CloudProcessingTaskCollection
		{
			get	{ return GetMultiCloudProcessingTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CloudProcessingTaskCollection. When set to true, CloudProcessingTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloudProcessingTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCloudProcessingTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloudProcessingTaskCollection
		{
			get	{ return _alwaysFetchCloudProcessingTaskCollection; }
			set	{ _alwaysFetchCloudProcessingTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloudProcessingTaskCollection already has been fetched. Setting this property to false when CloudProcessingTaskCollection has been fetched
		/// will clear the CloudProcessingTaskCollection collection well. Setting this property to true while CloudProcessingTaskCollection hasn't been fetched disables lazy loading for CloudProcessingTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloudProcessingTaskCollection
		{
			get { return _alreadyFetchedCloudProcessingTaskCollection;}
			set 
			{
				if(_alreadyFetchedCloudProcessingTaskCollection && !value && (_cloudProcessingTaskCollection != null))
				{
					_cloudProcessingTaskCollection.Clear();
				}
				_alreadyFetchedCloudProcessingTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyReleaseCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyReleaseCollection CompanyReleaseCollection
		{
			get	{ return GetMultiCompanyReleaseCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyReleaseCollection. When set to true, CompanyReleaseCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyReleaseCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyReleaseCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyReleaseCollection
		{
			get	{ return _alwaysFetchCompanyReleaseCollection; }
			set	{ _alwaysFetchCompanyReleaseCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyReleaseCollection already has been fetched. Setting this property to false when CompanyReleaseCollection has been fetched
		/// will clear the CompanyReleaseCollection collection well. Setting this property to true while CompanyReleaseCollection hasn't been fetched disables lazy loading for CompanyReleaseCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyReleaseCollection
		{
			get { return _alreadyFetchedCompanyReleaseCollection;}
			set 
			{
				if(_alreadyFetchedCompanyReleaseCollection && !value && (_companyReleaseCollection != null))
				{
					_companyReleaseCollection.Clear();
				}
				_alreadyFetchedCompanyReleaseCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIntermediateReleaseCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReleaseCollection IntermediateReleaseCollection
		{
			get	{ return GetMultiIntermediateReleaseCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IntermediateReleaseCollection. When set to true, IntermediateReleaseCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IntermediateReleaseCollection is accessed. You can always execute/ a forced fetch by calling GetMultiIntermediateReleaseCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIntermediateReleaseCollection
		{
			get	{ return _alwaysFetchIntermediateReleaseCollection; }
			set	{ _alwaysFetchIntermediateReleaseCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property IntermediateReleaseCollection already has been fetched. Setting this property to false when IntermediateReleaseCollection has been fetched
		/// will clear the IntermediateReleaseCollection collection well. Setting this property to true while IntermediateReleaseCollection hasn't been fetched disables lazy loading for IntermediateReleaseCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIntermediateReleaseCollection
		{
			get { return _alreadyFetchedIntermediateReleaseCollection;}
			set 
			{
				if(_alreadyFetchedIntermediateReleaseCollection && !value && (_intermediateReleaseCollection != null))
				{
					_intermediateReleaseCollection.Clear();
				}
				_alreadyFetchedIntermediateReleaseCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ApplicationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationEntity ApplicationEntity
		{
			get	{ return GetSingleApplicationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReleaseCollection", "ApplicationEntity", _applicationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationEntity. When set to true, ApplicationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationEntity
		{
			get	{ return _alwaysFetchApplicationEntity; }
			set	{ _alwaysFetchApplicationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationEntity already has been fetched. Setting this property to false when ApplicationEntity has been fetched
		/// will set ApplicationEntity to null as well. Setting this property to true while ApplicationEntity hasn't been fetched disables lazy loading for ApplicationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationEntity
		{
			get { return _alreadyFetchedApplicationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationEntity && !value)
				{
					this.ApplicationEntity = null;
				}
				_alreadyFetchedApplicationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationEntity is not found
		/// in the database. When set to true, ApplicationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationEntityReturnsNewIfNotFound; }
			set { _applicationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReleaseEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIntermedateReleaseEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReleaseEntity IntermedateReleaseEntity
		{
			get	{ return GetSingleIntermedateReleaseEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIntermedateReleaseEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "IntermediateReleaseCollection", "IntermedateReleaseEntity", _intermedateReleaseEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IntermedateReleaseEntity. When set to true, IntermedateReleaseEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IntermedateReleaseEntity is accessed. You can always execute a forced fetch by calling GetSingleIntermedateReleaseEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIntermedateReleaseEntity
		{
			get	{ return _alwaysFetchIntermedateReleaseEntity; }
			set	{ _alwaysFetchIntermedateReleaseEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IntermedateReleaseEntity already has been fetched. Setting this property to false when IntermedateReleaseEntity has been fetched
		/// will set IntermedateReleaseEntity to null as well. Setting this property to true while IntermedateReleaseEntity hasn't been fetched disables lazy loading for IntermedateReleaseEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIntermedateReleaseEntity
		{
			get { return _alreadyFetchedIntermedateReleaseEntity;}
			set 
			{
				if(_alreadyFetchedIntermedateReleaseEntity && !value)
				{
					this.IntermedateReleaseEntity = null;
				}
				_alreadyFetchedIntermedateReleaseEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IntermedateReleaseEntity is not found
		/// in the database. When set to true, IntermedateReleaseEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool IntermedateReleaseEntityReturnsNewIfNotFound
		{
			get	{ return _intermedateReleaseEntityReturnsNewIfNotFound; }
			set { _intermedateReleaseEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReleaseEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
