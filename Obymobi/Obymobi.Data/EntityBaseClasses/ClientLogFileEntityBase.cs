﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ClientLogFile'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ClientLogFileEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ClientLogFileEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientLogCollection	_clientLogCollection;
		private bool	_alwaysFetchClientLogCollection, _alreadyFetchedClientLogCollection;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaClientLog;
		private bool	_alwaysFetchClientCollectionViaClientLog, _alreadyFetchedClientCollectionViaClientLog;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientLogCollection</summary>
			public static readonly string ClientLogCollection = "ClientLogCollection";
			/// <summary>Member name ClientCollectionViaClientLog</summary>
			public static readonly string ClientCollectionViaClientLog = "ClientCollectionViaClientLog";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientLogFileEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ClientLogFileEntityBase() :base("ClientLogFileEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		protected ClientLogFileEntityBase(System.Int32 clientLogFileId):base("ClientLogFileEntity")
		{
			InitClassFetch(clientLogFileId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ClientLogFileEntityBase(System.Int32 clientLogFileId, IPrefetchPath prefetchPathToUse): base("ClientLogFileEntity")
		{
			InitClassFetch(clientLogFileId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="validator">The custom validator object for this ClientLogFileEntity</param>
		protected ClientLogFileEntityBase(System.Int32 clientLogFileId, IValidator validator):base("ClientLogFileEntity")
		{
			InitClassFetch(clientLogFileId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientLogFileEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientLogCollection = (Obymobi.Data.CollectionClasses.ClientLogCollection)info.GetValue("_clientLogCollection", typeof(Obymobi.Data.CollectionClasses.ClientLogCollection));
			_alwaysFetchClientLogCollection = info.GetBoolean("_alwaysFetchClientLogCollection");
			_alreadyFetchedClientLogCollection = info.GetBoolean("_alreadyFetchedClientLogCollection");
			_clientCollectionViaClientLog = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaClientLog", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaClientLog = info.GetBoolean("_alwaysFetchClientCollectionViaClientLog");
			_alreadyFetchedClientCollectionViaClientLog = info.GetBoolean("_alreadyFetchedClientCollectionViaClientLog");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientLogCollection = (_clientLogCollection.Count > 0);
			_alreadyFetchedClientCollectionViaClientLog = (_clientCollectionViaClientLog.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientLogCollection":
					toReturn.Add(Relations.ClientLogEntityUsingClientLogFileId);
					break;
				case "ClientCollectionViaClientLog":
					toReturn.Add(Relations.ClientLogEntityUsingClientLogFileId, "ClientLogFileEntity__", "ClientLog_", JoinHint.None);
					toReturn.Add(ClientLogEntity.Relations.ClientEntityUsingClientId, "ClientLog_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientLogCollection", (!this.MarkedForDeletion?_clientLogCollection:null));
			info.AddValue("_alwaysFetchClientLogCollection", _alwaysFetchClientLogCollection);
			info.AddValue("_alreadyFetchedClientLogCollection", _alreadyFetchedClientLogCollection);
			info.AddValue("_clientCollectionViaClientLog", (!this.MarkedForDeletion?_clientCollectionViaClientLog:null));
			info.AddValue("_alwaysFetchClientCollectionViaClientLog", _alwaysFetchClientCollectionViaClientLog);
			info.AddValue("_alreadyFetchedClientCollectionViaClientLog", _alreadyFetchedClientCollectionViaClientLog);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientLogCollection":
					_alreadyFetchedClientLogCollection = true;
					if(entity!=null)
					{
						this.ClientLogCollection.Add((ClientLogEntity)entity);
					}
					break;
				case "ClientCollectionViaClientLog":
					_alreadyFetchedClientCollectionViaClientLog = true;
					if(entity!=null)
					{
						this.ClientCollectionViaClientLog.Add((ClientEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientLogCollection":
					_clientLogCollection.Add((ClientLogEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientLogCollection":
					this.PerformRelatedEntityRemoval(_clientLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientLogCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogFileId)
		{
			return FetchUsingPK(clientLogFileId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogFileId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientLogFileId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientLogFileId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientLogFileId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientLogFileId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientLogFileRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch)
		{
			return GetMultiClientLogCollection(forceFetch, _clientLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientLogCollection(forceFetch, _clientLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientLogCollection || forceFetch || _alwaysFetchClientLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientLogCollection);
				_clientLogCollection.SuppressClearInGetMulti=!forceFetch;
				_clientLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientLogCollection.GetMultiManyToOne(null, this, filter);
				_clientLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientLogCollection = true;
			}
			return _clientLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientLogCollection'. These settings will be taken into account
		/// when the property ClientLogCollection is requested or GetMultiClientLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientLogCollection.SortClauses=sortClauses;
			_clientLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaClientLog(bool forceFetch)
		{
			return GetMultiClientCollectionViaClientLog(forceFetch, _clientCollectionViaClientLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaClientLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaClientLog || forceFetch || _alwaysFetchClientCollectionViaClientLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaClientLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientLogFileFields.ClientLogFileId, ComparisonOperator.Equal, this.ClientLogFileId, "ClientLogFileEntity__"));
				_clientCollectionViaClientLog.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaClientLog.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaClientLog.GetMulti(filter, GetRelationsForField("ClientCollectionViaClientLog"));
				_clientCollectionViaClientLog.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaClientLog = true;
			}
			return _clientCollectionViaClientLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaClientLog'. These settings will be taken into account
		/// when the property ClientCollectionViaClientLog is requested or GetMultiClientCollectionViaClientLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaClientLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaClientLog.SortClauses=sortClauses;
			_clientCollectionViaClientLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientLogCollection", _clientLogCollection);
			toReturn.Add("ClientCollectionViaClientLog", _clientCollectionViaClientLog);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="validator">The validator object for this ClientLogFileEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 clientLogFileId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientLogFileId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientLogCollection = new Obymobi.Data.CollectionClasses.ClientLogCollection();
			_clientLogCollection.SetContainingEntityInfo(this, "ClientLogFileEntity");
			_clientCollectionViaClientLog = new Obymobi.Data.CollectionClasses.ClientCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientLogFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Application", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientLogFileId">PK value for ClientLogFile which data should be fetched into this ClientLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 clientLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientLogFileFieldIndex.ClientLogFileId].ForcedCurrentValueWrite(clientLogFileId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientLogFileDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientLogFileEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientLogFileRelations Relations
		{
			get	{ return new ClientLogFileRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientLogCollection(), (IEntityRelation)GetRelationsForField("ClientLogCollection")[0], (int)Obymobi.Data.EntityType.ClientLogFileEntity, (int)Obymobi.Data.EntityType.ClientLogEntity, 0, null, null, null, "ClientLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaClientLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientLogEntityUsingClientLogFileId;
				intermediateRelation.SetAliases(string.Empty, "ClientLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientLogFileEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaClientLog"), "ClientCollectionViaClientLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientLogFileId property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."ClientLogFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientLogFileId
		{
			get { return (System.Int32)GetValue((int)ClientLogFileFieldIndex.ClientLogFileId, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.ClientLogFileId, value, true); }
		}

		/// <summary> The Message property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)ClientLogFileFieldIndex.Message, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.Message, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFileFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFileFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The LogDate property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."LogDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LogDate
		{
			get { return (System.DateTime)GetValue((int)ClientLogFileFieldIndex.LogDate, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.LogDate, value, true); }
		}

		/// <summary> The Application property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."Application"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Application
		{
			get { return (System.String)GetValue((int)ClientLogFileFieldIndex.Application, true); }
			set	{ SetValue((int)ClientLogFileFieldIndex.Application, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFileFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientLogFileFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ClientLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLogFile"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFileFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientLogFileFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientLogCollection ClientLogCollection
		{
			get	{ return GetMultiClientLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientLogCollection. When set to true, ClientLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientLogCollection
		{
			get	{ return _alwaysFetchClientLogCollection; }
			set	{ _alwaysFetchClientLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientLogCollection already has been fetched. Setting this property to false when ClientLogCollection has been fetched
		/// will clear the ClientLogCollection collection well. Setting this property to true while ClientLogCollection hasn't been fetched disables lazy loading for ClientLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientLogCollection
		{
			get { return _alreadyFetchedClientLogCollection;}
			set 
			{
				if(_alreadyFetchedClientLogCollection && !value && (_clientLogCollection != null))
				{
					_clientLogCollection.Clear();
				}
				_alreadyFetchedClientLogCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaClientLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaClientLog
		{
			get { return GetMultiClientCollectionViaClientLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaClientLog. When set to true, ClientCollectionViaClientLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaClientLog is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaClientLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaClientLog
		{
			get	{ return _alwaysFetchClientCollectionViaClientLog; }
			set	{ _alwaysFetchClientCollectionViaClientLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaClientLog already has been fetched. Setting this property to false when ClientCollectionViaClientLog has been fetched
		/// will clear the ClientCollectionViaClientLog collection well. Setting this property to true while ClientCollectionViaClientLog hasn't been fetched disables lazy loading for ClientCollectionViaClientLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaClientLog
		{
			get { return _alreadyFetchedClientCollectionViaClientLog;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaClientLog && !value && (_clientCollectionViaClientLog != null))
				{
					_clientCollectionViaClientLog.Clear();
				}
				_alreadyFetchedClientCollectionViaClientLog = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ClientLogFileEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
