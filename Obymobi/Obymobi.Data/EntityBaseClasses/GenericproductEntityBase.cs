﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Genericproduct'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GenericproductEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GenericproductEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection	_advertisementTagGenericproductCollection;
		private bool	_alwaysFetchAdvertisementTagGenericproductCollection, _alreadyFetchedAdvertisementTagGenericproductCollection;
		private Obymobi.Data.CollectionClasses.AttachmentCollection	_attachmentCollection;
		private bool	_alwaysFetchAttachmentCollection, _alreadyFetchedAttachmentCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection	_genericproductGenericalterationCollection;
		private bool	_alwaysFetchGenericproductGenericalterationCollection, _alreadyFetchedGenericproductGenericalterationCollection;
		private Obymobi.Data.CollectionClasses.GenericproductLanguageCollection	_genericproductLanguageCollection;
		private bool	_alwaysFetchGenericproductLanguageCollection, _alreadyFetchedGenericproductLanguageCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAdvertisement;
		private bool	_alwaysFetchCategoryCollectionViaAdvertisement, _alreadyFetchedCategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAdvertisement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAdvertisement, _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement, _alreadyFetchedEntertainmentCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement, _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.GenericalterationCollection _genericalterationCollectionViaGenericproductGenericalteration;
		private bool	_alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration, _alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaProduct;
		private bool	_alwaysFetchRouteCollectionViaProduct, _alreadyFetchedRouteCollectionViaProduct;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.VattariffCollection _vattariffCollectionViaProduct;
		private bool	_alwaysFetchVattariffCollectionViaProduct, _alreadyFetchedVattariffCollectionViaProduct;
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;
		private GenericcategoryEntity _genericcategoryEntity;
		private bool	_alwaysFetchGenericcategoryEntity, _alreadyFetchedGenericcategoryEntity, _genericcategoryEntityReturnsNewIfNotFound;
		private SupplierEntity _supplierEntity;
		private bool	_alwaysFetchSupplierEntity, _alreadyFetchedSupplierEntity, _supplierEntityReturnsNewIfNotFound;
		private VattariffEntity _vattariffEntity;
		private bool	_alwaysFetchVattariffEntity, _alreadyFetchedVattariffEntity, _vattariffEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name GenericcategoryEntity</summary>
			public static readonly string GenericcategoryEntity = "GenericcategoryEntity";
			/// <summary>Member name SupplierEntity</summary>
			public static readonly string SupplierEntity = "SupplierEntity";
			/// <summary>Member name VattariffEntity</summary>
			public static readonly string VattariffEntity = "VattariffEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AdvertisementTagGenericproductCollection</summary>
			public static readonly string AdvertisementTagGenericproductCollection = "AdvertisementTagGenericproductCollection";
			/// <summary>Member name AttachmentCollection</summary>
			public static readonly string AttachmentCollection = "AttachmentCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name GenericproductGenericalterationCollection</summary>
			public static readonly string GenericproductGenericalterationCollection = "GenericproductGenericalterationCollection";
			/// <summary>Member name GenericproductLanguageCollection</summary>
			public static readonly string GenericproductLanguageCollection = "GenericproductLanguageCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaAdvertisement</summary>
			public static readonly string CategoryCollectionViaAdvertisement = "CategoryCollectionViaAdvertisement";
			/// <summary>Member name DeliverypointgroupCollectionViaAdvertisement</summary>
			public static readonly string DeliverypointgroupCollectionViaAdvertisement = "DeliverypointgroupCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement = "EntertainmentCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAdvertisement = "EntertainmentcategoryCollectionViaAdvertisement";
			/// <summary>Member name GenericalterationCollectionViaGenericproductGenericalteration</summary>
			public static readonly string GenericalterationCollectionViaGenericproductGenericalteration = "GenericalterationCollectionViaGenericproductGenericalteration";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name RouteCollectionViaProduct</summary>
			public static readonly string RouteCollectionViaProduct = "RouteCollectionViaProduct";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
			/// <summary>Member name VattariffCollectionViaProduct</summary>
			public static readonly string VattariffCollectionViaProduct = "VattariffCollectionViaProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GenericproductEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GenericproductEntityBase() :base("GenericproductEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		protected GenericproductEntityBase(System.Int32 genericproductId):base("GenericproductEntity")
		{
			InitClassFetch(genericproductId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GenericproductEntityBase(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse): base("GenericproductEntity")
		{
			InitClassFetch(genericproductId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="validator">The custom validator object for this GenericproductEntity</param>
		protected GenericproductEntityBase(System.Int32 genericproductId, IValidator validator):base("GenericproductEntity")
		{
			InitClassFetch(genericproductId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericproductEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_advertisementTagGenericproductCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection)info.GetValue("_advertisementTagGenericproductCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection));
			_alwaysFetchAdvertisementTagGenericproductCollection = info.GetBoolean("_alwaysFetchAdvertisementTagGenericproductCollection");
			_alreadyFetchedAdvertisementTagGenericproductCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagGenericproductCollection");

			_attachmentCollection = (Obymobi.Data.CollectionClasses.AttachmentCollection)info.GetValue("_attachmentCollection", typeof(Obymobi.Data.CollectionClasses.AttachmentCollection));
			_alwaysFetchAttachmentCollection = info.GetBoolean("_alwaysFetchAttachmentCollection");
			_alreadyFetchedAttachmentCollection = info.GetBoolean("_alreadyFetchedAttachmentCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_genericproductGenericalterationCollection = (Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection)info.GetValue("_genericproductGenericalterationCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection));
			_alwaysFetchGenericproductGenericalterationCollection = info.GetBoolean("_alwaysFetchGenericproductGenericalterationCollection");
			_alreadyFetchedGenericproductGenericalterationCollection = info.GetBoolean("_alreadyFetchedGenericproductGenericalterationCollection");

			_genericproductLanguageCollection = (Obymobi.Data.CollectionClasses.GenericproductLanguageCollection)info.GetValue("_genericproductLanguageCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductLanguageCollection));
			_alwaysFetchGenericproductLanguageCollection = info.GetBoolean("_alwaysFetchGenericproductLanguageCollection");
			_alreadyFetchedGenericproductLanguageCollection = info.GetBoolean("_alreadyFetchedGenericproductLanguageCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");
			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAdvertisement");
			_alreadyFetchedCategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAdvertisement");

			_deliverypointgroupCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement");
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement");

			_entertainmentCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_entertainmentcategoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement");

			_genericalterationCollectionViaGenericproductGenericalteration = (Obymobi.Data.CollectionClasses.GenericalterationCollection)info.GetValue("_genericalterationCollectionViaGenericproductGenericalteration", typeof(Obymobi.Data.CollectionClasses.GenericalterationCollection));
			_alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration = info.GetBoolean("_alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration");
			_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration = info.GetBoolean("_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_routeCollectionViaProduct = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaProduct = info.GetBoolean("_alwaysFetchRouteCollectionViaProduct");
			_alreadyFetchedRouteCollectionViaProduct = info.GetBoolean("_alreadyFetchedRouteCollectionViaProduct");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");

			_vattariffCollectionViaProduct = (Obymobi.Data.CollectionClasses.VattariffCollection)info.GetValue("_vattariffCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.VattariffCollection));
			_alwaysFetchVattariffCollectionViaProduct = info.GetBoolean("_alwaysFetchVattariffCollectionViaProduct");
			_alreadyFetchedVattariffCollectionViaProduct = info.GetBoolean("_alreadyFetchedVattariffCollectionViaProduct");
			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");

			_genericcategoryEntity = (GenericcategoryEntity)info.GetValue("_genericcategoryEntity", typeof(GenericcategoryEntity));
			if(_genericcategoryEntity!=null)
			{
				_genericcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_genericcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchGenericcategoryEntity = info.GetBoolean("_alwaysFetchGenericcategoryEntity");
			_alreadyFetchedGenericcategoryEntity = info.GetBoolean("_alreadyFetchedGenericcategoryEntity");

			_supplierEntity = (SupplierEntity)info.GetValue("_supplierEntity", typeof(SupplierEntity));
			if(_supplierEntity!=null)
			{
				_supplierEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_supplierEntityReturnsNewIfNotFound = info.GetBoolean("_supplierEntityReturnsNewIfNotFound");
			_alwaysFetchSupplierEntity = info.GetBoolean("_alwaysFetchSupplierEntity");
			_alreadyFetchedSupplierEntity = info.GetBoolean("_alreadyFetchedSupplierEntity");

			_vattariffEntity = (VattariffEntity)info.GetValue("_vattariffEntity", typeof(VattariffEntity));
			if(_vattariffEntity!=null)
			{
				_vattariffEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vattariffEntityReturnsNewIfNotFound = info.GetBoolean("_vattariffEntityReturnsNewIfNotFound");
			_alwaysFetchVattariffEntity = info.GetBoolean("_alwaysFetchVattariffEntity");
			_alreadyFetchedVattariffEntity = info.GetBoolean("_alreadyFetchedVattariffEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GenericproductFieldIndex)fieldIndex)
			{
				case GenericproductFieldIndex.SupplierId:
					DesetupSyncSupplierEntity(true, false);
					_alreadyFetchedSupplierEntity = false;
					break;
				case GenericproductFieldIndex.GenericcategoryId:
					DesetupSyncGenericcategoryEntity(true, false);
					_alreadyFetchedGenericcategoryEntity = false;
					break;
				case GenericproductFieldIndex.VattariffId:
					DesetupSyncVattariffEntity(true, false);
					_alreadyFetchedVattariffEntity = false;
					break;
				case GenericproductFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAdvertisementTagGenericproductCollection = (_advertisementTagGenericproductCollection.Count > 0);
			_alreadyFetchedAttachmentCollection = (_attachmentCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedGenericproductGenericalterationCollection = (_genericproductGenericalterationCollection.Count > 0);
			_alreadyFetchedGenericproductLanguageCollection = (_genericproductLanguageCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaAdvertisement = (_categoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = (_deliverypointgroupCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = (_entertainmentCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = (_entertainmentcategoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration = (_genericalterationCollectionViaGenericproductGenericalteration.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedRouteCollectionViaProduct = (_routeCollectionViaProduct.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedVattariffCollectionViaProduct = (_vattariffCollectionViaProduct.Count > 0);
			_alreadyFetchedBrandEntity = (_brandEntity != null);
			_alreadyFetchedGenericcategoryEntity = (_genericcategoryEntity != null);
			_alreadyFetchedSupplierEntity = (_supplierEntity != null);
			_alreadyFetchedVattariffEntity = (_vattariffEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "GenericcategoryEntity":
					toReturn.Add(Relations.GenericcategoryEntityUsingGenericcategoryId);
					break;
				case "SupplierEntity":
					toReturn.Add(Relations.SupplierEntityUsingSupplierId);
					break;
				case "VattariffEntity":
					toReturn.Add(Relations.VattariffEntityUsingVattariffId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingGenericproductId);
					break;
				case "AdvertisementTagGenericproductCollection":
					toReturn.Add(Relations.AdvertisementTagGenericproductEntityUsingGenericproductId);
					break;
				case "AttachmentCollection":
					toReturn.Add(Relations.AttachmentEntityUsingGenericproductId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingGenericproductId);
					break;
				case "GenericproductGenericalterationCollection":
					toReturn.Add(Relations.GenericproductGenericalterationEntityUsingGenericproductId);
					break;
				case "GenericproductLanguageCollection":
					toReturn.Add(Relations.GenericproductLanguageEntityUsingGenericproductId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingGenericproductId);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingGenericproductId, "GenericproductEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CategoryEntityUsingActionCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingGenericproductId, "GenericproductEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingGenericproductId, "GenericproductEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingGenericproductId, "GenericproductEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "GenericalterationCollectionViaGenericproductGenericalteration":
					toReturn.Add(Relations.GenericproductGenericalterationEntityUsingGenericproductId, "GenericproductEntity__", "GenericproductGenericalteration_", JoinHint.None);
					toReturn.Add(GenericproductGenericalterationEntity.Relations.GenericalterationEntityUsingGenericalterationId, "GenericproductGenericalteration_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingGenericproductId, "GenericproductEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.RouteEntityUsingRouteId, "Product_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericproductId, "GenericproductEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				case "VattariffCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingGenericproductId, "GenericproductEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.VattariffEntityUsingVattariffId, "Product_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_advertisementTagGenericproductCollection", (!this.MarkedForDeletion?_advertisementTagGenericproductCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagGenericproductCollection", _alwaysFetchAdvertisementTagGenericproductCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagGenericproductCollection", _alreadyFetchedAdvertisementTagGenericproductCollection);
			info.AddValue("_attachmentCollection", (!this.MarkedForDeletion?_attachmentCollection:null));
			info.AddValue("_alwaysFetchAttachmentCollection", _alwaysFetchAttachmentCollection);
			info.AddValue("_alreadyFetchedAttachmentCollection", _alreadyFetchedAttachmentCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_genericproductGenericalterationCollection", (!this.MarkedForDeletion?_genericproductGenericalterationCollection:null));
			info.AddValue("_alwaysFetchGenericproductGenericalterationCollection", _alwaysFetchGenericproductGenericalterationCollection);
			info.AddValue("_alreadyFetchedGenericproductGenericalterationCollection", _alreadyFetchedGenericproductGenericalterationCollection);
			info.AddValue("_genericproductLanguageCollection", (!this.MarkedForDeletion?_genericproductLanguageCollection:null));
			info.AddValue("_alwaysFetchGenericproductLanguageCollection", _alwaysFetchGenericproductLanguageCollection);
			info.AddValue("_alreadyFetchedGenericproductLanguageCollection", _alreadyFetchedGenericproductLanguageCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_categoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAdvertisement", _alwaysFetchCategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAdvertisement", _alreadyFetchedCategoryCollectionViaAdvertisement);
			info.AddValue("_deliverypointgroupCollectionViaAdvertisement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement", _alwaysFetchDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement", _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement", _alwaysFetchEntertainmentCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement", _alreadyFetchedEntertainmentCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement", _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement", _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_genericalterationCollectionViaGenericproductGenericalteration", (!this.MarkedForDeletion?_genericalterationCollectionViaGenericproductGenericalteration:null));
			info.AddValue("_alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration", _alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration);
			info.AddValue("_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration", _alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_routeCollectionViaProduct", (!this.MarkedForDeletion?_routeCollectionViaProduct:null));
			info.AddValue("_alwaysFetchRouteCollectionViaProduct", _alwaysFetchRouteCollectionViaProduct);
			info.AddValue("_alreadyFetchedRouteCollectionViaProduct", _alreadyFetchedRouteCollectionViaProduct);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_vattariffCollectionViaProduct", (!this.MarkedForDeletion?_vattariffCollectionViaProduct:null));
			info.AddValue("_alwaysFetchVattariffCollectionViaProduct", _alwaysFetchVattariffCollectionViaProduct);
			info.AddValue("_alreadyFetchedVattariffCollectionViaProduct", _alreadyFetchedVattariffCollectionViaProduct);
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);
			info.AddValue("_genericcategoryEntity", (!this.MarkedForDeletion?_genericcategoryEntity:null));
			info.AddValue("_genericcategoryEntityReturnsNewIfNotFound", _genericcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericcategoryEntity", _alwaysFetchGenericcategoryEntity);
			info.AddValue("_alreadyFetchedGenericcategoryEntity", _alreadyFetchedGenericcategoryEntity);
			info.AddValue("_supplierEntity", (!this.MarkedForDeletion?_supplierEntity:null));
			info.AddValue("_supplierEntityReturnsNewIfNotFound", _supplierEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSupplierEntity", _alwaysFetchSupplierEntity);
			info.AddValue("_alreadyFetchedSupplierEntity", _alreadyFetchedSupplierEntity);
			info.AddValue("_vattariffEntity", (!this.MarkedForDeletion?_vattariffEntity:null));
			info.AddValue("_vattariffEntityReturnsNewIfNotFound", _vattariffEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVattariffEntity", _alwaysFetchVattariffEntity);
			info.AddValue("_alreadyFetchedVattariffEntity", _alreadyFetchedVattariffEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "GenericcategoryEntity":
					_alreadyFetchedGenericcategoryEntity = true;
					this.GenericcategoryEntity = (GenericcategoryEntity)entity;
					break;
				case "SupplierEntity":
					_alreadyFetchedSupplierEntity = true;
					this.SupplierEntity = (SupplierEntity)entity;
					break;
				case "VattariffEntity":
					_alreadyFetchedVattariffEntity = true;
					this.VattariffEntity = (VattariffEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AdvertisementTagGenericproductCollection":
					_alreadyFetchedAdvertisementTagGenericproductCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagGenericproductCollection.Add((AdvertisementTagGenericproductEntity)entity);
					}
					break;
				case "AttachmentCollection":
					_alreadyFetchedAttachmentCollection = true;
					if(entity!=null)
					{
						this.AttachmentCollection.Add((AttachmentEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "GenericproductGenericalterationCollection":
					_alreadyFetchedGenericproductGenericalterationCollection = true;
					if(entity!=null)
					{
						this.GenericproductGenericalterationCollection.Add((GenericproductGenericalterationEntity)entity);
					}
					break;
				case "GenericproductLanguageCollection":
					_alreadyFetchedGenericproductLanguageCollection = true;
					if(entity!=null)
					{
						this.GenericproductLanguageCollection.Add((GenericproductLanguageEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaAdvertisement":
					_alreadyFetchedCategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAdvertisement.Add((CategoryEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAdvertisement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAdvertisement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericalterationCollectionViaGenericproductGenericalteration":
					_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration = true;
					if(entity!=null)
					{
						this.GenericalterationCollectionViaGenericproductGenericalteration.Add((GenericalterationEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaProduct":
					_alreadyFetchedRouteCollectionViaProduct = true;
					if(entity!=null)
					{
						this.RouteCollectionViaProduct.Add((RouteEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				case "VattariffCollectionViaProduct":
					_alreadyFetchedVattariffCollectionViaProduct = true;
					if(entity!=null)
					{
						this.VattariffCollectionViaProduct.Add((VattariffEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "GenericcategoryEntity":
					SetupSyncGenericcategoryEntity(relatedEntity);
					break;
				case "SupplierEntity":
					SetupSyncSupplierEntity(relatedEntity);
					break;
				case "VattariffEntity":
					SetupSyncVattariffEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AdvertisementTagGenericproductCollection":
					_advertisementTagGenericproductCollection.Add((AdvertisementTagGenericproductEntity)relatedEntity);
					break;
				case "AttachmentCollection":
					_attachmentCollection.Add((AttachmentEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "GenericproductGenericalterationCollection":
					_genericproductGenericalterationCollection.Add((GenericproductGenericalterationEntity)relatedEntity);
					break;
				case "GenericproductLanguageCollection":
					_genericproductLanguageCollection.Add((GenericproductLanguageEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "GenericcategoryEntity":
					DesetupSyncGenericcategoryEntity(false, true);
					break;
				case "SupplierEntity":
					DesetupSyncSupplierEntity(false, true);
					break;
				case "VattariffEntity":
					DesetupSyncVattariffEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagGenericproductCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagGenericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttachmentCollection":
					this.PerformRelatedEntityRemoval(_attachmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductGenericalterationCollection":
					this.PerformRelatedEntityRemoval(_genericproductGenericalterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductLanguageCollection":
					this.PerformRelatedEntityRemoval(_genericproductLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			if(_genericcategoryEntity!=null)
			{
				toReturn.Add(_genericcategoryEntity);
			}
			if(_supplierEntity!=null)
			{
				toReturn.Add(_supplierEntity);
			}
			if(_vattariffEntity!=null)
			{
				toReturn.Add(_vattariffEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_advertisementTagGenericproductCollection);
			toReturn.Add(_attachmentCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_genericproductGenericalterationCollection);
			toReturn.Add(_genericproductLanguageCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_productCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericproductId)
		{
			return FetchUsingPK(genericproductId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(genericproductId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(genericproductId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(genericproductId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GenericproductId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GenericproductRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagGenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, _advertisementTagGenericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagGenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, _advertisementTagGenericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagGenericproductCollection || forceFetch || _alwaysFetchAdvertisementTagGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagGenericproductCollection);
				_advertisementTagGenericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagGenericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagGenericproductCollection.GetMultiManyToOne(null, this, filter);
				_advertisementTagGenericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagGenericproductCollection = true;
			}
			return _advertisementTagGenericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagGenericproductCollection'. These settings will be taken into account
		/// when the property AdvertisementTagGenericproductCollection is requested or GetMultiAdvertisementTagGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagGenericproductCollection.SortClauses=sortClauses;
			_advertisementTagGenericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttachmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttachmentCollection || forceFetch || _alwaysFetchAttachmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attachmentCollection);
				_attachmentCollection.SuppressClearInGetMulti=!forceFetch;
				_attachmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_attachmentCollection.GetMultiManyToOne(this, null, null, null, filter);
				_attachmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAttachmentCollection = true;
			}
			return _attachmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttachmentCollection'. These settings will be taken into account
		/// when the property AttachmentCollection is requested or GetMultiAttachmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttachmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attachmentCollection.SortClauses=sortClauses;
			_attachmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductGenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, _genericproductGenericalterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductGenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, _genericproductGenericalterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductGenericalterationCollection || forceFetch || _alwaysFetchGenericproductGenericalterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductGenericalterationCollection);
				_genericproductGenericalterationCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductGenericalterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductGenericalterationCollection.GetMultiManyToOne(null, this, filter);
				_genericproductGenericalterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductGenericalterationCollection = true;
			}
			return _genericproductGenericalterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductGenericalterationCollection'. These settings will be taken into account
		/// when the property GenericproductGenericalterationCollection is requested or GetMultiGenericproductGenericalterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductGenericalterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductGenericalterationCollection.SortClauses=sortClauses;
			_genericproductGenericalterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, _genericproductLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, _genericproductLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductLanguageCollection || forceFetch || _alwaysFetchGenericproductLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductLanguageCollection);
				_genericproductLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductLanguageCollection.GetMultiManyToOne(this, null, filter);
				_genericproductLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductLanguageCollection = true;
			}
			return _genericproductLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductLanguageCollection'. These settings will be taken into account
		/// when the property GenericproductLanguageCollection is requested or GetMultiGenericproductLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductLanguageCollection.SortClauses=sortClauses;
			_genericproductLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAdvertisement(forceFetch, _categoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchCategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAdvertisement"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAdvertisement = true;
			}
			return _categoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAdvertisement is requested or GetMultiCategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_categoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAdvertisement(forceFetch, _deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
			}
			return _deliverypointgroupCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAdvertisement is requested or GetMultiDeliverypointgroupCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAdvertisement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement(forceFetch, _entertainmentCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
			}
			return _entertainmentCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement is requested or GetMultiEntertainmentCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAdvertisement(forceFetch, _entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
			}
			return _entertainmentcategoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAdvertisement is requested or GetMultiEntertainmentcategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaGenericproductGenericalteration(bool forceFetch)
		{
			return GetMultiGenericalterationCollectionViaGenericproductGenericalteration(forceFetch, _genericalterationCollectionViaGenericproductGenericalteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaGenericproductGenericalteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration || forceFetch || _alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationCollectionViaGenericproductGenericalteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_genericalterationCollectionViaGenericproductGenericalteration.SuppressClearInGetMulti=!forceFetch;
				_genericalterationCollectionViaGenericproductGenericalteration.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationCollectionViaGenericproductGenericalteration.GetMulti(filter, GetRelationsForField("GenericalterationCollectionViaGenericproductGenericalteration"));
				_genericalterationCollectionViaGenericproductGenericalteration.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration = true;
			}
			return _genericalterationCollectionViaGenericproductGenericalteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationCollectionViaGenericproductGenericalteration'. These settings will be taken into account
		/// when the property GenericalterationCollectionViaGenericproductGenericalteration is requested or GetMultiGenericalterationCollectionViaGenericproductGenericalteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationCollectionViaGenericproductGenericalteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationCollectionViaGenericproductGenericalteration.SortClauses=sortClauses;
			_genericalterationCollectionViaGenericproductGenericalteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch)
		{
			return GetMultiRouteCollectionViaProduct(forceFetch, _routeCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaProduct || forceFetch || _alwaysFetchRouteCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaProduct.GetMulti(filter, GetRelationsForField("RouteCollectionViaProduct"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaProduct = true;
			}
			return _routeCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaProduct'. These settings will be taken into account
		/// when the property RouteCollectionViaProduct is requested or GetMultiRouteCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaProduct.SortClauses=sortClauses;
			_routeCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VattariffEntity'</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollectionViaProduct(bool forceFetch)
		{
			return GetMultiVattariffCollectionViaProduct(forceFetch, _vattariffCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedVattariffCollectionViaProduct || forceFetch || _alwaysFetchVattariffCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vattariffCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericproductFields.GenericproductId, ComparisonOperator.Equal, this.GenericproductId, "GenericproductEntity__"));
				_vattariffCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_vattariffCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_vattariffCollectionViaProduct.GetMulti(filter, GetRelationsForField("VattariffCollectionViaProduct"));
				_vattariffCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVattariffCollectionViaProduct = true;
			}
			return _vattariffCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VattariffCollectionViaProduct'. These settings will be taken into account
		/// when the property VattariffCollectionViaProduct is requested or GetMultiVattariffCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVattariffCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vattariffCollectionViaProduct.SortClauses=sortClauses;
			_vattariffCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public GenericcategoryEntity GetSingleGenericcategoryEntity()
		{
			return GetSingleGenericcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public virtual GenericcategoryEntity GetSingleGenericcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericcategoryEntity || forceFetch || _alwaysFetchGenericcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericcategoryEntityUsingGenericcategoryId);
				GenericcategoryEntity newEntity = new GenericcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericcategoryEntity = newEntity;
				_alreadyFetchedGenericcategoryEntity = fetchResult;
			}
			return _genericcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'SupplierEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SupplierEntity' which is related to this entity.</returns>
		public SupplierEntity GetSingleSupplierEntity()
		{
			return GetSingleSupplierEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SupplierEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SupplierEntity' which is related to this entity.</returns>
		public virtual SupplierEntity GetSingleSupplierEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSupplierEntity || forceFetch || _alwaysFetchSupplierEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SupplierEntityUsingSupplierId);
				SupplierEntity newEntity = new SupplierEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SupplierId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SupplierEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_supplierEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SupplierEntity = newEntity;
				_alreadyFetchedSupplierEntity = fetchResult;
			}
			return _supplierEntity;
		}


		/// <summary> Retrieves the related entity of type 'VattariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VattariffEntity' which is related to this entity.</returns>
		public VattariffEntity GetSingleVattariffEntity()
		{
			return GetSingleVattariffEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'VattariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VattariffEntity' which is related to this entity.</returns>
		public virtual VattariffEntity GetSingleVattariffEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedVattariffEntity || forceFetch || _alwaysFetchVattariffEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VattariffEntityUsingVattariffId);
				VattariffEntity newEntity = new VattariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VattariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VattariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vattariffEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VattariffEntity = newEntity;
				_alreadyFetchedVattariffEntity = fetchResult;
			}
			return _vattariffEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("GenericcategoryEntity", _genericcategoryEntity);
			toReturn.Add("SupplierEntity", _supplierEntity);
			toReturn.Add("VattariffEntity", _vattariffEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AdvertisementTagGenericproductCollection", _advertisementTagGenericproductCollection);
			toReturn.Add("AttachmentCollection", _attachmentCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("GenericproductGenericalterationCollection", _genericproductGenericalterationCollection);
			toReturn.Add("GenericproductLanguageCollection", _genericproductLanguageCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaAdvertisement", _categoryCollectionViaAdvertisement);
			toReturn.Add("DeliverypointgroupCollectionViaAdvertisement", _deliverypointgroupCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaAdvertisement", _entertainmentCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaAdvertisement", _entertainmentcategoryCollectionViaAdvertisement);
			toReturn.Add("GenericalterationCollectionViaGenericproductGenericalteration", _genericalterationCollectionViaGenericproductGenericalteration);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("RouteCollectionViaProduct", _routeCollectionViaProduct);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			toReturn.Add("VattariffCollectionViaProduct", _vattariffCollectionViaProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="validator">The validator object for this GenericproductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 genericproductId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(genericproductId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_advertisementTagGenericproductCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection();
			_advertisementTagGenericproductCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_attachmentCollection = new Obymobi.Data.CollectionClasses.AttachmentCollection();
			_attachmentCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_genericproductGenericalterationCollection = new Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection();
			_genericproductGenericalterationCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_genericproductLanguageCollection = new Obymobi.Data.CollectionClasses.GenericproductLanguageCollection();
			_genericproductLanguageCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "GenericproductEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "GenericproductEntity");
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_deliverypointgroupCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericalterationCollectionViaGenericproductGenericalteration = new Obymobi.Data.CollectionClasses.GenericalterationCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaProduct = new Obymobi.Data.CollectionClasses.RouteCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_vattariffCollectionViaProduct = new Obymobi.Data.CollectionClasses.VattariffCollection();
			_brandEntityReturnsNewIfNotFound = true;
			_genericcategoryEntityReturnsNewIfNotFound = true;
			_supplierEntityReturnsNewIfNotFound = true;
			_vattariffEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericproductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupplierId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VattariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericproductType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibilityType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HidePrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebTypeSmartphoneUrl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebTypeTabletUrl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewLayoutType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "GenericproductCollection", resetFKFields, new int[] { (int)GenericproductFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] { "BrandName" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Name":
					this.OnPropertyChanged("BrandName");
					break;
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, signalRelatedEntity, "GenericproductCollection", resetFKFields, new int[] { (int)GenericproductFieldIndex.GenericcategoryId } );		
			_genericcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericcategoryEntity(IEntityCore relatedEntity)
		{
			if(_genericcategoryEntity!=relatedEntity)
			{		
				DesetupSyncGenericcategoryEntity(true, true);
				_genericcategoryEntity = (GenericcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, ref _alreadyFetchedGenericcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _supplierEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSupplierEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _supplierEntity, new PropertyChangedEventHandler( OnSupplierEntityPropertyChanged ), "SupplierEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.SupplierEntityUsingSupplierIdStatic, true, signalRelatedEntity, "GenericproductCollection", resetFKFields, new int[] { (int)GenericproductFieldIndex.SupplierId } );		
			_supplierEntity = null;
		}
		
		/// <summary> setups the sync logic for member _supplierEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSupplierEntity(IEntityCore relatedEntity)
		{
			if(_supplierEntity!=relatedEntity)
			{		
				DesetupSyncSupplierEntity(true, true);
				_supplierEntity = (SupplierEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _supplierEntity, new PropertyChangedEventHandler( OnSupplierEntityPropertyChanged ), "SupplierEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.SupplierEntityUsingSupplierIdStatic, true, ref _alreadyFetchedSupplierEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSupplierEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _vattariffEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVattariffEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vattariffEntity, new PropertyChangedEventHandler( OnVattariffEntityPropertyChanged ), "VattariffEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.VattariffEntityUsingVattariffIdStatic, true, signalRelatedEntity, "GenericproductCollection", resetFKFields, new int[] { (int)GenericproductFieldIndex.VattariffId } );		
			_vattariffEntity = null;
		}
		
		/// <summary> setups the sync logic for member _vattariffEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVattariffEntity(IEntityCore relatedEntity)
		{
			if(_vattariffEntity!=relatedEntity)
			{		
				DesetupSyncVattariffEntity(true, true);
				_vattariffEntity = (VattariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vattariffEntity, new PropertyChangedEventHandler( OnVattariffEntityPropertyChanged ), "VattariffEntity", Obymobi.Data.RelationClasses.StaticGenericproductRelations.VattariffEntityUsingVattariffIdStatic, true, ref _alreadyFetchedVattariffEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVattariffEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GenericproductFieldIndex.GenericproductId].ForcedCurrentValueWrite(genericproductId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericproductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GenericproductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GenericproductRelations Relations
		{
			get	{ return new GenericproductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagGenericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagGenericproductCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity, 0, null, null, null, "AdvertisementTagGenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attachment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentCollection(), (IEntityRelation)GetRelationsForField("AttachmentCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.AttachmentEntity, 0, null, null, null, "AttachmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericproductGenericalteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductGenericalterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection(), (IEntityRelation)GetRelationsForField("GenericproductGenericalterationCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.GenericproductGenericalterationEntity, 0, null, null, null, "GenericproductGenericalterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericproductLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductLanguageCollection(), (IEntityRelation)GetRelationsForField("GenericproductLanguageCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.GenericproductLanguageEntity, 0, null, null, null, "GenericproductLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAdvertisement"), "CategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"), "DeliverypointgroupCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement"), "EntertainmentCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"), "EntertainmentcategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationCollectionViaGenericproductGenericalteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.GenericproductGenericalterationEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "GenericproductGenericalteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, GetRelationsForField("GenericalterationCollectionViaGenericproductGenericalteration"), "GenericalterationCollectionViaGenericproductGenericalteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaProduct"), "RouteCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Vattariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVattariffCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingGenericproductId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VattariffCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.VattariffEntity, 0, null, null, GetRelationsForField("VattariffCollectionViaProduct"), "VattariffCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryEntity")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supplier'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupplierEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupplierCollection(), (IEntityRelation)GetRelationsForField("SupplierEntity")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.SupplierEntity, 0, null, null, null, "SupplierEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Vattariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVattariffEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VattariffCollection(), (IEntityRelation)GetRelationsForField("VattariffEntity")[0], (int)Obymobi.Data.EntityType.GenericproductEntity, (int)Obymobi.Data.EntityType.VattariffEntity, 0, null, null, null, "VattariffEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GenericproductId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."GenericproductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GenericproductId
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.GenericproductId, true); }
			set	{ SetValue((int)GenericproductFieldIndex.GenericproductId, value, true); }
		}

		/// <summary> The SupplierId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."SupplierId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupplierId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.SupplierId, false); }
			set	{ SetValue((int)GenericproductFieldIndex.SupplierId, value, true); }
		}

		/// <summary> The GenericcategoryId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."GenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)GenericproductFieldIndex.GenericcategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.Name, true); }
			set	{ SetValue((int)GenericproductFieldIndex.Name, value, true); }
		}

		/// <summary> The Code property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.Code, true); }
			set	{ SetValue((int)GenericproductFieldIndex.Code, value, true); }
		}

		/// <summary> The Description property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.Description, true); }
			set	{ SetValue((int)GenericproductFieldIndex.Description, value, true); }
		}

		/// <summary> The TextColor property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."TextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TextColor
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.TextColor, true); }
			set	{ SetValue((int)GenericproductFieldIndex.TextColor, value, true); }
		}

		/// <summary> The BackgroundColor property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BackgroundColor
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.BackgroundColor, true); }
			set	{ SetValue((int)GenericproductFieldIndex.BackgroundColor, value, true); }
		}

		/// <summary> The VattariffId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."VattariffId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VattariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.VattariffId, false); }
			set	{ SetValue((int)GenericproductFieldIndex.VattariffId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)GenericproductFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)GenericproductFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.ExternalId, false); }
			set	{ SetValue((int)GenericproductFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The GenericproductType property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."GenericproductType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductType
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.GenericproductType, false); }
			set	{ SetValue((int)GenericproductFieldIndex.GenericproductType, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericproductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)GenericproductFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericproductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GenericproductFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The BrandId property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericproductFieldIndex.BrandId, false); }
			set	{ SetValue((int)GenericproductFieldIndex.BrandId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.SortOrder, true); }
			set	{ SetValue((int)GenericproductFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The VisibilityType property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."VisibilityType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.VisibilityType VisibilityType
		{
			get { return (Obymobi.Enums.VisibilityType)GetValue((int)GenericproductFieldIndex.VisibilityType, true); }
			set	{ SetValue((int)GenericproductFieldIndex.VisibilityType, value, true); }
		}

		/// <summary> The Visible property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)GenericproductFieldIndex.Visible, true); }
			set	{ SetValue((int)GenericproductFieldIndex.Visible, value, true); }
		}

		/// <summary> The SubType property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."SubType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SubType
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.SubType, true); }
			set	{ SetValue((int)GenericproductFieldIndex.SubType, value, true); }
		}

		/// <summary> The PriceIn property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceIn
		{
			get { return (Nullable<System.Decimal>)GetValue((int)GenericproductFieldIndex.PriceIn, false); }
			set	{ SetValue((int)GenericproductFieldIndex.PriceIn, value, true); }
		}

		/// <summary> The HidePrice property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."HidePrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrice
		{
			get { return (System.Boolean)GetValue((int)GenericproductFieldIndex.HidePrice, true); }
			set	{ SetValue((int)GenericproductFieldIndex.HidePrice, value, true); }
		}

		/// <summary> The WebTypeSmartphoneUrl property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."WebTypeSmartphoneUrl"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTypeSmartphoneUrl
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.WebTypeSmartphoneUrl, true); }
			set	{ SetValue((int)GenericproductFieldIndex.WebTypeSmartphoneUrl, value, true); }
		}

		/// <summary> The WebTypeTabletUrl property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."WebTypeTabletUrl"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WebTypeTabletUrl
		{
			get { return (System.String)GetValue((int)GenericproductFieldIndex.WebTypeTabletUrl, true); }
			set	{ SetValue((int)GenericproductFieldIndex.WebTypeTabletUrl, value, true); }
		}

		/// <summary> The ViewLayoutType property of the Entity Genericproduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericproduct"."ViewLayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ViewLayoutType
		{
			get { return (System.Int32)GetValue((int)GenericproductFieldIndex.ViewLayoutType, true); }
			set	{ SetValue((int)GenericproductFieldIndex.ViewLayoutType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection AdvertisementTagGenericproductCollection
		{
			get	{ return GetMultiAdvertisementTagGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagGenericproductCollection. When set to true, AdvertisementTagGenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagGenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagGenericproductCollection
		{
			get	{ return _alwaysFetchAdvertisementTagGenericproductCollection; }
			set	{ _alwaysFetchAdvertisementTagGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagGenericproductCollection already has been fetched. Setting this property to false when AdvertisementTagGenericproductCollection has been fetched
		/// will clear the AdvertisementTagGenericproductCollection collection well. Setting this property to true while AdvertisementTagGenericproductCollection hasn't been fetched disables lazy loading for AdvertisementTagGenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagGenericproductCollection
		{
			get { return _alreadyFetchedAdvertisementTagGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagGenericproductCollection && !value && (_advertisementTagGenericproductCollection != null))
				{
					_advertisementTagGenericproductCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagGenericproductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttachmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection AttachmentCollection
		{
			get	{ return GetMultiAttachmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentCollection. When set to true, AttachmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAttachmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentCollection
		{
			get	{ return _alwaysFetchAttachmentCollection; }
			set	{ _alwaysFetchAttachmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentCollection already has been fetched. Setting this property to false when AttachmentCollection has been fetched
		/// will clear the AttachmentCollection collection well. Setting this property to true while AttachmentCollection hasn't been fetched disables lazy loading for AttachmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentCollection
		{
			get { return _alreadyFetchedAttachmentCollection;}
			set 
			{
				if(_alreadyFetchedAttachmentCollection && !value && (_attachmentCollection != null))
				{
					_attachmentCollection.Clear();
				}
				_alreadyFetchedAttachmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductGenericalterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GenericproductGenericalterationCollection
		{
			get	{ return GetMultiGenericproductGenericalterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductGenericalterationCollection. When set to true, GenericproductGenericalterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductGenericalterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductGenericalterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductGenericalterationCollection
		{
			get	{ return _alwaysFetchGenericproductGenericalterationCollection; }
			set	{ _alwaysFetchGenericproductGenericalterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductGenericalterationCollection already has been fetched. Setting this property to false when GenericproductGenericalterationCollection has been fetched
		/// will clear the GenericproductGenericalterationCollection collection well. Setting this property to true while GenericproductGenericalterationCollection hasn't been fetched disables lazy loading for GenericproductGenericalterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductGenericalterationCollection
		{
			get { return _alreadyFetchedGenericproductGenericalterationCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductGenericalterationCollection && !value && (_genericproductGenericalterationCollection != null))
				{
					_genericproductGenericalterationCollection.Clear();
				}
				_alreadyFetchedGenericproductGenericalterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GenericproductLanguageCollection
		{
			get	{ return GetMultiGenericproductLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductLanguageCollection. When set to true, GenericproductLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductLanguageCollection
		{
			get	{ return _alwaysFetchGenericproductLanguageCollection; }
			set	{ _alwaysFetchGenericproductLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductLanguageCollection already has been fetched. Setting this property to false when GenericproductLanguageCollection has been fetched
		/// will clear the GenericproductLanguageCollection collection well. Setting this property to true while GenericproductLanguageCollection hasn't been fetched disables lazy loading for GenericproductLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductLanguageCollection
		{
			get { return _alreadyFetchedGenericproductLanguageCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductLanguageCollection && !value && (_genericproductLanguageCollection != null))
				{
					_genericproductLanguageCollection.Clear();
				}
				_alreadyFetchedGenericproductLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAdvertisement
		{
			get { return GetMultiCategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAdvertisement. When set to true, CategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchCategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when CategoryCollectionViaAdvertisement has been fetched
		/// will clear the CategoryCollectionViaAdvertisement collection well. Setting this property to true while CategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for CategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAdvertisement && !value && (_categoryCollectionViaAdvertisement != null))
				{
					_categoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAdvertisement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAdvertisement. When set to true, DeliverypointgroupCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAdvertisement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAdvertisement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAdvertisement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAdvertisement collection well. Setting this property to true while DeliverypointgroupCollectionViaAdvertisement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAdvertisement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement && !value && (_deliverypointgroupCollectionViaAdvertisement != null))
				{
					_deliverypointgroupCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement. When set to true, EntertainmentCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement && !value && (_entertainmentCollectionViaAdvertisement != null))
				{
					_entertainmentCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAdvertisement. When set to true, EntertainmentcategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement && !value && (_entertainmentcategoryCollectionViaAdvertisement != null))
				{
					_entertainmentcategoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationCollectionViaGenericproductGenericalteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationCollection GenericalterationCollectionViaGenericproductGenericalteration
		{
			get { return GetMultiGenericalterationCollectionViaGenericproductGenericalteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationCollectionViaGenericproductGenericalteration. When set to true, GenericalterationCollectionViaGenericproductGenericalteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationCollectionViaGenericproductGenericalteration is accessed. You can always execute a forced fetch by calling GetMultiGenericalterationCollectionViaGenericproductGenericalteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationCollectionViaGenericproductGenericalteration
		{
			get	{ return _alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration; }
			set	{ _alwaysFetchGenericalterationCollectionViaGenericproductGenericalteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationCollectionViaGenericproductGenericalteration already has been fetched. Setting this property to false when GenericalterationCollectionViaGenericproductGenericalteration has been fetched
		/// will clear the GenericalterationCollectionViaGenericproductGenericalteration collection well. Setting this property to true while GenericalterationCollectionViaGenericproductGenericalteration hasn't been fetched disables lazy loading for GenericalterationCollectionViaGenericproductGenericalteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration
		{
			get { return _alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration;}
			set 
			{
				if(_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration && !value && (_genericalterationCollectionViaGenericproductGenericalteration != null))
				{
					_genericalterationCollectionViaGenericproductGenericalteration.Clear();
				}
				_alreadyFetchedGenericalterationCollectionViaGenericproductGenericalteration = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaProduct
		{
			get { return GetMultiRouteCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaProduct. When set to true, RouteCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaProduct
		{
			get	{ return _alwaysFetchRouteCollectionViaProduct; }
			set	{ _alwaysFetchRouteCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaProduct already has been fetched. Setting this property to false when RouteCollectionViaProduct has been fetched
		/// will clear the RouteCollectionViaProduct collection well. Setting this property to true while RouteCollectionViaProduct hasn't been fetched disables lazy loading for RouteCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaProduct
		{
			get { return _alreadyFetchedRouteCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaProduct && !value && (_routeCollectionViaProduct != null))
				{
					_routeCollectionViaProduct.Clear();
				}
				_alreadyFetchedRouteCollectionViaProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVattariffCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VattariffCollection VattariffCollectionViaProduct
		{
			get { return GetMultiVattariffCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VattariffCollectionViaProduct. When set to true, VattariffCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VattariffCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiVattariffCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVattariffCollectionViaProduct
		{
			get	{ return _alwaysFetchVattariffCollectionViaProduct; }
			set	{ _alwaysFetchVattariffCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VattariffCollectionViaProduct already has been fetched. Setting this property to false when VattariffCollectionViaProduct has been fetched
		/// will clear the VattariffCollectionViaProduct collection well. Setting this property to true while VattariffCollectionViaProduct hasn't been fetched disables lazy loading for VattariffCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVattariffCollectionViaProduct
		{
			get { return _alreadyFetchedVattariffCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedVattariffCollectionViaProduct && !value && (_vattariffCollectionViaProduct != null))
				{
					_vattariffCollectionViaProduct.Clear();
				}
				_alreadyFetchedVattariffCollectionViaProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericproductCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericcategoryEntity GenericcategoryEntity
		{
			get	{ return GetSingleGenericcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericproductCollection", "GenericcategoryEntity", _genericcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryEntity. When set to true, GenericcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryEntity
		{
			get	{ return _alwaysFetchGenericcategoryEntity; }
			set	{ _alwaysFetchGenericcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryEntity already has been fetched. Setting this property to false when GenericcategoryEntity has been fetched
		/// will set GenericcategoryEntity to null as well. Setting this property to true while GenericcategoryEntity hasn't been fetched disables lazy loading for GenericcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryEntity
		{
			get { return _alreadyFetchedGenericcategoryEntity;}
			set 
			{
				if(_alreadyFetchedGenericcategoryEntity && !value)
				{
					this.GenericcategoryEntity = null;
				}
				_alreadyFetchedGenericcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericcategoryEntity is not found
		/// in the database. When set to true, GenericcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _genericcategoryEntityReturnsNewIfNotFound; }
			set { _genericcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SupplierEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSupplierEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SupplierEntity SupplierEntity
		{
			get	{ return GetSingleSupplierEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSupplierEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericproductCollection", "SupplierEntity", _supplierEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SupplierEntity. When set to true, SupplierEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupplierEntity is accessed. You can always execute a forced fetch by calling GetSingleSupplierEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupplierEntity
		{
			get	{ return _alwaysFetchSupplierEntity; }
			set	{ _alwaysFetchSupplierEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupplierEntity already has been fetched. Setting this property to false when SupplierEntity has been fetched
		/// will set SupplierEntity to null as well. Setting this property to true while SupplierEntity hasn't been fetched disables lazy loading for SupplierEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupplierEntity
		{
			get { return _alreadyFetchedSupplierEntity;}
			set 
			{
				if(_alreadyFetchedSupplierEntity && !value)
				{
					this.SupplierEntity = null;
				}
				_alreadyFetchedSupplierEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SupplierEntity is not found
		/// in the database. When set to true, SupplierEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SupplierEntityReturnsNewIfNotFound
		{
			get	{ return _supplierEntityReturnsNewIfNotFound; }
			set { _supplierEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VattariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVattariffEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual VattariffEntity VattariffEntity
		{
			get	{ return GetSingleVattariffEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVattariffEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericproductCollection", "VattariffEntity", _vattariffEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VattariffEntity. When set to true, VattariffEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VattariffEntity is accessed. You can always execute a forced fetch by calling GetSingleVattariffEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVattariffEntity
		{
			get	{ return _alwaysFetchVattariffEntity; }
			set	{ _alwaysFetchVattariffEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VattariffEntity already has been fetched. Setting this property to false when VattariffEntity has been fetched
		/// will set VattariffEntity to null as well. Setting this property to true while VattariffEntity hasn't been fetched disables lazy loading for VattariffEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVattariffEntity
		{
			get { return _alreadyFetchedVattariffEntity;}
			set 
			{
				if(_alreadyFetchedVattariffEntity && !value)
				{
					this.VattariffEntity = null;
				}
				_alreadyFetchedVattariffEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VattariffEntity is not found
		/// in the database. When set to true, VattariffEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool VattariffEntityReturnsNewIfNotFound
		{
			get	{ return _vattariffEntityReturnsNewIfNotFound; }
			set { _vattariffEntityReturnsNewIfNotFound = value; }	
		}

 
		/// <summary> Gets the value of the related field this.BrandEntity.Name.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String BrandName
		{
			get
			{
				BrandEntity relatedEntity = this.BrandEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Name;
			}

		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GenericproductEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
