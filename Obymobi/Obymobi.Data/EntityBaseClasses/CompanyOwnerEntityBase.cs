﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'CompanyOwner'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CompanyOwnerEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CompanyOwnerEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.CountryCollection _countryCollectionViaCompany;
		private bool	_alwaysFetchCountryCollectionViaCompany, _alreadyFetchedCountryCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CurrencyCollection _currencyCollectionViaCompany;
		private bool	_alwaysFetchCurrencyCollectionViaCompany, _alreadyFetchedCurrencyCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCompany;
		private bool	_alwaysFetchRouteCollectionViaCompany, _alreadyFetchedRouteCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaCompany;
		private bool	_alwaysFetchSupportpoolCollectionViaCompany, _alreadyFetchedSupportpoolCollectionViaCompany;
		private UserEntity _userEntity;
		private bool	_alwaysFetchUserEntity, _alreadyFetchedUserEntity, _userEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserEntity</summary>
			public static readonly string UserEntity = "UserEntity";
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name CountryCollectionViaCompany</summary>
			public static readonly string CountryCollectionViaCompany = "CountryCollectionViaCompany";
			/// <summary>Member name CurrencyCollectionViaCompany</summary>
			public static readonly string CurrencyCollectionViaCompany = "CurrencyCollectionViaCompany";
			/// <summary>Member name RouteCollectionViaCompany</summary>
			public static readonly string RouteCollectionViaCompany = "RouteCollectionViaCompany";
			/// <summary>Member name SupportpoolCollectionViaCompany</summary>
			public static readonly string SupportpoolCollectionViaCompany = "SupportpoolCollectionViaCompany";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CompanyOwnerEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CompanyOwnerEntityBase() :base("CompanyOwnerEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		protected CompanyOwnerEntityBase(System.Int32 companyOwnerId):base("CompanyOwnerEntity")
		{
			InitClassFetch(companyOwnerId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CompanyOwnerEntityBase(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse): base("CompanyOwnerEntity")
		{
			InitClassFetch(companyOwnerId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="validator">The custom validator object for this CompanyOwnerEntity</param>
		protected CompanyOwnerEntityBase(System.Int32 companyOwnerId, IValidator validator):base("CompanyOwnerEntity")
		{
			InitClassFetch(companyOwnerId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompanyOwnerEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");
			_countryCollectionViaCompany = (Obymobi.Data.CollectionClasses.CountryCollection)info.GetValue("_countryCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CountryCollection));
			_alwaysFetchCountryCollectionViaCompany = info.GetBoolean("_alwaysFetchCountryCollectionViaCompany");
			_alreadyFetchedCountryCollectionViaCompany = info.GetBoolean("_alreadyFetchedCountryCollectionViaCompany");

			_currencyCollectionViaCompany = (Obymobi.Data.CollectionClasses.CurrencyCollection)info.GetValue("_currencyCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CurrencyCollection));
			_alwaysFetchCurrencyCollectionViaCompany = info.GetBoolean("_alwaysFetchCurrencyCollectionViaCompany");
			_alreadyFetchedCurrencyCollectionViaCompany = info.GetBoolean("_alreadyFetchedCurrencyCollectionViaCompany");

			_routeCollectionViaCompany = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCompany = info.GetBoolean("_alwaysFetchRouteCollectionViaCompany");
			_alreadyFetchedRouteCollectionViaCompany = info.GetBoolean("_alreadyFetchedRouteCollectionViaCompany");

			_supportpoolCollectionViaCompany = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaCompany = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaCompany");
			_alreadyFetchedSupportpoolCollectionViaCompany = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaCompany");
			_userEntity = (UserEntity)info.GetValue("_userEntity", typeof(UserEntity));
			if(_userEntity!=null)
			{
				_userEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userEntityReturnsNewIfNotFound = info.GetBoolean("_userEntityReturnsNewIfNotFound");
			_alwaysFetchUserEntity = info.GetBoolean("_alwaysFetchUserEntity");
			_alreadyFetchedUserEntity = info.GetBoolean("_alreadyFetchedUserEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CompanyOwnerFieldIndex)fieldIndex)
			{
				case CompanyOwnerFieldIndex.UserId:
					DesetupSyncUserEntity(true, false);
					_alreadyFetchedUserEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedCountryCollectionViaCompany = (_countryCollectionViaCompany.Count > 0);
			_alreadyFetchedCurrencyCollectionViaCompany = (_currencyCollectionViaCompany.Count > 0);
			_alreadyFetchedRouteCollectionViaCompany = (_routeCollectionViaCompany.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaCompany = (_supportpoolCollectionViaCompany.Count > 0);
			_alreadyFetchedUserEntity = (_userEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserEntity":
					toReturn.Add(Relations.UserEntityUsingUserId);
					break;
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingCompanyOwnerId);
					break;
				case "CountryCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCompanyOwnerId, "CompanyOwnerEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CountryEntityUsingCountryId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CurrencyCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCompanyOwnerId, "CompanyOwnerEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CurrencyEntityUsingCurrencyId, "Company_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCompanyOwnerId, "CompanyOwnerEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.RouteEntityUsingRouteId, "Company_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCompanyOwnerId, "CompanyOwnerEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Company_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_countryCollectionViaCompany", (!this.MarkedForDeletion?_countryCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCountryCollectionViaCompany", _alwaysFetchCountryCollectionViaCompany);
			info.AddValue("_alreadyFetchedCountryCollectionViaCompany", _alreadyFetchedCountryCollectionViaCompany);
			info.AddValue("_currencyCollectionViaCompany", (!this.MarkedForDeletion?_currencyCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCurrencyCollectionViaCompany", _alwaysFetchCurrencyCollectionViaCompany);
			info.AddValue("_alreadyFetchedCurrencyCollectionViaCompany", _alreadyFetchedCurrencyCollectionViaCompany);
			info.AddValue("_routeCollectionViaCompany", (!this.MarkedForDeletion?_routeCollectionViaCompany:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCompany", _alwaysFetchRouteCollectionViaCompany);
			info.AddValue("_alreadyFetchedRouteCollectionViaCompany", _alreadyFetchedRouteCollectionViaCompany);
			info.AddValue("_supportpoolCollectionViaCompany", (!this.MarkedForDeletion?_supportpoolCollectionViaCompany:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaCompany", _alwaysFetchSupportpoolCollectionViaCompany);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaCompany", _alreadyFetchedSupportpoolCollectionViaCompany);
			info.AddValue("_userEntity", (!this.MarkedForDeletion?_userEntity:null));
			info.AddValue("_userEntityReturnsNewIfNotFound", _userEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserEntity", _alwaysFetchUserEntity);
			info.AddValue("_alreadyFetchedUserEntity", _alreadyFetchedUserEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserEntity":
					_alreadyFetchedUserEntity = true;
					this.UserEntity = (UserEntity)entity;
					break;
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "CountryCollectionViaCompany":
					_alreadyFetchedCountryCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CountryCollectionViaCompany.Add((CountryEntity)entity);
					}
					break;
				case "CurrencyCollectionViaCompany":
					_alreadyFetchedCurrencyCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CurrencyCollectionViaCompany.Add((CurrencyEntity)entity);
					}
					break;
				case "RouteCollectionViaCompany":
					_alreadyFetchedRouteCollectionViaCompany = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCompany.Add((RouteEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaCompany":
					_alreadyFetchedSupportpoolCollectionViaCompany = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaCompany.Add((SupportpoolEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserEntity":
					SetupSyncUserEntity(relatedEntity);
					break;
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserEntity":
					DesetupSyncUserEntity(false, true);
					break;
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userEntity!=null)
			{
				toReturn.Add(_userEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username)
		{
			return FetchUsingUCUsername( username, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCUsername( username, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCUsername( username, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CompanyOwnerDAO)CreateDAOInstance()).FetchCompanyOwnerUsingUCUsername(this, this.Transaction, username, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 companyOwnerId)
		{
			return FetchUsingPK(companyOwnerId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(companyOwnerId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(companyOwnerId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(companyOwnerId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CompanyOwnerId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CompanyOwnerRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCountryCollectionViaCompany(forceFetch, _countryCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCountryCollectionViaCompany || forceFetch || _alwaysFetchCountryCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_countryCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CompanyOwnerFields.CompanyOwnerId, ComparisonOperator.Equal, this.CompanyOwnerId, "CompanyOwnerEntity__"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_countryCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_countryCollectionViaCompany.GetMulti(filter, GetRelationsForField("CountryCollectionViaCompany"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCountryCollectionViaCompany = true;
			}
			return _countryCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CountryCollectionViaCompany'. These settings will be taken into account
		/// when the property CountryCollectionViaCompany is requested or GetMultiCountryCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCountryCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_countryCollectionViaCompany.SortClauses=sortClauses;
			_countryCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CurrencyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCurrencyCollectionViaCompany(forceFetch, _currencyCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCurrencyCollectionViaCompany || forceFetch || _alwaysFetchCurrencyCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_currencyCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CompanyOwnerFields.CompanyOwnerId, ComparisonOperator.Equal, this.CompanyOwnerId, "CompanyOwnerEntity__"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_currencyCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_currencyCollectionViaCompany.GetMulti(filter, GetRelationsForField("CurrencyCollectionViaCompany"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCurrencyCollectionViaCompany = true;
			}
			return _currencyCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CurrencyCollectionViaCompany'. These settings will be taken into account
		/// when the property CurrencyCollectionViaCompany is requested or GetMultiCurrencyCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCurrencyCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_currencyCollectionViaCompany.SortClauses=sortClauses;
			_currencyCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCompany(forceFetch, _routeCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCompany || forceFetch || _alwaysFetchRouteCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CompanyOwnerFields.CompanyOwnerId, ComparisonOperator.Equal, this.CompanyOwnerId, "CompanyOwnerEntity__"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCompany.GetMulti(filter, GetRelationsForField("RouteCollectionViaCompany"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCompany = true;
			}
			return _routeCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCompany'. These settings will be taken into account
		/// when the property RouteCollectionViaCompany is requested or GetMultiRouteCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCompany.SortClauses=sortClauses;
			_routeCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaCompany(forceFetch, _supportpoolCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaCompany || forceFetch || _alwaysFetchSupportpoolCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CompanyOwnerFields.CompanyOwnerId, ComparisonOperator.Equal, this.CompanyOwnerId, "CompanyOwnerEntity__"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaCompany.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaCompany"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaCompany = true;
			}
			return _supportpoolCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaCompany'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaCompany is requested or GetMultiSupportpoolCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaCompany.SortClauses=sortClauses;
			_supportpoolCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public UserEntity GetSingleUserEntity()
		{
			return GetSingleUserEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public virtual UserEntity GetSingleUserEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserEntity || forceFetch || _alwaysFetchUserEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserEntityUsingUserId);
				UserEntity newEntity = new UserEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserEntity = newEntity;
				_alreadyFetchedUserEntity = fetchResult;
			}
			return _userEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserEntity", _userEntity);
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("CountryCollectionViaCompany", _countryCollectionViaCompany);
			toReturn.Add("CurrencyCollectionViaCompany", _currencyCollectionViaCompany);
			toReturn.Add("RouteCollectionViaCompany", _routeCollectionViaCompany);
			toReturn.Add("SupportpoolCollectionViaCompany", _supportpoolCollectionViaCompany);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="validator">The validator object for this CompanyOwnerEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 companyOwnerId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(companyOwnerId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "CompanyOwnerEntity");
			_countryCollectionViaCompany = new Obymobi.Data.CollectionClasses.CountryCollection();
			_currencyCollectionViaCompany = new Obymobi.Data.CollectionClasses.CurrencyCollection();
			_routeCollectionViaCompany = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supportpoolCollectionViaCompany = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_userEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyOwnerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Username", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPasswordEncrypted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userEntity, new PropertyChangedEventHandler( OnUserEntityPropertyChanged ), "UserEntity", Obymobi.Data.RelationClasses.StaticCompanyOwnerRelations.UserEntityUsingUserIdStatic, true, signalRelatedEntity, "CompanyOwnerCollection", resetFKFields, new int[] { (int)CompanyOwnerFieldIndex.UserId } );		
			_userEntity = null;
		}
		
		/// <summary> setups the sync logic for member _userEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserEntity(IEntityCore relatedEntity)
		{
			if(_userEntity!=relatedEntity)
			{		
				DesetupSyncUserEntity(true, true);
				_userEntity = (UserEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userEntity, new PropertyChangedEventHandler( OnUserEntityPropertyChanged ), "UserEntity", Obymobi.Data.RelationClasses.StaticCompanyOwnerRelations.UserEntityUsingUserIdStatic, true, ref _alreadyFetchedUserEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CompanyOwnerFieldIndex.CompanyOwnerId].ForcedCurrentValueWrite(companyOwnerId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCompanyOwnerDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CompanyOwnerEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CompanyOwnerRelations Relations
		{
			get	{ return new CompanyOwnerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCompanyOwnerId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, GetRelationsForField("CountryCollectionViaCompany"), "CountryCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCompanyOwnerId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, GetRelationsForField("CurrencyCollectionViaCompany"), "CurrencyCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCompanyOwnerId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCompany"), "RouteCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCompanyOwnerId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaCompany"), "SupportpoolCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), (IEntityRelation)GetRelationsForField("UserEntity")[0], (int)Obymobi.Data.EntityType.CompanyOwnerEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, null, "UserEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CompanyOwnerId property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."CompanyOwnerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CompanyOwnerId
		{
			get { return (System.Int32)GetValue((int)CompanyOwnerFieldIndex.CompanyOwnerId, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.CompanyOwnerId, value, true); }
		}

		/// <summary> The Username property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."Username"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Username
		{
			get { return (System.String)GetValue((int)CompanyOwnerFieldIndex.Username, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.Username, value, true); }
		}

		/// <summary> The Password property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."Password"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)CompanyOwnerFieldIndex.Password, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.Password, value, true); }
		}

		/// <summary> The UserId property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."UserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UserId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CompanyOwnerFieldIndex.UserId, false); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.UserId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CompanyOwnerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CompanyOwnerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The IsPasswordEncrypted property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."IsPasswordEncrypted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPasswordEncrypted
		{
			get { return (System.Boolean)GetValue((int)CompanyOwnerFieldIndex.IsPasswordEncrypted, true); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.IsPasswordEncrypted, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyOwnerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity CompanyOwner<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CompanyOwner"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CompanyOwnerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CompanyOwnerFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCountryCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection CountryCollectionViaCompany
		{
			get { return GetMultiCountryCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CountryCollectionViaCompany. When set to true, CountryCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCountryCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryCollectionViaCompany
		{
			get	{ return _alwaysFetchCountryCollectionViaCompany; }
			set	{ _alwaysFetchCountryCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryCollectionViaCompany already has been fetched. Setting this property to false when CountryCollectionViaCompany has been fetched
		/// will clear the CountryCollectionViaCompany collection well. Setting this property to true while CountryCollectionViaCompany hasn't been fetched disables lazy loading for CountryCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryCollectionViaCompany
		{
			get { return _alreadyFetchedCountryCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCountryCollectionViaCompany && !value && (_countryCollectionViaCompany != null))
				{
					_countryCollectionViaCompany.Clear();
				}
				_alreadyFetchedCountryCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCurrencyCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CurrencyCollection CurrencyCollectionViaCompany
		{
			get { return GetMultiCurrencyCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyCollectionViaCompany. When set to true, CurrencyCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCurrencyCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyCollectionViaCompany
		{
			get	{ return _alwaysFetchCurrencyCollectionViaCompany; }
			set	{ _alwaysFetchCurrencyCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyCollectionViaCompany already has been fetched. Setting this property to false when CurrencyCollectionViaCompany has been fetched
		/// will clear the CurrencyCollectionViaCompany collection well. Setting this property to true while CurrencyCollectionViaCompany hasn't been fetched disables lazy loading for CurrencyCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyCollectionViaCompany
		{
			get { return _alreadyFetchedCurrencyCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCurrencyCollectionViaCompany && !value && (_currencyCollectionViaCompany != null))
				{
					_currencyCollectionViaCompany.Clear();
				}
				_alreadyFetchedCurrencyCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCompany
		{
			get { return GetMultiRouteCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCompany. When set to true, RouteCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCompany
		{
			get	{ return _alwaysFetchRouteCollectionViaCompany; }
			set	{ _alwaysFetchRouteCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCompany already has been fetched. Setting this property to false when RouteCollectionViaCompany has been fetched
		/// will clear the RouteCollectionViaCompany collection well. Setting this property to true while RouteCollectionViaCompany hasn't been fetched disables lazy loading for RouteCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCompany
		{
			get { return _alreadyFetchedRouteCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCompany && !value && (_routeCollectionViaCompany != null))
				{
					_routeCollectionViaCompany.Clear();
				}
				_alreadyFetchedRouteCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaCompany
		{
			get { return GetMultiSupportpoolCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaCompany. When set to true, SupportpoolCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaCompany
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaCompany; }
			set	{ _alwaysFetchSupportpoolCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaCompany already has been fetched. Setting this property to false when SupportpoolCollectionViaCompany has been fetched
		/// will clear the SupportpoolCollectionViaCompany collection well. Setting this property to true while SupportpoolCollectionViaCompany hasn't been fetched disables lazy loading for SupportpoolCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaCompany
		{
			get { return _alreadyFetchedSupportpoolCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaCompany && !value && (_supportpoolCollectionViaCompany != null))
				{
					_supportpoolCollectionViaCompany.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaCompany = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UserEntity UserEntity
		{
			get	{ return GetSingleUserEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CompanyOwnerCollection", "UserEntity", _userEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserEntity. When set to true, UserEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserEntity is accessed. You can always execute a forced fetch by calling GetSingleUserEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserEntity
		{
			get	{ return _alwaysFetchUserEntity; }
			set	{ _alwaysFetchUserEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserEntity already has been fetched. Setting this property to false when UserEntity has been fetched
		/// will set UserEntity to null as well. Setting this property to true while UserEntity hasn't been fetched disables lazy loading for UserEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserEntity
		{
			get { return _alreadyFetchedUserEntity;}
			set 
			{
				if(_alreadyFetchedUserEntity && !value)
				{
					this.UserEntity = null;
				}
				_alreadyFetchedUserEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserEntity is not found
		/// in the database. When set to true, UserEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UserEntityReturnsNewIfNotFound
		{
			get	{ return _userEntityReturnsNewIfNotFound; }
			set { _userEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CompanyOwnerEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
