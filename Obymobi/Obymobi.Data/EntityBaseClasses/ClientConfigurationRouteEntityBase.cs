﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ClientConfigurationRoute'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ClientConfigurationRouteEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ClientConfigurationRouteEntity"; }
		}
	
		#region Class Member Declarations
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private RouteEntity _routeEntity;
		private bool	_alwaysFetchRouteEntity, _alreadyFetchedRouteEntity, _routeEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name RouteEntity</summary>
			public static readonly string RouteEntity = "RouteEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientConfigurationRouteEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ClientConfigurationRouteEntityBase() :base("ClientConfigurationRouteEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		protected ClientConfigurationRouteEntityBase(System.Int32 clientConfigurationRouteId):base("ClientConfigurationRouteEntity")
		{
			InitClassFetch(clientConfigurationRouteId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ClientConfigurationRouteEntityBase(System.Int32 clientConfigurationRouteId, IPrefetchPath prefetchPathToUse): base("ClientConfigurationRouteEntity")
		{
			InitClassFetch(clientConfigurationRouteId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="validator">The custom validator object for this ClientConfigurationRouteEntity</param>
		protected ClientConfigurationRouteEntityBase(System.Int32 clientConfigurationRouteId, IValidator validator):base("ClientConfigurationRouteEntity")
		{
			InitClassFetch(clientConfigurationRouteId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientConfigurationRouteEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_routeEntity = (RouteEntity)info.GetValue("_routeEntity", typeof(RouteEntity));
			if(_routeEntity!=null)
			{
				_routeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeEntityReturnsNewIfNotFound = info.GetBoolean("_routeEntityReturnsNewIfNotFound");
			_alwaysFetchRouteEntity = info.GetBoolean("_alwaysFetchRouteEntity");
			_alreadyFetchedRouteEntity = info.GetBoolean("_alreadyFetchedRouteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClientConfigurationRouteFieldIndex)fieldIndex)
			{
				case ClientConfigurationRouteFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case ClientConfigurationRouteFieldIndex.RouteId:
					DesetupSyncRouteEntity(true, false);
					_alreadyFetchedRouteEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedRouteEntity = (_routeEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "RouteEntity":
					toReturn.Add(Relations.RouteEntityUsingRouteId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_routeEntity", (!this.MarkedForDeletion?_routeEntity:null));
			info.AddValue("_routeEntityReturnsNewIfNotFound", _routeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteEntity", _alwaysFetchRouteEntity);
			info.AddValue("_alreadyFetchedRouteEntity", _alreadyFetchedRouteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "RouteEntity":
					_alreadyFetchedRouteEntity = true;
					this.RouteEntity = (RouteEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "RouteEntity":
					SetupSyncRouteEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "RouteEntity":
					DesetupSyncRouteEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_routeEntity!=null)
			{
				toReturn.Add(_routeEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationRouteId)
		{
			return FetchUsingPK(clientConfigurationRouteId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationRouteId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientConfigurationRouteId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationRouteId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientConfigurationRouteId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationRouteId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientConfigurationRouteId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientConfigurationRouteId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientConfigurationRouteRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleRouteEntity()
		{
			return GetSingleRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteEntity || forceFetch || _alwaysFetchRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteId);
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteEntity = newEntity;
				_alreadyFetchedRouteEntity = fetchResult;
			}
			return _routeEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("RouteEntity", _routeEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="validator">The validator object for this ClientConfigurationRouteEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 clientConfigurationRouteId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientConfigurationRouteId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_routeEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRouteRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "ClientConfigurationRouteCollection", resetFKFields, new int[] { (int)ClientConfigurationRouteFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRouteRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRouteRelations.RouteEntityUsingRouteIdStatic, true, signalRelatedEntity, "ClientConfigurationRouteCollection", resetFKFields, new int[] { (int)ClientConfigurationRouteFieldIndex.RouteId } );		
			_routeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteEntity(IEntityCore relatedEntity)
		{
			if(_routeEntity!=relatedEntity)
			{		
				DesetupSyncRouteEntity(true, true);
				_routeEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRouteRelations.RouteEntityUsingRouteIdStatic, true, ref _alreadyFetchedRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientConfigurationRouteId">PK value for ClientConfigurationRoute which data should be fetched into this ClientConfigurationRoute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 clientConfigurationRouteId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId].ForcedCurrentValueWrite(clientConfigurationRouteId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientConfigurationRouteDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientConfigurationRouteEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientConfigurationRouteRelations Relations
		{
			get	{ return new ClientConfigurationRouteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationRouteEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RouteEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationRouteEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "RouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientConfigurationRouteId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ClientConfigurationRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientConfigurationRouteId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationRouteId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ClientConfigurationId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The RouteId property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."RouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.RouteId, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.RouteId, value, true); }
		}

		/// <summary> The Type property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationRouteFieldIndex.Type, true); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.Type, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationRouteFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationRouteFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationRouteFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ClientConfigurationRoute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfigurationRoute"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationRouteFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ClientConfigurationRouteFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationRouteCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity RouteEntity
		{
			get	{ return GetSingleRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationRouteCollection", "RouteEntity", _routeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteEntity. When set to true, RouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteEntity is accessed. You can always execute a forced fetch by calling GetSingleRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteEntity
		{
			get	{ return _alwaysFetchRouteEntity; }
			set	{ _alwaysFetchRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteEntity already has been fetched. Setting this property to false when RouteEntity has been fetched
		/// will set RouteEntity to null as well. Setting this property to true while RouteEntity hasn't been fetched disables lazy loading for RouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteEntity
		{
			get { return _alreadyFetchedRouteEntity;}
			set 
			{
				if(_alreadyFetchedRouteEntity && !value)
				{
					this.RouteEntity = null;
				}
				_alreadyFetchedRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteEntity is not found
		/// in the database. When set to true, RouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RouteEntityReturnsNewIfNotFound
		{
			get	{ return _routeEntityReturnsNewIfNotFound; }
			set { _routeEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ClientConfigurationRouteEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
