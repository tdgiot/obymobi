﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ReportProcessingTaskFile'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReportProcessingTaskFileEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReportProcessingTaskFileEntity"; }
		}
	
		#region Class Member Declarations
		private ReportProcessingTaskEntity _reportProcessingTaskEntity;
		private bool	_alwaysFetchReportProcessingTaskEntity, _alreadyFetchedReportProcessingTaskEntity, _reportProcessingTaskEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ReportProcessingTaskEntity</summary>
			public static readonly string ReportProcessingTaskEntity = "ReportProcessingTaskEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportProcessingTaskFileEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReportProcessingTaskFileEntityBase() :base("ReportProcessingTaskFileEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		protected ReportProcessingTaskFileEntityBase(System.Int32 reportProcessingTaskFileId):base("ReportProcessingTaskFileEntity")
		{
			InitClassFetch(reportProcessingTaskFileId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReportProcessingTaskFileEntityBase(System.Int32 reportProcessingTaskFileId, IPrefetchPath prefetchPathToUse): base("ReportProcessingTaskFileEntity")
		{
			InitClassFetch(reportProcessingTaskFileId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="validator">The custom validator object for this ReportProcessingTaskFileEntity</param>
		protected ReportProcessingTaskFileEntityBase(System.Int32 reportProcessingTaskFileId, IValidator validator):base("ReportProcessingTaskFileEntity")
		{
			InitClassFetch(reportProcessingTaskFileId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportProcessingTaskFileEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_reportProcessingTaskEntity = (ReportProcessingTaskEntity)info.GetValue("_reportProcessingTaskEntity", typeof(ReportProcessingTaskEntity));
			if(_reportProcessingTaskEntity!=null)
			{
				_reportProcessingTaskEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportProcessingTaskEntityReturnsNewIfNotFound = info.GetBoolean("_reportProcessingTaskEntityReturnsNewIfNotFound");
			_alwaysFetchReportProcessingTaskEntity = info.GetBoolean("_alwaysFetchReportProcessingTaskEntity");
			_alreadyFetchedReportProcessingTaskEntity = info.GetBoolean("_alreadyFetchedReportProcessingTaskEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportProcessingTaskFileFieldIndex)fieldIndex)
			{
				case ReportProcessingTaskFileFieldIndex.ReportProcessingTaskId:
					DesetupSyncReportProcessingTaskEntity(true, false);
					_alreadyFetchedReportProcessingTaskEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReportProcessingTaskEntity = (_reportProcessingTaskEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ReportProcessingTaskEntity":
					toReturn.Add(Relations.ReportProcessingTaskEntityUsingReportProcessingTaskId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			info.AddValue("_reportProcessingTaskEntity", (!this.MarkedForDeletion?_reportProcessingTaskEntity:null));
			info.AddValue("_reportProcessingTaskEntityReturnsNewIfNotFound", _reportProcessingTaskEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportProcessingTaskEntity", _alwaysFetchReportProcessingTaskEntity);
			info.AddValue("_alreadyFetchedReportProcessingTaskEntity", _alreadyFetchedReportProcessingTaskEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ReportProcessingTaskEntity":
					_alreadyFetchedReportProcessingTaskEntity = true;
					this.ReportProcessingTaskEntity = (ReportProcessingTaskEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ReportProcessingTaskEntity":
					SetupSyncReportProcessingTaskEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ReportProcessingTaskEntity":
					DesetupSyncReportProcessingTaskEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_reportProcessingTaskEntity!=null)
			{
				toReturn.Add(_reportProcessingTaskEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="reportProcessingTaskId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCReportProcessingTaskId(System.Int32 reportProcessingTaskId)
		{
			return FetchUsingUCReportProcessingTaskId( reportProcessingTaskId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="reportProcessingTaskId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCReportProcessingTaskId(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCReportProcessingTaskId( reportProcessingTaskId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="reportProcessingTaskId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCReportProcessingTaskId(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCReportProcessingTaskId( reportProcessingTaskId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="reportProcessingTaskId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCReportProcessingTaskId(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ReportProcessingTaskFileDAO)CreateDAOInstance()).FetchReportProcessingTaskFileUsingUCReportProcessingTaskId(this, this.Transaction, reportProcessingTaskId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskFileId)
		{
			return FetchUsingPK(reportProcessingTaskFileId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskFileId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportProcessingTaskFileId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskFileId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportProcessingTaskFileId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportProcessingTaskFileId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportProcessingTaskFileId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportProcessingTaskFileRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ReportProcessingTaskEntity' which is related to this entity.</returns>
		public ReportProcessingTaskEntity GetSingleReportProcessingTaskEntity()
		{
			return GetSingleReportProcessingTaskEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportProcessingTaskEntity' which is related to this entity.</returns>
		public virtual ReportProcessingTaskEntity GetSingleReportProcessingTaskEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportProcessingTaskEntity || forceFetch || _alwaysFetchReportProcessingTaskEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportProcessingTaskEntityUsingReportProcessingTaskId);
				ReportProcessingTaskEntity newEntity = new ReportProcessingTaskEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportProcessingTaskId);
				}
				if(fetchResult)
				{
					newEntity = (ReportProcessingTaskEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportProcessingTaskEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportProcessingTaskEntity = newEntity;
				_alreadyFetchedReportProcessingTaskEntity = fetchResult;
			}
			return _reportProcessingTaskEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ReportProcessingTaskEntity", _reportProcessingTaskEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="validator">The validator object for this ReportProcessingTaskFileEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 reportProcessingTaskFileId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportProcessingTaskFileId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_reportProcessingTaskEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("File", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _reportProcessingTaskEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportProcessingTaskEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportProcessingTaskEntity, new PropertyChangedEventHandler( OnReportProcessingTaskEntityPropertyChanged ), "ReportProcessingTaskEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskFileRelations.ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic, true, signalRelatedEntity, "ReportProcessingTaskFileEntity", resetFKFields, new int[] { (int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskId } );
			_reportProcessingTaskEntity = null;
		}
	
		/// <summary> setups the sync logic for member _reportProcessingTaskEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportProcessingTaskEntity(IEntityCore relatedEntity)
		{
			if(_reportProcessingTaskEntity!=relatedEntity)
			{
				DesetupSyncReportProcessingTaskEntity(true, true);
				_reportProcessingTaskEntity = (ReportProcessingTaskEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportProcessingTaskEntity, new PropertyChangedEventHandler( OnReportProcessingTaskEntityPropertyChanged ), "ReportProcessingTaskEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskFileRelations.ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic, true, ref _alreadyFetchedReportProcessingTaskEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportProcessingTaskEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportProcessingTaskFileId">PK value for ReportProcessingTaskFile which data should be fetched into this ReportProcessingTaskFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 reportProcessingTaskFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskFileId].ForcedCurrentValueWrite(reportProcessingTaskFileId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportProcessingTaskFileDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportProcessingTaskFileEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportProcessingTaskFileRelations Relations
		{
			get	{ return new ReportProcessingTaskFileRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTask'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskFileEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, 0, null, null, null, "ReportProcessingTaskEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReportProcessingTaskFileId property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."ReportProcessingTaskFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReportProcessingTaskFileId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskFileId, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskFileId, value, true); }
		}

		/// <summary> The ReportProcessingTaskId property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."ReportProcessingTaskId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReportProcessingTaskId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskId, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.ReportProcessingTaskId, value, true); }
		}

		/// <summary> The File property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."File"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarBinary, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] File
		{
			get { return (System.Byte[])GetValue((int)ReportProcessingTaskFileFieldIndex.File, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.File, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFileFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFileFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFileFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFileFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ReportProcessingTaskFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskFile"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFileFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFileFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ReportProcessingTaskEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportProcessingTaskEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReportProcessingTaskEntity ReportProcessingTaskEntity
		{
			get	{ return GetSingleReportProcessingTaskEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncReportProcessingTaskEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_reportProcessingTaskEntity !=null);
						DesetupSyncReportProcessingTaskEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ReportProcessingTaskEntity");
						}
					}
					else
					{
						if(_reportProcessingTaskEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ReportProcessingTaskFileEntity");
							SetupSyncReportProcessingTaskEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskEntity. When set to true, ReportProcessingTaskEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskEntity is accessed. You can always execute a forced fetch by calling GetSingleReportProcessingTaskEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskEntity
		{
			get	{ return _alwaysFetchReportProcessingTaskEntity; }
			set	{ _alwaysFetchReportProcessingTaskEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskEntity already has been fetched. Setting this property to false when ReportProcessingTaskEntity has been fetched
		/// will set ReportProcessingTaskEntity to null as well. Setting this property to true while ReportProcessingTaskEntity hasn't been fetched disables lazy loading for ReportProcessingTaskEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskEntity
		{
			get { return _alreadyFetchedReportProcessingTaskEntity;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskEntity && !value)
				{
					this.ReportProcessingTaskEntity = null;
				}
				_alreadyFetchedReportProcessingTaskEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportProcessingTaskEntity is not found
		/// in the database. When set to true, ReportProcessingTaskEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReportProcessingTaskEntityReturnsNewIfNotFound
		{
			get	{ return _reportProcessingTaskEntityReturnsNewIfNotFound; }
			set	{ _reportProcessingTaskEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReportProcessingTaskFileEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
