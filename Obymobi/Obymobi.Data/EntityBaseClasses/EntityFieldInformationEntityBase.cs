﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'EntityFieldInformation'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntityFieldInformationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "EntityFieldInformationEntity"; }
		}
	
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntityFieldInformationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntityFieldInformationEntityBase() :base("EntityFieldInformationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		protected EntityFieldInformationEntityBase(System.Int32 entityFieldInformationId):base("EntityFieldInformationEntity")
		{
			InitClassFetch(entityFieldInformationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntityFieldInformationEntityBase(System.Int32 entityFieldInformationId, IPrefetchPath prefetchPathToUse): base("EntityFieldInformationEntity")
		{
			InitClassFetch(entityFieldInformationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="validator">The custom validator object for this EntityFieldInformationEntity</param>
		protected EntityFieldInformationEntityBase(System.Int32 entityFieldInformationId, IValidator validator):base("EntityFieldInformationEntity")
		{
			InitClassFetch(entityFieldInformationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntityFieldInformationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityFieldInformationId)
		{
			return FetchUsingPK(entityFieldInformationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityFieldInformationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entityFieldInformationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityFieldInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entityFieldInformationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityFieldInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entityFieldInformationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntityFieldInformationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntityFieldInformationRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="validator">The validator object for this EntityFieldInformationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entityFieldInformationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entityFieldInformationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityFieldInformationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowOnDefaultGridView", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultDisplayPosition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowShowOnGridView", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HelpText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayFormatString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultSortPosition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultSortOperator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityFieldType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateWmsCacheDateOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateCompanyDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateMenuDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdatePosIntegrationInformationVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForLocalUseWhileSyncingIsUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateDeliverypointDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateSurveyDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateAnnouncementDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateEntertainmentDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExcludeForUpdateAdvertisementDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entityFieldInformationId">PK value for EntityFieldInformation which data should be fetched into this EntityFieldInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entityFieldInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntityFieldInformationFieldIndex.EntityFieldInformationId].ForcedCurrentValueWrite(entityFieldInformationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntityFieldInformationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntityFieldInformationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntityFieldInformationRelations Relations
		{
			get	{ return new EntityFieldInformationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntityFieldInformationId property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."EntityFieldInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntityFieldInformationId
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.EntityFieldInformationId, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.EntityFieldInformationId, value, true); }
		}

		/// <summary> The EntityName property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."EntityName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EntityName
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.EntityName, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.EntityName, value, true); }
		}

		/// <summary> The FieldName property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."FieldName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.FieldName, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.FieldName, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The ShowOnDefaultGridView property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ShowOnDefaultGridView"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowOnDefaultGridView
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ShowOnDefaultGridView, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ShowOnDefaultGridView, value, true); }
		}

		/// <summary> The DefaultDisplayPosition property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."DefaultDisplayPosition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DefaultDisplayPosition
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.DefaultDisplayPosition, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.DefaultDisplayPosition, value, true); }
		}

		/// <summary> The AllowShowOnGridView property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."AllowShowOnGridView"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowShowOnGridView
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.AllowShowOnGridView, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.AllowShowOnGridView, value, true); }
		}

		/// <summary> The HelpText property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."HelpText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HelpText
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.HelpText, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.HelpText, value, true); }
		}

		/// <summary> The DisplayFormatString property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."DisplayFormatString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayFormatString
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.DisplayFormatString, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.DisplayFormatString, value, true); }
		}

		/// <summary> The DefaultSortPosition property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."DefaultSortPosition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DefaultSortPosition
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.DefaultSortPosition, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.DefaultSortPosition, value, true); }
		}

		/// <summary> The DefaultSortOperator property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."DefaultSortOperator"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DefaultSortOperator
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.DefaultSortOperator, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.DefaultSortOperator, value, true); }
		}

		/// <summary> The Type property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Type
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.Type, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.Type, value, true); }
		}

		/// <summary> The IsRequired property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."IsRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsRequired
		{
			get { return (Nullable<System.Boolean>)GetValue((int)EntityFieldInformationFieldIndex.IsRequired, false); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.IsRequired, value, true); }
		}

		/// <summary> The EntityFieldType property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."EntityFieldType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EntityFieldType
		{
			get { return (System.String)GetValue((int)EntityFieldInformationFieldIndex.EntityFieldType, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.EntityFieldType, value, true); }
		}

		/// <summary> The ExcludeForUpdateWmsCacheDateOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateWmsCacheDateOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateWmsCacheDateOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateWmsCacheDateOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateWmsCacheDateOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateCompanyDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateCompanyDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateCompanyDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateCompanyDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateCompanyDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateMenuDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateMenuDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateMenuDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateMenuDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateMenuDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdatePosIntegrationInformationVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdatePosIntegrationInformationVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdatePosIntegrationInformationVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdatePosIntegrationInformationVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdatePosIntegrationInformationVersionOnChange, value, true); }
		}

		/// <summary> The ForLocalUseWhileSyncingIsUpdated property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ForLocalUseWhileSyncingIsUpdated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ForLocalUseWhileSyncingIsUpdated
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, value, true); }
		}

		/// <summary> The Archived property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.Archived, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.Archived, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)EntityFieldInformationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.Deleted, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.Deleted, value, true); }
		}

		/// <summary> The ExcludeForUpdateDeliverypointDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateDeliverypointDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateDeliverypointDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateDeliverypointDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateDeliverypointDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateSurveyDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateSurveyDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateSurveyDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateSurveyDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateSurveyDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateAnnouncementDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateAnnouncementDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateAnnouncementDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateAnnouncementDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateAnnouncementDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateEntertainmentDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateEntertainmentDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateEntertainmentDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateEntertainmentDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateEntertainmentDataVersionOnChange, value, true); }
		}

		/// <summary> The ExcludeForUpdateAdvertisementDataVersionOnChange property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."ExcludeForUpdateAdvertisementDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExcludeForUpdateAdvertisementDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateAdvertisementDataVersionOnChange, true); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.ExcludeForUpdateAdvertisementDataVersionOnChange, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityFieldInformationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity EntityFieldInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityFieldInformation"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityFieldInformationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntityFieldInformationFieldIndex.UpdatedUTC, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.EntityFieldInformationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
