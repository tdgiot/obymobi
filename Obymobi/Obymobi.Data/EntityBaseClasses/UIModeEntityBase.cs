﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UIMode'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIModeEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIModeEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientConfigurationCollection	_clientConfigurationCollection;
		private bool	_alwaysFetchClientConfigurationCollection, _alreadyFetchedClientConfigurationCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection_;
		private bool	_alwaysFetchDeliverypointgroupCollection_, _alreadyFetchedDeliverypointgroupCollection_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection__;
		private bool	_alwaysFetchDeliverypointgroupCollection__, _alreadyFetchedDeliverypointgroupCollection__;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection;
		private bool	_alwaysFetchTerminalCollection, _alreadyFetchedTerminalCollection;
		private Obymobi.Data.CollectionClasses.TerminalConfigurationCollection	_terminalConfigurationCollection;
		private bool	_alwaysFetchTerminalConfigurationCollection, _alreadyFetchedTerminalConfigurationCollection;
		private Obymobi.Data.CollectionClasses.TimestampCollection	_timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection;
		private Obymobi.Data.CollectionClasses.UIFooterItemCollection	_uIFooterItemCollection;
		private bool	_alwaysFetchUIFooterItemCollection, _alreadyFetchedUIFooterItemCollection;
		private Obymobi.Data.CollectionClasses.UITabCollection	_uITabCollection;
		private bool	_alwaysFetchUITabCollection, _alreadyFetchedUITabCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup__, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaUITab;
		private bool	_alwaysFetchCategoryCollectionViaUITab, _alreadyFetchedCategoryCollectionViaUITab;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup, _alreadyFetchedCompanyCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup_, _alreadyFetchedCompanyCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup__, _alreadyFetchedCompanyCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaTerminal;
		private bool	_alwaysFetchCompanyCollectionViaTerminal, _alreadyFetchedCompanyCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal, _alreadyFetchedDeliverypointCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal_, _alreadyFetchedDeliverypointCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal, _alreadyFetchedDeliverypointgroupCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal;
		private bool	_alwaysFetchDeviceCollectionViaTerminal, _alreadyFetchedDeviceCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaUITab;
		private bool	_alwaysFetchEntertainmentCollectionViaUITab, _alreadyFetchedEntertainmentCollectionViaUITab;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection _icrtouchprintermappingCollectionViaTerminal;
		private bool	_alwaysFetchIcrtouchprintermappingCollectionViaTerminal, _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup_, _alreadyFetchedMenuCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup__, _alreadyFetchedMenuCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup, _alreadyFetchedMenuCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal;
		private bool	_alwaysFetchProductCollectionViaTerminal, _alreadyFetchedProductCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal_;
		private bool	_alwaysFetchProductCollectionViaTerminal_, _alreadyFetchedProductCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal__;
		private bool	_alwaysFetchProductCollectionViaTerminal__, _alreadyFetchedProductCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal___;
		private bool	_alwaysFetchProductCollectionViaTerminal___, _alreadyFetchedProductCollectionViaTerminal___;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup__, _alreadyFetchedRouteCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup___;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup___, _alreadyFetchedRouteCollectionViaDeliverypointgroup___;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup____;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup____, _alreadyFetchedRouteCollectionViaDeliverypointgroup____;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_____;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_____, _alreadyFetchedRouteCollectionViaDeliverypointgroup_____;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup, _alreadyFetchedRouteCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_, _alreadyFetchedRouteCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup_, _alreadyFetchedTerminalCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup__, _alreadyFetchedTerminalCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal;
		private bool	_alwaysFetchTerminalCollectionViaTerminal, _alreadyFetchedTerminalCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup, _alreadyFetchedTerminalCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup, _alreadyFetchedUIModeCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup__, _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup___;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup___, _alreadyFetchedUIModeCollectionViaDeliverypointgroup___;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup____;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup____, _alreadyFetchedUIModeCollectionViaDeliverypointgroup____;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_____;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_____, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaTerminal;
		private bool	_alwaysFetchUserCollectionViaTerminal, _alreadyFetchedUserCollectionViaTerminal;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;
		private UITabEntity _defaultUITabEntity;
		private bool	_alwaysFetchDefaultUITabEntity, _alreadyFetchedDefaultUITabEntity, _defaultUITabEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
			/// <summary>Member name DefaultUITabEntity</summary>
			public static readonly string DefaultUITabEntity = "DefaultUITabEntity";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection_</summary>
			public static readonly string DeliverypointgroupCollection_ = "DeliverypointgroupCollection_";
			/// <summary>Member name DeliverypointgroupCollection__</summary>
			public static readonly string DeliverypointgroupCollection__ = "DeliverypointgroupCollection__";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name TerminalConfigurationCollection</summary>
			public static readonly string TerminalConfigurationCollection = "TerminalConfigurationCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
			/// <summary>Member name UIFooterItemCollection</summary>
			public static readonly string UIFooterItemCollection = "UIFooterItemCollection";
			/// <summary>Member name UITabCollection</summary>
			public static readonly string UITabCollection = "UITabCollection";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup = "AnnouncementCollectionViaDeliverypointgroup";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup_</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup_ = "AnnouncementCollectionViaDeliverypointgroup_";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup__</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup__ = "AnnouncementCollectionViaDeliverypointgroup__";
			/// <summary>Member name CategoryCollectionViaUITab</summary>
			public static readonly string CategoryCollectionViaUITab = "CategoryCollectionViaUITab";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup = "CompanyCollectionViaDeliverypointgroup";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup_</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup_ = "CompanyCollectionViaDeliverypointgroup_";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup__</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup__ = "CompanyCollectionViaDeliverypointgroup__";
			/// <summary>Member name CompanyCollectionViaTerminal</summary>
			public static readonly string CompanyCollectionViaTerminal = "CompanyCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal</summary>
			public static readonly string DeliverypointCollectionViaTerminal = "DeliverypointCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal_</summary>
			public static readonly string DeliverypointCollectionViaTerminal_ = "DeliverypointCollectionViaTerminal_";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal = "DeliverypointgroupCollectionViaTerminal";
			/// <summary>Member name DeviceCollectionViaTerminal</summary>
			public static readonly string DeviceCollectionViaTerminal = "DeviceCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name EntertainmentCollectionViaUITab</summary>
			public static readonly string EntertainmentCollectionViaUITab = "EntertainmentCollectionViaUITab";
			/// <summary>Member name IcrtouchprintermappingCollectionViaTerminal</summary>
			public static readonly string IcrtouchprintermappingCollectionViaTerminal = "IcrtouchprintermappingCollectionViaTerminal";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup_</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup_ = "MenuCollectionViaDeliverypointgroup_";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup__</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup__ = "MenuCollectionViaDeliverypointgroup__";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup = "MenuCollectionViaDeliverypointgroup";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup_</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup_ = "PosdeliverypointgroupCollectionViaDeliverypointgroup_";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup__</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup__ = "PosdeliverypointgroupCollectionViaDeliverypointgroup__";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup = "PosdeliverypointgroupCollectionViaDeliverypointgroup";
			/// <summary>Member name ProductCollectionViaTerminal</summary>
			public static readonly string ProductCollectionViaTerminal = "ProductCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal_</summary>
			public static readonly string ProductCollectionViaTerminal_ = "ProductCollectionViaTerminal_";
			/// <summary>Member name ProductCollectionViaTerminal__</summary>
			public static readonly string ProductCollectionViaTerminal__ = "ProductCollectionViaTerminal__";
			/// <summary>Member name ProductCollectionViaTerminal___</summary>
			public static readonly string ProductCollectionViaTerminal___ = "ProductCollectionViaTerminal___";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup__</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup__ = "RouteCollectionViaDeliverypointgroup__";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup___</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup___ = "RouteCollectionViaDeliverypointgroup___";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup____</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup____ = "RouteCollectionViaDeliverypointgroup____";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_____</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_____ = "RouteCollectionViaDeliverypointgroup_____";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup = "RouteCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_ = "RouteCollectionViaDeliverypointgroup_";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup_</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup_ = "TerminalCollectionViaDeliverypointgroup_";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup__</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup__ = "TerminalCollectionViaDeliverypointgroup__";
			/// <summary>Member name TerminalCollectionViaTerminal</summary>
			public static readonly string TerminalCollectionViaTerminal = "TerminalCollectionViaTerminal";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup = "TerminalCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup = "UIModeCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_ = "UIModeCollectionViaDeliverypointgroup_";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup__</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup__ = "UIModeCollectionViaDeliverypointgroup__";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup___</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup___ = "UIModeCollectionViaDeliverypointgroup___";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup____</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup____ = "UIModeCollectionViaDeliverypointgroup____";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_____</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_____ = "UIModeCollectionViaDeliverypointgroup_____";
			/// <summary>Member name UserCollectionViaTerminal</summary>
			public static readonly string UserCollectionViaTerminal = "UserCollectionViaTerminal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIModeEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIModeEntityBase() :base("UIModeEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		protected UIModeEntityBase(System.Int32 uIModeId):base("UIModeEntity")
		{
			InitClassFetch(uIModeId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIModeEntityBase(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse): base("UIModeEntity")
		{
			InitClassFetch(uIModeId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="validator">The custom validator object for this UIModeEntity</param>
		protected UIModeEntityBase(System.Int32 uIModeId, IValidator validator):base("UIModeEntity")
		{
			InitClassFetch(uIModeId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIModeEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientConfigurationCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationCollection)info.GetValue("_clientConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationCollection));
			_alwaysFetchClientConfigurationCollection = info.GetBoolean("_alwaysFetchClientConfigurationCollection");
			_alreadyFetchedClientConfigurationCollection = info.GetBoolean("_alreadyFetchedClientConfigurationCollection");

			_deliverypointgroupCollection_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection_");
			_alreadyFetchedDeliverypointgroupCollection_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection_");

			_deliverypointgroupCollection__ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection__", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection__ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection__");
			_alreadyFetchedDeliverypointgroupCollection__ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection__");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_terminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection = info.GetBoolean("_alwaysFetchTerminalCollection");
			_alreadyFetchedTerminalCollection = info.GetBoolean("_alreadyFetchedTerminalCollection");

			_terminalConfigurationCollection = (Obymobi.Data.CollectionClasses.TerminalConfigurationCollection)info.GetValue("_terminalConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.TerminalConfigurationCollection));
			_alwaysFetchTerminalConfigurationCollection = info.GetBoolean("_alwaysFetchTerminalConfigurationCollection");
			_alreadyFetchedTerminalConfigurationCollection = info.GetBoolean("_alreadyFetchedTerminalConfigurationCollection");

			_timestampCollection = (Obymobi.Data.CollectionClasses.TimestampCollection)info.GetValue("_timestampCollection", typeof(Obymobi.Data.CollectionClasses.TimestampCollection));
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");

			_uIFooterItemCollection = (Obymobi.Data.CollectionClasses.UIFooterItemCollection)info.GetValue("_uIFooterItemCollection", typeof(Obymobi.Data.CollectionClasses.UIFooterItemCollection));
			_alwaysFetchUIFooterItemCollection = info.GetBoolean("_alwaysFetchUIFooterItemCollection");
			_alreadyFetchedUIFooterItemCollection = info.GetBoolean("_alreadyFetchedUIFooterItemCollection");

			_uITabCollection = (Obymobi.Data.CollectionClasses.UITabCollection)info.GetValue("_uITabCollection", typeof(Obymobi.Data.CollectionClasses.UITabCollection));
			_alwaysFetchUITabCollection = info.GetBoolean("_alwaysFetchUITabCollection");
			_alreadyFetchedUITabCollection = info.GetBoolean("_alreadyFetchedUITabCollection");
			_announcementCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup");

			_announcementCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_");

			_announcementCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup__");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__");

			_categoryCollectionViaUITab = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaUITab", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaUITab = info.GetBoolean("_alwaysFetchCategoryCollectionViaUITab");
			_alreadyFetchedCategoryCollectionViaUITab = info.GetBoolean("_alreadyFetchedCategoryCollectionViaUITab");

			_companyCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup");

			_companyCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup_");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup_");

			_companyCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup__");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup__");

			_companyCollectionViaTerminal = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaTerminal = info.GetBoolean("_alwaysFetchCompanyCollectionViaTerminal");
			_alreadyFetchedCompanyCollectionViaTerminal = info.GetBoolean("_alreadyFetchedCompanyCollectionViaTerminal");

			_deliverypointCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal");
			_alreadyFetchedDeliverypointCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal");

			_deliverypointCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal_");
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal_");

			_deliverypointgroupCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal");

			_deviceCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal");
			_alreadyFetchedDeviceCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_entertainmentCollectionViaUITab = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaUITab", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaUITab = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaUITab");
			_alreadyFetchedEntertainmentCollectionViaUITab = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaUITab");

			_icrtouchprintermappingCollectionViaTerminal = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal");
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal");

			_menuCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup_");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup_");

			_menuCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup__");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup__");

			_menuCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup");

			_posdeliverypointgroupCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_");

			_posdeliverypointgroupCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__");

			_posdeliverypointgroupCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup");

			_productCollectionViaTerminal = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal");
			_alreadyFetchedProductCollectionViaTerminal = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal");

			_productCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal_");
			_alreadyFetchedProductCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal_");

			_productCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal__");
			_alreadyFetchedProductCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal__");

			_productCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal___");
			_alreadyFetchedProductCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal___");

			_routeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup__");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup__");

			_routeCollectionViaDeliverypointgroup___ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup___", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup___ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup___");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup___ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup___");

			_routeCollectionViaDeliverypointgroup____ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup____", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup____ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup____");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup____ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup____");

			_routeCollectionViaDeliverypointgroup_____ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_____", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_____");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_____");

			_routeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_");

			_terminalCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup_");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup_");

			_terminalCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup__");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup__");

			_terminalCollectionViaTerminal = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal");
			_alreadyFetchedTerminalCollectionViaTerminal = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal");

			_terminalCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_");

			_uIModeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup__");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__");

			_uIModeCollectionViaDeliverypointgroup___ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup___", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup___ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup___");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup___");

			_uIModeCollectionViaDeliverypointgroup____ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup____", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup____ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup____");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup____");

			_uIModeCollectionViaDeliverypointgroup_____ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_____", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_____");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____");

			_userCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaTerminal = info.GetBoolean("_alwaysFetchUserCollectionViaTerminal");
			_alreadyFetchedUserCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUserCollectionViaTerminal");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");

			_defaultUITabEntity = (UITabEntity)info.GetValue("_defaultUITabEntity", typeof(UITabEntity));
			if(_defaultUITabEntity!=null)
			{
				_defaultUITabEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_defaultUITabEntityReturnsNewIfNotFound = info.GetBoolean("_defaultUITabEntityReturnsNewIfNotFound");
			_alwaysFetchDefaultUITabEntity = info.GetBoolean("_alwaysFetchDefaultUITabEntity");
			_alreadyFetchedDefaultUITabEntity = info.GetBoolean("_alreadyFetchedDefaultUITabEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIModeFieldIndex)fieldIndex)
			{
				case UIModeFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case UIModeFieldIndex.DefaultUITabId:
					DesetupSyncDefaultUITabEntity(true, false);
					_alreadyFetchedDefaultUITabEntity = false;
					break;
				case UIModeFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientConfigurationCollection = (_clientConfigurationCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection_ = (_deliverypointgroupCollection_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection__ = (_deliverypointgroupCollection__.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedTerminalCollection = (_terminalCollection.Count > 0);
			_alreadyFetchedTerminalConfigurationCollection = (_terminalConfigurationCollection.Count > 0);
			_alreadyFetchedTimestampCollection = (_timestampCollection.Count > 0);
			_alreadyFetchedUIFooterItemCollection = (_uIFooterItemCollection.Count > 0);
			_alreadyFetchedUITabCollection = (_uITabCollection.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = (_announcementCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = (_announcementCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ = (_announcementCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedCategoryCollectionViaUITab = (_categoryCollectionViaUITab.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = (_companyCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = (_companyCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ = (_companyCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedCompanyCollectionViaTerminal = (_companyCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal = (_deliverypointCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = (_deliverypointCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = (_deliverypointgroupCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal = (_deviceCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaUITab = (_entertainmentCollectionViaUITab.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = (_icrtouchprintermappingCollectionViaTerminal.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = (_menuCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup__ = (_menuCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = (_menuCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = (_posdeliverypointgroupCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ = (_posdeliverypointgroupCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = (_posdeliverypointgroupCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal = (_productCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal_ = (_productCollectionViaTerminal_.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal__ = (_productCollectionViaTerminal__.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal___ = (_productCollectionViaTerminal___.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup__ = (_routeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup___ = (_routeCollectionViaDeliverypointgroup___.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup____ = (_routeCollectionViaDeliverypointgroup____.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ = (_routeCollectionViaDeliverypointgroup_____.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = (_routeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = (_routeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = (_terminalCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ = (_terminalCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal = (_terminalCollectionViaTerminal.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = (_terminalCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = (_uIModeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = (_uIModeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = (_uIModeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = (_uIModeCollectionViaDeliverypointgroup___.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = (_uIModeCollectionViaDeliverypointgroup____.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = (_uIModeCollectionViaDeliverypointgroup_____.Count > 0);
			_alreadyFetchedUserCollectionViaTerminal = (_userCollectionViaTerminal.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
			_alreadyFetchedDefaultUITabEntity = (_defaultUITabEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				case "DefaultUITabEntity":
					toReturn.Add(Relations.UITabEntityUsingDefaultUITabId);
					break;
				case "ClientConfigurationCollection":
					toReturn.Add(Relations.ClientConfigurationEntityUsingUIModeId);
					break;
				case "DeliverypointgroupCollection_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId);
					break;
				case "DeliverypointgroupCollection__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId);
					break;
				case "TerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId);
					break;
				case "TerminalConfigurationCollection":
					toReturn.Add(Relations.TerminalConfigurationEntityUsingUIModeId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingUIModeId);
					break;
				case "UIFooterItemCollection":
					toReturn.Add(Relations.UIFooterItemEntityUsingUIModeId);
					break;
				case "UITabCollection":
					toReturn.Add(Relations.UITabEntityUsingUIModeId);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaUITab":
					toReturn.Add(Relations.UITabEntityUsingUIModeId, "UIModeEntity__", "UITab_", JoinHint.None);
					toReturn.Add(UITabEntity.Relations.CategoryEntityUsingCategoryId, "UITab_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaUITab":
					toReturn.Add(Relations.UITabEntityUsingUIModeId, "UIModeEntity__", "UITab_", JoinHint.None);
					toReturn.Add(UITabEntity.Relations.EntertainmentEntityUsingEntertainmentId, "UITab_", string.Empty, JoinHint.None);
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingBatteryLowProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingClientDisconnectedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingOrderFailedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingUnlockDeliverypointProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup___":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingTabletUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup___":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMobileUIModeId, "UIModeEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UserCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingUIModeId, "UIModeEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientConfigurationCollection", (!this.MarkedForDeletion?_clientConfigurationCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationCollection", _alwaysFetchClientConfigurationCollection);
			info.AddValue("_alreadyFetchedClientConfigurationCollection", _alreadyFetchedClientConfigurationCollection);
			info.AddValue("_deliverypointgroupCollection_", (!this.MarkedForDeletion?_deliverypointgroupCollection_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection_", _alwaysFetchDeliverypointgroupCollection_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection_", _alreadyFetchedDeliverypointgroupCollection_);
			info.AddValue("_deliverypointgroupCollection__", (!this.MarkedForDeletion?_deliverypointgroupCollection__:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection__", _alwaysFetchDeliverypointgroupCollection__);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection__", _alreadyFetchedDeliverypointgroupCollection__);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_terminalCollection", (!this.MarkedForDeletion?_terminalCollection:null));
			info.AddValue("_alwaysFetchTerminalCollection", _alwaysFetchTerminalCollection);
			info.AddValue("_alreadyFetchedTerminalCollection", _alreadyFetchedTerminalCollection);
			info.AddValue("_terminalConfigurationCollection", (!this.MarkedForDeletion?_terminalConfigurationCollection:null));
			info.AddValue("_alwaysFetchTerminalConfigurationCollection", _alwaysFetchTerminalConfigurationCollection);
			info.AddValue("_alreadyFetchedTerminalConfigurationCollection", _alreadyFetchedTerminalConfigurationCollection);
			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);
			info.AddValue("_uIFooterItemCollection", (!this.MarkedForDeletion?_uIFooterItemCollection:null));
			info.AddValue("_alwaysFetchUIFooterItemCollection", _alwaysFetchUIFooterItemCollection);
			info.AddValue("_alreadyFetchedUIFooterItemCollection", _alreadyFetchedUIFooterItemCollection);
			info.AddValue("_uITabCollection", (!this.MarkedForDeletion?_uITabCollection:null));
			info.AddValue("_alwaysFetchUITabCollection", _alwaysFetchUITabCollection);
			info.AddValue("_alreadyFetchedUITabCollection", _alreadyFetchedUITabCollection);
			info.AddValue("_announcementCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_announcementCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_);
			info.AddValue("_announcementCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup__", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__);
			info.AddValue("_categoryCollectionViaUITab", (!this.MarkedForDeletion?_categoryCollectionViaUITab:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaUITab", _alwaysFetchCategoryCollectionViaUITab);
			info.AddValue("_alreadyFetchedCategoryCollectionViaUITab", _alreadyFetchedCategoryCollectionViaUITab);
			info.AddValue("_companyCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup", _alwaysFetchCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup", _alreadyFetchedCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_companyCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup_", _alwaysFetchCompanyCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup_", _alreadyFetchedCompanyCollectionViaDeliverypointgroup_);
			info.AddValue("_companyCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup__", _alwaysFetchCompanyCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup__", _alreadyFetchedCompanyCollectionViaDeliverypointgroup__);
			info.AddValue("_companyCollectionViaTerminal", (!this.MarkedForDeletion?_companyCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaTerminal", _alwaysFetchCompanyCollectionViaTerminal);
			info.AddValue("_alreadyFetchedCompanyCollectionViaTerminal", _alreadyFetchedCompanyCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal", _alwaysFetchDeliverypointCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal", _alreadyFetchedDeliverypointCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal_", _alwaysFetchDeliverypointCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal_", _alreadyFetchedDeliverypointCollectionViaTerminal_);
			info.AddValue("_deliverypointgroupCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal", _alwaysFetchDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal", _alreadyFetchedDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_deviceCollectionViaTerminal", (!this.MarkedForDeletion?_deviceCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal", _alwaysFetchDeviceCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal", _alreadyFetchedDeviceCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_entertainmentCollectionViaUITab", (!this.MarkedForDeletion?_entertainmentCollectionViaUITab:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaUITab", _alwaysFetchEntertainmentCollectionViaUITab);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaUITab", _alreadyFetchedEntertainmentCollectionViaUITab);
			info.AddValue("_icrtouchprintermappingCollectionViaTerminal", (!this.MarkedForDeletion?_icrtouchprintermappingCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal", _alwaysFetchIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal", _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_menuCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup_", _alwaysFetchMenuCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup_", _alreadyFetchedMenuCollectionViaDeliverypointgroup_);
			info.AddValue("_menuCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup__", _alwaysFetchMenuCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup__", _alreadyFetchedMenuCollectionViaDeliverypointgroup__);
			info.AddValue("_menuCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup", _alwaysFetchMenuCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup", _alreadyFetchedMenuCollectionViaDeliverypointgroup);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_productCollectionViaTerminal", (!this.MarkedForDeletion?_productCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal", _alwaysFetchProductCollectionViaTerminal);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal", _alreadyFetchedProductCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal_", (!this.MarkedForDeletion?_productCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal_", _alwaysFetchProductCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal_", _alreadyFetchedProductCollectionViaTerminal_);
			info.AddValue("_productCollectionViaTerminal__", (!this.MarkedForDeletion?_productCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal__", _alwaysFetchProductCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal__", _alreadyFetchedProductCollectionViaTerminal__);
			info.AddValue("_productCollectionViaTerminal___", (!this.MarkedForDeletion?_productCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal___", _alwaysFetchProductCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal___", _alreadyFetchedProductCollectionViaTerminal___);
			info.AddValue("_routeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup__", _alwaysFetchRouteCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup__", _alreadyFetchedRouteCollectionViaDeliverypointgroup__);
			info.AddValue("_routeCollectionViaDeliverypointgroup___", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup___:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup___", _alwaysFetchRouteCollectionViaDeliverypointgroup___);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup___", _alreadyFetchedRouteCollectionViaDeliverypointgroup___);
			info.AddValue("_routeCollectionViaDeliverypointgroup____", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup____:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup____", _alwaysFetchRouteCollectionViaDeliverypointgroup____);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup____", _alreadyFetchedRouteCollectionViaDeliverypointgroup____);
			info.AddValue("_routeCollectionViaDeliverypointgroup_____", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_____:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_____", _alwaysFetchRouteCollectionViaDeliverypointgroup_____);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_____", _alreadyFetchedRouteCollectionViaDeliverypointgroup_____);
			info.AddValue("_routeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup", _alwaysFetchRouteCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup", _alreadyFetchedRouteCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_", _alwaysFetchRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_", _alreadyFetchedRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_terminalCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup_", _alwaysFetchTerminalCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup_", _alreadyFetchedTerminalCollectionViaDeliverypointgroup_);
			info.AddValue("_terminalCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup__", _alwaysFetchTerminalCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup__", _alreadyFetchedTerminalCollectionViaDeliverypointgroup__);
			info.AddValue("_terminalCollectionViaTerminal", (!this.MarkedForDeletion?_terminalCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal", _alwaysFetchTerminalCollectionViaTerminal);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal", _alreadyFetchedTerminalCollectionViaTerminal);
			info.AddValue("_terminalCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup", _alwaysFetchTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup", _alreadyFetchedTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup", _alwaysFetchUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup", _alreadyFetchedUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_", _alwaysFetchUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup__", _alwaysFetchUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__", _alreadyFetchedUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup___", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup___:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup___", _alwaysFetchUIModeCollectionViaDeliverypointgroup___);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup___", _alreadyFetchedUIModeCollectionViaDeliverypointgroup___);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup____", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup____:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup____", _alwaysFetchUIModeCollectionViaDeliverypointgroup____);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup____", _alreadyFetchedUIModeCollectionViaDeliverypointgroup____);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_____", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_____:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_____", _alwaysFetchUIModeCollectionViaDeliverypointgroup_____);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____);
			info.AddValue("_userCollectionViaTerminal", (!this.MarkedForDeletion?_userCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUserCollectionViaTerminal", _alwaysFetchUserCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUserCollectionViaTerminal", _alreadyFetchedUserCollectionViaTerminal);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);
			info.AddValue("_defaultUITabEntity", (!this.MarkedForDeletion?_defaultUITabEntity:null));
			info.AddValue("_defaultUITabEntityReturnsNewIfNotFound", _defaultUITabEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDefaultUITabEntity", _alwaysFetchDefaultUITabEntity);
			info.AddValue("_alreadyFetchedDefaultUITabEntity", _alreadyFetchedDefaultUITabEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				case "DefaultUITabEntity":
					_alreadyFetchedDefaultUITabEntity = true;
					this.DefaultUITabEntity = (UITabEntity)entity;
					break;
				case "ClientConfigurationCollection":
					_alreadyFetchedClientConfigurationCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationCollection.Add((ClientConfigurationEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection_":
					_alreadyFetchedDeliverypointgroupCollection_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection__":
					_alreadyFetchedDeliverypointgroupCollection__ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection__.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "TerminalCollection":
					_alreadyFetchedTerminalCollection = true;
					if(entity!=null)
					{
						this.TerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalConfigurationCollection":
					_alreadyFetchedTerminalConfigurationCollection = true;
					if(entity!=null)
					{
						this.TerminalConfigurationCollection.Add((TerminalConfigurationEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					if(entity!=null)
					{
						this.TimestampCollection.Add((TimestampEntity)entity);
					}
					break;
				case "UIFooterItemCollection":
					_alreadyFetchedUIFooterItemCollection = true;
					if(entity!=null)
					{
						this.UIFooterItemCollection.Add((UIFooterItemEntity)entity);
					}
					break;
				case "UITabCollection":
					_alreadyFetchedUITabCollection = true;
					if(entity!=null)
					{
						this.UITabCollection.Add((UITabEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup.Add((AnnouncementEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup_":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup_.Add((AnnouncementEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup__":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup__.Add((AnnouncementEntity)entity);
					}
					break;
				case "CategoryCollectionViaUITab":
					_alreadyFetchedCategoryCollectionViaUITab = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaUITab.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup_":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup_.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup__":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup__.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaTerminal":
					_alreadyFetchedCompanyCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaTerminal.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal":
					_alreadyFetchedDeliverypointCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal_":
					_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal":
					_alreadyFetchedDeviceCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal.Add((DeviceEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaUITab":
					_alreadyFetchedEntertainmentCollectionViaUITab = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaUITab.Add((EntertainmentEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollectionViaTerminal.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup_":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup_.Add((MenuEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup__":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup__.Add((MenuEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup.Add((MenuEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup_":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup_.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup__":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup__.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal":
					_alreadyFetchedProductCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal_":
					_alreadyFetchedProductCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal__":
					_alreadyFetchedProductCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal___":
					_alreadyFetchedProductCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal___.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup__":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup__.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup___":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup___ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup___.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup____":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup____ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup____.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_____":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_____.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_.Add((RouteEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup_":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup__":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup__.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal":
					_alreadyFetchedTerminalCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup__.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup___":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup___.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup____":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup____.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_____":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_____.Add((UIModeEntity)entity);
					}
					break;
				case "UserCollectionViaTerminal":
					_alreadyFetchedUserCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UserCollectionViaTerminal.Add((UserEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				case "DefaultUITabEntity":
					SetupSyncDefaultUITabEntity(relatedEntity);
					break;
				case "ClientConfigurationCollection":
					_clientConfigurationCollection.Add((ClientConfigurationEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection_":
					_deliverypointgroupCollection_.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection__":
					_deliverypointgroupCollection__.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "TerminalCollection":
					_terminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				case "TerminalConfigurationCollection":
					_terminalConfigurationCollection.Add((TerminalConfigurationEntity)relatedEntity);
					break;
				case "TimestampCollection":
					_timestampCollection.Add((TimestampEntity)relatedEntity);
					break;
				case "UIFooterItemCollection":
					_uIFooterItemCollection.Add((UIFooterItemEntity)relatedEntity);
					break;
				case "UITabCollection":
					_uITabCollection.Add((UITabEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				case "DefaultUITabEntity":
					DesetupSyncDefaultUITabEntity(false, true);
					break;
				case "ClientConfigurationCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection_":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection__":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection__, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection":
					this.PerformRelatedEntityRemoval(_terminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalConfigurationCollection":
					this.PerformRelatedEntityRemoval(_terminalConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					this.PerformRelatedEntityRemoval(_timestampCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIFooterItemCollection":
					this.PerformRelatedEntityRemoval(_uIFooterItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UITabCollection":
					this.PerformRelatedEntityRemoval(_uITabCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			if(_defaultUITabEntity!=null)
			{
				toReturn.Add(_defaultUITabEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientConfigurationCollection);
			toReturn.Add(_deliverypointgroupCollection_);
			toReturn.Add(_deliverypointgroupCollection__);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_terminalCollection);
			toReturn.Add(_terminalConfigurationCollection);
			toReturn.Add(_timestampCollection);
			toReturn.Add(_uIFooterItemCollection);
			toReturn.Add(_uITabCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIModeId)
		{
			return FetchUsingPK(uIModeId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIModeId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIModeId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIModeId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIModeId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIModeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationCollection || forceFetch || _alwaysFetchClientConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationCollection);
				_clientConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, filter);
				_clientConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationCollection = true;
			}
			return _clientConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationCollection'. These settings will be taken into account
		/// when the property ClientConfigurationCollection is requested or GetMultiClientConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationCollection.SortClauses=sortClauses;
			_clientConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, _deliverypointgroupCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, _deliverypointgroupCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection_ || forceFetch || _alwaysFetchDeliverypointgroupCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection_);
				_deliverypointgroupCollection_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection_.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_deliverypointgroupCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection_ = true;
			}
			return _deliverypointgroupCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection_ is requested or GetMultiDeliverypointgroupCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection_.SortClauses=sortClauses;
			_deliverypointgroupCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, _deliverypointgroupCollection__.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, _deliverypointgroupCollection__.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection__ || forceFetch || _alwaysFetchDeliverypointgroupCollection__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection__);
				_deliverypointgroupCollection__.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection__.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection__.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_deliverypointgroupCollection__.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection__ = true;
			}
			return _deliverypointgroupCollection__;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection__'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection__ is requested or GetMultiDeliverypointgroupCollection__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection__.SortClauses=sortClauses;
			_deliverypointgroupCollection__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection || forceFetch || _alwaysFetchTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection);
				_terminalCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_terminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection = true;
			}
			return _terminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection'. These settings will be taken into account
		/// when the property TerminalCollection is requested or GetMultiTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection.SortClauses=sortClauses;
			_terminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, _terminalConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, _terminalConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalConfigurationCollection || forceFetch || _alwaysFetchTerminalConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalConfigurationCollection);
				_terminalConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalConfigurationCollection.GetMultiManyToOne(null, null, this, filter);
				_terminalConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalConfigurationCollection = true;
			}
			return _terminalConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalConfigurationCollection'. These settings will be taken into account
		/// when the property TerminalConfigurationCollection is requested or GetMultiTerminalConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalConfigurationCollection.SortClauses=sortClauses;
			_terminalConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTimestampCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_timestampCollection);
				_timestampCollection.SuppressClearInGetMulti=!forceFetch;
				_timestampCollection.EntityFactoryToUse = entityFactoryToUse;
				_timestampCollection.GetMultiManyToOne(null, this, null, null, filter);
				_timestampCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTimestampCollection = true;
			}
			return _timestampCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TimestampCollection'. These settings will be taken into account
		/// when the property TimestampCollection is requested or GetMultiTimestampCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTimestampCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_timestampCollection.SortClauses=sortClauses;
			_timestampCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIFooterItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemCollection GetMultiUIFooterItemCollection(bool forceFetch)
		{
			return GetMultiUIFooterItemCollection(forceFetch, _uIFooterItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIFooterItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemCollection GetMultiUIFooterItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIFooterItemCollection(forceFetch, _uIFooterItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemCollection GetMultiUIFooterItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIFooterItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIFooterItemCollection GetMultiUIFooterItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIFooterItemCollection || forceFetch || _alwaysFetchUIFooterItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIFooterItemCollection);
				_uIFooterItemCollection.SuppressClearInGetMulti=!forceFetch;
				_uIFooterItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIFooterItemCollection.GetMultiManyToOne(this, filter);
				_uIFooterItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIFooterItemCollection = true;
			}
			return _uIFooterItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIFooterItemCollection'. These settings will be taken into account
		/// when the property UIFooterItemCollection is requested or GetMultiUIFooterItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIFooterItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIFooterItemCollection.SortClauses=sortClauses;
			_uIFooterItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUITabCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUITabCollection || forceFetch || _alwaysFetchUITabCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabCollection);
				_uITabCollection.SuppressClearInGetMulti=!forceFetch;
				_uITabCollection.EntityFactoryToUse = entityFactoryToUse;
				_uITabCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_uITabCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabCollection = true;
			}
			return _uITabCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabCollection'. These settings will be taken into account
		/// when the property UITabCollection is requested or GetMultiUITabCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabCollection.SortClauses=sortClauses;
			_uITabCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup(forceFetch, _announcementCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
			}
			return _announcementCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup_(forceFetch, _announcementCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_announcementCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup_"));
				_announcementCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = true;
			}
			return _announcementCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup_ is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup__(forceFetch, _announcementCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_announcementCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup__"));
				_announcementCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ = true;
			}
			return _announcementCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup__ is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaUITab(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaUITab(forceFetch, _categoryCollectionViaUITab.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaUITab(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaUITab || forceFetch || _alwaysFetchCategoryCollectionViaUITab) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaUITab);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_categoryCollectionViaUITab.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaUITab.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaUITab.GetMulti(filter, GetRelationsForField("CategoryCollectionViaUITab"));
				_categoryCollectionViaUITab.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaUITab = true;
			}
			return _categoryCollectionViaUITab;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaUITab'. These settings will be taken into account
		/// when the property CategoryCollectionViaUITab is requested or GetMultiCategoryCollectionViaUITab is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaUITab(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaUITab.SortClauses=sortClauses;
			_categoryCollectionViaUITab.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup(forceFetch, _companyCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
			}
			return _companyCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup is requested or GetMultiCompanyCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup_(forceFetch, _companyCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_companyCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup_"));
				_companyCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = true;
			}
			return _companyCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup_ is requested or GetMultiCompanyCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup__(forceFetch, _companyCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_companyCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup__"));
				_companyCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ = true;
			}
			return _companyCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup__ is requested or GetMultiCompanyCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaTerminal(forceFetch, _companyCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaTerminal || forceFetch || _alwaysFetchCompanyCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaTerminal.GetMulti(filter, GetRelationsForField("CompanyCollectionViaTerminal"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaTerminal = true;
			}
			return _companyCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaTerminal'. These settings will be taken into account
		/// when the property CompanyCollectionViaTerminal is requested or GetMultiCompanyCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaTerminal.SortClauses=sortClauses;
			_companyCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal(forceFetch, _deliverypointCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal = true;
			}
			return _deliverypointCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal is requested or GetMultiDeliverypointCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal_(forceFetch, _deliverypointCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal_"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
			}
			return _deliverypointCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal_ is requested or GetMultiDeliverypointCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal(forceFetch, _deliverypointgroupCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
			}
			return _deliverypointgroupCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal is requested or GetMultiDeliverypointgroupCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal(forceFetch, _deviceCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal || forceFetch || _alwaysFetchDeviceCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal = true;
			}
			return _deviceCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal is requested or GetMultiDeviceCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal.SortClauses=sortClauses;
			_deviceCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaUITab(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaUITab(forceFetch, _entertainmentCollectionViaUITab.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaUITab(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaUITab || forceFetch || _alwaysFetchEntertainmentCollectionViaUITab) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaUITab);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_entertainmentCollectionViaUITab.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaUITab.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaUITab.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaUITab"));
				_entertainmentCollectionViaUITab.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaUITab = true;
			}
			return _entertainmentCollectionViaUITab;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaUITab'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaUITab is requested or GetMultiEntertainmentCollectionViaUITab is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaUITab(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaUITab.SortClauses=sortClauses;
			_entertainmentCollectionViaUITab.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollectionViaTerminal(forceFetch, _icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal || forceFetch || _alwaysFetchIcrtouchprintermappingCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollectionViaTerminal.GetMulti(filter, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
			}
			return _icrtouchprintermappingCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollectionViaTerminal'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollectionViaTerminal is requested or GetMultiIcrtouchprintermappingCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollectionViaTerminal.SortClauses=sortClauses;
			_icrtouchprintermappingCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup_(forceFetch, _menuCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_menuCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup_"));
				_menuCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = true;
			}
			return _menuCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup_ is requested or GetMultiMenuCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup__(forceFetch, _menuCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_menuCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup__"));
				_menuCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup__ = true;
			}
			return _menuCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup__ is requested or GetMultiMenuCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup(forceFetch, _menuCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
			}
			return _menuCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup is requested or GetMultiMenuCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup_"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup_ is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup__ is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal(forceFetch, _productCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal || forceFetch || _alwaysFetchProductCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal = true;
			}
			return _productCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal is requested or GetMultiProductCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal.SortClauses=sortClauses;
			_productCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal_(forceFetch, _productCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal_ || forceFetch || _alwaysFetchProductCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal_"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal_ = true;
			}
			return _productCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal_'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal_ is requested or GetMultiProductCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal_.SortClauses=sortClauses;
			_productCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal__(forceFetch, _productCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal__ || forceFetch || _alwaysFetchProductCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal__ = true;
			}
			return _productCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal__'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal__ is requested or GetMultiProductCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal__.SortClauses=sortClauses;
			_productCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal___(forceFetch, _productCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal___ || forceFetch || _alwaysFetchProductCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal___"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal___ = true;
			}
			return _productCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal___'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal___ is requested or GetMultiProductCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal___.SortClauses=sortClauses;
			_productCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup__(forceFetch, _routeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup__"));
				_routeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup__ = true;
			}
			return _routeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup__ is requested or GetMultiRouteCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup___(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup___(forceFetch, _routeCollectionViaDeliverypointgroup___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup___ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup___.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup___.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup___"));
				_routeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup___ = true;
			}
			return _routeCollectionViaDeliverypointgroup___;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup___'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup___ is requested or GetMultiRouteCollectionViaDeliverypointgroup___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup___.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup____(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup____(forceFetch, _routeCollectionViaDeliverypointgroup____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup____ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup____.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup____.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup____"));
				_routeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup____ = true;
			}
			return _routeCollectionViaDeliverypointgroup____;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup____'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup____ is requested or GetMultiRouteCollectionViaDeliverypointgroup____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup____.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_____(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_____(forceFetch, _routeCollectionViaDeliverypointgroup_____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_____.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_____.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_____"));
				_routeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ = true;
			}
			return _routeCollectionViaDeliverypointgroup_____;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_____'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_____ is requested or GetMultiRouteCollectionViaDeliverypointgroup_____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_____.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup(forceFetch, _routeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
			}
			return _routeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup is requested or GetMultiRouteCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_(forceFetch, _routeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
			}
			return _routeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_ is requested or GetMultiRouteCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup_(forceFetch, _terminalCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_terminalCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup_"));
				_terminalCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = true;
			}
			return _terminalCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup_ is requested or GetMultiTerminalCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup__(forceFetch, _terminalCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_terminalCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup__"));
				_terminalCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ = true;
			}
			return _terminalCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup__ is requested or GetMultiTerminalCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal(forceFetch, _terminalCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal || forceFetch || _alwaysFetchTerminalCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal = true;
			}
			return _terminalCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal is requested or GetMultiTerminalCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal.SortClauses=sortClauses;
			_terminalCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup(forceFetch, _terminalCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
			}
			return _terminalCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup is requested or GetMultiTerminalCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup(forceFetch, _uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
			}
			return _uIModeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup is requested or GetMultiUIModeCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_(forceFetch, _uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup__(forceFetch, _uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup__ is requested or GetMultiUIModeCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup___(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup___(forceFetch, _uIModeCollectionViaDeliverypointgroup___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup___.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup___.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup___"));
				_uIModeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup___;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup___'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup___ is requested or GetMultiUIModeCollectionViaDeliverypointgroup___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup___.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup____(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup____(forceFetch, _uIModeCollectionViaDeliverypointgroup____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup____.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup____.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup____"));
				_uIModeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup____;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup____'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup____ is requested or GetMultiUIModeCollectionViaDeliverypointgroup____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup____.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_____(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_____(forceFetch, _uIModeCollectionViaDeliverypointgroup_____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_uIModeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_____.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_____.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_____"));
				_uIModeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_____;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_____'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_____ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_____.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUserCollectionViaTerminal(forceFetch, _userCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaTerminal || forceFetch || _alwaysFetchUserCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UIModeFields.UIModeId, ComparisonOperator.Equal, this.UIModeId, "UIModeEntity__"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UserCollectionViaTerminal"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaTerminal = true;
			}
			return _userCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaTerminal'. These settings will be taken into account
		/// when the property UserCollectionViaTerminal is requested or GetMultiUserCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaTerminal.SortClauses=sortClauses;
			_userCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}


		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public UITabEntity GetSingleDefaultUITabEntity()
		{
			return GetSingleDefaultUITabEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public virtual UITabEntity GetSingleDefaultUITabEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDefaultUITabEntity || forceFetch || _alwaysFetchDefaultUITabEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UITabEntityUsingDefaultUITabId);
				UITabEntity newEntity = new UITabEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DefaultUITabId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UITabEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_defaultUITabEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DefaultUITabEntity = newEntity;
				_alreadyFetchedDefaultUITabEntity = fetchResult;
			}
			return _defaultUITabEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			toReturn.Add("DefaultUITabEntity", _defaultUITabEntity);
			toReturn.Add("ClientConfigurationCollection", _clientConfigurationCollection);
			toReturn.Add("DeliverypointgroupCollection_", _deliverypointgroupCollection_);
			toReturn.Add("DeliverypointgroupCollection__", _deliverypointgroupCollection__);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("TerminalCollection", _terminalCollection);
			toReturn.Add("TerminalConfigurationCollection", _terminalConfigurationCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			toReturn.Add("UIFooterItemCollection", _uIFooterItemCollection);
			toReturn.Add("UITabCollection", _uITabCollection);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup", _announcementCollectionViaDeliverypointgroup);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup_", _announcementCollectionViaDeliverypointgroup_);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup__", _announcementCollectionViaDeliverypointgroup__);
			toReturn.Add("CategoryCollectionViaUITab", _categoryCollectionViaUITab);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup", _companyCollectionViaDeliverypointgroup);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup_", _companyCollectionViaDeliverypointgroup_);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup__", _companyCollectionViaDeliverypointgroup__);
			toReturn.Add("CompanyCollectionViaTerminal", _companyCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal", _deliverypointCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal_", _deliverypointCollectionViaTerminal_);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal", _deliverypointgroupCollectionViaTerminal);
			toReturn.Add("DeviceCollectionViaTerminal", _deviceCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("EntertainmentCollectionViaUITab", _entertainmentCollectionViaUITab);
			toReturn.Add("IcrtouchprintermappingCollectionViaTerminal", _icrtouchprintermappingCollectionViaTerminal);
			toReturn.Add("MenuCollectionViaDeliverypointgroup_", _menuCollectionViaDeliverypointgroup_);
			toReturn.Add("MenuCollectionViaDeliverypointgroup__", _menuCollectionViaDeliverypointgroup__);
			toReturn.Add("MenuCollectionViaDeliverypointgroup", _menuCollectionViaDeliverypointgroup);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup_", _posdeliverypointgroupCollectionViaDeliverypointgroup_);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup__", _posdeliverypointgroupCollectionViaDeliverypointgroup__);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup", _posdeliverypointgroupCollectionViaDeliverypointgroup);
			toReturn.Add("ProductCollectionViaTerminal", _productCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal_", _productCollectionViaTerminal_);
			toReturn.Add("ProductCollectionViaTerminal__", _productCollectionViaTerminal__);
			toReturn.Add("ProductCollectionViaTerminal___", _productCollectionViaTerminal___);
			toReturn.Add("RouteCollectionViaDeliverypointgroup__", _routeCollectionViaDeliverypointgroup__);
			toReturn.Add("RouteCollectionViaDeliverypointgroup___", _routeCollectionViaDeliverypointgroup___);
			toReturn.Add("RouteCollectionViaDeliverypointgroup____", _routeCollectionViaDeliverypointgroup____);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_____", _routeCollectionViaDeliverypointgroup_____);
			toReturn.Add("RouteCollectionViaDeliverypointgroup", _routeCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_", _routeCollectionViaDeliverypointgroup_);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup_", _terminalCollectionViaDeliverypointgroup_);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup__", _terminalCollectionViaDeliverypointgroup__);
			toReturn.Add("TerminalCollectionViaTerminal", _terminalCollectionViaTerminal);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup", _terminalCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup", _uIModeCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_", _uIModeCollectionViaDeliverypointgroup_);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup__", _uIModeCollectionViaDeliverypointgroup__);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup___", _uIModeCollectionViaDeliverypointgroup___);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup____", _uIModeCollectionViaDeliverypointgroup____);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_____", _uIModeCollectionViaDeliverypointgroup_____);
			toReturn.Add("UserCollectionViaTerminal", _userCollectionViaTerminal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="validator">The validator object for this UIModeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIModeId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIModeId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientConfigurationCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationCollection();
			_clientConfigurationCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_deliverypointgroupCollection_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection_.SetContainingEntityInfo(this, "MobileUIModeEntity");

			_deliverypointgroupCollection__ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection__.SetContainingEntityInfo(this, "TabletUIModeEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_terminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_terminalConfigurationCollection = new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection();
			_terminalConfigurationCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_timestampCollection = new Obymobi.Data.CollectionClasses.TimestampCollection();
			_timestampCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_uIFooterItemCollection = new Obymobi.Data.CollectionClasses.UIFooterItemCollection();
			_uIFooterItemCollection.SetContainingEntityInfo(this, "UIModeEntity");

			_uITabCollection = new Obymobi.Data.CollectionClasses.UITabCollection();
			_uITabCollection.SetContainingEntityInfo(this, "UIModeEntity");
			_announcementCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_categoryCollectionViaUITab = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaTerminal = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deviceCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaUITab = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_icrtouchprintermappingCollectionViaTerminal = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_menuCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.MenuCollection();
			_menuCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.MenuCollection();
			_menuCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.MenuCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_productCollectionViaTerminal = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup___ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup____ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_____ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_terminalCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaTerminal = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup___ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup____ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup_____ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_userCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UserCollection();
			_companyEntityReturnsNewIfNotFound = true;
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			_defaultUITabEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowServiceOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowRequestBill", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowDeliverypoint", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowClock", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowBattery", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowBrightness", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowFullscreenEyecatcher", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultUITabId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowFooterLogo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "UIModeCollection", resetFKFields, new int[] { (int)UIModeFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "UIModeCollection", resetFKFields, new int[] { (int)UIModeFieldIndex.PointOfInterestId } );		
			_pointOfInterestEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{		
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _defaultUITabEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDefaultUITabEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _defaultUITabEntity, new PropertyChangedEventHandler( OnDefaultUITabEntityPropertyChanged ), "DefaultUITabEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.UITabEntityUsingDefaultUITabIdStatic, true, signalRelatedEntity, "UIModeCollection", resetFKFields, new int[] { (int)UIModeFieldIndex.DefaultUITabId } );		
			_defaultUITabEntity = null;
		}
		
		/// <summary> setups the sync logic for member _defaultUITabEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDefaultUITabEntity(IEntityCore relatedEntity)
		{
			if(_defaultUITabEntity!=relatedEntity)
			{		
				DesetupSyncDefaultUITabEntity(true, true);
				_defaultUITabEntity = (UITabEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _defaultUITabEntity, new PropertyChangedEventHandler( OnDefaultUITabEntityPropertyChanged ), "DefaultUITabEntity", Obymobi.Data.RelationClasses.StaticUIModeRelations.UITabEntityUsingDefaultUITabIdStatic, true, ref _alreadyFetchedDefaultUITabEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDefaultUITabEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIModeFieldIndex.UIModeId].ForcedCurrentValueWrite(uIModeId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIModeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIModeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIModeRelations Relations
		{
			get	{ return new UIModeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection_")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection__
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection__")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection(), (IEntityRelation)GetRelationsForField("TerminalConfigurationCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalConfigurationEntity, 0, null, null, null, "TerminalConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIFooterItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIFooterItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIFooterItemCollection(), (IEntityRelation)GetRelationsForField("UIFooterItemCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIFooterItemEntity, 0, null, null, null, "UIFooterItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("UITabCollection")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "UITabCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"), "AnnouncementCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup_"), "AnnouncementCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup__"), "AnnouncementCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaUITab
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "UITab_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaUITab"), "CategoryCollectionViaUITab", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"), "CompanyCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup_"), "CompanyCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup__"), "CompanyCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaTerminal"), "CompanyCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal"), "DeliverypointCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal_"), "DeliverypointCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"), "DeliverypointgroupCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal"), "DeviceCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaUITab
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "UITab_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaUITab"), "EntertainmentCollectionViaUITab", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"), "IcrtouchprintermappingCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup_"), "MenuCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup__"), "MenuCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup"), "MenuCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup_"), "PosdeliverypointgroupCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup__"), "PosdeliverypointgroupCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"), "PosdeliverypointgroupCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal"), "ProductCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal_"), "ProductCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal__"), "ProductCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal___"), "ProductCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup__"), "RouteCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup___"), "RouteCollectionViaDeliverypointgroup___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup____"), "RouteCollectionViaDeliverypointgroup____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_____"), "RouteCollectionViaDeliverypointgroup_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup"), "RouteCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"), "RouteCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup_"), "TerminalCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingTabletUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup__"), "TerminalCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal"), "TerminalCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"), "TerminalCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"), "UIModeCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"), "UIModeCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"), "UIModeCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup___"), "UIModeCollectionViaDeliverypointgroup___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup____"), "UIModeCollectionViaDeliverypointgroup____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMobileUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_____"), "UIModeCollectionViaDeliverypointgroup_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingUIModeId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaTerminal"), "UserCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDefaultUITabEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("DefaultUITabEntity")[0], (int)Obymobi.Data.EntityType.UIModeEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "DefaultUITabEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIModeId property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIModeId
		{
			get { return (System.Int32)GetValue((int)UIModeFieldIndex.UIModeId, true); }
			set	{ SetValue((int)UIModeFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIModeFieldIndex.CompanyId, false); }
			set	{ SetValue((int)UIModeFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UIModeFieldIndex.Name, true); }
			set	{ SetValue((int)UIModeFieldIndex.Name, value, true); }
		}

		/// <summary> The ShowServiceOptions property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowServiceOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowServiceOptions
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowServiceOptions, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowServiceOptions, value, true); }
		}

		/// <summary> The ShowRequestBill property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowRequestBill"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowRequestBill
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowRequestBill, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowRequestBill, value, true); }
		}

		/// <summary> The ShowDeliverypoint property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowDeliverypoint"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDeliverypoint
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowDeliverypoint, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowDeliverypoint, value, true); }
		}

		/// <summary> The ShowClock property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowClock"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowClock
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowClock, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowClock, value, true); }
		}

		/// <summary> The ShowBattery property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowBattery"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowBattery
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowBattery, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowBattery, value, true); }
		}

		/// <summary> The ShowBrightness property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowBrightness"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowBrightness
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowBrightness, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowBrightness, value, true); }
		}

		/// <summary> The ShowFullscreenEyecatcher property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowFullscreenEyecatcher"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowFullscreenEyecatcher
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowFullscreenEyecatcher, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowFullscreenEyecatcher, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)UIModeFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)UIModeFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)UIModeFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)UIModeFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Type property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.UIModeType Type
		{
			get { return (Obymobi.Enums.UIModeType)GetValue((int)UIModeFieldIndex.Type, true); }
			set	{ SetValue((int)UIModeFieldIndex.Type, value, true); }
		}

		/// <summary> The DefaultUITabId property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."DefaultUITabId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DefaultUITabId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIModeFieldIndex.DefaultUITabId, false); }
			set	{ SetValue((int)UIModeFieldIndex.DefaultUITabId, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIModeFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)UIModeFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The ShowFooterLogo property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."ShowFooterLogo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowFooterLogo
		{
			get { return (System.Boolean)GetValue((int)UIModeFieldIndex.ShowFooterLogo, true); }
			set	{ SetValue((int)UIModeFieldIndex.ShowFooterLogo, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIModeFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIModeFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UIMode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIMode"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIModeFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIModeFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection ClientConfigurationCollection
		{
			get	{ return GetMultiClientConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationCollection. When set to true, ClientConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationCollection
		{
			get	{ return _alwaysFetchClientConfigurationCollection; }
			set	{ _alwaysFetchClientConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationCollection already has been fetched. Setting this property to false when ClientConfigurationCollection has been fetched
		/// will clear the ClientConfigurationCollection collection well. Setting this property to true while ClientConfigurationCollection hasn't been fetched disables lazy loading for ClientConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationCollection
		{
			get { return _alreadyFetchedClientConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationCollection && !value && (_clientConfigurationCollection != null))
				{
					_clientConfigurationCollection.Clear();
				}
				_alreadyFetchedClientConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection_
		{
			get	{ return GetMultiDeliverypointgroupCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection_. When set to true, DeliverypointgroupCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection_; }
			set	{ _alwaysFetchDeliverypointgroupCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection_ already has been fetched. Setting this property to false when DeliverypointgroupCollection_ has been fetched
		/// will clear the DeliverypointgroupCollection_ collection well. Setting this property to true while DeliverypointgroupCollection_ hasn't been fetched disables lazy loading for DeliverypointgroupCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection_
		{
			get { return _alreadyFetchedDeliverypointgroupCollection_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection_ && !value && (_deliverypointgroupCollection_ != null))
				{
					_deliverypointgroupCollection_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection__
		{
			get	{ return GetMultiDeliverypointgroupCollection__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection__. When set to true, DeliverypointgroupCollection__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection__ is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection__
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection__; }
			set	{ _alwaysFetchDeliverypointgroupCollection__ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection__ already has been fetched. Setting this property to false when DeliverypointgroupCollection__ has been fetched
		/// will clear the DeliverypointgroupCollection__ collection well. Setting this property to true while DeliverypointgroupCollection__ hasn't been fetched disables lazy loading for DeliverypointgroupCollection__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection__
		{
			get { return _alreadyFetchedDeliverypointgroupCollection__;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection__ && !value && (_deliverypointgroupCollection__ != null))
				{
					_deliverypointgroupCollection__.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection__ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection
		{
			get	{ return GetMultiTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection. When set to true, TerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection
		{
			get	{ return _alwaysFetchTerminalCollection; }
			set	{ _alwaysFetchTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection already has been fetched. Setting this property to false when TerminalCollection has been fetched
		/// will clear the TerminalCollection collection well. Setting this property to true while TerminalCollection hasn't been fetched disables lazy loading for TerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection
		{
			get { return _alreadyFetchedTerminalCollection;}
			set 
			{
				if(_alreadyFetchedTerminalCollection && !value && (_terminalCollection != null))
				{
					_terminalCollection.Clear();
				}
				_alreadyFetchedTerminalCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalConfigurationCollection TerminalConfigurationCollection
		{
			get	{ return GetMultiTerminalConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalConfigurationCollection. When set to true, TerminalConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalConfigurationCollection
		{
			get	{ return _alwaysFetchTerminalConfigurationCollection; }
			set	{ _alwaysFetchTerminalConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalConfigurationCollection already has been fetched. Setting this property to false when TerminalConfigurationCollection has been fetched
		/// will clear the TerminalConfigurationCollection collection well. Setting this property to true while TerminalConfigurationCollection hasn't been fetched disables lazy loading for TerminalConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalConfigurationCollection
		{
			get { return _alreadyFetchedTerminalConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedTerminalConfigurationCollection && !value && (_terminalConfigurationCollection != null))
				{
					_terminalConfigurationCollection.Clear();
				}
				_alreadyFetchedTerminalConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection TimestampCollection
		{
			get	{ return GetMultiTimestampCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will clear the TimestampCollection collection well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value && (_timestampCollection != null))
				{
					_timestampCollection.Clear();
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIFooterItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIFooterItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIFooterItemCollection UIFooterItemCollection
		{
			get	{ return GetMultiUIFooterItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIFooterItemCollection. When set to true, UIFooterItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIFooterItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIFooterItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIFooterItemCollection
		{
			get	{ return _alwaysFetchUIFooterItemCollection; }
			set	{ _alwaysFetchUIFooterItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIFooterItemCollection already has been fetched. Setting this property to false when UIFooterItemCollection has been fetched
		/// will clear the UIFooterItemCollection collection well. Setting this property to true while UIFooterItemCollection hasn't been fetched disables lazy loading for UIFooterItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIFooterItemCollection
		{
			get { return _alreadyFetchedUIFooterItemCollection;}
			set 
			{
				if(_alreadyFetchedUIFooterItemCollection && !value && (_uIFooterItemCollection != null))
				{
					_uIFooterItemCollection.Clear();
				}
				_alreadyFetchedUIFooterItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection UITabCollection
		{
			get	{ return GetMultiUITabCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabCollection. When set to true, UITabCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUITabCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabCollection
		{
			get	{ return _alwaysFetchUITabCollection; }
			set	{ _alwaysFetchUITabCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabCollection already has been fetched. Setting this property to false when UITabCollection has been fetched
		/// will clear the UITabCollection collection well. Setting this property to true while UITabCollection hasn't been fetched disables lazy loading for UITabCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabCollection
		{
			get { return _alreadyFetchedUITabCollection;}
			set 
			{
				if(_alreadyFetchedUITabCollection && !value && (_uITabCollection != null))
				{
					_uITabCollection.Clear();
				}
				_alreadyFetchedUITabCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup. When set to true, AnnouncementCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup && !value && (_announcementCollectionViaDeliverypointgroup != null))
				{
					_announcementCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup_
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup_. When set to true, AnnouncementCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup_ collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ && !value && (_announcementCollectionViaDeliverypointgroup_ != null))
				{
					_announcementCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup__
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup__. When set to true, AnnouncementCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup__ collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ && !value && (_announcementCollectionViaDeliverypointgroup__ != null))
				{
					_announcementCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaUITab()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaUITab
		{
			get { return GetMultiCategoryCollectionViaUITab(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaUITab. When set to true, CategoryCollectionViaUITab is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaUITab is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaUITab(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaUITab
		{
			get	{ return _alwaysFetchCategoryCollectionViaUITab; }
			set	{ _alwaysFetchCategoryCollectionViaUITab = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaUITab already has been fetched. Setting this property to false when CategoryCollectionViaUITab has been fetched
		/// will clear the CategoryCollectionViaUITab collection well. Setting this property to true while CategoryCollectionViaUITab hasn't been fetched disables lazy loading for CategoryCollectionViaUITab</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaUITab
		{
			get { return _alreadyFetchedCategoryCollectionViaUITab;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaUITab && !value && (_categoryCollectionViaUITab != null))
				{
					_categoryCollectionViaUITab.Clear();
				}
				_alreadyFetchedCategoryCollectionViaUITab = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup. When set to true, CompanyCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup && !value && (_companyCollectionViaDeliverypointgroup != null))
				{
					_companyCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup_
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup_. When set to true, CompanyCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup_ collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ && !value && (_companyCollectionViaDeliverypointgroup_ != null))
				{
					_companyCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup__
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup__. When set to true, CompanyCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup__ collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ && !value && (_companyCollectionViaDeliverypointgroup__ != null))
				{
					_companyCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaTerminal
		{
			get { return GetMultiCompanyCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaTerminal. When set to true, CompanyCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaTerminal
		{
			get	{ return _alwaysFetchCompanyCollectionViaTerminal; }
			set	{ _alwaysFetchCompanyCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaTerminal already has been fetched. Setting this property to false when CompanyCollectionViaTerminal has been fetched
		/// will clear the CompanyCollectionViaTerminal collection well. Setting this property to true while CompanyCollectionViaTerminal hasn't been fetched disables lazy loading for CompanyCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaTerminal
		{
			get { return _alreadyFetchedCompanyCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaTerminal && !value && (_companyCollectionViaTerminal != null))
				{
					_companyCollectionViaTerminal.Clear();
				}
				_alreadyFetchedCompanyCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal
		{
			get { return GetMultiDeliverypointCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal. When set to true, DeliverypointCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal has been fetched
		/// will clear the DeliverypointCollectionViaTerminal collection well. Setting this property to true while DeliverypointCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal && !value && (_deliverypointCollectionViaTerminal != null))
				{
					_deliverypointCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal_. When set to true, DeliverypointCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal_ && !value && (_deliverypointCollectionViaTerminal_ != null))
				{
					_deliverypointCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal. When set to true, DeliverypointgroupCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal && !value && (_deliverypointgroupCollectionViaTerminal != null))
				{
					_deliverypointgroupCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal
		{
			get { return GetMultiDeviceCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal. When set to true, DeviceCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal already has been fetched. Setting this property to false when DeviceCollectionViaTerminal has been fetched
		/// will clear the DeviceCollectionViaTerminal collection well. Setting this property to true while DeviceCollectionViaTerminal hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal && !value && (_deviceCollectionViaTerminal != null))
				{
					_deviceCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaUITab()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaUITab
		{
			get { return GetMultiEntertainmentCollectionViaUITab(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaUITab. When set to true, EntertainmentCollectionViaUITab is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaUITab is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaUITab(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaUITab
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaUITab; }
			set	{ _alwaysFetchEntertainmentCollectionViaUITab = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaUITab already has been fetched. Setting this property to false when EntertainmentCollectionViaUITab has been fetched
		/// will clear the EntertainmentCollectionViaUITab collection well. Setting this property to true while EntertainmentCollectionViaUITab hasn't been fetched disables lazy loading for EntertainmentCollectionViaUITab</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaUITab
		{
			get { return _alreadyFetchedEntertainmentCollectionViaUITab;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaUITab && !value && (_entertainmentCollectionViaUITab != null))
				{
					_entertainmentCollectionViaUITab.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaUITab = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollectionViaTerminal
		{
			get { return GetMultiIcrtouchprintermappingCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollectionViaTerminal. When set to true, IcrtouchprintermappingCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiIcrtouchprintermappingCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollectionViaTerminal
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollectionViaTerminal; }
			set	{ _alwaysFetchIcrtouchprintermappingCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollectionViaTerminal already has been fetched. Setting this property to false when IcrtouchprintermappingCollectionViaTerminal has been fetched
		/// will clear the IcrtouchprintermappingCollectionViaTerminal collection well. Setting this property to true while IcrtouchprintermappingCollectionViaTerminal hasn't been fetched disables lazy loading for IcrtouchprintermappingCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollectionViaTerminal
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal && !value && (_icrtouchprintermappingCollectionViaTerminal != null))
				{
					_icrtouchprintermappingCollectionViaTerminal.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup_
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup_. When set to true, MenuCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup_ collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup_ && !value && (_menuCollectionViaDeliverypointgroup_ != null))
				{
					_menuCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup__
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup__. When set to true, MenuCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup__ collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup__ && !value && (_menuCollectionViaDeliverypointgroup__ != null))
				{
					_menuCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup. When set to true, MenuCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup && !value && (_menuCollectionViaDeliverypointgroup != null))
				{
					_menuCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup_. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup_ collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup_ != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup__
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup__. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup__ collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup__ != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal
		{
			get { return GetMultiProductCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal. When set to true, ProductCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal; }
			set	{ _alwaysFetchProductCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal already has been fetched. Setting this property to false when ProductCollectionViaTerminal has been fetched
		/// will clear the ProductCollectionViaTerminal collection well. Setting this property to true while ProductCollectionViaTerminal hasn't been fetched disables lazy loading for ProductCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal
		{
			get { return _alreadyFetchedProductCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal && !value && (_productCollectionViaTerminal != null))
				{
					_productCollectionViaTerminal.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal_
		{
			get { return GetMultiProductCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal_. When set to true, ProductCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal_
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal_; }
			set	{ _alwaysFetchProductCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal_ already has been fetched. Setting this property to false when ProductCollectionViaTerminal_ has been fetched
		/// will clear the ProductCollectionViaTerminal_ collection well. Setting this property to true while ProductCollectionViaTerminal_ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal_
		{
			get { return _alreadyFetchedProductCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal_ && !value && (_productCollectionViaTerminal_ != null))
				{
					_productCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal__
		{
			get { return GetMultiProductCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal__. When set to true, ProductCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal__
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal__; }
			set	{ _alwaysFetchProductCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal__ already has been fetched. Setting this property to false when ProductCollectionViaTerminal__ has been fetched
		/// will clear the ProductCollectionViaTerminal__ collection well. Setting this property to true while ProductCollectionViaTerminal__ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal__
		{
			get { return _alreadyFetchedProductCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal__ && !value && (_productCollectionViaTerminal__ != null))
				{
					_productCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal___
		{
			get { return GetMultiProductCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal___. When set to true, ProductCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal___
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal___; }
			set	{ _alwaysFetchProductCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal___ already has been fetched. Setting this property to false when ProductCollectionViaTerminal___ has been fetched
		/// will clear the ProductCollectionViaTerminal___ collection well. Setting this property to true while ProductCollectionViaTerminal___ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal___
		{
			get { return _alreadyFetchedProductCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal___ && !value && (_productCollectionViaTerminal___ != null))
				{
					_productCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup__
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup__. When set to true, RouteCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup__ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup__ && !value && (_routeCollectionViaDeliverypointgroup__ != null))
				{
					_routeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup___
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup___. When set to true, RouteCollectionViaDeliverypointgroup___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup___ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup___
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup___; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup___ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup___ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup___ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup___ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup___
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup___;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup___ && !value && (_routeCollectionViaDeliverypointgroup___ != null))
				{
					_routeCollectionViaDeliverypointgroup___.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup____
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup____. When set to true, RouteCollectionViaDeliverypointgroup____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup____ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup____
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup____; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup____ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup____ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup____ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup____ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup____
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup____;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup____ && !value && (_routeCollectionViaDeliverypointgroup____ != null))
				{
					_routeCollectionViaDeliverypointgroup____.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_____
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_____. When set to true, RouteCollectionViaDeliverypointgroup_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_____ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_____
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_____; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_____ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_____ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_____ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_____ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_____
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_____;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ && !value && (_routeCollectionViaDeliverypointgroup_____ != null))
				{
					_routeCollectionViaDeliverypointgroup_____.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup. When set to true, RouteCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup && !value && (_routeCollectionViaDeliverypointgroup != null))
				{
					_routeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_. When set to true, RouteCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_ && !value && (_routeCollectionViaDeliverypointgroup_ != null))
				{
					_routeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup_
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup_. When set to true, TerminalCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup_ collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ && !value && (_terminalCollectionViaDeliverypointgroup_ != null))
				{
					_terminalCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup__
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup__. When set to true, TerminalCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup__ collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ && !value && (_terminalCollectionViaDeliverypointgroup__ != null))
				{
					_terminalCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal
		{
			get { return GetMultiTerminalCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal. When set to true, TerminalCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal already has been fetched. Setting this property to false when TerminalCollectionViaTerminal has been fetched
		/// will clear the TerminalCollectionViaTerminal collection well. Setting this property to true while TerminalCollectionViaTerminal hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal && !value && (_terminalCollectionViaTerminal != null))
				{
					_terminalCollectionViaTerminal.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup. When set to true, TerminalCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup && !value && (_terminalCollectionViaDeliverypointgroup != null))
				{
					_terminalCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup. When set to true, UIModeCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup && !value && (_uIModeCollectionViaDeliverypointgroup != null))
				{
					_uIModeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_. When set to true, UIModeCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ && !value && (_uIModeCollectionViaDeliverypointgroup_ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup__
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup__. When set to true, UIModeCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup__ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ && !value && (_uIModeCollectionViaDeliverypointgroup__ != null))
				{
					_uIModeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup___
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup___. When set to true, UIModeCollectionViaDeliverypointgroup___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup___ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup___
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup___; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup___ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup___ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup___ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup___ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup___
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup___;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ && !value && (_uIModeCollectionViaDeliverypointgroup___ != null))
				{
					_uIModeCollectionViaDeliverypointgroup___.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup____
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup____. When set to true, UIModeCollectionViaDeliverypointgroup____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup____ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup____
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup____; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup____ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup____ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup____ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup____ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup____
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup____;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ && !value && (_uIModeCollectionViaDeliverypointgroup____ != null))
				{
					_uIModeCollectionViaDeliverypointgroup____.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_____
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_____. When set to true, UIModeCollectionViaDeliverypointgroup_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_____ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_____
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_____; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_____ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_____ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_____ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_____ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_____
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ && !value && (_uIModeCollectionViaDeliverypointgroup_____ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_____.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaTerminal
		{
			get { return GetMultiUserCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaTerminal. When set to true, UserCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaTerminal
		{
			get	{ return _alwaysFetchUserCollectionViaTerminal; }
			set	{ _alwaysFetchUserCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaTerminal already has been fetched. Setting this property to false when UserCollectionViaTerminal has been fetched
		/// will clear the UserCollectionViaTerminal collection well. Setting this property to true while UserCollectionViaTerminal hasn't been fetched disables lazy loading for UserCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaTerminal
		{
			get { return _alreadyFetchedUserCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaTerminal && !value && (_userCollectionViaTerminal != null))
				{
					_userCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUserCollectionViaTerminal = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIModeCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIModeCollection", "PointOfInterestEntity", _pointOfInterestEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set { _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UITabEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDefaultUITabEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UITabEntity DefaultUITabEntity
		{
			get	{ return GetSingleDefaultUITabEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDefaultUITabEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIModeCollection", "DefaultUITabEntity", _defaultUITabEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DefaultUITabEntity. When set to true, DefaultUITabEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DefaultUITabEntity is accessed. You can always execute a forced fetch by calling GetSingleDefaultUITabEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDefaultUITabEntity
		{
			get	{ return _alwaysFetchDefaultUITabEntity; }
			set	{ _alwaysFetchDefaultUITabEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DefaultUITabEntity already has been fetched. Setting this property to false when DefaultUITabEntity has been fetched
		/// will set DefaultUITabEntity to null as well. Setting this property to true while DefaultUITabEntity hasn't been fetched disables lazy loading for DefaultUITabEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDefaultUITabEntity
		{
			get { return _alreadyFetchedDefaultUITabEntity;}
			set 
			{
				if(_alreadyFetchedDefaultUITabEntity && !value)
				{
					this.DefaultUITabEntity = null;
				}
				_alreadyFetchedDefaultUITabEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DefaultUITabEntity is not found
		/// in the database. When set to true, DefaultUITabEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DefaultUITabEntityReturnsNewIfNotFound
		{
			get	{ return _defaultUITabEntityReturnsNewIfNotFound; }
			set { _defaultUITabEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIModeEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
