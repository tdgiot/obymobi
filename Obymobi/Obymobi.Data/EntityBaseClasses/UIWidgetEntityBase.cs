﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UIWidget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIWidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIWidgetEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemCollection	_uIScheduleItemCollection;
		private bool	_alwaysFetchUIScheduleItemCollection, _alreadyFetchedUIScheduleItemCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection	_uIWidgetAvailabilityCollection;
		private bool	_alwaysFetchUIWidgetAvailabilityCollection, _alreadyFetchedUIWidgetAvailabilityCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection	_uIWidgetLanguageCollection;
		private bool	_alwaysFetchUIWidgetLanguageCollection, _alreadyFetchedUIWidgetLanguageCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetTimerCollection	_uIWidgetTimerCollection;
		private bool	_alwaysFetchUIWidgetTimerCollection, _alreadyFetchedUIWidgetTimerCollection;
		private AdvertisementEntity _advertisementEntity;
		private bool	_alwaysFetchAdvertisementEntity, _alreadyFetchedAdvertisementEntity, _advertisementEntityReturnsNewIfNotFound;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private EntertainmentcategoryEntity _entertainmentcategoryEntity;
		private bool	_alwaysFetchEntertainmentcategoryEntity, _alreadyFetchedEntertainmentcategoryEntity, _entertainmentcategoryEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private ProductCategoryEntity _productCategoryEntity;
		private bool	_alwaysFetchProductCategoryEntity, _alreadyFetchedProductCategoryEntity, _productCategoryEntityReturnsNewIfNotFound;
		private RoomControlAreaEntity _roomControlAreaEntity;
		private bool	_alwaysFetchRoomControlAreaEntity, _alreadyFetchedRoomControlAreaEntity, _roomControlAreaEntityReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;
		private UITabEntity _uITabEntity;
		private bool	_alwaysFetchUITabEntity, _alreadyFetchedUITabEntity, _uITabEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementEntity</summary>
			public static readonly string AdvertisementEntity = "AdvertisementEntity";
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name EntertainmentcategoryEntity</summary>
			public static readonly string EntertainmentcategoryEntity = "EntertainmentcategoryEntity";
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name ProductCategoryEntity</summary>
			public static readonly string ProductCategoryEntity = "ProductCategoryEntity";
			/// <summary>Member name RoomControlAreaEntity</summary>
			public static readonly string RoomControlAreaEntity = "RoomControlAreaEntity";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name UITabEntity</summary>
			public static readonly string UITabEntity = "UITabEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name UIScheduleItemCollection</summary>
			public static readonly string UIScheduleItemCollection = "UIScheduleItemCollection";
			/// <summary>Member name UIWidgetAvailabilityCollection</summary>
			public static readonly string UIWidgetAvailabilityCollection = "UIWidgetAvailabilityCollection";
			/// <summary>Member name UIWidgetLanguageCollection</summary>
			public static readonly string UIWidgetLanguageCollection = "UIWidgetLanguageCollection";
			/// <summary>Member name UIWidgetTimerCollection</summary>
			public static readonly string UIWidgetTimerCollection = "UIWidgetTimerCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIWidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIWidgetEntityBase() :base("UIWidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		protected UIWidgetEntityBase(System.Int32 uIWidgetId):base("UIWidgetEntity")
		{
			InitClassFetch(uIWidgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIWidgetEntityBase(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse): base("UIWidgetEntity")
		{
			InitClassFetch(uIWidgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="validator">The custom validator object for this UIWidgetEntity</param>
		protected UIWidgetEntityBase(System.Int32 uIWidgetId, IValidator validator):base("UIWidgetEntity")
		{
			InitClassFetch(uIWidgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIWidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_uIScheduleItemCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemCollection)info.GetValue("_uIScheduleItemCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemCollection));
			_alwaysFetchUIScheduleItemCollection = info.GetBoolean("_alwaysFetchUIScheduleItemCollection");
			_alreadyFetchedUIScheduleItemCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemCollection");

			_uIWidgetAvailabilityCollection = (Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection)info.GetValue("_uIWidgetAvailabilityCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection));
			_alwaysFetchUIWidgetAvailabilityCollection = info.GetBoolean("_alwaysFetchUIWidgetAvailabilityCollection");
			_alreadyFetchedUIWidgetAvailabilityCollection = info.GetBoolean("_alreadyFetchedUIWidgetAvailabilityCollection");

			_uIWidgetLanguageCollection = (Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection)info.GetValue("_uIWidgetLanguageCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection));
			_alwaysFetchUIWidgetLanguageCollection = info.GetBoolean("_alwaysFetchUIWidgetLanguageCollection");
			_alreadyFetchedUIWidgetLanguageCollection = info.GetBoolean("_alreadyFetchedUIWidgetLanguageCollection");

			_uIWidgetTimerCollection = (Obymobi.Data.CollectionClasses.UIWidgetTimerCollection)info.GetValue("_uIWidgetTimerCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetTimerCollection));
			_alwaysFetchUIWidgetTimerCollection = info.GetBoolean("_alwaysFetchUIWidgetTimerCollection");
			_alreadyFetchedUIWidgetTimerCollection = info.GetBoolean("_alreadyFetchedUIWidgetTimerCollection");
			_advertisementEntity = (AdvertisementEntity)info.GetValue("_advertisementEntity", typeof(AdvertisementEntity));
			if(_advertisementEntity!=null)
			{
				_advertisementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementEntity = info.GetBoolean("_alwaysFetchAdvertisementEntity");
			_alreadyFetchedAdvertisementEntity = info.GetBoolean("_alreadyFetchedAdvertisementEntity");

			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_entertainmentcategoryEntity = (EntertainmentcategoryEntity)info.GetValue("_entertainmentcategoryEntity", typeof(EntertainmentcategoryEntity));
			if(_entertainmentcategoryEntity!=null)
			{
				_entertainmentcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentcategoryEntity = info.GetBoolean("_alwaysFetchEntertainmentcategoryEntity");
			_alreadyFetchedEntertainmentcategoryEntity = info.GetBoolean("_alreadyFetchedEntertainmentcategoryEntity");

			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_productCategoryEntity = (ProductCategoryEntity)info.GetValue("_productCategoryEntity", typeof(ProductCategoryEntity));
			if(_productCategoryEntity!=null)
			{
				_productCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_productCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchProductCategoryEntity = info.GetBoolean("_alwaysFetchProductCategoryEntity");
			_alreadyFetchedProductCategoryEntity = info.GetBoolean("_alreadyFetchedProductCategoryEntity");

			_roomControlAreaEntity = (RoomControlAreaEntity)info.GetValue("_roomControlAreaEntity", typeof(RoomControlAreaEntity));
			if(_roomControlAreaEntity!=null)
			{
				_roomControlAreaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlAreaEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlAreaEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlAreaEntity = info.GetBoolean("_alwaysFetchRoomControlAreaEntity");
			_alreadyFetchedRoomControlAreaEntity = info.GetBoolean("_alreadyFetchedRoomControlAreaEntity");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");

			_uITabEntity = (UITabEntity)info.GetValue("_uITabEntity", typeof(UITabEntity));
			if(_uITabEntity!=null)
			{
				_uITabEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uITabEntityReturnsNewIfNotFound = info.GetBoolean("_uITabEntityReturnsNewIfNotFound");
			_alwaysFetchUITabEntity = info.GetBoolean("_alwaysFetchUITabEntity");
			_alreadyFetchedUITabEntity = info.GetBoolean("_alreadyFetchedUITabEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIWidgetFieldIndex)fieldIndex)
			{
				case UIWidgetFieldIndex.UITabId:
					DesetupSyncUITabEntity(true, false);
					_alreadyFetchedUITabEntity = false;
					break;
				case UIWidgetFieldIndex.AdvertisementId:
					DesetupSyncAdvertisementEntity(true, false);
					_alreadyFetchedAdvertisementEntity = false;
					break;
				case UIWidgetFieldIndex.ProductCategoryId:
					DesetupSyncProductCategoryEntity(true, false);
					_alreadyFetchedProductCategoryEntity = false;
					break;
				case UIWidgetFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case UIWidgetFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case UIWidgetFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case UIWidgetFieldIndex.EntertainmentcategoryId:
					DesetupSyncEntertainmentcategoryEntity(true, false);
					_alreadyFetchedEntertainmentcategoryEntity = false;
					break;
				case UIWidgetFieldIndex.PageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case UIWidgetFieldIndex.RoomControlAreaId:
					DesetupSyncRoomControlAreaEntity(true, false);
					_alreadyFetchedRoomControlAreaEntity = false;
					break;
				case UIWidgetFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedUIScheduleItemCollection = (_uIScheduleItemCollection.Count > 0);
			_alreadyFetchedUIWidgetAvailabilityCollection = (_uIWidgetAvailabilityCollection.Count > 0);
			_alreadyFetchedUIWidgetLanguageCollection = (_uIWidgetLanguageCollection.Count > 0);
			_alreadyFetchedUIWidgetTimerCollection = (_uIWidgetTimerCollection.Count > 0);
			_alreadyFetchedAdvertisementEntity = (_advertisementEntity != null);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedEntertainmentcategoryEntity = (_entertainmentcategoryEntity != null);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedProductCategoryEntity = (_productCategoryEntity != null);
			_alreadyFetchedRoomControlAreaEntity = (_roomControlAreaEntity != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
			_alreadyFetchedUITabEntity = (_uITabEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementEntity":
					toReturn.Add(Relations.AdvertisementEntityUsingAdvertisementId);
					break;
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "EntertainmentcategoryEntity":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
					break;
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageId);
					break;
				case "ProductCategoryEntity":
					toReturn.Add(Relations.ProductCategoryEntityUsingProductCategoryId);
					break;
				case "RoomControlAreaEntity":
					toReturn.Add(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "UITabEntity":
					toReturn.Add(Relations.UITabEntityUsingUITabId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingUIWidgetId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingUIWidgetId);
					break;
				case "UIScheduleItemCollection":
					toReturn.Add(Relations.UIScheduleItemEntityUsingUIWidgetId);
					break;
				case "UIWidgetAvailabilityCollection":
					toReturn.Add(Relations.UIWidgetAvailabilityEntityUsingUIWidgetId);
					break;
				case "UIWidgetLanguageCollection":
					toReturn.Add(Relations.UIWidgetLanguageEntityUsingUIWidgetId);
					break;
				case "UIWidgetTimerCollection":
					toReturn.Add(Relations.UIWidgetTimerEntityUsingUIWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_uIScheduleItemCollection", (!this.MarkedForDeletion?_uIScheduleItemCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemCollection", _alwaysFetchUIScheduleItemCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemCollection", _alreadyFetchedUIScheduleItemCollection);
			info.AddValue("_uIWidgetAvailabilityCollection", (!this.MarkedForDeletion?_uIWidgetAvailabilityCollection:null));
			info.AddValue("_alwaysFetchUIWidgetAvailabilityCollection", _alwaysFetchUIWidgetAvailabilityCollection);
			info.AddValue("_alreadyFetchedUIWidgetAvailabilityCollection", _alreadyFetchedUIWidgetAvailabilityCollection);
			info.AddValue("_uIWidgetLanguageCollection", (!this.MarkedForDeletion?_uIWidgetLanguageCollection:null));
			info.AddValue("_alwaysFetchUIWidgetLanguageCollection", _alwaysFetchUIWidgetLanguageCollection);
			info.AddValue("_alreadyFetchedUIWidgetLanguageCollection", _alreadyFetchedUIWidgetLanguageCollection);
			info.AddValue("_uIWidgetTimerCollection", (!this.MarkedForDeletion?_uIWidgetTimerCollection:null));
			info.AddValue("_alwaysFetchUIWidgetTimerCollection", _alwaysFetchUIWidgetTimerCollection);
			info.AddValue("_alreadyFetchedUIWidgetTimerCollection", _alreadyFetchedUIWidgetTimerCollection);
			info.AddValue("_advertisementEntity", (!this.MarkedForDeletion?_advertisementEntity:null));
			info.AddValue("_advertisementEntityReturnsNewIfNotFound", _advertisementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementEntity", _alwaysFetchAdvertisementEntity);
			info.AddValue("_alreadyFetchedAdvertisementEntity", _alreadyFetchedAdvertisementEntity);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_entertainmentcategoryEntity", (!this.MarkedForDeletion?_entertainmentcategoryEntity:null));
			info.AddValue("_entertainmentcategoryEntityReturnsNewIfNotFound", _entertainmentcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentcategoryEntity", _alwaysFetchEntertainmentcategoryEntity);
			info.AddValue("_alreadyFetchedEntertainmentcategoryEntity", _alreadyFetchedEntertainmentcategoryEntity);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_productCategoryEntity", (!this.MarkedForDeletion?_productCategoryEntity:null));
			info.AddValue("_productCategoryEntityReturnsNewIfNotFound", _productCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductCategoryEntity", _alwaysFetchProductCategoryEntity);
			info.AddValue("_alreadyFetchedProductCategoryEntity", _alreadyFetchedProductCategoryEntity);
			info.AddValue("_roomControlAreaEntity", (!this.MarkedForDeletion?_roomControlAreaEntity:null));
			info.AddValue("_roomControlAreaEntityReturnsNewIfNotFound", _roomControlAreaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlAreaEntity", _alwaysFetchRoomControlAreaEntity);
			info.AddValue("_alreadyFetchedRoomControlAreaEntity", _alreadyFetchedRoomControlAreaEntity);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);
			info.AddValue("_uITabEntity", (!this.MarkedForDeletion?_uITabEntity:null));
			info.AddValue("_uITabEntityReturnsNewIfNotFound", _uITabEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUITabEntity", _alwaysFetchUITabEntity);
			info.AddValue("_alreadyFetchedUITabEntity", _alreadyFetchedUITabEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementEntity":
					_alreadyFetchedAdvertisementEntity = true;
					this.AdvertisementEntity = (AdvertisementEntity)entity;
					break;
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "EntertainmentcategoryEntity":
					_alreadyFetchedEntertainmentcategoryEntity = true;
					this.EntertainmentcategoryEntity = (EntertainmentcategoryEntity)entity;
					break;
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "ProductCategoryEntity":
					_alreadyFetchedProductCategoryEntity = true;
					this.ProductCategoryEntity = (ProductCategoryEntity)entity;
					break;
				case "RoomControlAreaEntity":
					_alreadyFetchedRoomControlAreaEntity = true;
					this.RoomControlAreaEntity = (RoomControlAreaEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "UITabEntity":
					_alreadyFetchedUITabEntity = true;
					this.UITabEntity = (UITabEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "UIScheduleItemCollection":
					_alreadyFetchedUIScheduleItemCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemCollection.Add((UIScheduleItemEntity)entity);
					}
					break;
				case "UIWidgetAvailabilityCollection":
					_alreadyFetchedUIWidgetAvailabilityCollection = true;
					if(entity!=null)
					{
						this.UIWidgetAvailabilityCollection.Add((UIWidgetAvailabilityEntity)entity);
					}
					break;
				case "UIWidgetLanguageCollection":
					_alreadyFetchedUIWidgetLanguageCollection = true;
					if(entity!=null)
					{
						this.UIWidgetLanguageCollection.Add((UIWidgetLanguageEntity)entity);
					}
					break;
				case "UIWidgetTimerCollection":
					_alreadyFetchedUIWidgetTimerCollection = true;
					if(entity!=null)
					{
						this.UIWidgetTimerCollection.Add((UIWidgetTimerEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					SetupSyncAdvertisementEntity(relatedEntity);
					break;
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "EntertainmentcategoryEntity":
					SetupSyncEntertainmentcategoryEntity(relatedEntity);
					break;
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "ProductCategoryEntity":
					SetupSyncProductCategoryEntity(relatedEntity);
					break;
				case "RoomControlAreaEntity":
					SetupSyncRoomControlAreaEntity(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "UITabEntity":
					SetupSyncUITabEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "UIScheduleItemCollection":
					_uIScheduleItemCollection.Add((UIScheduleItemEntity)relatedEntity);
					break;
				case "UIWidgetAvailabilityCollection":
					_uIWidgetAvailabilityCollection.Add((UIWidgetAvailabilityEntity)relatedEntity);
					break;
				case "UIWidgetLanguageCollection":
					_uIWidgetLanguageCollection.Add((UIWidgetLanguageEntity)relatedEntity);
					break;
				case "UIWidgetTimerCollection":
					_uIWidgetTimerCollection.Add((UIWidgetTimerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					DesetupSyncAdvertisementEntity(false, true);
					break;
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "EntertainmentcategoryEntity":
					DesetupSyncEntertainmentcategoryEntity(false, true);
					break;
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "ProductCategoryEntity":
					DesetupSyncProductCategoryEntity(false, true);
					break;
				case "RoomControlAreaEntity":
					DesetupSyncRoomControlAreaEntity(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "UITabEntity":
					DesetupSyncUITabEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetAvailabilityCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetAvailabilityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetLanguageCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetTimerCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetTimerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_advertisementEntity!=null)
			{
				toReturn.Add(_advertisementEntity);
			}
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_entertainmentcategoryEntity!=null)
			{
				toReturn.Add(_entertainmentcategoryEntity);
			}
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_productCategoryEntity!=null)
			{
				toReturn.Add(_productCategoryEntity);
			}
			if(_roomControlAreaEntity!=null)
			{
				toReturn.Add(_roomControlAreaEntity);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			if(_uITabEntity!=null)
			{
				toReturn.Add(_uITabEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_uIScheduleItemCollection);
			toReturn.Add(_uIWidgetAvailabilityCollection);
			toReturn.Add(_uIWidgetLanguageCollection);
			toReturn.Add(_uIWidgetTimerCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetId)
		{
			return FetchUsingPK(uIWidgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIWidgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIWidgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIWidgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIWidgetId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIWidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemCollection || forceFetch || _alwaysFetchUIScheduleItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemCollection);
				_uIScheduleItemCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_uIScheduleItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemCollection = true;
			}
			return _uIScheduleItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemCollection'. These settings will be taken into account
		/// when the property UIScheduleItemCollection is requested or GetMultiUIScheduleItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemCollection.SortClauses=sortClauses;
			_uIScheduleItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetAvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetAvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection GetMultiUIWidgetAvailabilityCollection(bool forceFetch)
		{
			return GetMultiUIWidgetAvailabilityCollection(forceFetch, _uIWidgetAvailabilityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetAvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetAvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection GetMultiUIWidgetAvailabilityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetAvailabilityCollection(forceFetch, _uIWidgetAvailabilityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetAvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection GetMultiUIWidgetAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetAvailabilityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetAvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection GetMultiUIWidgetAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetAvailabilityCollection || forceFetch || _alwaysFetchUIWidgetAvailabilityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetAvailabilityCollection);
				_uIWidgetAvailabilityCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetAvailabilityCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetAvailabilityCollection.GetMultiManyToOne(null, this, filter);
				_uIWidgetAvailabilityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetAvailabilityCollection = true;
			}
			return _uIWidgetAvailabilityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetAvailabilityCollection'. These settings will be taken into account
		/// when the property UIWidgetAvailabilityCollection is requested or GetMultiUIWidgetAvailabilityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetAvailabilityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetAvailabilityCollection.SortClauses=sortClauses;
			_uIWidgetAvailabilityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, _uIWidgetLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, _uIWidgetLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetLanguageCollection || forceFetch || _alwaysFetchUIWidgetLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetLanguageCollection);
				_uIWidgetLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetLanguageCollection.GetMultiManyToOne(null, this, filter);
				_uIWidgetLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetLanguageCollection = true;
			}
			return _uIWidgetLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetLanguageCollection'. These settings will be taken into account
		/// when the property UIWidgetLanguageCollection is requested or GetMultiUIWidgetLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetLanguageCollection.SortClauses=sortClauses;
			_uIWidgetLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetTimerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetTimerEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetTimerCollection GetMultiUIWidgetTimerCollection(bool forceFetch)
		{
			return GetMultiUIWidgetTimerCollection(forceFetch, _uIWidgetTimerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetTimerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetTimerEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetTimerCollection GetMultiUIWidgetTimerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetTimerCollection(forceFetch, _uIWidgetTimerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetTimerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetTimerCollection GetMultiUIWidgetTimerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetTimerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetTimerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetTimerCollection GetMultiUIWidgetTimerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetTimerCollection || forceFetch || _alwaysFetchUIWidgetTimerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetTimerCollection);
				_uIWidgetTimerCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetTimerCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetTimerCollection.GetMultiManyToOne(this, filter);
				_uIWidgetTimerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetTimerCollection = true;
			}
			return _uIWidgetTimerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetTimerCollection'. These settings will be taken into account
		/// when the property UIWidgetTimerCollection is requested or GetMultiUIWidgetTimerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetTimerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetTimerCollection.SortClauses=sortClauses;
			_uIWidgetTimerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public AdvertisementEntity GetSingleAdvertisementEntity()
		{
			return GetSingleAdvertisementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public virtual AdvertisementEntity GetSingleAdvertisementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementEntity || forceFetch || _alwaysFetchAdvertisementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementEntityUsingAdvertisementId);
				AdvertisementEntity newEntity = new AdvertisementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementEntity = newEntity;
				_alreadyFetchedAdvertisementEntity = fetchResult;
			}
			return _advertisementEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity()
		{
			return GetSingleEntertainmentcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentcategoryEntity || forceFetch || _alwaysFetchEntertainmentcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentcategoryEntity = newEntity;
				_alreadyFetchedEntertainmentcategoryEntity = fetchResult;
			}
			return _entertainmentcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public ProductCategoryEntity GetSingleProductCategoryEntity()
		{
			return GetSingleProductCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public virtual ProductCategoryEntity GetSingleProductCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductCategoryEntity || forceFetch || _alwaysFetchProductCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductCategoryEntityUsingProductCategoryId);
				ProductCategoryEntity newEntity = new ProductCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductCategoryEntity = newEntity;
				_alreadyFetchedProductCategoryEntity = fetchResult;
			}
			return _productCategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public RoomControlAreaEntity GetSingleRoomControlAreaEntity()
		{
			return GetSingleRoomControlAreaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public virtual RoomControlAreaEntity GetSingleRoomControlAreaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlAreaEntity || forceFetch || _alwaysFetchRoomControlAreaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
				RoomControlAreaEntity newEntity = new RoomControlAreaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlAreaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlAreaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlAreaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlAreaEntity = newEntity;
				_alreadyFetchedRoomControlAreaEntity = fetchResult;
			}
			return _roomControlAreaEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public UITabEntity GetSingleUITabEntity()
		{
			return GetSingleUITabEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public virtual UITabEntity GetSingleUITabEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUITabEntity || forceFetch || _alwaysFetchUITabEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UITabEntityUsingUITabId);
				UITabEntity newEntity = new UITabEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UITabId);
				}
				if(fetchResult)
				{
					newEntity = (UITabEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uITabEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UITabEntity = newEntity;
				_alreadyFetchedUITabEntity = fetchResult;
			}
			return _uITabEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementEntity", _advertisementEntity);
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("EntertainmentcategoryEntity", _entertainmentcategoryEntity);
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("ProductCategoryEntity", _productCategoryEntity);
			toReturn.Add("RoomControlAreaEntity", _roomControlAreaEntity);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("UITabEntity", _uITabEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("UIScheduleItemCollection", _uIScheduleItemCollection);
			toReturn.Add("UIWidgetAvailabilityCollection", _uIWidgetAvailabilityCollection);
			toReturn.Add("UIWidgetLanguageCollection", _uIWidgetLanguageCollection);
			toReturn.Add("UIWidgetTimerCollection", _uIWidgetTimerCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="validator">The validator object for this UIWidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIWidgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIWidgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "UIWidgetEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "UIWidgetEntity");

			_uIScheduleItemCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemCollection();
			_uIScheduleItemCollection.SetContainingEntityInfo(this, "UIWidgetEntity");

			_uIWidgetAvailabilityCollection = new Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection();
			_uIWidgetAvailabilityCollection.SetContainingEntityInfo(this, "UIWidgetEntity");

			_uIWidgetLanguageCollection = new Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection();
			_uIWidgetLanguageCollection.SetContainingEntityInfo(this, "UIWidgetEntity");

			_uIWidgetTimerCollection = new Obymobi.Data.CollectionClasses.UIWidgetTimerCollection();
			_uIWidgetTimerCollection.SetContainingEntityInfo(this, "UIWidgetEntity");
			_advertisementEntityReturnsNewIfNotFound = true;
			_categoryEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_entertainmentcategoryEntityReturnsNewIfNotFound = true;
			_pageEntityReturnsNewIfNotFound = true;
			_productCategoryEntityReturnsNewIfNotFound = true;
			_roomControlAreaEntityReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			_uITabEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UITabId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UITabType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Url", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsVisible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageLayoutType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _advertisementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.AdvertisementId } );		
			_advertisementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementEntity(IEntityCore relatedEntity)
		{
			if(_advertisementEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementEntity(true, true);
				_advertisementEntity = (AdvertisementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, ref _alreadyFetchedAdvertisementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.EntertainmentcategoryId } );		
			_entertainmentcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentcategoryEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentcategoryEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentcategoryEntity(true, true);
				_entertainmentcategoryEntity = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, ref _alreadyFetchedEntertainmentcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.PageEntityUsingPageIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.PageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.PageEntityUsingPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.ProductCategoryId } );		
			_productCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductCategoryEntity(IEntityCore relatedEntity)
		{
			if(_productCategoryEntity!=relatedEntity)
			{		
				DesetupSyncProductCategoryEntity(true, true);
				_productCategoryEntity = (ProductCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, ref _alreadyFetchedProductCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlAreaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.RoomControlAreaId } );		
			_roomControlAreaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlAreaEntity(IEntityCore relatedEntity)
		{
			if(_roomControlAreaEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlAreaEntity(true, true);
				_roomControlAreaEntity = (RoomControlAreaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, ref _alreadyFetchedRoomControlAreaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlAreaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uITabEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUITabEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uITabEntity, new PropertyChangedEventHandler( OnUITabEntityPropertyChanged ), "UITabEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.UITabEntityUsingUITabIdStatic, true, signalRelatedEntity, "UIWidgetCollection", resetFKFields, new int[] { (int)UIWidgetFieldIndex.UITabId } );		
			_uITabEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uITabEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUITabEntity(IEntityCore relatedEntity)
		{
			if(_uITabEntity!=relatedEntity)
			{		
				DesetupSyncUITabEntity(true, true);
				_uITabEntity = (UITabEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uITabEntity, new PropertyChangedEventHandler( OnUITabEntityPropertyChanged ), "UITabEntity", Obymobi.Data.RelationClasses.StaticUIWidgetRelations.UITabEntityUsingUITabIdStatic, true, ref _alreadyFetchedUITabEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUITabEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIWidgetFieldIndex.UIWidgetId].ForcedCurrentValueWrite(uIWidgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIWidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIWidgetRelations Relations
		{
			get	{ return new UIWidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.UIScheduleItemEntity, 0, null, null, null, "UIScheduleItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidgetAvailability' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetAvailabilityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection(), (IEntityRelation)GetRelationsForField("UIWidgetAvailabilityCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.UIWidgetAvailabilityEntity, 0, null, null, null, "UIWidgetAvailabilityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidgetLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection(), (IEntityRelation)GetRelationsForField("UIWidgetLanguageCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.UIWidgetLanguageEntity, 0, null, null, null, "UIWidgetLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidgetTimer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetTimerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetTimerCollection(), (IEntityRelation)GetRelationsForField("UIWidgetTimerCollection")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.UIWidgetTimerEntity, 0, null, null, null, "UIWidgetTimerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "EntertainmentcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.ProductCategoryEntity, 0, null, null, null, "ProductCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlArea'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlAreaEntity, 0, null, null, null, "RoomControlAreaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("UITabEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "UITabEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIWidgetId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."UIWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIWidgetId
		{
			get { return (System.Int32)GetValue((int)UIWidgetFieldIndex.UIWidgetId, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.UIWidgetId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The UITabId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."UITabId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UITabId
		{
			get { return (System.Int32)GetValue((int)UIWidgetFieldIndex.UITabId, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.UITabId, value, true); }
		}

		/// <summary> The Type property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.UIWidgetType Type
		{
			get { return (Obymobi.Enums.UIWidgetType)GetValue((int)UIWidgetFieldIndex.Type, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.Type, value, true); }
		}

		/// <summary> The Caption property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.Caption, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.Caption, value, true); }
		}

		/// <summary> The SortOrder property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)UIWidgetFieldIndex.SortOrder, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIWidgetFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIWidgetFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The AdvertisementId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."AdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.AdvertisementId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.AdvertisementId, value, true); }
		}

		/// <summary> The ProductCategoryId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."ProductCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.ProductCategoryId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.ProductCategoryId, value, true); }
		}

		/// <summary> The UITabType property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."UITabType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.UITabType> UITabType
		{
			get { return (Nullable<Obymobi.Enums.UITabType>)GetValue((int)UIWidgetFieldIndex.UITabType, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.UITabType, value, true); }
		}

		/// <summary> The Url property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."Url"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Url
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.Url, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.Url, value, true); }
		}

		/// <summary> The CategoryId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.CategoryId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The SiteId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.SiteId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.SiteId, value, true); }
		}

		/// <summary> The EntertainmentcategoryId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."EntertainmentcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.EntertainmentcategoryId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.EntertainmentcategoryId, value, true); }
		}

		/// <summary> The PageId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.PageId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.PageId, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The IsVisible property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."IsVisible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsVisible
		{
			get { return (System.Boolean)GetValue((int)UIWidgetFieldIndex.IsVisible, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.IsVisible, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The RoomControlType property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."RoomControlType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlType
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetFieldIndex.RoomControlType, false); }
			set	{ SetValue((int)UIWidgetFieldIndex.RoomControlType, value, true); }
		}

		/// <summary> The Name property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UIWidgetFieldIndex.Name, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.Name, value, true); }
		}

		/// <summary> The MessageLayoutType property of the Entity UIWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidget"."MessageLayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MessageLayoutType MessageLayoutType
		{
			get { return (Obymobi.Enums.MessageLayoutType)GetValue((int)UIWidgetFieldIndex.MessageLayoutType, true); }
			set	{ SetValue((int)UIWidgetFieldIndex.MessageLayoutType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection UIScheduleItemCollection
		{
			get	{ return GetMultiUIScheduleItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemCollection. When set to true, UIScheduleItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemCollection
		{
			get	{ return _alwaysFetchUIScheduleItemCollection; }
			set	{ _alwaysFetchUIScheduleItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemCollection already has been fetched. Setting this property to false when UIScheduleItemCollection has been fetched
		/// will clear the UIScheduleItemCollection collection well. Setting this property to true while UIScheduleItemCollection hasn't been fetched disables lazy loading for UIScheduleItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemCollection
		{
			get { return _alreadyFetchedUIScheduleItemCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemCollection && !value && (_uIScheduleItemCollection != null))
				{
					_uIScheduleItemCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetAvailabilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetAvailabilityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetAvailabilityCollection UIWidgetAvailabilityCollection
		{
			get	{ return GetMultiUIWidgetAvailabilityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetAvailabilityCollection. When set to true, UIWidgetAvailabilityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetAvailabilityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetAvailabilityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetAvailabilityCollection
		{
			get	{ return _alwaysFetchUIWidgetAvailabilityCollection; }
			set	{ _alwaysFetchUIWidgetAvailabilityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetAvailabilityCollection already has been fetched. Setting this property to false when UIWidgetAvailabilityCollection has been fetched
		/// will clear the UIWidgetAvailabilityCollection collection well. Setting this property to true while UIWidgetAvailabilityCollection hasn't been fetched disables lazy loading for UIWidgetAvailabilityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetAvailabilityCollection
		{
			get { return _alreadyFetchedUIWidgetAvailabilityCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetAvailabilityCollection && !value && (_uIWidgetAvailabilityCollection != null))
				{
					_uIWidgetAvailabilityCollection.Clear();
				}
				_alreadyFetchedUIWidgetAvailabilityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection UIWidgetLanguageCollection
		{
			get	{ return GetMultiUIWidgetLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetLanguageCollection. When set to true, UIWidgetLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetLanguageCollection
		{
			get	{ return _alwaysFetchUIWidgetLanguageCollection; }
			set	{ _alwaysFetchUIWidgetLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetLanguageCollection already has been fetched. Setting this property to false when UIWidgetLanguageCollection has been fetched
		/// will clear the UIWidgetLanguageCollection collection well. Setting this property to true while UIWidgetLanguageCollection hasn't been fetched disables lazy loading for UIWidgetLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetLanguageCollection
		{
			get { return _alreadyFetchedUIWidgetLanguageCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetLanguageCollection && !value && (_uIWidgetLanguageCollection != null))
				{
					_uIWidgetLanguageCollection.Clear();
				}
				_alreadyFetchedUIWidgetLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetTimerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetTimerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetTimerCollection UIWidgetTimerCollection
		{
			get	{ return GetMultiUIWidgetTimerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetTimerCollection. When set to true, UIWidgetTimerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetTimerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetTimerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetTimerCollection
		{
			get	{ return _alwaysFetchUIWidgetTimerCollection; }
			set	{ _alwaysFetchUIWidgetTimerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetTimerCollection already has been fetched. Setting this property to false when UIWidgetTimerCollection has been fetched
		/// will clear the UIWidgetTimerCollection collection well. Setting this property to true while UIWidgetTimerCollection hasn't been fetched disables lazy loading for UIWidgetTimerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetTimerCollection
		{
			get { return _alreadyFetchedUIWidgetTimerCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetTimerCollection && !value && (_uIWidgetTimerCollection != null))
				{
					_uIWidgetTimerCollection.Clear();
				}
				_alreadyFetchedUIWidgetTimerCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementEntity AdvertisementEntity
		{
			get	{ return GetSingleAdvertisementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "AdvertisementEntity", _advertisementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementEntity. When set to true, AdvertisementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementEntity
		{
			get	{ return _alwaysFetchAdvertisementEntity; }
			set	{ _alwaysFetchAdvertisementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementEntity already has been fetched. Setting this property to false when AdvertisementEntity has been fetched
		/// will set AdvertisementEntity to null as well. Setting this property to true while AdvertisementEntity hasn't been fetched disables lazy loading for AdvertisementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementEntity
		{
			get { return _alreadyFetchedAdvertisementEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementEntity && !value)
				{
					this.AdvertisementEntity = null;
				}
				_alreadyFetchedAdvertisementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementEntity is not found
		/// in the database. When set to true, AdvertisementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementEntityReturnsNewIfNotFound; }
			set { _advertisementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity EntertainmentcategoryEntity
		{
			get	{ return GetSingleEntertainmentcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "EntertainmentcategoryEntity", _entertainmentcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryEntity. When set to true, EntertainmentcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryEntity
		{
			get	{ return _alwaysFetchEntertainmentcategoryEntity; }
			set	{ _alwaysFetchEntertainmentcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryEntity already has been fetched. Setting this property to false when EntertainmentcategoryEntity has been fetched
		/// will set EntertainmentcategoryEntity to null as well. Setting this property to true while EntertainmentcategoryEntity hasn't been fetched disables lazy loading for EntertainmentcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryEntity
		{
			get { return _alreadyFetchedEntertainmentcategoryEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryEntity && !value)
				{
					this.EntertainmentcategoryEntity = null;
				}
				_alreadyFetchedEntertainmentcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentcategoryEntity is not found
		/// in the database. When set to true, EntertainmentcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentcategoryEntityReturnsNewIfNotFound; }
			set { _entertainmentcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductCategoryEntity ProductCategoryEntity
		{
			get	{ return GetSingleProductCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "ProductCategoryEntity", _productCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryEntity. When set to true, ProductCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleProductCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryEntity
		{
			get	{ return _alwaysFetchProductCategoryEntity; }
			set	{ _alwaysFetchProductCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryEntity already has been fetched. Setting this property to false when ProductCategoryEntity has been fetched
		/// will set ProductCategoryEntity to null as well. Setting this property to true while ProductCategoryEntity hasn't been fetched disables lazy loading for ProductCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryEntity
		{
			get { return _alreadyFetchedProductCategoryEntity;}
			set 
			{
				if(_alreadyFetchedProductCategoryEntity && !value)
				{
					this.ProductCategoryEntity = null;
				}
				_alreadyFetchedProductCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductCategoryEntity is not found
		/// in the database. When set to true, ProductCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _productCategoryEntityReturnsNewIfNotFound; }
			set { _productCategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlAreaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlAreaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlAreaEntity RoomControlAreaEntity
		{
			get	{ return GetSingleRoomControlAreaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlAreaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "RoomControlAreaEntity", _roomControlAreaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaEntity. When set to true, RoomControlAreaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlAreaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaEntity
		{
			get	{ return _alwaysFetchRoomControlAreaEntity; }
			set	{ _alwaysFetchRoomControlAreaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaEntity already has been fetched. Setting this property to false when RoomControlAreaEntity has been fetched
		/// will set RoomControlAreaEntity to null as well. Setting this property to true while RoomControlAreaEntity hasn't been fetched disables lazy loading for RoomControlAreaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaEntity
		{
			get { return _alreadyFetchedRoomControlAreaEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaEntity && !value)
				{
					this.RoomControlAreaEntity = null;
				}
				_alreadyFetchedRoomControlAreaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlAreaEntity is not found
		/// in the database. When set to true, RoomControlAreaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlAreaEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlAreaEntityReturnsNewIfNotFound; }
			set { _roomControlAreaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UITabEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUITabEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UITabEntity UITabEntity
		{
			get	{ return GetSingleUITabEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUITabEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetCollection", "UITabEntity", _uITabEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UITabEntity. When set to true, UITabEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabEntity is accessed. You can always execute a forced fetch by calling GetSingleUITabEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabEntity
		{
			get	{ return _alwaysFetchUITabEntity; }
			set	{ _alwaysFetchUITabEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabEntity already has been fetched. Setting this property to false when UITabEntity has been fetched
		/// will set UITabEntity to null as well. Setting this property to true while UITabEntity hasn't been fetched disables lazy loading for UITabEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabEntity
		{
			get { return _alreadyFetchedUITabEntity;}
			set 
			{
				if(_alreadyFetchedUITabEntity && !value)
				{
					this.UITabEntity = null;
				}
				_alreadyFetchedUITabEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UITabEntity is not found
		/// in the database. When set to true, UITabEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UITabEntityReturnsNewIfNotFound
		{
			get	{ return _uITabEntityReturnsNewIfNotFound; }
			set { _uITabEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIWidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
