﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'NavigationMenuItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class NavigationMenuItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "NavigationMenuItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private ActionEntity _actionEntity;
		private bool	_alwaysFetchActionEntity, _alreadyFetchedActionEntity, _actionEntityReturnsNewIfNotFound;
		private NavigationMenuEntity _navigationMenuEntity;
		private bool	_alwaysFetchNavigationMenuEntity, _alreadyFetchedNavigationMenuEntity, _navigationMenuEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ActionEntity</summary>
			public static readonly string ActionEntity = "ActionEntity";
			/// <summary>Member name NavigationMenuEntity</summary>
			public static readonly string NavigationMenuEntity = "NavigationMenuEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NavigationMenuItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected NavigationMenuItemEntityBase() :base("NavigationMenuItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		protected NavigationMenuItemEntityBase(System.Int32 navigationMenuItemId):base("NavigationMenuItemEntity")
		{
			InitClassFetch(navigationMenuItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected NavigationMenuItemEntityBase(System.Int32 navigationMenuItemId, IPrefetchPath prefetchPathToUse): base("NavigationMenuItemEntity")
		{
			InitClassFetch(navigationMenuItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="validator">The custom validator object for this NavigationMenuItemEntity</param>
		protected NavigationMenuItemEntityBase(System.Int32 navigationMenuItemId, IValidator validator):base("NavigationMenuItemEntity")
		{
			InitClassFetch(navigationMenuItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NavigationMenuItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");
			_actionEntity = (ActionEntity)info.GetValue("_actionEntity", typeof(ActionEntity));
			if(_actionEntity!=null)
			{
				_actionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionEntityReturnsNewIfNotFound = info.GetBoolean("_actionEntityReturnsNewIfNotFound");
			_alwaysFetchActionEntity = info.GetBoolean("_alwaysFetchActionEntity");
			_alreadyFetchedActionEntity = info.GetBoolean("_alreadyFetchedActionEntity");

			_navigationMenuEntity = (NavigationMenuEntity)info.GetValue("_navigationMenuEntity", typeof(NavigationMenuEntity));
			if(_navigationMenuEntity!=null)
			{
				_navigationMenuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_navigationMenuEntityReturnsNewIfNotFound = info.GetBoolean("_navigationMenuEntityReturnsNewIfNotFound");
			_alwaysFetchNavigationMenuEntity = info.GetBoolean("_alwaysFetchNavigationMenuEntity");
			_alreadyFetchedNavigationMenuEntity = info.GetBoolean("_alreadyFetchedNavigationMenuEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NavigationMenuItemFieldIndex)fieldIndex)
			{
				case NavigationMenuItemFieldIndex.NavigationMenuId:
					DesetupSyncNavigationMenuEntity(true, false);
					_alreadyFetchedNavigationMenuEntity = false;
					break;
				case NavigationMenuItemFieldIndex.ActionId:
					DesetupSyncActionEntity(true, false);
					_alreadyFetchedActionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedActionEntity = (_actionEntity != null);
			_alreadyFetchedNavigationMenuEntity = (_navigationMenuEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ActionEntity":
					toReturn.Add(Relations.ActionEntityUsingActionId);
					break;
				case "NavigationMenuEntity":
					toReturn.Add(Relations.NavigationMenuEntityUsingNavigationMenuId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingNavigationMenuItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_actionEntity", (!this.MarkedForDeletion?_actionEntity:null));
			info.AddValue("_actionEntityReturnsNewIfNotFound", _actionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionEntity", _alwaysFetchActionEntity);
			info.AddValue("_alreadyFetchedActionEntity", _alreadyFetchedActionEntity);
			info.AddValue("_navigationMenuEntity", (!this.MarkedForDeletion?_navigationMenuEntity:null));
			info.AddValue("_navigationMenuEntityReturnsNewIfNotFound", _navigationMenuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNavigationMenuEntity", _alwaysFetchNavigationMenuEntity);
			info.AddValue("_alreadyFetchedNavigationMenuEntity", _alreadyFetchedNavigationMenuEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ActionEntity":
					_alreadyFetchedActionEntity = true;
					this.ActionEntity = (ActionEntity)entity;
					break;
				case "NavigationMenuEntity":
					_alreadyFetchedNavigationMenuEntity = true;
					this.NavigationMenuEntity = (NavigationMenuEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ActionEntity":
					SetupSyncActionEntity(relatedEntity);
					break;
				case "NavigationMenuEntity":
					SetupSyncNavigationMenuEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ActionEntity":
					DesetupSyncActionEntity(false, true);
					break;
				case "NavigationMenuEntity":
					DesetupSyncNavigationMenuEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_actionEntity!=null)
			{
				toReturn.Add(_actionEntity);
			}
			if(_navigationMenuEntity!=null)
			{
				toReturn.Add(_navigationMenuEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuItemId)
		{
			return FetchUsingPK(navigationMenuItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(navigationMenuItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(navigationMenuItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(navigationMenuItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NavigationMenuItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NavigationMenuItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ActionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ActionEntity' which is related to this entity.</returns>
		public ActionEntity GetSingleActionEntity()
		{
			return GetSingleActionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ActionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ActionEntity' which is related to this entity.</returns>
		public virtual ActionEntity GetSingleActionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionEntity || forceFetch || _alwaysFetchActionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ActionEntityUsingActionId);
				ActionEntity newEntity = new ActionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ActionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionEntity = newEntity;
				_alreadyFetchedActionEntity = fetchResult;
			}
			return _actionEntity;
		}


		/// <summary> Retrieves the related entity of type 'NavigationMenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NavigationMenuEntity' which is related to this entity.</returns>
		public NavigationMenuEntity GetSingleNavigationMenuEntity()
		{
			return GetSingleNavigationMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'NavigationMenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NavigationMenuEntity' which is related to this entity.</returns>
		public virtual NavigationMenuEntity GetSingleNavigationMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedNavigationMenuEntity || forceFetch || _alwaysFetchNavigationMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NavigationMenuEntityUsingNavigationMenuId);
				NavigationMenuEntity newEntity = new NavigationMenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NavigationMenuId);
				}
				if(fetchResult)
				{
					newEntity = (NavigationMenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_navigationMenuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NavigationMenuEntity = newEntity;
				_alreadyFetchedNavigationMenuEntity = fetchResult;
			}
			return _navigationMenuEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ActionEntity", _actionEntity);
			toReturn.Add("NavigationMenuEntity", _navigationMenuEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="validator">The validator object for this NavigationMenuItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 navigationMenuItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(navigationMenuItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "NavigationMenuItemEntity");
			_actionEntityReturnsNewIfNotFound = true;
			_navigationMenuEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationMenuItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationMenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _actionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionEntity, new PropertyChangedEventHandler( OnActionEntityPropertyChanged ), "ActionEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuItemRelations.ActionEntityUsingActionIdStatic, true, signalRelatedEntity, "NavigationMenuItemCollection", resetFKFields, new int[] { (int)NavigationMenuItemFieldIndex.ActionId } );		
			_actionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionEntity(IEntityCore relatedEntity)
		{
			if(_actionEntity!=relatedEntity)
			{		
				DesetupSyncActionEntity(true, true);
				_actionEntity = (ActionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionEntity, new PropertyChangedEventHandler( OnActionEntityPropertyChanged ), "ActionEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuItemRelations.ActionEntityUsingActionIdStatic, true, ref _alreadyFetchedActionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _navigationMenuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNavigationMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _navigationMenuEntity, new PropertyChangedEventHandler( OnNavigationMenuEntityPropertyChanged ), "NavigationMenuEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuItemRelations.NavigationMenuEntityUsingNavigationMenuIdStatic, true, signalRelatedEntity, "NavigationMenuItemCollection", resetFKFields, new int[] { (int)NavigationMenuItemFieldIndex.NavigationMenuId } );		
			_navigationMenuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _navigationMenuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNavigationMenuEntity(IEntityCore relatedEntity)
		{
			if(_navigationMenuEntity!=relatedEntity)
			{		
				DesetupSyncNavigationMenuEntity(true, true);
				_navigationMenuEntity = (NavigationMenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _navigationMenuEntity, new PropertyChangedEventHandler( OnNavigationMenuEntityPropertyChanged ), "NavigationMenuEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuItemRelations.NavigationMenuEntityUsingNavigationMenuIdStatic, true, ref _alreadyFetchedNavigationMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNavigationMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="navigationMenuItemId">PK value for NavigationMenuItem which data should be fetched into this NavigationMenuItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 navigationMenuItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NavigationMenuItemFieldIndex.NavigationMenuItemId].ForcedCurrentValueWrite(navigationMenuItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNavigationMenuItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NavigationMenuItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NavigationMenuItemRelations Relations
		{
			get	{ return new NavigationMenuItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Action'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionCollection(), (IEntityRelation)GetRelationsForField("ActionEntity")[0], (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, (int)Obymobi.Data.EntityType.ActionEntity, 0, null, null, null, "ActionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuEntity")[0], (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, (int)Obymobi.Data.EntityType.NavigationMenuEntity, 0, null, null, null, "NavigationMenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The NavigationMenuItemId property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."NavigationMenuItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 NavigationMenuItemId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuItemFieldIndex.NavigationMenuItemId, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.NavigationMenuItemId, value, true); }
		}

		/// <summary> The NavigationMenuId property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."NavigationMenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NavigationMenuId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuItemFieldIndex.NavigationMenuId, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.NavigationMenuId, value, true); }
		}

		/// <summary> The ActionId property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."ActionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuItemFieldIndex.ActionId, false); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.ActionId, value, true); }
		}

		/// <summary> The Text property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."Text"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)NavigationMenuItemFieldIndex.Text, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.Text, value, true); }
		}

		/// <summary> The SortOrder property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)NavigationMenuItemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuItemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)NavigationMenuItemFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NavigationMenuItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity NavigationMenuItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)NavigationMenuItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ActionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ActionEntity ActionEntity
		{
			get	{ return GetSingleActionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NavigationMenuItemCollection", "ActionEntity", _actionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionEntity. When set to true, ActionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionEntity is accessed. You can always execute a forced fetch by calling GetSingleActionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionEntity
		{
			get	{ return _alwaysFetchActionEntity; }
			set	{ _alwaysFetchActionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionEntity already has been fetched. Setting this property to false when ActionEntity has been fetched
		/// will set ActionEntity to null as well. Setting this property to true while ActionEntity hasn't been fetched disables lazy loading for ActionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionEntity
		{
			get { return _alreadyFetchedActionEntity;}
			set 
			{
				if(_alreadyFetchedActionEntity && !value)
				{
					this.ActionEntity = null;
				}
				_alreadyFetchedActionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionEntity is not found
		/// in the database. When set to true, ActionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionEntityReturnsNewIfNotFound
		{
			get	{ return _actionEntityReturnsNewIfNotFound; }
			set { _actionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'NavigationMenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNavigationMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual NavigationMenuEntity NavigationMenuEntity
		{
			get	{ return GetSingleNavigationMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNavigationMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NavigationMenuItemCollection", "NavigationMenuEntity", _navigationMenuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuEntity. When set to true, NavigationMenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuEntity is accessed. You can always execute a forced fetch by calling GetSingleNavigationMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuEntity
		{
			get	{ return _alwaysFetchNavigationMenuEntity; }
			set	{ _alwaysFetchNavigationMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuEntity already has been fetched. Setting this property to false when NavigationMenuEntity has been fetched
		/// will set NavigationMenuEntity to null as well. Setting this property to true while NavigationMenuEntity hasn't been fetched disables lazy loading for NavigationMenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuEntity
		{
			get { return _alreadyFetchedNavigationMenuEntity;}
			set 
			{
				if(_alreadyFetchedNavigationMenuEntity && !value)
				{
					this.NavigationMenuEntity = null;
				}
				_alreadyFetchedNavigationMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NavigationMenuEntity is not found
		/// in the database. When set to true, NavigationMenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool NavigationMenuEntityReturnsNewIfNotFound
		{
			get	{ return _navigationMenuEntityReturnsNewIfNotFound; }
			set { _navigationMenuEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.NavigationMenuItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
