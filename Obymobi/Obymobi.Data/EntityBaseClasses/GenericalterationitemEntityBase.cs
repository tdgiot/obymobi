﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Genericalterationitem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GenericalterationitemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GenericalterationitemEntity"; }
		}
	
		#region Class Member Declarations
		private GenericalterationEntity _genericalterationEntity;
		private bool	_alwaysFetchGenericalterationEntity, _alreadyFetchedGenericalterationEntity, _genericalterationEntityReturnsNewIfNotFound;
		private GenericalterationoptionEntity _genericalterationoptionEntity;
		private bool	_alwaysFetchGenericalterationoptionEntity, _alreadyFetchedGenericalterationoptionEntity, _genericalterationoptionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name GenericalterationEntity</summary>
			public static readonly string GenericalterationEntity = "GenericalterationEntity";
			/// <summary>Member name GenericalterationoptionEntity</summary>
			public static readonly string GenericalterationoptionEntity = "GenericalterationoptionEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GenericalterationitemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GenericalterationitemEntityBase() :base("GenericalterationitemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		protected GenericalterationitemEntityBase(System.Int32 genericalterationitemId):base("GenericalterationitemEntity")
		{
			InitClassFetch(genericalterationitemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GenericalterationitemEntityBase(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse): base("GenericalterationitemEntity")
		{
			InitClassFetch(genericalterationitemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="validator">The custom validator object for this GenericalterationitemEntity</param>
		protected GenericalterationitemEntityBase(System.Int32 genericalterationitemId, IValidator validator):base("GenericalterationitemEntity")
		{
			InitClassFetch(genericalterationitemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericalterationitemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_genericalterationEntity = (GenericalterationEntity)info.GetValue("_genericalterationEntity", typeof(GenericalterationEntity));
			if(_genericalterationEntity!=null)
			{
				_genericalterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericalterationEntityReturnsNewIfNotFound = info.GetBoolean("_genericalterationEntityReturnsNewIfNotFound");
			_alwaysFetchGenericalterationEntity = info.GetBoolean("_alwaysFetchGenericalterationEntity");
			_alreadyFetchedGenericalterationEntity = info.GetBoolean("_alreadyFetchedGenericalterationEntity");

			_genericalterationoptionEntity = (GenericalterationoptionEntity)info.GetValue("_genericalterationoptionEntity", typeof(GenericalterationoptionEntity));
			if(_genericalterationoptionEntity!=null)
			{
				_genericalterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericalterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_genericalterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchGenericalterationoptionEntity = info.GetBoolean("_alwaysFetchGenericalterationoptionEntity");
			_alreadyFetchedGenericalterationoptionEntity = info.GetBoolean("_alreadyFetchedGenericalterationoptionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GenericalterationitemFieldIndex)fieldIndex)
			{
				case GenericalterationitemFieldIndex.GenericalterationId:
					DesetupSyncGenericalterationEntity(true, false);
					_alreadyFetchedGenericalterationEntity = false;
					break;
				case GenericalterationitemFieldIndex.GenericalterationoptionId:
					DesetupSyncGenericalterationoptionEntity(true, false);
					_alreadyFetchedGenericalterationoptionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedGenericalterationEntity = (_genericalterationEntity != null);
			_alreadyFetchedGenericalterationoptionEntity = (_genericalterationoptionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "GenericalterationEntity":
					toReturn.Add(Relations.GenericalterationEntityUsingGenericalterationId);
					break;
				case "GenericalterationoptionEntity":
					toReturn.Add(Relations.GenericalterationoptionEntityUsingGenericalterationoptionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_genericalterationEntity", (!this.MarkedForDeletion?_genericalterationEntity:null));
			info.AddValue("_genericalterationEntityReturnsNewIfNotFound", _genericalterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericalterationEntity", _alwaysFetchGenericalterationEntity);
			info.AddValue("_alreadyFetchedGenericalterationEntity", _alreadyFetchedGenericalterationEntity);
			info.AddValue("_genericalterationoptionEntity", (!this.MarkedForDeletion?_genericalterationoptionEntity:null));
			info.AddValue("_genericalterationoptionEntityReturnsNewIfNotFound", _genericalterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericalterationoptionEntity", _alwaysFetchGenericalterationoptionEntity);
			info.AddValue("_alreadyFetchedGenericalterationoptionEntity", _alreadyFetchedGenericalterationoptionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "GenericalterationEntity":
					_alreadyFetchedGenericalterationEntity = true;
					this.GenericalterationEntity = (GenericalterationEntity)entity;
					break;
				case "GenericalterationoptionEntity":
					_alreadyFetchedGenericalterationoptionEntity = true;
					this.GenericalterationoptionEntity = (GenericalterationoptionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "GenericalterationEntity":
					SetupSyncGenericalterationEntity(relatedEntity);
					break;
				case "GenericalterationoptionEntity":
					SetupSyncGenericalterationoptionEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "GenericalterationEntity":
					DesetupSyncGenericalterationEntity(false, true);
					break;
				case "GenericalterationoptionEntity":
					DesetupSyncGenericalterationoptionEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_genericalterationEntity!=null)
			{
				toReturn.Add(_genericalterationEntity);
			}
			if(_genericalterationoptionEntity!=null)
			{
				toReturn.Add(_genericalterationoptionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationitemId)
		{
			return FetchUsingPK(genericalterationitemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(genericalterationitemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(genericalterationitemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(genericalterationitemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GenericalterationitemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GenericalterationitemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'GenericalterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericalterationEntity' which is related to this entity.</returns>
		public GenericalterationEntity GetSingleGenericalterationEntity()
		{
			return GetSingleGenericalterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericalterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericalterationEntity' which is related to this entity.</returns>
		public virtual GenericalterationEntity GetSingleGenericalterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericalterationEntity || forceFetch || _alwaysFetchGenericalterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericalterationEntityUsingGenericalterationId);
				GenericalterationEntity newEntity = new GenericalterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericalterationId);
				}
				if(fetchResult)
				{
					newEntity = (GenericalterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericalterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericalterationEntity = newEntity;
				_alreadyFetchedGenericalterationEntity = fetchResult;
			}
			return _genericalterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericalterationoptionEntity' which is related to this entity.</returns>
		public GenericalterationoptionEntity GetSingleGenericalterationoptionEntity()
		{
			return GetSingleGenericalterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericalterationoptionEntity' which is related to this entity.</returns>
		public virtual GenericalterationoptionEntity GetSingleGenericalterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericalterationoptionEntity || forceFetch || _alwaysFetchGenericalterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericalterationoptionEntityUsingGenericalterationoptionId);
				GenericalterationoptionEntity newEntity = new GenericalterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericalterationoptionId);
				}
				if(fetchResult)
				{
					newEntity = (GenericalterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericalterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericalterationoptionEntity = newEntity;
				_alreadyFetchedGenericalterationoptionEntity = fetchResult;
			}
			return _genericalterationoptionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("GenericalterationEntity", _genericalterationEntity);
			toReturn.Add("GenericalterationoptionEntity", _genericalterationoptionEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="validator">The validator object for this GenericalterationitemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 genericalterationitemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(genericalterationitemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_genericalterationEntityReturnsNewIfNotFound = true;
			_genericalterationoptionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SelectedOnDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _genericalterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericalterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericalterationEntity, new PropertyChangedEventHandler( OnGenericalterationEntityPropertyChanged ), "GenericalterationEntity", Obymobi.Data.RelationClasses.StaticGenericalterationitemRelations.GenericalterationEntityUsingGenericalterationIdStatic, true, signalRelatedEntity, "GenericalterationitemCollection", resetFKFields, new int[] { (int)GenericalterationitemFieldIndex.GenericalterationId } );		
			_genericalterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericalterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericalterationEntity(IEntityCore relatedEntity)
		{
			if(_genericalterationEntity!=relatedEntity)
			{		
				DesetupSyncGenericalterationEntity(true, true);
				_genericalterationEntity = (GenericalterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericalterationEntity, new PropertyChangedEventHandler( OnGenericalterationEntityPropertyChanged ), "GenericalterationEntity", Obymobi.Data.RelationClasses.StaticGenericalterationitemRelations.GenericalterationEntityUsingGenericalterationIdStatic, true, ref _alreadyFetchedGenericalterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericalterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericalterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericalterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericalterationoptionEntity, new PropertyChangedEventHandler( OnGenericalterationoptionEntityPropertyChanged ), "GenericalterationoptionEntity", Obymobi.Data.RelationClasses.StaticGenericalterationitemRelations.GenericalterationoptionEntityUsingGenericalterationoptionIdStatic, true, signalRelatedEntity, "GenericalterationitemCollection", resetFKFields, new int[] { (int)GenericalterationitemFieldIndex.GenericalterationoptionId } );		
			_genericalterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericalterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericalterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_genericalterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncGenericalterationoptionEntity(true, true);
				_genericalterationoptionEntity = (GenericalterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericalterationoptionEntity, new PropertyChangedEventHandler( OnGenericalterationoptionEntityPropertyChanged ), "GenericalterationoptionEntity", Obymobi.Data.RelationClasses.StaticGenericalterationitemRelations.GenericalterationoptionEntityUsingGenericalterationoptionIdStatic, true, ref _alreadyFetchedGenericalterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericalterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GenericalterationitemFieldIndex.GenericalterationitemId].ForcedCurrentValueWrite(genericalterationitemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericalterationitemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GenericalterationitemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GenericalterationitemRelations Relations
		{
			get	{ return new GenericalterationitemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), (IEntityRelation)GetRelationsForField("GenericalterationEntity")[0], (int)Obymobi.Data.EntityType.GenericalterationitemEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, null, "GenericalterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection(), (IEntityRelation)GetRelationsForField("GenericalterationoptionEntity")[0], (int)Obymobi.Data.EntityType.GenericalterationitemEntity, (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, 0, null, null, null, "GenericalterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GenericalterationitemId property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."GenericalterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GenericalterationitemId
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.GenericalterationitemId, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.GenericalterationitemId, value, true); }
		}

		/// <summary> The GenericalterationId property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."GenericalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GenericalterationId
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.GenericalterationId, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.GenericalterationId, value, true); }
		}

		/// <summary> The GenericalterationoptionId property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."GenericalterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GenericalterationoptionId
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.GenericalterationoptionId, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.GenericalterationoptionId, value, true); }
		}

		/// <summary> The SelectedOnDefault property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."SelectedOnDefault"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SelectedOnDefault
		{
			get { return (System.Boolean)GetValue((int)GenericalterationitemFieldIndex.SelectedOnDefault, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.SelectedOnDefault, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationitemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Genericalterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationitem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GenericalterationitemFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'GenericalterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericalterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericalterationEntity GenericalterationEntity
		{
			get	{ return GetSingleGenericalterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericalterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericalterationitemCollection", "GenericalterationEntity", _genericalterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationEntity. When set to true, GenericalterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericalterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationEntity
		{
			get	{ return _alwaysFetchGenericalterationEntity; }
			set	{ _alwaysFetchGenericalterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationEntity already has been fetched. Setting this property to false when GenericalterationEntity has been fetched
		/// will set GenericalterationEntity to null as well. Setting this property to true while GenericalterationEntity hasn't been fetched disables lazy loading for GenericalterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationEntity
		{
			get { return _alreadyFetchedGenericalterationEntity;}
			set 
			{
				if(_alreadyFetchedGenericalterationEntity && !value)
				{
					this.GenericalterationEntity = null;
				}
				_alreadyFetchedGenericalterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericalterationEntity is not found
		/// in the database. When set to true, GenericalterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericalterationEntityReturnsNewIfNotFound
		{
			get	{ return _genericalterationEntityReturnsNewIfNotFound; }
			set { _genericalterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericalterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericalterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericalterationoptionEntity GenericalterationoptionEntity
		{
			get	{ return GetSingleGenericalterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericalterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericalterationitemCollection", "GenericalterationoptionEntity", _genericalterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationoptionEntity. When set to true, GenericalterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericalterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationoptionEntity
		{
			get	{ return _alwaysFetchGenericalterationoptionEntity; }
			set	{ _alwaysFetchGenericalterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationoptionEntity already has been fetched. Setting this property to false when GenericalterationoptionEntity has been fetched
		/// will set GenericalterationoptionEntity to null as well. Setting this property to true while GenericalterationoptionEntity hasn't been fetched disables lazy loading for GenericalterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationoptionEntity
		{
			get { return _alreadyFetchedGenericalterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedGenericalterationoptionEntity && !value)
				{
					this.GenericalterationoptionEntity = null;
				}
				_alreadyFetchedGenericalterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericalterationoptionEntity is not found
		/// in the database. When set to true, GenericalterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericalterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _genericalterationoptionEntityReturnsNewIfNotFound; }
			set { _genericalterationoptionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GenericalterationitemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
