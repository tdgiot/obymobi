﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SiteTemplateLanguage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SiteTemplateLanguageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SiteTemplateLanguageEntity"; }
		}
	
		#region Class Member Declarations
		private LanguageEntity _languageEntity;
		private bool	_alwaysFetchLanguageEntity, _alreadyFetchedLanguageEntity, _languageEntityReturnsNewIfNotFound;
		private SiteTemplateEntity _siteTemplateEntity;
		private bool	_alwaysFetchSiteTemplateEntity, _alreadyFetchedSiteTemplateEntity, _siteTemplateEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name LanguageEntity</summary>
			public static readonly string LanguageEntity = "LanguageEntity";
			/// <summary>Member name SiteTemplateEntity</summary>
			public static readonly string SiteTemplateEntity = "SiteTemplateEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SiteTemplateLanguageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SiteTemplateLanguageEntityBase() :base("SiteTemplateLanguageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		protected SiteTemplateLanguageEntityBase(System.Int32 siteTemplateLanguageId):base("SiteTemplateLanguageEntity")
		{
			InitClassFetch(siteTemplateLanguageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SiteTemplateLanguageEntityBase(System.Int32 siteTemplateLanguageId, IPrefetchPath prefetchPathToUse): base("SiteTemplateLanguageEntity")
		{
			InitClassFetch(siteTemplateLanguageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="validator">The custom validator object for this SiteTemplateLanguageEntity</param>
		protected SiteTemplateLanguageEntityBase(System.Int32 siteTemplateLanguageId, IValidator validator):base("SiteTemplateLanguageEntity")
		{
			InitClassFetch(siteTemplateLanguageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteTemplateLanguageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_languageEntity = (LanguageEntity)info.GetValue("_languageEntity", typeof(LanguageEntity));
			if(_languageEntity!=null)
			{
				_languageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageEntityReturnsNewIfNotFound = info.GetBoolean("_languageEntityReturnsNewIfNotFound");
			_alwaysFetchLanguageEntity = info.GetBoolean("_alwaysFetchLanguageEntity");
			_alreadyFetchedLanguageEntity = info.GetBoolean("_alreadyFetchedLanguageEntity");

			_siteTemplateEntity = (SiteTemplateEntity)info.GetValue("_siteTemplateEntity", typeof(SiteTemplateEntity));
			if(_siteTemplateEntity!=null)
			{
				_siteTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_siteTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchSiteTemplateEntity = info.GetBoolean("_alwaysFetchSiteTemplateEntity");
			_alreadyFetchedSiteTemplateEntity = info.GetBoolean("_alreadyFetchedSiteTemplateEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SiteTemplateLanguageFieldIndex)fieldIndex)
			{
				case SiteTemplateLanguageFieldIndex.SiteTemplateId:
					DesetupSyncSiteTemplateEntity(true, false);
					_alreadyFetchedSiteTemplateEntity = false;
					break;
				case SiteTemplateLanguageFieldIndex.LanguageId:
					DesetupSyncLanguageEntity(true, false);
					_alreadyFetchedLanguageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLanguageEntity = (_languageEntity != null);
			_alreadyFetchedSiteTemplateEntity = (_siteTemplateEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "LanguageEntity":
					toReturn.Add(Relations.LanguageEntityUsingLanguageId);
					break;
				case "SiteTemplateEntity":
					toReturn.Add(Relations.SiteTemplateEntityUsingSiteTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_languageEntity", (!this.MarkedForDeletion?_languageEntity:null));
			info.AddValue("_languageEntityReturnsNewIfNotFound", _languageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguageEntity", _alwaysFetchLanguageEntity);
			info.AddValue("_alreadyFetchedLanguageEntity", _alreadyFetchedLanguageEntity);
			info.AddValue("_siteTemplateEntity", (!this.MarkedForDeletion?_siteTemplateEntity:null));
			info.AddValue("_siteTemplateEntityReturnsNewIfNotFound", _siteTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteTemplateEntity", _alwaysFetchSiteTemplateEntity);
			info.AddValue("_alreadyFetchedSiteTemplateEntity", _alreadyFetchedSiteTemplateEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "LanguageEntity":
					_alreadyFetchedLanguageEntity = true;
					this.LanguageEntity = (LanguageEntity)entity;
					break;
				case "SiteTemplateEntity":
					_alreadyFetchedSiteTemplateEntity = true;
					this.SiteTemplateEntity = (SiteTemplateEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "LanguageEntity":
					SetupSyncLanguageEntity(relatedEntity);
					break;
				case "SiteTemplateEntity":
					SetupSyncSiteTemplateEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "LanguageEntity":
					DesetupSyncLanguageEntity(false, true);
					break;
				case "SiteTemplateEntity":
					DesetupSyncSiteTemplateEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_languageEntity!=null)
			{
				toReturn.Add(_languageEntity);
			}
			if(_siteTemplateEntity!=null)
			{
				toReturn.Add(_siteTemplateEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateLanguageId)
		{
			return FetchUsingPK(siteTemplateLanguageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateLanguageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(siteTemplateLanguageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(siteTemplateLanguageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(siteTemplateLanguageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SiteTemplateLanguageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SiteTemplateLanguageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguageEntity()
		{
			return GetSingleLanguageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguageEntity || forceFetch || _alwaysFetchLanguageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageId);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageId);
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LanguageEntity = newEntity;
				_alreadyFetchedLanguageEntity = fetchResult;
			}
			return _languageEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public SiteTemplateEntity GetSingleSiteTemplateEntity()
		{
			return GetSingleSiteTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public virtual SiteTemplateEntity GetSingleSiteTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteTemplateEntity || forceFetch || _alwaysFetchSiteTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteTemplateEntityUsingSiteTemplateId);
				SiteTemplateEntity newEntity = new SiteTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteTemplateId);
				}
				if(fetchResult)
				{
					newEntity = (SiteTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteTemplateEntity = newEntity;
				_alreadyFetchedSiteTemplateEntity = fetchResult;
			}
			return _siteTemplateEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("LanguageEntity", _languageEntity);
			toReturn.Add("SiteTemplateEntity", _siteTemplateEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="validator">The validator object for this SiteTemplateLanguageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 siteTemplateLanguageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(siteTemplateLanguageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_languageEntityReturnsNewIfNotFound = true;
			_siteTemplateEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _languageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticSiteTemplateLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, signalRelatedEntity, "SiteTemplateLanguageCollection", resetFKFields, new int[] { (int)SiteTemplateLanguageFieldIndex.LanguageId } );		
			_languageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _languageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguageEntity(IEntityCore relatedEntity)
		{
			if(_languageEntity!=relatedEntity)
			{		
				DesetupSyncLanguageEntity(true, true);
				_languageEntity = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticSiteTemplateLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, ref _alreadyFetchedLanguageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticSiteTemplateLanguageRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, signalRelatedEntity, "SiteTemplateLanguageCollection", resetFKFields, new int[] { (int)SiteTemplateLanguageFieldIndex.SiteTemplateId } );		
			_siteTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteTemplateEntity(IEntityCore relatedEntity)
		{
			if(_siteTemplateEntity!=relatedEntity)
			{		
				DesetupSyncSiteTemplateEntity(true, true);
				_siteTemplateEntity = (SiteTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticSiteTemplateLanguageRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, ref _alreadyFetchedSiteTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="siteTemplateLanguageId">PK value for SiteTemplateLanguage which data should be fetched into this SiteTemplateLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 siteTemplateLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SiteTemplateLanguageFieldIndex.SiteTemplateLanguageId].ForcedCurrentValueWrite(siteTemplateLanguageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSiteTemplateLanguageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SiteTemplateLanguageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SiteTemplateLanguageRelations Relations
		{
			get	{ return new SiteTemplateLanguageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("LanguageEntity")[0], (int)Obymobi.Data.EntityType.SiteTemplateLanguageEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, null, "LanguageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateEntity")[0], (int)Obymobi.Data.EntityType.SiteTemplateLanguageEntity, (int)Obymobi.Data.EntityType.SiteTemplateEntity, 0, null, null, null, "SiteTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SiteTemplateLanguageId property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."SiteTemplateLanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SiteTemplateLanguageId
		{
			get { return (System.Int32)GetValue((int)SiteTemplateLanguageFieldIndex.SiteTemplateLanguageId, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.SiteTemplateLanguageId, value, true); }
		}

		/// <summary> The SiteTemplateId property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."SiteTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteTemplateId
		{
			get { return (System.Int32)GetValue((int)SiteTemplateLanguageFieldIndex.SiteTemplateId, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.SiteTemplateId, value, true); }
		}

		/// <summary> The LanguageId property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LanguageId
		{
			get { return (System.Int32)GetValue((int)SiteTemplateLanguageFieldIndex.LanguageId, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The Name property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SiteTemplateLanguageFieldIndex.Name, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SiteTemplateLanguageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SiteTemplateLanguageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteTemplateLanguageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SiteTemplateLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplateLanguage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteTemplateLanguageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SiteTemplateLanguageFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LanguageEntity LanguageEntity
		{
			get	{ return GetSingleLanguageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteTemplateLanguageCollection", "LanguageEntity", _languageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageEntity. When set to true, LanguageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageEntity is accessed. You can always execute a forced fetch by calling GetSingleLanguageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageEntity
		{
			get	{ return _alwaysFetchLanguageEntity; }
			set	{ _alwaysFetchLanguageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageEntity already has been fetched. Setting this property to false when LanguageEntity has been fetched
		/// will set LanguageEntity to null as well. Setting this property to true while LanguageEntity hasn't been fetched disables lazy loading for LanguageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageEntity
		{
			get { return _alreadyFetchedLanguageEntity;}
			set 
			{
				if(_alreadyFetchedLanguageEntity && !value)
				{
					this.LanguageEntity = null;
				}
				_alreadyFetchedLanguageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LanguageEntity is not found
		/// in the database. When set to true, LanguageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LanguageEntityReturnsNewIfNotFound
		{
			get	{ return _languageEntityReturnsNewIfNotFound; }
			set { _languageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteTemplateEntity SiteTemplateEntity
		{
			get	{ return GetSingleSiteTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteTemplateLanguageCollection", "SiteTemplateEntity", _siteTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateEntity. When set to true, SiteTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateEntity
		{
			get	{ return _alwaysFetchSiteTemplateEntity; }
			set	{ _alwaysFetchSiteTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateEntity already has been fetched. Setting this property to false when SiteTemplateEntity has been fetched
		/// will set SiteTemplateEntity to null as well. Setting this property to true while SiteTemplateEntity hasn't been fetched disables lazy loading for SiteTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateEntity
		{
			get { return _alreadyFetchedSiteTemplateEntity;}
			set 
			{
				if(_alreadyFetchedSiteTemplateEntity && !value)
				{
					this.SiteTemplateEntity = null;
				}
				_alreadyFetchedSiteTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteTemplateEntity is not found
		/// in the database. When set to true, SiteTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _siteTemplateEntityReturnsNewIfNotFound; }
			set { _siteTemplateEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SiteTemplateLanguageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
