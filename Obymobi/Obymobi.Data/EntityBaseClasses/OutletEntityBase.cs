﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Outlet'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	[DebuggerDisplay("OutletId={OutletId}, Name={Name}")]
	public abstract partial class OutletEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OutletEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.LandingPageCollection	_landingPageCollection;
		private bool	_alwaysFetchLandingPageCollection, _alreadyFetchedLandingPageCollection;
		private Obymobi.Data.CollectionClasses.BusinesshoursCollection	_businesshoursCollection;
		private bool	_alwaysFetchBusinesshoursCollection, _alreadyFetchedBusinesshoursCollection;
		private Obymobi.Data.CollectionClasses.CheckoutMethodCollection	_checkoutMethodCollection;
		private bool	_alwaysFetchCheckoutMethodCollection, _alreadyFetchedCheckoutMethodCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.OptInCollection	_optInCollection;
		private bool	_alwaysFetchOptInCollection, _alreadyFetchedOptInCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.ServiceMethodCollection	_serviceMethodCollection;
		private bool	_alwaysFetchServiceMethodCollection, _alreadyFetchedServiceMethodCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private OutletOperationalStateEntity _outletOperationalStateEntity;
		private bool	_alwaysFetchOutletOperationalStateEntity, _alreadyFetchedOutletOperationalStateEntity, _outletOperationalStateEntityReturnsNewIfNotFound;
		private OutletSellerInformationEntity _outletSellerInformationEntity;
		private bool	_alwaysFetchOutletSellerInformationEntity, _alreadyFetchedOutletSellerInformationEntity, _outletSellerInformationEntityReturnsNewIfNotFound;
		private ProductEntity _deliveryChargeProductEntity;
		private bool	_alwaysFetchDeliveryChargeProductEntity, _alreadyFetchedDeliveryChargeProductEntity, _deliveryChargeProductEntityReturnsNewIfNotFound;
		private ProductEntity _serviceChargeProductEntity;
		private bool	_alwaysFetchServiceChargeProductEntity, _alreadyFetchedServiceChargeProductEntity, _serviceChargeProductEntityReturnsNewIfNotFound;
		private ProductEntity _tippingProductEntity;
		private bool	_alwaysFetchTippingProductEntity, _alreadyFetchedTippingProductEntity, _tippingProductEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name OutletOperationalStateEntity</summary>
			public static readonly string OutletOperationalStateEntity = "OutletOperationalStateEntity";
			/// <summary>Member name OutletSellerInformationEntity</summary>
			public static readonly string OutletSellerInformationEntity = "OutletSellerInformationEntity";
			/// <summary>Member name DeliveryChargeProductEntity</summary>
			public static readonly string DeliveryChargeProductEntity = "DeliveryChargeProductEntity";
			/// <summary>Member name ServiceChargeProductEntity</summary>
			public static readonly string ServiceChargeProductEntity = "ServiceChargeProductEntity";
			/// <summary>Member name TippingProductEntity</summary>
			public static readonly string TippingProductEntity = "TippingProductEntity";
			/// <summary>Member name LandingPageCollection</summary>
			public static readonly string LandingPageCollection = "LandingPageCollection";
			/// <summary>Member name BusinesshoursCollection</summary>
			public static readonly string BusinesshoursCollection = "BusinesshoursCollection";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name OptInCollection</summary>
			public static readonly string OptInCollection = "OptInCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name ServiceMethodCollection</summary>
			public static readonly string ServiceMethodCollection = "ServiceMethodCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OutletEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OutletEntityBase() :base("OutletEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		protected OutletEntityBase(System.Int32 outletId):base("OutletEntity")
		{
			InitClassFetch(outletId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OutletEntityBase(System.Int32 outletId, IPrefetchPath prefetchPathToUse): base("OutletEntity")
		{
			InitClassFetch(outletId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="validator">The custom validator object for this OutletEntity</param>
		protected OutletEntityBase(System.Int32 outletId, IValidator validator):base("OutletEntity")
		{
			InitClassFetch(outletId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_landingPageCollection = (Obymobi.Data.CollectionClasses.LandingPageCollection)info.GetValue("_landingPageCollection", typeof(Obymobi.Data.CollectionClasses.LandingPageCollection));
			_alwaysFetchLandingPageCollection = info.GetBoolean("_alwaysFetchLandingPageCollection");
			_alreadyFetchedLandingPageCollection = info.GetBoolean("_alreadyFetchedLandingPageCollection");

			_businesshoursCollection = (Obymobi.Data.CollectionClasses.BusinesshoursCollection)info.GetValue("_businesshoursCollection", typeof(Obymobi.Data.CollectionClasses.BusinesshoursCollection));
			_alwaysFetchBusinesshoursCollection = info.GetBoolean("_alwaysFetchBusinesshoursCollection");
			_alreadyFetchedBusinesshoursCollection = info.GetBoolean("_alreadyFetchedBusinesshoursCollection");

			_checkoutMethodCollection = (Obymobi.Data.CollectionClasses.CheckoutMethodCollection)info.GetValue("_checkoutMethodCollection", typeof(Obymobi.Data.CollectionClasses.CheckoutMethodCollection));
			_alwaysFetchCheckoutMethodCollection = info.GetBoolean("_alwaysFetchCheckoutMethodCollection");
			_alreadyFetchedCheckoutMethodCollection = info.GetBoolean("_alreadyFetchedCheckoutMethodCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_optInCollection = (Obymobi.Data.CollectionClasses.OptInCollection)info.GetValue("_optInCollection", typeof(Obymobi.Data.CollectionClasses.OptInCollection));
			_alwaysFetchOptInCollection = info.GetBoolean("_alwaysFetchOptInCollection");
			_alreadyFetchedOptInCollection = info.GetBoolean("_alreadyFetchedOptInCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_serviceMethodCollection = (Obymobi.Data.CollectionClasses.ServiceMethodCollection)info.GetValue("_serviceMethodCollection", typeof(Obymobi.Data.CollectionClasses.ServiceMethodCollection));
			_alwaysFetchServiceMethodCollection = info.GetBoolean("_alwaysFetchServiceMethodCollection");
			_alreadyFetchedServiceMethodCollection = info.GetBoolean("_alreadyFetchedServiceMethodCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_outletOperationalStateEntity = (OutletOperationalStateEntity)info.GetValue("_outletOperationalStateEntity", typeof(OutletOperationalStateEntity));
			if(_outletOperationalStateEntity!=null)
			{
				_outletOperationalStateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletOperationalStateEntityReturnsNewIfNotFound = info.GetBoolean("_outletOperationalStateEntityReturnsNewIfNotFound");
			_alwaysFetchOutletOperationalStateEntity = info.GetBoolean("_alwaysFetchOutletOperationalStateEntity");
			_alreadyFetchedOutletOperationalStateEntity = info.GetBoolean("_alreadyFetchedOutletOperationalStateEntity");

			_outletSellerInformationEntity = (OutletSellerInformationEntity)info.GetValue("_outletSellerInformationEntity", typeof(OutletSellerInformationEntity));
			if(_outletSellerInformationEntity!=null)
			{
				_outletSellerInformationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletSellerInformationEntityReturnsNewIfNotFound = info.GetBoolean("_outletSellerInformationEntityReturnsNewIfNotFound");
			_alwaysFetchOutletSellerInformationEntity = info.GetBoolean("_alwaysFetchOutletSellerInformationEntity");
			_alreadyFetchedOutletSellerInformationEntity = info.GetBoolean("_alreadyFetchedOutletSellerInformationEntity");

			_deliveryChargeProductEntity = (ProductEntity)info.GetValue("_deliveryChargeProductEntity", typeof(ProductEntity));
			if(_deliveryChargeProductEntity!=null)
			{
				_deliveryChargeProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliveryChargeProductEntityReturnsNewIfNotFound = info.GetBoolean("_deliveryChargeProductEntityReturnsNewIfNotFound");
			_alwaysFetchDeliveryChargeProductEntity = info.GetBoolean("_alwaysFetchDeliveryChargeProductEntity");
			_alreadyFetchedDeliveryChargeProductEntity = info.GetBoolean("_alreadyFetchedDeliveryChargeProductEntity");

			_serviceChargeProductEntity = (ProductEntity)info.GetValue("_serviceChargeProductEntity", typeof(ProductEntity));
			if(_serviceChargeProductEntity!=null)
			{
				_serviceChargeProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_serviceChargeProductEntityReturnsNewIfNotFound = info.GetBoolean("_serviceChargeProductEntityReturnsNewIfNotFound");
			_alwaysFetchServiceChargeProductEntity = info.GetBoolean("_alwaysFetchServiceChargeProductEntity");
			_alreadyFetchedServiceChargeProductEntity = info.GetBoolean("_alreadyFetchedServiceChargeProductEntity");

			_tippingProductEntity = (ProductEntity)info.GetValue("_tippingProductEntity", typeof(ProductEntity));
			if(_tippingProductEntity!=null)
			{
				_tippingProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tippingProductEntityReturnsNewIfNotFound = info.GetBoolean("_tippingProductEntityReturnsNewIfNotFound");
			_alwaysFetchTippingProductEntity = info.GetBoolean("_alwaysFetchTippingProductEntity");
			_alreadyFetchedTippingProductEntity = info.GetBoolean("_alreadyFetchedTippingProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OutletFieldIndex)fieldIndex)
			{
				case OutletFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case OutletFieldIndex.TippingProductId:
					DesetupSyncTippingProductEntity(true, false);
					_alreadyFetchedTippingProductEntity = false;
					break;
				case OutletFieldIndex.OutletOperationalStateId:
					DesetupSyncOutletOperationalStateEntity(true, false);
					_alreadyFetchedOutletOperationalStateEntity = false;
					break;
				case OutletFieldIndex.DeliveryChargeProductId:
					DesetupSyncDeliveryChargeProductEntity(true, false);
					_alreadyFetchedDeliveryChargeProductEntity = false;
					break;
				case OutletFieldIndex.ServiceChargeProductId:
					DesetupSyncServiceChargeProductEntity(true, false);
					_alreadyFetchedServiceChargeProductEntity = false;
					break;
				case OutletFieldIndex.OutletSellerInformationId:
					DesetupSyncOutletSellerInformationEntity(true, false);
					_alreadyFetchedOutletSellerInformationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLandingPageCollection = (_landingPageCollection.Count > 0);
			_alreadyFetchedBusinesshoursCollection = (_businesshoursCollection.Count > 0);
			_alreadyFetchedCheckoutMethodCollection = (_checkoutMethodCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedOptInCollection = (_optInCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedServiceMethodCollection = (_serviceMethodCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedOutletOperationalStateEntity = (_outletOperationalStateEntity != null);
			_alreadyFetchedOutletSellerInformationEntity = (_outletSellerInformationEntity != null);
			_alreadyFetchedDeliveryChargeProductEntity = (_deliveryChargeProductEntity != null);
			_alreadyFetchedServiceChargeProductEntity = (_serviceChargeProductEntity != null);
			_alreadyFetchedTippingProductEntity = (_tippingProductEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "OutletOperationalStateEntity":
					toReturn.Add(Relations.OutletOperationalStateEntityUsingOutletOperationalStateId);
					break;
				case "OutletSellerInformationEntity":
					toReturn.Add(Relations.OutletSellerInformationEntityUsingOutletSellerInformationId);
					break;
				case "DeliveryChargeProductEntity":
					toReturn.Add(Relations.ProductEntityUsingDeliveryChargeProductId);
					break;
				case "ServiceChargeProductEntity":
					toReturn.Add(Relations.ProductEntityUsingServiceChargeProductId);
					break;
				case "TippingProductEntity":
					toReturn.Add(Relations.ProductEntityUsingTippingProductId);
					break;
				case "LandingPageCollection":
					toReturn.Add(Relations.LandingPageEntityUsingOutletId);
					break;
				case "BusinesshoursCollection":
					toReturn.Add(Relations.BusinesshoursEntityUsingOutletId);
					break;
				case "CheckoutMethodCollection":
					toReturn.Add(Relations.CheckoutMethodEntityUsingOutletId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingOutletId);
					break;
				case "OptInCollection":
					toReturn.Add(Relations.OptInEntityUsingOutletId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingOutletId);
					break;
				case "ServiceMethodCollection":
					toReturn.Add(Relations.ServiceMethodEntityUsingOutletId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_landingPageCollection", (!this.MarkedForDeletion?_landingPageCollection:null));
			info.AddValue("_alwaysFetchLandingPageCollection", _alwaysFetchLandingPageCollection);
			info.AddValue("_alreadyFetchedLandingPageCollection", _alreadyFetchedLandingPageCollection);
			info.AddValue("_businesshoursCollection", (!this.MarkedForDeletion?_businesshoursCollection:null));
			info.AddValue("_alwaysFetchBusinesshoursCollection", _alwaysFetchBusinesshoursCollection);
			info.AddValue("_alreadyFetchedBusinesshoursCollection", _alreadyFetchedBusinesshoursCollection);
			info.AddValue("_checkoutMethodCollection", (!this.MarkedForDeletion?_checkoutMethodCollection:null));
			info.AddValue("_alwaysFetchCheckoutMethodCollection", _alwaysFetchCheckoutMethodCollection);
			info.AddValue("_alreadyFetchedCheckoutMethodCollection", _alreadyFetchedCheckoutMethodCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_optInCollection", (!this.MarkedForDeletion?_optInCollection:null));
			info.AddValue("_alwaysFetchOptInCollection", _alwaysFetchOptInCollection);
			info.AddValue("_alreadyFetchedOptInCollection", _alreadyFetchedOptInCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_serviceMethodCollection", (!this.MarkedForDeletion?_serviceMethodCollection:null));
			info.AddValue("_alwaysFetchServiceMethodCollection", _alwaysFetchServiceMethodCollection);
			info.AddValue("_alreadyFetchedServiceMethodCollection", _alreadyFetchedServiceMethodCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_outletOperationalStateEntity", (!this.MarkedForDeletion?_outletOperationalStateEntity:null));
			info.AddValue("_outletOperationalStateEntityReturnsNewIfNotFound", _outletOperationalStateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletOperationalStateEntity", _alwaysFetchOutletOperationalStateEntity);
			info.AddValue("_alreadyFetchedOutletOperationalStateEntity", _alreadyFetchedOutletOperationalStateEntity);
			info.AddValue("_outletSellerInformationEntity", (!this.MarkedForDeletion?_outletSellerInformationEntity:null));
			info.AddValue("_outletSellerInformationEntityReturnsNewIfNotFound", _outletSellerInformationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletSellerInformationEntity", _alwaysFetchOutletSellerInformationEntity);
			info.AddValue("_alreadyFetchedOutletSellerInformationEntity", _alreadyFetchedOutletSellerInformationEntity);
			info.AddValue("_deliveryChargeProductEntity", (!this.MarkedForDeletion?_deliveryChargeProductEntity:null));
			info.AddValue("_deliveryChargeProductEntityReturnsNewIfNotFound", _deliveryChargeProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliveryChargeProductEntity", _alwaysFetchDeliveryChargeProductEntity);
			info.AddValue("_alreadyFetchedDeliveryChargeProductEntity", _alreadyFetchedDeliveryChargeProductEntity);
			info.AddValue("_serviceChargeProductEntity", (!this.MarkedForDeletion?_serviceChargeProductEntity:null));
			info.AddValue("_serviceChargeProductEntityReturnsNewIfNotFound", _serviceChargeProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchServiceChargeProductEntity", _alwaysFetchServiceChargeProductEntity);
			info.AddValue("_alreadyFetchedServiceChargeProductEntity", _alreadyFetchedServiceChargeProductEntity);
			info.AddValue("_tippingProductEntity", (!this.MarkedForDeletion?_tippingProductEntity:null));
			info.AddValue("_tippingProductEntityReturnsNewIfNotFound", _tippingProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTippingProductEntity", _alwaysFetchTippingProductEntity);
			info.AddValue("_alreadyFetchedTippingProductEntity", _alreadyFetchedTippingProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "OutletOperationalStateEntity":
					_alreadyFetchedOutletOperationalStateEntity = true;
					this.OutletOperationalStateEntity = (OutletOperationalStateEntity)entity;
					break;
				case "OutletSellerInformationEntity":
					_alreadyFetchedOutletSellerInformationEntity = true;
					this.OutletSellerInformationEntity = (OutletSellerInformationEntity)entity;
					break;
				case "DeliveryChargeProductEntity":
					_alreadyFetchedDeliveryChargeProductEntity = true;
					this.DeliveryChargeProductEntity = (ProductEntity)entity;
					break;
				case "ServiceChargeProductEntity":
					_alreadyFetchedServiceChargeProductEntity = true;
					this.ServiceChargeProductEntity = (ProductEntity)entity;
					break;
				case "TippingProductEntity":
					_alreadyFetchedTippingProductEntity = true;
					this.TippingProductEntity = (ProductEntity)entity;
					break;
				case "LandingPageCollection":
					_alreadyFetchedLandingPageCollection = true;
					if(entity!=null)
					{
						this.LandingPageCollection.Add((LandingPageEntity)entity);
					}
					break;
				case "BusinesshoursCollection":
					_alreadyFetchedBusinesshoursCollection = true;
					if(entity!=null)
					{
						this.BusinesshoursCollection.Add((BusinesshoursEntity)entity);
					}
					break;
				case "CheckoutMethodCollection":
					_alreadyFetchedCheckoutMethodCollection = true;
					if(entity!=null)
					{
						this.CheckoutMethodCollection.Add((CheckoutMethodEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "OptInCollection":
					_alreadyFetchedOptInCollection = true;
					if(entity!=null)
					{
						this.OptInCollection.Add((OptInEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "ServiceMethodCollection":
					_alreadyFetchedServiceMethodCollection = true;
					if(entity!=null)
					{
						this.ServiceMethodCollection.Add((ServiceMethodEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "OutletOperationalStateEntity":
					SetupSyncOutletOperationalStateEntity(relatedEntity);
					break;
				case "OutletSellerInformationEntity":
					SetupSyncOutletSellerInformationEntity(relatedEntity);
					break;
				case "DeliveryChargeProductEntity":
					SetupSyncDeliveryChargeProductEntity(relatedEntity);
					break;
				case "ServiceChargeProductEntity":
					SetupSyncServiceChargeProductEntity(relatedEntity);
					break;
				case "TippingProductEntity":
					SetupSyncTippingProductEntity(relatedEntity);
					break;
				case "LandingPageCollection":
					_landingPageCollection.Add((LandingPageEntity)relatedEntity);
					break;
				case "BusinesshoursCollection":
					_businesshoursCollection.Add((BusinesshoursEntity)relatedEntity);
					break;
				case "CheckoutMethodCollection":
					_checkoutMethodCollection.Add((CheckoutMethodEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "OptInCollection":
					_optInCollection.Add((OptInEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "ServiceMethodCollection":
					_serviceMethodCollection.Add((ServiceMethodEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "OutletOperationalStateEntity":
					DesetupSyncOutletOperationalStateEntity(false, true);
					break;
				case "OutletSellerInformationEntity":
					DesetupSyncOutletSellerInformationEntity(false, true);
					break;
				case "DeliveryChargeProductEntity":
					DesetupSyncDeliveryChargeProductEntity(false, true);
					break;
				case "ServiceChargeProductEntity":
					DesetupSyncServiceChargeProductEntity(false, true);
					break;
				case "TippingProductEntity":
					DesetupSyncTippingProductEntity(false, true);
					break;
				case "LandingPageCollection":
					this.PerformRelatedEntityRemoval(_landingPageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinesshoursCollection":
					this.PerformRelatedEntityRemoval(_businesshoursCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CheckoutMethodCollection":
					this.PerformRelatedEntityRemoval(_checkoutMethodCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OptInCollection":
					this.PerformRelatedEntityRemoval(_optInCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ServiceMethodCollection":
					this.PerformRelatedEntityRemoval(_serviceMethodCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_outletOperationalStateEntity!=null)
			{
				toReturn.Add(_outletOperationalStateEntity);
			}
			if(_outletSellerInformationEntity!=null)
			{
				toReturn.Add(_outletSellerInformationEntity);
			}
			if(_deliveryChargeProductEntity!=null)
			{
				toReturn.Add(_deliveryChargeProductEntity);
			}
			if(_serviceChargeProductEntity!=null)
			{
				toReturn.Add(_serviceChargeProductEntity);
			}
			if(_tippingProductEntity!=null)
			{
				toReturn.Add(_tippingProductEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_landingPageCollection);
			toReturn.Add(_businesshoursCollection);
			toReturn.Add(_checkoutMethodCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_optInCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_serviceMethodCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletId)
		{
			return FetchUsingPK(outletId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(outletId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(outletId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(outletId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OutletId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OutletRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLandingPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLandingPageCollection || forceFetch || _alwaysFetchLandingPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_landingPageCollection);
				_landingPageCollection.SuppressClearInGetMulti=!forceFetch;
				_landingPageCollection.EntityFactoryToUse = entityFactoryToUse;
				_landingPageCollection.GetMultiManyToOne(null, null, null, this, filter);
				_landingPageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedLandingPageCollection = true;
			}
			return _landingPageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'LandingPageCollection'. These settings will be taken into account
		/// when the property LandingPageCollection is requested or GetMultiLandingPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLandingPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_landingPageCollection.SortClauses=sortClauses;
			_landingPageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinesshoursEntity'</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch)
		{
			return GetMultiBusinesshoursCollection(forceFetch, _businesshoursCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinesshoursEntity'</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinesshoursCollection(forceFetch, _businesshoursCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinesshoursCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinesshoursCollection || forceFetch || _alwaysFetchBusinesshoursCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businesshoursCollection);
				_businesshoursCollection.SuppressClearInGetMulti=!forceFetch;
				_businesshoursCollection.EntityFactoryToUse = entityFactoryToUse;
				_businesshoursCollection.GetMultiManyToOne(null, this, null, filter);
				_businesshoursCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinesshoursCollection = true;
			}
			return _businesshoursCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinesshoursCollection'. These settings will be taken into account
		/// when the property BusinesshoursCollection is requested or GetMultiBusinesshoursCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinesshoursCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businesshoursCollection.SortClauses=sortClauses;
			_businesshoursCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCheckoutMethodCollection || forceFetch || _alwaysFetchCheckoutMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_checkoutMethodCollection);
				_checkoutMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_checkoutMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_checkoutMethodCollection.GetMultiManyToOne(null, this, null, null, filter);
				_checkoutMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCheckoutMethodCollection = true;
			}
			return _checkoutMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CheckoutMethodCollection'. These settings will be taken into account
		/// when the property CheckoutMethodCollection is requested or GetMultiCheckoutMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCheckoutMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_checkoutMethodCollection.SortClauses=sortClauses;
			_checkoutMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OptInEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OptInEntity'</returns>
		public Obymobi.Data.CollectionClasses.OptInCollection GetMultiOptInCollection(bool forceFetch)
		{
			return GetMultiOptInCollection(forceFetch, _optInCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OptInEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OptInEntity'</returns>
		public Obymobi.Data.CollectionClasses.OptInCollection GetMultiOptInCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOptInCollection(forceFetch, _optInCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OptInEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OptInCollection GetMultiOptInCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOptInCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OptInEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OptInCollection GetMultiOptInCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOptInCollection || forceFetch || _alwaysFetchOptInCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_optInCollection);
				_optInCollection.SuppressClearInGetMulti=!forceFetch;
				_optInCollection.EntityFactoryToUse = entityFactoryToUse;
				_optInCollection.GetMultiManyToOne(null, this, filter);
				_optInCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOptInCollection = true;
			}
			return _optInCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OptInCollection'. These settings will be taken into account
		/// when the property OptInCollection is requested or GetMultiOptInCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOptInCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_optInCollection.SortClauses=sortClauses;
			_optInCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodCollection GetMultiServiceMethodCollection(bool forceFetch)
		{
			return GetMultiServiceMethodCollection(forceFetch, _serviceMethodCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodCollection GetMultiServiceMethodCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiServiceMethodCollection(forceFetch, _serviceMethodCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodCollection GetMultiServiceMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiServiceMethodCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodCollection GetMultiServiceMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedServiceMethodCollection || forceFetch || _alwaysFetchServiceMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_serviceMethodCollection);
				_serviceMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_serviceMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_serviceMethodCollection.GetMultiManyToOne(null, this, null, filter);
				_serviceMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedServiceMethodCollection = true;
			}
			return _serviceMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ServiceMethodCollection'. These settings will be taken into account
		/// when the property ServiceMethodCollection is requested or GetMultiServiceMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersServiceMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_serviceMethodCollection.SortClauses=sortClauses;
			_serviceMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletOperationalStateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletOperationalStateEntity' which is related to this entity.</returns>
		public OutletOperationalStateEntity GetSingleOutletOperationalStateEntity()
		{
			return GetSingleOutletOperationalStateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletOperationalStateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletOperationalStateEntity' which is related to this entity.</returns>
		public virtual OutletOperationalStateEntity GetSingleOutletOperationalStateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletOperationalStateEntity || forceFetch || _alwaysFetchOutletOperationalStateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletOperationalStateEntityUsingOutletOperationalStateId);
				OutletOperationalStateEntity newEntity = new OutletOperationalStateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletOperationalStateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletOperationalStateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletOperationalStateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletOperationalStateEntity = newEntity;
				_alreadyFetchedOutletOperationalStateEntity = fetchResult;
			}
			return _outletOperationalStateEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletSellerInformationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletSellerInformationEntity' which is related to this entity.</returns>
		public OutletSellerInformationEntity GetSingleOutletSellerInformationEntity()
		{
			return GetSingleOutletSellerInformationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletSellerInformationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletSellerInformationEntity' which is related to this entity.</returns>
		public virtual OutletSellerInformationEntity GetSingleOutletSellerInformationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletSellerInformationEntity || forceFetch || _alwaysFetchOutletSellerInformationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletSellerInformationEntityUsingOutletSellerInformationId);
				OutletSellerInformationEntity newEntity = new OutletSellerInformationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletSellerInformationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletSellerInformationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletSellerInformationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletSellerInformationEntity = newEntity;
				_alreadyFetchedOutletSellerInformationEntity = fetchResult;
			}
			return _outletSellerInformationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleDeliveryChargeProductEntity()
		{
			return GetSingleDeliveryChargeProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleDeliveryChargeProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliveryChargeProductEntity || forceFetch || _alwaysFetchDeliveryChargeProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingDeliveryChargeProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliveryChargeProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliveryChargeProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliveryChargeProductEntity = newEntity;
				_alreadyFetchedDeliveryChargeProductEntity = fetchResult;
			}
			return _deliveryChargeProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleServiceChargeProductEntity()
		{
			return GetSingleServiceChargeProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleServiceChargeProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedServiceChargeProductEntity || forceFetch || _alwaysFetchServiceChargeProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingServiceChargeProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceChargeProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_serviceChargeProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ServiceChargeProductEntity = newEntity;
				_alreadyFetchedServiceChargeProductEntity = fetchResult;
			}
			return _serviceChargeProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleTippingProductEntity()
		{
			return GetSingleTippingProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleTippingProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTippingProductEntity || forceFetch || _alwaysFetchTippingProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingTippingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TippingProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tippingProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TippingProductEntity = newEntity;
				_alreadyFetchedTippingProductEntity = fetchResult;
			}
			return _tippingProductEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("OutletOperationalStateEntity", _outletOperationalStateEntity);
			toReturn.Add("OutletSellerInformationEntity", _outletSellerInformationEntity);
			toReturn.Add("DeliveryChargeProductEntity", _deliveryChargeProductEntity);
			toReturn.Add("ServiceChargeProductEntity", _serviceChargeProductEntity);
			toReturn.Add("TippingProductEntity", _tippingProductEntity);
			toReturn.Add("LandingPageCollection", _landingPageCollection);
			toReturn.Add("BusinesshoursCollection", _businesshoursCollection);
			toReturn.Add("CheckoutMethodCollection", _checkoutMethodCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("OptInCollection", _optInCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("ServiceMethodCollection", _serviceMethodCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="validator">The validator object for this OutletEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 outletId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(outletId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_landingPageCollection = new Obymobi.Data.CollectionClasses.LandingPageCollection();
			_landingPageCollection.SetContainingEntityInfo(this, "OutletEntity");

			_businesshoursCollection = new Obymobi.Data.CollectionClasses.BusinesshoursCollection();
			_businesshoursCollection.SetContainingEntityInfo(this, "OutletEntity");

			_checkoutMethodCollection = new Obymobi.Data.CollectionClasses.CheckoutMethodCollection();
			_checkoutMethodCollection.SetContainingEntityInfo(this, "OutletEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "OutletEntity");

			_optInCollection = new Obymobi.Data.CollectionClasses.OptInCollection();
			_optInCollection.SetContainingEntityInfo(this, "OutletEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "OutletEntity");

			_serviceMethodCollection = new Obymobi.Data.CollectionClasses.ServiceMethodCollection();
			_serviceMethodCollection.SetContainingEntityInfo(this, "OutletEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_outletOperationalStateEntityReturnsNewIfNotFound = true;
			_outletSellerInformationEntityReturnsNewIfNotFound = true;
			_deliveryChargeProductEntityReturnsNewIfNotFound = true;
			_serviceChargeProductEntityReturnsNewIfNotFound = true;
			_tippingProductEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TippingActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TippingDefaultPercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TippingMinimumPercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TippingStepSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TippingProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerDetailsSectionActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerDetailsPhoneRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerDetailsEmailRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowPricesIncludingTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowTaxBreakDownOnBasket", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowTaxBreakDownOnCheckout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletOperationalStateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliveryChargeProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceChargeProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerDetailsNameRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletSellerInformationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowTaxBreakdown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OptInType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxDisclaimer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxNumberTitle", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "OutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletOperationalStateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletOperationalStateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletOperationalStateEntity, new PropertyChangedEventHandler( OnOutletOperationalStateEntityPropertyChanged ), "OutletOperationalStateEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.OutletOperationalStateEntityUsingOutletOperationalStateIdStatic, true, signalRelatedEntity, "OutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.OutletOperationalStateId } );		
			_outletOperationalStateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletOperationalStateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletOperationalStateEntity(IEntityCore relatedEntity)
		{
			if(_outletOperationalStateEntity!=relatedEntity)
			{		
				DesetupSyncOutletOperationalStateEntity(true, true);
				_outletOperationalStateEntity = (OutletOperationalStateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletOperationalStateEntity, new PropertyChangedEventHandler( OnOutletOperationalStateEntityPropertyChanged ), "OutletOperationalStateEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.OutletOperationalStateEntityUsingOutletOperationalStateIdStatic, true, ref _alreadyFetchedOutletOperationalStateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletOperationalStateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletSellerInformationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletSellerInformationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletSellerInformationEntity, new PropertyChangedEventHandler( OnOutletSellerInformationEntityPropertyChanged ), "OutletSellerInformationEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, true, signalRelatedEntity, "OutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.OutletSellerInformationId } );		
			_outletSellerInformationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletSellerInformationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletSellerInformationEntity(IEntityCore relatedEntity)
		{
			if(_outletSellerInformationEntity!=relatedEntity)
			{		
				DesetupSyncOutletSellerInformationEntity(true, true);
				_outletSellerInformationEntity = (OutletSellerInformationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletSellerInformationEntity, new PropertyChangedEventHandler( OnOutletSellerInformationEntityPropertyChanged ), "OutletSellerInformationEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, true, ref _alreadyFetchedOutletSellerInformationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletSellerInformationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliveryChargeProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliveryChargeProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliveryChargeProductEntity, new PropertyChangedEventHandler( OnDeliveryChargeProductEntityPropertyChanged ), "DeliveryChargeProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingDeliveryChargeProductIdStatic, true, signalRelatedEntity, "DeliveryChargeOutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.DeliveryChargeProductId } );		
			_deliveryChargeProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliveryChargeProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliveryChargeProductEntity(IEntityCore relatedEntity)
		{
			if(_deliveryChargeProductEntity!=relatedEntity)
			{		
				DesetupSyncDeliveryChargeProductEntity(true, true);
				_deliveryChargeProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliveryChargeProductEntity, new PropertyChangedEventHandler( OnDeliveryChargeProductEntityPropertyChanged ), "DeliveryChargeProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingDeliveryChargeProductIdStatic, true, ref _alreadyFetchedDeliveryChargeProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliveryChargeProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _serviceChargeProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncServiceChargeProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _serviceChargeProductEntity, new PropertyChangedEventHandler( OnServiceChargeProductEntityPropertyChanged ), "ServiceChargeProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingServiceChargeProductIdStatic, true, signalRelatedEntity, "ServiceChargeOutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.ServiceChargeProductId } );		
			_serviceChargeProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _serviceChargeProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncServiceChargeProductEntity(IEntityCore relatedEntity)
		{
			if(_serviceChargeProductEntity!=relatedEntity)
			{		
				DesetupSyncServiceChargeProductEntity(true, true);
				_serviceChargeProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _serviceChargeProductEntity, new PropertyChangedEventHandler( OnServiceChargeProductEntityPropertyChanged ), "ServiceChargeProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingServiceChargeProductIdStatic, true, ref _alreadyFetchedServiceChargeProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnServiceChargeProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tippingProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTippingProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tippingProductEntity, new PropertyChangedEventHandler( OnTippingProductEntityPropertyChanged ), "TippingProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingTippingProductIdStatic, true, signalRelatedEntity, "TippingProductOutletCollection", resetFKFields, new int[] { (int)OutletFieldIndex.TippingProductId } );		
			_tippingProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _tippingProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTippingProductEntity(IEntityCore relatedEntity)
		{
			if(_tippingProductEntity!=relatedEntity)
			{		
				DesetupSyncTippingProductEntity(true, true);
				_tippingProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tippingProductEntity, new PropertyChangedEventHandler( OnTippingProductEntityPropertyChanged ), "TippingProductEntity", Obymobi.Data.RelationClasses.StaticOutletRelations.ProductEntityUsingTippingProductIdStatic, true, ref _alreadyFetchedTippingProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTippingProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="outletId">PK value for Outlet which data should be fetched into this Outlet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 outletId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OutletFieldIndex.OutletId].ForcedCurrentValueWrite(outletId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOutletDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OutletEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OutletRelations Relations
		{
			get	{ return new OutletRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Businesshours' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinesshoursCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BusinesshoursCollection(), (IEntityRelation)GetRelationsForField("BusinesshoursCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.BusinesshoursEntity, 0, null, null, null, "BusinesshoursCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.CheckoutMethodEntity, 0, null, null, null, "CheckoutMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OptIn' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOptInCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OptInCollection(), (IEntityRelation)GetRelationsForField("OptInCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.OptInEntity, 0, null, null, null, "OptInCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodCollection")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.ServiceMethodEntity, 0, null, null, null, "ServiceMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutletOperationalState'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletOperationalStateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletOperationalStateCollection(), (IEntityRelation)GetRelationsForField("OutletOperationalStateEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.OutletOperationalStateEntity, 0, null, null, null, "OutletOperationalStateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutletSellerInformation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletSellerInformationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletSellerInformationCollection(), (IEntityRelation)GetRelationsForField("OutletSellerInformationEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.OutletSellerInformationEntity, 0, null, null, null, "OutletSellerInformationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliveryChargeProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("DeliveryChargeProductEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "DeliveryChargeProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceChargeProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ServiceChargeProductEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ServiceChargeProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTippingProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("TippingProductEntity")[0], (int)Obymobi.Data.EntityType.OutletEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "TippingProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OutletId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletId
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.OutletId, true); }
			set	{ SetValue((int)OutletFieldIndex.OutletId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.CompanyId, true); }
			set	{ SetValue((int)OutletFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.Name, true); }
			set	{ SetValue((int)OutletFieldIndex.Name, value, true); }
		}

		/// <summary> The TippingActive property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TippingActive
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.TippingActive, true); }
			set	{ SetValue((int)OutletFieldIndex.TippingActive, value, true); }
		}

		/// <summary> The TippingDefaultPercentage property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingDefaultPercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingDefaultPercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingDefaultPercentage, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingDefaultPercentage, value, true); }
		}

		/// <summary> The TippingMinimumPercentage property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingMinimumPercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingMinimumPercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingMinimumPercentage, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingMinimumPercentage, value, true); }
		}

		/// <summary> The TippingStepSize property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingStepSize"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TippingStepSize
		{
			get { return (Nullable<System.Double>)GetValue((int)OutletFieldIndex.TippingStepSize, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingStepSize, value, true); }
		}

		/// <summary> The TippingProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TippingProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TippingProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.TippingProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.TippingProductId, value, true); }
		}

		/// <summary> The CustomerDetailsSectionActive property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsSectionActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsSectionActive
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsSectionActive, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsSectionActive, value, true); }
		}

		/// <summary> The CustomerDetailsPhoneRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsPhoneRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsPhoneRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsPhoneRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsPhoneRequired, value, true); }
		}

		/// <summary> The CustomerDetailsEmailRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsEmailRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsEmailRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsEmailRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsEmailRequired, value, true); }
		}

		/// <summary> The ShowPricesIncludingTax property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowPricesIncludingTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowPricesIncludingTax
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowPricesIncludingTax, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowPricesIncludingTax, value, true); }
		}

		/// <summary> The ShowTaxBreakDownOnBasket property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakDownOnBasket"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowTaxBreakDownOnBasket
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowTaxBreakDownOnBasket, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakDownOnBasket, value, true); }
		}

		/// <summary> The ShowTaxBreakDownOnCheckout property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakDownOnCheckout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowTaxBreakDownOnCheckout
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.ShowTaxBreakDownOnCheckout, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakDownOnCheckout, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OutletFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OutletFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OutletFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OutletFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ReceiptEmail property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ReceiptEmail"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptEmail
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.ReceiptEmail, true); }
			set	{ SetValue((int)OutletFieldIndex.ReceiptEmail, value, true); }
		}

		/// <summary> The OutletOperationalStateId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletOperationalStateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletOperationalStateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.OutletOperationalStateId, false); }
			set	{ SetValue((int)OutletFieldIndex.OutletOperationalStateId, value, true); }
		}

		/// <summary> The Latitude property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Latitude"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Latitude
		{
			get { return (System.Double)GetValue((int)OutletFieldIndex.Latitude, true); }
			set	{ SetValue((int)OutletFieldIndex.Latitude, value, true); }
		}

		/// <summary> The Longitude property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."Longitude"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Longitude
		{
			get { return (System.Double)GetValue((int)OutletFieldIndex.Longitude, true); }
			set	{ SetValue((int)OutletFieldIndex.Longitude, value, true); }
		}

		/// <summary> The DeliveryChargeProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."DeliveryChargeProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliveryChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.DeliveryChargeProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.DeliveryChargeProductId, value, true); }
		}

		/// <summary> The ServiceChargeProductId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ServiceChargeProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.ServiceChargeProductId, false); }
			set	{ SetValue((int)OutletFieldIndex.ServiceChargeProductId, value, true); }
		}

		/// <summary> The CustomerDetailsNameRequired property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."CustomerDetailsNameRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CustomerDetailsNameRequired
		{
			get { return (System.Boolean)GetValue((int)OutletFieldIndex.CustomerDetailsNameRequired, true); }
			set	{ SetValue((int)OutletFieldIndex.CustomerDetailsNameRequired, value, true); }
		}

		/// <summary> The OutletSellerInformationId property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OutletSellerInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletSellerInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletFieldIndex.OutletSellerInformationId, false); }
			set	{ SetValue((int)OutletFieldIndex.OutletSellerInformationId, value, true); }
		}

		/// <summary> The ShowTaxBreakdown property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."ShowTaxBreakdown"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.TaxBreakdown ShowTaxBreakdown
		{
			get { return (Obymobi.Enums.TaxBreakdown)GetValue((int)OutletFieldIndex.ShowTaxBreakdown, true); }
			set	{ SetValue((int)OutletFieldIndex.ShowTaxBreakdown, value, true); }
		}

		/// <summary> The OptInType property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."OptInType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OptInType OptInType
		{
			get { return (Obymobi.Enums.OptInType)GetValue((int)OutletFieldIndex.OptInType, true); }
			set	{ SetValue((int)OutletFieldIndex.OptInType, value, true); }
		}

		/// <summary> The TaxDisclaimer property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TaxDisclaimer"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxDisclaimer
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.TaxDisclaimer, true); }
			set	{ SetValue((int)OutletFieldIndex.TaxDisclaimer, value, true); }
		}

		/// <summary> The TaxNumberTitle property of the Entity Outlet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Outlet"."TaxNumberTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxNumberTitle
		{
			get { return (System.String)GetValue((int)OutletFieldIndex.TaxNumberTitle, true); }
			set	{ SetValue((int)OutletFieldIndex.TaxNumberTitle, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLandingPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection LandingPageCollection
		{
			get	{ return GetMultiLandingPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageCollection. When set to true, LandingPageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiLandingPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageCollection
		{
			get	{ return _alwaysFetchLandingPageCollection; }
			set	{ _alwaysFetchLandingPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageCollection already has been fetched. Setting this property to false when LandingPageCollection has been fetched
		/// will clear the LandingPageCollection collection well. Setting this property to true while LandingPageCollection hasn't been fetched disables lazy loading for LandingPageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageCollection
		{
			get { return _alreadyFetchedLandingPageCollection;}
			set 
			{
				if(_alreadyFetchedLandingPageCollection && !value && (_landingPageCollection != null))
				{
					_landingPageCollection.Clear();
				}
				_alreadyFetchedLandingPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinesshoursCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.BusinesshoursCollection BusinesshoursCollection
		{
			get	{ return GetMultiBusinesshoursCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinesshoursCollection. When set to true, BusinesshoursCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinesshoursCollection is accessed. You can always execute/ a forced fetch by calling GetMultiBusinesshoursCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinesshoursCollection
		{
			get	{ return _alwaysFetchBusinesshoursCollection; }
			set	{ _alwaysFetchBusinesshoursCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinesshoursCollection already has been fetched. Setting this property to false when BusinesshoursCollection has been fetched
		/// will clear the BusinesshoursCollection collection well. Setting this property to true while BusinesshoursCollection hasn't been fetched disables lazy loading for BusinesshoursCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinesshoursCollection
		{
			get { return _alreadyFetchedBusinesshoursCollection;}
			set 
			{
				if(_alreadyFetchedBusinesshoursCollection && !value && (_businesshoursCollection != null))
				{
					_businesshoursCollection.Clear();
				}
				_alreadyFetchedBusinesshoursCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCheckoutMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection CheckoutMethodCollection
		{
			get	{ return GetMultiCheckoutMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodCollection. When set to true, CheckoutMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCheckoutMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodCollection
		{
			get	{ return _alwaysFetchCheckoutMethodCollection; }
			set	{ _alwaysFetchCheckoutMethodCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodCollection already has been fetched. Setting this property to false when CheckoutMethodCollection has been fetched
		/// will clear the CheckoutMethodCollection collection well. Setting this property to true while CheckoutMethodCollection hasn't been fetched disables lazy loading for CheckoutMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodCollection
		{
			get { return _alreadyFetchedCheckoutMethodCollection;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodCollection && !value && (_checkoutMethodCollection != null))
				{
					_checkoutMethodCollection.Clear();
				}
				_alreadyFetchedCheckoutMethodCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OptInEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOptInCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OptInCollection OptInCollection
		{
			get	{ return GetMultiOptInCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OptInCollection. When set to true, OptInCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OptInCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOptInCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOptInCollection
		{
			get	{ return _alwaysFetchOptInCollection; }
			set	{ _alwaysFetchOptInCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OptInCollection already has been fetched. Setting this property to false when OptInCollection has been fetched
		/// will clear the OptInCollection collection well. Setting this property to true while OptInCollection hasn't been fetched disables lazy loading for OptInCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOptInCollection
		{
			get { return _alreadyFetchedOptInCollection;}
			set 
			{
				if(_alreadyFetchedOptInCollection && !value && (_optInCollection != null))
				{
					_optInCollection.Clear();
				}
				_alreadyFetchedOptInCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ServiceMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiServiceMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodCollection ServiceMethodCollection
		{
			get	{ return GetMultiServiceMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodCollection. When set to true, ServiceMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodCollection is accessed. You can always execute/ a forced fetch by calling GetMultiServiceMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodCollection
		{
			get	{ return _alwaysFetchServiceMethodCollection; }
			set	{ _alwaysFetchServiceMethodCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodCollection already has been fetched. Setting this property to false when ServiceMethodCollection has been fetched
		/// will clear the ServiceMethodCollection collection well. Setting this property to true while ServiceMethodCollection hasn't been fetched disables lazy loading for ServiceMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodCollection
		{
			get { return _alreadyFetchedServiceMethodCollection;}
			set 
			{
				if(_alreadyFetchedServiceMethodCollection && !value && (_serviceMethodCollection != null))
				{
					_serviceMethodCollection.Clear();
				}
				_alreadyFetchedServiceMethodCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OutletCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletOperationalStateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletOperationalStateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletOperationalStateEntity OutletOperationalStateEntity
		{
			get	{ return GetSingleOutletOperationalStateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletOperationalStateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OutletCollection", "OutletOperationalStateEntity", _outletOperationalStateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletOperationalStateEntity. When set to true, OutletOperationalStateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletOperationalStateEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletOperationalStateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletOperationalStateEntity
		{
			get	{ return _alwaysFetchOutletOperationalStateEntity; }
			set	{ _alwaysFetchOutletOperationalStateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletOperationalStateEntity already has been fetched. Setting this property to false when OutletOperationalStateEntity has been fetched
		/// will set OutletOperationalStateEntity to null as well. Setting this property to true while OutletOperationalStateEntity hasn't been fetched disables lazy loading for OutletOperationalStateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletOperationalStateEntity
		{
			get { return _alreadyFetchedOutletOperationalStateEntity;}
			set 
			{
				if(_alreadyFetchedOutletOperationalStateEntity && !value)
				{
					this.OutletOperationalStateEntity = null;
				}
				_alreadyFetchedOutletOperationalStateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletOperationalStateEntity is not found
		/// in the database. When set to true, OutletOperationalStateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletOperationalStateEntityReturnsNewIfNotFound
		{
			get	{ return _outletOperationalStateEntityReturnsNewIfNotFound; }
			set { _outletOperationalStateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletSellerInformationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletSellerInformationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletSellerInformationEntity OutletSellerInformationEntity
		{
			get	{ return GetSingleOutletSellerInformationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletSellerInformationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OutletCollection", "OutletSellerInformationEntity", _outletSellerInformationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletSellerInformationEntity. When set to true, OutletSellerInformationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletSellerInformationEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletSellerInformationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletSellerInformationEntity
		{
			get	{ return _alwaysFetchOutletSellerInformationEntity; }
			set	{ _alwaysFetchOutletSellerInformationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletSellerInformationEntity already has been fetched. Setting this property to false when OutletSellerInformationEntity has been fetched
		/// will set OutletSellerInformationEntity to null as well. Setting this property to true while OutletSellerInformationEntity hasn't been fetched disables lazy loading for OutletSellerInformationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletSellerInformationEntity
		{
			get { return _alreadyFetchedOutletSellerInformationEntity;}
			set 
			{
				if(_alreadyFetchedOutletSellerInformationEntity && !value)
				{
					this.OutletSellerInformationEntity = null;
				}
				_alreadyFetchedOutletSellerInformationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletSellerInformationEntity is not found
		/// in the database. When set to true, OutletSellerInformationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletSellerInformationEntityReturnsNewIfNotFound
		{
			get	{ return _outletSellerInformationEntityReturnsNewIfNotFound; }
			set { _outletSellerInformationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliveryChargeProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity DeliveryChargeProductEntity
		{
			get	{ return GetSingleDeliveryChargeProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliveryChargeProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliveryChargeOutletCollection", "DeliveryChargeProductEntity", _deliveryChargeProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliveryChargeProductEntity. When set to true, DeliveryChargeProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliveryChargeProductEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliveryChargeProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliveryChargeProductEntity
		{
			get	{ return _alwaysFetchDeliveryChargeProductEntity; }
			set	{ _alwaysFetchDeliveryChargeProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliveryChargeProductEntity already has been fetched. Setting this property to false when DeliveryChargeProductEntity has been fetched
		/// will set DeliveryChargeProductEntity to null as well. Setting this property to true while DeliveryChargeProductEntity hasn't been fetched disables lazy loading for DeliveryChargeProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliveryChargeProductEntity
		{
			get { return _alreadyFetchedDeliveryChargeProductEntity;}
			set 
			{
				if(_alreadyFetchedDeliveryChargeProductEntity && !value)
				{
					this.DeliveryChargeProductEntity = null;
				}
				_alreadyFetchedDeliveryChargeProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliveryChargeProductEntity is not found
		/// in the database. When set to true, DeliveryChargeProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliveryChargeProductEntityReturnsNewIfNotFound
		{
			get	{ return _deliveryChargeProductEntityReturnsNewIfNotFound; }
			set { _deliveryChargeProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleServiceChargeProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ServiceChargeProductEntity
		{
			get	{ return GetSingleServiceChargeProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncServiceChargeProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ServiceChargeOutletCollection", "ServiceChargeProductEntity", _serviceChargeProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceChargeProductEntity. When set to true, ServiceChargeProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceChargeProductEntity is accessed. You can always execute a forced fetch by calling GetSingleServiceChargeProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceChargeProductEntity
		{
			get	{ return _alwaysFetchServiceChargeProductEntity; }
			set	{ _alwaysFetchServiceChargeProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceChargeProductEntity already has been fetched. Setting this property to false when ServiceChargeProductEntity has been fetched
		/// will set ServiceChargeProductEntity to null as well. Setting this property to true while ServiceChargeProductEntity hasn't been fetched disables lazy loading for ServiceChargeProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceChargeProductEntity
		{
			get { return _alreadyFetchedServiceChargeProductEntity;}
			set 
			{
				if(_alreadyFetchedServiceChargeProductEntity && !value)
				{
					this.ServiceChargeProductEntity = null;
				}
				_alreadyFetchedServiceChargeProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ServiceChargeProductEntity is not found
		/// in the database. When set to true, ServiceChargeProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ServiceChargeProductEntityReturnsNewIfNotFound
		{
			get	{ return _serviceChargeProductEntityReturnsNewIfNotFound; }
			set { _serviceChargeProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTippingProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity TippingProductEntity
		{
			get	{ return GetSingleTippingProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTippingProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TippingProductOutletCollection", "TippingProductEntity", _tippingProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TippingProductEntity. When set to true, TippingProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TippingProductEntity is accessed. You can always execute a forced fetch by calling GetSingleTippingProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTippingProductEntity
		{
			get	{ return _alwaysFetchTippingProductEntity; }
			set	{ _alwaysFetchTippingProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TippingProductEntity already has been fetched. Setting this property to false when TippingProductEntity has been fetched
		/// will set TippingProductEntity to null as well. Setting this property to true while TippingProductEntity hasn't been fetched disables lazy loading for TippingProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTippingProductEntity
		{
			get { return _alreadyFetchedTippingProductEntity;}
			set 
			{
				if(_alreadyFetchedTippingProductEntity && !value)
				{
					this.TippingProductEntity = null;
				}
				_alreadyFetchedTippingProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TippingProductEntity is not found
		/// in the database. When set to true, TippingProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TippingProductEntityReturnsNewIfNotFound
		{
			get	{ return _tippingProductEntityReturnsNewIfNotFound; }
			set { _tippingProductEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OutletEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
