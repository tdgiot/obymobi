﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PmsReportColumn'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PmsReportColumnEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PmsReportColumnEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection	_pmsReportConfigurationCollection;
		private bool	_alwaysFetchPmsReportConfigurationCollection, _alreadyFetchedPmsReportConfigurationCollection;
		private Obymobi.Data.CollectionClasses.PmsRuleCollection	_pmsRuleCollection;
		private bool	_alwaysFetchPmsRuleCollection, _alreadyFetchedPmsRuleCollection;
		private PmsReportConfigurationEntity _pmsReportConfigurationEntity;
		private bool	_alwaysFetchPmsReportConfigurationEntity, _alreadyFetchedPmsReportConfigurationEntity, _pmsReportConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PmsReportConfigurationEntity</summary>
			public static readonly string PmsReportConfigurationEntity = "PmsReportConfigurationEntity";
			/// <summary>Member name PmsReportConfigurationCollection</summary>
			public static readonly string PmsReportConfigurationCollection = "PmsReportConfigurationCollection";
			/// <summary>Member name PmsRuleCollection</summary>
			public static readonly string PmsRuleCollection = "PmsRuleCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PmsReportColumnEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PmsReportColumnEntityBase() :base("PmsReportColumnEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		protected PmsReportColumnEntityBase(System.Int32 pmsReportColumnId):base("PmsReportColumnEntity")
		{
			InitClassFetch(pmsReportColumnId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PmsReportColumnEntityBase(System.Int32 pmsReportColumnId, IPrefetchPath prefetchPathToUse): base("PmsReportColumnEntity")
		{
			InitClassFetch(pmsReportColumnId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="validator">The custom validator object for this PmsReportColumnEntity</param>
		protected PmsReportColumnEntityBase(System.Int32 pmsReportColumnId, IValidator validator):base("PmsReportColumnEntity")
		{
			InitClassFetch(pmsReportColumnId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PmsReportColumnEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pmsReportConfigurationCollection = (Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection)info.GetValue("_pmsReportConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection));
			_alwaysFetchPmsReportConfigurationCollection = info.GetBoolean("_alwaysFetchPmsReportConfigurationCollection");
			_alreadyFetchedPmsReportConfigurationCollection = info.GetBoolean("_alreadyFetchedPmsReportConfigurationCollection");

			_pmsRuleCollection = (Obymobi.Data.CollectionClasses.PmsRuleCollection)info.GetValue("_pmsRuleCollection", typeof(Obymobi.Data.CollectionClasses.PmsRuleCollection));
			_alwaysFetchPmsRuleCollection = info.GetBoolean("_alwaysFetchPmsRuleCollection");
			_alreadyFetchedPmsRuleCollection = info.GetBoolean("_alreadyFetchedPmsRuleCollection");
			_pmsReportConfigurationEntity = (PmsReportConfigurationEntity)info.GetValue("_pmsReportConfigurationEntity", typeof(PmsReportConfigurationEntity));
			if(_pmsReportConfigurationEntity!=null)
			{
				_pmsReportConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pmsReportConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_pmsReportConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchPmsReportConfigurationEntity = info.GetBoolean("_alwaysFetchPmsReportConfigurationEntity");
			_alreadyFetchedPmsReportConfigurationEntity = info.GetBoolean("_alreadyFetchedPmsReportConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PmsReportColumnFieldIndex)fieldIndex)
			{
				case PmsReportColumnFieldIndex.PmsReportConfigurationId:
					DesetupSyncPmsReportConfigurationEntity(true, false);
					_alreadyFetchedPmsReportConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPmsReportConfigurationCollection = (_pmsReportConfigurationCollection.Count > 0);
			_alreadyFetchedPmsRuleCollection = (_pmsRuleCollection.Count > 0);
			_alreadyFetchedPmsReportConfigurationEntity = (_pmsReportConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PmsReportConfigurationEntity":
					toReturn.Add(Relations.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
					break;
				case "PmsReportConfigurationCollection":
					toReturn.Add(Relations.PmsReportConfigurationEntityUsingRoomNumberColumnId);
					break;
				case "PmsRuleCollection":
					toReturn.Add(Relations.PmsRuleEntityUsingPmsReportColumnId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pmsReportConfigurationCollection", (!this.MarkedForDeletion?_pmsReportConfigurationCollection:null));
			info.AddValue("_alwaysFetchPmsReportConfigurationCollection", _alwaysFetchPmsReportConfigurationCollection);
			info.AddValue("_alreadyFetchedPmsReportConfigurationCollection", _alreadyFetchedPmsReportConfigurationCollection);
			info.AddValue("_pmsRuleCollection", (!this.MarkedForDeletion?_pmsRuleCollection:null));
			info.AddValue("_alwaysFetchPmsRuleCollection", _alwaysFetchPmsRuleCollection);
			info.AddValue("_alreadyFetchedPmsRuleCollection", _alreadyFetchedPmsRuleCollection);
			info.AddValue("_pmsReportConfigurationEntity", (!this.MarkedForDeletion?_pmsReportConfigurationEntity:null));
			info.AddValue("_pmsReportConfigurationEntityReturnsNewIfNotFound", _pmsReportConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPmsReportConfigurationEntity", _alwaysFetchPmsReportConfigurationEntity);
			info.AddValue("_alreadyFetchedPmsReportConfigurationEntity", _alreadyFetchedPmsReportConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PmsReportConfigurationEntity":
					_alreadyFetchedPmsReportConfigurationEntity = true;
					this.PmsReportConfigurationEntity = (PmsReportConfigurationEntity)entity;
					break;
				case "PmsReportConfigurationCollection":
					_alreadyFetchedPmsReportConfigurationCollection = true;
					if(entity!=null)
					{
						this.PmsReportConfigurationCollection.Add((PmsReportConfigurationEntity)entity);
					}
					break;
				case "PmsRuleCollection":
					_alreadyFetchedPmsRuleCollection = true;
					if(entity!=null)
					{
						this.PmsRuleCollection.Add((PmsRuleEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PmsReportConfigurationEntity":
					SetupSyncPmsReportConfigurationEntity(relatedEntity);
					break;
				case "PmsReportConfigurationCollection":
					_pmsReportConfigurationCollection.Add((PmsReportConfigurationEntity)relatedEntity);
					break;
				case "PmsRuleCollection":
					_pmsRuleCollection.Add((PmsRuleEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PmsReportConfigurationEntity":
					DesetupSyncPmsReportConfigurationEntity(false, true);
					break;
				case "PmsReportConfigurationCollection":
					this.PerformRelatedEntityRemoval(_pmsReportConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PmsRuleCollection":
					this.PerformRelatedEntityRemoval(_pmsRuleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pmsReportConfigurationEntity!=null)
			{
				toReturn.Add(_pmsReportConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_pmsReportConfigurationCollection);
			toReturn.Add(_pmsRuleCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsReportColumnId)
		{
			return FetchUsingPK(pmsReportColumnId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsReportColumnId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pmsReportColumnId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsReportColumnId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pmsReportColumnId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsReportColumnId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pmsReportColumnId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PmsReportColumnId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PmsReportColumnRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PmsReportConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PmsReportConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection GetMultiPmsReportConfigurationCollection(bool forceFetch)
		{
			return GetMultiPmsReportConfigurationCollection(forceFetch, _pmsReportConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsReportConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PmsReportConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection GetMultiPmsReportConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPmsReportConfigurationCollection(forceFetch, _pmsReportConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PmsReportConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection GetMultiPmsReportConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPmsReportConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsReportConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection GetMultiPmsReportConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPmsReportConfigurationCollection || forceFetch || _alwaysFetchPmsReportConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pmsReportConfigurationCollection);
				_pmsReportConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_pmsReportConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_pmsReportConfigurationCollection.GetMultiManyToOne(null, this, filter);
				_pmsReportConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPmsReportConfigurationCollection = true;
			}
			return _pmsReportConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PmsReportConfigurationCollection'. These settings will be taken into account
		/// when the property PmsReportConfigurationCollection is requested or GetMultiPmsReportConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPmsReportConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pmsReportConfigurationCollection.SortClauses=sortClauses;
			_pmsReportConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PmsRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch)
		{
			return GetMultiPmsRuleCollection(forceFetch, _pmsRuleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PmsRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPmsRuleCollection(forceFetch, _pmsRuleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPmsRuleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPmsRuleCollection || forceFetch || _alwaysFetchPmsRuleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pmsRuleCollection);
				_pmsRuleCollection.SuppressClearInGetMulti=!forceFetch;
				_pmsRuleCollection.EntityFactoryToUse = entityFactoryToUse;
				_pmsRuleCollection.GetMultiManyToOne(null, this, filter);
				_pmsRuleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPmsRuleCollection = true;
			}
			return _pmsRuleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PmsRuleCollection'. These settings will be taken into account
		/// when the property PmsRuleCollection is requested or GetMultiPmsRuleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPmsRuleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pmsRuleCollection.SortClauses=sortClauses;
			_pmsRuleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PmsReportConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PmsReportConfigurationEntity' which is related to this entity.</returns>
		public PmsReportConfigurationEntity GetSinglePmsReportConfigurationEntity()
		{
			return GetSinglePmsReportConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PmsReportConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PmsReportConfigurationEntity' which is related to this entity.</returns>
		public virtual PmsReportConfigurationEntity GetSinglePmsReportConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPmsReportConfigurationEntity || forceFetch || _alwaysFetchPmsReportConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
				PmsReportConfigurationEntity newEntity = new PmsReportConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PmsReportConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (PmsReportConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pmsReportConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PmsReportConfigurationEntity = newEntity;
				_alreadyFetchedPmsReportConfigurationEntity = fetchResult;
			}
			return _pmsReportConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PmsReportConfigurationEntity", _pmsReportConfigurationEntity);
			toReturn.Add("PmsReportConfigurationCollection", _pmsReportConfigurationCollection);
			toReturn.Add("PmsRuleCollection", _pmsRuleCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="validator">The validator object for this PmsReportColumnEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pmsReportColumnId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pmsReportColumnId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_pmsReportConfigurationCollection = new Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection();
			_pmsReportConfigurationCollection.SetContainingEntityInfo(this, "PmsReportColumnEntity");

			_pmsRuleCollection = new Obymobi.Data.CollectionClasses.PmsRuleCollection();
			_pmsRuleCollection.SetContainingEntityInfo(this, "PmsReportColumnEntity");
			_pmsReportConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsReportColumnId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsReportConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Format", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ColumnIndex", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pmsReportConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPmsReportConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pmsReportConfigurationEntity, new PropertyChangedEventHandler( OnPmsReportConfigurationEntityPropertyChanged ), "PmsReportConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsReportColumnRelations.PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic, true, signalRelatedEntity, "PmsReportColumnCollection", resetFKFields, new int[] { (int)PmsReportColumnFieldIndex.PmsReportConfigurationId } );		
			_pmsReportConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pmsReportConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPmsReportConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_pmsReportConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncPmsReportConfigurationEntity(true, true);
				_pmsReportConfigurationEntity = (PmsReportConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pmsReportConfigurationEntity, new PropertyChangedEventHandler( OnPmsReportConfigurationEntityPropertyChanged ), "PmsReportConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsReportColumnRelations.PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic, true, ref _alreadyFetchedPmsReportConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPmsReportConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pmsReportColumnId">PK value for PmsReportColumn which data should be fetched into this PmsReportColumn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pmsReportColumnId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PmsReportColumnFieldIndex.PmsReportColumnId].ForcedCurrentValueWrite(pmsReportColumnId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePmsReportColumnDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PmsReportColumnEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PmsReportColumnRelations Relations
		{
			get	{ return new PmsReportColumnRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsReportConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsReportConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection(), (IEntityRelation)GetRelationsForField("PmsReportConfigurationCollection")[0], (int)Obymobi.Data.EntityType.PmsReportColumnEntity, (int)Obymobi.Data.EntityType.PmsReportConfigurationEntity, 0, null, null, null, "PmsReportConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsRuleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsRuleCollection(), (IEntityRelation)GetRelationsForField("PmsRuleCollection")[0], (int)Obymobi.Data.EntityType.PmsReportColumnEntity, (int)Obymobi.Data.EntityType.PmsRuleEntity, 0, null, null, null, "PmsRuleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsReportConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsReportConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection(), (IEntityRelation)GetRelationsForField("PmsReportConfigurationEntity")[0], (int)Obymobi.Data.EntityType.PmsReportColumnEntity, (int)Obymobi.Data.EntityType.PmsReportConfigurationEntity, 0, null, null, null, "PmsReportConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PmsReportColumnId property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."PmsReportColumnId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PmsReportColumnId
		{
			get { return (System.Int32)GetValue((int)PmsReportColumnFieldIndex.PmsReportColumnId, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.PmsReportColumnId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsReportColumnFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The PmsReportConfigurationId property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."PmsReportConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsReportConfigurationId
		{
			get { return (System.Int32)GetValue((int)PmsReportColumnFieldIndex.PmsReportConfigurationId, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.PmsReportConfigurationId, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)PmsReportColumnFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The DataType property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."DataType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.DataType DataType
		{
			get { return (Obymobi.Enums.DataType)GetValue((int)PmsReportColumnFieldIndex.DataType, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.DataType, value, true); }
		}

		/// <summary> The Format property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."Format"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Format
		{
			get { return (System.String)GetValue((int)PmsReportColumnFieldIndex.Format, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.Format, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PmsReportColumnFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsReportColumnFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PmsReportColumnFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsReportColumnFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The ColumnIndex property of the Entity PmsReportColumn<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsReportColumn"."ColumnIndex"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ColumnIndex
		{
			get { return (System.Int32)GetValue((int)PmsReportColumnFieldIndex.ColumnIndex, true); }
			set	{ SetValue((int)PmsReportColumnFieldIndex.ColumnIndex, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PmsReportConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPmsReportConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection PmsReportConfigurationCollection
		{
			get	{ return GetMultiPmsReportConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PmsReportConfigurationCollection. When set to true, PmsReportConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsReportConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPmsReportConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsReportConfigurationCollection
		{
			get	{ return _alwaysFetchPmsReportConfigurationCollection; }
			set	{ _alwaysFetchPmsReportConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsReportConfigurationCollection already has been fetched. Setting this property to false when PmsReportConfigurationCollection has been fetched
		/// will clear the PmsReportConfigurationCollection collection well. Setting this property to true while PmsReportConfigurationCollection hasn't been fetched disables lazy loading for PmsReportConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsReportConfigurationCollection
		{
			get { return _alreadyFetchedPmsReportConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedPmsReportConfigurationCollection && !value && (_pmsReportConfigurationCollection != null))
				{
					_pmsReportConfigurationCollection.Clear();
				}
				_alreadyFetchedPmsReportConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPmsRuleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PmsRuleCollection PmsRuleCollection
		{
			get	{ return GetMultiPmsRuleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PmsRuleCollection. When set to true, PmsRuleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsRuleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPmsRuleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsRuleCollection
		{
			get	{ return _alwaysFetchPmsRuleCollection; }
			set	{ _alwaysFetchPmsRuleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsRuleCollection already has been fetched. Setting this property to false when PmsRuleCollection has been fetched
		/// will clear the PmsRuleCollection collection well. Setting this property to true while PmsRuleCollection hasn't been fetched disables lazy loading for PmsRuleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsRuleCollection
		{
			get { return _alreadyFetchedPmsRuleCollection;}
			set 
			{
				if(_alreadyFetchedPmsRuleCollection && !value && (_pmsRuleCollection != null))
				{
					_pmsRuleCollection.Clear();
				}
				_alreadyFetchedPmsRuleCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PmsReportConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePmsReportConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PmsReportConfigurationEntity PmsReportConfigurationEntity
		{
			get	{ return GetSinglePmsReportConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPmsReportConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PmsReportColumnCollection", "PmsReportConfigurationEntity", _pmsReportConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PmsReportConfigurationEntity. When set to true, PmsReportConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsReportConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSinglePmsReportConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsReportConfigurationEntity
		{
			get	{ return _alwaysFetchPmsReportConfigurationEntity; }
			set	{ _alwaysFetchPmsReportConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsReportConfigurationEntity already has been fetched. Setting this property to false when PmsReportConfigurationEntity has been fetched
		/// will set PmsReportConfigurationEntity to null as well. Setting this property to true while PmsReportConfigurationEntity hasn't been fetched disables lazy loading for PmsReportConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsReportConfigurationEntity
		{
			get { return _alreadyFetchedPmsReportConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedPmsReportConfigurationEntity && !value)
				{
					this.PmsReportConfigurationEntity = null;
				}
				_alreadyFetchedPmsReportConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PmsReportConfigurationEntity is not found
		/// in the database. When set to true, PmsReportConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PmsReportConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _pmsReportConfigurationEntityReturnsNewIfNotFound; }
			set { _pmsReportConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PmsReportColumnEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
