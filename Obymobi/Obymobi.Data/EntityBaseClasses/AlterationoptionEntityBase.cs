﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Alterationoption'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AlterationoptionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AlterationoptionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationitemCollection	_alterationitemCollection;
		private bool	_alwaysFetchAlterationitemCollection, _alreadyFetchedAlterationitemCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection	_alterationoptionLanguageCollection;
		private bool	_alwaysFetchAlterationoptionLanguageCollection, _alreadyFetchedAlterationoptionLanguageCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionTagCollection	_alterationoptionTagCollection;
		private bool	_alwaysFetchAlterationoptionTagCollection, _alreadyFetchedAlterationoptionTagCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection	_orderitemAlterationitemTagCollection;
		private bool	_alwaysFetchOrderitemAlterationitemTagCollection, _alreadyFetchedOrderitemAlterationitemTagCollection;
		private Obymobi.Data.CollectionClasses.PriceLevelItemCollection	_priceLevelItemCollection;
		private bool	_alwaysFetchPriceLevelItemCollection, _alreadyFetchedPriceLevelItemCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaMedium;
		private bool	_alwaysFetchAdvertisementCollectionViaMedium, _alreadyFetchedAdvertisementCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaMedium;
		private bool	_alwaysFetchAlterationCollectionViaMedium, _alreadyFetchedAlterationCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium_;
		private bool	_alwaysFetchCategoryCollectionViaMedium_, _alreadyFetchedCategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMedium;
		private bool	_alwaysFetchCompanyCollectionViaMedium, _alreadyFetchedCompanyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaMedium;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaMedium, _alreadyFetchedDeliverypointgroupCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium_, _alreadyFetchedEntertainmentCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaMedium;
		private bool	_alwaysFetchGenericcategoryCollectionViaMedium, _alreadyFetchedGenericcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaMedium;
		private bool	_alwaysFetchGenericproductCollectionViaMedium, _alreadyFetchedGenericproductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium_;
		private bool	_alwaysFetchProductCollectionViaMedium_, _alreadyFetchedProductCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.SurveyCollection _surveyCollectionViaMedium;
		private bool	_alwaysFetchSurveyCollectionViaMedium, _alreadyFetchedSurveyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private ExternalProductEntity _externalProductEntity;
		private bool	_alwaysFetchExternalProductEntity, _alreadyFetchedExternalProductEntity, _externalProductEntityReturnsNewIfNotFound;
		private GenericalterationoptionEntity _genericalterationoptionEntity;
		private bool	_alwaysFetchGenericalterationoptionEntity, _alreadyFetchedGenericalterationoptionEntity, _genericalterationoptionEntityReturnsNewIfNotFound;
		private PosalterationoptionEntity _posalterationoptionEntity;
		private bool	_alwaysFetchPosalterationoptionEntity, _alreadyFetchedPosalterationoptionEntity, _posalterationoptionEntityReturnsNewIfNotFound;
		private PosproductEntity _posproductEntity;
		private bool	_alwaysFetchPosproductEntity, _alreadyFetchedPosproductEntity, _posproductEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private TaxTariffEntity _taxTariffEntity;
		private bool	_alwaysFetchTaxTariffEntity, _alreadyFetchedTaxTariffEntity, _taxTariffEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ExternalProductEntity</summary>
			public static readonly string ExternalProductEntity = "ExternalProductEntity";
			/// <summary>Member name GenericalterationoptionEntity</summary>
			public static readonly string GenericalterationoptionEntity = "GenericalterationoptionEntity";
			/// <summary>Member name PosalterationoptionEntity</summary>
			public static readonly string PosalterationoptionEntity = "PosalterationoptionEntity";
			/// <summary>Member name PosproductEntity</summary>
			public static readonly string PosproductEntity = "PosproductEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name TaxTariffEntity</summary>
			public static readonly string TaxTariffEntity = "TaxTariffEntity";
			/// <summary>Member name AlterationitemCollection</summary>
			public static readonly string AlterationitemCollection = "AlterationitemCollection";
			/// <summary>Member name AlterationoptionLanguageCollection</summary>
			public static readonly string AlterationoptionLanguageCollection = "AlterationoptionLanguageCollection";
			/// <summary>Member name AlterationoptionTagCollection</summary>
			public static readonly string AlterationoptionTagCollection = "AlterationoptionTagCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name AdvertisementCollectionViaMedium</summary>
			public static readonly string AdvertisementCollectionViaMedium = "AdvertisementCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaMedium</summary>
			public static readonly string AlterationCollectionViaMedium = "AlterationCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium_</summary>
			public static readonly string CategoryCollectionViaMedium_ = "CategoryCollectionViaMedium_";
			/// <summary>Member name CompanyCollectionViaMedium</summary>
			public static readonly string CompanyCollectionViaMedium = "CompanyCollectionViaMedium";
			/// <summary>Member name DeliverypointgroupCollectionViaMedium</summary>
			public static readonly string DeliverypointgroupCollectionViaMedium = "DeliverypointgroupCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium_</summary>
			public static readonly string EntertainmentCollectionViaMedium_ = "EntertainmentCollectionViaMedium_";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name GenericcategoryCollectionViaMedium</summary>
			public static readonly string GenericcategoryCollectionViaMedium = "GenericcategoryCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaMedium</summary>
			public static readonly string GenericproductCollectionViaMedium = "GenericproductCollectionViaMedium";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium_</summary>
			public static readonly string ProductCollectionViaMedium_ = "ProductCollectionViaMedium_";
			/// <summary>Member name SurveyCollectionViaMedium</summary>
			public static readonly string SurveyCollectionViaMedium = "SurveyCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AlterationoptionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AlterationoptionEntityBase() :base("AlterationoptionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		protected AlterationoptionEntityBase(System.Int32 alterationoptionId):base("AlterationoptionEntity")
		{
			InitClassFetch(alterationoptionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AlterationoptionEntityBase(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse): base("AlterationoptionEntity")
		{
			InitClassFetch(alterationoptionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="validator">The custom validator object for this AlterationoptionEntity</param>
		protected AlterationoptionEntityBase(System.Int32 alterationoptionId, IValidator validator):base("AlterationoptionEntity")
		{
			InitClassFetch(alterationoptionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationoptionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationitemCollection = (Obymobi.Data.CollectionClasses.AlterationitemCollection)info.GetValue("_alterationitemCollection", typeof(Obymobi.Data.CollectionClasses.AlterationitemCollection));
			_alwaysFetchAlterationitemCollection = info.GetBoolean("_alwaysFetchAlterationitemCollection");
			_alreadyFetchedAlterationitemCollection = info.GetBoolean("_alreadyFetchedAlterationitemCollection");

			_alterationoptionLanguageCollection = (Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection)info.GetValue("_alterationoptionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection));
			_alwaysFetchAlterationoptionLanguageCollection = info.GetBoolean("_alwaysFetchAlterationoptionLanguageCollection");
			_alreadyFetchedAlterationoptionLanguageCollection = info.GetBoolean("_alreadyFetchedAlterationoptionLanguageCollection");

			_alterationoptionTagCollection = (Obymobi.Data.CollectionClasses.AlterationoptionTagCollection)info.GetValue("_alterationoptionTagCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionTagCollection));
			_alwaysFetchAlterationoptionTagCollection = info.GetBoolean("_alwaysFetchAlterationoptionTagCollection");
			_alreadyFetchedAlterationoptionTagCollection = info.GetBoolean("_alreadyFetchedAlterationoptionTagCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_orderitemAlterationitemTagCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection)info.GetValue("_orderitemAlterationitemTagCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection));
			_alwaysFetchOrderitemAlterationitemTagCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemTagCollection");
			_alreadyFetchedOrderitemAlterationitemTagCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemTagCollection");

			_priceLevelItemCollection = (Obymobi.Data.CollectionClasses.PriceLevelItemCollection)info.GetValue("_priceLevelItemCollection", typeof(Obymobi.Data.CollectionClasses.PriceLevelItemCollection));
			_alwaysFetchPriceLevelItemCollection = info.GetBoolean("_alwaysFetchPriceLevelItemCollection");
			_alreadyFetchedPriceLevelItemCollection = info.GetBoolean("_alreadyFetchedPriceLevelItemCollection");
			_advertisementCollectionViaMedium = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaMedium = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaMedium");
			_alreadyFetchedAdvertisementCollectionViaMedium = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaMedium");

			_alterationCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationCollectionViaMedium");
			_alreadyFetchedAlterationCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium_");
			_alreadyFetchedCategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium_");

			_companyCollectionViaMedium = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMedium = info.GetBoolean("_alwaysFetchCompanyCollectionViaMedium");
			_alreadyFetchedCompanyCollectionViaMedium = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMedium");

			_deliverypointgroupCollectionViaMedium = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaMedium");
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaMedium");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium_");
			_alreadyFetchedEntertainmentCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium_");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_genericcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaMedium");
			_alreadyFetchedGenericcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaMedium");

			_genericproductCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericproductCollectionViaMedium");
			_alreadyFetchedGenericproductCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaMedium");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium_ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium_");
			_alreadyFetchedProductCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium_");

			_surveyCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyCollection)info.GetValue("_surveyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyCollection));
			_alwaysFetchSurveyCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyCollectionViaMedium");
			_alreadyFetchedSurveyCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyCollectionViaMedium");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");
			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_externalProductEntity = (ExternalProductEntity)info.GetValue("_externalProductEntity", typeof(ExternalProductEntity));
			if(_externalProductEntity!=null)
			{
				_externalProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalProductEntityReturnsNewIfNotFound = info.GetBoolean("_externalProductEntityReturnsNewIfNotFound");
			_alwaysFetchExternalProductEntity = info.GetBoolean("_alwaysFetchExternalProductEntity");
			_alreadyFetchedExternalProductEntity = info.GetBoolean("_alreadyFetchedExternalProductEntity");

			_genericalterationoptionEntity = (GenericalterationoptionEntity)info.GetValue("_genericalterationoptionEntity", typeof(GenericalterationoptionEntity));
			if(_genericalterationoptionEntity!=null)
			{
				_genericalterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericalterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_genericalterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchGenericalterationoptionEntity = info.GetBoolean("_alwaysFetchGenericalterationoptionEntity");
			_alreadyFetchedGenericalterationoptionEntity = info.GetBoolean("_alreadyFetchedGenericalterationoptionEntity");

			_posalterationoptionEntity = (PosalterationoptionEntity)info.GetValue("_posalterationoptionEntity", typeof(PosalterationoptionEntity));
			if(_posalterationoptionEntity!=null)
			{
				_posalterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posalterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_posalterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchPosalterationoptionEntity = info.GetBoolean("_alwaysFetchPosalterationoptionEntity");
			_alreadyFetchedPosalterationoptionEntity = info.GetBoolean("_alreadyFetchedPosalterationoptionEntity");

			_posproductEntity = (PosproductEntity)info.GetValue("_posproductEntity", typeof(PosproductEntity));
			if(_posproductEntity!=null)
			{
				_posproductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posproductEntityReturnsNewIfNotFound = info.GetBoolean("_posproductEntityReturnsNewIfNotFound");
			_alwaysFetchPosproductEntity = info.GetBoolean("_alwaysFetchPosproductEntity");
			_alreadyFetchedPosproductEntity = info.GetBoolean("_alreadyFetchedPosproductEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_taxTariffEntity = (TaxTariffEntity)info.GetValue("_taxTariffEntity", typeof(TaxTariffEntity));
			if(_taxTariffEntity!=null)
			{
				_taxTariffEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_taxTariffEntityReturnsNewIfNotFound = info.GetBoolean("_taxTariffEntityReturnsNewIfNotFound");
			_alwaysFetchTaxTariffEntity = info.GetBoolean("_alwaysFetchTaxTariffEntity");
			_alreadyFetchedTaxTariffEntity = info.GetBoolean("_alreadyFetchedTaxTariffEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AlterationoptionFieldIndex)fieldIndex)
			{
				case AlterationoptionFieldIndex.PosalterationoptionId:
					DesetupSyncPosalterationoptionEntity(true, false);
					_alreadyFetchedPosalterationoptionEntity = false;
					break;
				case AlterationoptionFieldIndex.PosproductId:
					DesetupSyncPosproductEntity(true, false);
					_alreadyFetchedPosproductEntity = false;
					break;
				case AlterationoptionFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case AlterationoptionFieldIndex.GenericalterationoptionId:
					DesetupSyncGenericalterationoptionEntity(true, false);
					_alreadyFetchedGenericalterationoptionEntity = false;
					break;
				case AlterationoptionFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				case AlterationoptionFieldIndex.TaxTariffId:
					DesetupSyncTaxTariffEntity(true, false);
					_alreadyFetchedTaxTariffEntity = false;
					break;
				case AlterationoptionFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case AlterationoptionFieldIndex.ExternalProductId:
					DesetupSyncExternalProductEntity(true, false);
					_alreadyFetchedExternalProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationitemCollection = (_alterationitemCollection.Count > 0);
			_alreadyFetchedAlterationoptionLanguageCollection = (_alterationoptionLanguageCollection.Count > 0);
			_alreadyFetchedAlterationoptionTagCollection = (_alterationoptionTagCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedOrderitemAlterationitemTagCollection = (_orderitemAlterationitemTagCollection.Count > 0);
			_alreadyFetchedPriceLevelItemCollection = (_priceLevelItemCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaMedium = (_advertisementCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaMedium = (_alterationCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium_ = (_categoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedCompanyCollectionViaMedium = (_companyCollectionViaMedium.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = (_deliverypointgroupCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium_ = (_entertainmentCollectionViaMedium_.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaMedium = (_genericcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaMedium = (_genericproductCollectionViaMedium.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium_ = (_productCollectionViaMedium_.Count > 0);
			_alreadyFetchedSurveyCollectionViaMedium = (_surveyCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedBrandEntity = (_brandEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedExternalProductEntity = (_externalProductEntity != null);
			_alreadyFetchedGenericalterationoptionEntity = (_genericalterationoptionEntity != null);
			_alreadyFetchedPosalterationoptionEntity = (_posalterationoptionEntity != null);
			_alreadyFetchedPosproductEntity = (_posproductEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedTaxTariffEntity = (_taxTariffEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ExternalProductEntity":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalProductId);
					break;
				case "GenericalterationoptionEntity":
					toReturn.Add(Relations.GenericalterationoptionEntityUsingGenericalterationoptionId);
					break;
				case "PosalterationoptionEntity":
					toReturn.Add(Relations.PosalterationoptionEntityUsingPosalterationoptionId);
					break;
				case "PosproductEntity":
					toReturn.Add(Relations.PosproductEntityUsingPosproductId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "TaxTariffEntity":
					toReturn.Add(Relations.TaxTariffEntityUsingTaxTariffId);
					break;
				case "AlterationitemCollection":
					toReturn.Add(Relations.AlterationitemEntityUsingAlterationoptionId);
					break;
				case "AlterationoptionLanguageCollection":
					toReturn.Add(Relations.AlterationoptionLanguageEntityUsingAlterationoptionId);
					break;
				case "AlterationoptionTagCollection":
					toReturn.Add(Relations.AlterationoptionTagEntityUsingAlterationoptionId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingAlterationoptionId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId);
					break;
				case "OrderitemAlterationitemTagCollection":
					toReturn.Add(Relations.OrderitemAlterationitemTagEntityUsingAlterationoptionId);
					break;
				case "PriceLevelItemCollection":
					toReturn.Add(Relations.PriceLevelItemEntityUsingAlterationoptionId);
					break;
				case "AdvertisementCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CompanyEntityUsingCompanyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericproductEntityUsingGenericproductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationoptionId, "AlterationoptionEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationitemCollection", (!this.MarkedForDeletion?_alterationitemCollection:null));
			info.AddValue("_alwaysFetchAlterationitemCollection", _alwaysFetchAlterationitemCollection);
			info.AddValue("_alreadyFetchedAlterationitemCollection", _alreadyFetchedAlterationitemCollection);
			info.AddValue("_alterationoptionLanguageCollection", (!this.MarkedForDeletion?_alterationoptionLanguageCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionLanguageCollection", _alwaysFetchAlterationoptionLanguageCollection);
			info.AddValue("_alreadyFetchedAlterationoptionLanguageCollection", _alreadyFetchedAlterationoptionLanguageCollection);
			info.AddValue("_alterationoptionTagCollection", (!this.MarkedForDeletion?_alterationoptionTagCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionTagCollection", _alwaysFetchAlterationoptionTagCollection);
			info.AddValue("_alreadyFetchedAlterationoptionTagCollection", _alreadyFetchedAlterationoptionTagCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_orderitemAlterationitemTagCollection", (!this.MarkedForDeletion?_orderitemAlterationitemTagCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemTagCollection", _alwaysFetchOrderitemAlterationitemTagCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemTagCollection", _alreadyFetchedOrderitemAlterationitemTagCollection);
			info.AddValue("_priceLevelItemCollection", (!this.MarkedForDeletion?_priceLevelItemCollection:null));
			info.AddValue("_alwaysFetchPriceLevelItemCollection", _alwaysFetchPriceLevelItemCollection);
			info.AddValue("_alreadyFetchedPriceLevelItemCollection", _alreadyFetchedPriceLevelItemCollection);
			info.AddValue("_advertisementCollectionViaMedium", (!this.MarkedForDeletion?_advertisementCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaMedium", _alwaysFetchAdvertisementCollectionViaMedium);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaMedium", _alreadyFetchedAdvertisementCollectionViaMedium);
			info.AddValue("_alterationCollectionViaMedium", (!this.MarkedForDeletion?_alterationCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaMedium", _alwaysFetchAlterationCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationCollectionViaMedium", _alreadyFetchedAlterationCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium_", (!this.MarkedForDeletion?_categoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium_", _alwaysFetchCategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium_", _alreadyFetchedCategoryCollectionViaMedium_);
			info.AddValue("_companyCollectionViaMedium", (!this.MarkedForDeletion?_companyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMedium", _alwaysFetchCompanyCollectionViaMedium);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMedium", _alreadyFetchedCompanyCollectionViaMedium);
			info.AddValue("_deliverypointgroupCollectionViaMedium", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaMedium:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaMedium", _alwaysFetchDeliverypointgroupCollectionViaMedium);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaMedium", _alreadyFetchedDeliverypointgroupCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium_", _alwaysFetchEntertainmentCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium_", _alreadyFetchedEntertainmentCollectionViaMedium_);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_genericcategoryCollectionViaMedium", (!this.MarkedForDeletion?_genericcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaMedium", _alwaysFetchGenericcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaMedium", _alreadyFetchedGenericcategoryCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaMedium", (!this.MarkedForDeletion?_genericproductCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaMedium", _alwaysFetchGenericproductCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaMedium", _alreadyFetchedGenericproductCollectionViaMedium);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium_", (!this.MarkedForDeletion?_productCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium_", _alwaysFetchProductCollectionViaMedium_);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium_", _alreadyFetchedProductCollectionViaMedium_);
			info.AddValue("_surveyCollectionViaMedium", (!this.MarkedForDeletion?_surveyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyCollectionViaMedium", _alwaysFetchSurveyCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyCollectionViaMedium", _alreadyFetchedSurveyCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_externalProductEntity", (!this.MarkedForDeletion?_externalProductEntity:null));
			info.AddValue("_externalProductEntityReturnsNewIfNotFound", _externalProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalProductEntity", _alwaysFetchExternalProductEntity);
			info.AddValue("_alreadyFetchedExternalProductEntity", _alreadyFetchedExternalProductEntity);
			info.AddValue("_genericalterationoptionEntity", (!this.MarkedForDeletion?_genericalterationoptionEntity:null));
			info.AddValue("_genericalterationoptionEntityReturnsNewIfNotFound", _genericalterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericalterationoptionEntity", _alwaysFetchGenericalterationoptionEntity);
			info.AddValue("_alreadyFetchedGenericalterationoptionEntity", _alreadyFetchedGenericalterationoptionEntity);
			info.AddValue("_posalterationoptionEntity", (!this.MarkedForDeletion?_posalterationoptionEntity:null));
			info.AddValue("_posalterationoptionEntityReturnsNewIfNotFound", _posalterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosalterationoptionEntity", _alwaysFetchPosalterationoptionEntity);
			info.AddValue("_alreadyFetchedPosalterationoptionEntity", _alreadyFetchedPosalterationoptionEntity);
			info.AddValue("_posproductEntity", (!this.MarkedForDeletion?_posproductEntity:null));
			info.AddValue("_posproductEntityReturnsNewIfNotFound", _posproductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosproductEntity", _alwaysFetchPosproductEntity);
			info.AddValue("_alreadyFetchedPosproductEntity", _alreadyFetchedPosproductEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_taxTariffEntity", (!this.MarkedForDeletion?_taxTariffEntity:null));
			info.AddValue("_taxTariffEntityReturnsNewIfNotFound", _taxTariffEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTaxTariffEntity", _alwaysFetchTaxTariffEntity);
			info.AddValue("_alreadyFetchedTaxTariffEntity", _alreadyFetchedTaxTariffEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ExternalProductEntity":
					_alreadyFetchedExternalProductEntity = true;
					this.ExternalProductEntity = (ExternalProductEntity)entity;
					break;
				case "GenericalterationoptionEntity":
					_alreadyFetchedGenericalterationoptionEntity = true;
					this.GenericalterationoptionEntity = (GenericalterationoptionEntity)entity;
					break;
				case "PosalterationoptionEntity":
					_alreadyFetchedPosalterationoptionEntity = true;
					this.PosalterationoptionEntity = (PosalterationoptionEntity)entity;
					break;
				case "PosproductEntity":
					_alreadyFetchedPosproductEntity = true;
					this.PosproductEntity = (PosproductEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "TaxTariffEntity":
					_alreadyFetchedTaxTariffEntity = true;
					this.TaxTariffEntity = (TaxTariffEntity)entity;
					break;
				case "AlterationitemCollection":
					_alreadyFetchedAlterationitemCollection = true;
					if(entity!=null)
					{
						this.AlterationitemCollection.Add((AlterationitemEntity)entity);
					}
					break;
				case "AlterationoptionLanguageCollection":
					_alreadyFetchedAlterationoptionLanguageCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionLanguageCollection.Add((AlterationoptionLanguageEntity)entity);
					}
					break;
				case "AlterationoptionTagCollection":
					_alreadyFetchedAlterationoptionTagCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionTagCollection.Add((AlterationoptionTagEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "OrderitemAlterationitemTagCollection":
					_alreadyFetchedOrderitemAlterationitemTagCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)entity);
					}
					break;
				case "PriceLevelItemCollection":
					_alreadyFetchedPriceLevelItemCollection = true;
					if(entity!=null)
					{
						this.PriceLevelItemCollection.Add((PriceLevelItemEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaMedium":
					_alreadyFetchedAdvertisementCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaMedium.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationCollectionViaMedium":
					_alreadyFetchedAlterationCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaMedium.Add((AlterationEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium_":
					_alreadyFetchedCategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium_.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaMedium":
					_alreadyFetchedCompanyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMedium.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaMedium":
					_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaMedium.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium_":
					_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaMedium":
					_alreadyFetchedGenericcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaMedium.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaMedium":
					_alreadyFetchedGenericproductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaMedium.Add((GenericproductEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium_":
					_alreadyFetchedProductCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium_.Add((ProductEntity)entity);
					}
					break;
				case "SurveyCollectionViaMedium":
					_alreadyFetchedSurveyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyCollectionViaMedium.Add((SurveyEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ExternalProductEntity":
					SetupSyncExternalProductEntity(relatedEntity);
					break;
				case "GenericalterationoptionEntity":
					SetupSyncGenericalterationoptionEntity(relatedEntity);
					break;
				case "PosalterationoptionEntity":
					SetupSyncPosalterationoptionEntity(relatedEntity);
					break;
				case "PosproductEntity":
					SetupSyncPosproductEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "TaxTariffEntity":
					SetupSyncTaxTariffEntity(relatedEntity);
					break;
				case "AlterationitemCollection":
					_alterationitemCollection.Add((AlterationitemEntity)relatedEntity);
					break;
				case "AlterationoptionLanguageCollection":
					_alterationoptionLanguageCollection.Add((AlterationoptionLanguageEntity)relatedEntity);
					break;
				case "AlterationoptionTagCollection":
					_alterationoptionTagCollection.Add((AlterationoptionTagEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "OrderitemAlterationitemTagCollection":
					_orderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)relatedEntity);
					break;
				case "PriceLevelItemCollection":
					_priceLevelItemCollection.Add((PriceLevelItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ExternalProductEntity":
					DesetupSyncExternalProductEntity(false, true);
					break;
				case "GenericalterationoptionEntity":
					DesetupSyncGenericalterationoptionEntity(false, true);
					break;
				case "PosalterationoptionEntity":
					DesetupSyncPosalterationoptionEntity(false, true);
					break;
				case "PosproductEntity":
					DesetupSyncPosproductEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "TaxTariffEntity":
					DesetupSyncTaxTariffEntity(false, true);
					break;
				case "AlterationitemCollection":
					this.PerformRelatedEntityRemoval(_alterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationoptionLanguageCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationoptionTagCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemAlterationitemTagCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PriceLevelItemCollection":
					this.PerformRelatedEntityRemoval(_priceLevelItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_externalProductEntity!=null)
			{
				toReturn.Add(_externalProductEntity);
			}
			if(_genericalterationoptionEntity!=null)
			{
				toReturn.Add(_genericalterationoptionEntity);
			}
			if(_posalterationoptionEntity!=null)
			{
				toReturn.Add(_posalterationoptionEntity);
			}
			if(_posproductEntity!=null)
			{
				toReturn.Add(_posproductEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_taxTariffEntity!=null)
			{
				toReturn.Add(_taxTariffEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationitemCollection);
			toReturn.Add(_alterationoptionLanguageCollection);
			toReturn.Add(_alterationoptionTagCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_orderitemAlterationitemTagCollection);
			toReturn.Add(_priceLevelItemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionId)
		{
			return FetchUsingPK(alterationoptionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(alterationoptionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(alterationoptionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(alterationoptionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AlterationoptionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AlterationoptionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch)
		{
			return GetMultiAlterationitemCollection(forceFetch, _alterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationitemCollection(forceFetch, _alterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationitemCollection || forceFetch || _alwaysFetchAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationitemCollection);
				_alterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationitemCollection.GetMultiManyToOne(null, this, null, filter);
				_alterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationitemCollection = true;
			}
			return _alterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationitemCollection'. These settings will be taken into account
		/// when the property AlterationitemCollection is requested or GetMultiAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationitemCollection.SortClauses=sortClauses;
			_alterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, _alterationoptionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, _alterationoptionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionLanguageCollection || forceFetch || _alwaysFetchAlterationoptionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionLanguageCollection);
				_alterationoptionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionLanguageCollection.GetMultiManyToOne(this, null, filter);
				_alterationoptionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionLanguageCollection = true;
			}
			return _alterationoptionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionLanguageCollection'. These settings will be taken into account
		/// when the property AlterationoptionLanguageCollection is requested or GetMultiAlterationoptionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionLanguageCollection.SortClauses=sortClauses;
			_alterationoptionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, _alterationoptionTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, _alterationoptionTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionTagCollection || forceFetch || _alwaysFetchAlterationoptionTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionTagCollection);
				_alterationoptionTagCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionTagCollection.GetMultiManyToOne(this, null, filter);
				_alterationoptionTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionTagCollection = true;
			}
			return _alterationoptionTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionTagCollection'. These settings will be taken into account
		/// when the property AlterationoptionTagCollection is requested or GetMultiAlterationoptionTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionTagCollection.SortClauses=sortClauses;
			_alterationoptionTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemTagCollection || forceFetch || _alwaysFetchOrderitemAlterationitemTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemTagCollection);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemTagCollection.GetMultiManyToOne(this, null, null, filter);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemTagCollection = true;
			}
			return _orderitemAlterationitemTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemTagCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemTagCollection is requested or GetMultiOrderitemAlterationitemTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemTagCollection.SortClauses=sortClauses;
			_orderitemAlterationitemTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPriceLevelItemCollection || forceFetch || _alwaysFetchPriceLevelItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_priceLevelItemCollection);
				_priceLevelItemCollection.SuppressClearInGetMulti=!forceFetch;
				_priceLevelItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_priceLevelItemCollection.GetMultiManyToOne(null, this, null, null, filter);
				_priceLevelItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPriceLevelItemCollection = true;
			}
			return _priceLevelItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PriceLevelItemCollection'. These settings will be taken into account
		/// when the property PriceLevelItemCollection is requested or GetMultiPriceLevelItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPriceLevelItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_priceLevelItemCollection.SortClauses=sortClauses;
			_priceLevelItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaMedium(forceFetch, _advertisementCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaMedium || forceFetch || _alwaysFetchAdvertisementCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaMedium.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaMedium"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaMedium = true;
			}
			return _advertisementCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaMedium'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaMedium is requested or GetMultiAdvertisementCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaMedium.SortClauses=sortClauses;
			_advertisementCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaMedium(forceFetch, _alterationCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaMedium || forceFetch || _alwaysFetchAlterationCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationCollectionViaMedium"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaMedium = true;
			}
			return _alterationCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationCollectionViaMedium is requested or GetMultiAlterationCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaMedium.SortClauses=sortClauses;
			_alterationCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium_(forceFetch, _categoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium_ || forceFetch || _alwaysFetchCategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium_"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium_ = true;
			}
			return _categoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium_ is requested or GetMultiCategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium_.SortClauses=sortClauses;
			_categoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMedium(forceFetch, _companyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMedium || forceFetch || _alwaysFetchCompanyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMedium.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMedium"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMedium = true;
			}
			return _companyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMedium'. These settings will be taken into account
		/// when the property CompanyCollectionViaMedium is requested or GetMultiCompanyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMedium.SortClauses=sortClauses;
			_companyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaMedium(forceFetch, _deliverypointgroupCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaMedium || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaMedium.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaMedium"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
			}
			return _deliverypointgroupCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaMedium'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaMedium is requested or GetMultiDeliverypointgroupCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaMedium.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium_(forceFetch, _entertainmentCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium_"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
			}
			return _entertainmentCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium_ is requested or GetMultiEntertainmentCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaMedium(forceFetch, _genericcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaMedium || forceFetch || _alwaysFetchGenericcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaMedium"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaMedium = true;
			}
			return _genericcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaMedium is requested or GetMultiGenericcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaMedium.SortClauses=sortClauses;
			_genericcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaMedium(forceFetch, _genericproductCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaMedium || forceFetch || _alwaysFetchGenericproductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaMedium"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaMedium = true;
			}
			return _genericproductCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericproductCollectionViaMedium is requested or GetMultiGenericproductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaMedium.SortClauses=sortClauses;
			_genericproductCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium_(forceFetch, _productCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium_ || forceFetch || _alwaysFetchProductCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium_.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium_"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium_ = true;
			}
			return _productCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium_'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium_ is requested or GetMultiProductCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium_.SortClauses=sortClauses;
			_productCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyCollectionViaMedium(forceFetch, _surveyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyCollectionViaMedium || forceFetch || _alwaysFetchSurveyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyCollectionViaMedium"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyCollectionViaMedium = true;
			}
			return _surveyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyCollectionViaMedium is requested or GetMultiSurveyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyCollectionViaMedium.SortClauses=sortClauses;
			_surveyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationoptionFields.AlterationoptionId, ComparisonOperator.Equal, this.AlterationoptionId, "AlterationoptionEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public ExternalProductEntity GetSingleExternalProductEntity()
		{
			return GetSingleExternalProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public virtual ExternalProductEntity GetSingleExternalProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalProductEntity || forceFetch || _alwaysFetchExternalProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalProductEntityUsingExternalProductId);
				ExternalProductEntity newEntity = new ExternalProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalProductEntity = newEntity;
				_alreadyFetchedExternalProductEntity = fetchResult;
			}
			return _externalProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericalterationoptionEntity' which is related to this entity.</returns>
		public GenericalterationoptionEntity GetSingleGenericalterationoptionEntity()
		{
			return GetSingleGenericalterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericalterationoptionEntity' which is related to this entity.</returns>
		public virtual GenericalterationoptionEntity GetSingleGenericalterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericalterationoptionEntity || forceFetch || _alwaysFetchGenericalterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericalterationoptionEntityUsingGenericalterationoptionId);
				GenericalterationoptionEntity newEntity = new GenericalterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericalterationoptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericalterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericalterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericalterationoptionEntity = newEntity;
				_alreadyFetchedGenericalterationoptionEntity = fetchResult;
			}
			return _genericalterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosalterationoptionEntity' which is related to this entity.</returns>
		public PosalterationoptionEntity GetSinglePosalterationoptionEntity()
		{
			return GetSinglePosalterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosalterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosalterationoptionEntity' which is related to this entity.</returns>
		public virtual PosalterationoptionEntity GetSinglePosalterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosalterationoptionEntity || forceFetch || _alwaysFetchPosalterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosalterationoptionEntityUsingPosalterationoptionId);
				PosalterationoptionEntity newEntity = new PosalterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosalterationoptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosalterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posalterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosalterationoptionEntity = newEntity;
				_alreadyFetchedPosalterationoptionEntity = fetchResult;
			}
			return _posalterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosproductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosproductEntity' which is related to this entity.</returns>
		public PosproductEntity GetSinglePosproductEntity()
		{
			return GetSinglePosproductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosproductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosproductEntity' which is related to this entity.</returns>
		public virtual PosproductEntity GetSinglePosproductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosproductEntity || forceFetch || _alwaysFetchPosproductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosproductEntityUsingPosproductId);
				PosproductEntity newEntity = new PosproductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosproductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosproductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posproductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosproductEntity = newEntity;
				_alreadyFetchedPosproductEntity = fetchResult;
			}
			return _posproductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public TaxTariffEntity GetSingleTaxTariffEntity()
		{
			return GetSingleTaxTariffEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public virtual TaxTariffEntity GetSingleTaxTariffEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTaxTariffEntity || forceFetch || _alwaysFetchTaxTariffEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TaxTariffEntityUsingTaxTariffId);
				TaxTariffEntity newEntity = new TaxTariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TaxTariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TaxTariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_taxTariffEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TaxTariffEntity = newEntity;
				_alreadyFetchedTaxTariffEntity = fetchResult;
			}
			return _taxTariffEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ExternalProductEntity", _externalProductEntity);
			toReturn.Add("GenericalterationoptionEntity", _genericalterationoptionEntity);
			toReturn.Add("PosalterationoptionEntity", _posalterationoptionEntity);
			toReturn.Add("PosproductEntity", _posproductEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("TaxTariffEntity", _taxTariffEntity);
			toReturn.Add("AlterationitemCollection", _alterationitemCollection);
			toReturn.Add("AlterationoptionLanguageCollection", _alterationoptionLanguageCollection);
			toReturn.Add("AlterationoptionTagCollection", _alterationoptionTagCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("OrderitemAlterationitemTagCollection", _orderitemAlterationitemTagCollection);
			toReturn.Add("PriceLevelItemCollection", _priceLevelItemCollection);
			toReturn.Add("AdvertisementCollectionViaMedium", _advertisementCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaMedium", _alterationCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium_", _categoryCollectionViaMedium_);
			toReturn.Add("CompanyCollectionViaMedium", _companyCollectionViaMedium);
			toReturn.Add("DeliverypointgroupCollectionViaMedium", _deliverypointgroupCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium_", _entertainmentCollectionViaMedium_);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("GenericcategoryCollectionViaMedium", _genericcategoryCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaMedium", _genericproductCollectionViaMedium);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium_", _productCollectionViaMedium_);
			toReturn.Add("SurveyCollectionViaMedium", _surveyCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="validator">The validator object for this AlterationoptionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 alterationoptionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(alterationoptionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationitemCollection = new Obymobi.Data.CollectionClasses.AlterationitemCollection();
			_alterationitemCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_alterationoptionLanguageCollection = new Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection();
			_alterationoptionLanguageCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_alterationoptionTagCollection = new Obymobi.Data.CollectionClasses.AlterationoptionTagCollection();
			_alterationoptionTagCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_orderitemAlterationitemTagCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection();
			_orderitemAlterationitemTagCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");

			_priceLevelItemCollection = new Obymobi.Data.CollectionClasses.PriceLevelItemCollection();
			_priceLevelItemCollection.SetContainingEntityInfo(this, "AlterationoptionEntity");
			_advertisementCollectionViaMedium = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaMedium = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointgroupCollectionViaMedium = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_surveyCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_brandEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_externalProductEntityReturnsNewIfNotFound = true;
			_genericalterationoptionEntityReturnsNewIfNotFound = true;
			_posalterationoptionEntityReturnsNewIfNotFound = true;
			_posproductEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_taxTariffEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosalterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosproductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProductRelated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XPriceAddition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinLeadMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxLeadHours", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntervalMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowDatePicker", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAlcoholic", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritPrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritTaxTariff", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritIsAlcoholic", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritTags", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritCustomText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAvailable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.ExternalProductEntityUsingExternalProductIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.ExternalProductId } );		
			_externalProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalProductEntity(IEntityCore relatedEntity)
		{
			if(_externalProductEntity!=relatedEntity)
			{		
				DesetupSyncExternalProductEntity(true, true);
				_externalProductEntity = (ExternalProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.ExternalProductEntityUsingExternalProductIdStatic, true, ref _alreadyFetchedExternalProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericalterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericalterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericalterationoptionEntity, new PropertyChangedEventHandler( OnGenericalterationoptionEntityPropertyChanged ), "GenericalterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.GenericalterationoptionEntityUsingGenericalterationoptionIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.GenericalterationoptionId } );		
			_genericalterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericalterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericalterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_genericalterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncGenericalterationoptionEntity(true, true);
				_genericalterationoptionEntity = (GenericalterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericalterationoptionEntity, new PropertyChangedEventHandler( OnGenericalterationoptionEntityPropertyChanged ), "GenericalterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.GenericalterationoptionEntityUsingGenericalterationoptionIdStatic, true, ref _alreadyFetchedGenericalterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericalterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posalterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosalterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posalterationoptionEntity, new PropertyChangedEventHandler( OnPosalterationoptionEntityPropertyChanged ), "PosalterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.PosalterationoptionEntityUsingPosalterationoptionIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.PosalterationoptionId } );		
			_posalterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posalterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosalterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_posalterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncPosalterationoptionEntity(true, true);
				_posalterationoptionEntity = (PosalterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posalterationoptionEntity, new PropertyChangedEventHandler( OnPosalterationoptionEntityPropertyChanged ), "PosalterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.PosalterationoptionEntityUsingPosalterationoptionIdStatic, true, ref _alreadyFetchedPosalterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosalterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posproductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosproductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posproductEntity, new PropertyChangedEventHandler( OnPosproductEntityPropertyChanged ), "PosproductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.PosproductEntityUsingPosproductIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.PosproductId } );		
			_posproductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posproductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosproductEntity(IEntityCore relatedEntity)
		{
			if(_posproductEntity!=relatedEntity)
			{		
				DesetupSyncPosproductEntity(true, true);
				_posproductEntity = (PosproductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posproductEntity, new PropertyChangedEventHandler( OnPosproductEntityPropertyChanged ), "PosproductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.PosproductEntityUsingPosproductIdStatic, true, ref _alreadyFetchedPosproductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosproductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _taxTariffEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTaxTariffEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, signalRelatedEntity, "AlterationoptionCollection", resetFKFields, new int[] { (int)AlterationoptionFieldIndex.TaxTariffId } );		
			_taxTariffEntity = null;
		}
		
		/// <summary> setups the sync logic for member _taxTariffEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTaxTariffEntity(IEntityCore relatedEntity)
		{
			if(_taxTariffEntity!=relatedEntity)
			{		
				DesetupSyncTaxTariffEntity(true, true);
				_taxTariffEntity = (TaxTariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, ref _alreadyFetchedTaxTariffEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTaxTariffEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AlterationoptionFieldIndex.AlterationoptionId].ForcedCurrentValueWrite(alterationoptionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAlterationoptionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AlterationoptionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AlterationoptionRelations Relations
		{
			get	{ return new AlterationoptionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemCollection(), (IEntityRelation)GetRelationsForField("AlterationitemCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.AlterationitemEntity, 0, null, null, null, "AlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationoptionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionLanguageCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.AlterationoptionLanguageEntity, 0, null, null, null, "AlterationoptionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationoptionTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionTagCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionTagCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.AlterationoptionTagEntity, 0, null, null, null, "AlterationoptionTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemTagCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity, 0, null, null, null, "OrderitemAlterationitemTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelItemCollection(), (IEntityRelation)GetRelationsForField("PriceLevelItemCollection")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.PriceLevelItemEntity, 0, null, null, null, "PriceLevelItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaMedium"), "AdvertisementCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaMedium"), "AlterationCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium_"), "CategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMedium"), "CompanyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaMedium"), "DeliverypointgroupCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium_"), "EntertainmentCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaMedium"), "GenericcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaMedium"), "GenericproductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium_"), "ProductCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, GetRelationsForField("SurveyCollectionViaMedium"), "SurveyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection(), (IEntityRelation)GetRelationsForField("GenericalterationoptionEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, 0, null, null, null, "GenericalterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosalterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosalterationoptionCollection(), (IEntityRelation)GetRelationsForField("PosalterationoptionEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.PosalterationoptionEntity, 0, null, null, null, "PosalterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosproductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosproductCollection(), (IEntityRelation)GetRelationsForField("PosproductEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.PosproductEntity, 0, null, null, null, "PosproductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TaxTariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTaxTariffEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TaxTariffCollection(), (IEntityRelation)GetRelationsForField("TaxTariffEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionEntity, (int)Obymobi.Data.EntityType.TaxTariffEntity, 0, null, null, null, "TaxTariffEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.AlterationoptionId, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The PosalterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PosalterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.PosalterationoptionId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PosalterationoptionId, value, true); }
		}

		/// <summary> The Name property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.Name, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Name, value, true); }
		}

		/// <summary> The PriceIn property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceIn
		{
			get { return (Nullable<System.Decimal>)GetValue((int)AlterationoptionFieldIndex.PriceIn, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PriceIn, value, true); }
		}

		/// <summary> The Description property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.Description, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Description, value, true); }
		}

		/// <summary> The PosproductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."PosproductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.PosproductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.PosproductId, value, true); }
		}

		/// <summary> The IsProductRelated property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsProductRelated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProductRelated
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsProductRelated, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsProductRelated, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.CompanyId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The GenericalterationoptionId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."GenericalterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.GenericalterationoptionId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.GenericalterationoptionId, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The Type property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.Type, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Type, value, true); }
		}

		/// <summary> The XPriceAddition property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."xPriceAddition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> XPriceAddition
		{
			get { return (Nullable<System.Decimal>)GetValue((int)AlterationoptionFieldIndex.XPriceAddition, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.XPriceAddition, value, true); }
		}

		/// <summary> The StartTime property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."StartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.StartTime, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.StartTime, value, true); }
		}

		/// <summary> The EndTime property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."EndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.EndTime, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.EndTime, value, true); }
		}

		/// <summary> The MinLeadMinutes property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."MinLeadMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinLeadMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.MinLeadMinutes, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.MinLeadMinutes, value, true); }
		}

		/// <summary> The MaxLeadHours property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."MaxLeadHours"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxLeadHours
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.MaxLeadHours, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.MaxLeadHours, value, true); }
		}

		/// <summary> The IntervalMinutes property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IntervalMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IntervalMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.IntervalMinutes, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IntervalMinutes, value, true); }
		}

		/// <summary> The ShowDatePicker property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ShowDatePicker"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDatePicker
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.ShowDatePicker, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ShowDatePicker, value, true); }
		}

		/// <summary> The Version property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationoptionFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.Version, value, true); }
		}

		/// <summary> The StartTimeUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."StartTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.StartTimeUTC, value, true); }
		}

		/// <summary> The EndTimeUTC property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."EndTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.EndTimeUTC, value, true); }
		}

		/// <summary> The BrandId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.BrandId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.BrandId, value, true); }
		}

		/// <summary> The TaxTariffId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."TaxTariffId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.TaxTariffId, value, true); }
		}

		/// <summary> The IsAlcoholic property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsAlcoholic"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAlcoholic
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsAlcoholic, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsAlcoholic, value, true); }
		}

		/// <summary> The InheritName property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritName"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritName
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritName, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritName, value, true); }
		}

		/// <summary> The InheritPrice property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritPrice"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritPrice
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritPrice, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritPrice, value, true); }
		}

		/// <summary> The InheritTaxTariff property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritTaxTariff"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritTaxTariff
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritTaxTariff, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritTaxTariff, value, true); }
		}

		/// <summary> The InheritIsAlcoholic property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritIsAlcoholic"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritIsAlcoholic
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritIsAlcoholic, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritIsAlcoholic, value, true); }
		}

		/// <summary> The ProductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.ProductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ProductId, value, true); }
		}

		/// <summary> The InheritTags property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritTags"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritTags
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritTags, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritTags, value, true); }
		}

		/// <summary> The InheritCustomText property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."InheritCustomText"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritCustomText
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.InheritCustomText, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.InheritCustomText, value, true); }
		}

		/// <summary> The ExternalProductId property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ExternalProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionFieldIndex.ExternalProductId, false); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ExternalProductId, value, true); }
		}

		/// <summary> The IsAvailable property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."IsAvailable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAvailable
		{
			get { return (System.Boolean)GetValue((int)AlterationoptionFieldIndex.IsAvailable, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.IsAvailable, value, true); }
		}

		/// <summary> The ExternalIdentifier property of the Entity Alterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationoption"."ExternalIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)AlterationoptionFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)AlterationoptionFieldIndex.ExternalIdentifier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemCollection AlterationitemCollection
		{
			get	{ return GetMultiAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemCollection. When set to true, AlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemCollection
		{
			get	{ return _alwaysFetchAlterationitemCollection; }
			set	{ _alwaysFetchAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemCollection already has been fetched. Setting this property to false when AlterationitemCollection has been fetched
		/// will clear the AlterationitemCollection collection well. Setting this property to true while AlterationitemCollection hasn't been fetched disables lazy loading for AlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemCollection
		{
			get { return _alreadyFetchedAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedAlterationitemCollection && !value && (_alterationitemCollection != null))
				{
					_alterationitemCollection.Clear();
				}
				_alreadyFetchedAlterationitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection AlterationoptionLanguageCollection
		{
			get	{ return GetMultiAlterationoptionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionLanguageCollection. When set to true, AlterationoptionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionLanguageCollection
		{
			get	{ return _alwaysFetchAlterationoptionLanguageCollection; }
			set	{ _alwaysFetchAlterationoptionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionLanguageCollection already has been fetched. Setting this property to false when AlterationoptionLanguageCollection has been fetched
		/// will clear the AlterationoptionLanguageCollection collection well. Setting this property to true while AlterationoptionLanguageCollection hasn't been fetched disables lazy loading for AlterationoptionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionLanguageCollection
		{
			get { return _alreadyFetchedAlterationoptionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionLanguageCollection && !value && (_alterationoptionLanguageCollection != null))
				{
					_alterationoptionLanguageCollection.Clear();
				}
				_alreadyFetchedAlterationoptionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionTagCollection AlterationoptionTagCollection
		{
			get	{ return GetMultiAlterationoptionTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionTagCollection. When set to true, AlterationoptionTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionTagCollection
		{
			get	{ return _alwaysFetchAlterationoptionTagCollection; }
			set	{ _alwaysFetchAlterationoptionTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionTagCollection already has been fetched. Setting this property to false when AlterationoptionTagCollection has been fetched
		/// will clear the AlterationoptionTagCollection collection well. Setting this property to true while AlterationoptionTagCollection hasn't been fetched disables lazy loading for AlterationoptionTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionTagCollection
		{
			get { return _alreadyFetchedAlterationoptionTagCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionTagCollection && !value && (_alterationoptionTagCollection != null))
				{
					_alterationoptionTagCollection.Clear();
				}
				_alreadyFetchedAlterationoptionTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection OrderitemAlterationitemTagCollection
		{
			get	{ return GetMultiOrderitemAlterationitemTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemTagCollection. When set to true, OrderitemAlterationitemTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemTagCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemTagCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemTagCollection already has been fetched. Setting this property to false when OrderitemAlterationitemTagCollection has been fetched
		/// will clear the OrderitemAlterationitemTagCollection collection well. Setting this property to true while OrderitemAlterationitemTagCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemTagCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemTagCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemTagCollection && !value && (_orderitemAlterationitemTagCollection != null))
				{
					_orderitemAlterationitemTagCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPriceLevelItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection PriceLevelItemCollection
		{
			get	{ return GetMultiPriceLevelItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelItemCollection. When set to true, PriceLevelItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPriceLevelItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelItemCollection
		{
			get	{ return _alwaysFetchPriceLevelItemCollection; }
			set	{ _alwaysFetchPriceLevelItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelItemCollection already has been fetched. Setting this property to false when PriceLevelItemCollection has been fetched
		/// will clear the PriceLevelItemCollection collection well. Setting this property to true while PriceLevelItemCollection hasn't been fetched disables lazy loading for PriceLevelItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelItemCollection
		{
			get { return _alreadyFetchedPriceLevelItemCollection;}
			set 
			{
				if(_alreadyFetchedPriceLevelItemCollection && !value && (_priceLevelItemCollection != null))
				{
					_priceLevelItemCollection.Clear();
				}
				_alreadyFetchedPriceLevelItemCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaMedium
		{
			get { return GetMultiAdvertisementCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaMedium. When set to true, AdvertisementCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaMedium
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaMedium; }
			set	{ _alwaysFetchAdvertisementCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaMedium already has been fetched. Setting this property to false when AdvertisementCollectionViaMedium has been fetched
		/// will clear the AdvertisementCollectionViaMedium collection well. Setting this property to true while AdvertisementCollectionViaMedium hasn't been fetched disables lazy loading for AdvertisementCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaMedium
		{
			get { return _alreadyFetchedAdvertisementCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaMedium && !value && (_advertisementCollectionViaMedium != null))
				{
					_advertisementCollectionViaMedium.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaMedium
		{
			get { return GetMultiAlterationCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaMedium. When set to true, AlterationCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationCollectionViaMedium; }
			set	{ _alwaysFetchAlterationCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaMedium already has been fetched. Setting this property to false when AlterationCollectionViaMedium has been fetched
		/// will clear the AlterationCollectionViaMedium collection well. Setting this property to true while AlterationCollectionViaMedium hasn't been fetched disables lazy loading for AlterationCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaMedium && !value && (_alterationCollectionViaMedium != null))
				{
					_alterationCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium_
		{
			get { return GetMultiCategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium_. When set to true, CategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium_; }
			set	{ _alwaysFetchCategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium_ already has been fetched. Setting this property to false when CategoryCollectionViaMedium_ has been fetched
		/// will clear the CategoryCollectionViaMedium_ collection well. Setting this property to true while CategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for CategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium_ && !value && (_categoryCollectionViaMedium_ != null))
				{
					_categoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMedium
		{
			get { return GetMultiCompanyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMedium. When set to true, CompanyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMedium
		{
			get	{ return _alwaysFetchCompanyCollectionViaMedium; }
			set	{ _alwaysFetchCompanyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMedium already has been fetched. Setting this property to false when CompanyCollectionViaMedium has been fetched
		/// will clear the CompanyCollectionViaMedium collection well. Setting this property to true while CompanyCollectionViaMedium hasn't been fetched disables lazy loading for CompanyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMedium
		{
			get { return _alreadyFetchedCompanyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMedium && !value && (_companyCollectionViaMedium != null))
				{
					_companyCollectionViaMedium.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaMedium
		{
			get { return GetMultiDeliverypointgroupCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaMedium. When set to true, DeliverypointgroupCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaMedium
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaMedium; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaMedium already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaMedium has been fetched
		/// will clear the DeliverypointgroupCollectionViaMedium collection well. Setting this property to true while DeliverypointgroupCollectionViaMedium hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaMedium
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaMedium && !value && (_deliverypointgroupCollectionViaMedium != null))
				{
					_deliverypointgroupCollectionViaMedium.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium_
		{
			get { return GetMultiEntertainmentCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium_. When set to true, EntertainmentCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentCollectionViaMedium_ collection well. Setting this property to true while EntertainmentCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium_ && !value && (_entertainmentCollectionViaMedium_ != null))
				{
					_entertainmentCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaMedium
		{
			get { return GetMultiGenericcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaMedium. When set to true, GenericcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaMedium; }
			set	{ _alwaysFetchGenericcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaMedium already has been fetched. Setting this property to false when GenericcategoryCollectionViaMedium has been fetched
		/// will clear the GenericcategoryCollectionViaMedium collection well. Setting this property to true while GenericcategoryCollectionViaMedium hasn't been fetched disables lazy loading for GenericcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaMedium && !value && (_genericcategoryCollectionViaMedium != null))
				{
					_genericcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaMedium
		{
			get { return GetMultiGenericproductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaMedium. When set to true, GenericproductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericproductCollectionViaMedium; }
			set	{ _alwaysFetchGenericproductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaMedium already has been fetched. Setting this property to false when GenericproductCollectionViaMedium has been fetched
		/// will clear the GenericproductCollectionViaMedium collection well. Setting this property to true while GenericproductCollectionViaMedium hasn't been fetched disables lazy loading for GenericproductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaMedium
		{
			get { return _alreadyFetchedGenericproductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaMedium && !value && (_genericproductCollectionViaMedium != null))
				{
					_genericproductCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium_
		{
			get { return GetMultiProductCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium_. When set to true, ProductCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium_
		{
			get	{ return _alwaysFetchProductCollectionViaMedium_; }
			set	{ _alwaysFetchProductCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium_ already has been fetched. Setting this property to false when ProductCollectionViaMedium_ has been fetched
		/// will clear the ProductCollectionViaMedium_ collection well. Setting this property to true while ProductCollectionViaMedium_ hasn't been fetched disables lazy loading for ProductCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium_
		{
			get { return _alreadyFetchedProductCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium_ && !value && (_productCollectionViaMedium_ != null))
				{
					_productCollectionViaMedium_.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyCollection SurveyCollectionViaMedium
		{
			get { return GetMultiSurveyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyCollectionViaMedium. When set to true, SurveyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyCollectionViaMedium; }
			set	{ _alwaysFetchSurveyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyCollectionViaMedium already has been fetched. Setting this property to false when SurveyCollectionViaMedium has been fetched
		/// will clear the SurveyCollectionViaMedium collection well. Setting this property to true while SurveyCollectionViaMedium hasn't been fetched disables lazy loading for SurveyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyCollectionViaMedium && !value && (_surveyCollectionViaMedium != null))
				{
					_surveyCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalProductEntity ExternalProductEntity
		{
			get	{ return GetSingleExternalProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "ExternalProductEntity", _externalProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductEntity. When set to true, ExternalProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductEntity
		{
			get	{ return _alwaysFetchExternalProductEntity; }
			set	{ _alwaysFetchExternalProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductEntity already has been fetched. Setting this property to false when ExternalProductEntity has been fetched
		/// will set ExternalProductEntity to null as well. Setting this property to true while ExternalProductEntity hasn't been fetched disables lazy loading for ExternalProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductEntity
		{
			get { return _alreadyFetchedExternalProductEntity;}
			set 
			{
				if(_alreadyFetchedExternalProductEntity && !value)
				{
					this.ExternalProductEntity = null;
				}
				_alreadyFetchedExternalProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalProductEntity is not found
		/// in the database. When set to true, ExternalProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalProductEntityReturnsNewIfNotFound
		{
			get	{ return _externalProductEntityReturnsNewIfNotFound; }
			set { _externalProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericalterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericalterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericalterationoptionEntity GenericalterationoptionEntity
		{
			get	{ return GetSingleGenericalterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericalterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "GenericalterationoptionEntity", _genericalterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationoptionEntity. When set to true, GenericalterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericalterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationoptionEntity
		{
			get	{ return _alwaysFetchGenericalterationoptionEntity; }
			set	{ _alwaysFetchGenericalterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationoptionEntity already has been fetched. Setting this property to false when GenericalterationoptionEntity has been fetched
		/// will set GenericalterationoptionEntity to null as well. Setting this property to true while GenericalterationoptionEntity hasn't been fetched disables lazy loading for GenericalterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationoptionEntity
		{
			get { return _alreadyFetchedGenericalterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedGenericalterationoptionEntity && !value)
				{
					this.GenericalterationoptionEntity = null;
				}
				_alreadyFetchedGenericalterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericalterationoptionEntity is not found
		/// in the database. When set to true, GenericalterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericalterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _genericalterationoptionEntityReturnsNewIfNotFound; }
			set { _genericalterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosalterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosalterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosalterationoptionEntity PosalterationoptionEntity
		{
			get	{ return GetSinglePosalterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosalterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "PosalterationoptionEntity", _posalterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosalterationoptionEntity. When set to true, PosalterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosalterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSinglePosalterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosalterationoptionEntity
		{
			get	{ return _alwaysFetchPosalterationoptionEntity; }
			set	{ _alwaysFetchPosalterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosalterationoptionEntity already has been fetched. Setting this property to false when PosalterationoptionEntity has been fetched
		/// will set PosalterationoptionEntity to null as well. Setting this property to true while PosalterationoptionEntity hasn't been fetched disables lazy loading for PosalterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosalterationoptionEntity
		{
			get { return _alreadyFetchedPosalterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedPosalterationoptionEntity && !value)
				{
					this.PosalterationoptionEntity = null;
				}
				_alreadyFetchedPosalterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosalterationoptionEntity is not found
		/// in the database. When set to true, PosalterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosalterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _posalterationoptionEntityReturnsNewIfNotFound; }
			set { _posalterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosproductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosproductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosproductEntity PosproductEntity
		{
			get	{ return GetSinglePosproductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosproductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "PosproductEntity", _posproductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosproductEntity. When set to true, PosproductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosproductEntity is accessed. You can always execute a forced fetch by calling GetSinglePosproductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosproductEntity
		{
			get	{ return _alwaysFetchPosproductEntity; }
			set	{ _alwaysFetchPosproductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosproductEntity already has been fetched. Setting this property to false when PosproductEntity has been fetched
		/// will set PosproductEntity to null as well. Setting this property to true while PosproductEntity hasn't been fetched disables lazy loading for PosproductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosproductEntity
		{
			get { return _alreadyFetchedPosproductEntity;}
			set 
			{
				if(_alreadyFetchedPosproductEntity && !value)
				{
					this.PosproductEntity = null;
				}
				_alreadyFetchedPosproductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosproductEntity is not found
		/// in the database. When set to true, PosproductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosproductEntityReturnsNewIfNotFound
		{
			get	{ return _posproductEntityReturnsNewIfNotFound; }
			set { _posproductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TaxTariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTaxTariffEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TaxTariffEntity TaxTariffEntity
		{
			get	{ return GetSingleTaxTariffEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTaxTariffEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionCollection", "TaxTariffEntity", _taxTariffEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TaxTariffEntity. When set to true, TaxTariffEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TaxTariffEntity is accessed. You can always execute a forced fetch by calling GetSingleTaxTariffEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTaxTariffEntity
		{
			get	{ return _alwaysFetchTaxTariffEntity; }
			set	{ _alwaysFetchTaxTariffEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TaxTariffEntity already has been fetched. Setting this property to false when TaxTariffEntity has been fetched
		/// will set TaxTariffEntity to null as well. Setting this property to true while TaxTariffEntity hasn't been fetched disables lazy loading for TaxTariffEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTaxTariffEntity
		{
			get { return _alreadyFetchedTaxTariffEntity;}
			set 
			{
				if(_alreadyFetchedTaxTariffEntity && !value)
				{
					this.TaxTariffEntity = null;
				}
				_alreadyFetchedTaxTariffEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TaxTariffEntity is not found
		/// in the database. When set to true, TaxTariffEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TaxTariffEntityReturnsNewIfNotFound
		{
			get	{ return _taxTariffEntityReturnsNewIfNotFound; }
			set { _taxTariffEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AlterationoptionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
