﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UIScheduleItemOccurrence'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIScheduleItemOccurrenceEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIScheduleItemOccurrenceEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection	_scheduledMessageHistoryCollection;
		private bool	_alwaysFetchScheduledMessageHistoryCollection, _alreadyFetchedScheduledMessageHistoryCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection	_uIScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchUIScheduleItemOccurrenceCollection, _alreadyFetchedUIScheduleItemOccurrenceCollection;
		private MessagegroupEntity _messagegroupEntity;
		private bool	_alwaysFetchMessagegroupEntity, _alreadyFetchedMessagegroupEntity, _messagegroupEntityReturnsNewIfNotFound;
		private ReportProcessingTaskTemplateEntity _reportProcessingTaskTemplateEntity;
		private bool	_alwaysFetchReportProcessingTaskTemplateEntity, _alreadyFetchedReportProcessingTaskTemplateEntity, _reportProcessingTaskTemplateEntityReturnsNewIfNotFound;
		private UIScheduleEntity _uIScheduleEntity;
		private bool	_alwaysFetchUIScheduleEntity, _alreadyFetchedUIScheduleEntity, _uIScheduleEntityReturnsNewIfNotFound;
		private UIScheduleItemEntity _uIScheduleItemEntity;
		private bool	_alwaysFetchUIScheduleItemEntity, _alreadyFetchedUIScheduleItemEntity, _uIScheduleItemEntityReturnsNewIfNotFound;
		private UIScheduleItemOccurrenceEntity _parentUIScheduleItemOccurrenceEntity;
		private bool	_alwaysFetchParentUIScheduleItemOccurrenceEntity, _alreadyFetchedParentUIScheduleItemOccurrenceEntity, _parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name MessagegroupEntity</summary>
			public static readonly string MessagegroupEntity = "MessagegroupEntity";
			/// <summary>Member name ReportProcessingTaskTemplateEntity</summary>
			public static readonly string ReportProcessingTaskTemplateEntity = "ReportProcessingTaskTemplateEntity";
			/// <summary>Member name UIScheduleEntity</summary>
			public static readonly string UIScheduleEntity = "UIScheduleEntity";
			/// <summary>Member name UIScheduleItemEntity</summary>
			public static readonly string UIScheduleItemEntity = "UIScheduleItemEntity";
			/// <summary>Member name ParentUIScheduleItemOccurrenceEntity</summary>
			public static readonly string ParentUIScheduleItemOccurrenceEntity = "ParentUIScheduleItemOccurrenceEntity";
			/// <summary>Member name ScheduledMessageHistoryCollection</summary>
			public static readonly string ScheduledMessageHistoryCollection = "ScheduledMessageHistoryCollection";
			/// <summary>Member name UIScheduleItemOccurrenceCollection</summary>
			public static readonly string UIScheduleItemOccurrenceCollection = "UIScheduleItemOccurrenceCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIScheduleItemOccurrenceEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIScheduleItemOccurrenceEntityBase() :base("UIScheduleItemOccurrenceEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		protected UIScheduleItemOccurrenceEntityBase(System.Int32 uIScheduleItemOccurrenceId):base("UIScheduleItemOccurrenceEntity")
		{
			InitClassFetch(uIScheduleItemOccurrenceId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIScheduleItemOccurrenceEntityBase(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse): base("UIScheduleItemOccurrenceEntity")
		{
			InitClassFetch(uIScheduleItemOccurrenceId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="validator">The custom validator object for this UIScheduleItemOccurrenceEntity</param>
		protected UIScheduleItemOccurrenceEntityBase(System.Int32 uIScheduleItemOccurrenceId, IValidator validator):base("UIScheduleItemOccurrenceEntity")
		{
			InitClassFetch(uIScheduleItemOccurrenceId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIScheduleItemOccurrenceEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_scheduledMessageHistoryCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection)info.GetValue("_scheduledMessageHistoryCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection));
			_alwaysFetchScheduledMessageHistoryCollection = info.GetBoolean("_alwaysFetchScheduledMessageHistoryCollection");
			_alreadyFetchedScheduledMessageHistoryCollection = info.GetBoolean("_alreadyFetchedScheduledMessageHistoryCollection");

			_uIScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection)info.GetValue("_uIScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection));
			_alwaysFetchUIScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchUIScheduleItemOccurrenceCollection");
			_alreadyFetchedUIScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemOccurrenceCollection");
			_messagegroupEntity = (MessagegroupEntity)info.GetValue("_messagegroupEntity", typeof(MessagegroupEntity));
			if(_messagegroupEntity!=null)
			{
				_messagegroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messagegroupEntityReturnsNewIfNotFound = info.GetBoolean("_messagegroupEntityReturnsNewIfNotFound");
			_alwaysFetchMessagegroupEntity = info.GetBoolean("_alwaysFetchMessagegroupEntity");
			_alreadyFetchedMessagegroupEntity = info.GetBoolean("_alreadyFetchedMessagegroupEntity");

			_reportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)info.GetValue("_reportProcessingTaskTemplateEntity", typeof(ReportProcessingTaskTemplateEntity));
			if(_reportProcessingTaskTemplateEntity!=null)
			{
				_reportProcessingTaskTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportProcessingTaskTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_reportProcessingTaskTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchReportProcessingTaskTemplateEntity = info.GetBoolean("_alwaysFetchReportProcessingTaskTemplateEntity");
			_alreadyFetchedReportProcessingTaskTemplateEntity = info.GetBoolean("_alreadyFetchedReportProcessingTaskTemplateEntity");

			_uIScheduleEntity = (UIScheduleEntity)info.GetValue("_uIScheduleEntity", typeof(UIScheduleEntity));
			if(_uIScheduleEntity!=null)
			{
				_uIScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleEntity = info.GetBoolean("_alwaysFetchUIScheduleEntity");
			_alreadyFetchedUIScheduleEntity = info.GetBoolean("_alreadyFetchedUIScheduleEntity");

			_uIScheduleItemEntity = (UIScheduleItemEntity)info.GetValue("_uIScheduleItemEntity", typeof(UIScheduleItemEntity));
			if(_uIScheduleItemEntity!=null)
			{
				_uIScheduleItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleItemEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleItemEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleItemEntity = info.GetBoolean("_alwaysFetchUIScheduleItemEntity");
			_alreadyFetchedUIScheduleItemEntity = info.GetBoolean("_alreadyFetchedUIScheduleItemEntity");

			_parentUIScheduleItemOccurrenceEntity = (UIScheduleItemOccurrenceEntity)info.GetValue("_parentUIScheduleItemOccurrenceEntity", typeof(UIScheduleItemOccurrenceEntity));
			if(_parentUIScheduleItemOccurrenceEntity!=null)
			{
				_parentUIScheduleItemOccurrenceEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound = info.GetBoolean("_parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound");
			_alwaysFetchParentUIScheduleItemOccurrenceEntity = info.GetBoolean("_alwaysFetchParentUIScheduleItemOccurrenceEntity");
			_alreadyFetchedParentUIScheduleItemOccurrenceEntity = info.GetBoolean("_alreadyFetchedParentUIScheduleItemOccurrenceEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIScheduleItemOccurrenceFieldIndex)fieldIndex)
			{
				case UIScheduleItemOccurrenceFieldIndex.UIScheduleItemId:
					DesetupSyncUIScheduleItemEntity(true, false);
					_alreadyFetchedUIScheduleItemEntity = false;
					break;
				case UIScheduleItemOccurrenceFieldIndex.MessagegroupId:
					DesetupSyncMessagegroupEntity(true, false);
					_alreadyFetchedMessagegroupEntity = false;
					break;
				case UIScheduleItemOccurrenceFieldIndex.ParentUIScheduleItemOccurrenceId:
					DesetupSyncParentUIScheduleItemOccurrenceEntity(true, false);
					_alreadyFetchedParentUIScheduleItemOccurrenceEntity = false;
					break;
				case UIScheduleItemOccurrenceFieldIndex.ReportProcessingTaskTemplateId:
					DesetupSyncReportProcessingTaskTemplateEntity(true, false);
					_alreadyFetchedReportProcessingTaskTemplateEntity = false;
					break;
				case UIScheduleItemOccurrenceFieldIndex.UIScheduleId:
					DesetupSyncUIScheduleEntity(true, false);
					_alreadyFetchedUIScheduleEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedScheduledMessageHistoryCollection = (_scheduledMessageHistoryCollection.Count > 0);
			_alreadyFetchedUIScheduleItemOccurrenceCollection = (_uIScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedMessagegroupEntity = (_messagegroupEntity != null);
			_alreadyFetchedReportProcessingTaskTemplateEntity = (_reportProcessingTaskTemplateEntity != null);
			_alreadyFetchedUIScheduleEntity = (_uIScheduleEntity != null);
			_alreadyFetchedUIScheduleItemEntity = (_uIScheduleItemEntity != null);
			_alreadyFetchedParentUIScheduleItemOccurrenceEntity = (_parentUIScheduleItemOccurrenceEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "MessagegroupEntity":
					toReturn.Add(Relations.MessagegroupEntityUsingMessagegroupId);
					break;
				case "ReportProcessingTaskTemplateEntity":
					toReturn.Add(Relations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
					break;
				case "UIScheduleEntity":
					toReturn.Add(Relations.UIScheduleEntityUsingUIScheduleId);
					break;
				case "UIScheduleItemEntity":
					toReturn.Add(Relations.UIScheduleItemEntityUsingUIScheduleItemId);
					break;
				case "ParentUIScheduleItemOccurrenceEntity":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceId);
					break;
				case "ScheduledMessageHistoryCollection":
					toReturn.Add(Relations.ScheduledMessageHistoryEntityUsingUIScheduleItemOccurrenceId);
					break;
				case "UIScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingParentUIScheduleItemOccurrenceId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_scheduledMessageHistoryCollection", (!this.MarkedForDeletion?_scheduledMessageHistoryCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageHistoryCollection", _alwaysFetchScheduledMessageHistoryCollection);
			info.AddValue("_alreadyFetchedScheduledMessageHistoryCollection", _alreadyFetchedScheduledMessageHistoryCollection);
			info.AddValue("_uIScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_uIScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemOccurrenceCollection", _alwaysFetchUIScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemOccurrenceCollection", _alreadyFetchedUIScheduleItemOccurrenceCollection);
			info.AddValue("_messagegroupEntity", (!this.MarkedForDeletion?_messagegroupEntity:null));
			info.AddValue("_messagegroupEntityReturnsNewIfNotFound", _messagegroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessagegroupEntity", _alwaysFetchMessagegroupEntity);
			info.AddValue("_alreadyFetchedMessagegroupEntity", _alreadyFetchedMessagegroupEntity);
			info.AddValue("_reportProcessingTaskTemplateEntity", (!this.MarkedForDeletion?_reportProcessingTaskTemplateEntity:null));
			info.AddValue("_reportProcessingTaskTemplateEntityReturnsNewIfNotFound", _reportProcessingTaskTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportProcessingTaskTemplateEntity", _alwaysFetchReportProcessingTaskTemplateEntity);
			info.AddValue("_alreadyFetchedReportProcessingTaskTemplateEntity", _alreadyFetchedReportProcessingTaskTemplateEntity);
			info.AddValue("_uIScheduleEntity", (!this.MarkedForDeletion?_uIScheduleEntity:null));
			info.AddValue("_uIScheduleEntityReturnsNewIfNotFound", _uIScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleEntity", _alwaysFetchUIScheduleEntity);
			info.AddValue("_alreadyFetchedUIScheduleEntity", _alreadyFetchedUIScheduleEntity);
			info.AddValue("_uIScheduleItemEntity", (!this.MarkedForDeletion?_uIScheduleItemEntity:null));
			info.AddValue("_uIScheduleItemEntityReturnsNewIfNotFound", _uIScheduleItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleItemEntity", _alwaysFetchUIScheduleItemEntity);
			info.AddValue("_alreadyFetchedUIScheduleItemEntity", _alreadyFetchedUIScheduleItemEntity);
			info.AddValue("_parentUIScheduleItemOccurrenceEntity", (!this.MarkedForDeletion?_parentUIScheduleItemOccurrenceEntity:null));
			info.AddValue("_parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound", _parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentUIScheduleItemOccurrenceEntity", _alwaysFetchParentUIScheduleItemOccurrenceEntity);
			info.AddValue("_alreadyFetchedParentUIScheduleItemOccurrenceEntity", _alreadyFetchedParentUIScheduleItemOccurrenceEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "MessagegroupEntity":
					_alreadyFetchedMessagegroupEntity = true;
					this.MessagegroupEntity = (MessagegroupEntity)entity;
					break;
				case "ReportProcessingTaskTemplateEntity":
					_alreadyFetchedReportProcessingTaskTemplateEntity = true;
					this.ReportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)entity;
					break;
				case "UIScheduleEntity":
					_alreadyFetchedUIScheduleEntity = true;
					this.UIScheduleEntity = (UIScheduleEntity)entity;
					break;
				case "UIScheduleItemEntity":
					_alreadyFetchedUIScheduleItemEntity = true;
					this.UIScheduleItemEntity = (UIScheduleItemEntity)entity;
					break;
				case "ParentUIScheduleItemOccurrenceEntity":
					_alreadyFetchedParentUIScheduleItemOccurrenceEntity = true;
					this.ParentUIScheduleItemOccurrenceEntity = (UIScheduleItemOccurrenceEntity)entity;
					break;
				case "ScheduledMessageHistoryCollection":
					_alreadyFetchedScheduledMessageHistoryCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)entity);
					}
					break;
				case "UIScheduleItemOccurrenceCollection":
					_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "MessagegroupEntity":
					SetupSyncMessagegroupEntity(relatedEntity);
					break;
				case "ReportProcessingTaskTemplateEntity":
					SetupSyncReportProcessingTaskTemplateEntity(relatedEntity);
					break;
				case "UIScheduleEntity":
					SetupSyncUIScheduleEntity(relatedEntity);
					break;
				case "UIScheduleItemEntity":
					SetupSyncUIScheduleItemEntity(relatedEntity);
					break;
				case "ParentUIScheduleItemOccurrenceEntity":
					SetupSyncParentUIScheduleItemOccurrenceEntity(relatedEntity);
					break;
				case "ScheduledMessageHistoryCollection":
					_scheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)relatedEntity);
					break;
				case "UIScheduleItemOccurrenceCollection":
					_uIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "MessagegroupEntity":
					DesetupSyncMessagegroupEntity(false, true);
					break;
				case "ReportProcessingTaskTemplateEntity":
					DesetupSyncReportProcessingTaskTemplateEntity(false, true);
					break;
				case "UIScheduleEntity":
					DesetupSyncUIScheduleEntity(false, true);
					break;
				case "UIScheduleItemEntity":
					DesetupSyncUIScheduleItemEntity(false, true);
					break;
				case "ParentUIScheduleItemOccurrenceEntity":
					DesetupSyncParentUIScheduleItemOccurrenceEntity(false, true);
					break;
				case "ScheduledMessageHistoryCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_messagegroupEntity!=null)
			{
				toReturn.Add(_messagegroupEntity);
			}
			if(_reportProcessingTaskTemplateEntity!=null)
			{
				toReturn.Add(_reportProcessingTaskTemplateEntity);
			}
			if(_uIScheduleEntity!=null)
			{
				toReturn.Add(_uIScheduleEntity);
			}
			if(_uIScheduleItemEntity!=null)
			{
				toReturn.Add(_uIScheduleItemEntity);
			}
			if(_parentUIScheduleItemOccurrenceEntity!=null)
			{
				toReturn.Add(_parentUIScheduleItemOccurrenceEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_scheduledMessageHistoryCollection);
			toReturn.Add(_uIScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemOccurrenceId)
		{
			return FetchUsingPK(uIScheduleItemOccurrenceId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIScheduleItemOccurrenceId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIScheduleItemOccurrenceId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIScheduleItemOccurrenceId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIScheduleItemOccurrenceId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIScheduleItemOccurrenceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageHistoryCollection || forceFetch || _alwaysFetchScheduledMessageHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageHistoryCollection);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageHistoryCollection.GetMultiManyToOne(null, null, null, this, filter);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageHistoryCollection = true;
			}
			return _scheduledMessageHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageHistoryCollection'. These settings will be taken into account
		/// when the property ScheduledMessageHistoryCollection is requested or GetMultiScheduledMessageHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageHistoryCollection.SortClauses=sortClauses;
			_scheduledMessageHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchUIScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemOccurrenceCollection);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemOccurrenceCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
			}
			return _uIScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property UIScheduleItemOccurrenceCollection is requested or GetMultiUIScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_uIScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public MessagegroupEntity GetSingleMessagegroupEntity()
		{
			return GetSingleMessagegroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public virtual MessagegroupEntity GetSingleMessagegroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessagegroupEntity || forceFetch || _alwaysFetchMessagegroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessagegroupEntityUsingMessagegroupId);
				MessagegroupEntity newEntity = new MessagegroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessagegroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MessagegroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messagegroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MessagegroupEntity = newEntity;
				_alreadyFetchedMessagegroupEntity = fetchResult;
			}
			return _messagegroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportProcessingTaskTemplateEntity' which is related to this entity.</returns>
		public ReportProcessingTaskTemplateEntity GetSingleReportProcessingTaskTemplateEntity()
		{
			return GetSingleReportProcessingTaskTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportProcessingTaskTemplateEntity' which is related to this entity.</returns>
		public virtual ReportProcessingTaskTemplateEntity GetSingleReportProcessingTaskTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportProcessingTaskTemplateEntity || forceFetch || _alwaysFetchReportProcessingTaskTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
				ReportProcessingTaskTemplateEntity newEntity = new ReportProcessingTaskTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportProcessingTaskTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportProcessingTaskTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportProcessingTaskTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportProcessingTaskTemplateEntity = newEntity;
				_alreadyFetchedReportProcessingTaskTemplateEntity = fetchResult;
			}
			return _reportProcessingTaskTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public UIScheduleEntity GetSingleUIScheduleEntity()
		{
			return GetSingleUIScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public virtual UIScheduleEntity GetSingleUIScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleEntity || forceFetch || _alwaysFetchUIScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleEntityUsingUIScheduleId);
				UIScheduleEntity newEntity = new UIScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleEntity = newEntity;
				_alreadyFetchedUIScheduleEntity = fetchResult;
			}
			return _uIScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleItemEntity' which is related to this entity.</returns>
		public UIScheduleItemEntity GetSingleUIScheduleItemEntity()
		{
			return GetSingleUIScheduleItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleItemEntity' which is related to this entity.</returns>
		public virtual UIScheduleItemEntity GetSingleUIScheduleItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleItemEntity || forceFetch || _alwaysFetchUIScheduleItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleItemEntityUsingUIScheduleItemId);
				UIScheduleItemEntity newEntity = new UIScheduleItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleItemEntity = newEntity;
				_alreadyFetchedUIScheduleItemEntity = fetchResult;
			}
			return _uIScheduleItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleItemOccurrenceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleItemOccurrenceEntity' which is related to this entity.</returns>
		public UIScheduleItemOccurrenceEntity GetSingleParentUIScheduleItemOccurrenceEntity()
		{
			return GetSingleParentUIScheduleItemOccurrenceEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleItemOccurrenceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleItemOccurrenceEntity' which is related to this entity.</returns>
		public virtual UIScheduleItemOccurrenceEntity GetSingleParentUIScheduleItemOccurrenceEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentUIScheduleItemOccurrenceEntity || forceFetch || _alwaysFetchParentUIScheduleItemOccurrenceEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceId);
				UIScheduleItemOccurrenceEntity newEntity = new UIScheduleItemOccurrenceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentUIScheduleItemOccurrenceId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleItemOccurrenceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentUIScheduleItemOccurrenceEntity = newEntity;
				_alreadyFetchedParentUIScheduleItemOccurrenceEntity = fetchResult;
			}
			return _parentUIScheduleItemOccurrenceEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("MessagegroupEntity", _messagegroupEntity);
			toReturn.Add("ReportProcessingTaskTemplateEntity", _reportProcessingTaskTemplateEntity);
			toReturn.Add("UIScheduleEntity", _uIScheduleEntity);
			toReturn.Add("UIScheduleItemEntity", _uIScheduleItemEntity);
			toReturn.Add("ParentUIScheduleItemOccurrenceEntity", _parentUIScheduleItemOccurrenceEntity);
			toReturn.Add("ScheduledMessageHistoryCollection", _scheduledMessageHistoryCollection);
			toReturn.Add("UIScheduleItemOccurrenceCollection", _uIScheduleItemOccurrenceCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="validator">The validator object for this UIScheduleItemOccurrenceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIScheduleItemOccurrenceId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIScheduleItemOccurrenceId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_scheduledMessageHistoryCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection();
			_scheduledMessageHistoryCollection.SetContainingEntityInfo(this, "UIScheduleItemOccurrenceEntity");

			_uIScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection();
			_uIScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "ParentUIScheduleItemOccurrenceEntity");
			_messagegroupEntityReturnsNewIfNotFound = true;
			_reportProcessingTaskTemplateEntityReturnsNewIfNotFound = true;
			_uIScheduleEntityReturnsNewIfNotFound = true;
			_uIScheduleItemEntityReturnsNewIfNotFound = true;
			_parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleItemOccurrenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Recurring", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceRange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceOccurrenceCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrencePeriodicity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceDayNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceWeekDays", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceWeekOfMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagegroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceStartUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceEndUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceIndex", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentUIScheduleItemOccurrenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastTriggeredUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _messagegroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessagegroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, signalRelatedEntity, "UIScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)UIScheduleItemOccurrenceFieldIndex.MessagegroupId } );		
			_messagegroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _messagegroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessagegroupEntity(IEntityCore relatedEntity)
		{
			if(_messagegroupEntity!=relatedEntity)
			{		
				DesetupSyncMessagegroupEntity(true, true);
				_messagegroupEntity = (MessagegroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, ref _alreadyFetchedMessagegroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessagegroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportProcessingTaskTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportProcessingTaskTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportProcessingTaskTemplateEntity, new PropertyChangedEventHandler( OnReportProcessingTaskTemplateEntityPropertyChanged ), "ReportProcessingTaskTemplateEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic, true, signalRelatedEntity, "UIScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)UIScheduleItemOccurrenceFieldIndex.ReportProcessingTaskTemplateId } );		
			_reportProcessingTaskTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _reportProcessingTaskTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportProcessingTaskTemplateEntity(IEntityCore relatedEntity)
		{
			if(_reportProcessingTaskTemplateEntity!=relatedEntity)
			{		
				DesetupSyncReportProcessingTaskTemplateEntity(true, true);
				_reportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportProcessingTaskTemplateEntity, new PropertyChangedEventHandler( OnReportProcessingTaskTemplateEntityPropertyChanged ), "ReportProcessingTaskTemplateEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic, true, ref _alreadyFetchedReportProcessingTaskTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportProcessingTaskTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, signalRelatedEntity, "UIScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)UIScheduleItemOccurrenceFieldIndex.UIScheduleId } );		
			_uIScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleEntity(true, true);
				_uIScheduleEntity = (UIScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, ref _alreadyFetchedUIScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleItemEntity, new PropertyChangedEventHandler( OnUIScheduleItemEntityPropertyChanged ), "UIScheduleItemEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleItemEntityUsingUIScheduleItemIdStatic, true, signalRelatedEntity, "UIScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemId } );		
			_uIScheduleItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleItemEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleItemEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleItemEntity(true, true);
				_uIScheduleItemEntity = (UIScheduleItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleItemEntity, new PropertyChangedEventHandler( OnUIScheduleItemEntityPropertyChanged ), "UIScheduleItemEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleItemEntityUsingUIScheduleItemIdStatic, true, ref _alreadyFetchedUIScheduleItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parentUIScheduleItemOccurrenceEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentUIScheduleItemOccurrenceEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentUIScheduleItemOccurrenceEntity, new PropertyChangedEventHandler( OnParentUIScheduleItemOccurrenceEntityPropertyChanged ), "ParentUIScheduleItemOccurrenceEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceIdStatic, true, signalRelatedEntity, "UIScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)UIScheduleItemOccurrenceFieldIndex.ParentUIScheduleItemOccurrenceId } );		
			_parentUIScheduleItemOccurrenceEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentUIScheduleItemOccurrenceEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentUIScheduleItemOccurrenceEntity(IEntityCore relatedEntity)
		{
			if(_parentUIScheduleItemOccurrenceEntity!=relatedEntity)
			{		
				DesetupSyncParentUIScheduleItemOccurrenceEntity(true, true);
				_parentUIScheduleItemOccurrenceEntity = (UIScheduleItemOccurrenceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentUIScheduleItemOccurrenceEntity, new PropertyChangedEventHandler( OnParentUIScheduleItemOccurrenceEntityPropertyChanged ), "ParentUIScheduleItemOccurrenceEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemOccurrenceRelations.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceIdStatic, true, ref _alreadyFetchedParentUIScheduleItemOccurrenceEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentUIScheduleItemOccurrenceEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemOccurrenceId].ForcedCurrentValueWrite(uIScheduleItemOccurrenceId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIScheduleItemOccurrenceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIScheduleItemOccurrenceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIScheduleItemOccurrenceRelations Relations
		{
			get	{ return new UIScheduleItemOccurrenceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessageHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageHistoryCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.ScheduledMessageHistoryEntity, 0, null, null, null, "ScheduledMessageHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "UIScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Messagegroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupCollection(), (IEntityRelation)GetRelationsForField("MessagegroupEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.MessagegroupEntity, 0, null, null, null, "MessagegroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTaskTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskTemplateEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, 0, null, null, null, "ReportProcessingTaskTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.UIScheduleItemEntity, 0, null, null, null, "UIScheduleItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentUIScheduleItemOccurrenceEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("ParentUIScheduleItemOccurrenceEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "ParentUIScheduleItemOccurrenceEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIScheduleItemOccurrenceId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."UIScheduleItemOccurrenceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIScheduleItemOccurrenceId
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemOccurrenceId, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemOccurrenceId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The UIScheduleItemId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."UIScheduleItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleItemId, value, true); }
		}

		/// <summary> The StartTime property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."StartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.StartTime, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.StartTime, value, true); }
		}

		/// <summary> The EndTime property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."EndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.EndTime, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.EndTime, value, true); }
		}

		/// <summary> The Recurring property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."Recurring"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Recurring
		{
			get { return (System.Boolean)GetValue((int)UIScheduleItemOccurrenceFieldIndex.Recurring, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.Recurring, value, true); }
		}

		/// <summary> The RecurrenceType property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceType
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceType, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceType, value, true); }
		}

		/// <summary> The RecurrenceRange property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceRange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceRange
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceRange, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceRange, value, true); }
		}

		/// <summary> The RecurrenceStart property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceStart"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStart
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceStart, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceStart, value, true); }
		}

		/// <summary> The RecurrenceEnd property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceEnd"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceEnd, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceEnd, value, true); }
		}

		/// <summary> The RecurrenceOccurrenceCount property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceOccurrenceCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceOccurrenceCount
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, value, true); }
		}

		/// <summary> The RecurrencePeriodicity property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrencePeriodicity"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrencePeriodicity
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, value, true); }
		}

		/// <summary> The RecurrenceDayNumber property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceDayNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceDayNumber
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, value, true); }
		}

		/// <summary> The RecurrenceWeekDays property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceWeekDays"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekDays
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, value, true); }
		}

		/// <summary> The RecurrenceWeekOfMonth property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceWeekOfMonth"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekOfMonth
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, value, true); }
		}

		/// <summary> The RecurrenceMonth property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceMonth"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceMonth
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceMonth, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceMonth, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The BackgroundColor property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.BackgroundColor, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.BackgroundColor, value, true); }
		}

		/// <summary> The TextColor property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."TextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextColor
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.TextColor, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.TextColor, value, true); }
		}

		/// <summary> The MessagegroupId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."MessagegroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MessagegroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.MessagegroupId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.MessagegroupId, value, true); }
		}

		/// <summary> The StartTimeUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."StartTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.StartTimeUTC, value, true); }
		}

		/// <summary> The EndTimeUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."EndTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.EndTimeUTC, value, true); }
		}

		/// <summary> The RecurrenceStartUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceStartUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStartUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, value, true); }
		}

		/// <summary> The RecurrenceEndUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceEndUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEndUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, value, true); }
		}

		/// <summary> The Type property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.Type, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.Type, value, true); }
		}

		/// <summary> The RecurrenceIndex property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."RecurrenceIndex"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceIndex
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceIndex, true); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.RecurrenceIndex, value, true); }
		}

		/// <summary> The ParentUIScheduleItemOccurrenceId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."ParentUIScheduleItemOccurrenceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentUIScheduleItemOccurrenceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.ParentUIScheduleItemOccurrenceId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.ParentUIScheduleItemOccurrenceId, value, true); }
		}

		/// <summary> The LastTriggeredUTC property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."LastTriggeredUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastTriggeredUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.LastTriggeredUTC, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.LastTriggeredUTC, value, true); }
		}

		/// <summary> The ReportProcessingTaskTemplateId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."ReportProcessingTaskTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReportProcessingTaskTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.ReportProcessingTaskTemplateId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.ReportProcessingTaskTemplateId, value, true); }
		}

		/// <summary> The UIScheduleId property of the Entity UIScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItemOccurrence"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)UIScheduleItemOccurrenceFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection ScheduledMessageHistoryCollection
		{
			get	{ return GetMultiScheduledMessageHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageHistoryCollection. When set to true, ScheduledMessageHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageHistoryCollection
		{
			get	{ return _alwaysFetchScheduledMessageHistoryCollection; }
			set	{ _alwaysFetchScheduledMessageHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageHistoryCollection already has been fetched. Setting this property to false when ScheduledMessageHistoryCollection has been fetched
		/// will clear the ScheduledMessageHistoryCollection collection well. Setting this property to true while ScheduledMessageHistoryCollection hasn't been fetched disables lazy loading for ScheduledMessageHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageHistoryCollection
		{
			get { return _alreadyFetchedScheduledMessageHistoryCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageHistoryCollection && !value && (_scheduledMessageHistoryCollection != null))
				{
					_scheduledMessageHistoryCollection.Clear();
				}
				_alreadyFetchedScheduledMessageHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection UIScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiUIScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemOccurrenceCollection. When set to true, UIScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchUIScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchUIScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when UIScheduleItemOccurrenceCollection has been fetched
		/// will clear the UIScheduleItemOccurrenceCollection collection well. Setting this property to true while UIScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for UIScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedUIScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemOccurrenceCollection && !value && (_uIScheduleItemOccurrenceCollection != null))
				{
					_uIScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'MessagegroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessagegroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MessagegroupEntity MessagegroupEntity
		{
			get	{ return GetSingleMessagegroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessagegroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemOccurrenceCollection", "MessagegroupEntity", _messagegroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupEntity. When set to true, MessagegroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupEntity is accessed. You can always execute a forced fetch by calling GetSingleMessagegroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupEntity
		{
			get	{ return _alwaysFetchMessagegroupEntity; }
			set	{ _alwaysFetchMessagegroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupEntity already has been fetched. Setting this property to false when MessagegroupEntity has been fetched
		/// will set MessagegroupEntity to null as well. Setting this property to true while MessagegroupEntity hasn't been fetched disables lazy loading for MessagegroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupEntity
		{
			get { return _alreadyFetchedMessagegroupEntity;}
			set 
			{
				if(_alreadyFetchedMessagegroupEntity && !value)
				{
					this.MessagegroupEntity = null;
				}
				_alreadyFetchedMessagegroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MessagegroupEntity is not found
		/// in the database. When set to true, MessagegroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MessagegroupEntityReturnsNewIfNotFound
		{
			get	{ return _messagegroupEntityReturnsNewIfNotFound; }
			set { _messagegroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportProcessingTaskTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportProcessingTaskTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReportProcessingTaskTemplateEntity ReportProcessingTaskTemplateEntity
		{
			get	{ return GetSingleReportProcessingTaskTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReportProcessingTaskTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemOccurrenceCollection", "ReportProcessingTaskTemplateEntity", _reportProcessingTaskTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskTemplateEntity. When set to true, ReportProcessingTaskTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleReportProcessingTaskTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskTemplateEntity
		{
			get	{ return _alwaysFetchReportProcessingTaskTemplateEntity; }
			set	{ _alwaysFetchReportProcessingTaskTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskTemplateEntity already has been fetched. Setting this property to false when ReportProcessingTaskTemplateEntity has been fetched
		/// will set ReportProcessingTaskTemplateEntity to null as well. Setting this property to true while ReportProcessingTaskTemplateEntity hasn't been fetched disables lazy loading for ReportProcessingTaskTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskTemplateEntity
		{
			get { return _alreadyFetchedReportProcessingTaskTemplateEntity;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskTemplateEntity && !value)
				{
					this.ReportProcessingTaskTemplateEntity = null;
				}
				_alreadyFetchedReportProcessingTaskTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportProcessingTaskTemplateEntity is not found
		/// in the database. When set to true, ReportProcessingTaskTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReportProcessingTaskTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _reportProcessingTaskTemplateEntityReturnsNewIfNotFound; }
			set { _reportProcessingTaskTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleEntity UIScheduleEntity
		{
			get	{ return GetSingleUIScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemOccurrenceCollection", "UIScheduleEntity", _uIScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleEntity. When set to true, UIScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleEntity
		{
			get	{ return _alwaysFetchUIScheduleEntity; }
			set	{ _alwaysFetchUIScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleEntity already has been fetched. Setting this property to false when UIScheduleEntity has been fetched
		/// will set UIScheduleEntity to null as well. Setting this property to true while UIScheduleEntity hasn't been fetched disables lazy loading for UIScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleEntity
		{
			get { return _alreadyFetchedUIScheduleEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleEntity && !value)
				{
					this.UIScheduleEntity = null;
				}
				_alreadyFetchedUIScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleEntity is not found
		/// in the database. When set to true, UIScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleEntityReturnsNewIfNotFound; }
			set { _uIScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleItemEntity UIScheduleItemEntity
		{
			get	{ return GetSingleUIScheduleItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemOccurrenceCollection", "UIScheduleItemEntity", _uIScheduleItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemEntity. When set to true, UIScheduleItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemEntity
		{
			get	{ return _alwaysFetchUIScheduleItemEntity; }
			set	{ _alwaysFetchUIScheduleItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemEntity already has been fetched. Setting this property to false when UIScheduleItemEntity has been fetched
		/// will set UIScheduleItemEntity to null as well. Setting this property to true while UIScheduleItemEntity hasn't been fetched disables lazy loading for UIScheduleItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemEntity
		{
			get { return _alreadyFetchedUIScheduleItemEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemEntity && !value)
				{
					this.UIScheduleItemEntity = null;
				}
				_alreadyFetchedUIScheduleItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleItemEntity is not found
		/// in the database. When set to true, UIScheduleItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleItemEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleItemEntityReturnsNewIfNotFound; }
			set { _uIScheduleItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleItemOccurrenceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentUIScheduleItemOccurrenceEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleItemOccurrenceEntity ParentUIScheduleItemOccurrenceEntity
		{
			get	{ return GetSingleParentUIScheduleItemOccurrenceEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentUIScheduleItemOccurrenceEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemOccurrenceCollection", "ParentUIScheduleItemOccurrenceEntity", _parentUIScheduleItemOccurrenceEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentUIScheduleItemOccurrenceEntity. When set to true, ParentUIScheduleItemOccurrenceEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentUIScheduleItemOccurrenceEntity is accessed. You can always execute a forced fetch by calling GetSingleParentUIScheduleItemOccurrenceEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentUIScheduleItemOccurrenceEntity
		{
			get	{ return _alwaysFetchParentUIScheduleItemOccurrenceEntity; }
			set	{ _alwaysFetchParentUIScheduleItemOccurrenceEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentUIScheduleItemOccurrenceEntity already has been fetched. Setting this property to false when ParentUIScheduleItemOccurrenceEntity has been fetched
		/// will set ParentUIScheduleItemOccurrenceEntity to null as well. Setting this property to true while ParentUIScheduleItemOccurrenceEntity hasn't been fetched disables lazy loading for ParentUIScheduleItemOccurrenceEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentUIScheduleItemOccurrenceEntity
		{
			get { return _alreadyFetchedParentUIScheduleItemOccurrenceEntity;}
			set 
			{
				if(_alreadyFetchedParentUIScheduleItemOccurrenceEntity && !value)
				{
					this.ParentUIScheduleItemOccurrenceEntity = null;
				}
				_alreadyFetchedParentUIScheduleItemOccurrenceEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentUIScheduleItemOccurrenceEntity is not found
		/// in the database. When set to true, ParentUIScheduleItemOccurrenceEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound
		{
			get	{ return _parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound; }
			set { _parentUIScheduleItemOccurrenceEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
