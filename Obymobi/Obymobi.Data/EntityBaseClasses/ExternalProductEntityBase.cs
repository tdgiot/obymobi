﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ExternalProduct'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ExternalProductEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ExternalProductEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationCollection	_alterationCollection;
		private bool	_alwaysFetchAlterationCollection, _alreadyFetchedAlterationCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection	_alterationoptionCollection;
		private bool	_alwaysFetchAlterationoptionCollection, _alreadyFetchedAlterationoptionCollection;
		private Obymobi.Data.CollectionClasses.ExternalSubProductCollection	_externalSubProductCollection;
		private bool	_alwaysFetchExternalSubProductCollection, _alreadyFetchedExternalSubProductCollection;
		private Obymobi.Data.CollectionClasses.ExternalSubProductCollection	_externalSubProductCollection1;
		private bool	_alwaysFetchExternalSubProductCollection1, _alreadyFetchedExternalSubProductCollection1;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private ExternalMenuEntity _externalMenuEntity;
		private bool	_alwaysFetchExternalMenuEntity, _alreadyFetchedExternalMenuEntity, _externalMenuEntityReturnsNewIfNotFound;
		private ExternalSystemEntity _externalSystemEntity;
		private bool	_alwaysFetchExternalSystemEntity, _alreadyFetchedExternalSystemEntity, _externalSystemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalMenuEntity</summary>
			public static readonly string ExternalMenuEntity = "ExternalMenuEntity";
			/// <summary>Member name ExternalSystemEntity</summary>
			public static readonly string ExternalSystemEntity = "ExternalSystemEntity";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name ExternalSubProductCollection</summary>
			public static readonly string ExternalSubProductCollection = "ExternalSubProductCollection";
			/// <summary>Member name ExternalSubProductCollection1</summary>
			public static readonly string ExternalSubProductCollection1 = "ExternalSubProductCollection1";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalProductEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ExternalProductEntityBase() :base("ExternalProductEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		protected ExternalProductEntityBase(System.Int32 externalProductId):base("ExternalProductEntity")
		{
			InitClassFetch(externalProductId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ExternalProductEntityBase(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse): base("ExternalProductEntity")
		{
			InitClassFetch(externalProductId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="validator">The custom validator object for this ExternalProductEntity</param>
		protected ExternalProductEntityBase(System.Int32 externalProductId, IValidator validator):base("ExternalProductEntity")
		{
			InitClassFetch(externalProductId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalProductEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationCollection = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollection = info.GetBoolean("_alwaysFetchAlterationCollection");
			_alreadyFetchedAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationCollection");

			_alterationoptionCollection = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollection = info.GetBoolean("_alwaysFetchAlterationoptionCollection");
			_alreadyFetchedAlterationoptionCollection = info.GetBoolean("_alreadyFetchedAlterationoptionCollection");

			_externalSubProductCollection = (Obymobi.Data.CollectionClasses.ExternalSubProductCollection)info.GetValue("_externalSubProductCollection", typeof(Obymobi.Data.CollectionClasses.ExternalSubProductCollection));
			_alwaysFetchExternalSubProductCollection = info.GetBoolean("_alwaysFetchExternalSubProductCollection");
			_alreadyFetchedExternalSubProductCollection = info.GetBoolean("_alreadyFetchedExternalSubProductCollection");

			_externalSubProductCollection1 = (Obymobi.Data.CollectionClasses.ExternalSubProductCollection)info.GetValue("_externalSubProductCollection1", typeof(Obymobi.Data.CollectionClasses.ExternalSubProductCollection));
			_alwaysFetchExternalSubProductCollection1 = info.GetBoolean("_alwaysFetchExternalSubProductCollection1");
			_alreadyFetchedExternalSubProductCollection1 = info.GetBoolean("_alreadyFetchedExternalSubProductCollection1");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");
			_externalMenuEntity = (ExternalMenuEntity)info.GetValue("_externalMenuEntity", typeof(ExternalMenuEntity));
			if(_externalMenuEntity!=null)
			{
				_externalMenuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalMenuEntityReturnsNewIfNotFound = info.GetBoolean("_externalMenuEntityReturnsNewIfNotFound");
			_alwaysFetchExternalMenuEntity = info.GetBoolean("_alwaysFetchExternalMenuEntity");
			_alreadyFetchedExternalMenuEntity = info.GetBoolean("_alreadyFetchedExternalMenuEntity");

			_externalSystemEntity = (ExternalSystemEntity)info.GetValue("_externalSystemEntity", typeof(ExternalSystemEntity));
			if(_externalSystemEntity!=null)
			{
				_externalSystemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalSystemEntityReturnsNewIfNotFound = info.GetBoolean("_externalSystemEntityReturnsNewIfNotFound");
			_alwaysFetchExternalSystemEntity = info.GetBoolean("_alwaysFetchExternalSystemEntity");
			_alreadyFetchedExternalSystemEntity = info.GetBoolean("_alreadyFetchedExternalSystemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalProductFieldIndex)fieldIndex)
			{
				case ExternalProductFieldIndex.ExternalSystemId:
					DesetupSyncExternalSystemEntity(true, false);
					_alreadyFetchedExternalSystemEntity = false;
					break;
				case ExternalProductFieldIndex.ExternalMenuId:
					DesetupSyncExternalMenuEntity(true, false);
					_alreadyFetchedExternalMenuEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationCollection = (_alterationCollection.Count > 0);
			_alreadyFetchedAlterationoptionCollection = (_alterationoptionCollection.Count > 0);
			_alreadyFetchedExternalSubProductCollection = (_externalSubProductCollection.Count > 0);
			_alreadyFetchedExternalSubProductCollection1 = (_externalSubProductCollection1.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedExternalMenuEntity = (_externalMenuEntity != null);
			_alreadyFetchedExternalSystemEntity = (_externalSystemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalMenuEntity":
					toReturn.Add(Relations.ExternalMenuEntityUsingExternalMenuId);
					break;
				case "ExternalSystemEntity":
					toReturn.Add(Relations.ExternalSystemEntityUsingExternalSystemId);
					break;
				case "AlterationCollection":
					toReturn.Add(Relations.AlterationEntityUsingExternalProductId);
					break;
				case "AlterationoptionCollection":
					toReturn.Add(Relations.AlterationoptionEntityUsingExternalProductId);
					break;
				case "ExternalSubProductCollection":
					toReturn.Add(Relations.ExternalSubProductEntityUsingExternalProductId);
					break;
				case "ExternalSubProductCollection1":
					toReturn.Add(Relations.ExternalSubProductEntityUsingExternalSubProductId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingExternalProductId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationCollection", (!this.MarkedForDeletion?_alterationCollection:null));
			info.AddValue("_alwaysFetchAlterationCollection", _alwaysFetchAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationCollection", _alreadyFetchedAlterationCollection);
			info.AddValue("_alterationoptionCollection", (!this.MarkedForDeletion?_alterationoptionCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionCollection", _alwaysFetchAlterationoptionCollection);
			info.AddValue("_alreadyFetchedAlterationoptionCollection", _alreadyFetchedAlterationoptionCollection);
			info.AddValue("_externalSubProductCollection", (!this.MarkedForDeletion?_externalSubProductCollection:null));
			info.AddValue("_alwaysFetchExternalSubProductCollection", _alwaysFetchExternalSubProductCollection);
			info.AddValue("_alreadyFetchedExternalSubProductCollection", _alreadyFetchedExternalSubProductCollection);
			info.AddValue("_externalSubProductCollection1", (!this.MarkedForDeletion?_externalSubProductCollection1:null));
			info.AddValue("_alwaysFetchExternalSubProductCollection1", _alwaysFetchExternalSubProductCollection1);
			info.AddValue("_alreadyFetchedExternalSubProductCollection1", _alreadyFetchedExternalSubProductCollection1);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_externalMenuEntity", (!this.MarkedForDeletion?_externalMenuEntity:null));
			info.AddValue("_externalMenuEntityReturnsNewIfNotFound", _externalMenuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalMenuEntity", _alwaysFetchExternalMenuEntity);
			info.AddValue("_alreadyFetchedExternalMenuEntity", _alreadyFetchedExternalMenuEntity);
			info.AddValue("_externalSystemEntity", (!this.MarkedForDeletion?_externalSystemEntity:null));
			info.AddValue("_externalSystemEntityReturnsNewIfNotFound", _externalSystemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalSystemEntity", _alwaysFetchExternalSystemEntity);
			info.AddValue("_alreadyFetchedExternalSystemEntity", _alreadyFetchedExternalSystemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalMenuEntity":
					_alreadyFetchedExternalMenuEntity = true;
					this.ExternalMenuEntity = (ExternalMenuEntity)entity;
					break;
				case "ExternalSystemEntity":
					_alreadyFetchedExternalSystemEntity = true;
					this.ExternalSystemEntity = (ExternalSystemEntity)entity;
					break;
				case "AlterationCollection":
					_alreadyFetchedAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationCollection.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollection":
					_alreadyFetchedAlterationoptionCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionCollection.Add((AlterationoptionEntity)entity);
					}
					break;
				case "ExternalSubProductCollection":
					_alreadyFetchedExternalSubProductCollection = true;
					if(entity!=null)
					{
						this.ExternalSubProductCollection.Add((ExternalSubProductEntity)entity);
					}
					break;
				case "ExternalSubProductCollection1":
					_alreadyFetchedExternalSubProductCollection1 = true;
					if(entity!=null)
					{
						this.ExternalSubProductCollection1.Add((ExternalSubProductEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalMenuEntity":
					SetupSyncExternalMenuEntity(relatedEntity);
					break;
				case "ExternalSystemEntity":
					SetupSyncExternalSystemEntity(relatedEntity);
					break;
				case "AlterationCollection":
					_alterationCollection.Add((AlterationEntity)relatedEntity);
					break;
				case "AlterationoptionCollection":
					_alterationoptionCollection.Add((AlterationoptionEntity)relatedEntity);
					break;
				case "ExternalSubProductCollection":
					_externalSubProductCollection.Add((ExternalSubProductEntity)relatedEntity);
					break;
				case "ExternalSubProductCollection1":
					_externalSubProductCollection1.Add((ExternalSubProductEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalMenuEntity":
					DesetupSyncExternalMenuEntity(false, true);
					break;
				case "ExternalSystemEntity":
					DesetupSyncExternalSystemEntity(false, true);
					break;
				case "AlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationoptionCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalSubProductCollection":
					this.PerformRelatedEntityRemoval(_externalSubProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalSubProductCollection1":
					this.PerformRelatedEntityRemoval(_externalSubProductCollection1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalMenuEntity!=null)
			{
				toReturn.Add(_externalMenuEntity);
			}
			if(_externalSystemEntity!=null)
			{
				toReturn.Add(_externalSystemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationCollection);
			toReturn.Add(_alterationoptionCollection);
			toReturn.Add(_externalSubProductCollection);
			toReturn.Add(_externalSubProductCollection1);
			toReturn.Add(_productCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductId)
		{
			return FetchUsingPK(externalProductId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(externalProductId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(externalProductId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(externalProductId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ExternalProductId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalProductRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationCollection || forceFetch || _alwaysFetchAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollection);
				_alterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollection.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_alterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollection = true;
			}
			return _alterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollection'. These settings will be taken into account
		/// when the property AlterationCollection is requested or GetMultiAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollection.SortClauses=sortClauses;
			_alterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollection || forceFetch || _alwaysFetchAlterationoptionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollection);
				_alterationoptionCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_alterationoptionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollection = true;
			}
			return _alterationoptionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollection'. These settings will be taken into account
		/// when the property AlterationoptionCollection is requested or GetMultiAlterationoptionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollection.SortClauses=sortClauses;
			_alterationoptionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSubProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection(bool forceFetch)
		{
			return GetMultiExternalSubProductCollection(forceFetch, _externalSubProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSubProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalSubProductCollection(forceFetch, _externalSubProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalSubProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalSubProductCollection || forceFetch || _alwaysFetchExternalSubProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalSubProductCollection);
				_externalSubProductCollection.SuppressClearInGetMulti=!forceFetch;
				_externalSubProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalSubProductCollection.GetMultiManyToOne(this, null, filter);
				_externalSubProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalSubProductCollection = true;
			}
			return _externalSubProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalSubProductCollection'. These settings will be taken into account
		/// when the property ExternalSubProductCollection is requested or GetMultiExternalSubProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalSubProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalSubProductCollection.SortClauses=sortClauses;
			_externalSubProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSubProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection1(bool forceFetch)
		{
			return GetMultiExternalSubProductCollection1(forceFetch, _externalSubProductCollection1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSubProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalSubProductCollection1(forceFetch, _externalSubProductCollection1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalSubProductCollection1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalSubProductCollection GetMultiExternalSubProductCollection1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalSubProductCollection1 || forceFetch || _alwaysFetchExternalSubProductCollection1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalSubProductCollection1);
				_externalSubProductCollection1.SuppressClearInGetMulti=!forceFetch;
				_externalSubProductCollection1.EntityFactoryToUse = entityFactoryToUse;
				_externalSubProductCollection1.GetMultiManyToOne(null, this, filter);
				_externalSubProductCollection1.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalSubProductCollection1 = true;
			}
			return _externalSubProductCollection1;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalSubProductCollection1'. These settings will be taken into account
		/// when the property ExternalSubProductCollection1 is requested or GetMultiExternalSubProductCollection1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalSubProductCollection1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalSubProductCollection1.SortClauses=sortClauses;
			_externalSubProductCollection1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ExternalMenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalMenuEntity' which is related to this entity.</returns>
		public ExternalMenuEntity GetSingleExternalMenuEntity()
		{
			return GetSingleExternalMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalMenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalMenuEntity' which is related to this entity.</returns>
		public virtual ExternalMenuEntity GetSingleExternalMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalMenuEntity || forceFetch || _alwaysFetchExternalMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalMenuEntityUsingExternalMenuId);
				ExternalMenuEntity newEntity = new ExternalMenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalMenuId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalMenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalMenuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalMenuEntity = newEntity;
				_alreadyFetchedExternalMenuEntity = fetchResult;
			}
			return _externalMenuEntity;
		}


		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public ExternalSystemEntity GetSingleExternalSystemEntity()
		{
			return GetSingleExternalSystemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public virtual ExternalSystemEntity GetSingleExternalSystemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalSystemEntity || forceFetch || _alwaysFetchExternalSystemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalSystemEntityUsingExternalSystemId);
				ExternalSystemEntity newEntity = new ExternalSystemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalSystemId);
				}
				if(fetchResult)
				{
					newEntity = (ExternalSystemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalSystemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalSystemEntity = newEntity;
				_alreadyFetchedExternalSystemEntity = fetchResult;
			}
			return _externalSystemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalMenuEntity", _externalMenuEntity);
			toReturn.Add("ExternalSystemEntity", _externalSystemEntity);
			toReturn.Add("AlterationCollection", _alterationCollection);
			toReturn.Add("AlterationoptionCollection", _alterationoptionCollection);
			toReturn.Add("ExternalSubProductCollection", _externalSubProductCollection);
			toReturn.Add("ExternalSubProductCollection1", _externalSubProductCollection1);
			toReturn.Add("ProductCollection", _productCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="validator">The validator object for this ExternalProductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 externalProductId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(externalProductId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationCollection = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollection.SetContainingEntityInfo(this, "ExternalProductEntity");

			_alterationoptionCollection = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_alterationoptionCollection.SetContainingEntityInfo(this, "ExternalProductEntity");

			_externalSubProductCollection = new Obymobi.Data.CollectionClasses.ExternalSubProductCollection();
			_externalSubProductCollection.SetContainingEntityInfo(this, "ExternalProductEntity");

			_externalSubProductCollection1 = new Obymobi.Data.CollectionClasses.ExternalSubProductCollection();
			_externalSubProductCollection1.SetContainingEntityInfo(this, "ExternalProductEntity1");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "ExternalProductEntity");
			_externalMenuEntityReturnsNewIfNotFound = true;
			_externalSystemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSystemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronisationBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowDuplicates", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsSnoozed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalMenuId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalMenuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalMenuEntity, new PropertyChangedEventHandler( OnExternalMenuEntityPropertyChanged ), "ExternalMenuEntity", Obymobi.Data.RelationClasses.StaticExternalProductRelations.ExternalMenuEntityUsingExternalMenuIdStatic, true, signalRelatedEntity, "ExternalProductCollection", resetFKFields, new int[] { (int)ExternalProductFieldIndex.ExternalMenuId } );		
			_externalMenuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalMenuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalMenuEntity(IEntityCore relatedEntity)
		{
			if(_externalMenuEntity!=relatedEntity)
			{		
				DesetupSyncExternalMenuEntity(true, true);
				_externalMenuEntity = (ExternalMenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalMenuEntity, new PropertyChangedEventHandler( OnExternalMenuEntityPropertyChanged ), "ExternalMenuEntity", Obymobi.Data.RelationClasses.StaticExternalProductRelations.ExternalMenuEntityUsingExternalMenuIdStatic, true, ref _alreadyFetchedExternalMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalSystemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalSystemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalProductRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, signalRelatedEntity, "ExternalProductCollection", resetFKFields, new int[] { (int)ExternalProductFieldIndex.ExternalSystemId } );		
			_externalSystemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalSystemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalSystemEntity(IEntityCore relatedEntity)
		{
			if(_externalSystemEntity!=relatedEntity)
			{		
				DesetupSyncExternalSystemEntity(true, true);
				_externalSystemEntity = (ExternalSystemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalProductRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, ref _alreadyFetchedExternalSystemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalSystemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalProductFieldIndex.ExternalProductId].ForcedCurrentValueWrite(externalProductId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalProductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalProductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalProductRelations Relations
		{
			get	{ return new ExternalProductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationCollection")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionCollection")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSubProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSubProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSubProductCollection(), (IEntityRelation)GetRelationsForField("ExternalSubProductCollection")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.ExternalSubProductEntity, 0, null, null, null, "ExternalSubProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSubProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSubProductCollection1
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSubProductCollection(), (IEntityRelation)GetRelationsForField("ExternalSubProductCollection1")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.ExternalSubProductEntity, 0, null, null, null, "ExternalSubProductCollection1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalMenu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalMenuCollection(), (IEntityRelation)GetRelationsForField("ExternalMenuEntity")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.ExternalMenuEntity, 0, null, null, null, "ExternalMenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemEntity")[0], (int)Obymobi.Data.EntityType.ExternalProductEntity, (int)Obymobi.Data.EntityType.ExternalSystemEntity, 0, null, null, null, "ExternalSystemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ExternalProductId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ExternalProductId, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.ExternalProductId, value, true); }
		}

		/// <summary> The ExternalSystemId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalSystemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.ExternalSystemId, value, true); }
		}

		/// <summary> The Id property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Id, value, true); }
		}

		/// <summary> The Name property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Name, value, true); }
		}

		/// <summary> The Price property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Price"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)ExternalProductFieldIndex.Price, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Price, value, true); }
		}

		/// <summary> The Visible property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.Visible, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Visible, value, true); }
		}

		/// <summary> The StringValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue1, value, true); }
		}

		/// <summary> The StringValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue2, value, true); }
		}

		/// <summary> The StringValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue3, value, true); }
		}

		/// <summary> The StringValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue4, value, true); }
		}

		/// <summary> The StringValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."StringValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalProductFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.StringValue5, value, true); }
		}

		/// <summary> The IntValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue1
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue1, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue1, value, true); }
		}

		/// <summary> The IntValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue2
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue2, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue2, value, true); }
		}

		/// <summary> The IntValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue3
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue3, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue3, value, true); }
		}

		/// <summary> The IntValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue4
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue4, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue4, value, true); }
		}

		/// <summary> The IntValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IntValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue5
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.IntValue5, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.IntValue5, value, true); }
		}

		/// <summary> The BoolValue1 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue1, value, true); }
		}

		/// <summary> The BoolValue2 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue2, value, true); }
		}

		/// <summary> The BoolValue3 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue3, value, true); }
		}

		/// <summary> The BoolValue4 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue4, value, true); }
		}

		/// <summary> The BoolValue5 property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."BoolValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalProductFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.BoolValue5, value, true); }
		}

		/// <summary> The CreatedInBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedInBatchId, value, true); }
		}

		/// <summary> The UpdatedInBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedInBatchId, value, true); }
		}

		/// <summary> The SynchronisationBatchId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."SynchronisationBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalProductFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.SynchronisationBatchId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalProductFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The Type property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ExternalProductType Type
		{
			get { return (Obymobi.Enums.ExternalProductType)GetValue((int)ExternalProductFieldIndex.Type, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.Type, value, true); }
		}

		/// <summary> The MinOptions property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."MinOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinOptions
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.MinOptions, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.MinOptions, value, true); }
		}

		/// <summary> The MaxOptions property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."MaxOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxOptions
		{
			get { return (System.Int32)GetValue((int)ExternalProductFieldIndex.MaxOptions, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.MaxOptions, value, true); }
		}

		/// <summary> The AllowDuplicates property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."AllowDuplicates"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowDuplicates
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.AllowDuplicates, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.AllowDuplicates, value, true); }
		}

		/// <summary> The IsEnabled property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IsEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsEnabled
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.IsEnabled, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.IsEnabled, value, true); }
		}

		/// <summary> The IsSnoozed property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."IsSnoozed"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsSnoozed
		{
			get { return (System.Boolean)GetValue((int)ExternalProductFieldIndex.IsSnoozed, true); }
			set	{ SetValue((int)ExternalProductFieldIndex.IsSnoozed, value, true); }
		}

		/// <summary> The ExternalMenuId property of the Entity ExternalProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalProduct"."ExternalMenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalMenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalProductFieldIndex.ExternalMenuId, false); }
			set	{ SetValue((int)ExternalProductFieldIndex.ExternalMenuId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollection
		{
			get	{ return GetMultiAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollection. When set to true, AlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollection
		{
			get	{ return _alwaysFetchAlterationCollection; }
			set	{ _alwaysFetchAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollection already has been fetched. Setting this property to false when AlterationCollection has been fetched
		/// will clear the AlterationCollection collection well. Setting this property to true while AlterationCollection hasn't been fetched disables lazy loading for AlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollection
		{
			get { return _alreadyFetchedAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationCollection && !value && (_alterationCollection != null))
				{
					_alterationCollection.Clear();
				}
				_alreadyFetchedAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollection
		{
			get	{ return GetMultiAlterationoptionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollection. When set to true, AlterationoptionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollection
		{
			get	{ return _alwaysFetchAlterationoptionCollection; }
			set	{ _alwaysFetchAlterationoptionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollection already has been fetched. Setting this property to false when AlterationoptionCollection has been fetched
		/// will clear the AlterationoptionCollection collection well. Setting this property to true while AlterationoptionCollection hasn't been fetched disables lazy loading for AlterationoptionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollection
		{
			get { return _alreadyFetchedAlterationoptionCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollection && !value && (_alterationoptionCollection != null))
				{
					_alterationoptionCollection.Clear();
				}
				_alreadyFetchedAlterationoptionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalSubProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalSubProductCollection ExternalSubProductCollection
		{
			get	{ return GetMultiExternalSubProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSubProductCollection. When set to true, ExternalSubProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSubProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalSubProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSubProductCollection
		{
			get	{ return _alwaysFetchExternalSubProductCollection; }
			set	{ _alwaysFetchExternalSubProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSubProductCollection already has been fetched. Setting this property to false when ExternalSubProductCollection has been fetched
		/// will clear the ExternalSubProductCollection collection well. Setting this property to true while ExternalSubProductCollection hasn't been fetched disables lazy loading for ExternalSubProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSubProductCollection
		{
			get { return _alreadyFetchedExternalSubProductCollection;}
			set 
			{
				if(_alreadyFetchedExternalSubProductCollection && !value && (_externalSubProductCollection != null))
				{
					_externalSubProductCollection.Clear();
				}
				_alreadyFetchedExternalSubProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalSubProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalSubProductCollection1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalSubProductCollection ExternalSubProductCollection1
		{
			get	{ return GetMultiExternalSubProductCollection1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSubProductCollection1. When set to true, ExternalSubProductCollection1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSubProductCollection1 is accessed. You can always execute/ a forced fetch by calling GetMultiExternalSubProductCollection1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSubProductCollection1
		{
			get	{ return _alwaysFetchExternalSubProductCollection1; }
			set	{ _alwaysFetchExternalSubProductCollection1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSubProductCollection1 already has been fetched. Setting this property to false when ExternalSubProductCollection1 has been fetched
		/// will clear the ExternalSubProductCollection1 collection well. Setting this property to true while ExternalSubProductCollection1 hasn't been fetched disables lazy loading for ExternalSubProductCollection1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSubProductCollection1
		{
			get { return _alreadyFetchedExternalSubProductCollection1;}
			set 
			{
				if(_alreadyFetchedExternalSubProductCollection1 && !value && (_externalSubProductCollection1 != null))
				{
					_externalSubProductCollection1.Clear();
				}
				_alreadyFetchedExternalSubProductCollection1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ExternalMenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalMenuEntity ExternalMenuEntity
		{
			get	{ return GetSingleExternalMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalProductCollection", "ExternalMenuEntity", _externalMenuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalMenuEntity. When set to true, ExternalMenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalMenuEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalMenuEntity
		{
			get	{ return _alwaysFetchExternalMenuEntity; }
			set	{ _alwaysFetchExternalMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalMenuEntity already has been fetched. Setting this property to false when ExternalMenuEntity has been fetched
		/// will set ExternalMenuEntity to null as well. Setting this property to true while ExternalMenuEntity hasn't been fetched disables lazy loading for ExternalMenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalMenuEntity
		{
			get { return _alreadyFetchedExternalMenuEntity;}
			set 
			{
				if(_alreadyFetchedExternalMenuEntity && !value)
				{
					this.ExternalMenuEntity = null;
				}
				_alreadyFetchedExternalMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalMenuEntity is not found
		/// in the database. When set to true, ExternalMenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalMenuEntityReturnsNewIfNotFound
		{
			get	{ return _externalMenuEntityReturnsNewIfNotFound; }
			set { _externalMenuEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalSystemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalSystemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalSystemEntity ExternalSystemEntity
		{
			get	{ return GetSingleExternalSystemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalSystemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalProductCollection", "ExternalSystemEntity", _externalSystemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemEntity. When set to true, ExternalSystemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalSystemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemEntity
		{
			get	{ return _alwaysFetchExternalSystemEntity; }
			set	{ _alwaysFetchExternalSystemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemEntity already has been fetched. Setting this property to false when ExternalSystemEntity has been fetched
		/// will set ExternalSystemEntity to null as well. Setting this property to true while ExternalSystemEntity hasn't been fetched disables lazy loading for ExternalSystemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemEntity
		{
			get { return _alreadyFetchedExternalSystemEntity;}
			set 
			{
				if(_alreadyFetchedExternalSystemEntity && !value)
				{
					this.ExternalSystemEntity = null;
				}
				_alreadyFetchedExternalSystemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalSystemEntity is not found
		/// in the database. When set to true, ExternalSystemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalSystemEntityReturnsNewIfNotFound
		{
			get	{ return _externalSystemEntityReturnsNewIfNotFound; }
			set { _externalSystemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ExternalProductEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
