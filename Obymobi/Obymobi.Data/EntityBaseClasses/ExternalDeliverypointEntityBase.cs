﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ExternalDeliverypoint'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ExternalDeliverypointEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ExternalDeliverypointEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection	_deliverypointExternalDeliverypointCollection;
		private bool	_alwaysFetchDeliverypointExternalDeliverypointCollection, _alreadyFetchedDeliverypointExternalDeliverypointCollection;
		private ExternalSystemEntity _externalSystemEntity;
		private bool	_alwaysFetchExternalSystemEntity, _alreadyFetchedExternalSystemEntity, _externalSystemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystemEntity</summary>
			public static readonly string ExternalSystemEntity = "ExternalSystemEntity";
			/// <summary>Member name DeliverypointExternalDeliverypointCollection</summary>
			public static readonly string DeliverypointExternalDeliverypointCollection = "DeliverypointExternalDeliverypointCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalDeliverypointEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ExternalDeliverypointEntityBase() :base("ExternalDeliverypointEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		protected ExternalDeliverypointEntityBase(System.Int32 externalDeliverypointId):base("ExternalDeliverypointEntity")
		{
			InitClassFetch(externalDeliverypointId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ExternalDeliverypointEntityBase(System.Int32 externalDeliverypointId, IPrefetchPath prefetchPathToUse): base("ExternalDeliverypointEntity")
		{
			InitClassFetch(externalDeliverypointId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this ExternalDeliverypointEntity</param>
		protected ExternalDeliverypointEntityBase(System.Int32 externalDeliverypointId, IValidator validator):base("ExternalDeliverypointEntity")
		{
			InitClassFetch(externalDeliverypointId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalDeliverypointEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deliverypointExternalDeliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection)info.GetValue("_deliverypointExternalDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection));
			_alwaysFetchDeliverypointExternalDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointExternalDeliverypointCollection");
			_alreadyFetchedDeliverypointExternalDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointExternalDeliverypointCollection");
			_externalSystemEntity = (ExternalSystemEntity)info.GetValue("_externalSystemEntity", typeof(ExternalSystemEntity));
			if(_externalSystemEntity!=null)
			{
				_externalSystemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalSystemEntityReturnsNewIfNotFound = info.GetBoolean("_externalSystemEntityReturnsNewIfNotFound");
			_alwaysFetchExternalSystemEntity = info.GetBoolean("_alwaysFetchExternalSystemEntity");
			_alreadyFetchedExternalSystemEntity = info.GetBoolean("_alreadyFetchedExternalSystemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalDeliverypointFieldIndex)fieldIndex)
			{
				case ExternalDeliverypointFieldIndex.ExternalSystemId:
					DesetupSyncExternalSystemEntity(true, false);
					_alreadyFetchedExternalSystemEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeliverypointExternalDeliverypointCollection = (_deliverypointExternalDeliverypointCollection.Count > 0);
			_alreadyFetchedExternalSystemEntity = (_externalSystemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					toReturn.Add(Relations.ExternalSystemEntityUsingExternalSystemId);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					toReturn.Add(Relations.DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deliverypointExternalDeliverypointCollection", (!this.MarkedForDeletion?_deliverypointExternalDeliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointExternalDeliverypointCollection", _alwaysFetchDeliverypointExternalDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointExternalDeliverypointCollection", _alreadyFetchedDeliverypointExternalDeliverypointCollection);
			info.AddValue("_externalSystemEntity", (!this.MarkedForDeletion?_externalSystemEntity:null));
			info.AddValue("_externalSystemEntityReturnsNewIfNotFound", _externalSystemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalSystemEntity", _alwaysFetchExternalSystemEntity);
			info.AddValue("_alreadyFetchedExternalSystemEntity", _alreadyFetchedExternalSystemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalSystemEntity":
					_alreadyFetchedExternalSystemEntity = true;
					this.ExternalSystemEntity = (ExternalSystemEntity)entity;
					break;
				case "DeliverypointExternalDeliverypointCollection":
					_alreadyFetchedDeliverypointExternalDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointExternalDeliverypointCollection.Add((DeliverypointExternalDeliverypointEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					SetupSyncExternalSystemEntity(relatedEntity);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					_deliverypointExternalDeliverypointCollection.Add((DeliverypointExternalDeliverypointEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					DesetupSyncExternalSystemEntity(false, true);
					break;
				case "DeliverypointExternalDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointExternalDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalSystemEntity!=null)
			{
				toReturn.Add(_externalSystemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deliverypointExternalDeliverypointCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalDeliverypointId)
		{
			return FetchUsingPK(externalDeliverypointId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalDeliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(externalDeliverypointId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(externalDeliverypointId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(externalDeliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ExternalDeliverypointId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalDeliverypointRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, _deliverypointExternalDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, _deliverypointExternalDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointExternalDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection GetMultiDeliverypointExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointExternalDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointExternalDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointExternalDeliverypointCollection);
				_deliverypointExternalDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointExternalDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointExternalDeliverypointCollection.GetMultiManyToOne(null, this, filter);
				_deliverypointExternalDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointExternalDeliverypointCollection = true;
			}
			return _deliverypointExternalDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointExternalDeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointExternalDeliverypointCollection is requested or GetMultiDeliverypointExternalDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointExternalDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointExternalDeliverypointCollection.SortClauses=sortClauses;
			_deliverypointExternalDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public ExternalSystemEntity GetSingleExternalSystemEntity()
		{
			return GetSingleExternalSystemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public virtual ExternalSystemEntity GetSingleExternalSystemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalSystemEntity || forceFetch || _alwaysFetchExternalSystemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalSystemEntityUsingExternalSystemId);
				ExternalSystemEntity newEntity = new ExternalSystemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalSystemId);
				}
				if(fetchResult)
				{
					newEntity = (ExternalSystemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalSystemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalSystemEntity = newEntity;
				_alreadyFetchedExternalSystemEntity = fetchResult;
			}
			return _externalSystemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalSystemEntity", _externalSystemEntity);
			toReturn.Add("DeliverypointExternalDeliverypointCollection", _deliverypointExternalDeliverypointCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="validator">The validator object for this ExternalDeliverypointEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 externalDeliverypointId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(externalDeliverypointId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deliverypointExternalDeliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection();
			_deliverypointExternalDeliverypointCollection.SetContainingEntityInfo(this, "ExternalDeliverypointEntity");
			_externalSystemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSystemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronisationBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalSystemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalSystemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalDeliverypointRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, signalRelatedEntity, "ExternalDeliverypointCollection", resetFKFields, new int[] { (int)ExternalDeliverypointFieldIndex.ExternalSystemId } );		
			_externalSystemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalSystemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalSystemEntity(IEntityCore relatedEntity)
		{
			if(_externalSystemEntity!=relatedEntity)
			{		
				DesetupSyncExternalSystemEntity(true, true);
				_externalSystemEntity = (ExternalSystemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalDeliverypointRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, ref _alreadyFetchedExternalSystemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalSystemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="externalDeliverypointId">PK value for ExternalDeliverypoint which data should be fetched into this ExternalDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 externalDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalDeliverypointFieldIndex.ExternalDeliverypointId].ForcedCurrentValueWrite(externalDeliverypointId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalDeliverypointDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalDeliverypointEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalDeliverypointRelations Relations
		{
			get	{ return new ExternalDeliverypointRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointExternalDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointExternalDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.ExternalDeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity, 0, null, null, null, "DeliverypointExternalDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemEntity")[0], (int)Obymobi.Data.EntityType.ExternalDeliverypointEntity, (int)Obymobi.Data.EntityType.ExternalSystemEntity, 0, null, null, null, "ExternalSystemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ExternalDeliverypointId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ExternalDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalDeliverypointId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ExternalDeliverypointId, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.ExternalDeliverypointId, value, true); }
		}

		/// <summary> The ExternalSystemId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ExternalSystemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.ExternalSystemId, value, true); }
		}

		/// <summary> The Id property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Id, value, true); }
		}

		/// <summary> The Name property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Name, value, true); }
		}

		/// <summary> The Visible property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)ExternalDeliverypointFieldIndex.Visible, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.Visible, value, true); }
		}

		/// <summary> The StringValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue1, value, true); }
		}

		/// <summary> The StringValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue2, value, true); }
		}

		/// <summary> The StringValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue3, value, true); }
		}

		/// <summary> The StringValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue4, value, true); }
		}

		/// <summary> The StringValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."StringValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalDeliverypointFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.StringValue5, value, true); }
		}

		/// <summary> The IntValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue1
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue1, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue1, value, true); }
		}

		/// <summary> The IntValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue2
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue2, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue2, value, true); }
		}

		/// <summary> The IntValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue3
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue3, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue3, value, true); }
		}

		/// <summary> The IntValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue4
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue4, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue4, value, true); }
		}

		/// <summary> The IntValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."IntValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IntValue5
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.IntValue5, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.IntValue5, value, true); }
		}

		/// <summary> The BoolValue1 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue1, value, true); }
		}

		/// <summary> The BoolValue2 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue2, value, true); }
		}

		/// <summary> The BoolValue3 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue3, value, true); }
		}

		/// <summary> The BoolValue4 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue4, value, true); }
		}

		/// <summary> The BoolValue5 property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."BoolValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalDeliverypointFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.BoolValue5, value, true); }
		}

		/// <summary> The CreatedInBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedInBatchId, value, true); }
		}

		/// <summary> The UpdatedInBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedInBatchId, value, true); }
		}

		/// <summary> The SynchronisationBatchId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."SynchronisationBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalDeliverypointFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.SynchronisationBatchId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalDeliverypointFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ExternalDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalDeliverypoint"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalDeliverypointFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalDeliverypointFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointExternalDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointExternalDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointExternalDeliverypointCollection DeliverypointExternalDeliverypointCollection
		{
			get	{ return GetMultiDeliverypointExternalDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointExternalDeliverypointCollection. When set to true, DeliverypointExternalDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointExternalDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointExternalDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointExternalDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointExternalDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointExternalDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointExternalDeliverypointCollection already has been fetched. Setting this property to false when DeliverypointExternalDeliverypointCollection has been fetched
		/// will clear the DeliverypointExternalDeliverypointCollection collection well. Setting this property to true while DeliverypointExternalDeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointExternalDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointExternalDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointExternalDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointExternalDeliverypointCollection && !value && (_deliverypointExternalDeliverypointCollection != null))
				{
					_deliverypointExternalDeliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointExternalDeliverypointCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ExternalSystemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalSystemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalSystemEntity ExternalSystemEntity
		{
			get	{ return GetSingleExternalSystemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalSystemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalDeliverypointCollection", "ExternalSystemEntity", _externalSystemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemEntity. When set to true, ExternalSystemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalSystemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemEntity
		{
			get	{ return _alwaysFetchExternalSystemEntity; }
			set	{ _alwaysFetchExternalSystemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemEntity already has been fetched. Setting this property to false when ExternalSystemEntity has been fetched
		/// will set ExternalSystemEntity to null as well. Setting this property to true while ExternalSystemEntity hasn't been fetched disables lazy loading for ExternalSystemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemEntity
		{
			get { return _alreadyFetchedExternalSystemEntity;}
			set 
			{
				if(_alreadyFetchedExternalSystemEntity && !value)
				{
					this.ExternalSystemEntity = null;
				}
				_alreadyFetchedExternalSystemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalSystemEntity is not found
		/// in the database. When set to true, ExternalSystemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalSystemEntityReturnsNewIfNotFound
		{
			get	{ return _externalSystemEntityReturnsNewIfNotFound; }
			set { _externalSystemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ExternalDeliverypointEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
