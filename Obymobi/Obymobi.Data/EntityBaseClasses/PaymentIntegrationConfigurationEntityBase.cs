﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PaymentIntegrationConfiguration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PaymentIntegrationConfigurationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PaymentIntegrationConfigurationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection	_adyenPaymentMethodCollection;
		private bool	_alwaysFetchAdyenPaymentMethodCollection, _alreadyFetchedAdyenPaymentMethodCollection;
		private Obymobi.Data.CollectionClasses.CheckoutMethodCollection	_checkoutMethodCollection;
		private bool	_alwaysFetchCheckoutMethodCollection, _alreadyFetchedCheckoutMethodCollection;
		private Obymobi.Data.CollectionClasses.PaymentTransactionCollection	_paymentTransactionCollection;
		private bool	_alwaysFetchPaymentTransactionCollection, _alreadyFetchedPaymentTransactionCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private PaymentProviderEntity _paymentProviderEntity;
		private bool	_alwaysFetchPaymentProviderEntity, _alreadyFetchedPaymentProviderEntity, _paymentProviderEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name PaymentProviderEntity</summary>
			public static readonly string PaymentProviderEntity = "PaymentProviderEntity";
			/// <summary>Member name AdyenPaymentMethodCollection</summary>
			public static readonly string AdyenPaymentMethodCollection = "AdyenPaymentMethodCollection";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentIntegrationConfigurationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PaymentIntegrationConfigurationEntityBase() :base("PaymentIntegrationConfigurationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		protected PaymentIntegrationConfigurationEntityBase(System.Int32 paymentIntegrationConfigurationId):base("PaymentIntegrationConfigurationEntity")
		{
			InitClassFetch(paymentIntegrationConfigurationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PaymentIntegrationConfigurationEntityBase(System.Int32 paymentIntegrationConfigurationId, IPrefetchPath prefetchPathToUse): base("PaymentIntegrationConfigurationEntity")
		{
			InitClassFetch(paymentIntegrationConfigurationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="validator">The custom validator object for this PaymentIntegrationConfigurationEntity</param>
		protected PaymentIntegrationConfigurationEntityBase(System.Int32 paymentIntegrationConfigurationId, IValidator validator):base("PaymentIntegrationConfigurationEntity")
		{
			InitClassFetch(paymentIntegrationConfigurationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentIntegrationConfigurationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_adyenPaymentMethodCollection = (Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection)info.GetValue("_adyenPaymentMethodCollection", typeof(Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection));
			_alwaysFetchAdyenPaymentMethodCollection = info.GetBoolean("_alwaysFetchAdyenPaymentMethodCollection");
			_alreadyFetchedAdyenPaymentMethodCollection = info.GetBoolean("_alreadyFetchedAdyenPaymentMethodCollection");

			_checkoutMethodCollection = (Obymobi.Data.CollectionClasses.CheckoutMethodCollection)info.GetValue("_checkoutMethodCollection", typeof(Obymobi.Data.CollectionClasses.CheckoutMethodCollection));
			_alwaysFetchCheckoutMethodCollection = info.GetBoolean("_alwaysFetchCheckoutMethodCollection");
			_alreadyFetchedCheckoutMethodCollection = info.GetBoolean("_alreadyFetchedCheckoutMethodCollection");

			_paymentTransactionCollection = (Obymobi.Data.CollectionClasses.PaymentTransactionCollection)info.GetValue("_paymentTransactionCollection", typeof(Obymobi.Data.CollectionClasses.PaymentTransactionCollection));
			_alwaysFetchPaymentTransactionCollection = info.GetBoolean("_alwaysFetchPaymentTransactionCollection");
			_alreadyFetchedPaymentTransactionCollection = info.GetBoolean("_alreadyFetchedPaymentTransactionCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_paymentProviderEntity = (PaymentProviderEntity)info.GetValue("_paymentProviderEntity", typeof(PaymentProviderEntity));
			if(_paymentProviderEntity!=null)
			{
				_paymentProviderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentProviderEntityReturnsNewIfNotFound = info.GetBoolean("_paymentProviderEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentProviderEntity = info.GetBoolean("_alwaysFetchPaymentProviderEntity");
			_alreadyFetchedPaymentProviderEntity = info.GetBoolean("_alreadyFetchedPaymentProviderEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentIntegrationConfigurationFieldIndex)fieldIndex)
			{
				case PaymentIntegrationConfigurationFieldIndex.PaymentProviderId:
					DesetupSyncPaymentProviderEntity(true, false);
					_alreadyFetchedPaymentProviderEntity = false;
					break;
				case PaymentIntegrationConfigurationFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdyenPaymentMethodCollection = (_adyenPaymentMethodCollection.Count > 0);
			_alreadyFetchedCheckoutMethodCollection = (_checkoutMethodCollection.Count > 0);
			_alreadyFetchedPaymentTransactionCollection = (_paymentTransactionCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedPaymentProviderEntity = (_paymentProviderEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "PaymentProviderEntity":
					toReturn.Add(Relations.PaymentProviderEntityUsingPaymentProviderId);
					break;
				case "AdyenPaymentMethodCollection":
					toReturn.Add(Relations.AdyenPaymentMethodEntityUsingPaymentIntegrationConfigurationId);
					break;
				case "CheckoutMethodCollection":
					toReturn.Add(Relations.CheckoutMethodEntityUsingPaymentIntegrationConfigurationId);
					break;
				case "PaymentTransactionCollection":
					toReturn.Add(Relations.PaymentTransactionEntityUsingPaymentIntegrationConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_adyenPaymentMethodCollection", (!this.MarkedForDeletion?_adyenPaymentMethodCollection:null));
			info.AddValue("_alwaysFetchAdyenPaymentMethodCollection", _alwaysFetchAdyenPaymentMethodCollection);
			info.AddValue("_alreadyFetchedAdyenPaymentMethodCollection", _alreadyFetchedAdyenPaymentMethodCollection);
			info.AddValue("_checkoutMethodCollection", (!this.MarkedForDeletion?_checkoutMethodCollection:null));
			info.AddValue("_alwaysFetchCheckoutMethodCollection", _alwaysFetchCheckoutMethodCollection);
			info.AddValue("_alreadyFetchedCheckoutMethodCollection", _alreadyFetchedCheckoutMethodCollection);
			info.AddValue("_paymentTransactionCollection", (!this.MarkedForDeletion?_paymentTransactionCollection:null));
			info.AddValue("_alwaysFetchPaymentTransactionCollection", _alwaysFetchPaymentTransactionCollection);
			info.AddValue("_alreadyFetchedPaymentTransactionCollection", _alreadyFetchedPaymentTransactionCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_paymentProviderEntity", (!this.MarkedForDeletion?_paymentProviderEntity:null));
			info.AddValue("_paymentProviderEntityReturnsNewIfNotFound", _paymentProviderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentProviderEntity", _alwaysFetchPaymentProviderEntity);
			info.AddValue("_alreadyFetchedPaymentProviderEntity", _alreadyFetchedPaymentProviderEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "PaymentProviderEntity":
					_alreadyFetchedPaymentProviderEntity = true;
					this.PaymentProviderEntity = (PaymentProviderEntity)entity;
					break;
				case "AdyenPaymentMethodCollection":
					_alreadyFetchedAdyenPaymentMethodCollection = true;
					if(entity!=null)
					{
						this.AdyenPaymentMethodCollection.Add((AdyenPaymentMethodEntity)entity);
					}
					break;
				case "CheckoutMethodCollection":
					_alreadyFetchedCheckoutMethodCollection = true;
					if(entity!=null)
					{
						this.CheckoutMethodCollection.Add((CheckoutMethodEntity)entity);
					}
					break;
				case "PaymentTransactionCollection":
					_alreadyFetchedPaymentTransactionCollection = true;
					if(entity!=null)
					{
						this.PaymentTransactionCollection.Add((PaymentTransactionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "PaymentProviderEntity":
					SetupSyncPaymentProviderEntity(relatedEntity);
					break;
				case "AdyenPaymentMethodCollection":
					_adyenPaymentMethodCollection.Add((AdyenPaymentMethodEntity)relatedEntity);
					break;
				case "CheckoutMethodCollection":
					_checkoutMethodCollection.Add((CheckoutMethodEntity)relatedEntity);
					break;
				case "PaymentTransactionCollection":
					_paymentTransactionCollection.Add((PaymentTransactionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "PaymentProviderEntity":
					DesetupSyncPaymentProviderEntity(false, true);
					break;
				case "AdyenPaymentMethodCollection":
					this.PerformRelatedEntityRemoval(_adyenPaymentMethodCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CheckoutMethodCollection":
					this.PerformRelatedEntityRemoval(_checkoutMethodCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentTransactionCollection":
					this.PerformRelatedEntityRemoval(_paymentTransactionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_paymentProviderEntity!=null)
			{
				toReturn.Add(_paymentProviderEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_adyenPaymentMethodCollection);
			toReturn.Add(_checkoutMethodCollection);
			toReturn.Add(_paymentTransactionCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntegrationConfigurationId)
		{
			return FetchUsingPK(paymentIntegrationConfigurationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntegrationConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentIntegrationConfigurationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntegrationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentIntegrationConfigurationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntegrationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentIntegrationConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentIntegrationConfigurationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentIntegrationConfigurationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdyenPaymentMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection GetMultiAdyenPaymentMethodCollection(bool forceFetch)
		{
			return GetMultiAdyenPaymentMethodCollection(forceFetch, _adyenPaymentMethodCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdyenPaymentMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection GetMultiAdyenPaymentMethodCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdyenPaymentMethodCollection(forceFetch, _adyenPaymentMethodCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection GetMultiAdyenPaymentMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdyenPaymentMethodCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection GetMultiAdyenPaymentMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdyenPaymentMethodCollection || forceFetch || _alwaysFetchAdyenPaymentMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_adyenPaymentMethodCollection);
				_adyenPaymentMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_adyenPaymentMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_adyenPaymentMethodCollection.GetMultiManyToOne(this, filter);
				_adyenPaymentMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdyenPaymentMethodCollection = true;
			}
			return _adyenPaymentMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdyenPaymentMethodCollection'. These settings will be taken into account
		/// when the property AdyenPaymentMethodCollection is requested or GetMultiAdyenPaymentMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdyenPaymentMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_adyenPaymentMethodCollection.SortClauses=sortClauses;
			_adyenPaymentMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCheckoutMethodCollection || forceFetch || _alwaysFetchCheckoutMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_checkoutMethodCollection);
				_checkoutMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_checkoutMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_checkoutMethodCollection.GetMultiManyToOne(null, null, this, null, filter);
				_checkoutMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCheckoutMethodCollection = true;
			}
			return _checkoutMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CheckoutMethodCollection'. These settings will be taken into account
		/// when the property CheckoutMethodCollection is requested or GetMultiCheckoutMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCheckoutMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_checkoutMethodCollection.SortClauses=sortClauses;
			_checkoutMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentTransactionCollection || forceFetch || _alwaysFetchPaymentTransactionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentTransactionCollection);
				_paymentTransactionCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentTransactionCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentTransactionCollection.GetMultiManyToOne(null, this, null, filter);
				_paymentTransactionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentTransactionCollection = true;
			}
			return _paymentTransactionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentTransactionCollection'. These settings will be taken into account
		/// when the property PaymentTransactionCollection is requested or GetMultiPaymentTransactionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentTransactionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentTransactionCollection.SortClauses=sortClauses;
			_paymentTransactionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'PaymentProviderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentProviderEntity' which is related to this entity.</returns>
		public PaymentProviderEntity GetSinglePaymentProviderEntity()
		{
			return GetSinglePaymentProviderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentProviderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentProviderEntity' which is related to this entity.</returns>
		public virtual PaymentProviderEntity GetSinglePaymentProviderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentProviderEntity || forceFetch || _alwaysFetchPaymentProviderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentProviderEntityUsingPaymentProviderId);
				PaymentProviderEntity newEntity = new PaymentProviderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentProviderId);
				}
				if(fetchResult)
				{
					newEntity = (PaymentProviderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentProviderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentProviderEntity = newEntity;
				_alreadyFetchedPaymentProviderEntity = fetchResult;
			}
			return _paymentProviderEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("PaymentProviderEntity", _paymentProviderEntity);
			toReturn.Add("AdyenPaymentMethodCollection", _adyenPaymentMethodCollection);
			toReturn.Add("CheckoutMethodCollection", _checkoutMethodCollection);
			toReturn.Add("PaymentTransactionCollection", _paymentTransactionCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="validator">The validator object for this PaymentIntegrationConfigurationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 paymentIntegrationConfigurationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentIntegrationConfigurationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_adyenPaymentMethodCollection = new Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection();
			_adyenPaymentMethodCollection.SetContainingEntityInfo(this, "PaymentIntegrationConfigurationEntity");

			_checkoutMethodCollection = new Obymobi.Data.CollectionClasses.CheckoutMethodCollection();
			_checkoutMethodCollection.SetContainingEntityInfo(this, "PaymentIntegrationConfigurationEntity");

			_paymentTransactionCollection = new Obymobi.Data.CollectionClasses.PaymentTransactionCollection();
			_paymentTransactionCollection.SetContainingEntityInfo(this, "PaymentIntegrationConfigurationEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_paymentProviderEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntegrationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenMerchantCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePayMerchantIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplePayMerchantIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProcessorInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProcessorInfoFooter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenAccountCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenAccountName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenAccountHolderCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenAccountHolderName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPaymentIntegrationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PaymentIntegrationConfigurationCollection", resetFKFields, new int[] { (int)PaymentIntegrationConfigurationFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPaymentIntegrationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentProviderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentProviderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentProviderEntity, new PropertyChangedEventHandler( OnPaymentProviderEntityPropertyChanged ), "PaymentProviderEntity", Obymobi.Data.RelationClasses.StaticPaymentIntegrationConfigurationRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, true, signalRelatedEntity, "PaymentIntegrationConfigurationCollection", resetFKFields, new int[] { (int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId } );		
			_paymentProviderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentProviderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentProviderEntity(IEntityCore relatedEntity)
		{
			if(_paymentProviderEntity!=relatedEntity)
			{		
				DesetupSyncPaymentProviderEntity(true, true);
				_paymentProviderEntity = (PaymentProviderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentProviderEntity, new PropertyChangedEventHandler( OnPaymentProviderEntityPropertyChanged ), "PaymentProviderEntity", Obymobi.Data.RelationClasses.StaticPaymentIntegrationConfigurationRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, true, ref _alreadyFetchedPaymentProviderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentProviderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentIntegrationConfigurationId">PK value for PaymentIntegrationConfiguration which data should be fetched into this PaymentIntegrationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 paymentIntegrationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId].ForcedCurrentValueWrite(paymentIntegrationConfigurationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentIntegrationConfigurationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentIntegrationConfigurationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentIntegrationConfigurationRelations Relations
		{
			get	{ return new PaymentIntegrationConfigurationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdyenPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdyenPaymentMethodCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("AdyenPaymentMethodCollection")[0], (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, (int)Obymobi.Data.EntityType.AdyenPaymentMethodEntity, 0, null, null, null, "AdyenPaymentMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodCollection")[0], (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, (int)Obymobi.Data.EntityType.CheckoutMethodEntity, 0, null, null, null, "CheckoutMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionCollection")[0], (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, (int)Obymobi.Data.EntityType.PaymentTransactionEntity, 0, null, null, null, "PaymentTransactionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentProvider'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentProviderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentProviderCollection(), (IEntityRelation)GetRelationsForField("PaymentProviderEntity")[0], (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, (int)Obymobi.Data.EntityType.PaymentProviderEntity, 0, null, null, null, "PaymentProviderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PaymentIntegrationConfigurationId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentIntegrationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentIntegrationConfigurationId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentIntegrationConfigurationId, value, true); }
		}

		/// <summary> The PaymentProviderId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProviderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentProviderId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProviderId, value, true); }
		}

		/// <summary> The DisplayName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."DisplayName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DisplayName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.DisplayName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.DisplayName, value, true); }
		}

		/// <summary> The MerchantName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."MerchantName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String MerchantName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.MerchantName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.MerchantName, value, true); }
		}

		/// <summary> The AdyenMerchantCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenMerchantCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenMerchantCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenMerchantCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenMerchantCode, value, true); }
		}

		/// <summary> The GooglePayMerchantIdentifier property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."GooglePayMerchantIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.GooglePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.GooglePayMerchantIdentifier, value, true); }
		}

		/// <summary> The ApplePayMerchantIdentifier property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."ApplePayMerchantIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.ApplePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.ApplePayMerchantIdentifier, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentIntegrationConfigurationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentIntegrationConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CompanyId property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PaymentIntegrationConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The PaymentProcessorInfo property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProcessorInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfo, value, true); }
		}

		/// <summary> The PaymentProcessorInfoFooter property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."PaymentProcessorInfoFooter"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfoFooter
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfoFooter, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.PaymentProcessorInfoFooter, value, true); }
		}

		/// <summary> The AdyenAccountCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountCode, value, true); }
		}

		/// <summary> The AdyenAccountName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountName, value, true); }
		}

		/// <summary> The AdyenAccountHolderCode property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountHolderCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountHolderCode
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderCode, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderCode, value, true); }
		}

		/// <summary> The AdyenAccountHolderName property of the Entity PaymentIntegrationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentIntegrationConfiguration"."AdyenAccountHolderName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenAccountHolderName
		{
			get { return (System.String)GetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderName, true); }
			set	{ SetValue((int)PaymentIntegrationConfigurationFieldIndex.AdyenAccountHolderName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdyenPaymentMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdyenPaymentMethodCollection AdyenPaymentMethodCollection
		{
			get	{ return GetMultiAdyenPaymentMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdyenPaymentMethodCollection. When set to true, AdyenPaymentMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdyenPaymentMethodCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdyenPaymentMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdyenPaymentMethodCollection
		{
			get	{ return _alwaysFetchAdyenPaymentMethodCollection; }
			set	{ _alwaysFetchAdyenPaymentMethodCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdyenPaymentMethodCollection already has been fetched. Setting this property to false when AdyenPaymentMethodCollection has been fetched
		/// will clear the AdyenPaymentMethodCollection collection well. Setting this property to true while AdyenPaymentMethodCollection hasn't been fetched disables lazy loading for AdyenPaymentMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdyenPaymentMethodCollection
		{
			get { return _alreadyFetchedAdyenPaymentMethodCollection;}
			set 
			{
				if(_alreadyFetchedAdyenPaymentMethodCollection && !value && (_adyenPaymentMethodCollection != null))
				{
					_adyenPaymentMethodCollection.Clear();
				}
				_alreadyFetchedAdyenPaymentMethodCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCheckoutMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection CheckoutMethodCollection
		{
			get	{ return GetMultiCheckoutMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodCollection. When set to true, CheckoutMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCheckoutMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodCollection
		{
			get	{ return _alwaysFetchCheckoutMethodCollection; }
			set	{ _alwaysFetchCheckoutMethodCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodCollection already has been fetched. Setting this property to false when CheckoutMethodCollection has been fetched
		/// will clear the CheckoutMethodCollection collection well. Setting this property to true while CheckoutMethodCollection hasn't been fetched disables lazy loading for CheckoutMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodCollection
		{
			get { return _alreadyFetchedCheckoutMethodCollection;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodCollection && !value && (_checkoutMethodCollection != null))
				{
					_checkoutMethodCollection.Clear();
				}
				_alreadyFetchedCheckoutMethodCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentTransactionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection PaymentTransactionCollection
		{
			get	{ return GetMultiPaymentTransactionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionCollection. When set to true, PaymentTransactionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentTransactionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionCollection
		{
			get	{ return _alwaysFetchPaymentTransactionCollection; }
			set	{ _alwaysFetchPaymentTransactionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionCollection already has been fetched. Setting this property to false when PaymentTransactionCollection has been fetched
		/// will clear the PaymentTransactionCollection collection well. Setting this property to true while PaymentTransactionCollection hasn't been fetched disables lazy loading for PaymentTransactionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionCollection
		{
			get { return _alreadyFetchedPaymentTransactionCollection;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionCollection && !value && (_paymentTransactionCollection != null))
				{
					_paymentTransactionCollection.Clear();
				}
				_alreadyFetchedPaymentTransactionCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentIntegrationConfigurationCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentProviderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentProviderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentProviderEntity PaymentProviderEntity
		{
			get	{ return GetSinglePaymentProviderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentProviderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentIntegrationConfigurationCollection", "PaymentProviderEntity", _paymentProviderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentProviderEntity. When set to true, PaymentProviderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentProviderEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentProviderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentProviderEntity
		{
			get	{ return _alwaysFetchPaymentProviderEntity; }
			set	{ _alwaysFetchPaymentProviderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentProviderEntity already has been fetched. Setting this property to false when PaymentProviderEntity has been fetched
		/// will set PaymentProviderEntity to null as well. Setting this property to true while PaymentProviderEntity hasn't been fetched disables lazy loading for PaymentProviderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentProviderEntity
		{
			get { return _alreadyFetchedPaymentProviderEntity;}
			set 
			{
				if(_alreadyFetchedPaymentProviderEntity && !value)
				{
					this.PaymentProviderEntity = null;
				}
				_alreadyFetchedPaymentProviderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentProviderEntity is not found
		/// in the database. When set to true, PaymentProviderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentProviderEntityReturnsNewIfNotFound
		{
			get	{ return _paymentProviderEntityReturnsNewIfNotFound; }
			set { _paymentProviderEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
