﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'DeliverypointPosdeliverypoint'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointPosdeliverypointEntityBase : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations


		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private PosdeliverypointEntity _posdeliverypointEntity;
		private bool	_alwaysFetchPosdeliverypointEntity, _alreadyFetchedPosdeliverypointEntity, _posdeliverypointEntityReturnsNewIfNotFound;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name PosdeliverypointEntity</summary>
			public static readonly string PosdeliverypointEntity = "PosdeliverypointEntity";



		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointPosdeliverypointEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeliverypointPosdeliverypointEntityBase()
		{
			InitClassEmpty(null);
		}

	
		/// <summary>CTor</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		public DeliverypointPosdeliverypointEntityBase(System.Int32 deliverypointPosdeliverypointId)
		{
			InitClassFetch(deliverypointPosdeliverypointId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeliverypointPosdeliverypointEntityBase(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse)
		{
			InitClassFetch(deliverypointPosdeliverypointId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointPosdeliverypointEntity</param>
		public DeliverypointPosdeliverypointEntityBase(System.Int32 deliverypointPosdeliverypointId, IValidator validator)
		{
			InitClassFetch(deliverypointPosdeliverypointId, validator, null);
		}
	

		/// <summary>Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointPosdeliverypointEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{


			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");
			_posdeliverypointEntity = (PosdeliverypointEntity)info.GetValue("_posdeliverypointEntity", typeof(PosdeliverypointEntity));
			if(_posdeliverypointEntity!=null)
			{
				_posdeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posdeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_posdeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchPosdeliverypointEntity = info.GetBoolean("_alwaysFetchPosdeliverypointEntity");
			_alreadyFetchedPosdeliverypointEntity = info.GetBoolean("_alreadyFetchedPosdeliverypointEntity");

			base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointPosdeliverypointFieldIndex)fieldIndex)
			{
				case DeliverypointPosdeliverypointFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				case DeliverypointPosdeliverypointFieldIndex.PosdeliverypointId:
					DesetupSyncPosdeliverypointEntity(true, false);
					_alreadyFetchedPosdeliverypointEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
		
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PostReadXmlFixups()
		{


			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedPosdeliverypointEntity = (_posdeliverypointEntity != null);

		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return DeliverypointPosdeliverypointEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeliverypointEntity":
					toReturn.Add(DeliverypointPosdeliverypointEntity.Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "PosdeliverypointEntity":
					toReturn.Add(DeliverypointPosdeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId);
					break;



				default:

					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.
		/// Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{


			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_posdeliverypointEntity", (!this.MarkedForDeletion?_posdeliverypointEntity:null));
			info.AddValue("_posdeliverypointEntityReturnsNewIfNotFound", _posdeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosdeliverypointEntity", _alwaysFetchPosdeliverypointEntity);
			info.AddValue("_alreadyFetchedPosdeliverypointEntity", _alreadyFetchedPosdeliverypointEntity);

			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity entity)
		{
			switch(propertyName)
			{
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "PosdeliverypointEntity":
					_alreadyFetchedPosdeliverypointEntity = true;
					this.PosdeliverypointEntity = (PosdeliverypointEntity)entity;
					break;



				default:

					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "PosdeliverypointEntity":
					SetupSyncPosdeliverypointEntity(relatedEntity);
					break;


				default:

					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "PosdeliverypointEntity":
					DesetupSyncPosdeliverypointEntity(false, true);
					break;


				default:

					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These
		/// entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();


			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_posdeliverypointEntity!=null)
			{
				toReturn.Add(_posdeliverypointEntity);
			}


			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. The contents of the ArrayList is
		/// used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		public override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		

		

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointPosdeliverypointId)
		{
			return FetchUsingPK(deliverypointPosdeliverypointId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointPosdeliverypointId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return Fetch(deliverypointPosdeliverypointId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointPosdeliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. 
		/// Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointPosdeliverypointId, null, null, null);
		}

		/// <summary> Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(DeliverypointPosdeliverypointFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(DeliverypointPosdeliverypointFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointPosdeliverypointRelations().GetAllRelations();
		}




		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(DeliverypointPosdeliverypointEntity.Relations.DeliverypointEntityUsingDeliverypointId);

				DeliverypointEntity newEntity = new DeliverypointEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (DeliverypointEntity)base.ActiveContext.Get(newEntity);
					}
					this.DeliverypointEntity = newEntity;
				}
				else
				{
					if(_deliverypointEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_deliverypointEntity == null)))
						{
							this.DeliverypointEntity = newEntity;
						}
					}
					else
					{
						this.DeliverypointEntity = null;
					}
				}
				_alreadyFetchedDeliverypointEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _deliverypointEntity;
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosdeliverypointEntity' which is related to this entity.</returns>
		public PosdeliverypointEntity GetSinglePosdeliverypointEntity()
		{
			return GetSinglePosdeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosdeliverypointEntity' which is related to this entity.</returns>
		public virtual PosdeliverypointEntity GetSinglePosdeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosdeliverypointEntity || forceFetch || _alwaysFetchPosdeliverypointEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(DeliverypointPosdeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId);

				PosdeliverypointEntity newEntity = new PosdeliverypointEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.PosdeliverypointId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (PosdeliverypointEntity)base.ActiveContext.Get(newEntity);
					}
					this.PosdeliverypointEntity = newEntity;
				}
				else
				{
					if(_posdeliverypointEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_posdeliverypointEntity == null)))
						{
							this.PosdeliverypointEntity = newEntity;
						}
					}
					else
					{
						this.PosdeliverypointEntity = null;
					}
				}
				_alreadyFetchedPosdeliverypointEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _posdeliverypointEntity;
		}


		/// <summary> Performs the insert action of a new Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool InsertEntity()
		{
			DeliverypointPosdeliverypointDAO dao = (DeliverypointPosdeliverypointDAO)CreateDAOInstance();
			return dao.AddNew(base.Fields, base.Transaction);
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{


			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.ActiveContext = base.ActiveContext;
			}
			if(_posdeliverypointEntity!=null)
			{
				_posdeliverypointEntity.ActiveContext = base.ActiveContext;
			}


		}


		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity()
		{
			DeliverypointPosdeliverypointDAO dao = (DeliverypointPosdeliverypointDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction);
		}
		
		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <param name="updateRestriction">Predicate expression, meant for concurrency checks in an Update query</param>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity(IPredicate updateRestriction)
		{
			DeliverypointPosdeliverypointDAO dao = (DeliverypointPosdeliverypointDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction, updateRestriction);
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		protected virtual void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			base.Fields = CreateFields();
			base.IsNew=true;
			base.Validator = validatorToUse;

			InitClassMembers();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.DeliverypointPosdeliverypointEntity);
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("PosdeliverypointEntity", _posdeliverypointEntity);



			return toReturn;
		}
		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="validator">The validator object for this DeliverypointPosdeliverypointEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected virtual void InitClassFetch(System.Int32 deliverypointPosdeliverypointId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			base.Validator = validator;
			InitClassMembers();
			base.Fields = CreateFields();
			bool wasSuccesful = Fetch(deliverypointPosdeliverypointId, prefetchPathToUse, null, null);
			base.IsNew = !wasSuccesful;

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{


			_deliverypointEntity = null;
			_deliverypointEntityReturnsNewIfNotFound = true;
			_alwaysFetchDeliverypointEntity = false;
			_alreadyFetchedDeliverypointEntity = false;
			_posdeliverypointEntity = null;
			_posdeliverypointEntityReturnsNewIfNotFound = true;
			_alwaysFetchPosdeliverypointEntity = false;
			_alreadyFetchedPosdeliverypointEntity = false;


			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("DeliverypointPosdeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("PosdeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion


		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", DeliverypointPosdeliverypointEntity.Relations.DeliverypointEntityUsingDeliverypointId, true, signalRelatedEntity, "DeliverypointPosdeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointPosdeliverypointFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntity relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", DeliverypointPosdeliverypointEntity.Relations.DeliverypointEntityUsingDeliverypointId, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posdeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosdeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _posdeliverypointEntity, new PropertyChangedEventHandler( OnPosdeliverypointEntityPropertyChanged ), "PosdeliverypointEntity", DeliverypointPosdeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId, true, signalRelatedEntity, "DeliverypointPosdeliverypointCollection", resetFKFields, new int[] { (int)DeliverypointPosdeliverypointFieldIndex.PosdeliverypointId } );		
			_posdeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posdeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosdeliverypointEntity(IEntity relatedEntity)
		{
			if(_posdeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncPosdeliverypointEntity(true, true);
				_posdeliverypointEntity = (PosdeliverypointEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _posdeliverypointEntity, new PropertyChangedEventHandler( OnPosdeliverypointEntityPropertyChanged ), "PosdeliverypointEntity", DeliverypointPosdeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId, true, ref _alreadyFetchedPosdeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosdeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				IDao dao = this.CreateDAOInstance();
				base.Fields[(int)DeliverypointPosdeliverypointFieldIndex.DeliverypointPosdeliverypointId].ForcedCurrentValueWrite(deliverypointPosdeliverypointId);
				dao.FetchExisting(this, base.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (base.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointPosdeliverypointDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointPosdeliverypointEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointPosdeliverypointRelations Relations
		{
			get	{ return new DeliverypointPosdeliverypointRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}




		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(),
					(IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointPosdeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypoint' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointCollection(),
					(IEntityRelation)GetRelationsForField("PosdeliverypointEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointPosdeliverypointEntity, (int)Obymobi.Data.EntityType.PosdeliverypointEntity, 0, null, null, null, "PosdeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary>Returns the full name for this entity, which is important for the DAO to find back persistence info for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override string LLBLGenProEntityName
		{
			get { return "DeliverypointPosdeliverypointEntity";}
		}

		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return DeliverypointPosdeliverypointEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return DeliverypointPosdeliverypointEntity.FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointPosdeliverypointId property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."DeliverypointPosdeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointPosdeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointPosdeliverypointFieldIndex.DeliverypointPosdeliverypointId, true); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.DeliverypointPosdeliverypointId, value, true); }
		}
		/// <summary> The DeliverypointId property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointPosdeliverypointFieldIndex.DeliverypointId, true); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.DeliverypointId, value, true); }
		}
		/// <summary> The PosdeliverypointId property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."PosdeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PosdeliverypointId
		{
			get { return (System.Int32)GetValue((int)DeliverypointPosdeliverypointFieldIndex.PosdeliverypointId, true); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.PosdeliverypointId, value, true); }
		}
		/// <summary> The Created property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."Created"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointPosdeliverypointFieldIndex.Created, false); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.Created, value, true); }
		}
		/// <summary> The CreatedBy property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointPosdeliverypointFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.CreatedBy, value, true); }
		}
		/// <summary> The Updated property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointPosdeliverypointFieldIndex.Updated, false); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.Updated, value, true); }
		}
		/// <summary> The UpdatedBy property of the Entity DeliverypointPosdeliverypoint<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointPosdeliverypoint"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointPosdeliverypointFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointPosdeliverypointFieldIndex.UpdatedBy, value, true); }
		}



		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_deliverypointEntity != null)
						{
							_deliverypointEntity.UnsetRelatedEntity(this, "DeliverypointPosdeliverypointCollection");
						}
					}
					else
					{
						if(_deliverypointEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointPosdeliverypointCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute
		/// a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PosdeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosdeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PosdeliverypointEntity PosdeliverypointEntity
		{
			get	{ return GetSinglePosdeliverypointEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncPosdeliverypointEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_posdeliverypointEntity != null)
						{
							_posdeliverypointEntity.UnsetRelatedEntity(this, "DeliverypointPosdeliverypointCollection");
						}
					}
					else
					{
						if(_posdeliverypointEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointPosdeliverypointCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointEntity. When set to true, PosdeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointEntity is accessed. You can always execute
		/// a forced fetch by calling GetSinglePosdeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointEntity
		{
			get	{ return _alwaysFetchPosdeliverypointEntity; }
			set	{ _alwaysFetchPosdeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointEntity already has been fetched. Setting this property to false when PosdeliverypointEntity has been fetched
		/// will set PosdeliverypointEntity to null as well. Setting this property to true while PosdeliverypointEntity hasn't been fetched disables lazy loading for PosdeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointEntity
		{
			get { return _alreadyFetchedPosdeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointEntity && !value)
				{
					this.PosdeliverypointEntity = null;
				}
				_alreadyFetchedPosdeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosdeliverypointEntity is not found
		/// in the database. When set to true, PosdeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosdeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _posdeliverypointEntityReturnsNewIfNotFound; }
			set { _posdeliverypointEntityReturnsNewIfNotFound = value; }	
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointPosdeliverypointEntity; }
		}
		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
