﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ServiceMethod'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ServiceMethodEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ServiceMethodEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliveryDistanceCollection	_deliveryDistanceCollection;
		private bool	_alwaysFetchDeliveryDistanceCollection, _alreadyFetchedDeliveryDistanceCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection	_serviceMethodDeliverypointgroupCollection;
		private bool	_alwaysFetchServiceMethodDeliverypointgroupCollection, _alreadyFetchedServiceMethodDeliverypointgroupCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private OutletEntity _outletEntity;
		private bool	_alwaysFetchOutletEntity, _alreadyFetchedOutletEntity, _outletEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name OutletEntity</summary>
			public static readonly string OutletEntity = "OutletEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliveryDistanceCollection</summary>
			public static readonly string DeliveryDistanceCollection = "DeliveryDistanceCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name ServiceMethodDeliverypointgroupCollection</summary>
			public static readonly string ServiceMethodDeliverypointgroupCollection = "ServiceMethodDeliverypointgroupCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ServiceMethodEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ServiceMethodEntityBase() :base("ServiceMethodEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		protected ServiceMethodEntityBase(System.Int32 serviceMethodId):base("ServiceMethodEntity")
		{
			InitClassFetch(serviceMethodId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ServiceMethodEntityBase(System.Int32 serviceMethodId, IPrefetchPath prefetchPathToUse): base("ServiceMethodEntity")
		{
			InitClassFetch(serviceMethodId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="validator">The custom validator object for this ServiceMethodEntity</param>
		protected ServiceMethodEntityBase(System.Int32 serviceMethodId, IValidator validator):base("ServiceMethodEntity")
		{
			InitClassFetch(serviceMethodId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServiceMethodEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliveryDistanceCollection = (Obymobi.Data.CollectionClasses.DeliveryDistanceCollection)info.GetValue("_deliveryDistanceCollection", typeof(Obymobi.Data.CollectionClasses.DeliveryDistanceCollection));
			_alwaysFetchDeliveryDistanceCollection = info.GetBoolean("_alwaysFetchDeliveryDistanceCollection");
			_alreadyFetchedDeliveryDistanceCollection = info.GetBoolean("_alreadyFetchedDeliveryDistanceCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_serviceMethodDeliverypointgroupCollection = (Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection)info.GetValue("_serviceMethodDeliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection));
			_alwaysFetchServiceMethodDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchServiceMethodDeliverypointgroupCollection");
			_alreadyFetchedServiceMethodDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedServiceMethodDeliverypointgroupCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_outletEntity = (OutletEntity)info.GetValue("_outletEntity", typeof(OutletEntity));
			if(_outletEntity!=null)
			{
				_outletEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletEntityReturnsNewIfNotFound = info.GetBoolean("_outletEntityReturnsNewIfNotFound");
			_alwaysFetchOutletEntity = info.GetBoolean("_alwaysFetchOutletEntity");
			_alreadyFetchedOutletEntity = info.GetBoolean("_alreadyFetchedOutletEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ServiceMethodFieldIndex)fieldIndex)
			{
				case ServiceMethodFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ServiceMethodFieldIndex.OutletId:
					DesetupSyncOutletEntity(true, false);
					_alreadyFetchedOutletEntity = false;
					break;
				case ServiceMethodFieldIndex.ServiceChargeProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliveryDistanceCollection = (_deliveryDistanceCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedServiceMethodDeliverypointgroupCollection = (_serviceMethodDeliverypointgroupCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedOutletEntity = (_outletEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "OutletEntity":
					toReturn.Add(Relations.OutletEntityUsingOutletId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingServiceChargeProductId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingServiceMethodId);
					break;
				case "DeliveryDistanceCollection":
					toReturn.Add(Relations.DeliveryDistanceEntityUsingServiceMethodId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingServiceMethodId);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					toReturn.Add(Relations.ServiceMethodDeliverypointgroupEntityUsingServiceMethodId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliveryDistanceCollection", (!this.MarkedForDeletion?_deliveryDistanceCollection:null));
			info.AddValue("_alwaysFetchDeliveryDistanceCollection", _alwaysFetchDeliveryDistanceCollection);
			info.AddValue("_alreadyFetchedDeliveryDistanceCollection", _alreadyFetchedDeliveryDistanceCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_serviceMethodDeliverypointgroupCollection", (!this.MarkedForDeletion?_serviceMethodDeliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchServiceMethodDeliverypointgroupCollection", _alwaysFetchServiceMethodDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedServiceMethodDeliverypointgroupCollection", _alreadyFetchedServiceMethodDeliverypointgroupCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_outletEntity", (!this.MarkedForDeletion?_outletEntity:null));
			info.AddValue("_outletEntityReturnsNewIfNotFound", _outletEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletEntity", _alwaysFetchOutletEntity);
			info.AddValue("_alreadyFetchedOutletEntity", _alreadyFetchedOutletEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "OutletEntity":
					_alreadyFetchedOutletEntity = true;
					this.OutletEntity = (OutletEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliveryDistanceCollection":
					_alreadyFetchedDeliveryDistanceCollection = true;
					if(entity!=null)
					{
						this.DeliveryDistanceCollection.Add((DeliveryDistanceEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					_alreadyFetchedServiceMethodDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.ServiceMethodDeliverypointgroupCollection.Add((ServiceMethodDeliverypointgroupEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "OutletEntity":
					SetupSyncOutletEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliveryDistanceCollection":
					_deliveryDistanceCollection.Add((DeliveryDistanceEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					_serviceMethodDeliverypointgroupCollection.Add((ServiceMethodDeliverypointgroupEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "OutletEntity":
					DesetupSyncOutletEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliveryDistanceCollection":
					this.PerformRelatedEntityRemoval(_deliveryDistanceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_serviceMethodDeliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_outletEntity!=null)
			{
				toReturn.Add(_outletEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliveryDistanceCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_serviceMethodDeliverypointgroupCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 serviceMethodId)
		{
			return FetchUsingPK(serviceMethodId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 serviceMethodId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(serviceMethodId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 serviceMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(serviceMethodId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 serviceMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(serviceMethodId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ServiceMethodId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ServiceMethodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliveryDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliveryDistanceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliveryDistanceCollection GetMultiDeliveryDistanceCollection(bool forceFetch)
		{
			return GetMultiDeliveryDistanceCollection(forceFetch, _deliveryDistanceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliveryDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliveryDistanceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliveryDistanceCollection GetMultiDeliveryDistanceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliveryDistanceCollection(forceFetch, _deliveryDistanceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliveryDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliveryDistanceCollection GetMultiDeliveryDistanceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliveryDistanceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliveryDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliveryDistanceCollection GetMultiDeliveryDistanceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliveryDistanceCollection || forceFetch || _alwaysFetchDeliveryDistanceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliveryDistanceCollection);
				_deliveryDistanceCollection.SuppressClearInGetMulti=!forceFetch;
				_deliveryDistanceCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliveryDistanceCollection.GetMultiManyToOne(this, filter);
				_deliveryDistanceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliveryDistanceCollection = true;
			}
			return _deliveryDistanceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliveryDistanceCollection'. These settings will be taken into account
		/// when the property DeliveryDistanceCollection is requested or GetMultiDeliveryDistanceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliveryDistanceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliveryDistanceCollection.SortClauses=sortClauses;
			_deliveryDistanceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, _serviceMethodDeliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, _serviceMethodDeliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedServiceMethodDeliverypointgroupCollection || forceFetch || _alwaysFetchServiceMethodDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_serviceMethodDeliverypointgroupCollection);
				_serviceMethodDeliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_serviceMethodDeliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_serviceMethodDeliverypointgroupCollection.GetMultiManyToOne(null, null, this, filter);
				_serviceMethodDeliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedServiceMethodDeliverypointgroupCollection = true;
			}
			return _serviceMethodDeliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ServiceMethodDeliverypointgroupCollection'. These settings will be taken into account
		/// when the property ServiceMethodDeliverypointgroupCollection is requested or GetMultiServiceMethodDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersServiceMethodDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_serviceMethodDeliverypointgroupCollection.SortClauses=sortClauses;
			_serviceMethodDeliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public OutletEntity GetSingleOutletEntity()
		{
			return GetSingleOutletEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public virtual OutletEntity GetSingleOutletEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletEntity || forceFetch || _alwaysFetchOutletEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletEntityUsingOutletId);
				OutletEntity newEntity = new OutletEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletId);
				}
				if(fetchResult)
				{
					newEntity = (OutletEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletEntity = newEntity;
				_alreadyFetchedOutletEntity = fetchResult;
			}
			return _outletEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingServiceChargeProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceChargeProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("OutletEntity", _outletEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliveryDistanceCollection", _deliveryDistanceCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("ServiceMethodDeliverypointgroupCollection", _serviceMethodDeliverypointgroupCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="validator">The validator object for this ServiceMethodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 serviceMethodId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(serviceMethodId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "ServiceMethodEntity");

			_deliveryDistanceCollection = new Obymobi.Data.CollectionClasses.DeliveryDistanceCollection();
			_deliveryDistanceCollection.SetContainingEntityInfo(this, "ServiceMethodEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "ServiceMethodEntity");

			_serviceMethodDeliverypointgroupCollection = new Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection();
			_serviceMethodDeliverypointgroupCollection.SetContainingEntityInfo(this, "ServiceMethodEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_outletEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Label", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AskEntryPermission", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceChargePercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceChargeAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceChargeProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfirmationActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumOrderAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FreeDeliveryAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceChargeType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumAmountForFreeDelivery", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumAmountForFreeServiceCharge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompleteTextMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompleteMailSubject", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompleteMailMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompleteNotificationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxCovers", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoversType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ServiceMethodCollection", resetFKFields, new int[] { (int)ServiceMethodFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.OutletEntityUsingOutletIdStatic, true, signalRelatedEntity, "ServiceMethodCollection", resetFKFields, new int[] { (int)ServiceMethodFieldIndex.OutletId } );		
			_outletEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletEntity(IEntityCore relatedEntity)
		{
			if(_outletEntity!=relatedEntity)
			{		
				DesetupSyncOutletEntity(true, true);
				_outletEntity = (OutletEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.OutletEntityUsingOutletIdStatic, true, ref _alreadyFetchedOutletEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.ProductEntityUsingServiceChargeProductIdStatic, true, signalRelatedEntity, "ServiceMethodCollection", resetFKFields, new int[] { (int)ServiceMethodFieldIndex.ServiceChargeProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticServiceMethodRelations.ProductEntityUsingServiceChargeProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="serviceMethodId">PK value for ServiceMethod which data should be fetched into this ServiceMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 serviceMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ServiceMethodFieldIndex.ServiceMethodId].ForcedCurrentValueWrite(serviceMethodId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateServiceMethodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ServiceMethodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ServiceMethodRelations Relations
		{
			get	{ return new ServiceMethodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliveryDistance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliveryDistanceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliveryDistanceCollection(), (IEntityRelation)GetRelationsForField("DeliveryDistanceCollection")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.DeliveryDistanceEntity, 0, null, null, null, "DeliveryDistanceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodDeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity, 0, null, null, null, "ServiceMethodDeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletEntity")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.ServiceMethodEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ServiceMethodId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ServiceMethodId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.ServiceMethodId, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceMethodId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The OutletId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OutletId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutletId
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.OutletId, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OutletId, value, true); }
		}

		/// <summary> The Active property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)ServiceMethodFieldIndex.Active, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Active, value, true); }
		}

		/// <summary> The Type property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ServiceMethodType Type
		{
			get { return (Obymobi.Enums.ServiceMethodType)GetValue((int)ServiceMethodFieldIndex.Type, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Type, value, true); }
		}

		/// <summary> The Name property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Name, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Name, value, true); }
		}

		/// <summary> The Label property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Label"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Label, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Label, value, true); }
		}

		/// <summary> The Description property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.Description, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.Description, value, true); }
		}

		/// <summary> The AskEntryPermission property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."AskEntryPermission"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AskEntryPermission
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ServiceMethodFieldIndex.AskEntryPermission, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.AskEntryPermission, value, true); }
		}

		/// <summary> The ServiceChargePercentage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargePercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> ServiceChargePercentage
		{
			get { return (Nullable<System.Double>)GetValue((int)ServiceMethodFieldIndex.ServiceChargePercentage, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargePercentage, value, true); }
		}

		/// <summary> The ServiceChargeAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ServiceChargeAmount
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ServiceMethodFieldIndex.ServiceChargeAmount, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeAmount, value, true); }
		}

		/// <summary> The ServiceChargeProductId property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceChargeProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServiceMethodFieldIndex.ServiceChargeProductId, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeProductId, value, true); }
		}

		/// <summary> The ConfirmationActive property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ConfirmationActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ConfirmationActive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ServiceMethodFieldIndex.ConfirmationActive, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ConfirmationActive, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServiceMethodFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ServiceMethodFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The MinimumOrderAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumOrderAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumOrderAmount
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumOrderAmount, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumOrderAmount, value, true); }
		}

		/// <summary> The FreeDeliveryAmount property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."FreeDeliveryAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal FreeDeliveryAmount
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.FreeDeliveryAmount, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.FreeDeliveryAmount, value, true); }
		}

		/// <summary> The ServiceChargeType property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."ServiceChargeType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ChargeType ServiceChargeType
		{
			get { return (Obymobi.Enums.ChargeType)GetValue((int)ServiceMethodFieldIndex.ServiceChargeType, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.ServiceChargeType, value, true); }
		}

		/// <summary> The MinimumAmountForFreeDelivery property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumAmountForFreeDelivery"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumAmountForFreeDelivery
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeDelivery, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeDelivery, value, true); }
		}

		/// <summary> The MinimumAmountForFreeServiceCharge property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MinimumAmountForFreeServiceCharge"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal MinimumAmountForFreeServiceCharge
		{
			get { return (System.Decimal)GetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeServiceCharge, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MinimumAmountForFreeServiceCharge, value, true); }
		}

		/// <summary> The OrderCompleteTextMessage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteTextMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 320<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteTextMessage
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteTextMessage, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteTextMessage, value, true); }
		}

		/// <summary> The OrderCompleteMailSubject property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteMailSubject"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteMailSubject
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteMailSubject, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteMailSubject, value, true); }
		}

		/// <summary> The OrderCompleteMailMessage property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteMailMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2048<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompleteMailMessage
		{
			get { return (System.String)GetValue((int)ServiceMethodFieldIndex.OrderCompleteMailMessage, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteMailMessage, value, true); }
		}

		/// <summary> The OrderCompleteNotificationMethod property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."OrderCompleteNotificationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderNotificationMethod OrderCompleteNotificationMethod
		{
			get { return (Obymobi.Enums.OrderNotificationMethod)GetValue((int)ServiceMethodFieldIndex.OrderCompleteNotificationMethod, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.OrderCompleteNotificationMethod, value, true); }
		}

		/// <summary> The MaxCovers property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."MaxCovers"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxCovers
		{
			get { return (System.Int32)GetValue((int)ServiceMethodFieldIndex.MaxCovers, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.MaxCovers, value, true); }
		}

		/// <summary> The CoversType property of the Entity ServiceMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ServiceMethod"."CoversType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.CoversType CoversType
		{
			get { return (Obymobi.Enums.CoversType)GetValue((int)ServiceMethodFieldIndex.CoversType, true); }
			set	{ SetValue((int)ServiceMethodFieldIndex.CoversType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliveryDistanceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliveryDistanceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliveryDistanceCollection DeliveryDistanceCollection
		{
			get	{ return GetMultiDeliveryDistanceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliveryDistanceCollection. When set to true, DeliveryDistanceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliveryDistanceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliveryDistanceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliveryDistanceCollection
		{
			get	{ return _alwaysFetchDeliveryDistanceCollection; }
			set	{ _alwaysFetchDeliveryDistanceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliveryDistanceCollection already has been fetched. Setting this property to false when DeliveryDistanceCollection has been fetched
		/// will clear the DeliveryDistanceCollection collection well. Setting this property to true while DeliveryDistanceCollection hasn't been fetched disables lazy loading for DeliveryDistanceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliveryDistanceCollection
		{
			get { return _alreadyFetchedDeliveryDistanceCollection;}
			set 
			{
				if(_alreadyFetchedDeliveryDistanceCollection && !value && (_deliveryDistanceCollection != null))
				{
					_deliveryDistanceCollection.Clear();
				}
				_alreadyFetchedDeliveryDistanceCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiServiceMethodDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection ServiceMethodDeliverypointgroupCollection
		{
			get	{ return GetMultiServiceMethodDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodDeliverypointgroupCollection. When set to true, ServiceMethodDeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodDeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiServiceMethodDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchServiceMethodDeliverypointgroupCollection; }
			set	{ _alwaysFetchServiceMethodDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodDeliverypointgroupCollection already has been fetched. Setting this property to false when ServiceMethodDeliverypointgroupCollection has been fetched
		/// will clear the ServiceMethodDeliverypointgroupCollection collection well. Setting this property to true while ServiceMethodDeliverypointgroupCollection hasn't been fetched disables lazy loading for ServiceMethodDeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodDeliverypointgroupCollection
		{
			get { return _alreadyFetchedServiceMethodDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedServiceMethodDeliverypointgroupCollection && !value && (_serviceMethodDeliverypointgroupCollection != null))
				{
					_serviceMethodDeliverypointgroupCollection.Clear();
				}
				_alreadyFetchedServiceMethodDeliverypointgroupCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ServiceMethodCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletEntity OutletEntity
		{
			get	{ return GetSingleOutletEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ServiceMethodCollection", "OutletEntity", _outletEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletEntity. When set to true, OutletEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletEntity
		{
			get	{ return _alwaysFetchOutletEntity; }
			set	{ _alwaysFetchOutletEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletEntity already has been fetched. Setting this property to false when OutletEntity has been fetched
		/// will set OutletEntity to null as well. Setting this property to true while OutletEntity hasn't been fetched disables lazy loading for OutletEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletEntity
		{
			get { return _alreadyFetchedOutletEntity;}
			set 
			{
				if(_alreadyFetchedOutletEntity && !value)
				{
					this.OutletEntity = null;
				}
				_alreadyFetchedOutletEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletEntity is not found
		/// in the database. When set to true, OutletEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletEntityReturnsNewIfNotFound
		{
			get	{ return _outletEntityReturnsNewIfNotFound; }
			set { _outletEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ServiceMethodCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ServiceMethodEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
