﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Alterationitem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AlterationitemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AlterationitemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection	_alterationitemAlterationCollection;
		private bool	_alwaysFetchAlterationitemAlterationCollection, _alreadyFetchedAlterationitemAlterationCollection;
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection	_orderitemAlterationitemCollection;
		private bool	_alwaysFetchOrderitemAlterationitemCollection, _alreadyFetchedOrderitemAlterationitemCollection;
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationoptionEntity _alterationoptionEntity;
		private bool	_alwaysFetchAlterationoptionEntity, _alreadyFetchedAlterationoptionEntity, _alterationoptionEntityReturnsNewIfNotFound;
		private PosalterationitemEntity _posalterationitemEntity;
		private bool	_alwaysFetchPosalterationitemEntity, _alreadyFetchedPosalterationitemEntity, _posalterationitemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationoptionEntity</summary>
			public static readonly string AlterationoptionEntity = "AlterationoptionEntity";
			/// <summary>Member name PosalterationitemEntity</summary>
			public static readonly string PosalterationitemEntity = "PosalterationitemEntity";
			/// <summary>Member name AlterationitemAlterationCollection</summary>
			public static readonly string AlterationitemAlterationCollection = "AlterationitemAlterationCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AlterationitemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AlterationitemEntityBase() :base("AlterationitemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		protected AlterationitemEntityBase(System.Int32 alterationitemId):base("AlterationitemEntity")
		{
			InitClassFetch(alterationitemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AlterationitemEntityBase(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse): base("AlterationitemEntity")
		{
			InitClassFetch(alterationitemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="validator">The custom validator object for this AlterationitemEntity</param>
		protected AlterationitemEntityBase(System.Int32 alterationitemId, IValidator validator):base("AlterationitemEntity")
		{
			InitClassFetch(alterationitemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationitemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationitemAlterationCollection = (Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection)info.GetValue("_alterationitemAlterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection));
			_alwaysFetchAlterationitemAlterationCollection = info.GetBoolean("_alwaysFetchAlterationitemAlterationCollection");
			_alreadyFetchedAlterationitemAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationitemAlterationCollection");

			_orderitemAlterationitemCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection)info.GetValue("_orderitemAlterationitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection));
			_alwaysFetchOrderitemAlterationitemCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemCollection");
			_alreadyFetchedOrderitemAlterationitemCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemCollection");
			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationoptionEntity = (AlterationoptionEntity)info.GetValue("_alterationoptionEntity", typeof(AlterationoptionEntity));
			if(_alterationoptionEntity!=null)
			{
				_alterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_alterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationoptionEntity = info.GetBoolean("_alwaysFetchAlterationoptionEntity");
			_alreadyFetchedAlterationoptionEntity = info.GetBoolean("_alreadyFetchedAlterationoptionEntity");

			_posalterationitemEntity = (PosalterationitemEntity)info.GetValue("_posalterationitemEntity", typeof(PosalterationitemEntity));
			if(_posalterationitemEntity!=null)
			{
				_posalterationitemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posalterationitemEntityReturnsNewIfNotFound = info.GetBoolean("_posalterationitemEntityReturnsNewIfNotFound");
			_alwaysFetchPosalterationitemEntity = info.GetBoolean("_alwaysFetchPosalterationitemEntity");
			_alreadyFetchedPosalterationitemEntity = info.GetBoolean("_alreadyFetchedPosalterationitemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AlterationitemFieldIndex)fieldIndex)
			{
				case AlterationitemFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case AlterationitemFieldIndex.AlterationoptionId:
					DesetupSyncAlterationoptionEntity(true, false);
					_alreadyFetchedAlterationoptionEntity = false;
					break;
				case AlterationitemFieldIndex.PosalterationitemId:
					DesetupSyncPosalterationitemEntity(true, false);
					_alreadyFetchedPosalterationitemEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationitemAlterationCollection = (_alterationitemAlterationCollection.Count > 0);
			_alreadyFetchedOrderitemAlterationitemCollection = (_orderitemAlterationitemCollection.Count > 0);
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationoptionEntity = (_alterationoptionEntity != null);
			_alreadyFetchedPosalterationitemEntity = (_posalterationitemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationoptionEntity":
					toReturn.Add(Relations.AlterationoptionEntityUsingAlterationoptionId);
					break;
				case "PosalterationitemEntity":
					toReturn.Add(Relations.PosalterationitemEntityUsingPosalterationitemId);
					break;
				case "AlterationitemAlterationCollection":
					toReturn.Add(Relations.AlterationitemAlterationEntityUsingAlterationitemId);
					break;
				case "OrderitemAlterationitemCollection":
					toReturn.Add(Relations.OrderitemAlterationitemEntityUsingAlterationitemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationitemAlterationCollection", (!this.MarkedForDeletion?_alterationitemAlterationCollection:null));
			info.AddValue("_alwaysFetchAlterationitemAlterationCollection", _alwaysFetchAlterationitemAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationitemAlterationCollection", _alreadyFetchedAlterationitemAlterationCollection);
			info.AddValue("_orderitemAlterationitemCollection", (!this.MarkedForDeletion?_orderitemAlterationitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemCollection", _alwaysFetchOrderitemAlterationitemCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemCollection", _alreadyFetchedOrderitemAlterationitemCollection);
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationoptionEntity", (!this.MarkedForDeletion?_alterationoptionEntity:null));
			info.AddValue("_alterationoptionEntityReturnsNewIfNotFound", _alterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationoptionEntity", _alwaysFetchAlterationoptionEntity);
			info.AddValue("_alreadyFetchedAlterationoptionEntity", _alreadyFetchedAlterationoptionEntity);
			info.AddValue("_posalterationitemEntity", (!this.MarkedForDeletion?_posalterationitemEntity:null));
			info.AddValue("_posalterationitemEntityReturnsNewIfNotFound", _posalterationitemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosalterationitemEntity", _alwaysFetchPosalterationitemEntity);
			info.AddValue("_alreadyFetchedPosalterationitemEntity", _alreadyFetchedPosalterationitemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationoptionEntity":
					_alreadyFetchedAlterationoptionEntity = true;
					this.AlterationoptionEntity = (AlterationoptionEntity)entity;
					break;
				case "PosalterationitemEntity":
					_alreadyFetchedPosalterationitemEntity = true;
					this.PosalterationitemEntity = (PosalterationitemEntity)entity;
					break;
				case "AlterationitemAlterationCollection":
					_alreadyFetchedAlterationitemAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationitemAlterationCollection.Add((AlterationitemAlterationEntity)entity);
					}
					break;
				case "OrderitemAlterationitemCollection":
					_alreadyFetchedOrderitemAlterationitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationoptionEntity":
					SetupSyncAlterationoptionEntity(relatedEntity);
					break;
				case "PosalterationitemEntity":
					SetupSyncPosalterationitemEntity(relatedEntity);
					break;
				case "AlterationitemAlterationCollection":
					_alterationitemAlterationCollection.Add((AlterationitemAlterationEntity)relatedEntity);
					break;
				case "OrderitemAlterationitemCollection":
					_orderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationoptionEntity":
					DesetupSyncAlterationoptionEntity(false, true);
					break;
				case "PosalterationitemEntity":
					DesetupSyncPosalterationitemEntity(false, true);
					break;
				case "AlterationitemAlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationitemAlterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemAlterationitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationoptionEntity!=null)
			{
				toReturn.Add(_alterationoptionEntity);
			}
			if(_posalterationitemEntity!=null)
			{
				toReturn.Add(_posalterationitemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationitemAlterationCollection);
			toReturn.Add(_orderitemAlterationitemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemId)
		{
			return FetchUsingPK(alterationitemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(alterationitemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(alterationitemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(alterationitemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AlterationitemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AlterationitemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, _alterationitemAlterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, _alterationitemAlterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationitemAlterationCollection || forceFetch || _alwaysFetchAlterationitemAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationitemAlterationCollection);
				_alterationitemAlterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationitemAlterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationitemAlterationCollection.GetMultiManyToOne(null, this, filter);
				_alterationitemAlterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationitemAlterationCollection = true;
			}
			return _alterationitemAlterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationitemAlterationCollection'. These settings will be taken into account
		/// when the property AlterationitemAlterationCollection is requested or GetMultiAlterationitemAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationitemAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationitemAlterationCollection.SortClauses=sortClauses;
			_alterationitemAlterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemCollection || forceFetch || _alwaysFetchOrderitemAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemCollection);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemCollection.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemCollection = true;
			}
			return _orderitemAlterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemCollection is requested or GetMultiOrderitemAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemCollection.SortClauses=sortClauses;
			_orderitemAlterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public AlterationoptionEntity GetSingleAlterationoptionEntity()
		{
			return GetSingleAlterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public virtual AlterationoptionEntity GetSingleAlterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationoptionEntity || forceFetch || _alwaysFetchAlterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationoptionEntityUsingAlterationoptionId);
				AlterationoptionEntity newEntity = new AlterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationoptionEntity = newEntity;
				_alreadyFetchedAlterationoptionEntity = fetchResult;
			}
			return _alterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosalterationitemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosalterationitemEntity' which is related to this entity.</returns>
		public PosalterationitemEntity GetSinglePosalterationitemEntity()
		{
			return GetSinglePosalterationitemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosalterationitemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosalterationitemEntity' which is related to this entity.</returns>
		public virtual PosalterationitemEntity GetSinglePosalterationitemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosalterationitemEntity || forceFetch || _alwaysFetchPosalterationitemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosalterationitemEntityUsingPosalterationitemId);
				PosalterationitemEntity newEntity = new PosalterationitemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosalterationitemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosalterationitemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posalterationitemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosalterationitemEntity = newEntity;
				_alreadyFetchedPosalterationitemEntity = fetchResult;
			}
			return _posalterationitemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationoptionEntity", _alterationoptionEntity);
			toReturn.Add("PosalterationitemEntity", _posalterationitemEntity);
			toReturn.Add("AlterationitemAlterationCollection", _alterationitemAlterationCollection);
			toReturn.Add("OrderitemAlterationitemCollection", _orderitemAlterationitemCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="validator">The validator object for this AlterationitemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 alterationitemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(alterationitemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationitemAlterationCollection = new Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection();
			_alterationitemAlterationCollection.SetContainingEntityInfo(this, "AlterationitemEntity");

			_orderitemAlterationitemCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection();
			_orderitemAlterationitemCollection.SetContainingEntityInfo(this, "AlterationitemEntity");
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationoptionEntityReturnsNewIfNotFound = true;
			_posalterationitemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SelectedOnDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosalterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "AlterationitemCollection", resetFKFields, new int[] { (int)AlterationitemFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, signalRelatedEntity, "AlterationitemCollection", resetFKFields, new int[] { (int)AlterationitemFieldIndex.AlterationoptionId } );		
			_alterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_alterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncAlterationoptionEntity(true, true);
				_alterationoptionEntity = (AlterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, ref _alreadyFetchedAlterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posalterationitemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosalterationitemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posalterationitemEntity, new PropertyChangedEventHandler( OnPosalterationitemEntityPropertyChanged ), "PosalterationitemEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.PosalterationitemEntityUsingPosalterationitemIdStatic, true, signalRelatedEntity, "AlterationitemCollection", resetFKFields, new int[] { (int)AlterationitemFieldIndex.PosalterationitemId } );		
			_posalterationitemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posalterationitemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosalterationitemEntity(IEntityCore relatedEntity)
		{
			if(_posalterationitemEntity!=relatedEntity)
			{		
				DesetupSyncPosalterationitemEntity(true, true);
				_posalterationitemEntity = (PosalterationitemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posalterationitemEntity, new PropertyChangedEventHandler( OnPosalterationitemEntityPropertyChanged ), "PosalterationitemEntity", Obymobi.Data.RelationClasses.StaticAlterationitemRelations.PosalterationitemEntityUsingPosalterationitemIdStatic, true, ref _alreadyFetchedPosalterationitemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosalterationitemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AlterationitemFieldIndex.AlterationitemId].ForcedCurrentValueWrite(alterationitemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAlterationitemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AlterationitemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AlterationitemRelations Relations
		{
			get	{ return new AlterationitemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationitemAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationitemAlterationCollection")[0], (int)Obymobi.Data.EntityType.AlterationitemEntity, (int)Obymobi.Data.EntityType.AlterationitemAlterationEntity, 0, null, null, null, "AlterationitemAlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemCollection")[0], (int)Obymobi.Data.EntityType.AlterationitemEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, 0, null, null, null, "OrderitemAlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.AlterationitemEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionEntity")[0], (int)Obymobi.Data.EntityType.AlterationitemEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posalterationitem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosalterationitemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosalterationitemCollection(), (IEntityRelation)GetRelationsForField("PosalterationitemEntity")[0], (int)Obymobi.Data.EntityType.AlterationitemEntity, (int)Obymobi.Data.EntityType.PosalterationitemEntity, 0, null, null, null, "PosalterationitemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationitemId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationitemId, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.AlterationitemId, value, true); }
		}

		/// <summary> The AlterationId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The AlterationoptionId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.AlterationoptionId, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The SelectedOnDefault property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."SelectedOnDefault"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SelectedOnDefault
		{
			get { return (System.Boolean)GetValue((int)AlterationitemFieldIndex.SelectedOnDefault, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.SelectedOnDefault, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The PosalterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."PosalterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.PosalterationitemId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.PosalterationitemId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The GenericalterationitemId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."GenericalterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.GenericalterationitemId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.GenericalterationitemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationitemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The Version property of the Entity Alterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alterationitem"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationitemFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationitemFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationitemAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection AlterationitemAlterationCollection
		{
			get	{ return GetMultiAlterationitemAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemAlterationCollection. When set to true, AlterationitemAlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemAlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationitemAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemAlterationCollection
		{
			get	{ return _alwaysFetchAlterationitemAlterationCollection; }
			set	{ _alwaysFetchAlterationitemAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemAlterationCollection already has been fetched. Setting this property to false when AlterationitemAlterationCollection has been fetched
		/// will clear the AlterationitemAlterationCollection collection well. Setting this property to true while AlterationitemAlterationCollection hasn't been fetched disables lazy loading for AlterationitemAlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemAlterationCollection
		{
			get { return _alreadyFetchedAlterationitemAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationitemAlterationCollection && !value && (_alterationitemAlterationCollection != null))
				{
					_alterationitemAlterationCollection.Clear();
				}
				_alreadyFetchedAlterationitemAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection OrderitemAlterationitemCollection
		{
			get	{ return GetMultiOrderitemAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemCollection. When set to true, OrderitemAlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemCollection already has been fetched. Setting this property to false when OrderitemAlterationitemCollection has been fetched
		/// will clear the OrderitemAlterationitemCollection collection well. Setting this property to true while OrderitemAlterationitemCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemCollection && !value && (_orderitemAlterationitemCollection != null))
				{
					_orderitemAlterationitemCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationitemCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationoptionEntity AlterationoptionEntity
		{
			get	{ return GetSingleAlterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationitemCollection", "AlterationoptionEntity", _alterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionEntity. When set to true, AlterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionEntity
		{
			get	{ return _alwaysFetchAlterationoptionEntity; }
			set	{ _alwaysFetchAlterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionEntity already has been fetched. Setting this property to false when AlterationoptionEntity has been fetched
		/// will set AlterationoptionEntity to null as well. Setting this property to true while AlterationoptionEntity hasn't been fetched disables lazy loading for AlterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionEntity
		{
			get { return _alreadyFetchedAlterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedAlterationoptionEntity && !value)
				{
					this.AlterationoptionEntity = null;
				}
				_alreadyFetchedAlterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationoptionEntity is not found
		/// in the database. When set to true, AlterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _alterationoptionEntityReturnsNewIfNotFound; }
			set { _alterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosalterationitemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosalterationitemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosalterationitemEntity PosalterationitemEntity
		{
			get	{ return GetSinglePosalterationitemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosalterationitemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationitemCollection", "PosalterationitemEntity", _posalterationitemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosalterationitemEntity. When set to true, PosalterationitemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosalterationitemEntity is accessed. You can always execute a forced fetch by calling GetSinglePosalterationitemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosalterationitemEntity
		{
			get	{ return _alwaysFetchPosalterationitemEntity; }
			set	{ _alwaysFetchPosalterationitemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosalterationitemEntity already has been fetched. Setting this property to false when PosalterationitemEntity has been fetched
		/// will set PosalterationitemEntity to null as well. Setting this property to true while PosalterationitemEntity hasn't been fetched disables lazy loading for PosalterationitemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosalterationitemEntity
		{
			get { return _alreadyFetchedPosalterationitemEntity;}
			set 
			{
				if(_alreadyFetchedPosalterationitemEntity && !value)
				{
					this.PosalterationitemEntity = null;
				}
				_alreadyFetchedPosalterationitemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosalterationitemEntity is not found
		/// in the database. When set to true, PosalterationitemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosalterationitemEntityReturnsNewIfNotFound
		{
			get	{ return _posalterationitemEntityReturnsNewIfNotFound; }
			set { _posalterationitemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AlterationitemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
