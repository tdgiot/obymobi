﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PageTemplate'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PageTemplateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PageTemplateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AttachmentCollection	_attachmentCollection;
		private bool	_alwaysFetchAttachmentCollection, _alreadyFetchedAttachmentCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.PageCollection	_pageCollection;
		private bool	_alwaysFetchPageCollection, _alreadyFetchedPageCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateCollection	_pageTemplateCollection;
		private bool	_alwaysFetchPageTemplateCollection, _alreadyFetchedPageTemplateCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateElementCollection	_pageTemplateElementCollection;
		private bool	_alwaysFetchPageTemplateElementCollection, _alreadyFetchedPageTemplateElementCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection	_pageTemplateLanguageCollection;
		private bool	_alwaysFetchPageTemplateLanguageCollection, _alreadyFetchedPageTemplateLanguageCollection;
		private PageTemplateEntity _pageTemplateEntity;
		private bool	_alwaysFetchPageTemplateEntity, _alreadyFetchedPageTemplateEntity, _pageTemplateEntityReturnsNewIfNotFound;
		private SiteTemplateEntity _siteTemplateEntity;
		private bool	_alwaysFetchSiteTemplateEntity, _alreadyFetchedSiteTemplateEntity, _siteTemplateEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PageTemplateEntity</summary>
			public static readonly string PageTemplateEntity = "PageTemplateEntity";
			/// <summary>Member name SiteTemplateEntity</summary>
			public static readonly string SiteTemplateEntity = "SiteTemplateEntity";
			/// <summary>Member name AttachmentCollection</summary>
			public static readonly string AttachmentCollection = "AttachmentCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name PageCollection</summary>
			public static readonly string PageCollection = "PageCollection";
			/// <summary>Member name PageTemplateCollection</summary>
			public static readonly string PageTemplateCollection = "PageTemplateCollection";
			/// <summary>Member name PageTemplateElementCollection</summary>
			public static readonly string PageTemplateElementCollection = "PageTemplateElementCollection";
			/// <summary>Member name PageTemplateLanguageCollection</summary>
			public static readonly string PageTemplateLanguageCollection = "PageTemplateLanguageCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PageTemplateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PageTemplateEntityBase() :base("PageTemplateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		protected PageTemplateEntityBase(System.Int32 pageTemplateId):base("PageTemplateEntity")
		{
			InitClassFetch(pageTemplateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PageTemplateEntityBase(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse): base("PageTemplateEntity")
		{
			InitClassFetch(pageTemplateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="validator">The custom validator object for this PageTemplateEntity</param>
		protected PageTemplateEntityBase(System.Int32 pageTemplateId, IValidator validator):base("PageTemplateEntity")
		{
			InitClassFetch(pageTemplateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageTemplateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attachmentCollection = (Obymobi.Data.CollectionClasses.AttachmentCollection)info.GetValue("_attachmentCollection", typeof(Obymobi.Data.CollectionClasses.AttachmentCollection));
			_alwaysFetchAttachmentCollection = info.GetBoolean("_alwaysFetchAttachmentCollection");
			_alreadyFetchedAttachmentCollection = info.GetBoolean("_alreadyFetchedAttachmentCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_pageCollection = (Obymobi.Data.CollectionClasses.PageCollection)info.GetValue("_pageCollection", typeof(Obymobi.Data.CollectionClasses.PageCollection));
			_alwaysFetchPageCollection = info.GetBoolean("_alwaysFetchPageCollection");
			_alreadyFetchedPageCollection = info.GetBoolean("_alreadyFetchedPageCollection");

			_pageTemplateCollection = (Obymobi.Data.CollectionClasses.PageTemplateCollection)info.GetValue("_pageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateCollection));
			_alwaysFetchPageTemplateCollection = info.GetBoolean("_alwaysFetchPageTemplateCollection");
			_alreadyFetchedPageTemplateCollection = info.GetBoolean("_alreadyFetchedPageTemplateCollection");

			_pageTemplateElementCollection = (Obymobi.Data.CollectionClasses.PageTemplateElementCollection)info.GetValue("_pageTemplateElementCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateElementCollection));
			_alwaysFetchPageTemplateElementCollection = info.GetBoolean("_alwaysFetchPageTemplateElementCollection");
			_alreadyFetchedPageTemplateElementCollection = info.GetBoolean("_alreadyFetchedPageTemplateElementCollection");

			_pageTemplateLanguageCollection = (Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection)info.GetValue("_pageTemplateLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection));
			_alwaysFetchPageTemplateLanguageCollection = info.GetBoolean("_alwaysFetchPageTemplateLanguageCollection");
			_alreadyFetchedPageTemplateLanguageCollection = info.GetBoolean("_alreadyFetchedPageTemplateLanguageCollection");
			_pageTemplateEntity = (PageTemplateEntity)info.GetValue("_pageTemplateEntity", typeof(PageTemplateEntity));
			if(_pageTemplateEntity!=null)
			{
				_pageTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_pageTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchPageTemplateEntity = info.GetBoolean("_alwaysFetchPageTemplateEntity");
			_alreadyFetchedPageTemplateEntity = info.GetBoolean("_alreadyFetchedPageTemplateEntity");

			_siteTemplateEntity = (SiteTemplateEntity)info.GetValue("_siteTemplateEntity", typeof(SiteTemplateEntity));
			if(_siteTemplateEntity!=null)
			{
				_siteTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_siteTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchSiteTemplateEntity = info.GetBoolean("_alwaysFetchSiteTemplateEntity");
			_alreadyFetchedSiteTemplateEntity = info.GetBoolean("_alreadyFetchedSiteTemplateEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PageTemplateFieldIndex)fieldIndex)
			{
				case PageTemplateFieldIndex.ParentPageTemplateId:
					DesetupSyncPageTemplateEntity(true, false);
					_alreadyFetchedPageTemplateEntity = false;
					break;
				case PageTemplateFieldIndex.SiteTemplateId:
					DesetupSyncSiteTemplateEntity(true, false);
					_alreadyFetchedSiteTemplateEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttachmentCollection = (_attachmentCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedPageCollection = (_pageCollection.Count > 0);
			_alreadyFetchedPageTemplateCollection = (_pageTemplateCollection.Count > 0);
			_alreadyFetchedPageTemplateElementCollection = (_pageTemplateElementCollection.Count > 0);
			_alreadyFetchedPageTemplateLanguageCollection = (_pageTemplateLanguageCollection.Count > 0);
			_alreadyFetchedPageTemplateEntity = (_pageTemplateEntity != null);
			_alreadyFetchedSiteTemplateEntity = (_siteTemplateEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PageTemplateEntity":
					toReturn.Add(Relations.PageTemplateEntityUsingPageTemplateIdParentPageTemplateId);
					break;
				case "SiteTemplateEntity":
					toReturn.Add(Relations.SiteTemplateEntityUsingSiteTemplateId);
					break;
				case "AttachmentCollection":
					toReturn.Add(Relations.AttachmentEntityUsingPageTemplateId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingPageTemplateId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingPageTemplateId);
					break;
				case "PageCollection":
					toReturn.Add(Relations.PageEntityUsingPageTemplateId);
					break;
				case "PageTemplateCollection":
					toReturn.Add(Relations.PageTemplateEntityUsingParentPageTemplateId);
					break;
				case "PageTemplateElementCollection":
					toReturn.Add(Relations.PageTemplateElementEntityUsingPageTemplateId);
					break;
				case "PageTemplateLanguageCollection":
					toReturn.Add(Relations.PageTemplateLanguageEntityUsingPageTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attachmentCollection", (!this.MarkedForDeletion?_attachmentCollection:null));
			info.AddValue("_alwaysFetchAttachmentCollection", _alwaysFetchAttachmentCollection);
			info.AddValue("_alreadyFetchedAttachmentCollection", _alreadyFetchedAttachmentCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_pageCollection", (!this.MarkedForDeletion?_pageCollection:null));
			info.AddValue("_alwaysFetchPageCollection", _alwaysFetchPageCollection);
			info.AddValue("_alreadyFetchedPageCollection", _alreadyFetchedPageCollection);
			info.AddValue("_pageTemplateCollection", (!this.MarkedForDeletion?_pageTemplateCollection:null));
			info.AddValue("_alwaysFetchPageTemplateCollection", _alwaysFetchPageTemplateCollection);
			info.AddValue("_alreadyFetchedPageTemplateCollection", _alreadyFetchedPageTemplateCollection);
			info.AddValue("_pageTemplateElementCollection", (!this.MarkedForDeletion?_pageTemplateElementCollection:null));
			info.AddValue("_alwaysFetchPageTemplateElementCollection", _alwaysFetchPageTemplateElementCollection);
			info.AddValue("_alreadyFetchedPageTemplateElementCollection", _alreadyFetchedPageTemplateElementCollection);
			info.AddValue("_pageTemplateLanguageCollection", (!this.MarkedForDeletion?_pageTemplateLanguageCollection:null));
			info.AddValue("_alwaysFetchPageTemplateLanguageCollection", _alwaysFetchPageTemplateLanguageCollection);
			info.AddValue("_alreadyFetchedPageTemplateLanguageCollection", _alreadyFetchedPageTemplateLanguageCollection);
			info.AddValue("_pageTemplateEntity", (!this.MarkedForDeletion?_pageTemplateEntity:null));
			info.AddValue("_pageTemplateEntityReturnsNewIfNotFound", _pageTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageTemplateEntity", _alwaysFetchPageTemplateEntity);
			info.AddValue("_alreadyFetchedPageTemplateEntity", _alreadyFetchedPageTemplateEntity);
			info.AddValue("_siteTemplateEntity", (!this.MarkedForDeletion?_siteTemplateEntity:null));
			info.AddValue("_siteTemplateEntityReturnsNewIfNotFound", _siteTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteTemplateEntity", _alwaysFetchSiteTemplateEntity);
			info.AddValue("_alreadyFetchedSiteTemplateEntity", _alreadyFetchedSiteTemplateEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PageTemplateEntity":
					_alreadyFetchedPageTemplateEntity = true;
					this.PageTemplateEntity = (PageTemplateEntity)entity;
					break;
				case "SiteTemplateEntity":
					_alreadyFetchedSiteTemplateEntity = true;
					this.SiteTemplateEntity = (SiteTemplateEntity)entity;
					break;
				case "AttachmentCollection":
					_alreadyFetchedAttachmentCollection = true;
					if(entity!=null)
					{
						this.AttachmentCollection.Add((AttachmentEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "PageCollection":
					_alreadyFetchedPageCollection = true;
					if(entity!=null)
					{
						this.PageCollection.Add((PageEntity)entity);
					}
					break;
				case "PageTemplateCollection":
					_alreadyFetchedPageTemplateCollection = true;
					if(entity!=null)
					{
						this.PageTemplateCollection.Add((PageTemplateEntity)entity);
					}
					break;
				case "PageTemplateElementCollection":
					_alreadyFetchedPageTemplateElementCollection = true;
					if(entity!=null)
					{
						this.PageTemplateElementCollection.Add((PageTemplateElementEntity)entity);
					}
					break;
				case "PageTemplateLanguageCollection":
					_alreadyFetchedPageTemplateLanguageCollection = true;
					if(entity!=null)
					{
						this.PageTemplateLanguageCollection.Add((PageTemplateLanguageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PageTemplateEntity":
					SetupSyncPageTemplateEntity(relatedEntity);
					break;
				case "SiteTemplateEntity":
					SetupSyncSiteTemplateEntity(relatedEntity);
					break;
				case "AttachmentCollection":
					_attachmentCollection.Add((AttachmentEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "PageCollection":
					_pageCollection.Add((PageEntity)relatedEntity);
					break;
				case "PageTemplateCollection":
					_pageTemplateCollection.Add((PageTemplateEntity)relatedEntity);
					break;
				case "PageTemplateElementCollection":
					_pageTemplateElementCollection.Add((PageTemplateElementEntity)relatedEntity);
					break;
				case "PageTemplateLanguageCollection":
					_pageTemplateLanguageCollection.Add((PageTemplateLanguageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PageTemplateEntity":
					DesetupSyncPageTemplateEntity(false, true);
					break;
				case "SiteTemplateEntity":
					DesetupSyncSiteTemplateEntity(false, true);
					break;
				case "AttachmentCollection":
					this.PerformRelatedEntityRemoval(_attachmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageCollection":
					this.PerformRelatedEntityRemoval(_pageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateElementCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateElementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateLanguageCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pageTemplateEntity!=null)
			{
				toReturn.Add(_pageTemplateEntity);
			}
			if(_siteTemplateEntity!=null)
			{
				toReturn.Add(_siteTemplateEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attachmentCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_pageCollection);
			toReturn.Add(_pageTemplateCollection);
			toReturn.Add(_pageTemplateElementCollection);
			toReturn.Add(_pageTemplateLanguageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageTemplateId)
		{
			return FetchUsingPK(pageTemplateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pageTemplateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pageTemplateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pageTemplateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PageTemplateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PageTemplateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttachmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttachmentCollection || forceFetch || _alwaysFetchAttachmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attachmentCollection);
				_attachmentCollection.SuppressClearInGetMulti=!forceFetch;
				_attachmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_attachmentCollection.GetMultiManyToOne(null, null, this, null, filter);
				_attachmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAttachmentCollection = true;
			}
			return _attachmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttachmentCollection'. These settings will be taken into account
		/// when the property AttachmentCollection is requested or GetMultiAttachmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttachmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attachmentCollection.SortClauses=sortClauses;
			_attachmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageCollection || forceFetch || _alwaysFetchPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageCollection);
				_pageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageCollection.GetMultiManyToOne(null, this, null, filter);
				_pageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageCollection = true;
			}
			return _pageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageCollection'. These settings will be taken into account
		/// when the property PageCollection is requested or GetMultiPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageCollection.SortClauses=sortClauses;
			_pageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch)
		{
			return GetMultiPageTemplateCollection(forceFetch, _pageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateCollection(forceFetch, _pageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateCollection || forceFetch || _alwaysFetchPageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateCollection);
				_pageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateCollection.GetMultiManyToOne(this, null, filter);
				_pageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateCollection = true;
			}
			return _pageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateCollection'. These settings will be taken into account
		/// when the property PageTemplateCollection is requested or GetMultiPageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateCollection.SortClauses=sortClauses;
			_pageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, _pageTemplateElementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, _pageTemplateElementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateElementCollection || forceFetch || _alwaysFetchPageTemplateElementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateElementCollection);
				_pageTemplateElementCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateElementCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateElementCollection.GetMultiManyToOne(null, this, filter);
				_pageTemplateElementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateElementCollection = true;
			}
			return _pageTemplateElementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateElementCollection'. These settings will be taken into account
		/// when the property PageTemplateElementCollection is requested or GetMultiPageTemplateElementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateElementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateElementCollection.SortClauses=sortClauses;
			_pageTemplateElementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, _pageTemplateLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, _pageTemplateLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateLanguageCollection || forceFetch || _alwaysFetchPageTemplateLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateLanguageCollection);
				_pageTemplateLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateLanguageCollection.GetMultiManyToOne(null, this, filter);
				_pageTemplateLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateLanguageCollection = true;
			}
			return _pageTemplateLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateLanguageCollection'. These settings will be taken into account
		/// when the property PageTemplateLanguageCollection is requested or GetMultiPageTemplateLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateLanguageCollection.SortClauses=sortClauses;
			_pageTemplateLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public PageTemplateEntity GetSinglePageTemplateEntity()
		{
			return GetSinglePageTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public virtual PageTemplateEntity GetSinglePageTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageTemplateEntity || forceFetch || _alwaysFetchPageTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageTemplateEntityUsingPageTemplateIdParentPageTemplateId);
				PageTemplateEntity newEntity = new PageTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentPageTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageTemplateEntity = newEntity;
				_alreadyFetchedPageTemplateEntity = fetchResult;
			}
			return _pageTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public SiteTemplateEntity GetSingleSiteTemplateEntity()
		{
			return GetSingleSiteTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public virtual SiteTemplateEntity GetSingleSiteTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteTemplateEntity || forceFetch || _alwaysFetchSiteTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteTemplateEntityUsingSiteTemplateId);
				SiteTemplateEntity newEntity = new SiteTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteTemplateId);
				}
				if(fetchResult)
				{
					newEntity = (SiteTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteTemplateEntity = newEntity;
				_alreadyFetchedSiteTemplateEntity = fetchResult;
			}
			return _siteTemplateEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PageTemplateEntity", _pageTemplateEntity);
			toReturn.Add("SiteTemplateEntity", _siteTemplateEntity);
			toReturn.Add("AttachmentCollection", _attachmentCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("PageCollection", _pageCollection);
			toReturn.Add("PageTemplateCollection", _pageTemplateCollection);
			toReturn.Add("PageTemplateElementCollection", _pageTemplateElementCollection);
			toReturn.Add("PageTemplateLanguageCollection", _pageTemplateLanguageCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="validator">The validator object for this PageTemplateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pageTemplateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pageTemplateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attachmentCollection = new Obymobi.Data.CollectionClasses.AttachmentCollection();
			_attachmentCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_pageCollection = new Obymobi.Data.CollectionClasses.PageCollection();
			_pageCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_pageTemplateCollection = new Obymobi.Data.CollectionClasses.PageTemplateCollection();
			_pageTemplateCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_pageTemplateElementCollection = new Obymobi.Data.CollectionClasses.PageTemplateElementCollection();
			_pageTemplateElementCollection.SetContainingEntityInfo(this, "PageTemplateEntity");

			_pageTemplateLanguageCollection = new Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection();
			_pageTemplateLanguageCollection.SetContainingEntityInfo(this, "PageTemplateEntity");
			_pageTemplateEntityReturnsNewIfNotFound = true;
			_siteTemplateEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentPageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pageTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticPageTemplateRelations.PageTemplateEntityUsingPageTemplateIdParentPageTemplateIdStatic, true, signalRelatedEntity, "PageTemplateCollection", resetFKFields, new int[] { (int)PageTemplateFieldIndex.ParentPageTemplateId } );		
			_pageTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageTemplateEntity(IEntityCore relatedEntity)
		{
			if(_pageTemplateEntity!=relatedEntity)
			{		
				DesetupSyncPageTemplateEntity(true, true);
				_pageTemplateEntity = (PageTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticPageTemplateRelations.PageTemplateEntityUsingPageTemplateIdParentPageTemplateIdStatic, true, ref _alreadyFetchedPageTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticPageTemplateRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, signalRelatedEntity, "PageTemplateCollection", resetFKFields, new int[] { (int)PageTemplateFieldIndex.SiteTemplateId } );		
			_siteTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteTemplateEntity(IEntityCore relatedEntity)
		{
			if(_siteTemplateEntity!=relatedEntity)
			{		
				DesetupSyncSiteTemplateEntity(true, true);
				_siteTemplateEntity = (SiteTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticPageTemplateRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, ref _alreadyFetchedSiteTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PageTemplateFieldIndex.PageTemplateId].ForcedCurrentValueWrite(pageTemplateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePageTemplateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PageTemplateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PageTemplateRelations Relations
		{
			get	{ return new PageTemplateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attachment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentCollection(), (IEntityRelation)GetRelationsForField("AttachmentCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.AttachmentEntity, 0, null, null, null, "AttachmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplateElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateElementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateElementCollection(), (IEntityRelation)GetRelationsForField("PageTemplateElementCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.PageTemplateElementEntity, 0, null, null, null, "PageTemplateElementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplateLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection(), (IEntityRelation)GetRelationsForField("PageTemplateLanguageCollection")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.PageTemplateLanguageEntity, 0, null, null, null, "PageTemplateLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateEntity")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateEntity")[0], (int)Obymobi.Data.EntityType.PageTemplateEntity, (int)Obymobi.Data.EntityType.SiteTemplateEntity, 0, null, null, null, "SiteTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PageTemplateId property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."PageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PageTemplateId
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.PageTemplateId, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.PageTemplateId, value, true); }
		}

		/// <summary> The Name property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PageTemplateFieldIndex.Name, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.Name, value, true); }
		}

		/// <summary> The PageType property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."PageType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageType
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.PageType, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.PageType, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentPageTemplateId property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."ParentPageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentPageTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PageTemplateFieldIndex.ParentPageTemplateId, false); }
			set	{ SetValue((int)PageTemplateFieldIndex.ParentPageTemplateId, value, true); }
		}

		/// <summary> The SiteTemplateId property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."SiteTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteTemplateId
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.SiteTemplateId, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.SiteTemplateId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)PageTemplateFieldIndex.SortOrder, true); }
			set	{ SetValue((int)PageTemplateFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PageTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PageTemplateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PageTemplate"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PageTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PageTemplateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttachmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection AttachmentCollection
		{
			get	{ return GetMultiAttachmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentCollection. When set to true, AttachmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAttachmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentCollection
		{
			get	{ return _alwaysFetchAttachmentCollection; }
			set	{ _alwaysFetchAttachmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentCollection already has been fetched. Setting this property to false when AttachmentCollection has been fetched
		/// will clear the AttachmentCollection collection well. Setting this property to true while AttachmentCollection hasn't been fetched disables lazy loading for AttachmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentCollection
		{
			get { return _alreadyFetchedAttachmentCollection;}
			set 
			{
				if(_alreadyFetchedAttachmentCollection && !value && (_attachmentCollection != null))
				{
					_attachmentCollection.Clear();
				}
				_alreadyFetchedAttachmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageCollection PageCollection
		{
			get	{ return GetMultiPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageCollection. When set to true, PageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageCollection
		{
			get	{ return _alwaysFetchPageCollection; }
			set	{ _alwaysFetchPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageCollection already has been fetched. Setting this property to false when PageCollection has been fetched
		/// will clear the PageCollection collection well. Setting this property to true while PageCollection hasn't been fetched disables lazy loading for PageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageCollection
		{
			get { return _alreadyFetchedPageCollection;}
			set 
			{
				if(_alreadyFetchedPageCollection && !value && (_pageCollection != null))
				{
					_pageCollection.Clear();
				}
				_alreadyFetchedPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateCollection PageTemplateCollection
		{
			get	{ return GetMultiPageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateCollection. When set to true, PageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateCollection
		{
			get	{ return _alwaysFetchPageTemplateCollection; }
			set	{ _alwaysFetchPageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateCollection already has been fetched. Setting this property to false when PageTemplateCollection has been fetched
		/// will clear the PageTemplateCollection collection well. Setting this property to true while PageTemplateCollection hasn't been fetched disables lazy loading for PageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateCollection
		{
			get { return _alreadyFetchedPageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateCollection && !value && (_pageTemplateCollection != null))
				{
					_pageTemplateCollection.Clear();
				}
				_alreadyFetchedPageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateElementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateElementCollection PageTemplateElementCollection
		{
			get	{ return GetMultiPageTemplateElementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateElementCollection. When set to true, PageTemplateElementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateElementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateElementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateElementCollection
		{
			get	{ return _alwaysFetchPageTemplateElementCollection; }
			set	{ _alwaysFetchPageTemplateElementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateElementCollection already has been fetched. Setting this property to false when PageTemplateElementCollection has been fetched
		/// will clear the PageTemplateElementCollection collection well. Setting this property to true while PageTemplateElementCollection hasn't been fetched disables lazy loading for PageTemplateElementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateElementCollection
		{
			get { return _alreadyFetchedPageTemplateElementCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateElementCollection && !value && (_pageTemplateElementCollection != null))
				{
					_pageTemplateElementCollection.Clear();
				}
				_alreadyFetchedPageTemplateElementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection PageTemplateLanguageCollection
		{
			get	{ return GetMultiPageTemplateLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateLanguageCollection. When set to true, PageTemplateLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateLanguageCollection
		{
			get	{ return _alwaysFetchPageTemplateLanguageCollection; }
			set	{ _alwaysFetchPageTemplateLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateLanguageCollection already has been fetched. Setting this property to false when PageTemplateLanguageCollection has been fetched
		/// will clear the PageTemplateLanguageCollection collection well. Setting this property to true while PageTemplateLanguageCollection hasn't been fetched disables lazy loading for PageTemplateLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateLanguageCollection
		{
			get { return _alreadyFetchedPageTemplateLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateLanguageCollection && !value && (_pageTemplateLanguageCollection != null))
				{
					_pageTemplateLanguageCollection.Clear();
				}
				_alreadyFetchedPageTemplateLanguageCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PageTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageTemplateEntity PageTemplateEntity
		{
			get	{ return GetSinglePageTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageTemplateCollection", "PageTemplateEntity", _pageTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateEntity. When set to true, PageTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateEntity is accessed. You can always execute a forced fetch by calling GetSinglePageTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateEntity
		{
			get	{ return _alwaysFetchPageTemplateEntity; }
			set	{ _alwaysFetchPageTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateEntity already has been fetched. Setting this property to false when PageTemplateEntity has been fetched
		/// will set PageTemplateEntity to null as well. Setting this property to true while PageTemplateEntity hasn't been fetched disables lazy loading for PageTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateEntity
		{
			get { return _alreadyFetchedPageTemplateEntity;}
			set 
			{
				if(_alreadyFetchedPageTemplateEntity && !value)
				{
					this.PageTemplateEntity = null;
				}
				_alreadyFetchedPageTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageTemplateEntity is not found
		/// in the database. When set to true, PageTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _pageTemplateEntityReturnsNewIfNotFound; }
			set { _pageTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteTemplateEntity SiteTemplateEntity
		{
			get	{ return GetSingleSiteTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageTemplateCollection", "SiteTemplateEntity", _siteTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateEntity. When set to true, SiteTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateEntity
		{
			get	{ return _alwaysFetchSiteTemplateEntity; }
			set	{ _alwaysFetchSiteTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateEntity already has been fetched. Setting this property to false when SiteTemplateEntity has been fetched
		/// will set SiteTemplateEntity to null as well. Setting this property to true while SiteTemplateEntity hasn't been fetched disables lazy loading for SiteTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateEntity
		{
			get { return _alreadyFetchedSiteTemplateEntity;}
			set 
			{
				if(_alreadyFetchedSiteTemplateEntity && !value)
				{
					this.SiteTemplateEntity = null;
				}
				_alreadyFetchedSiteTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteTemplateEntity is not found
		/// in the database. When set to true, SiteTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _siteTemplateEntityReturnsNewIfNotFound; }
			set { _siteTemplateEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PageTemplateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
