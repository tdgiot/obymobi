﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'WidgetCarousel'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public  partial class WidgetCarouselEntityBase : WidgetEntity
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "WidgetCarouselEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CarouselItemCollection	_carouselItemCollection;
		private bool	_alwaysFetchCarouselItemCollection, _alreadyFetchedCarouselItemCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static new partial class MemberNames
		{
			/// <summary>Member name ActionEntity</summary>
			public static readonly string ActionEntity = "ActionEntity";
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name CarouselItemCollection</summary>
			public static readonly string CarouselItemCollection = "CarouselItemCollection";
			/// <summary>Member name LandingPageWidgetCollection</summary>
			public static readonly string LandingPageWidgetCollection = "LandingPageWidgetCollection";
			/// <summary>Member name NavigationMenuWidgetCollection</summary>
			public static readonly string NavigationMenuWidgetCollection = "NavigationMenuWidgetCollection";
			/// <summary>Member name ChildWidgetCollection</summary>
			public static readonly string ChildWidgetCollection = "ChildWidgetCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WidgetCarouselEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected WidgetCarouselEntityBase() 
		{
			InitClassEmpty(null);
			SetName("WidgetCarouselEntity");
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		protected WidgetCarouselEntityBase(System.Int32 widgetId):base(widgetId)
		{
			InitClassFetch(widgetId, null, null);
			SetName("WidgetCarouselEntity");
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected WidgetCarouselEntityBase(System.Int32 widgetId, IPrefetchPath prefetchPathToUse): base(widgetId, prefetchPathToUse)
		{
			InitClassFetch(widgetId, null, prefetchPathToUse);
			SetName("WidgetCarouselEntity");
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		/// <param name="validator">The custom validator object for this WidgetCarouselEntity</param>
		protected WidgetCarouselEntityBase(System.Int32 widgetId, IValidator validator):base(widgetId, validator)
		{
			InitClassFetch(widgetId, validator, null);
			SetName("WidgetCarouselEntity");
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetCarouselEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_carouselItemCollection = (Obymobi.Data.CollectionClasses.CarouselItemCollection)info.GetValue("_carouselItemCollection", typeof(Obymobi.Data.CollectionClasses.CarouselItemCollection));
			_alwaysFetchCarouselItemCollection = info.GetBoolean("_alwaysFetchCarouselItemCollection");
			_alreadyFetchedCarouselItemCollection = info.GetBoolean("_alreadyFetchedCarouselItemCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCarouselItemCollection = (_carouselItemCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static new RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CarouselItemCollection":
					toReturn.Add(Relations.CarouselItemEntityUsingWidgetCarouselId);
					break;
				default:
					toReturn = WidgetEntity.GetRelationsForField(fieldName);
					break;				
			}
			return toReturn;
		}

		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public new static IPredicateExpression GetEntityTypeFilter()
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WidgetCarouselEntity", false);
		}
		
		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public new static IPredicateExpression GetEntityTypeFilter(bool negate)
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WidgetCarouselEntity", negate);
		}

		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_carouselItemCollection", (!this.MarkedForDeletion?_carouselItemCollection:null));
			info.AddValue("_alwaysFetchCarouselItemCollection", _alwaysFetchCarouselItemCollection);
			info.AddValue("_alreadyFetchedCarouselItemCollection", _alreadyFetchedCarouselItemCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CarouselItemCollection":
					_alreadyFetchedCarouselItemCollection = true;
					if(entity!=null)
					{
						this.CarouselItemCollection.Add((CarouselItemEntity)entity);
					}
					break;
				default:
					base.SetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CarouselItemCollection":
					_carouselItemCollection.Add((CarouselItemEntity)relatedEntity);
					break;
				default:
					base.SetRelatedEntity(relatedEntity, fieldName);
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CarouselItemCollection":
					this.PerformRelatedEntityRemoval(_carouselItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					base.UnsetRelatedEntity(relatedEntity, fieldName, signalRelatedEntityManyToOne);
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			toReturn.AddRange(base.GetDependingRelatedEntities());
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			toReturn.AddRange(base.GetDependentRelatedEntities());
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_carouselItemCollection);
			toReturn.AddRange(base.GetMemberEntityCollections());
			return toReturn;
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static new WidgetCarouselEntity FetchPolymorphic(ITransaction transactionToUse, System.Int32 widgetId, Context contextToUse)
		{
			return FetchPolymorphic(transactionToUse, widgetId, contextToUse, null);
		}
				
		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static new WidgetCarouselEntity FetchPolymorphic(ITransaction transactionToUse, System.Int32 widgetId, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.WidgetCarouselEntity);
			fields.ForcedValueWrite((int)WidgetCarouselFieldIndex.WidgetId, widgetId);
			return (WidgetCarouselEntity)new WidgetCarouselDAO().FetchExistingPolymorphic(transactionToUse, fields, contextToUse, excludedIncludedFields);
		}


		/// <summary>Determines whether this entity is a subType of the entity represented by the passed in enum value, which represents a value in the Obymobi.Data.EntityType enum</summary>
		/// <param name="typeOfEntity">Type of entity.</param>
		/// <returns>true if the passed in type is a supertype of this entity, otherwise false</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckIfIsSubTypeOf(int typeOfEntity)
		{
			return InheritanceInfoProviderSingleton.GetInstance().CheckIfIsSubTypeOf("WidgetCarouselEntity", ((Obymobi.Data.EntityType)typeOfEntity).ToString());
		}
				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WidgetCarouselRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CarouselItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CarouselItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.CarouselItemCollection GetMultiCarouselItemCollection(bool forceFetch)
		{
			return GetMultiCarouselItemCollection(forceFetch, _carouselItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CarouselItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CarouselItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.CarouselItemCollection GetMultiCarouselItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCarouselItemCollection(forceFetch, _carouselItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CarouselItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CarouselItemCollection GetMultiCarouselItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCarouselItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CarouselItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CarouselItemCollection GetMultiCarouselItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCarouselItemCollection || forceFetch || _alwaysFetchCarouselItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_carouselItemCollection);
				_carouselItemCollection.SuppressClearInGetMulti=!forceFetch;
				_carouselItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_carouselItemCollection.GetMultiManyToOne(null, this, filter);
				_carouselItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCarouselItemCollection = true;
			}
			return _carouselItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CarouselItemCollection'. These settings will be taken into account
		/// when the property CarouselItemCollection is requested or GetMultiCarouselItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCarouselItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_carouselItemCollection.SortClauses=sortClauses;
			_carouselItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = base.GetRelatedData();
			toReturn.Add("CarouselItemCollection", _carouselItemCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="widgetId">PK value for WidgetCarousel which data should be fetched into this WidgetCarousel object</param>
		/// <param name="validator">The validator object for this WidgetCarouselEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 widgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			InitClassMembers();	

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_carouselItemCollection = new Obymobi.Data.CollectionClasses.CarouselItemCollection();
			_carouselItemCollection.SetContainingEntityInfo(this, "WidgetCarouselEntity");

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END

		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
		}
		#endregion

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWidgetCarouselDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WidgetCarouselEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public new static WidgetCarouselRelations Relations
		{
			get	{ return new WidgetCarouselRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public new static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CarouselItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCarouselItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CarouselItemCollection(), (IEntityRelation)GetRelationsForField("CarouselItemCollection")[0], (int)Obymobi.Data.EntityType.WidgetCarouselEntity, (int)Obymobi.Data.EntityType.CarouselItemEntity, 0, null, null, null, "CarouselItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public new static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> Retrieves all related entities of type 'CarouselItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCarouselItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CarouselItemCollection CarouselItemCollection
		{
			get	{ return GetMultiCarouselItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CarouselItemCollection. When set to true, CarouselItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CarouselItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCarouselItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCarouselItemCollection
		{
			get	{ return _alwaysFetchCarouselItemCollection; }
			set	{ _alwaysFetchCarouselItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CarouselItemCollection already has been fetched. Setting this property to false when CarouselItemCollection has been fetched
		/// will clear the CarouselItemCollection collection well. Setting this property to true while CarouselItemCollection hasn't been fetched disables lazy loading for CarouselItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCarouselItemCollection
		{
			get { return _alreadyFetchedCarouselItemCollection;}
			set 
			{
				if(_alreadyFetchedCarouselItemCollection && !value && (_carouselItemCollection != null))
				{
					_carouselItemCollection.Clear();
				}
				_alreadyFetchedCarouselItemCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return true;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.TargetPerEntity;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.WidgetCarouselEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
