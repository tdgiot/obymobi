﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Site'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SiteEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SiteEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AvailabilityCollection	_availabilityCollection;
		private bool	_alwaysFetchAvailabilityCollection, _alreadyFetchedAvailabilityCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection	_entertainmentCollection;
		private bool	_alwaysFetchEntertainmentCollection, _alreadyFetchedEntertainmentCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection_;
		private bool	_alwaysFetchMediaCollection_, _alreadyFetchedMediaCollection_;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageTemplateCollection	_messageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCollection, _alreadyFetchedMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.PageCollection	_pageCollection;
		private bool	_alwaysFetchPageCollection, _alreadyFetchedPageCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private Obymobi.Data.CollectionClasses.SiteCultureCollection	_siteCultureCollection;
		private bool	_alwaysFetchSiteCultureCollection, _alreadyFetchedSiteCultureCollection;
		private Obymobi.Data.CollectionClasses.SiteLanguageCollection	_siteLanguageCollection;
		private bool	_alwaysFetchSiteLanguageCollection, _alreadyFetchedSiteLanguageCollection;
		private Obymobi.Data.CollectionClasses.UITabCollection	_uITabCollection;
		private bool	_alwaysFetchUITabCollection, _alreadyFetchedUITabCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private SiteTemplateEntity _siteTemplateEntity;
		private bool	_alwaysFetchSiteTemplateEntity, _alreadyFetchedSiteTemplateEntity, _siteTemplateEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name SiteTemplateEntity</summary>
			public static readonly string SiteTemplateEntity = "SiteTemplateEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AvailabilityCollection</summary>
			public static readonly string AvailabilityCollection = "AvailabilityCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name EntertainmentCollection</summary>
			public static readonly string EntertainmentCollection = "EntertainmentCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaCollection_</summary>
			public static readonly string MediaCollection_ = "MediaCollection_";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageTemplateCollection</summary>
			public static readonly string MessageTemplateCollection = "MessageTemplateCollection";
			/// <summary>Member name PageCollection</summary>
			public static readonly string PageCollection = "PageCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
			/// <summary>Member name SiteCultureCollection</summary>
			public static readonly string SiteCultureCollection = "SiteCultureCollection";
			/// <summary>Member name SiteLanguageCollection</summary>
			public static readonly string SiteLanguageCollection = "SiteLanguageCollection";
			/// <summary>Member name UITabCollection</summary>
			public static readonly string UITabCollection = "UITabCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SiteEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SiteEntityBase() :base("SiteEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		protected SiteEntityBase(System.Int32 siteId):base("SiteEntity")
		{
			InitClassFetch(siteId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SiteEntityBase(System.Int32 siteId, IPrefetchPath prefetchPathToUse): base("SiteEntity")
		{
			InitClassFetch(siteId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="validator">The custom validator object for this SiteEntity</param>
		protected SiteEntityBase(System.Int32 siteId, IValidator validator):base("SiteEntity")
		{
			InitClassFetch(siteId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_availabilityCollection = (Obymobi.Data.CollectionClasses.AvailabilityCollection)info.GetValue("_availabilityCollection", typeof(Obymobi.Data.CollectionClasses.AvailabilityCollection));
			_alwaysFetchAvailabilityCollection = info.GetBoolean("_alwaysFetchAvailabilityCollection");
			_alreadyFetchedAvailabilityCollection = info.GetBoolean("_alreadyFetchedAvailabilityCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_entertainmentCollection = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollection", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollection = info.GetBoolean("_alwaysFetchEntertainmentCollection");
			_alreadyFetchedEntertainmentCollection = info.GetBoolean("_alreadyFetchedEntertainmentCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_mediaCollection_ = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection_", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection_ = info.GetBoolean("_alwaysFetchMediaCollection_");
			_alreadyFetchedMediaCollection_ = info.GetBoolean("_alreadyFetchedMediaCollection_");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCollection)info.GetValue("_messageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCollection));
			_alwaysFetchMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCollection");

			_pageCollection = (Obymobi.Data.CollectionClasses.PageCollection)info.GetValue("_pageCollection", typeof(Obymobi.Data.CollectionClasses.PageCollection));
			_alwaysFetchPageCollection = info.GetBoolean("_alwaysFetchPageCollection");
			_alreadyFetchedPageCollection = info.GetBoolean("_alreadyFetchedPageCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");

			_siteCultureCollection = (Obymobi.Data.CollectionClasses.SiteCultureCollection)info.GetValue("_siteCultureCollection", typeof(Obymobi.Data.CollectionClasses.SiteCultureCollection));
			_alwaysFetchSiteCultureCollection = info.GetBoolean("_alwaysFetchSiteCultureCollection");
			_alreadyFetchedSiteCultureCollection = info.GetBoolean("_alreadyFetchedSiteCultureCollection");

			_siteLanguageCollection = (Obymobi.Data.CollectionClasses.SiteLanguageCollection)info.GetValue("_siteLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SiteLanguageCollection));
			_alwaysFetchSiteLanguageCollection = info.GetBoolean("_alwaysFetchSiteLanguageCollection");
			_alreadyFetchedSiteLanguageCollection = info.GetBoolean("_alreadyFetchedSiteLanguageCollection");

			_uITabCollection = (Obymobi.Data.CollectionClasses.UITabCollection)info.GetValue("_uITabCollection", typeof(Obymobi.Data.CollectionClasses.UITabCollection));
			_alwaysFetchUITabCollection = info.GetBoolean("_alwaysFetchUITabCollection");
			_alreadyFetchedUITabCollection = info.GetBoolean("_alreadyFetchedUITabCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_siteTemplateEntity = (SiteTemplateEntity)info.GetValue("_siteTemplateEntity", typeof(SiteTemplateEntity));
			if(_siteTemplateEntity!=null)
			{
				_siteTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_siteTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchSiteTemplateEntity = info.GetBoolean("_alwaysFetchSiteTemplateEntity");
			_alreadyFetchedSiteTemplateEntity = info.GetBoolean("_alreadyFetchedSiteTemplateEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SiteFieldIndex)fieldIndex)
			{
				case SiteFieldIndex.SiteTemplateId:
					DesetupSyncSiteTemplateEntity(true, false);
					_alreadyFetchedSiteTemplateEntity = false;
					break;
				case SiteFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAvailabilityCollection = (_availabilityCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedEntertainmentCollection = (_entertainmentCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedMediaCollection_ = (_mediaCollection_.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageTemplateCollection = (_messageTemplateCollection.Count > 0);
			_alreadyFetchedPageCollection = (_pageCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedSiteCultureCollection = (_siteCultureCollection.Count > 0);
			_alreadyFetchedSiteLanguageCollection = (_siteLanguageCollection.Count > 0);
			_alreadyFetchedUITabCollection = (_uITabCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedSiteTemplateEntity = (_siteTemplateEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "SiteTemplateEntity":
					toReturn.Add(Relations.SiteTemplateEntityUsingSiteTemplateId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingActionSiteId);
					break;
				case "AvailabilityCollection":
					toReturn.Add(Relations.AvailabilityEntityUsingActionSiteId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingSiteId);
					break;
				case "EntertainmentCollection":
					toReturn.Add(Relations.EntertainmentEntityUsingSiteId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingSiteId);
					break;
				case "MediaCollection_":
					toReturn.Add(Relations.MediaEntityUsingActionSiteId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingSiteId);
					break;
				case "MessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateEntityUsingSiteId);
					break;
				case "PageCollection":
					toReturn.Add(Relations.PageEntityUsingSiteId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingSiteId);
					break;
				case "SiteCultureCollection":
					toReturn.Add(Relations.SiteCultureEntityUsingSiteId);
					break;
				case "SiteLanguageCollection":
					toReturn.Add(Relations.SiteLanguageEntityUsingSiteId);
					break;
				case "UITabCollection":
					toReturn.Add(Relations.UITabEntityUsingSiteId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingSiteId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingSiteId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_availabilityCollection", (!this.MarkedForDeletion?_availabilityCollection:null));
			info.AddValue("_alwaysFetchAvailabilityCollection", _alwaysFetchAvailabilityCollection);
			info.AddValue("_alreadyFetchedAvailabilityCollection", _alreadyFetchedAvailabilityCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_entertainmentCollection", (!this.MarkedForDeletion?_entertainmentCollection:null));
			info.AddValue("_alwaysFetchEntertainmentCollection", _alwaysFetchEntertainmentCollection);
			info.AddValue("_alreadyFetchedEntertainmentCollection", _alreadyFetchedEntertainmentCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_mediaCollection_", (!this.MarkedForDeletion?_mediaCollection_:null));
			info.AddValue("_alwaysFetchMediaCollection_", _alwaysFetchMediaCollection_);
			info.AddValue("_alreadyFetchedMediaCollection_", _alreadyFetchedMediaCollection_);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCollection", _alwaysFetchMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCollection", _alreadyFetchedMessageTemplateCollection);
			info.AddValue("_pageCollection", (!this.MarkedForDeletion?_pageCollection:null));
			info.AddValue("_alwaysFetchPageCollection", _alwaysFetchPageCollection);
			info.AddValue("_alreadyFetchedPageCollection", _alreadyFetchedPageCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_siteCultureCollection", (!this.MarkedForDeletion?_siteCultureCollection:null));
			info.AddValue("_alwaysFetchSiteCultureCollection", _alwaysFetchSiteCultureCollection);
			info.AddValue("_alreadyFetchedSiteCultureCollection", _alreadyFetchedSiteCultureCollection);
			info.AddValue("_siteLanguageCollection", (!this.MarkedForDeletion?_siteLanguageCollection:null));
			info.AddValue("_alwaysFetchSiteLanguageCollection", _alwaysFetchSiteLanguageCollection);
			info.AddValue("_alreadyFetchedSiteLanguageCollection", _alreadyFetchedSiteLanguageCollection);
			info.AddValue("_uITabCollection", (!this.MarkedForDeletion?_uITabCollection:null));
			info.AddValue("_alwaysFetchUITabCollection", _alwaysFetchUITabCollection);
			info.AddValue("_alreadyFetchedUITabCollection", _alreadyFetchedUITabCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_siteTemplateEntity", (!this.MarkedForDeletion?_siteTemplateEntity:null));
			info.AddValue("_siteTemplateEntityReturnsNewIfNotFound", _siteTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteTemplateEntity", _alwaysFetchSiteTemplateEntity);
			info.AddValue("_alreadyFetchedSiteTemplateEntity", _alreadyFetchedSiteTemplateEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "SiteTemplateEntity":
					_alreadyFetchedSiteTemplateEntity = true;
					this.SiteTemplateEntity = (SiteTemplateEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AvailabilityCollection":
					_alreadyFetchedAvailabilityCollection = true;
					if(entity!=null)
					{
						this.AvailabilityCollection.Add((AvailabilityEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "EntertainmentCollection":
					_alreadyFetchedEntertainmentCollection = true;
					if(entity!=null)
					{
						this.EntertainmentCollection.Add((EntertainmentEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollection_":
					_alreadyFetchedMediaCollection_ = true;
					if(entity!=null)
					{
						this.MediaCollection_.Add((MediaEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageTemplateCollection":
					_alreadyFetchedMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCollection.Add((MessageTemplateEntity)entity);
					}
					break;
				case "PageCollection":
					_alreadyFetchedPageCollection = true;
					if(entity!=null)
					{
						this.PageCollection.Add((PageEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				case "SiteCultureCollection":
					_alreadyFetchedSiteCultureCollection = true;
					if(entity!=null)
					{
						this.SiteCultureCollection.Add((SiteCultureEntity)entity);
					}
					break;
				case "SiteLanguageCollection":
					_alreadyFetchedSiteLanguageCollection = true;
					if(entity!=null)
					{
						this.SiteLanguageCollection.Add((SiteLanguageEntity)entity);
					}
					break;
				case "UITabCollection":
					_alreadyFetchedUITabCollection = true;
					if(entity!=null)
					{
						this.UITabCollection.Add((UITabEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "SiteTemplateEntity":
					SetupSyncSiteTemplateEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AvailabilityCollection":
					_availabilityCollection.Add((AvailabilityEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "EntertainmentCollection":
					_entertainmentCollection.Add((EntertainmentEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "MediaCollection_":
					_mediaCollection_.Add((MediaEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageTemplateCollection":
					_messageTemplateCollection.Add((MessageTemplateEntity)relatedEntity);
					break;
				case "PageCollection":
					_pageCollection.Add((PageEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				case "SiteCultureCollection":
					_siteCultureCollection.Add((SiteCultureEntity)relatedEntity);
					break;
				case "SiteLanguageCollection":
					_siteLanguageCollection.Add((SiteLanguageEntity)relatedEntity);
					break;
				case "UITabCollection":
					_uITabCollection.Add((UITabEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "SiteTemplateEntity":
					DesetupSyncSiteTemplateEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AvailabilityCollection":
					this.PerformRelatedEntityRemoval(_availabilityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntertainmentCollection":
					this.PerformRelatedEntityRemoval(_entertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection_":
					this.PerformRelatedEntityRemoval(_mediaCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageCollection":
					this.PerformRelatedEntityRemoval(_pageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteCultureCollection":
					this.PerformRelatedEntityRemoval(_siteCultureCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteLanguageCollection":
					this.PerformRelatedEntityRemoval(_siteLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UITabCollection":
					this.PerformRelatedEntityRemoval(_uITabCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_siteTemplateEntity!=null)
			{
				toReturn.Add(_siteTemplateEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_availabilityCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_entertainmentCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_mediaCollection_);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageTemplateCollection);
			toReturn.Add(_pageCollection);
			toReturn.Add(_scheduledMessageCollection);
			toReturn.Add(_siteCultureCollection);
			toReturn.Add(_siteLanguageCollection);
			toReturn.Add(_uITabCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteId)
		{
			return FetchUsingPK(siteId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(siteId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(siteId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(siteId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SiteId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SiteRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAvailabilityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAvailabilityCollection || forceFetch || _alwaysFetchAvailabilityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_availabilityCollection);
				_availabilityCollection.SuppressClearInGetMulti=!forceFetch;
				_availabilityCollection.EntityFactoryToUse = entityFactoryToUse;
				_availabilityCollection.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_availabilityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAvailabilityCollection = true;
			}
			return _availabilityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AvailabilityCollection'. These settings will be taken into account
		/// when the property AvailabilityCollection is requested or GetMultiAvailabilityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAvailabilityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_availabilityCollection.SortClauses=sortClauses;
			_availabilityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntertainmentCollection || forceFetch || _alwaysFetchEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollection);
				_entertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_entertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollection = true;
			}
			return _entertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollection'. These settings will be taken into account
		/// when the property EntertainmentCollection is requested or GetMultiEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollection.SortClauses=sortClauses;
			_entertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch)
		{
			return GetMultiMediaCollection_(forceFetch, _mediaCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection_(forceFetch, _mediaCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection_ || forceFetch || _alwaysFetchMediaCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection_);
				_mediaCollection_.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection_.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection_.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_mediaCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection_ = true;
			}
			return _mediaCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection_'. These settings will be taken into account
		/// when the property MediaCollection_ is requested or GetMultiMediaCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection_.SortClauses=sortClauses;
			_mediaCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCollection);
				_messageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, filter);
				_messageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCollection = true;
			}
			return _messageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCollection is requested or GetMultiMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageCollection || forceFetch || _alwaysFetchPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageCollection);
				_pageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageCollection.GetMultiManyToOne(null, null, this, filter);
				_pageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageCollection = true;
			}
			return _pageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageCollection'. These settings will be taken into account
		/// when the property PageCollection is requested or GetMultiPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageCollection.SortClauses=sortClauses;
			_pageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteCultureCollection GetMultiSiteCultureCollection(bool forceFetch)
		{
			return GetMultiSiteCultureCollection(forceFetch, _siteCultureCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteCultureCollection GetMultiSiteCultureCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteCultureCollection(forceFetch, _siteCultureCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteCultureCollection GetMultiSiteCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteCultureCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteCultureCollection GetMultiSiteCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteCultureCollection || forceFetch || _alwaysFetchSiteCultureCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteCultureCollection);
				_siteCultureCollection.SuppressClearInGetMulti=!forceFetch;
				_siteCultureCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteCultureCollection.GetMultiManyToOne(this, filter);
				_siteCultureCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteCultureCollection = true;
			}
			return _siteCultureCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteCultureCollection'. These settings will be taken into account
		/// when the property SiteCultureCollection is requested or GetMultiSiteCultureCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteCultureCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteCultureCollection.SortClauses=sortClauses;
			_siteCultureCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch)
		{
			return GetMultiSiteLanguageCollection(forceFetch, _siteLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteLanguageCollection(forceFetch, _siteLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteLanguageCollection || forceFetch || _alwaysFetchSiteLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteLanguageCollection);
				_siteLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_siteLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteLanguageCollection.GetMultiManyToOne(null, this, filter);
				_siteLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteLanguageCollection = true;
			}
			return _siteLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteLanguageCollection'. These settings will be taken into account
		/// when the property SiteLanguageCollection is requested or GetMultiSiteLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteLanguageCollection.SortClauses=sortClauses;
			_siteLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUITabCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUITabCollection || forceFetch || _alwaysFetchUITabCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabCollection);
				_uITabCollection.SuppressClearInGetMulti=!forceFetch;
				_uITabCollection.EntityFactoryToUse = entityFactoryToUse;
				_uITabCollection.GetMultiManyToOne(null, null, null, this, null, filter);
				_uITabCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabCollection = true;
			}
			return _uITabCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabCollection'. These settings will be taken into account
		/// when the property UITabCollection is requested or GetMultiUITabCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabCollection.SortClauses=sortClauses;
			_uITabCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public SiteTemplateEntity GetSingleSiteTemplateEntity()
		{
			return GetSingleSiteTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public virtual SiteTemplateEntity GetSingleSiteTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteTemplateEntity || forceFetch || _alwaysFetchSiteTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteTemplateEntityUsingSiteTemplateId);
				SiteTemplateEntity newEntity = new SiteTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteTemplateEntity = newEntity;
				_alreadyFetchedSiteTemplateEntity = fetchResult;
			}
			return _siteTemplateEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingSiteId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCSiteId(this.SiteId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("SiteTemplateEntity", _siteTemplateEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AvailabilityCollection", _availabilityCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("EntertainmentCollection", _entertainmentCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("MediaCollection_", _mediaCollection_);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageTemplateCollection", _messageTemplateCollection);
			toReturn.Add("PageCollection", _pageCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			toReturn.Add("SiteCultureCollection", _siteCultureCollection);
			toReturn.Add("SiteLanguageCollection", _siteLanguageCollection);
			toReturn.Add("UITabCollection", _uITabCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="validator">The validator object for this SiteEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 siteId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(siteId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "SiteEntity");

			_availabilityCollection = new Obymobi.Data.CollectionClasses.AvailabilityCollection();
			_availabilityCollection.SetContainingEntityInfo(this, "SiteEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "SiteEntity");

			_entertainmentCollection = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollection.SetContainingEntityInfo(this, "SiteEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "SiteEntity");

			_mediaCollection_ = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection_.SetContainingEntityInfo(this, "SiteEntity_");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "SiteEntity");

			_messageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCollection();
			_messageTemplateCollection.SetContainingEntityInfo(this, "SiteEntity");

			_pageCollection = new Obymobi.Data.CollectionClasses.PageCollection();
			_pageCollection.SetContainingEntityInfo(this, "SiteEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "SiteEntity");

			_siteCultureCollection = new Obymobi.Data.CollectionClasses.SiteCultureCollection();
			_siteCultureCollection.SetContainingEntityInfo(this, "SiteEntity");

			_siteLanguageCollection = new Obymobi.Data.CollectionClasses.SiteLanguageCollection();
			_siteLanguageCollection.SetContainingEntityInfo(this, "SiteEntity");

			_uITabCollection = new Obymobi.Data.CollectionClasses.UITabCollection();
			_uITabCollection.SetContainingEntityInfo(this, "SiteEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "SiteEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_siteTemplateEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifiedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticSiteRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "SiteCollection", resetFKFields, new int[] { (int)SiteFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticSiteRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticSiteRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, signalRelatedEntity, "SiteCollection", resetFKFields, new int[] { (int)SiteFieldIndex.SiteTemplateId } );		
			_siteTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteTemplateEntity(IEntityCore relatedEntity)
		{
			if(_siteTemplateEntity!=relatedEntity)
			{		
				DesetupSyncSiteTemplateEntity(true, true);
				_siteTemplateEntity = (SiteTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticSiteRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, ref _alreadyFetchedSiteTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticSiteRelations.TimestampEntityUsingSiteIdStatic, false, signalRelatedEntity, "SiteEntity", false, new int[] { (int)SiteFieldIndex.SiteId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticSiteRelations.TimestampEntityUsingSiteIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 siteId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SiteFieldIndex.SiteId].ForcedCurrentValueWrite(siteId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSiteDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SiteEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SiteRelations Relations
		{
			get	{ return new SiteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection_")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.MessageTemplateEntity, 0, null, null, null, "MessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteCulture' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteCultureCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCultureCollection(), (IEntityRelation)GetRelationsForField("SiteCultureCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.SiteCultureEntity, 0, null, null, null, "SiteCultureCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteLanguageCollection(), (IEntityRelation)GetRelationsForField("SiteLanguageCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.SiteLanguageEntity, 0, null, null, null, "SiteLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("UITabCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "UITabCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateEntity")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.SiteTemplateEntity, 0, null, null, null, "SiteTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.SiteEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SiteId property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SiteId
		{
			get { return (System.Int32)GetValue((int)SiteFieldIndex.SiteId, true); }
			set	{ SetValue((int)SiteFieldIndex.SiteId, value, true); }
		}

		/// <summary> The Name property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SiteFieldIndex.Name, true); }
			set	{ SetValue((int)SiteFieldIndex.Name, value, true); }
		}

		/// <summary> The SiteType property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."SiteType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteType
		{
			get { return (System.Int32)GetValue((int)SiteFieldIndex.SiteType, true); }
			set	{ SetValue((int)SiteFieldIndex.SiteType, value, true); }
		}

		/// <summary> The SiteTemplateId property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."SiteTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SiteFieldIndex.SiteTemplateId, false); }
			set	{ SetValue((int)SiteFieldIndex.SiteTemplateId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SiteFieldIndex.CompanyId, false); }
			set	{ SetValue((int)SiteFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SiteFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SiteFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SiteFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SiteFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Description property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SiteFieldIndex.Description, true); }
			set	{ SetValue((int)SiteFieldIndex.Description, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SiteFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SiteFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastModifiedUTC property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."LastModifiedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)SiteFieldIndex.LastModifiedUTC, true); }
			set	{ SetValue((int)SiteFieldIndex.LastModifiedUTC, value, true); }
		}

		/// <summary> The Version property of the Entity Site<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Site"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)SiteFieldIndex.Version, true); }
			set	{ SetValue((int)SiteFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAvailabilityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection AvailabilityCollection
		{
			get	{ return GetMultiAvailabilityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityCollection. When set to true, AvailabilityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAvailabilityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityCollection
		{
			get	{ return _alwaysFetchAvailabilityCollection; }
			set	{ _alwaysFetchAvailabilityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityCollection already has been fetched. Setting this property to false when AvailabilityCollection has been fetched
		/// will clear the AvailabilityCollection collection well. Setting this property to true while AvailabilityCollection hasn't been fetched disables lazy loading for AvailabilityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityCollection
		{
			get { return _alreadyFetchedAvailabilityCollection;}
			set 
			{
				if(_alreadyFetchedAvailabilityCollection && !value && (_availabilityCollection != null))
				{
					_availabilityCollection.Clear();
				}
				_alreadyFetchedAvailabilityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollection
		{
			get	{ return GetMultiEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollection. When set to true, EntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollection
		{
			get	{ return _alwaysFetchEntertainmentCollection; }
			set	{ _alwaysFetchEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollection already has been fetched. Setting this property to false when EntertainmentCollection has been fetched
		/// will clear the EntertainmentCollection collection well. Setting this property to true while EntertainmentCollection hasn't been fetched disables lazy loading for EntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollection
		{
			get { return _alreadyFetchedEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollection && !value && (_entertainmentCollection != null))
				{
					_entertainmentCollection.Clear();
				}
				_alreadyFetchedEntertainmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection_
		{
			get	{ return GetMultiMediaCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection_. When set to true, MediaCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection_
		{
			get	{ return _alwaysFetchMediaCollection_; }
			set	{ _alwaysFetchMediaCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection_ already has been fetched. Setting this property to false when MediaCollection_ has been fetched
		/// will clear the MediaCollection_ collection well. Setting this property to true while MediaCollection_ hasn't been fetched disables lazy loading for MediaCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection_
		{
			get { return _alreadyFetchedMediaCollection_;}
			set 
			{
				if(_alreadyFetchedMediaCollection_ && !value && (_mediaCollection_ != null))
				{
					_mediaCollection_.Clear();
				}
				_alreadyFetchedMediaCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection MessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCollection. When set to true, MessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCollection collection well. Setting this property to true while MessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCollection && !value && (_messageTemplateCollection != null))
				{
					_messageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageCollection PageCollection
		{
			get	{ return GetMultiPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageCollection. When set to true, PageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageCollection
		{
			get	{ return _alwaysFetchPageCollection; }
			set	{ _alwaysFetchPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageCollection already has been fetched. Setting this property to false when PageCollection has been fetched
		/// will clear the PageCollection collection well. Setting this property to true while PageCollection hasn't been fetched disables lazy loading for PageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageCollection
		{
			get { return _alreadyFetchedPageCollection;}
			set 
			{
				if(_alreadyFetchedPageCollection && !value && (_pageCollection != null))
				{
					_pageCollection.Clear();
				}
				_alreadyFetchedPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteCultureEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteCultureCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteCultureCollection SiteCultureCollection
		{
			get	{ return GetMultiSiteCultureCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteCultureCollection. When set to true, SiteCultureCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteCultureCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteCultureCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteCultureCollection
		{
			get	{ return _alwaysFetchSiteCultureCollection; }
			set	{ _alwaysFetchSiteCultureCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteCultureCollection already has been fetched. Setting this property to false when SiteCultureCollection has been fetched
		/// will clear the SiteCultureCollection collection well. Setting this property to true while SiteCultureCollection hasn't been fetched disables lazy loading for SiteCultureCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteCultureCollection
		{
			get { return _alreadyFetchedSiteCultureCollection;}
			set 
			{
				if(_alreadyFetchedSiteCultureCollection && !value && (_siteCultureCollection != null))
				{
					_siteCultureCollection.Clear();
				}
				_alreadyFetchedSiteCultureCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteLanguageCollection SiteLanguageCollection
		{
			get	{ return GetMultiSiteLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteLanguageCollection. When set to true, SiteLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteLanguageCollection
		{
			get	{ return _alwaysFetchSiteLanguageCollection; }
			set	{ _alwaysFetchSiteLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteLanguageCollection already has been fetched. Setting this property to false when SiteLanguageCollection has been fetched
		/// will clear the SiteLanguageCollection collection well. Setting this property to true while SiteLanguageCollection hasn't been fetched disables lazy loading for SiteLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteLanguageCollection
		{
			get { return _alreadyFetchedSiteLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSiteLanguageCollection && !value && (_siteLanguageCollection != null))
				{
					_siteLanguageCollection.Clear();
				}
				_alreadyFetchedSiteLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection UITabCollection
		{
			get	{ return GetMultiUITabCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabCollection. When set to true, UITabCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUITabCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabCollection
		{
			get	{ return _alwaysFetchUITabCollection; }
			set	{ _alwaysFetchUITabCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabCollection already has been fetched. Setting this property to false when UITabCollection has been fetched
		/// will clear the UITabCollection collection well. Setting this property to true while UITabCollection hasn't been fetched disables lazy loading for UITabCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabCollection
		{
			get { return _alreadyFetchedUITabCollection;}
			set 
			{
				if(_alreadyFetchedUITabCollection && !value && (_uITabCollection != null))
				{
					_uITabCollection.Clear();
				}
				_alreadyFetchedUITabCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteTemplateEntity SiteTemplateEntity
		{
			get	{ return GetSingleSiteTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SiteCollection", "SiteTemplateEntity", _siteTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateEntity. When set to true, SiteTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateEntity
		{
			get	{ return _alwaysFetchSiteTemplateEntity; }
			set	{ _alwaysFetchSiteTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateEntity already has been fetched. Setting this property to false when SiteTemplateEntity has been fetched
		/// will set SiteTemplateEntity to null as well. Setting this property to true while SiteTemplateEntity hasn't been fetched disables lazy loading for SiteTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateEntity
		{
			get { return _alreadyFetchedSiteTemplateEntity;}
			set 
			{
				if(_alreadyFetchedSiteTemplateEntity && !value)
				{
					this.SiteTemplateEntity = null;
				}
				_alreadyFetchedSiteTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteTemplateEntity is not found
		/// in the database. When set to true, SiteTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _siteTemplateEntityReturnsNewIfNotFound; }
			set { _siteTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "SiteEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SiteEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
