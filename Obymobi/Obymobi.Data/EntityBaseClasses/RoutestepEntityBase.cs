﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Routestep'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoutestepEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoutestepEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.RoutestephandlerCollection	_routestephandlerCollection;
		private bool	_alwaysFetchRoutestephandlerCollection, _alreadyFetchedRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaRoutestephandler;
		private bool	_alwaysFetchSupportpoolCollectionViaRoutestephandler, _alreadyFetchedSupportpoolCollectionViaRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaRoutestephandler;
		private bool	_alwaysFetchTerminalCollectionViaRoutestephandler, _alreadyFetchedTerminalCollectionViaRoutestephandler;
		private RouteEntity _routeEntity;
		private bool	_alwaysFetchRouteEntity, _alreadyFetchedRouteEntity, _routeEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RouteEntity</summary>
			public static readonly string RouteEntity = "RouteEntity";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
			/// <summary>Member name SupportpoolCollectionViaRoutestephandler</summary>
			public static readonly string SupportpoolCollectionViaRoutestephandler = "SupportpoolCollectionViaRoutestephandler";
			/// <summary>Member name TerminalCollectionViaRoutestephandler</summary>
			public static readonly string TerminalCollectionViaRoutestephandler = "TerminalCollectionViaRoutestephandler";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoutestepEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoutestepEntityBase() :base("RoutestepEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		protected RoutestepEntityBase(System.Int32 routestepId):base("RoutestepEntity")
		{
			InitClassFetch(routestepId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoutestepEntityBase(System.Int32 routestepId, IPrefetchPath prefetchPathToUse): base("RoutestepEntity")
		{
			InitClassFetch(routestepId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="validator">The custom validator object for this RoutestepEntity</param>
		protected RoutestepEntityBase(System.Int32 routestepId, IValidator validator):base("RoutestepEntity")
		{
			InitClassFetch(routestepId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoutestepEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_routestephandlerCollection = (Obymobi.Data.CollectionClasses.RoutestephandlerCollection)info.GetValue("_routestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.RoutestephandlerCollection));
			_alwaysFetchRoutestephandlerCollection = info.GetBoolean("_alwaysFetchRoutestephandlerCollection");
			_alreadyFetchedRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedRoutestephandlerCollection");
			_supportpoolCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaRoutestephandler");
			_alreadyFetchedSupportpoolCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaRoutestephandler");

			_terminalCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchTerminalCollectionViaRoutestephandler");
			_alreadyFetchedTerminalCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedTerminalCollectionViaRoutestephandler");
			_routeEntity = (RouteEntity)info.GetValue("_routeEntity", typeof(RouteEntity));
			if(_routeEntity!=null)
			{
				_routeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeEntityReturnsNewIfNotFound = info.GetBoolean("_routeEntityReturnsNewIfNotFound");
			_alwaysFetchRouteEntity = info.GetBoolean("_alwaysFetchRouteEntity");
			_alreadyFetchedRouteEntity = info.GetBoolean("_alreadyFetchedRouteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoutestepFieldIndex)fieldIndex)
			{
				case RoutestepFieldIndex.RouteId:
					DesetupSyncRouteEntity(true, false);
					_alreadyFetchedRouteEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRoutestephandlerCollection = (_routestephandlerCollection.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaRoutestephandler = (_supportpoolCollectionViaRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaRoutestephandler = (_terminalCollectionViaRoutestephandler.Count > 0);
			_alreadyFetchedRouteEntity = (_routeEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RouteEntity":
					toReturn.Add(Relations.RouteEntityUsingRouteId);
					break;
				case "RoutestephandlerCollection":
					toReturn.Add(Relations.RoutestephandlerEntityUsingRoutestepId);
					break;
				case "SupportpoolCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingRoutestepId, "RoutestepEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingRoutestepId, "RoutestepEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.TerminalEntityUsingTerminalId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_routestephandlerCollection", (!this.MarkedForDeletion?_routestephandlerCollection:null));
			info.AddValue("_alwaysFetchRoutestephandlerCollection", _alwaysFetchRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedRoutestephandlerCollection", _alreadyFetchedRoutestephandlerCollection);
			info.AddValue("_supportpoolCollectionViaRoutestephandler", (!this.MarkedForDeletion?_supportpoolCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaRoutestephandler", _alwaysFetchSupportpoolCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaRoutestephandler", _alreadyFetchedSupportpoolCollectionViaRoutestephandler);
			info.AddValue("_terminalCollectionViaRoutestephandler", (!this.MarkedForDeletion?_terminalCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaRoutestephandler", _alwaysFetchTerminalCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedTerminalCollectionViaRoutestephandler", _alreadyFetchedTerminalCollectionViaRoutestephandler);
			info.AddValue("_routeEntity", (!this.MarkedForDeletion?_routeEntity:null));
			info.AddValue("_routeEntityReturnsNewIfNotFound", _routeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteEntity", _alwaysFetchRouteEntity);
			info.AddValue("_alreadyFetchedRouteEntity", _alreadyFetchedRouteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RouteEntity":
					_alreadyFetchedRouteEntity = true;
					this.RouteEntity = (RouteEntity)entity;
					break;
				case "RoutestephandlerCollection":
					_alreadyFetchedRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.RoutestephandlerCollection.Add((RoutestephandlerEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaRoutestephandler":
					_alreadyFetchedSupportpoolCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaRoutestephandler.Add((SupportpoolEntity)entity);
					}
					break;
				case "TerminalCollectionViaRoutestephandler":
					_alreadyFetchedTerminalCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaRoutestephandler.Add((TerminalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RouteEntity":
					SetupSyncRouteEntity(relatedEntity);
					break;
				case "RoutestephandlerCollection":
					_routestephandlerCollection.Add((RoutestephandlerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RouteEntity":
					DesetupSyncRouteEntity(false, true);
					break;
				case "RoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_routestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_routeEntity!=null)
			{
				toReturn.Add(_routeEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_routestephandlerCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routestepId)
		{
			return FetchUsingPK(routestepId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routestepId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(routestepId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routestepId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(routestepId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routestepId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(routestepId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoutestepId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoutestepRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutestephandlerCollection || forceFetch || _alwaysFetchRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestephandlerCollection);
				_routestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_routestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_routestephandlerCollection.GetMultiManyToOne(null, this, null, null, filter);
				_routestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestephandlerCollection = true;
			}
			return _routestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestephandlerCollection'. These settings will be taken into account
		/// when the property RoutestephandlerCollection is requested or GetMultiRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestephandlerCollection.SortClauses=sortClauses;
			_routestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaRoutestephandler(forceFetch, _supportpoolCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaRoutestephandler || forceFetch || _alwaysFetchSupportpoolCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RoutestepFields.RoutestepId, ComparisonOperator.Equal, this.RoutestepId, "RoutestepEntity__"));
				_supportpoolCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaRoutestephandler"));
				_supportpoolCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaRoutestephandler = true;
			}
			return _supportpoolCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaRoutestephandler is requested or GetMultiSupportpoolCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaRoutestephandler.SortClauses=sortClauses;
			_supportpoolCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaRoutestephandler(forceFetch, _terminalCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaRoutestephandler || forceFetch || _alwaysFetchTerminalCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RoutestepFields.RoutestepId, ComparisonOperator.Equal, this.RoutestepId, "RoutestepEntity__"));
				_terminalCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("TerminalCollectionViaRoutestephandler"));
				_terminalCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaRoutestephandler = true;
			}
			return _terminalCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property TerminalCollectionViaRoutestephandler is requested or GetMultiTerminalCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaRoutestephandler.SortClauses=sortClauses;
			_terminalCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleRouteEntity()
		{
			return GetSingleRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteEntity || forceFetch || _alwaysFetchRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteId);
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteEntity = newEntity;
				_alreadyFetchedRouteEntity = fetchResult;
			}
			return _routeEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RouteEntity", _routeEntity);
			toReturn.Add("RoutestephandlerCollection", _routestephandlerCollection);
			toReturn.Add("SupportpoolCollectionViaRoutestephandler", _supportpoolCollectionViaRoutestephandler);
			toReturn.Add("TerminalCollectionViaRoutestephandler", _terminalCollectionViaRoutestephandler);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="validator">The validator object for this RoutestepEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 routestepId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(routestepId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_routestephandlerCollection = new Obymobi.Data.CollectionClasses.RoutestephandlerCollection();
			_routestephandlerCollection.SetContainingEntityInfo(this, "RoutestepEntity");
			_supportpoolCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_terminalCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_routeEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoutestepId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContinueOnFailure", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompleteRouteOnComplete", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _routeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticRoutestepRelations.RouteEntityUsingRouteIdStatic, true, signalRelatedEntity, "RoutestepCollection", resetFKFields, new int[] { (int)RoutestepFieldIndex.RouteId } );		
			_routeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteEntity(IEntityCore relatedEntity)
		{
			if(_routeEntity!=relatedEntity)
			{		
				DesetupSyncRouteEntity(true, true);
				_routeEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticRoutestepRelations.RouteEntityUsingRouteIdStatic, true, ref _alreadyFetchedRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 routestepId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoutestepFieldIndex.RoutestepId].ForcedCurrentValueWrite(routestepId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoutestepDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoutestepEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoutestepRelations Relations
		{
			get	{ return new RoutestepRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.RoutestepEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingRoutestepId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RoutestepEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaRoutestephandler"), "SupportpoolCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingRoutestepId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RoutestepEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaRoutestephandler"), "TerminalCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RouteEntity")[0], (int)Obymobi.Data.EntityType.RoutestepEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "RouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoutestepId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."RoutestepId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoutestepId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.RoutestepId, true); }
			set	{ SetValue((int)RoutestepFieldIndex.RoutestepId, value, true); }
		}

		/// <summary> The RouteId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."RouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.RouteId, true); }
			set	{ SetValue((int)RoutestepFieldIndex.RouteId, value, true); }
		}

		/// <summary> The Number property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."Number"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.Number, true); }
			set	{ SetValue((int)RoutestepFieldIndex.Number, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)RoutestepFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)RoutestepFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The TimeoutMinutes property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."TimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoutestepFieldIndex.TimeoutMinutes, false); }
			set	{ SetValue((int)RoutestepFieldIndex.TimeoutMinutes, value, true); }
		}

		/// <summary> The ContinueOnFailure property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."ContinueOnFailure"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContinueOnFailure
		{
			get { return (System.Boolean)GetValue((int)RoutestepFieldIndex.ContinueOnFailure, true); }
			set	{ SetValue((int)RoutestepFieldIndex.ContinueOnFailure, value, true); }
		}

		/// <summary> The CompleteRouteOnComplete property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CompleteRouteOnComplete"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompleteRouteOnComplete
		{
			get { return (System.Boolean)GetValue((int)RoutestepFieldIndex.CompleteRouteOnComplete, true); }
			set	{ SetValue((int)RoutestepFieldIndex.CompleteRouteOnComplete, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)RoutestepFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)RoutestepFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestepFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoutestepFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Routestep<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Routestep"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoutestepFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoutestepFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection RoutestephandlerCollection
		{
			get	{ return GetMultiRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerCollection. When set to true, RoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerCollection
		{
			get	{ return _alwaysFetchRoutestephandlerCollection; }
			set	{ _alwaysFetchRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerCollection already has been fetched. Setting this property to false when RoutestephandlerCollection has been fetched
		/// will clear the RoutestephandlerCollection collection well. Setting this property to true while RoutestephandlerCollection hasn't been fetched disables lazy loading for RoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerCollection
		{
			get { return _alreadyFetchedRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerCollection && !value && (_routestephandlerCollection != null))
				{
					_routestephandlerCollection.Clear();
				}
				_alreadyFetchedRoutestephandlerCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaRoutestephandler
		{
			get { return GetMultiSupportpoolCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaRoutestephandler. When set to true, SupportpoolCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaRoutestephandler; }
			set	{ _alwaysFetchSupportpoolCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaRoutestephandler already has been fetched. Setting this property to false when SupportpoolCollectionViaRoutestephandler has been fetched
		/// will clear the SupportpoolCollectionViaRoutestephandler collection well. Setting this property to true while SupportpoolCollectionViaRoutestephandler hasn't been fetched disables lazy loading for SupportpoolCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedSupportpoolCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaRoutestephandler && !value && (_supportpoolCollectionViaRoutestephandler != null))
				{
					_supportpoolCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaRoutestephandler
		{
			get { return GetMultiTerminalCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaRoutestephandler. When set to true, TerminalCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchTerminalCollectionViaRoutestephandler; }
			set	{ _alwaysFetchTerminalCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaRoutestephandler already has been fetched. Setting this property to false when TerminalCollectionViaRoutestephandler has been fetched
		/// will clear the TerminalCollectionViaRoutestephandler collection well. Setting this property to true while TerminalCollectionViaRoutestephandler hasn't been fetched disables lazy loading for TerminalCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedTerminalCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaRoutestephandler && !value && (_terminalCollectionViaRoutestephandler != null))
				{
					_terminalCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedTerminalCollectionViaRoutestephandler = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity RouteEntity
		{
			get	{ return GetSingleRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoutestepCollection", "RouteEntity", _routeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteEntity. When set to true, RouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteEntity is accessed. You can always execute a forced fetch by calling GetSingleRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteEntity
		{
			get	{ return _alwaysFetchRouteEntity; }
			set	{ _alwaysFetchRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteEntity already has been fetched. Setting this property to false when RouteEntity has been fetched
		/// will set RouteEntity to null as well. Setting this property to true while RouteEntity hasn't been fetched disables lazy loading for RouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteEntity
		{
			get { return _alreadyFetchedRouteEntity;}
			set 
			{
				if(_alreadyFetchedRouteEntity && !value)
				{
					this.RouteEntity = null;
				}
				_alreadyFetchedRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteEntity is not found
		/// in the database. When set to true, RouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RouteEntityReturnsNewIfNotFound
		{
			get	{ return _routeEntityReturnsNewIfNotFound; }
			set { _routeEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoutestepEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
