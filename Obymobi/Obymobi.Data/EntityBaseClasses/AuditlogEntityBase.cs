﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Auditlog'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AuditlogEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AuditlogEntity"; }
		}
	
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AuditlogEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AuditlogEntityBase() :base("AuditlogEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		protected AuditlogEntityBase(System.Int32 auditLogId):base("AuditlogEntity")
		{
			InitClassFetch(auditLogId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AuditlogEntityBase(System.Int32 auditLogId, IPrefetchPath prefetchPathToUse): base("AuditlogEntity")
		{
			InitClassFetch(auditLogId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="validator">The custom validator object for this AuditlogEntity</param>
		protected AuditlogEntityBase(System.Int32 auditLogId, IValidator validator):base("AuditlogEntity")
		{
			InitClassFetch(auditLogId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AuditlogEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 auditLogId)
		{
			return FetchUsingPK(auditLogId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 auditLogId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(auditLogId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 auditLogId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(auditLogId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 auditLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(auditLogId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AuditLogId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AuditlogRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="validator">The validator object for this AuditlogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 auditLogId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(auditLogId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuditLogId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName0", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue0", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName11", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue11", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName12", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue12", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName13", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue13", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName14", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue14", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName15", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue15", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName16", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue16", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName17", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue17", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName18", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue18", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName19", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue19", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName20", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue20", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldsAndValues", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CallStack", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Page", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BreadcrumbSteps", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionPerformed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionPerformedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="auditLogId">PK value for Auditlog which data should be fetched into this Auditlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 auditLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AuditlogFieldIndex.AuditLogId].ForcedCurrentValueWrite(auditLogId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAuditlogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AuditlogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AuditlogRelations Relations
		{
			get	{ return new AuditlogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AuditLogId property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."AuditLogId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AuditLogId
		{
			get { return (System.Int32)GetValue((int)AuditlogFieldIndex.AuditLogId, true); }
			set	{ SetValue((int)AuditlogFieldIndex.AuditLogId, value, true); }
		}

		/// <summary> The ActionType property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."ActionType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionType
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.ActionType, true); }
			set	{ SetValue((int)AuditlogFieldIndex.ActionType, value, true); }
		}

		/// <summary> The EntityType property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."EntityType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EntityType
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.EntityType, true); }
			set	{ SetValue((int)AuditlogFieldIndex.EntityType, value, true); }
		}

		/// <summary> The EntityId property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."EntityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntityId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AuditlogFieldIndex.EntityId, false); }
			set	{ SetValue((int)AuditlogFieldIndex.EntityId, value, true); }
		}

		/// <summary> The ShowName property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."ShowName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShowName
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.ShowName, true); }
			set	{ SetValue((int)AuditlogFieldIndex.ShowName, value, true); }
		}

		/// <summary> The FieldName0 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName0"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName0
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName0, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName0, value, true); }
		}

		/// <summary> The FieldValue0 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue0"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue0
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue0, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue0, value, true); }
		}

		/// <summary> The FieldName1 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName1
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName1, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName1, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldName2 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName2
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName2, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName2, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldName3 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName3
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName3, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName3, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldName4 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName4
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName4, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName4, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldName5 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName5
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName5, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName5, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldName6 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName6
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName6, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName6, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldName7 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName7
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName7, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName7, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldName8 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName8
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName8, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName8, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldName9 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName9
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName9, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName9, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldName10 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName10
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName10, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName10, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The FieldName11 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName11"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName11
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName11, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName11, value, true); }
		}

		/// <summary> The FieldValue11 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue11"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue11
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue11, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue11, value, true); }
		}

		/// <summary> The FieldName12 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName12"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName12
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName12, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName12, value, true); }
		}

		/// <summary> The FieldValue12 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue12"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue12
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue12, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue12, value, true); }
		}

		/// <summary> The FieldName13 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName13"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName13
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName13, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName13, value, true); }
		}

		/// <summary> The FieldValue13 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue13"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue13
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue13, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue13, value, true); }
		}

		/// <summary> The FieldName14 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName14"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName14
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName14, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName14, value, true); }
		}

		/// <summary> The FieldValue14 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue14"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue14
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue14, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue14, value, true); }
		}

		/// <summary> The FieldName15 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName15"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName15
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName15, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName15, value, true); }
		}

		/// <summary> The FieldValue15 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue15"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue15
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue15, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue15, value, true); }
		}

		/// <summary> The FieldName16 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName16"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName16
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName16, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName16, value, true); }
		}

		/// <summary> The FieldValue16 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue16"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue16
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue16, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue16, value, true); }
		}

		/// <summary> The FieldName17 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName17"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName17
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName17, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName17, value, true); }
		}

		/// <summary> The FieldValue17 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue17"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue17
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue17, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue17, value, true); }
		}

		/// <summary> The FieldName18 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName18"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName18
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName18, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName18, value, true); }
		}

		/// <summary> The FieldValue18 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue18"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue18
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue18, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue18, value, true); }
		}

		/// <summary> The FieldName19 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName19"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName19
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName19, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName19, value, true); }
		}

		/// <summary> The FieldValue19 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue19"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue19
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue19, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue19, value, true); }
		}

		/// <summary> The FieldName20 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldName20"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName20
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldName20, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldName20, value, true); }
		}

		/// <summary> The FieldValue20 property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldValue20"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue20
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldValue20, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldValue20, value, true); }
		}

		/// <summary> The FieldsAndValues property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."FieldsAndValues"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldsAndValues
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.FieldsAndValues, true); }
			set	{ SetValue((int)AuditlogFieldIndex.FieldsAndValues, value, true); }
		}

		/// <summary> The CallStack property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."CallStack"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CallStack
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.CallStack, true); }
			set	{ SetValue((int)AuditlogFieldIndex.CallStack, value, true); }
		}

		/// <summary> The Page property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."Page"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Page
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.Page, true); }
			set	{ SetValue((int)AuditlogFieldIndex.Page, value, true); }
		}

		/// <summary> The BreadcrumbSteps property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."BreadcrumbSteps"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BreadcrumbSteps
		{
			get { return (System.String)GetValue((int)AuditlogFieldIndex.BreadcrumbSteps, true); }
			set	{ SetValue((int)AuditlogFieldIndex.BreadcrumbSteps, value, true); }
		}

		/// <summary> The ActionBy property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."ActionBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AuditlogFieldIndex.ActionBy, false); }
			set	{ SetValue((int)AuditlogFieldIndex.ActionBy, value, true); }
		}

		/// <summary> The ActionPerformed property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."ActionPerformed"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ActionPerformed
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AuditlogFieldIndex.ActionPerformed, false); }
			set	{ SetValue((int)AuditlogFieldIndex.ActionPerformed, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AuditlogFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AuditlogFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AuditlogFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AuditlogFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AuditlogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AuditlogFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AuditlogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AuditlogFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ActionPerformedUTC property of the Entity Auditlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Auditlog"."ActionPerformedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ActionPerformedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AuditlogFieldIndex.ActionPerformedUTC, false); }
			set	{ SetValue((int)AuditlogFieldIndex.ActionPerformedUTC, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AuditlogEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
