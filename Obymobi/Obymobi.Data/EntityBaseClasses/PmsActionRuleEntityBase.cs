﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PmsActionRule'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PmsActionRuleEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PmsActionRuleEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PmsRuleCollection	_pmsRuleCollection;
		private bool	_alwaysFetchPmsRuleCollection, _alreadyFetchedPmsRuleCollection;
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private MessagegroupEntity _messagegroupEntity;
		private bool	_alwaysFetchMessagegroupEntity, _alreadyFetchedMessagegroupEntity, _messagegroupEntityReturnsNewIfNotFound;
		private PmsReportConfigurationEntity _pmsReportConfigurationEntity;
		private bool	_alwaysFetchPmsReportConfigurationEntity, _alreadyFetchedPmsReportConfigurationEntity, _pmsReportConfigurationEntityReturnsNewIfNotFound;
		private ScheduledMessageEntity _scheduledMessageEntity;
		private bool	_alwaysFetchScheduledMessageEntity, _alreadyFetchedScheduledMessageEntity, _scheduledMessageEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name MessagegroupEntity</summary>
			public static readonly string MessagegroupEntity = "MessagegroupEntity";
			/// <summary>Member name PmsReportConfigurationEntity</summary>
			public static readonly string PmsReportConfigurationEntity = "PmsReportConfigurationEntity";
			/// <summary>Member name ScheduledMessageEntity</summary>
			public static readonly string ScheduledMessageEntity = "ScheduledMessageEntity";
			/// <summary>Member name PmsRuleCollection</summary>
			public static readonly string PmsRuleCollection = "PmsRuleCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PmsActionRuleEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PmsActionRuleEntityBase() :base("PmsActionRuleEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		protected PmsActionRuleEntityBase(System.Int32 pmsActionRuleId):base("PmsActionRuleEntity")
		{
			InitClassFetch(pmsActionRuleId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PmsActionRuleEntityBase(System.Int32 pmsActionRuleId, IPrefetchPath prefetchPathToUse): base("PmsActionRuleEntity")
		{
			InitClassFetch(pmsActionRuleId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="validator">The custom validator object for this PmsActionRuleEntity</param>
		protected PmsActionRuleEntityBase(System.Int32 pmsActionRuleId, IValidator validator):base("PmsActionRuleEntity")
		{
			InitClassFetch(pmsActionRuleId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PmsActionRuleEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pmsRuleCollection = (Obymobi.Data.CollectionClasses.PmsRuleCollection)info.GetValue("_pmsRuleCollection", typeof(Obymobi.Data.CollectionClasses.PmsRuleCollection));
			_alwaysFetchPmsRuleCollection = info.GetBoolean("_alwaysFetchPmsRuleCollection");
			_alreadyFetchedPmsRuleCollection = info.GetBoolean("_alreadyFetchedPmsRuleCollection");
			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_messagegroupEntity = (MessagegroupEntity)info.GetValue("_messagegroupEntity", typeof(MessagegroupEntity));
			if(_messagegroupEntity!=null)
			{
				_messagegroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messagegroupEntityReturnsNewIfNotFound = info.GetBoolean("_messagegroupEntityReturnsNewIfNotFound");
			_alwaysFetchMessagegroupEntity = info.GetBoolean("_alwaysFetchMessagegroupEntity");
			_alreadyFetchedMessagegroupEntity = info.GetBoolean("_alreadyFetchedMessagegroupEntity");

			_pmsReportConfigurationEntity = (PmsReportConfigurationEntity)info.GetValue("_pmsReportConfigurationEntity", typeof(PmsReportConfigurationEntity));
			if(_pmsReportConfigurationEntity!=null)
			{
				_pmsReportConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pmsReportConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_pmsReportConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchPmsReportConfigurationEntity = info.GetBoolean("_alwaysFetchPmsReportConfigurationEntity");
			_alreadyFetchedPmsReportConfigurationEntity = info.GetBoolean("_alreadyFetchedPmsReportConfigurationEntity");

			_scheduledMessageEntity = (ScheduledMessageEntity)info.GetValue("_scheduledMessageEntity", typeof(ScheduledMessageEntity));
			if(_scheduledMessageEntity!=null)
			{
				_scheduledMessageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduledMessageEntityReturnsNewIfNotFound = info.GetBoolean("_scheduledMessageEntityReturnsNewIfNotFound");
			_alwaysFetchScheduledMessageEntity = info.GetBoolean("_alwaysFetchScheduledMessageEntity");
			_alreadyFetchedScheduledMessageEntity = info.GetBoolean("_alreadyFetchedScheduledMessageEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PmsActionRuleFieldIndex)fieldIndex)
			{
				case PmsActionRuleFieldIndex.PmsReportConfigurationId:
					DesetupSyncPmsReportConfigurationEntity(true, false);
					_alreadyFetchedPmsReportConfigurationEntity = false;
					break;
				case PmsActionRuleFieldIndex.ScheduledMessageId:
					DesetupSyncScheduledMessageEntity(true, false);
					_alreadyFetchedScheduledMessageEntity = false;
					break;
				case PmsActionRuleFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case PmsActionRuleFieldIndex.MessagegroupId:
					DesetupSyncMessagegroupEntity(true, false);
					_alreadyFetchedMessagegroupEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPmsRuleCollection = (_pmsRuleCollection.Count > 0);
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedMessagegroupEntity = (_messagegroupEntity != null);
			_alreadyFetchedPmsReportConfigurationEntity = (_pmsReportConfigurationEntity != null);
			_alreadyFetchedScheduledMessageEntity = (_scheduledMessageEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "MessagegroupEntity":
					toReturn.Add(Relations.MessagegroupEntityUsingMessagegroupId);
					break;
				case "PmsReportConfigurationEntity":
					toReturn.Add(Relations.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
					break;
				case "ScheduledMessageEntity":
					toReturn.Add(Relations.ScheduledMessageEntityUsingScheduledMessageId);
					break;
				case "PmsRuleCollection":
					toReturn.Add(Relations.PmsRuleEntityUsingPmsActionRuleId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pmsRuleCollection", (!this.MarkedForDeletion?_pmsRuleCollection:null));
			info.AddValue("_alwaysFetchPmsRuleCollection", _alwaysFetchPmsRuleCollection);
			info.AddValue("_alreadyFetchedPmsRuleCollection", _alreadyFetchedPmsRuleCollection);
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_messagegroupEntity", (!this.MarkedForDeletion?_messagegroupEntity:null));
			info.AddValue("_messagegroupEntityReturnsNewIfNotFound", _messagegroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessagegroupEntity", _alwaysFetchMessagegroupEntity);
			info.AddValue("_alreadyFetchedMessagegroupEntity", _alreadyFetchedMessagegroupEntity);
			info.AddValue("_pmsReportConfigurationEntity", (!this.MarkedForDeletion?_pmsReportConfigurationEntity:null));
			info.AddValue("_pmsReportConfigurationEntityReturnsNewIfNotFound", _pmsReportConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPmsReportConfigurationEntity", _alwaysFetchPmsReportConfigurationEntity);
			info.AddValue("_alreadyFetchedPmsReportConfigurationEntity", _alreadyFetchedPmsReportConfigurationEntity);
			info.AddValue("_scheduledMessageEntity", (!this.MarkedForDeletion?_scheduledMessageEntity:null));
			info.AddValue("_scheduledMessageEntityReturnsNewIfNotFound", _scheduledMessageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduledMessageEntity", _alwaysFetchScheduledMessageEntity);
			info.AddValue("_alreadyFetchedScheduledMessageEntity", _alreadyFetchedScheduledMessageEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "MessagegroupEntity":
					_alreadyFetchedMessagegroupEntity = true;
					this.MessagegroupEntity = (MessagegroupEntity)entity;
					break;
				case "PmsReportConfigurationEntity":
					_alreadyFetchedPmsReportConfigurationEntity = true;
					this.PmsReportConfigurationEntity = (PmsReportConfigurationEntity)entity;
					break;
				case "ScheduledMessageEntity":
					_alreadyFetchedScheduledMessageEntity = true;
					this.ScheduledMessageEntity = (ScheduledMessageEntity)entity;
					break;
				case "PmsRuleCollection":
					_alreadyFetchedPmsRuleCollection = true;
					if(entity!=null)
					{
						this.PmsRuleCollection.Add((PmsRuleEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "MessagegroupEntity":
					SetupSyncMessagegroupEntity(relatedEntity);
					break;
				case "PmsReportConfigurationEntity":
					SetupSyncPmsReportConfigurationEntity(relatedEntity);
					break;
				case "ScheduledMessageEntity":
					SetupSyncScheduledMessageEntity(relatedEntity);
					break;
				case "PmsRuleCollection":
					_pmsRuleCollection.Add((PmsRuleEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "MessagegroupEntity":
					DesetupSyncMessagegroupEntity(false, true);
					break;
				case "PmsReportConfigurationEntity":
					DesetupSyncPmsReportConfigurationEntity(false, true);
					break;
				case "ScheduledMessageEntity":
					DesetupSyncScheduledMessageEntity(false, true);
					break;
				case "PmsRuleCollection":
					this.PerformRelatedEntityRemoval(_pmsRuleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_messagegroupEntity!=null)
			{
				toReturn.Add(_messagegroupEntity);
			}
			if(_pmsReportConfigurationEntity!=null)
			{
				toReturn.Add(_pmsReportConfigurationEntity);
			}
			if(_scheduledMessageEntity!=null)
			{
				toReturn.Add(_scheduledMessageEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_pmsRuleCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsActionRuleId)
		{
			return FetchUsingPK(pmsActionRuleId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsActionRuleId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pmsActionRuleId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsActionRuleId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pmsActionRuleId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pmsActionRuleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pmsActionRuleId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PmsActionRuleId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PmsActionRuleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PmsRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch)
		{
			return GetMultiPmsRuleCollection(forceFetch, _pmsRuleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PmsRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPmsRuleCollection(forceFetch, _pmsRuleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPmsRuleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PmsRuleCollection GetMultiPmsRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPmsRuleCollection || forceFetch || _alwaysFetchPmsRuleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pmsRuleCollection);
				_pmsRuleCollection.SuppressClearInGetMulti=!forceFetch;
				_pmsRuleCollection.EntityFactoryToUse = entityFactoryToUse;
				_pmsRuleCollection.GetMultiManyToOne(this, null, filter);
				_pmsRuleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPmsRuleCollection = true;
			}
			return _pmsRuleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PmsRuleCollection'. These settings will be taken into account
		/// when the property PmsRuleCollection is requested or GetMultiPmsRuleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPmsRuleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pmsRuleCollection.SortClauses=sortClauses;
			_pmsRuleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public MessagegroupEntity GetSingleMessagegroupEntity()
		{
			return GetSingleMessagegroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public virtual MessagegroupEntity GetSingleMessagegroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessagegroupEntity || forceFetch || _alwaysFetchMessagegroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessagegroupEntityUsingMessagegroupId);
				MessagegroupEntity newEntity = new MessagegroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessagegroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MessagegroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messagegroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MessagegroupEntity = newEntity;
				_alreadyFetchedMessagegroupEntity = fetchResult;
			}
			return _messagegroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'PmsReportConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PmsReportConfigurationEntity' which is related to this entity.</returns>
		public PmsReportConfigurationEntity GetSinglePmsReportConfigurationEntity()
		{
			return GetSinglePmsReportConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PmsReportConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PmsReportConfigurationEntity' which is related to this entity.</returns>
		public virtual PmsReportConfigurationEntity GetSinglePmsReportConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPmsReportConfigurationEntity || forceFetch || _alwaysFetchPmsReportConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
				PmsReportConfigurationEntity newEntity = new PmsReportConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PmsReportConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (PmsReportConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pmsReportConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PmsReportConfigurationEntity = newEntity;
				_alreadyFetchedPmsReportConfigurationEntity = fetchResult;
			}
			return _pmsReportConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public ScheduledMessageEntity GetSingleScheduledMessageEntity()
		{
			return GetSingleScheduledMessageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public virtual ScheduledMessageEntity GetSingleScheduledMessageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduledMessageEntity || forceFetch || _alwaysFetchScheduledMessageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduledMessageEntityUsingScheduledMessageId);
				ScheduledMessageEntity newEntity = new ScheduledMessageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduledMessageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ScheduledMessageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduledMessageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduledMessageEntity = newEntity;
				_alreadyFetchedScheduledMessageEntity = fetchResult;
			}
			return _scheduledMessageEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("MessagegroupEntity", _messagegroupEntity);
			toReturn.Add("PmsReportConfigurationEntity", _pmsReportConfigurationEntity);
			toReturn.Add("ScheduledMessageEntity", _scheduledMessageEntity);
			toReturn.Add("PmsRuleCollection", _pmsRuleCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="validator">The validator object for this PmsActionRuleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pmsActionRuleId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pmsActionRuleId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_pmsRuleCollection = new Obymobi.Data.CollectionClasses.PmsRuleCollection();
			_pmsRuleCollection.SetContainingEntityInfo(this, "PmsActionRuleEntity");
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_messagegroupEntityReturnsNewIfNotFound = true;
			_pmsReportConfigurationEntityReturnsNewIfNotFound = true;
			_scheduledMessageEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsActionRuleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsReportConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduledMessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagegroupId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "PmsActionRuleCollection", resetFKFields, new int[] { (int)PmsActionRuleFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _messagegroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessagegroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, signalRelatedEntity, "PmsActionRuleCollection", resetFKFields, new int[] { (int)PmsActionRuleFieldIndex.MessagegroupId } );		
			_messagegroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _messagegroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessagegroupEntity(IEntityCore relatedEntity)
		{
			if(_messagegroupEntity!=relatedEntity)
			{		
				DesetupSyncMessagegroupEntity(true, true);
				_messagegroupEntity = (MessagegroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, ref _alreadyFetchedMessagegroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessagegroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pmsReportConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPmsReportConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pmsReportConfigurationEntity, new PropertyChangedEventHandler( OnPmsReportConfigurationEntityPropertyChanged ), "PmsReportConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic, true, signalRelatedEntity, "PmsActionRuleCollection", resetFKFields, new int[] { (int)PmsActionRuleFieldIndex.PmsReportConfigurationId } );		
			_pmsReportConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pmsReportConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPmsReportConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_pmsReportConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncPmsReportConfigurationEntity(true, true);
				_pmsReportConfigurationEntity = (PmsReportConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pmsReportConfigurationEntity, new PropertyChangedEventHandler( OnPmsReportConfigurationEntityPropertyChanged ), "PmsReportConfigurationEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic, true, ref _alreadyFetchedPmsReportConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPmsReportConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduledMessageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, signalRelatedEntity, "PmsActionRuleCollection", resetFKFields, new int[] { (int)PmsActionRuleFieldIndex.ScheduledMessageId } );		
			_scheduledMessageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduledMessageEntity(IEntityCore relatedEntity)
		{
			if(_scheduledMessageEntity!=relatedEntity)
			{		
				DesetupSyncScheduledMessageEntity(true, true);
				_scheduledMessageEntity = (ScheduledMessageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticPmsActionRuleRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, ref _alreadyFetchedScheduledMessageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduledMessageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pmsActionRuleId">PK value for PmsActionRule which data should be fetched into this PmsActionRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pmsActionRuleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PmsActionRuleFieldIndex.PmsActionRuleId].ForcedCurrentValueWrite(pmsActionRuleId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePmsActionRuleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PmsActionRuleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PmsActionRuleRelations Relations
		{
			get	{ return new PmsActionRuleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsRuleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsRuleCollection(), (IEntityRelation)GetRelationsForField("PmsRuleCollection")[0], (int)Obymobi.Data.EntityType.PmsActionRuleEntity, (int)Obymobi.Data.EntityType.PmsRuleEntity, 0, null, null, null, "PmsRuleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.PmsActionRuleEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Messagegroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupCollection(), (IEntityRelation)GetRelationsForField("MessagegroupEntity")[0], (int)Obymobi.Data.EntityType.PmsActionRuleEntity, (int)Obymobi.Data.EntityType.MessagegroupEntity, 0, null, null, null, "MessagegroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsReportConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsReportConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsReportConfigurationCollection(), (IEntityRelation)GetRelationsForField("PmsReportConfigurationEntity")[0], (int)Obymobi.Data.EntityType.PmsActionRuleEntity, (int)Obymobi.Data.EntityType.PmsReportConfigurationEntity, 0, null, null, null, "PmsReportConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageEntity")[0], (int)Obymobi.Data.EntityType.PmsActionRuleEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PmsActionRuleId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."PmsActionRuleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PmsActionRuleId
		{
			get { return (System.Int32)GetValue((int)PmsActionRuleFieldIndex.PmsActionRuleId, true); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.PmsActionRuleId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The PmsReportConfigurationId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."PmsReportConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsReportConfigurationId
		{
			get { return (System.Int32)GetValue((int)PmsActionRuleFieldIndex.PmsReportConfigurationId, true); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.PmsReportConfigurationId, value, true); }
		}

		/// <summary> The Active property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)PmsActionRuleFieldIndex.Active, true); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.Active, value, true); }
		}

		/// <summary> The Type property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PmsActionType Type
		{
			get { return (Obymobi.Enums.PmsActionType)GetValue((int)PmsActionRuleFieldIndex.Type, true); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.Type, value, true); }
		}

		/// <summary> The ScheduledMessageId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."ScheduledMessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduledMessageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.ScheduledMessageId, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.ScheduledMessageId, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PmsActionRuleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PmsActionRuleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The MessagegroupId property of the Entity PmsActionRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PmsActionRule"."MessagegroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MessagegroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PmsActionRuleFieldIndex.MessagegroupId, false); }
			set	{ SetValue((int)PmsActionRuleFieldIndex.MessagegroupId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PmsRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPmsRuleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PmsRuleCollection PmsRuleCollection
		{
			get	{ return GetMultiPmsRuleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PmsRuleCollection. When set to true, PmsRuleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsRuleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPmsRuleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsRuleCollection
		{
			get	{ return _alwaysFetchPmsRuleCollection; }
			set	{ _alwaysFetchPmsRuleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsRuleCollection already has been fetched. Setting this property to false when PmsRuleCollection has been fetched
		/// will clear the PmsRuleCollection collection well. Setting this property to true while PmsRuleCollection hasn't been fetched disables lazy loading for PmsRuleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsRuleCollection
		{
			get { return _alreadyFetchedPmsRuleCollection;}
			set 
			{
				if(_alreadyFetchedPmsRuleCollection && !value && (_pmsRuleCollection != null))
				{
					_pmsRuleCollection.Clear();
				}
				_alreadyFetchedPmsRuleCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PmsActionRuleCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessagegroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessagegroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MessagegroupEntity MessagegroupEntity
		{
			get	{ return GetSingleMessagegroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessagegroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PmsActionRuleCollection", "MessagegroupEntity", _messagegroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupEntity. When set to true, MessagegroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupEntity is accessed. You can always execute a forced fetch by calling GetSingleMessagegroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupEntity
		{
			get	{ return _alwaysFetchMessagegroupEntity; }
			set	{ _alwaysFetchMessagegroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupEntity already has been fetched. Setting this property to false when MessagegroupEntity has been fetched
		/// will set MessagegroupEntity to null as well. Setting this property to true while MessagegroupEntity hasn't been fetched disables lazy loading for MessagegroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupEntity
		{
			get { return _alreadyFetchedMessagegroupEntity;}
			set 
			{
				if(_alreadyFetchedMessagegroupEntity && !value)
				{
					this.MessagegroupEntity = null;
				}
				_alreadyFetchedMessagegroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MessagegroupEntity is not found
		/// in the database. When set to true, MessagegroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MessagegroupEntityReturnsNewIfNotFound
		{
			get	{ return _messagegroupEntityReturnsNewIfNotFound; }
			set { _messagegroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PmsReportConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePmsReportConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PmsReportConfigurationEntity PmsReportConfigurationEntity
		{
			get	{ return GetSinglePmsReportConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPmsReportConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PmsActionRuleCollection", "PmsReportConfigurationEntity", _pmsReportConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PmsReportConfigurationEntity. When set to true, PmsReportConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsReportConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSinglePmsReportConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsReportConfigurationEntity
		{
			get	{ return _alwaysFetchPmsReportConfigurationEntity; }
			set	{ _alwaysFetchPmsReportConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsReportConfigurationEntity already has been fetched. Setting this property to false when PmsReportConfigurationEntity has been fetched
		/// will set PmsReportConfigurationEntity to null as well. Setting this property to true while PmsReportConfigurationEntity hasn't been fetched disables lazy loading for PmsReportConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsReportConfigurationEntity
		{
			get { return _alreadyFetchedPmsReportConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedPmsReportConfigurationEntity && !value)
				{
					this.PmsReportConfigurationEntity = null;
				}
				_alreadyFetchedPmsReportConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PmsReportConfigurationEntity is not found
		/// in the database. When set to true, PmsReportConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PmsReportConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _pmsReportConfigurationEntityReturnsNewIfNotFound; }
			set { _pmsReportConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ScheduledMessageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduledMessageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduledMessageEntity ScheduledMessageEntity
		{
			get	{ return GetSingleScheduledMessageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduledMessageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PmsActionRuleCollection", "ScheduledMessageEntity", _scheduledMessageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageEntity. When set to true, ScheduledMessageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduledMessageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageEntity
		{
			get	{ return _alwaysFetchScheduledMessageEntity; }
			set	{ _alwaysFetchScheduledMessageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageEntity already has been fetched. Setting this property to false when ScheduledMessageEntity has been fetched
		/// will set ScheduledMessageEntity to null as well. Setting this property to true while ScheduledMessageEntity hasn't been fetched disables lazy loading for ScheduledMessageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageEntity
		{
			get { return _alreadyFetchedScheduledMessageEntity;}
			set 
			{
				if(_alreadyFetchedScheduledMessageEntity && !value)
				{
					this.ScheduledMessageEntity = null;
				}
				_alreadyFetchedScheduledMessageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduledMessageEntity is not found
		/// in the database. When set to true, ScheduledMessageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduledMessageEntityReturnsNewIfNotFound
		{
			get	{ return _scheduledMessageEntityReturnsNewIfNotFound; }
			set { _scheduledMessageEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PmsActionRuleEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
