﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Receipt'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReceiptEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReceiptEntity"; }
		}
	
		#region Class Member Declarations
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private OutletSellerInformationEntity _outletSellerInformationEntity;
		private bool	_alwaysFetchOutletSellerInformationEntity, _alreadyFetchedOutletSellerInformationEntity, _outletSellerInformationEntityReturnsNewIfNotFound;
		private ReceiptTemplateEntity _receiptTemplateEntity;
		private bool	_alwaysFetchReceiptTemplateEntity, _alreadyFetchedReceiptTemplateEntity, _receiptTemplateEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name OutletSellerInformationEntity</summary>
			public static readonly string OutletSellerInformationEntity = "OutletSellerInformationEntity";
			/// <summary>Member name ReceiptTemplateEntity</summary>
			public static readonly string ReceiptTemplateEntity = "ReceiptTemplateEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReceiptEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReceiptEntityBase() :base("ReceiptEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		protected ReceiptEntityBase(System.Int32 receiptId):base("ReceiptEntity")
		{
			InitClassFetch(receiptId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReceiptEntityBase(System.Int32 receiptId, IPrefetchPath prefetchPathToUse): base("ReceiptEntity")
		{
			InitClassFetch(receiptId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="validator">The custom validator object for this ReceiptEntity</param>
		protected ReceiptEntityBase(System.Int32 receiptId, IValidator validator):base("ReceiptEntity")
		{
			InitClassFetch(receiptId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceiptEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_outletSellerInformationEntity = (OutletSellerInformationEntity)info.GetValue("_outletSellerInformationEntity", typeof(OutletSellerInformationEntity));
			if(_outletSellerInformationEntity!=null)
			{
				_outletSellerInformationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletSellerInformationEntityReturnsNewIfNotFound = info.GetBoolean("_outletSellerInformationEntityReturnsNewIfNotFound");
			_alwaysFetchOutletSellerInformationEntity = info.GetBoolean("_alwaysFetchOutletSellerInformationEntity");
			_alreadyFetchedOutletSellerInformationEntity = info.GetBoolean("_alreadyFetchedOutletSellerInformationEntity");

			_receiptTemplateEntity = (ReceiptTemplateEntity)info.GetValue("_receiptTemplateEntity", typeof(ReceiptTemplateEntity));
			if(_receiptTemplateEntity!=null)
			{
				_receiptTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiptTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_receiptTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchReceiptTemplateEntity = info.GetBoolean("_alwaysFetchReceiptTemplateEntity");
			_alreadyFetchedReceiptTemplateEntity = info.GetBoolean("_alreadyFetchedReceiptTemplateEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReceiptFieldIndex)fieldIndex)
			{
				case ReceiptFieldIndex.ReceiptTemplateId:
					DesetupSyncReceiptTemplateEntity(true, false);
					_alreadyFetchedReceiptTemplateEntity = false;
					break;
				case ReceiptFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ReceiptFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case ReceiptFieldIndex.OutletSellerInformationId:
					DesetupSyncOutletSellerInformationEntity(true, false);
					_alreadyFetchedOutletSellerInformationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedOutletSellerInformationEntity = (_outletSellerInformationEntity != null);
			_alreadyFetchedReceiptTemplateEntity = (_receiptTemplateEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "OutletSellerInformationEntity":
					toReturn.Add(Relations.OutletSellerInformationEntityUsingOutletSellerInformationId);
					break;
				case "ReceiptTemplateEntity":
					toReturn.Add(Relations.ReceiptTemplateEntityUsingReceiptTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_outletSellerInformationEntity", (!this.MarkedForDeletion?_outletSellerInformationEntity:null));
			info.AddValue("_outletSellerInformationEntityReturnsNewIfNotFound", _outletSellerInformationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletSellerInformationEntity", _alwaysFetchOutletSellerInformationEntity);
			info.AddValue("_alreadyFetchedOutletSellerInformationEntity", _alreadyFetchedOutletSellerInformationEntity);
			info.AddValue("_receiptTemplateEntity", (!this.MarkedForDeletion?_receiptTemplateEntity:null));
			info.AddValue("_receiptTemplateEntityReturnsNewIfNotFound", _receiptTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiptTemplateEntity", _alwaysFetchReceiptTemplateEntity);
			info.AddValue("_alreadyFetchedReceiptTemplateEntity", _alreadyFetchedReceiptTemplateEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "OutletSellerInformationEntity":
					_alreadyFetchedOutletSellerInformationEntity = true;
					this.OutletSellerInformationEntity = (OutletSellerInformationEntity)entity;
					break;
				case "ReceiptTemplateEntity":
					_alreadyFetchedReceiptTemplateEntity = true;
					this.ReceiptTemplateEntity = (ReceiptTemplateEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "OutletSellerInformationEntity":
					SetupSyncOutletSellerInformationEntity(relatedEntity);
					break;
				case "ReceiptTemplateEntity":
					SetupSyncReceiptTemplateEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "OutletSellerInformationEntity":
					DesetupSyncOutletSellerInformationEntity(false, true);
					break;
				case "ReceiptTemplateEntity":
					DesetupSyncReceiptTemplateEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_outletSellerInformationEntity!=null)
			{
				toReturn.Add(_outletSellerInformationEntity);
			}
			if(_receiptTemplateEntity!=null)
			{
				toReturn.Add(_receiptTemplateEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptId)
		{
			return FetchUsingPK(receiptId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(receiptId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(receiptId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(receiptId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReceiptId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReceiptRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId);
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletSellerInformationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletSellerInformationEntity' which is related to this entity.</returns>
		public OutletSellerInformationEntity GetSingleOutletSellerInformationEntity()
		{
			return GetSingleOutletSellerInformationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletSellerInformationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletSellerInformationEntity' which is related to this entity.</returns>
		public virtual OutletSellerInformationEntity GetSingleOutletSellerInformationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletSellerInformationEntity || forceFetch || _alwaysFetchOutletSellerInformationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletSellerInformationEntityUsingOutletSellerInformationId);
				OutletSellerInformationEntity newEntity = new OutletSellerInformationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletSellerInformationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletSellerInformationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletSellerInformationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletSellerInformationEntity = newEntity;
				_alreadyFetchedOutletSellerInformationEntity = fetchResult;
			}
			return _outletSellerInformationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReceiptTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReceiptTemplateEntity' which is related to this entity.</returns>
		public ReceiptTemplateEntity GetSingleReceiptTemplateEntity()
		{
			return GetSingleReceiptTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReceiptTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReceiptTemplateEntity' which is related to this entity.</returns>
		public virtual ReceiptTemplateEntity GetSingleReceiptTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiptTemplateEntity || forceFetch || _alwaysFetchReceiptTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReceiptTemplateEntityUsingReceiptTemplateId);
				ReceiptTemplateEntity newEntity = new ReceiptTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiptTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReceiptTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiptTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiptTemplateEntity = newEntity;
				_alreadyFetchedReceiptTemplateEntity = fetchResult;
			}
			return _receiptTemplateEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("OutletSellerInformationEntity", _outletSellerInformationEntity);
			toReturn.Add("ReceiptTemplateEntity", _receiptTemplateEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="validator">The validator object for this ReceiptEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 receiptId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(receiptId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_companyEntityReturnsNewIfNotFound = true;
			_orderEntityReturnsNewIfNotFound = true;
			_outletSellerInformationEntityReturnsNewIfNotFound = true;
			_receiptTemplateEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerContactInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxBreakdown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProcessorInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerContactEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletSellerInformationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ReceiptCollection", resetFKFields, new int[] { (int)ReceiptFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "ReceiptCollection", resetFKFields, new int[] { (int)ReceiptFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletSellerInformationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletSellerInformationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletSellerInformationEntity, new PropertyChangedEventHandler( OnOutletSellerInformationEntityPropertyChanged ), "OutletSellerInformationEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, true, signalRelatedEntity, "ReceiptCollection", resetFKFields, new int[] { (int)ReceiptFieldIndex.OutletSellerInformationId } );		
			_outletSellerInformationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletSellerInformationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletSellerInformationEntity(IEntityCore relatedEntity)
		{
			if(_outletSellerInformationEntity!=relatedEntity)
			{		
				DesetupSyncOutletSellerInformationEntity(true, true);
				_outletSellerInformationEntity = (OutletSellerInformationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletSellerInformationEntity, new PropertyChangedEventHandler( OnOutletSellerInformationEntityPropertyChanged ), "OutletSellerInformationEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.OutletSellerInformationEntityUsingOutletSellerInformationIdStatic, true, ref _alreadyFetchedOutletSellerInformationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletSellerInformationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiptTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiptTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiptTemplateEntity, new PropertyChangedEventHandler( OnReceiptTemplateEntityPropertyChanged ), "ReceiptTemplateEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, true, signalRelatedEntity, "ReceiptCollection", resetFKFields, new int[] { (int)ReceiptFieldIndex.ReceiptTemplateId } );		
			_receiptTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiptTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiptTemplateEntity(IEntityCore relatedEntity)
		{
			if(_receiptTemplateEntity!=relatedEntity)
			{		
				DesetupSyncReceiptTemplateEntity(true, true);
				_receiptTemplateEntity = (ReceiptTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiptTemplateEntity, new PropertyChangedEventHandler( OnReceiptTemplateEntityPropertyChanged ), "ReceiptTemplateEntity", Obymobi.Data.RelationClasses.StaticReceiptRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, true, ref _alreadyFetchedReceiptTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiptTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 receiptId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReceiptFieldIndex.ReceiptId].ForcedCurrentValueWrite(receiptId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReceiptDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReceiptEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReceiptRelations Relations
		{
			get	{ return new ReceiptRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ReceiptEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.ReceiptEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutletSellerInformation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletSellerInformationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletSellerInformationCollection(), (IEntityRelation)GetRelationsForField("OutletSellerInformationEntity")[0], (int)Obymobi.Data.EntityType.ReceiptEntity, (int)Obymobi.Data.EntityType.OutletSellerInformationEntity, 0, null, null, null, "OutletSellerInformationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReceiptTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReceiptTemplateCollection(), (IEntityRelation)GetRelationsForField("ReceiptTemplateEntity")[0], (int)Obymobi.Data.EntityType.ReceiptEntity, (int)Obymobi.Data.EntityType.ReceiptTemplateEntity, 0, null, null, null, "ReceiptTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReceiptId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ReceiptId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReceiptId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.ReceiptId, true); }
			set	{ SetValue((int)ReceiptFieldIndex.ReceiptId, value, true); }
		}

		/// <summary> The ReceiptTemplateId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ReceiptTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiptTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReceiptFieldIndex.ReceiptTemplateId, false); }
			set	{ SetValue((int)ReceiptFieldIndex.ReceiptTemplateId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The OrderId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.OrderId, true); }
			set	{ SetValue((int)ReceiptFieldIndex.OrderId, value, true); }
		}

		/// <summary> The Number property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Number"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.Number, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Number, value, true); }
		}

		/// <summary> The SellerName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerName, value, true); }
		}

		/// <summary> The SellerAddress property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerAddress
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerAddress, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerAddress, value, true); }
		}

		/// <summary> The SellerContactInfo property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerContactInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerContactInfo
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerContactInfo, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerContactInfo, value, true); }
		}

		/// <summary> The ServiceMethodName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."ServiceMethodName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ServiceMethodName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.ServiceMethodName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.ServiceMethodName, value, true); }
		}

		/// <summary> The CheckoutMethodName property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CheckoutMethodName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CheckoutMethodName
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.CheckoutMethodName, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CheckoutMethodName, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReceiptFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReceiptFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReceiptFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReceiptFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The TaxBreakdown property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."TaxBreakdown"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TaxBreakdown
		{
			get { return (System.Int32)GetValue((int)ReceiptFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)ReceiptFieldIndex.TaxBreakdown, value, true); }
		}

		/// <summary> The Email property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.Email, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Email, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)ReceiptFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The VatNumber property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."VatNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.VatNumber, true); }
			set	{ SetValue((int)ReceiptFieldIndex.VatNumber, value, true); }
		}

		/// <summary> The PaymentProcessorInfo property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."PaymentProcessorInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)ReceiptFieldIndex.PaymentProcessorInfo, value, true); }
		}

		/// <summary> The SellerContactEmail property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."SellerContactEmail"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SellerContactEmail
		{
			get { return (System.String)GetValue((int)ReceiptFieldIndex.SellerContactEmail, true); }
			set	{ SetValue((int)ReceiptFieldIndex.SellerContactEmail, value, true); }
		}

		/// <summary> The OutletSellerInformationId property of the Entity Receipt<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Receipt"."OutletSellerInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletSellerInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReceiptFieldIndex.OutletSellerInformationId, false); }
			set	{ SetValue((int)ReceiptFieldIndex.OutletSellerInformationId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceiptCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceiptCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletSellerInformationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletSellerInformationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletSellerInformationEntity OutletSellerInformationEntity
		{
			get	{ return GetSingleOutletSellerInformationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletSellerInformationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceiptCollection", "OutletSellerInformationEntity", _outletSellerInformationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletSellerInformationEntity. When set to true, OutletSellerInformationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletSellerInformationEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletSellerInformationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletSellerInformationEntity
		{
			get	{ return _alwaysFetchOutletSellerInformationEntity; }
			set	{ _alwaysFetchOutletSellerInformationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletSellerInformationEntity already has been fetched. Setting this property to false when OutletSellerInformationEntity has been fetched
		/// will set OutletSellerInformationEntity to null as well. Setting this property to true while OutletSellerInformationEntity hasn't been fetched disables lazy loading for OutletSellerInformationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletSellerInformationEntity
		{
			get { return _alreadyFetchedOutletSellerInformationEntity;}
			set 
			{
				if(_alreadyFetchedOutletSellerInformationEntity && !value)
				{
					this.OutletSellerInformationEntity = null;
				}
				_alreadyFetchedOutletSellerInformationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletSellerInformationEntity is not found
		/// in the database. When set to true, OutletSellerInformationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletSellerInformationEntityReturnsNewIfNotFound
		{
			get	{ return _outletSellerInformationEntityReturnsNewIfNotFound; }
			set { _outletSellerInformationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReceiptTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiptTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReceiptTemplateEntity ReceiptTemplateEntity
		{
			get	{ return GetSingleReceiptTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiptTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceiptCollection", "ReceiptTemplateEntity", _receiptTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptTemplateEntity. When set to true, ReceiptTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiptTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptTemplateEntity
		{
			get	{ return _alwaysFetchReceiptTemplateEntity; }
			set	{ _alwaysFetchReceiptTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptTemplateEntity already has been fetched. Setting this property to false when ReceiptTemplateEntity has been fetched
		/// will set ReceiptTemplateEntity to null as well. Setting this property to true while ReceiptTemplateEntity hasn't been fetched disables lazy loading for ReceiptTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptTemplateEntity
		{
			get { return _alreadyFetchedReceiptTemplateEntity;}
			set 
			{
				if(_alreadyFetchedReceiptTemplateEntity && !value)
				{
					this.ReceiptTemplateEntity = null;
				}
				_alreadyFetchedReceiptTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiptTemplateEntity is not found
		/// in the database. When set to true, ReceiptTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiptTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _receiptTemplateEntityReturnsNewIfNotFound; }
			set { _receiptTemplateEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReceiptEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
