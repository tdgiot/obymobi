﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'InfraredConfiguration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class InfraredConfigurationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "InfraredConfigurationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.InfraredCommandCollection	_infraredCommandCollection;
		private bool	_alwaysFetchInfraredCommandCollection, _alreadyFetchedInfraredCommandCollection;
		private Obymobi.Data.CollectionClasses.RoomControlComponentCollection	_roomControlComponentCollection;
		private bool	_alwaysFetchRoomControlComponentCollection, _alreadyFetchedRoomControlComponentCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection	_roomControlSectionItemCollection;
		private bool	_alwaysFetchRoomControlSectionItemCollection, _alreadyFetchedRoomControlSectionItemCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection;
		private bool	_alwaysFetchRoomControlWidgetCollection, _alreadyFetchedRoomControlWidgetCollection;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name InfraredCommandCollection</summary>
			public static readonly string InfraredCommandCollection = "InfraredCommandCollection";
			/// <summary>Member name RoomControlComponentCollection</summary>
			public static readonly string RoomControlComponentCollection = "RoomControlComponentCollection";
			/// <summary>Member name RoomControlSectionItemCollection</summary>
			public static readonly string RoomControlSectionItemCollection = "RoomControlSectionItemCollection";
			/// <summary>Member name RoomControlWidgetCollection</summary>
			public static readonly string RoomControlWidgetCollection = "RoomControlWidgetCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static InfraredConfigurationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected InfraredConfigurationEntityBase() :base("InfraredConfigurationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		protected InfraredConfigurationEntityBase(System.Int32 infraredConfigurationId):base("InfraredConfigurationEntity")
		{
			InitClassFetch(infraredConfigurationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected InfraredConfigurationEntityBase(System.Int32 infraredConfigurationId, IPrefetchPath prefetchPathToUse): base("InfraredConfigurationEntity")
		{
			InitClassFetch(infraredConfigurationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="validator">The custom validator object for this InfraredConfigurationEntity</param>
		protected InfraredConfigurationEntityBase(System.Int32 infraredConfigurationId, IValidator validator):base("InfraredConfigurationEntity")
		{
			InitClassFetch(infraredConfigurationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InfraredConfigurationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_infraredCommandCollection = (Obymobi.Data.CollectionClasses.InfraredCommandCollection)info.GetValue("_infraredCommandCollection", typeof(Obymobi.Data.CollectionClasses.InfraredCommandCollection));
			_alwaysFetchInfraredCommandCollection = info.GetBoolean("_alwaysFetchInfraredCommandCollection");
			_alreadyFetchedInfraredCommandCollection = info.GetBoolean("_alreadyFetchedInfraredCommandCollection");

			_roomControlComponentCollection = (Obymobi.Data.CollectionClasses.RoomControlComponentCollection)info.GetValue("_roomControlComponentCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlComponentCollection));
			_alwaysFetchRoomControlComponentCollection = info.GetBoolean("_alwaysFetchRoomControlComponentCollection");
			_alreadyFetchedRoomControlComponentCollection = info.GetBoolean("_alreadyFetchedRoomControlComponentCollection");

			_roomControlSectionItemCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection)info.GetValue("_roomControlSectionItemCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection));
			_alwaysFetchRoomControlSectionItemCollection = info.GetBoolean("_alwaysFetchRoomControlSectionItemCollection");
			_alreadyFetchedRoomControlSectionItemCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionItemCollection");

			_roomControlWidgetCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection");
			_alreadyFetchedRoomControlWidgetCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedInfraredCommandCollection = (_infraredCommandCollection.Count > 0);
			_alreadyFetchedRoomControlComponentCollection = (_roomControlComponentCollection.Count > 0);
			_alreadyFetchedRoomControlSectionItemCollection = (_roomControlSectionItemCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection = (_roomControlWidgetCollection.Count > 0);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "InfraredCommandCollection":
					toReturn.Add(Relations.InfraredCommandEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlComponentCollection":
					toReturn.Add(Relations.RoomControlComponentEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlSectionItemCollection":
					toReturn.Add(Relations.RoomControlSectionItemEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlWidgetCollection":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingInfraredConfigurationId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingInfraredConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_infraredCommandCollection", (!this.MarkedForDeletion?_infraredCommandCollection:null));
			info.AddValue("_alwaysFetchInfraredCommandCollection", _alwaysFetchInfraredCommandCollection);
			info.AddValue("_alreadyFetchedInfraredCommandCollection", _alreadyFetchedInfraredCommandCollection);
			info.AddValue("_roomControlComponentCollection", (!this.MarkedForDeletion?_roomControlComponentCollection:null));
			info.AddValue("_alwaysFetchRoomControlComponentCollection", _alwaysFetchRoomControlComponentCollection);
			info.AddValue("_alreadyFetchedRoomControlComponentCollection", _alreadyFetchedRoomControlComponentCollection);
			info.AddValue("_roomControlSectionItemCollection", (!this.MarkedForDeletion?_roomControlSectionItemCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionItemCollection", _alwaysFetchRoomControlSectionItemCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionItemCollection", _alreadyFetchedRoomControlSectionItemCollection);
			info.AddValue("_roomControlWidgetCollection", (!this.MarkedForDeletion?_roomControlWidgetCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection", _alwaysFetchRoomControlWidgetCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection", _alreadyFetchedRoomControlWidgetCollection);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "InfraredCommandCollection":
					_alreadyFetchedInfraredCommandCollection = true;
					if(entity!=null)
					{
						this.InfraredCommandCollection.Add((InfraredCommandEntity)entity);
					}
					break;
				case "RoomControlComponentCollection":
					_alreadyFetchedRoomControlComponentCollection = true;
					if(entity!=null)
					{
						this.RoomControlComponentCollection.Add((RoomControlComponentEntity)entity);
					}
					break;
				case "RoomControlSectionItemCollection":
					_alreadyFetchedRoomControlSectionItemCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionItemCollection.Add((RoomControlSectionItemEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection":
					_alreadyFetchedRoomControlWidgetCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "InfraredCommandCollection":
					_infraredCommandCollection.Add((InfraredCommandEntity)relatedEntity);
					break;
				case "RoomControlComponentCollection":
					_roomControlComponentCollection.Add((RoomControlComponentEntity)relatedEntity);
					break;
				case "RoomControlSectionItemCollection":
					_roomControlSectionItemCollection.Add((RoomControlSectionItemEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection":
					_roomControlWidgetCollection.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "InfraredCommandCollection":
					this.PerformRelatedEntityRemoval(_infraredCommandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlComponentCollection":
					this.PerformRelatedEntityRemoval(_roomControlComponentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionItemCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_infraredCommandCollection);
			toReturn.Add(_roomControlComponentCollection);
			toReturn.Add(_roomControlSectionItemCollection);
			toReturn.Add(_roomControlWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 infraredConfigurationId)
		{
			return FetchUsingPK(infraredConfigurationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 infraredConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(infraredConfigurationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 infraredConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(infraredConfigurationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 infraredConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(infraredConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.InfraredConfigurationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new InfraredConfigurationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'InfraredCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InfraredCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.InfraredCommandCollection GetMultiInfraredCommandCollection(bool forceFetch)
		{
			return GetMultiInfraredCommandCollection(forceFetch, _infraredCommandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InfraredCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InfraredCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.InfraredCommandCollection GetMultiInfraredCommandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInfraredCommandCollection(forceFetch, _infraredCommandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InfraredCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.InfraredCommandCollection GetMultiInfraredCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInfraredCommandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InfraredCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.InfraredCommandCollection GetMultiInfraredCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInfraredCommandCollection || forceFetch || _alwaysFetchInfraredCommandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_infraredCommandCollection);
				_infraredCommandCollection.SuppressClearInGetMulti=!forceFetch;
				_infraredCommandCollection.EntityFactoryToUse = entityFactoryToUse;
				_infraredCommandCollection.GetMultiManyToOne(this, filter);
				_infraredCommandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedInfraredCommandCollection = true;
			}
			return _infraredCommandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'InfraredCommandCollection'. These settings will be taken into account
		/// when the property InfraredCommandCollection is requested or GetMultiInfraredCommandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInfraredCommandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_infraredCommandCollection.SortClauses=sortClauses;
			_infraredCommandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, _roomControlComponentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, _roomControlComponentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlComponentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentCollection GetMultiRoomControlComponentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlComponentCollection || forceFetch || _alwaysFetchRoomControlComponentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlComponentCollection);
				_roomControlComponentCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlComponentCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlComponentCollection.GetMultiManyToOne(this, null, filter);
				_roomControlComponentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlComponentCollection = true;
			}
			return _roomControlComponentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlComponentCollection'. These settings will be taken into account
		/// when the property RoomControlComponentCollection is requested or GetMultiRoomControlComponentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlComponentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlComponentCollection.SortClauses=sortClauses;
			_roomControlComponentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, _roomControlSectionItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, _roomControlSectionItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection GetMultiRoomControlSectionItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionItemCollection || forceFetch || _alwaysFetchRoomControlSectionItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionItemCollection);
				_roomControlSectionItemCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionItemCollection.GetMultiManyToOne(this, null, null, filter);
				_roomControlSectionItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionItemCollection = true;
			}
			return _roomControlSectionItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionItemCollection'. These settings will be taken into account
		/// when the property RoomControlSectionItemCollection is requested or GetMultiRoomControlSectionItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionItemCollection.SortClauses=sortClauses;
			_roomControlSectionItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection || forceFetch || _alwaysFetchRoomControlWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection);
				_roomControlWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection = true;
			}
			return _roomControlWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection is requested or GetMultiRoomControlWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection.SortClauses=sortClauses;
			_roomControlWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingInfraredConfigurationId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCInfraredConfigurationId(this.InfraredConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("InfraredCommandCollection", _infraredCommandCollection);
			toReturn.Add("RoomControlComponentCollection", _roomControlComponentCollection);
			toReturn.Add("RoomControlSectionItemCollection", _roomControlSectionItemCollection);
			toReturn.Add("RoomControlWidgetCollection", _roomControlWidgetCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="validator">The validator object for this InfraredConfigurationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 infraredConfigurationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(infraredConfigurationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_infraredCommandCollection = new Obymobi.Data.CollectionClasses.InfraredCommandCollection();
			_infraredCommandCollection.SetContainingEntityInfo(this, "InfraredConfigurationEntity");

			_roomControlComponentCollection = new Obymobi.Data.CollectionClasses.RoomControlComponentCollection();
			_roomControlComponentCollection.SetContainingEntityInfo(this, "InfraredConfigurationEntity");

			_roomControlSectionItemCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection();
			_roomControlSectionItemCollection.SetContainingEntityInfo(this, "InfraredConfigurationEntity");

			_roomControlWidgetCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection.SetContainingEntityInfo(this, "InfraredConfigurationEntity");
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MillisecondsBetweenCommands", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticInfraredConfigurationRelations.TimestampEntityUsingInfraredConfigurationIdStatic, false, signalRelatedEntity, "InfraredConfigurationEntity", false, new int[] { (int)InfraredConfigurationFieldIndex.InfraredConfigurationId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticInfraredConfigurationRelations.TimestampEntityUsingInfraredConfigurationIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="infraredConfigurationId">PK value for InfraredConfiguration which data should be fetched into this InfraredConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 infraredConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)InfraredConfigurationFieldIndex.InfraredConfigurationId].ForcedCurrentValueWrite(infraredConfigurationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateInfraredConfigurationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new InfraredConfigurationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static InfraredConfigurationRelations Relations
		{
			get	{ return new InfraredConfigurationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredCommand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredCommandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredCommandCollection(), (IEntityRelation)GetRelationsForField("InfraredCommandCollection")[0], (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, (int)Obymobi.Data.EntityType.InfraredCommandEntity, 0, null, null, null, "InfraredCommandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentCollection")[0], (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemCollection")[0], (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, 0, null, null, null, "RoomControlSectionItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection")[0], (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The InfraredConfigurationId property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."InfraredConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 InfraredConfigurationId
		{
			get { return (System.Int32)GetValue((int)InfraredConfigurationFieldIndex.InfraredConfigurationId, true); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.InfraredConfigurationId, value, true); }
		}

		/// <summary> The Name property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)InfraredConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.Name, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)InfraredConfigurationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InfraredConfigurationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InfraredConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)InfraredConfigurationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The MillisecondsBetweenCommands property of the Entity InfraredConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "InfraredConfiguration"."MillisecondsBetweenCommands"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MillisecondsBetweenCommands
		{
			get { return (System.Int32)GetValue((int)InfraredConfigurationFieldIndex.MillisecondsBetweenCommands, true); }
			set	{ SetValue((int)InfraredConfigurationFieldIndex.MillisecondsBetweenCommands, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'InfraredCommandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInfraredCommandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.InfraredCommandCollection InfraredCommandCollection
		{
			get	{ return GetMultiInfraredCommandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredCommandCollection. When set to true, InfraredCommandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredCommandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiInfraredCommandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredCommandCollection
		{
			get	{ return _alwaysFetchInfraredCommandCollection; }
			set	{ _alwaysFetchInfraredCommandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredCommandCollection already has been fetched. Setting this property to false when InfraredCommandCollection has been fetched
		/// will clear the InfraredCommandCollection collection well. Setting this property to true while InfraredCommandCollection hasn't been fetched disables lazy loading for InfraredCommandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredCommandCollection
		{
			get { return _alreadyFetchedInfraredCommandCollection;}
			set 
			{
				if(_alreadyFetchedInfraredCommandCollection && !value && (_infraredCommandCollection != null))
				{
					_infraredCommandCollection.Clear();
				}
				_alreadyFetchedInfraredCommandCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlComponentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentCollection RoomControlComponentCollection
		{
			get	{ return GetMultiRoomControlComponentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentCollection. When set to true, RoomControlComponentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlComponentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentCollection
		{
			get	{ return _alwaysFetchRoomControlComponentCollection; }
			set	{ _alwaysFetchRoomControlComponentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentCollection already has been fetched. Setting this property to false when RoomControlComponentCollection has been fetched
		/// will clear the RoomControlComponentCollection collection well. Setting this property to true while RoomControlComponentCollection hasn't been fetched disables lazy loading for RoomControlComponentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentCollection
		{
			get { return _alreadyFetchedRoomControlComponentCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentCollection && !value && (_roomControlComponentCollection != null))
				{
					_roomControlComponentCollection.Clear();
				}
				_alreadyFetchedRoomControlComponentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection RoomControlSectionItemCollection
		{
			get	{ return GetMultiRoomControlSectionItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemCollection. When set to true, RoomControlSectionItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemCollection
		{
			get	{ return _alwaysFetchRoomControlSectionItemCollection; }
			set	{ _alwaysFetchRoomControlSectionItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemCollection already has been fetched. Setting this property to false when RoomControlSectionItemCollection has been fetched
		/// will clear the RoomControlSectionItemCollection collection well. Setting this property to true while RoomControlSectionItemCollection hasn't been fetched disables lazy loading for RoomControlSectionItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemCollection
		{
			get { return _alreadyFetchedRoomControlSectionItemCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemCollection && !value && (_roomControlSectionItemCollection != null))
				{
					_roomControlSectionItemCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection
		{
			get	{ return GetMultiRoomControlWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection. When set to true, RoomControlWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection; }
			set	{ _alwaysFetchRoomControlWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection already has been fetched. Setting this property to false when RoomControlWidgetCollection has been fetched
		/// will clear the RoomControlWidgetCollection collection well. Setting this property to true while RoomControlWidgetCollection hasn't been fetched disables lazy loading for RoomControlWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection
		{
			get { return _alreadyFetchedRoomControlWidgetCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection && !value && (_roomControlWidgetCollection != null))
				{
					_roomControlWidgetCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "InfraredConfigurationEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.InfraredConfigurationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
