﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ReportProcessingTask'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReportProcessingTaskEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReportProcessingTaskEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection	_reportProcessingTaskTemplateCollection;
		private bool	_alwaysFetchReportProcessingTaskTemplateCollection, _alreadyFetchedReportProcessingTaskTemplateCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private TimeZoneEntity _timeZoneEntity;
		private bool	_alwaysFetchTimeZoneEntity, _alreadyFetchedTimeZoneEntity, _timeZoneEntityReturnsNewIfNotFound;
		private ReportProcessingTaskFileEntity _reportProcessingTaskFileEntity;
		private bool	_alwaysFetchReportProcessingTaskFileEntity, _alreadyFetchedReportProcessingTaskFileEntity, _reportProcessingTaskFileEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name TimeZoneEntity</summary>
			public static readonly string TimeZoneEntity = "TimeZoneEntity";
			/// <summary>Member name ReportProcessingTaskTemplateCollection</summary>
			public static readonly string ReportProcessingTaskTemplateCollection = "ReportProcessingTaskTemplateCollection";
			/// <summary>Member name ReportProcessingTaskFileEntity</summary>
			public static readonly string ReportProcessingTaskFileEntity = "ReportProcessingTaskFileEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportProcessingTaskEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReportProcessingTaskEntityBase() :base("ReportProcessingTaskEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		protected ReportProcessingTaskEntityBase(System.Int32 reportProcessingTaskId):base("ReportProcessingTaskEntity")
		{
			InitClassFetch(reportProcessingTaskId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReportProcessingTaskEntityBase(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse): base("ReportProcessingTaskEntity")
		{
			InitClassFetch(reportProcessingTaskId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="validator">The custom validator object for this ReportProcessingTaskEntity</param>
		protected ReportProcessingTaskEntityBase(System.Int32 reportProcessingTaskId, IValidator validator):base("ReportProcessingTaskEntity")
		{
			InitClassFetch(reportProcessingTaskId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportProcessingTaskEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_reportProcessingTaskTemplateCollection = (Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection)info.GetValue("_reportProcessingTaskTemplateCollection", typeof(Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection));
			_alwaysFetchReportProcessingTaskTemplateCollection = info.GetBoolean("_alwaysFetchReportProcessingTaskTemplateCollection");
			_alreadyFetchedReportProcessingTaskTemplateCollection = info.GetBoolean("_alreadyFetchedReportProcessingTaskTemplateCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_timeZoneEntity = (TimeZoneEntity)info.GetValue("_timeZoneEntity", typeof(TimeZoneEntity));
			if(_timeZoneEntity!=null)
			{
				_timeZoneEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timeZoneEntityReturnsNewIfNotFound = info.GetBoolean("_timeZoneEntityReturnsNewIfNotFound");
			_alwaysFetchTimeZoneEntity = info.GetBoolean("_alwaysFetchTimeZoneEntity");
			_alreadyFetchedTimeZoneEntity = info.GetBoolean("_alreadyFetchedTimeZoneEntity");
			_reportProcessingTaskFileEntity = (ReportProcessingTaskFileEntity)info.GetValue("_reportProcessingTaskFileEntity", typeof(ReportProcessingTaskFileEntity));
			if(_reportProcessingTaskFileEntity!=null)
			{
				_reportProcessingTaskFileEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportProcessingTaskFileEntityReturnsNewIfNotFound = info.GetBoolean("_reportProcessingTaskFileEntityReturnsNewIfNotFound");
			_alwaysFetchReportProcessingTaskFileEntity = info.GetBoolean("_alwaysFetchReportProcessingTaskFileEntity");
			_alreadyFetchedReportProcessingTaskFileEntity = info.GetBoolean("_alreadyFetchedReportProcessingTaskFileEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportProcessingTaskFieldIndex)fieldIndex)
			{
				case ReportProcessingTaskFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ReportProcessingTaskFieldIndex.TimeZoneId:
					DesetupSyncTimeZoneEntity(true, false);
					_alreadyFetchedTimeZoneEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReportProcessingTaskTemplateCollection = (_reportProcessingTaskTemplateCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedTimeZoneEntity = (_timeZoneEntity != null);
			_alreadyFetchedReportProcessingTaskFileEntity = (_reportProcessingTaskFileEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "TimeZoneEntity":
					toReturn.Add(Relations.TimeZoneEntityUsingTimeZoneId);
					break;
				case "ReportProcessingTaskTemplateCollection":
					toReturn.Add(Relations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskId);
					break;
				case "ReportProcessingTaskFileEntity":
					toReturn.Add(Relations.ReportProcessingTaskFileEntityUsingReportProcessingTaskId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_reportProcessingTaskTemplateCollection", (!this.MarkedForDeletion?_reportProcessingTaskTemplateCollection:null));
			info.AddValue("_alwaysFetchReportProcessingTaskTemplateCollection", _alwaysFetchReportProcessingTaskTemplateCollection);
			info.AddValue("_alreadyFetchedReportProcessingTaskTemplateCollection", _alreadyFetchedReportProcessingTaskTemplateCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_timeZoneEntity", (!this.MarkedForDeletion?_timeZoneEntity:null));
			info.AddValue("_timeZoneEntityReturnsNewIfNotFound", _timeZoneEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimeZoneEntity", _alwaysFetchTimeZoneEntity);
			info.AddValue("_alreadyFetchedTimeZoneEntity", _alreadyFetchedTimeZoneEntity);

			info.AddValue("_reportProcessingTaskFileEntity", (!this.MarkedForDeletion?_reportProcessingTaskFileEntity:null));
			info.AddValue("_reportProcessingTaskFileEntityReturnsNewIfNotFound", _reportProcessingTaskFileEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportProcessingTaskFileEntity", _alwaysFetchReportProcessingTaskFileEntity);
			info.AddValue("_alreadyFetchedReportProcessingTaskFileEntity", _alreadyFetchedReportProcessingTaskFileEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "TimeZoneEntity":
					_alreadyFetchedTimeZoneEntity = true;
					this.TimeZoneEntity = (TimeZoneEntity)entity;
					break;
				case "ReportProcessingTaskTemplateCollection":
					_alreadyFetchedReportProcessingTaskTemplateCollection = true;
					if(entity!=null)
					{
						this.ReportProcessingTaskTemplateCollection.Add((ReportProcessingTaskTemplateEntity)entity);
					}
					break;
				case "ReportProcessingTaskFileEntity":
					_alreadyFetchedReportProcessingTaskFileEntity = true;
					this.ReportProcessingTaskFileEntity = (ReportProcessingTaskFileEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "TimeZoneEntity":
					SetupSyncTimeZoneEntity(relatedEntity);
					break;
				case "ReportProcessingTaskTemplateCollection":
					_reportProcessingTaskTemplateCollection.Add((ReportProcessingTaskTemplateEntity)relatedEntity);
					break;
				case "ReportProcessingTaskFileEntity":
					SetupSyncReportProcessingTaskFileEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "TimeZoneEntity":
					DesetupSyncTimeZoneEntity(false, true);
					break;
				case "ReportProcessingTaskTemplateCollection":
					this.PerformRelatedEntityRemoval(_reportProcessingTaskTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportProcessingTaskFileEntity":
					DesetupSyncReportProcessingTaskFileEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_reportProcessingTaskFileEntity!=null)
			{
				toReturn.Add(_reportProcessingTaskFileEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_timeZoneEntity!=null)
			{
				toReturn.Add(_timeZoneEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_reportProcessingTaskTemplateCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskId)
		{
			return FetchUsingPK(reportProcessingTaskId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportProcessingTaskId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportProcessingTaskId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportProcessingTaskId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportProcessingTaskId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportProcessingTaskRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ReportProcessingTaskTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportProcessingTaskTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection GetMultiReportProcessingTaskTemplateCollection(bool forceFetch)
		{
			return GetMultiReportProcessingTaskTemplateCollection(forceFetch, _reportProcessingTaskTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportProcessingTaskTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportProcessingTaskTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection GetMultiReportProcessingTaskTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportProcessingTaskTemplateCollection(forceFetch, _reportProcessingTaskTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportProcessingTaskTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection GetMultiReportProcessingTaskTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportProcessingTaskTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportProcessingTaskTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection GetMultiReportProcessingTaskTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportProcessingTaskTemplateCollection || forceFetch || _alwaysFetchReportProcessingTaskTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportProcessingTaskTemplateCollection);
				_reportProcessingTaskTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_reportProcessingTaskTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_reportProcessingTaskTemplateCollection.GetMultiManyToOne(null, this, filter);
				_reportProcessingTaskTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReportProcessingTaskTemplateCollection = true;
			}
			return _reportProcessingTaskTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportProcessingTaskTemplateCollection'. These settings will be taken into account
		/// when the property ReportProcessingTaskTemplateCollection is requested or GetMultiReportProcessingTaskTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportProcessingTaskTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportProcessingTaskTemplateCollection.SortClauses=sortClauses;
			_reportProcessingTaskTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public TimeZoneEntity GetSingleTimeZoneEntity()
		{
			return GetSingleTimeZoneEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public virtual TimeZoneEntity GetSingleTimeZoneEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimeZoneEntity || forceFetch || _alwaysFetchTimeZoneEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimeZoneEntityUsingTimeZoneId);
				TimeZoneEntity newEntity = new TimeZoneEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TimeZoneId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TimeZoneEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timeZoneEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimeZoneEntity = newEntity;
				_alreadyFetchedTimeZoneEntity = fetchResult;
			}
			return _timeZoneEntity;
		}

		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskFileEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ReportProcessingTaskFileEntity' which is related to this entity.</returns>
		public ReportProcessingTaskFileEntity GetSingleReportProcessingTaskFileEntity()
		{
			return GetSingleReportProcessingTaskFileEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskFileEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportProcessingTaskFileEntity' which is related to this entity.</returns>
		public virtual ReportProcessingTaskFileEntity GetSingleReportProcessingTaskFileEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportProcessingTaskFileEntity || forceFetch || _alwaysFetchReportProcessingTaskFileEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportProcessingTaskFileEntityUsingReportProcessingTaskId);
				ReportProcessingTaskFileEntity newEntity = new ReportProcessingTaskFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCReportProcessingTaskId(this.ReportProcessingTaskId);
				}
				if(fetchResult)
				{
					newEntity = (ReportProcessingTaskFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportProcessingTaskFileEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportProcessingTaskFileEntity = newEntity;
				_alreadyFetchedReportProcessingTaskFileEntity = fetchResult;
			}
			return _reportProcessingTaskFileEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("TimeZoneEntity", _timeZoneEntity);
			toReturn.Add("ReportProcessingTaskTemplateCollection", _reportProcessingTaskTemplateCollection);
			toReturn.Add("ReportProcessingTaskFileEntity", _reportProcessingTaskFileEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="validator">The validator object for this ReportProcessingTaskEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 reportProcessingTaskId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportProcessingTaskId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_reportProcessingTaskTemplateCollection = new Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection();
			_reportProcessingTaskTemplateCollection.SetContainingEntityInfo(this, "ReportProcessingTaskEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_timeZoneEntityReturnsNewIfNotFound = true;
			_reportProcessingTaskFileEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsReportType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Processed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XTill", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Filter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Failed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExceptionText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalHandled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TillUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneOlsonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ReportProcessingTaskCollection", resetFKFields, new int[] { (int)ReportProcessingTaskFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timeZoneEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimeZoneEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, signalRelatedEntity, "ReportProcessingTaskCollection", resetFKFields, new int[] { (int)ReportProcessingTaskFieldIndex.TimeZoneId } );		
			_timeZoneEntity = null;
		}
		
		/// <summary> setups the sync logic for member _timeZoneEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimeZoneEntity(IEntityCore relatedEntity)
		{
			if(_timeZoneEntity!=relatedEntity)
			{		
				DesetupSyncTimeZoneEntity(true, true);
				_timeZoneEntity = (TimeZoneEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, ref _alreadyFetchedTimeZoneEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimeZoneEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportProcessingTaskFileEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportProcessingTaskFileEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportProcessingTaskFileEntity, new PropertyChangedEventHandler( OnReportProcessingTaskFileEntityPropertyChanged ), "ReportProcessingTaskFileEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.ReportProcessingTaskFileEntityUsingReportProcessingTaskIdStatic, false, signalRelatedEntity, "ReportProcessingTaskEntity", false, new int[] { (int)ReportProcessingTaskFieldIndex.ReportProcessingTaskId } );
			_reportProcessingTaskFileEntity = null;
		}
	
		/// <summary> setups the sync logic for member _reportProcessingTaskFileEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportProcessingTaskFileEntity(IEntityCore relatedEntity)
		{
			if(_reportProcessingTaskFileEntity!=relatedEntity)
			{
				DesetupSyncReportProcessingTaskFileEntity(true, true);
				_reportProcessingTaskFileEntity = (ReportProcessingTaskFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportProcessingTaskFileEntity, new PropertyChangedEventHandler( OnReportProcessingTaskFileEntityPropertyChanged ), "ReportProcessingTaskFileEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskRelations.ReportProcessingTaskFileEntityUsingReportProcessingTaskIdStatic, false, ref _alreadyFetchedReportProcessingTaskFileEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportProcessingTaskFileEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportProcessingTaskFieldIndex.ReportProcessingTaskId].ForcedCurrentValueWrite(reportProcessingTaskId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportProcessingTaskDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportProcessingTaskEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportProcessingTaskRelations Relations
		{
			get	{ return new ReportProcessingTaskRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTaskTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskTemplateCollection")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, 0, null, null, null, "ReportProcessingTaskTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TimeZone'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimeZoneEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimeZoneCollection(), (IEntityRelation)GetRelationsForField("TimeZoneEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, (int)Obymobi.Data.EntityType.TimeZoneEntity, 0, null, null, null, "TimeZoneEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTaskFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskFileEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskFileCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskFileEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskFileEntity, 0, null, null, null, "ReportProcessingTaskFileEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReportProcessingTaskId property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."ReportProcessingTaskId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReportProcessingTaskId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFieldIndex.ReportProcessingTaskId, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.ReportProcessingTaskId, value, true); }
		}

		/// <summary> The AnalyticsReportType property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."AnalyticsReportType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AnalyticsReportType AnalyticsReportType
		{
			get { return (Obymobi.Enums.AnalyticsReportType)GetValue((int)ReportProcessingTaskFieldIndex.AnalyticsReportType, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.AnalyticsReportType, value, true); }
		}

		/// <summary> The Processed property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."Processed"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Processed
		{
			get { return (System.Boolean)GetValue((int)ReportProcessingTaskFieldIndex.Processed, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.Processed, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReportProcessingTaskFieldIndex.CompanyId, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The XFrom property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."From"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> XFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.XFrom, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.XFrom, value, true); }
		}

		/// <summary> The XTill property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."Till"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> XTill
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.XTill, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.XTill, value, true); }
		}

		/// <summary> The Filter property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."Filter"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Filter
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskFieldIndex.Filter, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.Filter, value, true); }
		}

		/// <summary> The Failed property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."Failed"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Failed
		{
			get { return (System.Boolean)GetValue((int)ReportProcessingTaskFieldIndex.Failed, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.Failed, value, true); }
		}

		/// <summary> The ExceptionText property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."ExceptionText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExceptionText
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskFieldIndex.ExceptionText, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.ExceptionText, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ExternalTask property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."ExternalTask"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExternalTask
		{
			get { return (System.Boolean)GetValue((int)ReportProcessingTaskFieldIndex.ExternalTask, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.ExternalTask, value, true); }
		}

		/// <summary> The ExternalHandled property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."ExternalHandled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExternalHandled
		{
			get { return (System.Boolean)GetValue((int)ReportProcessingTaskFieldIndex.ExternalHandled, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.ExternalHandled, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The FromUTC property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."FromUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FromUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.FromUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.FromUTC, value, true); }
		}

		/// <summary> The TillUTC property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."TillUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TillUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskFieldIndex.TillUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.TillUTC, value, true); }
		}

		/// <summary> The TimeZoneId property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."TimeZoneId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeZoneId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReportProcessingTaskFieldIndex.TimeZoneId, false); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.TimeZoneId, value, true); }
		}

		/// <summary> The TimeZoneOlsonId property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."TimeZoneOlsonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.TimeZoneOlsonId, value, true); }
		}

		/// <summary> The Email property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2048<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskFieldIndex.Email, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.Email, value, true); }
		}

		/// <summary> The ReportName property of the Entity ReportProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTask"."ReportName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReportName
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskFieldIndex.ReportName, true); }
			set	{ SetValue((int)ReportProcessingTaskFieldIndex.ReportName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ReportProcessingTaskTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportProcessingTaskTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection ReportProcessingTaskTemplateCollection
		{
			get	{ return GetMultiReportProcessingTaskTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskTemplateCollection. When set to true, ReportProcessingTaskTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReportProcessingTaskTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskTemplateCollection
		{
			get	{ return _alwaysFetchReportProcessingTaskTemplateCollection; }
			set	{ _alwaysFetchReportProcessingTaskTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskTemplateCollection already has been fetched. Setting this property to false when ReportProcessingTaskTemplateCollection has been fetched
		/// will clear the ReportProcessingTaskTemplateCollection collection well. Setting this property to true while ReportProcessingTaskTemplateCollection hasn't been fetched disables lazy loading for ReportProcessingTaskTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskTemplateCollection
		{
			get { return _alreadyFetchedReportProcessingTaskTemplateCollection;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskTemplateCollection && !value && (_reportProcessingTaskTemplateCollection != null))
				{
					_reportProcessingTaskTemplateCollection.Clear();
				}
				_alreadyFetchedReportProcessingTaskTemplateCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportProcessingTaskCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimeZoneEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimeZoneEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimeZoneEntity TimeZoneEntity
		{
			get	{ return GetSingleTimeZoneEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTimeZoneEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportProcessingTaskCollection", "TimeZoneEntity", _timeZoneEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimeZoneEntity. When set to true, TimeZoneEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimeZoneEntity is accessed. You can always execute a forced fetch by calling GetSingleTimeZoneEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimeZoneEntity
		{
			get	{ return _alwaysFetchTimeZoneEntity; }
			set	{ _alwaysFetchTimeZoneEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimeZoneEntity already has been fetched. Setting this property to false when TimeZoneEntity has been fetched
		/// will set TimeZoneEntity to null as well. Setting this property to true while TimeZoneEntity hasn't been fetched disables lazy loading for TimeZoneEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimeZoneEntity
		{
			get { return _alreadyFetchedTimeZoneEntity;}
			set 
			{
				if(_alreadyFetchedTimeZoneEntity && !value)
				{
					this.TimeZoneEntity = null;
				}
				_alreadyFetchedTimeZoneEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimeZoneEntity is not found
		/// in the database. When set to true, TimeZoneEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimeZoneEntityReturnsNewIfNotFound
		{
			get	{ return _timeZoneEntityReturnsNewIfNotFound; }
			set { _timeZoneEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportProcessingTaskFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportProcessingTaskFileEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReportProcessingTaskFileEntity ReportProcessingTaskFileEntity
		{
			get	{ return GetSingleReportProcessingTaskFileEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncReportProcessingTaskFileEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_reportProcessingTaskFileEntity !=null);
						DesetupSyncReportProcessingTaskFileEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ReportProcessingTaskFileEntity");
						}
					}
					else
					{
						if(_reportProcessingTaskFileEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ReportProcessingTaskEntity");
							SetupSyncReportProcessingTaskFileEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskFileEntity. When set to true, ReportProcessingTaskFileEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskFileEntity is accessed. You can always execute a forced fetch by calling GetSingleReportProcessingTaskFileEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskFileEntity
		{
			get	{ return _alwaysFetchReportProcessingTaskFileEntity; }
			set	{ _alwaysFetchReportProcessingTaskFileEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskFileEntity already has been fetched. Setting this property to false when ReportProcessingTaskFileEntity has been fetched
		/// will set ReportProcessingTaskFileEntity to null as well. Setting this property to true while ReportProcessingTaskFileEntity hasn't been fetched disables lazy loading for ReportProcessingTaskFileEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskFileEntity
		{
			get { return _alreadyFetchedReportProcessingTaskFileEntity;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskFileEntity && !value)
				{
					this.ReportProcessingTaskFileEntity = null;
				}
				_alreadyFetchedReportProcessingTaskFileEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportProcessingTaskFileEntity is not found
		/// in the database. When set to true, ReportProcessingTaskFileEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReportProcessingTaskFileEntityReturnsNewIfNotFound
		{
			get	{ return _reportProcessingTaskFileEntityReturnsNewIfNotFound; }
			set	{ _reportProcessingTaskFileEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
