﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PointOfInterestVenueCategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PointOfInterestVenueCategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PointOfInterestVenueCategoryEntity"; }
		}
	
		#region Class Member Declarations
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;
		private VenueCategoryEntity _venueCategoryEntity;
		private bool	_alwaysFetchVenueCategoryEntity, _alreadyFetchedVenueCategoryEntity, _venueCategoryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
			/// <summary>Member name VenueCategoryEntity</summary>
			public static readonly string VenueCategoryEntity = "VenueCategoryEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PointOfInterestVenueCategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PointOfInterestVenueCategoryEntityBase() :base("PointOfInterestVenueCategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		protected PointOfInterestVenueCategoryEntityBase(System.Int32 pointOfInterestVenueCategoryId):base("PointOfInterestVenueCategoryEntity")
		{
			InitClassFetch(pointOfInterestVenueCategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PointOfInterestVenueCategoryEntityBase(System.Int32 pointOfInterestVenueCategoryId, IPrefetchPath prefetchPathToUse): base("PointOfInterestVenueCategoryEntity")
		{
			InitClassFetch(pointOfInterestVenueCategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="validator">The custom validator object for this PointOfInterestVenueCategoryEntity</param>
		protected PointOfInterestVenueCategoryEntityBase(System.Int32 pointOfInterestVenueCategoryId, IValidator validator):base("PointOfInterestVenueCategoryEntity")
		{
			InitClassFetch(pointOfInterestVenueCategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfInterestVenueCategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");

			_venueCategoryEntity = (VenueCategoryEntity)info.GetValue("_venueCategoryEntity", typeof(VenueCategoryEntity));
			if(_venueCategoryEntity!=null)
			{
				_venueCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_venueCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_venueCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchVenueCategoryEntity = info.GetBoolean("_alwaysFetchVenueCategoryEntity");
			_alreadyFetchedVenueCategoryEntity = info.GetBoolean("_alreadyFetchedVenueCategoryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PointOfInterestVenueCategoryFieldIndex)fieldIndex)
			{
				case PointOfInterestVenueCategoryFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				case PointOfInterestVenueCategoryFieldIndex.VenueCategoryId:
					DesetupSyncVenueCategoryEntity(true, false);
					_alreadyFetchedVenueCategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
			_alreadyFetchedVenueCategoryEntity = (_venueCategoryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				case "VenueCategoryEntity":
					toReturn.Add(Relations.VenueCategoryEntityUsingVenueCategoryId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);
			info.AddValue("_venueCategoryEntity", (!this.MarkedForDeletion?_venueCategoryEntity:null));
			info.AddValue("_venueCategoryEntityReturnsNewIfNotFound", _venueCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVenueCategoryEntity", _alwaysFetchVenueCategoryEntity);
			info.AddValue("_alreadyFetchedVenueCategoryEntity", _alreadyFetchedVenueCategoryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				case "VenueCategoryEntity":
					_alreadyFetchedVenueCategoryEntity = true;
					this.VenueCategoryEntity = (VenueCategoryEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				case "VenueCategoryEntity":
					SetupSyncVenueCategoryEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				case "VenueCategoryEntity":
					DesetupSyncVenueCategoryEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			if(_venueCategoryEntity!=null)
			{
				toReturn.Add(_venueCategoryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="venueCategoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdVenueCategoryId(System.Int32 pointOfInterestId, System.Int32 venueCategoryId)
		{
			return FetchUsingUCPointOfInterestIdVenueCategoryId( pointOfInterestId,  venueCategoryId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="venueCategoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdVenueCategoryId(System.Int32 pointOfInterestId, System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPointOfInterestIdVenueCategoryId( pointOfInterestId,  venueCategoryId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="venueCategoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdVenueCategoryId(System.Int32 pointOfInterestId, System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPointOfInterestIdVenueCategoryId( pointOfInterestId,  venueCategoryId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="venueCategoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdVenueCategoryId(System.Int32 pointOfInterestId, System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PointOfInterestVenueCategoryDAO)CreateDAOInstance()).FetchPointOfInterestVenueCategoryUsingUCPointOfInterestIdVenueCategoryId(this, this.Transaction, pointOfInterestId, venueCategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestVenueCategoryId)
		{
			return FetchUsingPK(pointOfInterestVenueCategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestVenueCategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pointOfInterestVenueCategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestVenueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pointOfInterestVenueCategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestVenueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pointOfInterestVenueCategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PointOfInterestVenueCategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PointOfInterestVenueCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId);
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}


		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public VenueCategoryEntity GetSingleVenueCategoryEntity()
		{
			return GetSingleVenueCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public virtual VenueCategoryEntity GetSingleVenueCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedVenueCategoryEntity || forceFetch || _alwaysFetchVenueCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VenueCategoryEntityUsingVenueCategoryId);
				VenueCategoryEntity newEntity = new VenueCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VenueCategoryId);
				}
				if(fetchResult)
				{
					newEntity = (VenueCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_venueCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VenueCategoryEntity = newEntity;
				_alreadyFetchedVenueCategoryEntity = fetchResult;
			}
			return _venueCategoryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			toReturn.Add("VenueCategoryEntity", _venueCategoryEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="validator">The validator object for this PointOfInterestVenueCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pointOfInterestVenueCategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pointOfInterestVenueCategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			_venueCategoryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestVenueCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VenueCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestVenueCategoryRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "PointOfInterestVenueCategoryCollection", resetFKFields, new int[] { (int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestId } );		
			_pointOfInterestEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{		
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestVenueCategoryRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _venueCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVenueCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _venueCategoryEntity, new PropertyChangedEventHandler( OnVenueCategoryEntityPropertyChanged ), "VenueCategoryEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestVenueCategoryRelations.VenueCategoryEntityUsingVenueCategoryIdStatic, true, signalRelatedEntity, "PointOfInterestVenueCategoryCollection", resetFKFields, new int[] { (int)PointOfInterestVenueCategoryFieldIndex.VenueCategoryId } );		
			_venueCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _venueCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVenueCategoryEntity(IEntityCore relatedEntity)
		{
			if(_venueCategoryEntity!=relatedEntity)
			{		
				DesetupSyncVenueCategoryEntity(true, true);
				_venueCategoryEntity = (VenueCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _venueCategoryEntity, new PropertyChangedEventHandler( OnVenueCategoryEntityPropertyChanged ), "VenueCategoryEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestVenueCategoryRelations.VenueCategoryEntityUsingVenueCategoryIdStatic, true, ref _alreadyFetchedVenueCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVenueCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pointOfInterestVenueCategoryId">PK value for PointOfInterestVenueCategory which data should be fetched into this PointOfInterestVenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pointOfInterestVenueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestVenueCategoryId].ForcedCurrentValueWrite(pointOfInterestVenueCategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePointOfInterestVenueCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PointOfInterestVenueCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PointOfInterestVenueCategoryRelations Relations
		{
			get	{ return new PointOfInterestVenueCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVenueCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryCollection(), (IEntityRelation)GetRelationsForField("VenueCategoryEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity, (int)Obymobi.Data.EntityType.VenueCategoryEntity, 0, null, null, null, "VenueCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PointOfInterestVenueCategoryId property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."PointOfInterestVenueCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PointOfInterestVenueCategoryId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestVenueCategoryId, true); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestVenueCategoryId, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PointOfInterestId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestId, true); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The VenueCategoryId property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."VenueCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VenueCategoryId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestVenueCategoryFieldIndex.VenueCategoryId, true); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.VenueCategoryId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestVenueCategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestVenueCategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestVenueCategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PointOfInterestVenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestVenueCategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestVenueCategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PointOfInterestVenueCategoryFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestVenueCategoryCollection", "PointOfInterestEntity", _pointOfInterestEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set { _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VenueCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVenueCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual VenueCategoryEntity VenueCategoryEntity
		{
			get	{ return GetSingleVenueCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVenueCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestVenueCategoryCollection", "VenueCategoryEntity", _venueCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VenueCategoryEntity. When set to true, VenueCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VenueCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleVenueCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVenueCategoryEntity
		{
			get	{ return _alwaysFetchVenueCategoryEntity; }
			set	{ _alwaysFetchVenueCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VenueCategoryEntity already has been fetched. Setting this property to false when VenueCategoryEntity has been fetched
		/// will set VenueCategoryEntity to null as well. Setting this property to true while VenueCategoryEntity hasn't been fetched disables lazy loading for VenueCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVenueCategoryEntity
		{
			get { return _alreadyFetchedVenueCategoryEntity;}
			set 
			{
				if(_alreadyFetchedVenueCategoryEntity && !value)
				{
					this.VenueCategoryEntity = null;
				}
				_alreadyFetchedVenueCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VenueCategoryEntity is not found
		/// in the database. When set to true, VenueCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool VenueCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _venueCategoryEntityReturnsNewIfNotFound; }
			set { _venueCategoryEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
