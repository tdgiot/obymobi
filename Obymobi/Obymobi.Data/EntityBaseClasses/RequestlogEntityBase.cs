﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Requestlog'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RequestlogEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RequestlogEntity"; }
		}
	
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RequestlogEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RequestlogEntityBase() :base("RequestlogEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		protected RequestlogEntityBase(System.Int64 requestlogId):base("RequestlogEntity")
		{
			InitClassFetch(requestlogId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RequestlogEntityBase(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse): base("RequestlogEntity")
		{
			InitClassFetch(requestlogId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="validator">The custom validator object for this RequestlogEntity</param>
		protected RequestlogEntityBase(System.Int64 requestlogId, IValidator validator):base("RequestlogEntity")
		{
			InitClassFetch(requestlogId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RequestlogEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 requestlogId)
		{
			return FetchUsingPK(requestlogId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(requestlogId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(requestlogId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(requestlogId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RequestlogId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RequestlogRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="validator">The validator object for this RequestlogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 requestlogId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(requestlogId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestlogId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserAgent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SourceApplication", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Identifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MethodName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultEnumTypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultEnumValueName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultBody", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorStackTrace", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Xml", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawRequest", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TraceFromMobile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileVendor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobilePlatform", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileOs", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Log", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RequestlogFieldIndex.RequestlogId].ForcedCurrentValueWrite(requestlogId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRequestlogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RequestlogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RequestlogRelations Relations
		{
			get	{ return new RequestlogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RequestlogId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."RequestlogId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RequestlogId
		{
			get { return (System.Int64)GetValue((int)RequestlogFieldIndex.RequestlogId, true); }
			set	{ SetValue((int)RequestlogFieldIndex.RequestlogId, value, true); }
		}

		/// <summary> The UserAgent property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UserAgent"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserAgent
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.UserAgent, true); }
			set	{ SetValue((int)RequestlogFieldIndex.UserAgent, value, true); }
		}

		/// <summary> The SourceApplication property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."SourceApplication"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SourceApplication
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.SourceApplication, true); }
			set	{ SetValue((int)RequestlogFieldIndex.SourceApplication, value, true); }
		}

		/// <summary> The Identifier property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Identifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Identifier, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Identifier, value, true); }
		}

		/// <summary> The CustomerId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.CustomerId, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The TerminalId property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.TerminalId, false); }
			set	{ SetValue((int)RequestlogFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The MethodName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MethodName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MethodName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MethodName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MethodName, value, true); }
		}

		/// <summary> The ResultEnumTypeName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultEnumTypeName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultEnumTypeName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultEnumTypeName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultEnumTypeName, value, true); }
		}

		/// <summary> The ResultEnumValueName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultEnumValueName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultEnumValueName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultEnumValueName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultEnumValueName, value, true); }
		}

		/// <summary> The ResultCode property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultCode
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultCode, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultCode, value, true); }
		}

		/// <summary> The ResultMessage property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultMessage
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultMessage, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultMessage, value, true); }
		}

		/// <summary> The Parameter1 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter1
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter1, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter1, value, true); }
		}

		/// <summary> The Parameter2 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter2
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter2, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter2, value, true); }
		}

		/// <summary> The Parameter3 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter3
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter3, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter3, value, true); }
		}

		/// <summary> The Parameter4 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter4
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter4, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter4, value, true); }
		}

		/// <summary> The Parameter5 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter5
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter5, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter5, value, true); }
		}

		/// <summary> The Parameter6 property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Parameter6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter6
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Parameter6, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Parameter6, value, true); }
		}

		/// <summary> The ResultBody property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ResultBody"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultBody
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ResultBody, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ResultBody, value, true); }
		}

		/// <summary> The ErrorMessage property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ErrorMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorMessage
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ErrorMessage, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ErrorMessage, value, true); }
		}

		/// <summary> The ErrorStackTrace property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ErrorStackTrace"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorStackTrace
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ErrorStackTrace, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ErrorStackTrace, value, true); }
		}

		/// <summary> The Xml property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Xml"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Xml
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Xml, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Xml, value, true); }
		}

		/// <summary> The RawRequest property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."RawRequest"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RawRequest
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.RawRequest, true); }
			set	{ SetValue((int)RequestlogFieldIndex.RawRequest, value, true); }
		}

		/// <summary> The TraceFromMobile property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."TraceFromMobile"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TraceFromMobile
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.TraceFromMobile, true); }
			set	{ SetValue((int)RequestlogFieldIndex.TraceFromMobile, value, true); }
		}

		/// <summary> The MobileVendor property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileVendor"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileVendor
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileVendor, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileVendor, value, true); }
		}

		/// <summary> The MobileIdentifier property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileIdentifier
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileIdentifier, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileIdentifier, value, true); }
		}

		/// <summary> The MobilePlatform property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobilePlatform"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobilePlatform
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobilePlatform, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobilePlatform, value, true); }
		}

		/// <summary> The MobileOs property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileOs"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileOs
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileOs, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileOs, value, true); }
		}

		/// <summary> The MobileName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."MobileName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MobileName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.MobileName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.MobileName, value, true); }
		}

		/// <summary> The DeviceInfo property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."DeviceInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeviceInfo
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.DeviceInfo, true); }
			set	{ SetValue((int)RequestlogFieldIndex.DeviceInfo, value, true); }
		}

		/// <summary> The Log property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."Log"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.Log, true); }
			set	{ SetValue((int)RequestlogFieldIndex.Log, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RequestlogFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RequestlogFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ServerName property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."ServerName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServerName
		{
			get { return (System.String)GetValue((int)RequestlogFieldIndex.ServerName, true); }
			set	{ SetValue((int)RequestlogFieldIndex.ServerName, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RequestlogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RequestlogFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Requestlog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Requestlog"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RequestlogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RequestlogFieldIndex.UpdatedUTC, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RequestlogEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
