﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'DeliverypointgroupAnnouncement'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointgroupAnnouncementEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeliverypointgroupAnnouncementEntity"; }
		}
	
		#region Class Member Declarations
		private AnnouncementEntity _announcementEntity;
		private bool	_alwaysFetchAnnouncementEntity, _alreadyFetchedAnnouncementEntity, _announcementEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AnnouncementEntity</summary>
			public static readonly string AnnouncementEntity = "AnnouncementEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointgroupAnnouncementEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeliverypointgroupAnnouncementEntityBase() :base("DeliverypointgroupAnnouncementEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		protected DeliverypointgroupAnnouncementEntityBase(System.Int32 deliverypointAnnouncementId):base("DeliverypointgroupAnnouncementEntity")
		{
			InitClassFetch(deliverypointAnnouncementId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeliverypointgroupAnnouncementEntityBase(System.Int32 deliverypointAnnouncementId, IPrefetchPath prefetchPathToUse): base("DeliverypointgroupAnnouncementEntity")
		{
			InitClassFetch(deliverypointAnnouncementId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupAnnouncementEntity</param>
		protected DeliverypointgroupAnnouncementEntityBase(System.Int32 deliverypointAnnouncementId, IValidator validator):base("DeliverypointgroupAnnouncementEntity")
		{
			InitClassFetch(deliverypointAnnouncementId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupAnnouncementEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_announcementEntity = (AnnouncementEntity)info.GetValue("_announcementEntity", typeof(AnnouncementEntity));
			if(_announcementEntity!=null)
			{
				_announcementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_announcementEntityReturnsNewIfNotFound = info.GetBoolean("_announcementEntityReturnsNewIfNotFound");
			_alwaysFetchAnnouncementEntity = info.GetBoolean("_alwaysFetchAnnouncementEntity");
			_alreadyFetchedAnnouncementEntity = info.GetBoolean("_alreadyFetchedAnnouncementEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointgroupAnnouncementFieldIndex)fieldIndex)
			{
				case DeliverypointgroupAnnouncementFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case DeliverypointgroupAnnouncementFieldIndex.AnnouncementId:
					DesetupSyncAnnouncementEntity(true, false);
					_alreadyFetchedAnnouncementEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAnnouncementEntity = (_announcementEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AnnouncementEntity":
					toReturn.Add(Relations.AnnouncementEntityUsingAnnouncementId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_announcementEntity", (!this.MarkedForDeletion?_announcementEntity:null));
			info.AddValue("_announcementEntityReturnsNewIfNotFound", _announcementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAnnouncementEntity", _alwaysFetchAnnouncementEntity);
			info.AddValue("_alreadyFetchedAnnouncementEntity", _alreadyFetchedAnnouncementEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AnnouncementEntity":
					_alreadyFetchedAnnouncementEntity = true;
					this.AnnouncementEntity = (AnnouncementEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AnnouncementEntity":
					SetupSyncAnnouncementEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AnnouncementEntity":
					DesetupSyncAnnouncementEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_announcementEntity!=null)
			{
				toReturn.Add(_announcementEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointAnnouncementId)
		{
			return FetchUsingPK(deliverypointAnnouncementId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointAnnouncementId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointAnnouncementId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointAnnouncementId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deliverypointAnnouncementId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointAnnouncementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointAnnouncementId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointAnnouncementId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointgroupAnnouncementRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public AnnouncementEntity GetSingleAnnouncementEntity()
		{
			return GetSingleAnnouncementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public virtual AnnouncementEntity GetSingleAnnouncementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAnnouncementEntity || forceFetch || _alwaysFetchAnnouncementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AnnouncementEntityUsingAnnouncementId);
				AnnouncementEntity newEntity = new AnnouncementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AnnouncementId);
				}
				if(fetchResult)
				{
					newEntity = (AnnouncementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_announcementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AnnouncementEntity = newEntity;
				_alreadyFetchedAnnouncementEntity = fetchResult;
			}
			return _announcementEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId);
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AnnouncementEntity", _announcementEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="validator">The validator object for this DeliverypointgroupAnnouncementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deliverypointAnnouncementId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deliverypointAnnouncementId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_announcementEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointAnnouncementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnnouncementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _announcementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAnnouncementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupAnnouncementRelations.AnnouncementEntityUsingAnnouncementIdStatic, true, signalRelatedEntity, "DeliverypointgroupAnnouncementCollection", resetFKFields, new int[] { (int)DeliverypointgroupAnnouncementFieldIndex.AnnouncementId } );		
			_announcementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _announcementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAnnouncementEntity(IEntityCore relatedEntity)
		{
			if(_announcementEntity!=relatedEntity)
			{		
				DesetupSyncAnnouncementEntity(true, true);
				_announcementEntity = (AnnouncementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupAnnouncementRelations.AnnouncementEntityUsingAnnouncementIdStatic, true, ref _alreadyFetchedAnnouncementEntity, new string[] { "AnnouncementDateToShow", "AnnouncementRecurring", "AnnouncementTimeToShow", "AnnouncementTitle" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAnnouncementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "DateToShow":
					this.OnPropertyChanged("AnnouncementDateToShow");
					break;
				case "Recurring":
					this.OnPropertyChanged("AnnouncementRecurring");
					break;
				case "TimeToShow":
					this.OnPropertyChanged("AnnouncementTimeToShow");
					break;
				case "Title":
					this.OnPropertyChanged("AnnouncementTitle");
					break;
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupAnnouncementRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "DeliverypointgroupAnnouncementCollection", resetFKFields, new int[] { (int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupAnnouncementRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointAnnouncementId">PK value for DeliverypointgroupAnnouncement which data should be fetched into this DeliverypointgroupAnnouncement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointAnnouncementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointAnnouncementId].ForcedCurrentValueWrite(deliverypointAnnouncementId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointgroupAnnouncementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointgroupAnnouncementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointgroupAnnouncementRelations Relations
		{
			get	{ return new DeliverypointgroupAnnouncementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointAnnouncementId property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."DeliverypointAnnouncementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointAnnouncementId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointAnnouncementId, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointAnnouncementId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The AnnouncementId property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."AnnouncementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AnnouncementId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.AnnouncementId, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.AnnouncementId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity DeliverypointgroupAnnouncement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupAnnouncement"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupAnnouncementFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupAnnouncementFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AnnouncementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAnnouncementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AnnouncementEntity AnnouncementEntity
		{
			get	{ return GetSingleAnnouncementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAnnouncementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupAnnouncementCollection", "AnnouncementEntity", _announcementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementEntity. When set to true, AnnouncementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementEntity is accessed. You can always execute a forced fetch by calling GetSingleAnnouncementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementEntity
		{
			get	{ return _alwaysFetchAnnouncementEntity; }
			set	{ _alwaysFetchAnnouncementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementEntity already has been fetched. Setting this property to false when AnnouncementEntity has been fetched
		/// will set AnnouncementEntity to null as well. Setting this property to true while AnnouncementEntity hasn't been fetched disables lazy loading for AnnouncementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementEntity
		{
			get { return _alreadyFetchedAnnouncementEntity;}
			set 
			{
				if(_alreadyFetchedAnnouncementEntity && !value)
				{
					this.AnnouncementEntity = null;
				}
				_alreadyFetchedAnnouncementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AnnouncementEntity is not found
		/// in the database. When set to true, AnnouncementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AnnouncementEntityReturnsNewIfNotFound
		{
			get	{ return _announcementEntityReturnsNewIfNotFound; }
			set { _announcementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupAnnouncementCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

 
		/// <summary> Gets / Sets the value of the related field this.AnnouncementEntity.DateToShow.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual Nullable<System.DateTime> AnnouncementDateToShow
		{
			get
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				return relatedEntity==null ? (Nullable<System.DateTime>)TypeDefaultValue.GetDefaultValue(typeof(Nullable<System.DateTime>)) : relatedEntity.DateToShow;
			}
			set
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				if(relatedEntity!=null)
				{
					relatedEntity.DateToShow = value;
				}				
			}
		}
 
		/// <summary> Gets / Sets the value of the related field this.AnnouncementEntity.Recurring.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual Nullable<System.Boolean> AnnouncementRecurring
		{
			get
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				return relatedEntity==null ? (Nullable<System.Boolean>)TypeDefaultValue.GetDefaultValue(typeof(Nullable<System.Boolean>)) : relatedEntity.Recurring;
			}
			set
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				if(relatedEntity!=null)
				{
					relatedEntity.Recurring = value;
				}				
			}
		}
 
		/// <summary> Gets / Sets the value of the related field this.AnnouncementEntity.TimeToShow.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual Nullable<System.DateTime> AnnouncementTimeToShow
		{
			get
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				return relatedEntity==null ? (Nullable<System.DateTime>)TypeDefaultValue.GetDefaultValue(typeof(Nullable<System.DateTime>)) : relatedEntity.TimeToShow;
			}
			set
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				if(relatedEntity!=null)
				{
					relatedEntity.TimeToShow = value;
				}				
			}
		}
 
		/// <summary> Gets / Sets the value of the related field this.AnnouncementEntity.Title.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String AnnouncementTitle
		{
			get
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Title;
			}
			set
			{
				AnnouncementEntity relatedEntity = this.AnnouncementEntity;
				if(relatedEntity!=null)
				{
					relatedEntity.Title = value;
				}				
			}
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
