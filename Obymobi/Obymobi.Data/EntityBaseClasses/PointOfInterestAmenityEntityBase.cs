﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PointOfInterestAmenity'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PointOfInterestAmenityEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PointOfInterestAmenityEntity"; }
		}
	
		#region Class Member Declarations
		private AmenityEntity _amenityEntity;
		private bool	_alwaysFetchAmenityEntity, _alreadyFetchedAmenityEntity, _amenityEntityReturnsNewIfNotFound;
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AmenityEntity</summary>
			public static readonly string AmenityEntity = "AmenityEntity";
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PointOfInterestAmenityEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PointOfInterestAmenityEntityBase() :base("PointOfInterestAmenityEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		protected PointOfInterestAmenityEntityBase(System.Int32 pointOfInterestAmenityId):base("PointOfInterestAmenityEntity")
		{
			InitClassFetch(pointOfInterestAmenityId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PointOfInterestAmenityEntityBase(System.Int32 pointOfInterestAmenityId, IPrefetchPath prefetchPathToUse): base("PointOfInterestAmenityEntity")
		{
			InitClassFetch(pointOfInterestAmenityId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="validator">The custom validator object for this PointOfInterestAmenityEntity</param>
		protected PointOfInterestAmenityEntityBase(System.Int32 pointOfInterestAmenityId, IValidator validator):base("PointOfInterestAmenityEntity")
		{
			InitClassFetch(pointOfInterestAmenityId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfInterestAmenityEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_amenityEntity = (AmenityEntity)info.GetValue("_amenityEntity", typeof(AmenityEntity));
			if(_amenityEntity!=null)
			{
				_amenityEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_amenityEntityReturnsNewIfNotFound = info.GetBoolean("_amenityEntityReturnsNewIfNotFound");
			_alwaysFetchAmenityEntity = info.GetBoolean("_alwaysFetchAmenityEntity");
			_alreadyFetchedAmenityEntity = info.GetBoolean("_alreadyFetchedAmenityEntity");

			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PointOfInterestAmenityFieldIndex)fieldIndex)
			{
				case PointOfInterestAmenityFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				case PointOfInterestAmenityFieldIndex.AmenityId:
					DesetupSyncAmenityEntity(true, false);
					_alreadyFetchedAmenityEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAmenityEntity = (_amenityEntity != null);
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AmenityEntity":
					toReturn.Add(Relations.AmenityEntityUsingAmenityId);
					break;
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_amenityEntity", (!this.MarkedForDeletion?_amenityEntity:null));
			info.AddValue("_amenityEntityReturnsNewIfNotFound", _amenityEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAmenityEntity", _alwaysFetchAmenityEntity);
			info.AddValue("_alreadyFetchedAmenityEntity", _alreadyFetchedAmenityEntity);
			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AmenityEntity":
					_alreadyFetchedAmenityEntity = true;
					this.AmenityEntity = (AmenityEntity)entity;
					break;
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AmenityEntity":
					SetupSyncAmenityEntity(relatedEntity);
					break;
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AmenityEntity":
					DesetupSyncAmenityEntity(false, true);
					break;
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_amenityEntity!=null)
			{
				toReturn.Add(_amenityEntity);
			}
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="amenityId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdAmenityId(System.Int32 pointOfInterestId, System.Int32 amenityId)
		{
			return FetchUsingUCPointOfInterestIdAmenityId( pointOfInterestId,  amenityId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="amenityId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdAmenityId(System.Int32 pointOfInterestId, System.Int32 amenityId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPointOfInterestIdAmenityId( pointOfInterestId,  amenityId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="amenityId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdAmenityId(System.Int32 pointOfInterestId, System.Int32 amenityId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPointOfInterestIdAmenityId( pointOfInterestId,  amenityId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="amenityId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestIdAmenityId(System.Int32 pointOfInterestId, System.Int32 amenityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PointOfInterestAmenityDAO)CreateDAOInstance()).FetchPointOfInterestAmenityUsingUCPointOfInterestIdAmenityId(this, this.Transaction, pointOfInterestId, amenityId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestAmenityId)
		{
			return FetchUsingPK(pointOfInterestAmenityId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestAmenityId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pointOfInterestAmenityId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestAmenityId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pointOfInterestAmenityId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestAmenityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pointOfInterestAmenityId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PointOfInterestAmenityId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PointOfInterestAmenityRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AmenityEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AmenityEntity' which is related to this entity.</returns>
		public AmenityEntity GetSingleAmenityEntity()
		{
			return GetSingleAmenityEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AmenityEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AmenityEntity' which is related to this entity.</returns>
		public virtual AmenityEntity GetSingleAmenityEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAmenityEntity || forceFetch || _alwaysFetchAmenityEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AmenityEntityUsingAmenityId);
				AmenityEntity newEntity = new AmenityEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AmenityId);
				}
				if(fetchResult)
				{
					newEntity = (AmenityEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_amenityEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AmenityEntity = newEntity;
				_alreadyFetchedAmenityEntity = fetchResult;
			}
			return _amenityEntity;
		}


		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId);
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AmenityEntity", _amenityEntity);
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="validator">The validator object for this PointOfInterestAmenityEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pointOfInterestAmenityId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pointOfInterestAmenityId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_amenityEntityReturnsNewIfNotFound = true;
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestAmenityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmenityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _amenityEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAmenityEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _amenityEntity, new PropertyChangedEventHandler( OnAmenityEntityPropertyChanged ), "AmenityEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestAmenityRelations.AmenityEntityUsingAmenityIdStatic, true, signalRelatedEntity, "PointOfInterestAmenityCollection", resetFKFields, new int[] { (int)PointOfInterestAmenityFieldIndex.AmenityId } );		
			_amenityEntity = null;
		}
		
		/// <summary> setups the sync logic for member _amenityEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAmenityEntity(IEntityCore relatedEntity)
		{
			if(_amenityEntity!=relatedEntity)
			{		
				DesetupSyncAmenityEntity(true, true);
				_amenityEntity = (AmenityEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _amenityEntity, new PropertyChangedEventHandler( OnAmenityEntityPropertyChanged ), "AmenityEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestAmenityRelations.AmenityEntityUsingAmenityIdStatic, true, ref _alreadyFetchedAmenityEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAmenityEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestAmenityRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "PointOfInterestAmenityCollection", resetFKFields, new int[] { (int)PointOfInterestAmenityFieldIndex.PointOfInterestId } );		
			_pointOfInterestEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{		
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestAmenityRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pointOfInterestAmenityId">PK value for PointOfInterestAmenity which data should be fetched into this PointOfInterestAmenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pointOfInterestAmenityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PointOfInterestAmenityFieldIndex.PointOfInterestAmenityId].ForcedCurrentValueWrite(pointOfInterestAmenityId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePointOfInterestAmenityDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PointOfInterestAmenityEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PointOfInterestAmenityRelations Relations
		{
			get	{ return new PointOfInterestAmenityRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Amenity'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmenityEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AmenityCollection(), (IEntityRelation)GetRelationsForField("AmenityEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestAmenityEntity, (int)Obymobi.Data.EntityType.AmenityEntity, 0, null, null, null, "AmenityEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestAmenityEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PointOfInterestAmenityId property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."PointOfInterestAmenityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PointOfInterestAmenityId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestAmenityFieldIndex.PointOfInterestAmenityId, true); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.PointOfInterestAmenityId, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PointOfInterestId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestAmenityFieldIndex.PointOfInterestId, true); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The AmenityId property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."AmenityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AmenityId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestAmenityFieldIndex.AmenityId, true); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.AmenityId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestAmenityFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestAmenityFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestAmenityFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PointOfInterestAmenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterestAmenity"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestAmenityFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PointOfInterestAmenityFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AmenityEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAmenityEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AmenityEntity AmenityEntity
		{
			get	{ return GetSingleAmenityEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAmenityEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestAmenityCollection", "AmenityEntity", _amenityEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AmenityEntity. When set to true, AmenityEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmenityEntity is accessed. You can always execute a forced fetch by calling GetSingleAmenityEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmenityEntity
		{
			get	{ return _alwaysFetchAmenityEntity; }
			set	{ _alwaysFetchAmenityEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmenityEntity already has been fetched. Setting this property to false when AmenityEntity has been fetched
		/// will set AmenityEntity to null as well. Setting this property to true while AmenityEntity hasn't been fetched disables lazy loading for AmenityEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmenityEntity
		{
			get { return _alreadyFetchedAmenityEntity;}
			set 
			{
				if(_alreadyFetchedAmenityEntity && !value)
				{
					this.AmenityEntity = null;
				}
				_alreadyFetchedAmenityEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AmenityEntity is not found
		/// in the database. When set to true, AmenityEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AmenityEntityReturnsNewIfNotFound
		{
			get	{ return _amenityEntityReturnsNewIfNotFound; }
			set { _amenityEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestAmenityCollection", "PointOfInterestEntity", _pointOfInterestEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set { _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PointOfInterestAmenityEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
