﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'VenueCategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class VenueCategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "VenueCategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection	_companyVenueCategoryCollection;
		private bool	_alwaysFetchCompanyVenueCategoryCollection, _alreadyFetchedCompanyVenueCategoryCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection	_pointOfInterestVenueCategoryCollection;
		private bool	_alwaysFetchPointOfInterestVenueCategoryCollection, _alreadyFetchedPointOfInterestVenueCategoryCollection;
		private Obymobi.Data.CollectionClasses.VenueCategoryCollection	_childVenueCategoryCollection;
		private bool	_alwaysFetchChildVenueCategoryCollection, _alreadyFetchedChildVenueCategoryCollection;
		private Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection	_venueCategoryLanguageCollection;
		private bool	_alwaysFetchVenueCategoryLanguageCollection, _alreadyFetchedVenueCategoryLanguageCollection;
		private VenueCategoryEntity _parentVenueCategoryEntity;
		private bool	_alwaysFetchParentVenueCategoryEntity, _alreadyFetchedParentVenueCategoryEntity, _parentVenueCategoryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentVenueCategoryEntity</summary>
			public static readonly string ParentVenueCategoryEntity = "ParentVenueCategoryEntity";
			/// <summary>Member name CompanyVenueCategoryCollection</summary>
			public static readonly string CompanyVenueCategoryCollection = "CompanyVenueCategoryCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name PointOfInterestVenueCategoryCollection</summary>
			public static readonly string PointOfInterestVenueCategoryCollection = "PointOfInterestVenueCategoryCollection";
			/// <summary>Member name ChildVenueCategoryCollection</summary>
			public static readonly string ChildVenueCategoryCollection = "ChildVenueCategoryCollection";
			/// <summary>Member name VenueCategoryLanguageCollection</summary>
			public static readonly string VenueCategoryLanguageCollection = "VenueCategoryLanguageCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VenueCategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected VenueCategoryEntityBase() :base("VenueCategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		protected VenueCategoryEntityBase(System.Int32 venueCategoryId):base("VenueCategoryEntity")
		{
			InitClassFetch(venueCategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected VenueCategoryEntityBase(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse): base("VenueCategoryEntity")
		{
			InitClassFetch(venueCategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="validator">The custom validator object for this VenueCategoryEntity</param>
		protected VenueCategoryEntityBase(System.Int32 venueCategoryId, IValidator validator):base("VenueCategoryEntity")
		{
			InitClassFetch(venueCategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VenueCategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyVenueCategoryCollection = (Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection)info.GetValue("_companyVenueCategoryCollection", typeof(Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection));
			_alwaysFetchCompanyVenueCategoryCollection = info.GetBoolean("_alwaysFetchCompanyVenueCategoryCollection");
			_alreadyFetchedCompanyVenueCategoryCollection = info.GetBoolean("_alreadyFetchedCompanyVenueCategoryCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_pointOfInterestVenueCategoryCollection = (Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection)info.GetValue("_pointOfInterestVenueCategoryCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection));
			_alwaysFetchPointOfInterestVenueCategoryCollection = info.GetBoolean("_alwaysFetchPointOfInterestVenueCategoryCollection");
			_alreadyFetchedPointOfInterestVenueCategoryCollection = info.GetBoolean("_alreadyFetchedPointOfInterestVenueCategoryCollection");

			_childVenueCategoryCollection = (Obymobi.Data.CollectionClasses.VenueCategoryCollection)info.GetValue("_childVenueCategoryCollection", typeof(Obymobi.Data.CollectionClasses.VenueCategoryCollection));
			_alwaysFetchChildVenueCategoryCollection = info.GetBoolean("_alwaysFetchChildVenueCategoryCollection");
			_alreadyFetchedChildVenueCategoryCollection = info.GetBoolean("_alreadyFetchedChildVenueCategoryCollection");

			_venueCategoryLanguageCollection = (Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection)info.GetValue("_venueCategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection));
			_alwaysFetchVenueCategoryLanguageCollection = info.GetBoolean("_alwaysFetchVenueCategoryLanguageCollection");
			_alreadyFetchedVenueCategoryLanguageCollection = info.GetBoolean("_alreadyFetchedVenueCategoryLanguageCollection");
			_parentVenueCategoryEntity = (VenueCategoryEntity)info.GetValue("_parentVenueCategoryEntity", typeof(VenueCategoryEntity));
			if(_parentVenueCategoryEntity!=null)
			{
				_parentVenueCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentVenueCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_parentVenueCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchParentVenueCategoryEntity = info.GetBoolean("_alwaysFetchParentVenueCategoryEntity");
			_alreadyFetchedParentVenueCategoryEntity = info.GetBoolean("_alreadyFetchedParentVenueCategoryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VenueCategoryFieldIndex)fieldIndex)
			{
				case VenueCategoryFieldIndex.ParentVenueCategoryId:
					DesetupSyncParentVenueCategoryEntity(true, false);
					_alreadyFetchedParentVenueCategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyVenueCategoryCollection = (_companyVenueCategoryCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedPointOfInterestVenueCategoryCollection = (_pointOfInterestVenueCategoryCollection.Count > 0);
			_alreadyFetchedChildVenueCategoryCollection = (_childVenueCategoryCollection.Count > 0);
			_alreadyFetchedVenueCategoryLanguageCollection = (_venueCategoryLanguageCollection.Count > 0);
			_alreadyFetchedParentVenueCategoryEntity = (_parentVenueCategoryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentVenueCategoryEntity":
					toReturn.Add(Relations.VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryId);
					break;
				case "CompanyVenueCategoryCollection":
					toReturn.Add(Relations.CompanyVenueCategoryEntityUsingVenueCategoryId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingVenueCategoryId);
					break;
				case "PointOfInterestVenueCategoryCollection":
					toReturn.Add(Relations.PointOfInterestVenueCategoryEntityUsingVenueCategoryId);
					break;
				case "ChildVenueCategoryCollection":
					toReturn.Add(Relations.VenueCategoryEntityUsingParentVenueCategoryId);
					break;
				case "VenueCategoryLanguageCollection":
					toReturn.Add(Relations.VenueCategoryLanguageEntityUsingVenueCategoryId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyVenueCategoryCollection", (!this.MarkedForDeletion?_companyVenueCategoryCollection:null));
			info.AddValue("_alwaysFetchCompanyVenueCategoryCollection", _alwaysFetchCompanyVenueCategoryCollection);
			info.AddValue("_alreadyFetchedCompanyVenueCategoryCollection", _alreadyFetchedCompanyVenueCategoryCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_pointOfInterestVenueCategoryCollection", (!this.MarkedForDeletion?_pointOfInterestVenueCategoryCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestVenueCategoryCollection", _alwaysFetchPointOfInterestVenueCategoryCollection);
			info.AddValue("_alreadyFetchedPointOfInterestVenueCategoryCollection", _alreadyFetchedPointOfInterestVenueCategoryCollection);
			info.AddValue("_childVenueCategoryCollection", (!this.MarkedForDeletion?_childVenueCategoryCollection:null));
			info.AddValue("_alwaysFetchChildVenueCategoryCollection", _alwaysFetchChildVenueCategoryCollection);
			info.AddValue("_alreadyFetchedChildVenueCategoryCollection", _alreadyFetchedChildVenueCategoryCollection);
			info.AddValue("_venueCategoryLanguageCollection", (!this.MarkedForDeletion?_venueCategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchVenueCategoryLanguageCollection", _alwaysFetchVenueCategoryLanguageCollection);
			info.AddValue("_alreadyFetchedVenueCategoryLanguageCollection", _alreadyFetchedVenueCategoryLanguageCollection);
			info.AddValue("_parentVenueCategoryEntity", (!this.MarkedForDeletion?_parentVenueCategoryEntity:null));
			info.AddValue("_parentVenueCategoryEntityReturnsNewIfNotFound", _parentVenueCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentVenueCategoryEntity", _alwaysFetchParentVenueCategoryEntity);
			info.AddValue("_alreadyFetchedParentVenueCategoryEntity", _alreadyFetchedParentVenueCategoryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentVenueCategoryEntity":
					_alreadyFetchedParentVenueCategoryEntity = true;
					this.ParentVenueCategoryEntity = (VenueCategoryEntity)entity;
					break;
				case "CompanyVenueCategoryCollection":
					_alreadyFetchedCompanyVenueCategoryCollection = true;
					if(entity!=null)
					{
						this.CompanyVenueCategoryCollection.Add((CompanyVenueCategoryEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "PointOfInterestVenueCategoryCollection":
					_alreadyFetchedPointOfInterestVenueCategoryCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestVenueCategoryCollection.Add((PointOfInterestVenueCategoryEntity)entity);
					}
					break;
				case "ChildVenueCategoryCollection":
					_alreadyFetchedChildVenueCategoryCollection = true;
					if(entity!=null)
					{
						this.ChildVenueCategoryCollection.Add((VenueCategoryEntity)entity);
					}
					break;
				case "VenueCategoryLanguageCollection":
					_alreadyFetchedVenueCategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.VenueCategoryLanguageCollection.Add((VenueCategoryLanguageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentVenueCategoryEntity":
					SetupSyncParentVenueCategoryEntity(relatedEntity);
					break;
				case "CompanyVenueCategoryCollection":
					_companyVenueCategoryCollection.Add((CompanyVenueCategoryEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "PointOfInterestVenueCategoryCollection":
					_pointOfInterestVenueCategoryCollection.Add((PointOfInterestVenueCategoryEntity)relatedEntity);
					break;
				case "ChildVenueCategoryCollection":
					_childVenueCategoryCollection.Add((VenueCategoryEntity)relatedEntity);
					break;
				case "VenueCategoryLanguageCollection":
					_venueCategoryLanguageCollection.Add((VenueCategoryLanguageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentVenueCategoryEntity":
					DesetupSyncParentVenueCategoryEntity(false, true);
					break;
				case "CompanyVenueCategoryCollection":
					this.PerformRelatedEntityRemoval(_companyVenueCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestVenueCategoryCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestVenueCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ChildVenueCategoryCollection":
					this.PerformRelatedEntityRemoval(_childVenueCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VenueCategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_venueCategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentVenueCategoryEntity!=null)
			{
				toReturn.Add(_parentVenueCategoryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyVenueCategoryCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_pointOfInterestVenueCategoryCollection);
			toReturn.Add(_childVenueCategoryCollection);
			toReturn.Add(_venueCategoryLanguageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 venueCategoryId)
		{
			return FetchUsingPK(venueCategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(venueCategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(venueCategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(venueCategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.VenueCategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VenueCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection GetMultiCompanyVenueCategoryCollection(bool forceFetch)
		{
			return GetMultiCompanyVenueCategoryCollection(forceFetch, _companyVenueCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection GetMultiCompanyVenueCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyVenueCategoryCollection(forceFetch, _companyVenueCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection GetMultiCompanyVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyVenueCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection GetMultiCompanyVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyVenueCategoryCollection || forceFetch || _alwaysFetchCompanyVenueCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyVenueCategoryCollection);
				_companyVenueCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_companyVenueCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyVenueCategoryCollection.GetMultiManyToOne(null, this, filter);
				_companyVenueCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyVenueCategoryCollection = true;
			}
			return _companyVenueCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyVenueCategoryCollection'. These settings will be taken into account
		/// when the property CompanyVenueCategoryCollection is requested or GetMultiCompanyVenueCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyVenueCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyVenueCategoryCollection.SortClauses=sortClauses;
			_companyVenueCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, _pointOfInterestVenueCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, _pointOfInterestVenueCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestVenueCategoryCollection || forceFetch || _alwaysFetchPointOfInterestVenueCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestVenueCategoryCollection);
				_pointOfInterestVenueCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestVenueCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestVenueCategoryCollection.GetMultiManyToOne(null, this, filter);
				_pointOfInterestVenueCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestVenueCategoryCollection = true;
			}
			return _pointOfInterestVenueCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestVenueCategoryCollection'. These settings will be taken into account
		/// when the property PointOfInterestVenueCategoryCollection is requested or GetMultiPointOfInterestVenueCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestVenueCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestVenueCategoryCollection.SortClauses=sortClauses;
			_pointOfInterestVenueCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryCollection GetMultiChildVenueCategoryCollection(bool forceFetch)
		{
			return GetMultiChildVenueCategoryCollection(forceFetch, _childVenueCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryCollection GetMultiChildVenueCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildVenueCategoryCollection(forceFetch, _childVenueCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryCollection GetMultiChildVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildVenueCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryCollection GetMultiChildVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildVenueCategoryCollection || forceFetch || _alwaysFetchChildVenueCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childVenueCategoryCollection);
				_childVenueCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_childVenueCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_childVenueCategoryCollection.GetMultiManyToOne(this, filter);
				_childVenueCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedChildVenueCategoryCollection = true;
			}
			return _childVenueCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildVenueCategoryCollection'. These settings will be taken into account
		/// when the property ChildVenueCategoryCollection is requested or GetMultiChildVenueCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildVenueCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childVenueCategoryCollection.SortClauses=sortClauses;
			_childVenueCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, _venueCategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, _venueCategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVenueCategoryLanguageCollection || forceFetch || _alwaysFetchVenueCategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_venueCategoryLanguageCollection);
				_venueCategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_venueCategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_venueCategoryLanguageCollection.GetMultiManyToOne(null, this, filter);
				_venueCategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedVenueCategoryLanguageCollection = true;
			}
			return _venueCategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'VenueCategoryLanguageCollection'. These settings will be taken into account
		/// when the property VenueCategoryLanguageCollection is requested or GetMultiVenueCategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVenueCategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_venueCategoryLanguageCollection.SortClauses=sortClauses;
			_venueCategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public VenueCategoryEntity GetSingleParentVenueCategoryEntity()
		{
			return GetSingleParentVenueCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public virtual VenueCategoryEntity GetSingleParentVenueCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentVenueCategoryEntity || forceFetch || _alwaysFetchParentVenueCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryId);
				VenueCategoryEntity newEntity = new VenueCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentVenueCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VenueCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentVenueCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentVenueCategoryEntity = newEntity;
				_alreadyFetchedParentVenueCategoryEntity = fetchResult;
			}
			return _parentVenueCategoryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentVenueCategoryEntity", _parentVenueCategoryEntity);
			toReturn.Add("CompanyVenueCategoryCollection", _companyVenueCategoryCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("PointOfInterestVenueCategoryCollection", _pointOfInterestVenueCategoryCollection);
			toReturn.Add("ChildVenueCategoryCollection", _childVenueCategoryCollection);
			toReturn.Add("VenueCategoryLanguageCollection", _venueCategoryLanguageCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="validator">The validator object for this VenueCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 venueCategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(venueCategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyVenueCategoryCollection = new Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection();
			_companyVenueCategoryCollection.SetContainingEntityInfo(this, "VenueCategoryEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "VenueCategoryEntity");

			_pointOfInterestVenueCategoryCollection = new Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection();
			_pointOfInterestVenueCategoryCollection.SetContainingEntityInfo(this, "VenueCategoryEntity");

			_childVenueCategoryCollection = new Obymobi.Data.CollectionClasses.VenueCategoryCollection();
			_childVenueCategoryCollection.SetContainingEntityInfo(this, "ParentVenueCategoryEntity");

			_venueCategoryLanguageCollection = new Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection();
			_venueCategoryLanguageCollection.SetContainingEntityInfo(this, "VenueCategoryEntity");
			_parentVenueCategoryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VenueCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MarkerIcon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentVenueCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifiedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentVenueCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentVenueCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentVenueCategoryEntity, new PropertyChangedEventHandler( OnParentVenueCategoryEntityPropertyChanged ), "ParentVenueCategoryEntity", Obymobi.Data.RelationClasses.StaticVenueCategoryRelations.VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryIdStatic, true, signalRelatedEntity, "ChildVenueCategoryCollection", resetFKFields, new int[] { (int)VenueCategoryFieldIndex.ParentVenueCategoryId } );		
			_parentVenueCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentVenueCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentVenueCategoryEntity(IEntityCore relatedEntity)
		{
			if(_parentVenueCategoryEntity!=relatedEntity)
			{		
				DesetupSyncParentVenueCategoryEntity(true, true);
				_parentVenueCategoryEntity = (VenueCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentVenueCategoryEntity, new PropertyChangedEventHandler( OnParentVenueCategoryEntityPropertyChanged ), "ParentVenueCategoryEntity", Obymobi.Data.RelationClasses.StaticVenueCategoryRelations.VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryIdStatic, true, ref _alreadyFetchedParentVenueCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentVenueCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VenueCategoryFieldIndex.VenueCategoryId].ForcedCurrentValueWrite(venueCategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVenueCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VenueCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VenueCategoryRelations Relations
		{
			get	{ return new VenueCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyVenueCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyVenueCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection(), (IEntityRelation)GetRelationsForField("CompanyVenueCategoryCollection")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.CompanyVenueCategoryEntity, 0, null, null, null, "CompanyVenueCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestVenueCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestVenueCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestVenueCategoryCollection")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity, 0, null, null, null, "PointOfInterestVenueCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildVenueCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryCollection(), (IEntityRelation)GetRelationsForField("ChildVenueCategoryCollection")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.VenueCategoryEntity, 0, null, null, null, "ChildVenueCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVenueCategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("VenueCategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.VenueCategoryLanguageEntity, 0, null, null, null, "VenueCategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentVenueCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryCollection(), (IEntityRelation)GetRelationsForField("ParentVenueCategoryEntity")[0], (int)Obymobi.Data.EntityType.VenueCategoryEntity, (int)Obymobi.Data.EntityType.VenueCategoryEntity, 0, null, null, null, "ParentVenueCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The VenueCategoryId property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."VenueCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 VenueCategoryId
		{
			get { return (System.Int32)GetValue((int)VenueCategoryFieldIndex.VenueCategoryId, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.VenueCategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)VenueCategoryFieldIndex.Name, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)VenueCategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)VenueCategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The MarkerIcon property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."MarkerIcon"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MarkerIcon MarkerIcon
		{
			get { return (Obymobi.Enums.MarkerIcon)GetValue((int)VenueCategoryFieldIndex.MarkerIcon, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.MarkerIcon, value, true); }
		}

		/// <summary> The ParentVenueCategoryId property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."ParentVenueCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentVenueCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)VenueCategoryFieldIndex.ParentVenueCategoryId, false); }
			set	{ SetValue((int)VenueCategoryFieldIndex.ParentVenueCategoryId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VenueCategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)VenueCategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VenueCategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)VenueCategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastModifiedUTC property of the Entity VenueCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VenueCategory"."LastModifiedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)VenueCategoryFieldIndex.LastModifiedUTC, true); }
			set	{ SetValue((int)VenueCategoryFieldIndex.LastModifiedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyVenueCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyVenueCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyVenueCategoryCollection CompanyVenueCategoryCollection
		{
			get	{ return GetMultiCompanyVenueCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyVenueCategoryCollection. When set to true, CompanyVenueCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyVenueCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyVenueCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyVenueCategoryCollection
		{
			get	{ return _alwaysFetchCompanyVenueCategoryCollection; }
			set	{ _alwaysFetchCompanyVenueCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyVenueCategoryCollection already has been fetched. Setting this property to false when CompanyVenueCategoryCollection has been fetched
		/// will clear the CompanyVenueCategoryCollection collection well. Setting this property to true while CompanyVenueCategoryCollection hasn't been fetched disables lazy loading for CompanyVenueCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyVenueCategoryCollection
		{
			get { return _alreadyFetchedCompanyVenueCategoryCollection;}
			set 
			{
				if(_alreadyFetchedCompanyVenueCategoryCollection && !value && (_companyVenueCategoryCollection != null))
				{
					_companyVenueCategoryCollection.Clear();
				}
				_alreadyFetchedCompanyVenueCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestVenueCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection PointOfInterestVenueCategoryCollection
		{
			get	{ return GetMultiPointOfInterestVenueCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestVenueCategoryCollection. When set to true, PointOfInterestVenueCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestVenueCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestVenueCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestVenueCategoryCollection
		{
			get	{ return _alwaysFetchPointOfInterestVenueCategoryCollection; }
			set	{ _alwaysFetchPointOfInterestVenueCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestVenueCategoryCollection already has been fetched. Setting this property to false when PointOfInterestVenueCategoryCollection has been fetched
		/// will clear the PointOfInterestVenueCategoryCollection collection well. Setting this property to true while PointOfInterestVenueCategoryCollection hasn't been fetched disables lazy loading for PointOfInterestVenueCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestVenueCategoryCollection
		{
			get { return _alreadyFetchedPointOfInterestVenueCategoryCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestVenueCategoryCollection && !value && (_pointOfInterestVenueCategoryCollection != null))
				{
					_pointOfInterestVenueCategoryCollection.Clear();
				}
				_alreadyFetchedPointOfInterestVenueCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VenueCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildVenueCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryCollection ChildVenueCategoryCollection
		{
			get	{ return GetMultiChildVenueCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildVenueCategoryCollection. When set to true, ChildVenueCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildVenueCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiChildVenueCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildVenueCategoryCollection
		{
			get	{ return _alwaysFetchChildVenueCategoryCollection; }
			set	{ _alwaysFetchChildVenueCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildVenueCategoryCollection already has been fetched. Setting this property to false when ChildVenueCategoryCollection has been fetched
		/// will clear the ChildVenueCategoryCollection collection well. Setting this property to true while ChildVenueCategoryCollection hasn't been fetched disables lazy loading for ChildVenueCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildVenueCategoryCollection
		{
			get { return _alreadyFetchedChildVenueCategoryCollection;}
			set 
			{
				if(_alreadyFetchedChildVenueCategoryCollection && !value && (_childVenueCategoryCollection != null))
				{
					_childVenueCategoryCollection.Clear();
				}
				_alreadyFetchedChildVenueCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVenueCategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection VenueCategoryLanguageCollection
		{
			get	{ return GetMultiVenueCategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VenueCategoryLanguageCollection. When set to true, VenueCategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VenueCategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiVenueCategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVenueCategoryLanguageCollection
		{
			get	{ return _alwaysFetchVenueCategoryLanguageCollection; }
			set	{ _alwaysFetchVenueCategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VenueCategoryLanguageCollection already has been fetched. Setting this property to false when VenueCategoryLanguageCollection has been fetched
		/// will clear the VenueCategoryLanguageCollection collection well. Setting this property to true while VenueCategoryLanguageCollection hasn't been fetched disables lazy loading for VenueCategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVenueCategoryLanguageCollection
		{
			get { return _alreadyFetchedVenueCategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedVenueCategoryLanguageCollection && !value && (_venueCategoryLanguageCollection != null))
				{
					_venueCategoryLanguageCollection.Clear();
				}
				_alreadyFetchedVenueCategoryLanguageCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'VenueCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentVenueCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual VenueCategoryEntity ParentVenueCategoryEntity
		{
			get	{ return GetSingleParentVenueCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentVenueCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildVenueCategoryCollection", "ParentVenueCategoryEntity", _parentVenueCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentVenueCategoryEntity. When set to true, ParentVenueCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentVenueCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleParentVenueCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentVenueCategoryEntity
		{
			get	{ return _alwaysFetchParentVenueCategoryEntity; }
			set	{ _alwaysFetchParentVenueCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentVenueCategoryEntity already has been fetched. Setting this property to false when ParentVenueCategoryEntity has been fetched
		/// will set ParentVenueCategoryEntity to null as well. Setting this property to true while ParentVenueCategoryEntity hasn't been fetched disables lazy loading for ParentVenueCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentVenueCategoryEntity
		{
			get { return _alreadyFetchedParentVenueCategoryEntity;}
			set 
			{
				if(_alreadyFetchedParentVenueCategoryEntity && !value)
				{
					this.ParentVenueCategoryEntity = null;
				}
				_alreadyFetchedParentVenueCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentVenueCategoryEntity is not found
		/// in the database. When set to true, ParentVenueCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentVenueCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _parentVenueCategoryEntityReturnsNewIfNotFound; }
			set { _parentVenueCategoryEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.VenueCategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
