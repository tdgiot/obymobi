﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Genericalteration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GenericalterationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GenericalterationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationCollection	_alterationCollection;
		private bool	_alwaysFetchAlterationCollection, _alreadyFetchedAlterationCollection;
		private Obymobi.Data.CollectionClasses.GenericalterationitemCollection	_genericalterationitemCollection;
		private bool	_alwaysFetchGenericalterationitemCollection, _alreadyFetchedGenericalterationitemCollection;
		private Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection	_genericproductGenericalterationCollection;
		private bool	_alwaysFetchGenericproductGenericalterationCollection, _alreadyFetchedGenericproductGenericalterationCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAlteration;
		private bool	_alwaysFetchCompanyCollectionViaAlteration, _alreadyFetchedCompanyCollectionViaAlteration;
		private Obymobi.Data.CollectionClasses.GenericalterationoptionCollection _genericalterationoptionCollectionViaGenericalterationitem;
		private bool	_alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem, _alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaGenericproductGenericalteration;
		private bool	_alwaysFetchGenericproductCollectionViaGenericproductGenericalteration, _alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration;
		private Obymobi.Data.CollectionClasses.PosalterationCollection _posalterationCollectionViaAlteration;
		private bool	_alwaysFetchPosalterationCollectionViaAlteration, _alreadyFetchedPosalterationCollectionViaAlteration;
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name GenericalterationitemCollection</summary>
			public static readonly string GenericalterationitemCollection = "GenericalterationitemCollection";
			/// <summary>Member name GenericproductGenericalterationCollection</summary>
			public static readonly string GenericproductGenericalterationCollection = "GenericproductGenericalterationCollection";
			/// <summary>Member name CompanyCollectionViaAlteration</summary>
			public static readonly string CompanyCollectionViaAlteration = "CompanyCollectionViaAlteration";
			/// <summary>Member name GenericalterationoptionCollectionViaGenericalterationitem</summary>
			public static readonly string GenericalterationoptionCollectionViaGenericalterationitem = "GenericalterationoptionCollectionViaGenericalterationitem";
			/// <summary>Member name GenericproductCollectionViaGenericproductGenericalteration</summary>
			public static readonly string GenericproductCollectionViaGenericproductGenericalteration = "GenericproductCollectionViaGenericproductGenericalteration";
			/// <summary>Member name PosalterationCollectionViaAlteration</summary>
			public static readonly string PosalterationCollectionViaAlteration = "PosalterationCollectionViaAlteration";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GenericalterationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GenericalterationEntityBase() :base("GenericalterationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		protected GenericalterationEntityBase(System.Int32 genericalterationId):base("GenericalterationEntity")
		{
			InitClassFetch(genericalterationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GenericalterationEntityBase(System.Int32 genericalterationId, IPrefetchPath prefetchPathToUse): base("GenericalterationEntity")
		{
			InitClassFetch(genericalterationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="validator">The custom validator object for this GenericalterationEntity</param>
		protected GenericalterationEntityBase(System.Int32 genericalterationId, IValidator validator):base("GenericalterationEntity")
		{
			InitClassFetch(genericalterationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericalterationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationCollection = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollection = info.GetBoolean("_alwaysFetchAlterationCollection");
			_alreadyFetchedAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationCollection");

			_genericalterationitemCollection = (Obymobi.Data.CollectionClasses.GenericalterationitemCollection)info.GetValue("_genericalterationitemCollection", typeof(Obymobi.Data.CollectionClasses.GenericalterationitemCollection));
			_alwaysFetchGenericalterationitemCollection = info.GetBoolean("_alwaysFetchGenericalterationitemCollection");
			_alreadyFetchedGenericalterationitemCollection = info.GetBoolean("_alreadyFetchedGenericalterationitemCollection");

			_genericproductGenericalterationCollection = (Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection)info.GetValue("_genericproductGenericalterationCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection));
			_alwaysFetchGenericproductGenericalterationCollection = info.GetBoolean("_alwaysFetchGenericproductGenericalterationCollection");
			_alreadyFetchedGenericproductGenericalterationCollection = info.GetBoolean("_alreadyFetchedGenericproductGenericalterationCollection");
			_companyCollectionViaAlteration = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAlteration", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAlteration = info.GetBoolean("_alwaysFetchCompanyCollectionViaAlteration");
			_alreadyFetchedCompanyCollectionViaAlteration = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAlteration");

			_genericalterationoptionCollectionViaGenericalterationitem = (Obymobi.Data.CollectionClasses.GenericalterationoptionCollection)info.GetValue("_genericalterationoptionCollectionViaGenericalterationitem", typeof(Obymobi.Data.CollectionClasses.GenericalterationoptionCollection));
			_alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem = info.GetBoolean("_alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem");
			_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem = info.GetBoolean("_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem");

			_genericproductCollectionViaGenericproductGenericalteration = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaGenericproductGenericalteration", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaGenericproductGenericalteration = info.GetBoolean("_alwaysFetchGenericproductCollectionViaGenericproductGenericalteration");
			_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration");

			_posalterationCollectionViaAlteration = (Obymobi.Data.CollectionClasses.PosalterationCollection)info.GetValue("_posalterationCollectionViaAlteration", typeof(Obymobi.Data.CollectionClasses.PosalterationCollection));
			_alwaysFetchPosalterationCollectionViaAlteration = info.GetBoolean("_alwaysFetchPosalterationCollectionViaAlteration");
			_alreadyFetchedPosalterationCollectionViaAlteration = info.GetBoolean("_alreadyFetchedPosalterationCollectionViaAlteration");
			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GenericalterationFieldIndex)fieldIndex)
			{
				case GenericalterationFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationCollection = (_alterationCollection.Count > 0);
			_alreadyFetchedGenericalterationitemCollection = (_genericalterationitemCollection.Count > 0);
			_alreadyFetchedGenericproductGenericalterationCollection = (_genericproductGenericalterationCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaAlteration = (_companyCollectionViaAlteration.Count > 0);
			_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem = (_genericalterationoptionCollectionViaGenericalterationitem.Count > 0);
			_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration = (_genericproductCollectionViaGenericproductGenericalteration.Count > 0);
			_alreadyFetchedPosalterationCollectionViaAlteration = (_posalterationCollectionViaAlteration.Count > 0);
			_alreadyFetchedBrandEntity = (_brandEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "AlterationCollection":
					toReturn.Add(Relations.AlterationEntityUsingGenericalterationId);
					break;
				case "GenericalterationitemCollection":
					toReturn.Add(Relations.GenericalterationitemEntityUsingGenericalterationId);
					break;
				case "GenericproductGenericalterationCollection":
					toReturn.Add(Relations.GenericproductGenericalterationEntityUsingGenericalterationId);
					break;
				case "CompanyCollectionViaAlteration":
					toReturn.Add(Relations.AlterationEntityUsingGenericalterationId, "GenericalterationEntity__", "Alteration_", JoinHint.None);
					toReturn.Add(AlterationEntity.Relations.CompanyEntityUsingCompanyId, "Alteration_", string.Empty, JoinHint.None);
					break;
				case "GenericalterationoptionCollectionViaGenericalterationitem":
					toReturn.Add(Relations.GenericalterationitemEntityUsingGenericalterationId, "GenericalterationEntity__", "Genericalterationitem_", JoinHint.None);
					toReturn.Add(GenericalterationitemEntity.Relations.GenericalterationoptionEntityUsingGenericalterationoptionId, "Genericalterationitem_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaGenericproductGenericalteration":
					toReturn.Add(Relations.GenericproductGenericalterationEntityUsingGenericalterationId, "GenericalterationEntity__", "GenericproductGenericalteration_", JoinHint.None);
					toReturn.Add(GenericproductGenericalterationEntity.Relations.GenericproductEntityUsingGenericproductId, "GenericproductGenericalteration_", string.Empty, JoinHint.None);
					break;
				case "PosalterationCollectionViaAlteration":
					toReturn.Add(Relations.AlterationEntityUsingGenericalterationId, "GenericalterationEntity__", "Alteration_", JoinHint.None);
					toReturn.Add(AlterationEntity.Relations.PosalterationEntityUsingPosalterationId, "Alteration_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationCollection", (!this.MarkedForDeletion?_alterationCollection:null));
			info.AddValue("_alwaysFetchAlterationCollection", _alwaysFetchAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationCollection", _alreadyFetchedAlterationCollection);
			info.AddValue("_genericalterationitemCollection", (!this.MarkedForDeletion?_genericalterationitemCollection:null));
			info.AddValue("_alwaysFetchGenericalterationitemCollection", _alwaysFetchGenericalterationitemCollection);
			info.AddValue("_alreadyFetchedGenericalterationitemCollection", _alreadyFetchedGenericalterationitemCollection);
			info.AddValue("_genericproductGenericalterationCollection", (!this.MarkedForDeletion?_genericproductGenericalterationCollection:null));
			info.AddValue("_alwaysFetchGenericproductGenericalterationCollection", _alwaysFetchGenericproductGenericalterationCollection);
			info.AddValue("_alreadyFetchedGenericproductGenericalterationCollection", _alreadyFetchedGenericproductGenericalterationCollection);
			info.AddValue("_companyCollectionViaAlteration", (!this.MarkedForDeletion?_companyCollectionViaAlteration:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAlteration", _alwaysFetchCompanyCollectionViaAlteration);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAlteration", _alreadyFetchedCompanyCollectionViaAlteration);
			info.AddValue("_genericalterationoptionCollectionViaGenericalterationitem", (!this.MarkedForDeletion?_genericalterationoptionCollectionViaGenericalterationitem:null));
			info.AddValue("_alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem", _alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem);
			info.AddValue("_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem", _alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem);
			info.AddValue("_genericproductCollectionViaGenericproductGenericalteration", (!this.MarkedForDeletion?_genericproductCollectionViaGenericproductGenericalteration:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaGenericproductGenericalteration", _alwaysFetchGenericproductCollectionViaGenericproductGenericalteration);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration", _alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration);
			info.AddValue("_posalterationCollectionViaAlteration", (!this.MarkedForDeletion?_posalterationCollectionViaAlteration:null));
			info.AddValue("_alwaysFetchPosalterationCollectionViaAlteration", _alwaysFetchPosalterationCollectionViaAlteration);
			info.AddValue("_alreadyFetchedPosalterationCollectionViaAlteration", _alreadyFetchedPosalterationCollectionViaAlteration);
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "AlterationCollection":
					_alreadyFetchedAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationCollection.Add((AlterationEntity)entity);
					}
					break;
				case "GenericalterationitemCollection":
					_alreadyFetchedGenericalterationitemCollection = true;
					if(entity!=null)
					{
						this.GenericalterationitemCollection.Add((GenericalterationitemEntity)entity);
					}
					break;
				case "GenericproductGenericalterationCollection":
					_alreadyFetchedGenericproductGenericalterationCollection = true;
					if(entity!=null)
					{
						this.GenericproductGenericalterationCollection.Add((GenericproductGenericalterationEntity)entity);
					}
					break;
				case "CompanyCollectionViaAlteration":
					_alreadyFetchedCompanyCollectionViaAlteration = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAlteration.Add((CompanyEntity)entity);
					}
					break;
				case "GenericalterationoptionCollectionViaGenericalterationitem":
					_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem = true;
					if(entity!=null)
					{
						this.GenericalterationoptionCollectionViaGenericalterationitem.Add((GenericalterationoptionEntity)entity);
					}
					break;
				case "GenericproductCollectionViaGenericproductGenericalteration":
					_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaGenericproductGenericalteration.Add((GenericproductEntity)entity);
					}
					break;
				case "PosalterationCollectionViaAlteration":
					_alreadyFetchedPosalterationCollectionViaAlteration = true;
					if(entity!=null)
					{
						this.PosalterationCollectionViaAlteration.Add((PosalterationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "AlterationCollection":
					_alterationCollection.Add((AlterationEntity)relatedEntity);
					break;
				case "GenericalterationitemCollection":
					_genericalterationitemCollection.Add((GenericalterationitemEntity)relatedEntity);
					break;
				case "GenericproductGenericalterationCollection":
					_genericproductGenericalterationCollection.Add((GenericproductGenericalterationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "AlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericalterationitemCollection":
					this.PerformRelatedEntityRemoval(_genericalterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductGenericalterationCollection":
					this.PerformRelatedEntityRemoval(_genericproductGenericalterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationCollection);
			toReturn.Add(_genericalterationitemCollection);
			toReturn.Add(_genericproductGenericalterationCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationId)
		{
			return FetchUsingPK(genericalterationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(genericalterationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(genericalterationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(genericalterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GenericalterationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GenericalterationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationCollection || forceFetch || _alwaysFetchAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollection);
				_alterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollection.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_alterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollection = true;
			}
			return _alterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollection'. These settings will be taken into account
		/// when the property AlterationCollection is requested or GetMultiAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollection.SortClauses=sortClauses;
			_alterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, _genericalterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, _genericalterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericalterationitemCollection || forceFetch || _alwaysFetchGenericalterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationitemCollection);
				_genericalterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_genericalterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationitemCollection.GetMultiManyToOne(this, null, filter);
				_genericalterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationitemCollection = true;
			}
			return _genericalterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationitemCollection'. These settings will be taken into account
		/// when the property GenericalterationitemCollection is requested or GetMultiGenericalterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationitemCollection.SortClauses=sortClauses;
			_genericalterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductGenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, _genericproductGenericalterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductGenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, _genericproductGenericalterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductGenericalterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GetMultiGenericproductGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductGenericalterationCollection || forceFetch || _alwaysFetchGenericproductGenericalterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductGenericalterationCollection);
				_genericproductGenericalterationCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductGenericalterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductGenericalterationCollection.GetMultiManyToOne(this, null, filter);
				_genericproductGenericalterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductGenericalterationCollection = true;
			}
			return _genericproductGenericalterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductGenericalterationCollection'. These settings will be taken into account
		/// when the property GenericproductGenericalterationCollection is requested or GetMultiGenericproductGenericalterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductGenericalterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductGenericalterationCollection.SortClauses=sortClauses;
			_genericproductGenericalterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAlteration(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAlteration(forceFetch, _companyCollectionViaAlteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAlteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAlteration || forceFetch || _alwaysFetchCompanyCollectionViaAlteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAlteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationFields.GenericalterationId, ComparisonOperator.Equal, this.GenericalterationId, "GenericalterationEntity__"));
				_companyCollectionViaAlteration.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAlteration.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAlteration.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAlteration"));
				_companyCollectionViaAlteration.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAlteration = true;
			}
			return _companyCollectionViaAlteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAlteration'. These settings will be taken into account
		/// when the property CompanyCollectionViaAlteration is requested or GetMultiCompanyCollectionViaAlteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAlteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAlteration.SortClauses=sortClauses;
			_companyCollectionViaAlteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GetMultiGenericalterationoptionCollectionViaGenericalterationitem(bool forceFetch)
		{
			return GetMultiGenericalterationoptionCollectionViaGenericalterationitem(forceFetch, _genericalterationoptionCollectionViaGenericalterationitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GetMultiGenericalterationoptionCollectionViaGenericalterationitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem || forceFetch || _alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationoptionCollectionViaGenericalterationitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationFields.GenericalterationId, ComparisonOperator.Equal, this.GenericalterationId, "GenericalterationEntity__"));
				_genericalterationoptionCollectionViaGenericalterationitem.SuppressClearInGetMulti=!forceFetch;
				_genericalterationoptionCollectionViaGenericalterationitem.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationoptionCollectionViaGenericalterationitem.GetMulti(filter, GetRelationsForField("GenericalterationoptionCollectionViaGenericalterationitem"));
				_genericalterationoptionCollectionViaGenericalterationitem.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem = true;
			}
			return _genericalterationoptionCollectionViaGenericalterationitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationoptionCollectionViaGenericalterationitem'. These settings will be taken into account
		/// when the property GenericalterationoptionCollectionViaGenericalterationitem is requested or GetMultiGenericalterationoptionCollectionViaGenericalterationitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationoptionCollectionViaGenericalterationitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationoptionCollectionViaGenericalterationitem.SortClauses=sortClauses;
			_genericalterationoptionCollectionViaGenericalterationitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaGenericproductGenericalteration(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaGenericproductGenericalteration(forceFetch, _genericproductCollectionViaGenericproductGenericalteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaGenericproductGenericalteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration || forceFetch || _alwaysFetchGenericproductCollectionViaGenericproductGenericalteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaGenericproductGenericalteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationFields.GenericalterationId, ComparisonOperator.Equal, this.GenericalterationId, "GenericalterationEntity__"));
				_genericproductCollectionViaGenericproductGenericalteration.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaGenericproductGenericalteration.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaGenericproductGenericalteration.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaGenericproductGenericalteration"));
				_genericproductCollectionViaGenericproductGenericalteration.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration = true;
			}
			return _genericproductCollectionViaGenericproductGenericalteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaGenericproductGenericalteration'. These settings will be taken into account
		/// when the property GenericproductCollectionViaGenericproductGenericalteration is requested or GetMultiGenericproductCollectionViaGenericproductGenericalteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaGenericproductGenericalteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaGenericproductGenericalteration.SortClauses=sortClauses;
			_genericproductCollectionViaGenericproductGenericalteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosalterationCollection GetMultiPosalterationCollectionViaAlteration(bool forceFetch)
		{
			return GetMultiPosalterationCollectionViaAlteration(forceFetch, _posalterationCollectionViaAlteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosalterationCollection GetMultiPosalterationCollectionViaAlteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosalterationCollectionViaAlteration || forceFetch || _alwaysFetchPosalterationCollectionViaAlteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posalterationCollectionViaAlteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationFields.GenericalterationId, ComparisonOperator.Equal, this.GenericalterationId, "GenericalterationEntity__"));
				_posalterationCollectionViaAlteration.SuppressClearInGetMulti=!forceFetch;
				_posalterationCollectionViaAlteration.EntityFactoryToUse = entityFactoryToUse;
				_posalterationCollectionViaAlteration.GetMulti(filter, GetRelationsForField("PosalterationCollectionViaAlteration"));
				_posalterationCollectionViaAlteration.SuppressClearInGetMulti=false;
				_alreadyFetchedPosalterationCollectionViaAlteration = true;
			}
			return _posalterationCollectionViaAlteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosalterationCollectionViaAlteration'. These settings will be taken into account
		/// when the property PosalterationCollectionViaAlteration is requested or GetMultiPosalterationCollectionViaAlteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosalterationCollectionViaAlteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posalterationCollectionViaAlteration.SortClauses=sortClauses;
			_posalterationCollectionViaAlteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("AlterationCollection", _alterationCollection);
			toReturn.Add("GenericalterationitemCollection", _genericalterationitemCollection);
			toReturn.Add("GenericproductGenericalterationCollection", _genericproductGenericalterationCollection);
			toReturn.Add("CompanyCollectionViaAlteration", _companyCollectionViaAlteration);
			toReturn.Add("GenericalterationoptionCollectionViaGenericalterationitem", _genericalterationoptionCollectionViaGenericalterationitem);
			toReturn.Add("GenericproductCollectionViaGenericproductGenericalteration", _genericproductCollectionViaGenericproductGenericalteration);
			toReturn.Add("PosalterationCollectionViaAlteration", _posalterationCollectionViaAlteration);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="validator">The validator object for this GenericalterationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 genericalterationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(genericalterationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationCollection = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollection.SetContainingEntityInfo(this, "GenericalterationEntity");

			_genericalterationitemCollection = new Obymobi.Data.CollectionClasses.GenericalterationitemCollection();
			_genericalterationitemCollection.SetContainingEntityInfo(this, "GenericalterationEntity");

			_genericproductGenericalterationCollection = new Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection();
			_genericproductGenericalterationCollection.SetContainingEntityInfo(this, "GenericalterationEntity");
			_companyCollectionViaAlteration = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_genericalterationoptionCollectionViaGenericalterationitem = new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection();
			_genericproductCollectionViaGenericproductGenericalteration = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_posalterationCollectionViaAlteration = new Obymobi.Data.CollectionClasses.PosalterationCollection();
			_brandEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnObymobi", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinLeadMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxLeadHours", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntervalMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowDatePicker", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericalterationRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "GenericalterationCollection", resetFKFields, new int[] { (int)GenericalterationFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericalterationRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] { "BrandName" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Name":
					this.OnPropertyChanged("BrandName");
					break;
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="genericalterationId">PK value for Genericalteration which data should be fetched into this Genericalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 genericalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GenericalterationFieldIndex.GenericalterationId].ForcedCurrentValueWrite(genericalterationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericalterationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GenericalterationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GenericalterationRelations Relations
		{
			get	{ return new GenericalterationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationCollection")[0], (int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationitemCollection(), (IEntityRelation)GetRelationsForField("GenericalterationitemCollection")[0], (int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.GenericalterationitemEntity, 0, null, null, null, "GenericalterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericproductGenericalteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductGenericalterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection(), (IEntityRelation)GetRelationsForField("GenericproductGenericalterationCollection")[0], (int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.GenericproductGenericalterationEntity, 0, null, null, null, "GenericproductGenericalterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAlteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationEntityUsingGenericalterationId;
				intermediateRelation.SetAliases(string.Empty, "Alteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAlteration"), "CompanyCollectionViaAlteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationoptionCollectionViaGenericalterationitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.GenericalterationitemEntityUsingGenericalterationId;
				intermediateRelation.SetAliases(string.Empty, "Genericalterationitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, 0, null, null, GetRelationsForField("GenericalterationoptionCollectionViaGenericalterationitem"), "GenericalterationoptionCollectionViaGenericalterationitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaGenericproductGenericalteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.GenericproductGenericalterationEntityUsingGenericalterationId;
				intermediateRelation.SetAliases(string.Empty, "GenericproductGenericalteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaGenericproductGenericalteration"), "GenericproductCollectionViaGenericproductGenericalteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosalterationCollectionViaAlteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationEntityUsingGenericalterationId;
				intermediateRelation.SetAliases(string.Empty, "Alteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosalterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.PosalterationEntity, 0, null, null, GetRelationsForField("PosalterationCollectionViaAlteration"), "PosalterationCollectionViaAlteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.GenericalterationEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GenericalterationId property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."GenericalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GenericalterationId
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.GenericalterationId, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.GenericalterationId, value, true); }
		}

		/// <summary> The Name property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GenericalterationFieldIndex.Name, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.Name, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationFieldIndex.ExternalId, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The GenericalterationType property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."GenericalterationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationType
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationFieldIndex.GenericalterationType, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.GenericalterationType, value, true); }
		}

		/// <summary> The Type property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AlterationType Type
		{
			get { return (Obymobi.Enums.AlterationType)GetValue((int)GenericalterationFieldIndex.Type, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.Type, value, true); }
		}

		/// <summary> The MinOptions property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."MinOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MinOptions
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationFieldIndex.MinOptions, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.MinOptions, value, true); }
		}

		/// <summary> The MaxOptions property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."MaxOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MaxOptions
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationFieldIndex.MaxOptions, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.MaxOptions, value, true); }
		}

		/// <summary> The AvailableOnOtoucho property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."AvailableOnOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)GenericalterationFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.AvailableOnOtoucho, value, true); }
		}

		/// <summary> The AvailableOnObymobi property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."AvailableOnObymobi"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)GenericalterationFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.AvailableOnObymobi, value, true); }
		}

		/// <summary> The StartTime property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."StartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationFieldIndex.StartTime, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.StartTime, value, true); }
		}

		/// <summary> The EndTime property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."EndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationFieldIndex.EndTime, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.EndTime, value, true); }
		}

		/// <summary> The MinLeadMinutes property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."MinLeadMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinLeadMinutes
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.MinLeadMinutes, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.MinLeadMinutes, value, true); }
		}

		/// <summary> The MaxLeadHours property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."MaxLeadHours"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxLeadHours
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.MaxLeadHours, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.MaxLeadHours, value, true); }
		}

		/// <summary> The IntervalMinutes property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."IntervalMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IntervalMinutes
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.IntervalMinutes, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.IntervalMinutes, value, true); }
		}

		/// <summary> The ShowDatePicker property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."ShowDatePicker"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDatePicker
		{
			get { return (System.Boolean)GetValue((int)GenericalterationFieldIndex.ShowDatePicker, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.ShowDatePicker, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)GenericalterationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The BrandId property of the Entity Genericalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalteration"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationFieldIndex.BrandId, false); }
			set	{ SetValue((int)GenericalterationFieldIndex.BrandId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollection
		{
			get	{ return GetMultiAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollection. When set to true, AlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollection
		{
			get	{ return _alwaysFetchAlterationCollection; }
			set	{ _alwaysFetchAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollection already has been fetched. Setting this property to false when AlterationCollection has been fetched
		/// will clear the AlterationCollection collection well. Setting this property to true while AlterationCollection hasn't been fetched disables lazy loading for AlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollection
		{
			get { return _alreadyFetchedAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationCollection && !value && (_alterationCollection != null))
				{
					_alterationCollection.Clear();
				}
				_alreadyFetchedAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationitemCollection GenericalterationitemCollection
		{
			get	{ return GetMultiGenericalterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationitemCollection. When set to true, GenericalterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericalterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationitemCollection
		{
			get	{ return _alwaysFetchGenericalterationitemCollection; }
			set	{ _alwaysFetchGenericalterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationitemCollection already has been fetched. Setting this property to false when GenericalterationitemCollection has been fetched
		/// will clear the GenericalterationitemCollection collection well. Setting this property to true while GenericalterationitemCollection hasn't been fetched disables lazy loading for GenericalterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationitemCollection
		{
			get { return _alreadyFetchedGenericalterationitemCollection;}
			set 
			{
				if(_alreadyFetchedGenericalterationitemCollection && !value && (_genericalterationitemCollection != null))
				{
					_genericalterationitemCollection.Clear();
				}
				_alreadyFetchedGenericalterationitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductGenericalterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductGenericalterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductGenericalterationCollection GenericproductGenericalterationCollection
		{
			get	{ return GetMultiGenericproductGenericalterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductGenericalterationCollection. When set to true, GenericproductGenericalterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductGenericalterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductGenericalterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductGenericalterationCollection
		{
			get	{ return _alwaysFetchGenericproductGenericalterationCollection; }
			set	{ _alwaysFetchGenericproductGenericalterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductGenericalterationCollection already has been fetched. Setting this property to false when GenericproductGenericalterationCollection has been fetched
		/// will clear the GenericproductGenericalterationCollection collection well. Setting this property to true while GenericproductGenericalterationCollection hasn't been fetched disables lazy loading for GenericproductGenericalterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductGenericalterationCollection
		{
			get { return _alreadyFetchedGenericproductGenericalterationCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductGenericalterationCollection && !value && (_genericproductGenericalterationCollection != null))
				{
					_genericproductGenericalterationCollection.Clear();
				}
				_alreadyFetchedGenericproductGenericalterationCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAlteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAlteration
		{
			get { return GetMultiCompanyCollectionViaAlteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAlteration. When set to true, CompanyCollectionViaAlteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAlteration is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAlteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAlteration
		{
			get	{ return _alwaysFetchCompanyCollectionViaAlteration; }
			set	{ _alwaysFetchCompanyCollectionViaAlteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAlteration already has been fetched. Setting this property to false when CompanyCollectionViaAlteration has been fetched
		/// will clear the CompanyCollectionViaAlteration collection well. Setting this property to true while CompanyCollectionViaAlteration hasn't been fetched disables lazy loading for CompanyCollectionViaAlteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAlteration
		{
			get { return _alreadyFetchedCompanyCollectionViaAlteration;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAlteration && !value && (_companyCollectionViaAlteration != null))
				{
					_companyCollectionViaAlteration.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAlteration = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationoptionCollectionViaGenericalterationitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationoptionCollection GenericalterationoptionCollectionViaGenericalterationitem
		{
			get { return GetMultiGenericalterationoptionCollectionViaGenericalterationitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationoptionCollectionViaGenericalterationitem. When set to true, GenericalterationoptionCollectionViaGenericalterationitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationoptionCollectionViaGenericalterationitem is accessed. You can always execute a forced fetch by calling GetMultiGenericalterationoptionCollectionViaGenericalterationitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationoptionCollectionViaGenericalterationitem
		{
			get	{ return _alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem; }
			set	{ _alwaysFetchGenericalterationoptionCollectionViaGenericalterationitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationoptionCollectionViaGenericalterationitem already has been fetched. Setting this property to false when GenericalterationoptionCollectionViaGenericalterationitem has been fetched
		/// will clear the GenericalterationoptionCollectionViaGenericalterationitem collection well. Setting this property to true while GenericalterationoptionCollectionViaGenericalterationitem hasn't been fetched disables lazy loading for GenericalterationoptionCollectionViaGenericalterationitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem
		{
			get { return _alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem;}
			set 
			{
				if(_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem && !value && (_genericalterationoptionCollectionViaGenericalterationitem != null))
				{
					_genericalterationoptionCollectionViaGenericalterationitem.Clear();
				}
				_alreadyFetchedGenericalterationoptionCollectionViaGenericalterationitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaGenericproductGenericalteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaGenericproductGenericalteration
		{
			get { return GetMultiGenericproductCollectionViaGenericproductGenericalteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaGenericproductGenericalteration. When set to true, GenericproductCollectionViaGenericproductGenericalteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaGenericproductGenericalteration is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaGenericproductGenericalteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaGenericproductGenericalteration
		{
			get	{ return _alwaysFetchGenericproductCollectionViaGenericproductGenericalteration; }
			set	{ _alwaysFetchGenericproductCollectionViaGenericproductGenericalteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaGenericproductGenericalteration already has been fetched. Setting this property to false when GenericproductCollectionViaGenericproductGenericalteration has been fetched
		/// will clear the GenericproductCollectionViaGenericproductGenericalteration collection well. Setting this property to true while GenericproductCollectionViaGenericproductGenericalteration hasn't been fetched disables lazy loading for GenericproductCollectionViaGenericproductGenericalteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaGenericproductGenericalteration
		{
			get { return _alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration && !value && (_genericproductCollectionViaGenericproductGenericalteration != null))
				{
					_genericproductCollectionViaGenericproductGenericalteration.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaGenericproductGenericalteration = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosalterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosalterationCollectionViaAlteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosalterationCollection PosalterationCollectionViaAlteration
		{
			get { return GetMultiPosalterationCollectionViaAlteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosalterationCollectionViaAlteration. When set to true, PosalterationCollectionViaAlteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosalterationCollectionViaAlteration is accessed. You can always execute a forced fetch by calling GetMultiPosalterationCollectionViaAlteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosalterationCollectionViaAlteration
		{
			get	{ return _alwaysFetchPosalterationCollectionViaAlteration; }
			set	{ _alwaysFetchPosalterationCollectionViaAlteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosalterationCollectionViaAlteration already has been fetched. Setting this property to false when PosalterationCollectionViaAlteration has been fetched
		/// will clear the PosalterationCollectionViaAlteration collection well. Setting this property to true while PosalterationCollectionViaAlteration hasn't been fetched disables lazy loading for PosalterationCollectionViaAlteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosalterationCollectionViaAlteration
		{
			get { return _alreadyFetchedPosalterationCollectionViaAlteration;}
			set 
			{
				if(_alreadyFetchedPosalterationCollectionViaAlteration && !value && (_posalterationCollectionViaAlteration != null))
				{
					_posalterationCollectionViaAlteration.Clear();
				}
				_alreadyFetchedPosalterationCollectionViaAlteration = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericalterationCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

 
		/// <summary> Gets the value of the related field this.BrandEntity.Name.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String BrandName
		{
			get
			{
				BrandEntity relatedEntity = this.BrandEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Name;
			}

		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GenericalterationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
