﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Scheduleitem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ScheduleitemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ScheduleitemEntity"; }
		}
	
		#region Class Member Declarations
		private ScheduleEntity _scheduleEntity;
		private bool	_alwaysFetchScheduleEntity, _alreadyFetchedScheduleEntity, _scheduleEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ScheduleEntity</summary>
			public static readonly string ScheduleEntity = "ScheduleEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ScheduleitemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ScheduleitemEntityBase() :base("ScheduleitemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		protected ScheduleitemEntityBase(System.Int32 scheduleitemId):base("ScheduleitemEntity")
		{
			InitClassFetch(scheduleitemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ScheduleitemEntityBase(System.Int32 scheduleitemId, IPrefetchPath prefetchPathToUse): base("ScheduleitemEntity")
		{
			InitClassFetch(scheduleitemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="validator">The custom validator object for this ScheduleitemEntity</param>
		protected ScheduleitemEntityBase(System.Int32 scheduleitemId, IValidator validator):base("ScheduleitemEntity")
		{
			InitClassFetch(scheduleitemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduleitemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_scheduleEntity = (ScheduleEntity)info.GetValue("_scheduleEntity", typeof(ScheduleEntity));
			if(_scheduleEntity!=null)
			{
				_scheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduleEntityReturnsNewIfNotFound = info.GetBoolean("_scheduleEntityReturnsNewIfNotFound");
			_alwaysFetchScheduleEntity = info.GetBoolean("_alwaysFetchScheduleEntity");
			_alreadyFetchedScheduleEntity = info.GetBoolean("_alreadyFetchedScheduleEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ScheduleitemFieldIndex)fieldIndex)
			{
				case ScheduleitemFieldIndex.ScheduleId:
					DesetupSyncScheduleEntity(true, false);
					_alreadyFetchedScheduleEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedScheduleEntity = (_scheduleEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ScheduleEntity":
					toReturn.Add(Relations.ScheduleEntityUsingScheduleId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_scheduleEntity", (!this.MarkedForDeletion?_scheduleEntity:null));
			info.AddValue("_scheduleEntityReturnsNewIfNotFound", _scheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduleEntity", _alwaysFetchScheduleEntity);
			info.AddValue("_alreadyFetchedScheduleEntity", _alreadyFetchedScheduleEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ScheduleEntity":
					_alreadyFetchedScheduleEntity = true;
					this.ScheduleEntity = (ScheduleEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ScheduleEntity":
					SetupSyncScheduleEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ScheduleEntity":
					DesetupSyncScheduleEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_scheduleEntity!=null)
			{
				toReturn.Add(_scheduleEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduleitemId)
		{
			return FetchUsingPK(scheduleitemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduleitemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(scheduleitemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduleitemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(scheduleitemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 scheduleitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(scheduleitemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ScheduleitemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ScheduleitemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduleEntity' which is related to this entity.</returns>
		public ScheduleEntity GetSingleScheduleEntity()
		{
			return GetSingleScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduleEntity' which is related to this entity.</returns>
		public virtual ScheduleEntity GetSingleScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduleEntity || forceFetch || _alwaysFetchScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduleEntityUsingScheduleId);
				ScheduleEntity newEntity = new ScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduleId);
				}
				if(fetchResult)
				{
					newEntity = (ScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduleEntity = newEntity;
				_alreadyFetchedScheduleEntity = fetchResult;
			}
			return _scheduleEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ScheduleEntity", _scheduleEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="validator">The validator object for this ScheduleitemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 scheduleitemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(scheduleitemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_scheduleEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduleitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DayOfWeek", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _scheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduleEntity, new PropertyChangedEventHandler( OnScheduleEntityPropertyChanged ), "ScheduleEntity", Obymobi.Data.RelationClasses.StaticScheduleitemRelations.ScheduleEntityUsingScheduleIdStatic, true, signalRelatedEntity, "ScheduleitemCollection", resetFKFields, new int[] { (int)ScheduleitemFieldIndex.ScheduleId } );		
			_scheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduleEntity(IEntityCore relatedEntity)
		{
			if(_scheduleEntity!=relatedEntity)
			{		
				DesetupSyncScheduleEntity(true, true);
				_scheduleEntity = (ScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduleEntity, new PropertyChangedEventHandler( OnScheduleEntityPropertyChanged ), "ScheduleEntity", Obymobi.Data.RelationClasses.StaticScheduleitemRelations.ScheduleEntityUsingScheduleIdStatic, true, ref _alreadyFetchedScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="scheduleitemId">PK value for Scheduleitem which data should be fetched into this Scheduleitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 scheduleitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ScheduleitemFieldIndex.ScheduleitemId].ForcedCurrentValueWrite(scheduleitemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateScheduleitemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ScheduleitemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ScheduleitemRelations Relations
		{
			get	{ return new ScheduleitemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Schedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduleCollection(), (IEntityRelation)GetRelationsForField("ScheduleEntity")[0], (int)Obymobi.Data.EntityType.ScheduleitemEntity, (int)Obymobi.Data.EntityType.ScheduleEntity, 0, null, null, null, "ScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ScheduleitemId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ScheduleitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ScheduleitemId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ScheduleitemId, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.ScheduleitemId, value, true); }
		}

		/// <summary> The ScheduleId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScheduleId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ScheduleId, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.ScheduleId, value, true); }
		}

		/// <summary> The TimeStart property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."TimeStart"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TimeStart
		{
			get { return (System.String)GetValue((int)ScheduleitemFieldIndex.TimeStart, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.TimeStart, value, true); }
		}

		/// <summary> The TimeEnd property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."TimeEnd"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TimeEnd
		{
			get { return (System.String)GetValue((int)ScheduleitemFieldIndex.TimeEnd, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.TimeEnd, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleitemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ScheduleitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ScheduleitemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The DayOfWeek property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."DayOfWeek"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DayOfWeek> DayOfWeek
		{
			get { return (Nullable<System.DayOfWeek>)GetValue((int)ScheduleitemFieldIndex.DayOfWeek, false); }
			set	{ SetValue((int)ScheduleitemFieldIndex.DayOfWeek, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Scheduleitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Scheduleitem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ScheduleitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ScheduleitemFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduleEntity ScheduleEntity
		{
			get	{ return GetSingleScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ScheduleitemCollection", "ScheduleEntity", _scheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduleEntity. When set to true, ScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduleEntity
		{
			get	{ return _alwaysFetchScheduleEntity; }
			set	{ _alwaysFetchScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduleEntity already has been fetched. Setting this property to false when ScheduleEntity has been fetched
		/// will set ScheduleEntity to null as well. Setting this property to true while ScheduleEntity hasn't been fetched disables lazy loading for ScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduleEntity
		{
			get { return _alreadyFetchedScheduleEntity;}
			set 
			{
				if(_alreadyFetchedScheduleEntity && !value)
				{
					this.ScheduleEntity = null;
				}
				_alreadyFetchedScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduleEntity is not found
		/// in the database. When set to true, ScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _scheduleEntityReturnsNewIfNotFound; }
			set { _scheduleEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ScheduleitemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
