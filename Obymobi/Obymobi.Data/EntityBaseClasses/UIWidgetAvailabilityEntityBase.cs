﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UIWidgetAvailability'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIWidgetAvailabilityEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIWidgetAvailabilityEntity"; }
		}
	
		#region Class Member Declarations
		private AvailabilityEntity _availabilityEntity;
		private bool	_alwaysFetchAvailabilityEntity, _alreadyFetchedAvailabilityEntity, _availabilityEntityReturnsNewIfNotFound;
		private UIWidgetEntity _uIWidgetEntity;
		private bool	_alwaysFetchUIWidgetEntity, _alreadyFetchedUIWidgetEntity, _uIWidgetEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AvailabilityEntity</summary>
			public static readonly string AvailabilityEntity = "AvailabilityEntity";
			/// <summary>Member name UIWidgetEntity</summary>
			public static readonly string UIWidgetEntity = "UIWidgetEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIWidgetAvailabilityEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIWidgetAvailabilityEntityBase() :base("UIWidgetAvailabilityEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		protected UIWidgetAvailabilityEntityBase(System.Int32 uIWidgetAvailabilityId):base("UIWidgetAvailabilityEntity")
		{
			InitClassFetch(uIWidgetAvailabilityId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIWidgetAvailabilityEntityBase(System.Int32 uIWidgetAvailabilityId, IPrefetchPath prefetchPathToUse): base("UIWidgetAvailabilityEntity")
		{
			InitClassFetch(uIWidgetAvailabilityId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="validator">The custom validator object for this UIWidgetAvailabilityEntity</param>
		protected UIWidgetAvailabilityEntityBase(System.Int32 uIWidgetAvailabilityId, IValidator validator):base("UIWidgetAvailabilityEntity")
		{
			InitClassFetch(uIWidgetAvailabilityId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIWidgetAvailabilityEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_availabilityEntity = (AvailabilityEntity)info.GetValue("_availabilityEntity", typeof(AvailabilityEntity));
			if(_availabilityEntity!=null)
			{
				_availabilityEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_availabilityEntityReturnsNewIfNotFound = info.GetBoolean("_availabilityEntityReturnsNewIfNotFound");
			_alwaysFetchAvailabilityEntity = info.GetBoolean("_alwaysFetchAvailabilityEntity");
			_alreadyFetchedAvailabilityEntity = info.GetBoolean("_alreadyFetchedAvailabilityEntity");

			_uIWidgetEntity = (UIWidgetEntity)info.GetValue("_uIWidgetEntity", typeof(UIWidgetEntity));
			if(_uIWidgetEntity!=null)
			{
				_uIWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_uIWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchUIWidgetEntity = info.GetBoolean("_alwaysFetchUIWidgetEntity");
			_alreadyFetchedUIWidgetEntity = info.GetBoolean("_alreadyFetchedUIWidgetEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIWidgetAvailabilityFieldIndex)fieldIndex)
			{
				case UIWidgetAvailabilityFieldIndex.UIWidgetId:
					DesetupSyncUIWidgetEntity(true, false);
					_alreadyFetchedUIWidgetEntity = false;
					break;
				case UIWidgetAvailabilityFieldIndex.AvailabilityId:
					DesetupSyncAvailabilityEntity(true, false);
					_alreadyFetchedAvailabilityEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAvailabilityEntity = (_availabilityEntity != null);
			_alreadyFetchedUIWidgetEntity = (_uIWidgetEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AvailabilityEntity":
					toReturn.Add(Relations.AvailabilityEntityUsingAvailabilityId);
					break;
				case "UIWidgetEntity":
					toReturn.Add(Relations.UIWidgetEntityUsingUIWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_availabilityEntity", (!this.MarkedForDeletion?_availabilityEntity:null));
			info.AddValue("_availabilityEntityReturnsNewIfNotFound", _availabilityEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAvailabilityEntity", _alwaysFetchAvailabilityEntity);
			info.AddValue("_alreadyFetchedAvailabilityEntity", _alreadyFetchedAvailabilityEntity);
			info.AddValue("_uIWidgetEntity", (!this.MarkedForDeletion?_uIWidgetEntity:null));
			info.AddValue("_uIWidgetEntityReturnsNewIfNotFound", _uIWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIWidgetEntity", _alwaysFetchUIWidgetEntity);
			info.AddValue("_alreadyFetchedUIWidgetEntity", _alreadyFetchedUIWidgetEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AvailabilityEntity":
					_alreadyFetchedAvailabilityEntity = true;
					this.AvailabilityEntity = (AvailabilityEntity)entity;
					break;
				case "UIWidgetEntity":
					_alreadyFetchedUIWidgetEntity = true;
					this.UIWidgetEntity = (UIWidgetEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AvailabilityEntity":
					SetupSyncAvailabilityEntity(relatedEntity);
					break;
				case "UIWidgetEntity":
					SetupSyncUIWidgetEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AvailabilityEntity":
					DesetupSyncAvailabilityEntity(false, true);
					break;
				case "UIWidgetEntity":
					DesetupSyncUIWidgetEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_availabilityEntity!=null)
			{
				toReturn.Add(_availabilityEntity);
			}
			if(_uIWidgetEntity!=null)
			{
				toReturn.Add(_uIWidgetEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetAvailabilityId)
		{
			return FetchUsingPK(uIWidgetAvailabilityId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetAvailabilityId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIWidgetAvailabilityId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetAvailabilityId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIWidgetAvailabilityId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIWidgetAvailabilityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIWidgetAvailabilityId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIWidgetAvailabilityId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIWidgetAvailabilityRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AvailabilityEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AvailabilityEntity' which is related to this entity.</returns>
		public AvailabilityEntity GetSingleAvailabilityEntity()
		{
			return GetSingleAvailabilityEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AvailabilityEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AvailabilityEntity' which is related to this entity.</returns>
		public virtual AvailabilityEntity GetSingleAvailabilityEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAvailabilityEntity || forceFetch || _alwaysFetchAvailabilityEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AvailabilityEntityUsingAvailabilityId);
				AvailabilityEntity newEntity = new AvailabilityEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AvailabilityId);
				}
				if(fetchResult)
				{
					newEntity = (AvailabilityEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_availabilityEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AvailabilityEntity = newEntity;
				_alreadyFetchedAvailabilityEntity = fetchResult;
			}
			return _availabilityEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public UIWidgetEntity GetSingleUIWidgetEntity()
		{
			return GetSingleUIWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public virtual UIWidgetEntity GetSingleUIWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIWidgetEntity || forceFetch || _alwaysFetchUIWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIWidgetEntityUsingUIWidgetId);
				UIWidgetEntity newEntity = new UIWidgetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIWidgetId);
				}
				if(fetchResult)
				{
					newEntity = (UIWidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIWidgetEntity = newEntity;
				_alreadyFetchedUIWidgetEntity = fetchResult;
			}
			return _uIWidgetEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AvailabilityEntity", _availabilityEntity);
			toReturn.Add("UIWidgetEntity", _uIWidgetEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="validator">The validator object for this UIWidgetAvailabilityEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIWidgetAvailabilityId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIWidgetAvailabilityId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_availabilityEntityReturnsNewIfNotFound = true;
			_uIWidgetEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetAvailabilityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailabilityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _availabilityEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAvailabilityEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _availabilityEntity, new PropertyChangedEventHandler( OnAvailabilityEntityPropertyChanged ), "AvailabilityEntity", Obymobi.Data.RelationClasses.StaticUIWidgetAvailabilityRelations.AvailabilityEntityUsingAvailabilityIdStatic, true, signalRelatedEntity, "UIWidgetAvailabilityCollection", resetFKFields, new int[] { (int)UIWidgetAvailabilityFieldIndex.AvailabilityId } );		
			_availabilityEntity = null;
		}
		
		/// <summary> setups the sync logic for member _availabilityEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAvailabilityEntity(IEntityCore relatedEntity)
		{
			if(_availabilityEntity!=relatedEntity)
			{		
				DesetupSyncAvailabilityEntity(true, true);
				_availabilityEntity = (AvailabilityEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _availabilityEntity, new PropertyChangedEventHandler( OnAvailabilityEntityPropertyChanged ), "AvailabilityEntity", Obymobi.Data.RelationClasses.StaticUIWidgetAvailabilityRelations.AvailabilityEntityUsingAvailabilityIdStatic, true, ref _alreadyFetchedAvailabilityEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAvailabilityEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticUIWidgetAvailabilityRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, signalRelatedEntity, "UIWidgetAvailabilityCollection", resetFKFields, new int[] { (int)UIWidgetAvailabilityFieldIndex.UIWidgetId } );		
			_uIWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIWidgetEntity(IEntityCore relatedEntity)
		{
			if(_uIWidgetEntity!=relatedEntity)
			{		
				DesetupSyncUIWidgetEntity(true, true);
				_uIWidgetEntity = (UIWidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticUIWidgetAvailabilityRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, ref _alreadyFetchedUIWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIWidgetAvailabilityId">PK value for UIWidgetAvailability which data should be fetched into this UIWidgetAvailability object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIWidgetAvailabilityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIWidgetAvailabilityFieldIndex.UIWidgetAvailabilityId].ForcedCurrentValueWrite(uIWidgetAvailabilityId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIWidgetAvailabilityDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIWidgetAvailabilityEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIWidgetAvailabilityRelations Relations
		{
			get	{ return new UIWidgetAvailabilityRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetAvailabilityEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetEntity")[0], (int)Obymobi.Data.EntityType.UIWidgetAvailabilityEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIWidgetAvailabilityId property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."UIWidgetAvailabilityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIWidgetAvailabilityId
		{
			get { return (System.Int32)GetValue((int)UIWidgetAvailabilityFieldIndex.UIWidgetAvailabilityId, true); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.UIWidgetAvailabilityId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetAvailabilityFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The UIWidgetId property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."UIWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UIWidgetId
		{
			get { return (System.Int32)GetValue((int)UIWidgetAvailabilityFieldIndex.UIWidgetId, true); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.UIWidgetId, value, true); }
		}

		/// <summary> The AvailabilityId property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."AvailabilityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AvailabilityId
		{
			get { return (System.Int32)GetValue((int)UIWidgetAvailabilityFieldIndex.AvailabilityId, true); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.AvailabilityId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIWidgetAvailabilityFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetAvailabilityFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIWidgetAvailabilityFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIWidgetAvailabilityFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity UIWidgetAvailability<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIWidgetAvailability"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)UIWidgetAvailabilityFieldIndex.SortOrder, true); }
			set	{ SetValue((int)UIWidgetAvailabilityFieldIndex.SortOrder, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AvailabilityEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAvailabilityEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AvailabilityEntity AvailabilityEntity
		{
			get	{ return GetSingleAvailabilityEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAvailabilityEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetAvailabilityCollection", "AvailabilityEntity", _availabilityEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityEntity. When set to true, AvailabilityEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityEntity is accessed. You can always execute a forced fetch by calling GetSingleAvailabilityEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityEntity
		{
			get	{ return _alwaysFetchAvailabilityEntity; }
			set	{ _alwaysFetchAvailabilityEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityEntity already has been fetched. Setting this property to false when AvailabilityEntity has been fetched
		/// will set AvailabilityEntity to null as well. Setting this property to true while AvailabilityEntity hasn't been fetched disables lazy loading for AvailabilityEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityEntity
		{
			get { return _alreadyFetchedAvailabilityEntity;}
			set 
			{
				if(_alreadyFetchedAvailabilityEntity && !value)
				{
					this.AvailabilityEntity = null;
				}
				_alreadyFetchedAvailabilityEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AvailabilityEntity is not found
		/// in the database. When set to true, AvailabilityEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AvailabilityEntityReturnsNewIfNotFound
		{
			get	{ return _availabilityEntityReturnsNewIfNotFound; }
			set { _availabilityEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIWidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIWidgetEntity UIWidgetEntity
		{
			get	{ return GetSingleUIWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIWidgetAvailabilityCollection", "UIWidgetEntity", _uIWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetEntity. When set to true, UIWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleUIWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetEntity
		{
			get	{ return _alwaysFetchUIWidgetEntity; }
			set	{ _alwaysFetchUIWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetEntity already has been fetched. Setting this property to false when UIWidgetEntity has been fetched
		/// will set UIWidgetEntity to null as well. Setting this property to true while UIWidgetEntity hasn't been fetched disables lazy loading for UIWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetEntity
		{
			get { return _alreadyFetchedUIWidgetEntity;}
			set 
			{
				if(_alreadyFetchedUIWidgetEntity && !value)
				{
					this.UIWidgetEntity = null;
				}
				_alreadyFetchedUIWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIWidgetEntity is not found
		/// in the database. When set to true, UIWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _uIWidgetEntityReturnsNewIfNotFound; }
			set { _uIWidgetEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIWidgetAvailabilityEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
