﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PaymentProvider'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PaymentProviderEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PaymentProviderEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection	_paymentIntegrationConfigurationCollection;
		private bool	_alwaysFetchPaymentIntegrationConfigurationCollection, _alreadyFetchedPaymentIntegrationConfigurationCollection;
		private Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection	_paymentProviderCompanyCollection;
		private bool	_alwaysFetchPaymentProviderCompanyCollection, _alreadyFetchedPaymentProviderCompanyCollection;
		private Obymobi.Data.CollectionClasses.PaymentTransactionCollection	_paymentTransactionCollection;
		private bool	_alwaysFetchPaymentTransactionCollection, _alreadyFetchedPaymentTransactionCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name PaymentIntegrationConfigurationCollection</summary>
			public static readonly string PaymentIntegrationConfigurationCollection = "PaymentIntegrationConfigurationCollection";
			/// <summary>Member name PaymentProviderCompanyCollection</summary>
			public static readonly string PaymentProviderCompanyCollection = "PaymentProviderCompanyCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentProviderEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PaymentProviderEntityBase() :base("PaymentProviderEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		protected PaymentProviderEntityBase(System.Int32 paymentProviderId):base("PaymentProviderEntity")
		{
			InitClassFetch(paymentProviderId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PaymentProviderEntityBase(System.Int32 paymentProviderId, IPrefetchPath prefetchPathToUse): base("PaymentProviderEntity")
		{
			InitClassFetch(paymentProviderId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="validator">The custom validator object for this PaymentProviderEntity</param>
		protected PaymentProviderEntityBase(System.Int32 paymentProviderId, IValidator validator):base("PaymentProviderEntity")
		{
			InitClassFetch(paymentProviderId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentProviderEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentIntegrationConfigurationCollection = (Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection)info.GetValue("_paymentIntegrationConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection));
			_alwaysFetchPaymentIntegrationConfigurationCollection = info.GetBoolean("_alwaysFetchPaymentIntegrationConfigurationCollection");
			_alreadyFetchedPaymentIntegrationConfigurationCollection = info.GetBoolean("_alreadyFetchedPaymentIntegrationConfigurationCollection");

			_paymentProviderCompanyCollection = (Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection)info.GetValue("_paymentProviderCompanyCollection", typeof(Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection));
			_alwaysFetchPaymentProviderCompanyCollection = info.GetBoolean("_alwaysFetchPaymentProviderCompanyCollection");
			_alreadyFetchedPaymentProviderCompanyCollection = info.GetBoolean("_alreadyFetchedPaymentProviderCompanyCollection");

			_paymentTransactionCollection = (Obymobi.Data.CollectionClasses.PaymentTransactionCollection)info.GetValue("_paymentTransactionCollection", typeof(Obymobi.Data.CollectionClasses.PaymentTransactionCollection));
			_alwaysFetchPaymentTransactionCollection = info.GetBoolean("_alwaysFetchPaymentTransactionCollection");
			_alreadyFetchedPaymentTransactionCollection = info.GetBoolean("_alreadyFetchedPaymentTransactionCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentProviderFieldIndex)fieldIndex)
			{
				case PaymentProviderFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentIntegrationConfigurationCollection = (_paymentIntegrationConfigurationCollection.Count > 0);
			_alreadyFetchedPaymentProviderCompanyCollection = (_paymentProviderCompanyCollection.Count > 0);
			_alreadyFetchedPaymentTransactionCollection = (_paymentTransactionCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "PaymentIntegrationConfigurationCollection":
					toReturn.Add(Relations.PaymentIntegrationConfigurationEntityUsingPaymentProviderId);
					break;
				case "PaymentProviderCompanyCollection":
					toReturn.Add(Relations.PaymentProviderCompanyEntityUsingPaymentProviderId);
					break;
				case "PaymentTransactionCollection":
					toReturn.Add(Relations.PaymentTransactionEntityUsingPaymentProviderId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentIntegrationConfigurationCollection", (!this.MarkedForDeletion?_paymentIntegrationConfigurationCollection:null));
			info.AddValue("_alwaysFetchPaymentIntegrationConfigurationCollection", _alwaysFetchPaymentIntegrationConfigurationCollection);
			info.AddValue("_alreadyFetchedPaymentIntegrationConfigurationCollection", _alreadyFetchedPaymentIntegrationConfigurationCollection);
			info.AddValue("_paymentProviderCompanyCollection", (!this.MarkedForDeletion?_paymentProviderCompanyCollection:null));
			info.AddValue("_alwaysFetchPaymentProviderCompanyCollection", _alwaysFetchPaymentProviderCompanyCollection);
			info.AddValue("_alreadyFetchedPaymentProviderCompanyCollection", _alreadyFetchedPaymentProviderCompanyCollection);
			info.AddValue("_paymentTransactionCollection", (!this.MarkedForDeletion?_paymentTransactionCollection:null));
			info.AddValue("_alwaysFetchPaymentTransactionCollection", _alwaysFetchPaymentTransactionCollection);
			info.AddValue("_alreadyFetchedPaymentTransactionCollection", _alreadyFetchedPaymentTransactionCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "PaymentIntegrationConfigurationCollection":
					_alreadyFetchedPaymentIntegrationConfigurationCollection = true;
					if(entity!=null)
					{
						this.PaymentIntegrationConfigurationCollection.Add((PaymentIntegrationConfigurationEntity)entity);
					}
					break;
				case "PaymentProviderCompanyCollection":
					_alreadyFetchedPaymentProviderCompanyCollection = true;
					if(entity!=null)
					{
						this.PaymentProviderCompanyCollection.Add((PaymentProviderCompanyEntity)entity);
					}
					break;
				case "PaymentTransactionCollection":
					_alreadyFetchedPaymentTransactionCollection = true;
					if(entity!=null)
					{
						this.PaymentTransactionCollection.Add((PaymentTransactionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "PaymentIntegrationConfigurationCollection":
					_paymentIntegrationConfigurationCollection.Add((PaymentIntegrationConfigurationEntity)relatedEntity);
					break;
				case "PaymentProviderCompanyCollection":
					_paymentProviderCompanyCollection.Add((PaymentProviderCompanyEntity)relatedEntity);
					break;
				case "PaymentTransactionCollection":
					_paymentTransactionCollection.Add((PaymentTransactionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "PaymentIntegrationConfigurationCollection":
					this.PerformRelatedEntityRemoval(_paymentIntegrationConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentProviderCompanyCollection":
					this.PerformRelatedEntityRemoval(_paymentProviderCompanyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentTransactionCollection":
					this.PerformRelatedEntityRemoval(_paymentTransactionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_paymentIntegrationConfigurationCollection);
			toReturn.Add(_paymentProviderCompanyCollection);
			toReturn.Add(_paymentTransactionCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentProviderId)
		{
			return FetchUsingPK(paymentProviderId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentProviderId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentProviderId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentProviderId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentProviderId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentProviderId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentProviderId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentProviderId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentProviderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntegrationConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentIntegrationConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection GetMultiPaymentIntegrationConfigurationCollection(bool forceFetch)
		{
			return GetMultiPaymentIntegrationConfigurationCollection(forceFetch, _paymentIntegrationConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntegrationConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentIntegrationConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection GetMultiPaymentIntegrationConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentIntegrationConfigurationCollection(forceFetch, _paymentIntegrationConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntegrationConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection GetMultiPaymentIntegrationConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentIntegrationConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntegrationConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection GetMultiPaymentIntegrationConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentIntegrationConfigurationCollection || forceFetch || _alwaysFetchPaymentIntegrationConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentIntegrationConfigurationCollection);
				_paymentIntegrationConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentIntegrationConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentIntegrationConfigurationCollection.GetMultiManyToOne(null, this, filter);
				_paymentIntegrationConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentIntegrationConfigurationCollection = true;
			}
			return _paymentIntegrationConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentIntegrationConfigurationCollection'. These settings will be taken into account
		/// when the property PaymentIntegrationConfigurationCollection is requested or GetMultiPaymentIntegrationConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentIntegrationConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentIntegrationConfigurationCollection.SortClauses=sortClauses;
			_paymentIntegrationConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentProviderCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection GetMultiPaymentProviderCompanyCollection(bool forceFetch)
		{
			return GetMultiPaymentProviderCompanyCollection(forceFetch, _paymentProviderCompanyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentProviderCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection GetMultiPaymentProviderCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentProviderCompanyCollection(forceFetch, _paymentProviderCompanyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection GetMultiPaymentProviderCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentProviderCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection GetMultiPaymentProviderCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentProviderCompanyCollection || forceFetch || _alwaysFetchPaymentProviderCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentProviderCompanyCollection);
				_paymentProviderCompanyCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentProviderCompanyCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentProviderCompanyCollection.GetMultiManyToOne(null, this, filter);
				_paymentProviderCompanyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentProviderCompanyCollection = true;
			}
			return _paymentProviderCompanyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentProviderCompanyCollection'. These settings will be taken into account
		/// when the property PaymentProviderCompanyCollection is requested or GetMultiPaymentProviderCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentProviderCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentProviderCompanyCollection.SortClauses=sortClauses;
			_paymentProviderCompanyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentTransactionCollection || forceFetch || _alwaysFetchPaymentTransactionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentTransactionCollection);
				_paymentTransactionCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentTransactionCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentTransactionCollection.GetMultiManyToOne(null, null, this, filter);
				_paymentTransactionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentTransactionCollection = true;
			}
			return _paymentTransactionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentTransactionCollection'. These settings will be taken into account
		/// when the property PaymentTransactionCollection is requested or GetMultiPaymentTransactionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentTransactionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentTransactionCollection.SortClauses=sortClauses;
			_paymentTransactionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("PaymentIntegrationConfigurationCollection", _paymentIntegrationConfigurationCollection);
			toReturn.Add("PaymentProviderCompanyCollection", _paymentProviderCompanyCollection);
			toReturn.Add("PaymentTransactionCollection", _paymentTransactionCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="validator">The validator object for this PaymentProviderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 paymentProviderId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentProviderId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_paymentIntegrationConfigurationCollection = new Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection();
			_paymentIntegrationConfigurationCollection.SetContainingEntityInfo(this, "PaymentProviderEntity");

			_paymentProviderCompanyCollection = new Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection();
			_paymentProviderCompanyCollection.SetContainingEntityInfo(this, "PaymentProviderEntity");

			_paymentTransactionCollection = new Obymobi.Data.CollectionClasses.PaymentTransactionCollection();
			_paymentTransactionCollection.SetContainingEntityInfo(this, "PaymentProviderEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Environment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApiKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LiveEndpointUrlPrefix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginDomain", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProcessorInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProcessorInfoFooter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenUsername", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenPassword", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OmisePublicKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OmiseSecretKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SkinCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HmacKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenMerchantCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePayMerchantIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplePayMerchantIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPaymentProviderRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PaymentProviderCollection", resetFKFields, new int[] { (int)PaymentProviderFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPaymentProviderRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentProviderId">PK value for PaymentProvider which data should be fetched into this PaymentProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 paymentProviderId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentProviderFieldIndex.PaymentProviderId].ForcedCurrentValueWrite(paymentProviderId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentProviderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentProviderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentProviderRelations Relations
		{
			get	{ return new PaymentProviderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentIntegrationConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection(), (IEntityRelation)GetRelationsForField("PaymentIntegrationConfigurationCollection")[0], (int)Obymobi.Data.EntityType.PaymentProviderEntity, (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, 0, null, null, null, "PaymentIntegrationConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentProviderCompany' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentProviderCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection(), (IEntityRelation)GetRelationsForField("PaymentProviderCompanyCollection")[0], (int)Obymobi.Data.EntityType.PaymentProviderEntity, (int)Obymobi.Data.EntityType.PaymentProviderCompanyEntity, 0, null, null, null, "PaymentProviderCompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionCollection")[0], (int)Obymobi.Data.EntityType.PaymentProviderEntity, (int)Obymobi.Data.EntityType.PaymentTransactionEntity, 0, null, null, null, "PaymentTransactionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PaymentProviderEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PaymentProviderId property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProviderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentProviderId
		{
			get { return (System.Int32)GetValue((int)PaymentProviderFieldIndex.PaymentProviderId, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.PaymentProviderId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentProviderFieldIndex.CompanyId, false); }
			set	{ SetValue((int)PaymentProviderFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Type property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentProviderType Type
		{
			get { return (Obymobi.Enums.PaymentProviderType)GetValue((int)PaymentProviderFieldIndex.Type, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Type, value, true); }
		}

		/// <summary> The Name property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.Name, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Name, value, true); }
		}

		/// <summary> The Environment property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."Environment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentProviderEnvironment Environment
		{
			get { return (Obymobi.Enums.PaymentProviderEnvironment)GetValue((int)PaymentProviderFieldIndex.Environment, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.Environment, value, true); }
		}

		/// <summary> The ApiKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ApiKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApiKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ApiKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ApiKey, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)PaymentProviderFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentProviderFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentProviderFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LiveEndpointUrlPrefix property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."LiveEndpointUrlPrefix"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LiveEndpointUrlPrefix
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.LiveEndpointUrlPrefix, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.LiveEndpointUrlPrefix, value, true); }
		}

		/// <summary> The OriginDomain property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OriginDomain"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OriginDomain
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OriginDomain, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OriginDomain, value, true); }
		}

		/// <summary> The PaymentProcessorInfo property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProcessorInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfo
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfo, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfo, value, true); }
		}

		/// <summary> The PaymentProcessorInfoFooter property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."PaymentProcessorInfoFooter"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProcessorInfoFooter
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfoFooter, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.PaymentProcessorInfoFooter, value, true); }
		}

		/// <summary> The AdyenUsername property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenUsername"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenUsername
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenUsername, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenUsername, value, true); }
		}

		/// <summary> The AdyenPassword property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenPassword"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenPassword
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenPassword, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenPassword, value, true); }
		}

		/// <summary> The OmisePublicKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OmisePublicKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OmisePublicKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OmisePublicKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OmisePublicKey, value, true); }
		}

		/// <summary> The OmiseSecretKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."OmiseSecretKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OmiseSecretKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.OmiseSecretKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.OmiseSecretKey, value, true); }
		}

		/// <summary> The SkinCode property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."SkinCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SkinCode
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.SkinCode, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.SkinCode, value, true); }
		}

		/// <summary> The HmacKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."HmacKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HmacKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.HmacKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.HmacKey, value, true); }
		}

		/// <summary> The ClientKey property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ClientKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientKey
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ClientKey, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ClientKey, value, true); }
		}

		/// <summary> The AdyenMerchantCode property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."AdyenMerchantCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdyenMerchantCode
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.AdyenMerchantCode, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.AdyenMerchantCode, value, true); }
		}

		/// <summary> The GooglePayMerchantIdentifier property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."GooglePayMerchantIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.GooglePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.GooglePayMerchantIdentifier, value, true); }
		}

		/// <summary> The ApplePayMerchantIdentifier property of the Entity PaymentProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentProvider"."ApplePayMerchantIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplePayMerchantIdentifier
		{
			get { return (System.String)GetValue((int)PaymentProviderFieldIndex.ApplePayMerchantIdentifier, true); }
			set	{ SetValue((int)PaymentProviderFieldIndex.ApplePayMerchantIdentifier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PaymentIntegrationConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentIntegrationConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection PaymentIntegrationConfigurationCollection
		{
			get	{ return GetMultiPaymentIntegrationConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentIntegrationConfigurationCollection. When set to true, PaymentIntegrationConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentIntegrationConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentIntegrationConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentIntegrationConfigurationCollection
		{
			get	{ return _alwaysFetchPaymentIntegrationConfigurationCollection; }
			set	{ _alwaysFetchPaymentIntegrationConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentIntegrationConfigurationCollection already has been fetched. Setting this property to false when PaymentIntegrationConfigurationCollection has been fetched
		/// will clear the PaymentIntegrationConfigurationCollection collection well. Setting this property to true while PaymentIntegrationConfigurationCollection hasn't been fetched disables lazy loading for PaymentIntegrationConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentIntegrationConfigurationCollection
		{
			get { return _alreadyFetchedPaymentIntegrationConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedPaymentIntegrationConfigurationCollection && !value && (_paymentIntegrationConfigurationCollection != null))
				{
					_paymentIntegrationConfigurationCollection.Clear();
				}
				_alreadyFetchedPaymentIntegrationConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentProviderCompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentProviderCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentProviderCompanyCollection PaymentProviderCompanyCollection
		{
			get	{ return GetMultiPaymentProviderCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentProviderCompanyCollection. When set to true, PaymentProviderCompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentProviderCompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentProviderCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentProviderCompanyCollection
		{
			get	{ return _alwaysFetchPaymentProviderCompanyCollection; }
			set	{ _alwaysFetchPaymentProviderCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentProviderCompanyCollection already has been fetched. Setting this property to false when PaymentProviderCompanyCollection has been fetched
		/// will clear the PaymentProviderCompanyCollection collection well. Setting this property to true while PaymentProviderCompanyCollection hasn't been fetched disables lazy loading for PaymentProviderCompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentProviderCompanyCollection
		{
			get { return _alreadyFetchedPaymentProviderCompanyCollection;}
			set 
			{
				if(_alreadyFetchedPaymentProviderCompanyCollection && !value && (_paymentProviderCompanyCollection != null))
				{
					_paymentProviderCompanyCollection.Clear();
				}
				_alreadyFetchedPaymentProviderCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentTransactionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection PaymentTransactionCollection
		{
			get	{ return GetMultiPaymentTransactionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionCollection. When set to true, PaymentTransactionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentTransactionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionCollection
		{
			get	{ return _alwaysFetchPaymentTransactionCollection; }
			set	{ _alwaysFetchPaymentTransactionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionCollection already has been fetched. Setting this property to false when PaymentTransactionCollection has been fetched
		/// will clear the PaymentTransactionCollection collection well. Setting this property to true while PaymentTransactionCollection hasn't been fetched disables lazy loading for PaymentTransactionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionCollection
		{
			get { return _alreadyFetchedPaymentTransactionCollection;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionCollection && !value && (_paymentTransactionCollection != null))
				{
					_paymentTransactionCollection.Clear();
				}
				_alreadyFetchedPaymentTransactionCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentProviderCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PaymentProviderEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
