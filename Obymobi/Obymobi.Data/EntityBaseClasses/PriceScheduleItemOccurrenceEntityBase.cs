﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PriceScheduleItemOccurrence'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PriceScheduleItemOccurrenceEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PriceScheduleItemOccurrenceEntity"; }
		}
	
		#region Class Member Declarations
		private PriceScheduleItemEntity _priceScheduleItemEntity;
		private bool	_alwaysFetchPriceScheduleItemEntity, _alreadyFetchedPriceScheduleItemEntity, _priceScheduleItemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PriceScheduleItemEntity</summary>
			public static readonly string PriceScheduleItemEntity = "PriceScheduleItemEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PriceScheduleItemOccurrenceEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PriceScheduleItemOccurrenceEntityBase() :base("PriceScheduleItemOccurrenceEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		protected PriceScheduleItemOccurrenceEntityBase(System.Int32 priceScheduleItemOccurrenceId):base("PriceScheduleItemOccurrenceEntity")
		{
			InitClassFetch(priceScheduleItemOccurrenceId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PriceScheduleItemOccurrenceEntityBase(System.Int32 priceScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse): base("PriceScheduleItemOccurrenceEntity")
		{
			InitClassFetch(priceScheduleItemOccurrenceId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleItemOccurrenceEntity</param>
		protected PriceScheduleItemOccurrenceEntityBase(System.Int32 priceScheduleItemOccurrenceId, IValidator validator):base("PriceScheduleItemOccurrenceEntity")
		{
			InitClassFetch(priceScheduleItemOccurrenceId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleItemOccurrenceEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_priceScheduleItemEntity = (PriceScheduleItemEntity)info.GetValue("_priceScheduleItemEntity", typeof(PriceScheduleItemEntity));
			if(_priceScheduleItemEntity!=null)
			{
				_priceScheduleItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceScheduleItemEntityReturnsNewIfNotFound = info.GetBoolean("_priceScheduleItemEntityReturnsNewIfNotFound");
			_alwaysFetchPriceScheduleItemEntity = info.GetBoolean("_alwaysFetchPriceScheduleItemEntity");
			_alreadyFetchedPriceScheduleItemEntity = info.GetBoolean("_alreadyFetchedPriceScheduleItemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PriceScheduleItemOccurrenceFieldIndex)fieldIndex)
			{
				case PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId:
					DesetupSyncPriceScheduleItemEntity(true, false);
					_alreadyFetchedPriceScheduleItemEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPriceScheduleItemEntity = (_priceScheduleItemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PriceScheduleItemEntity":
					toReturn.Add(Relations.PriceScheduleItemEntityUsingPriceScheduleItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_priceScheduleItemEntity", (!this.MarkedForDeletion?_priceScheduleItemEntity:null));
			info.AddValue("_priceScheduleItemEntityReturnsNewIfNotFound", _priceScheduleItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceScheduleItemEntity", _alwaysFetchPriceScheduleItemEntity);
			info.AddValue("_alreadyFetchedPriceScheduleItemEntity", _alreadyFetchedPriceScheduleItemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PriceScheduleItemEntity":
					_alreadyFetchedPriceScheduleItemEntity = true;
					this.PriceScheduleItemEntity = (PriceScheduleItemEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PriceScheduleItemEntity":
					SetupSyncPriceScheduleItemEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PriceScheduleItemEntity":
					DesetupSyncPriceScheduleItemEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_priceScheduleItemEntity!=null)
			{
				toReturn.Add(_priceScheduleItemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemOccurrenceId)
		{
			return FetchUsingPK(priceScheduleItemOccurrenceId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(priceScheduleItemOccurrenceId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(priceScheduleItemOccurrenceId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(priceScheduleItemOccurrenceId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PriceScheduleItemOccurrenceId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PriceScheduleItemOccurrenceRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceScheduleItemEntity' which is related to this entity.</returns>
		public PriceScheduleItemEntity GetSinglePriceScheduleItemEntity()
		{
			return GetSinglePriceScheduleItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceScheduleItemEntity' which is related to this entity.</returns>
		public virtual PriceScheduleItemEntity GetSinglePriceScheduleItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceScheduleItemEntity || forceFetch || _alwaysFetchPriceScheduleItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceScheduleItemEntityUsingPriceScheduleItemId);
				PriceScheduleItemEntity newEntity = new PriceScheduleItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceScheduleItemId);
				}
				if(fetchResult)
				{
					newEntity = (PriceScheduleItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceScheduleItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceScheduleItemEntity = newEntity;
				_alreadyFetchedPriceScheduleItemEntity = fetchResult;
			}
			return _priceScheduleItemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PriceScheduleItemEntity", _priceScheduleItemEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="validator">The validator object for this PriceScheduleItemOccurrenceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 priceScheduleItemOccurrenceId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(priceScheduleItemOccurrenceId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_priceScheduleItemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleItemOccurrenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Recurring", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceRange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceOccurrenceCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrencePeriodicity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceDayNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceWeekDays", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceWeekOfMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceStartUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurrenceEndUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _priceScheduleItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceScheduleItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceScheduleItemEntity, new PropertyChangedEventHandler( OnPriceScheduleItemEntityPropertyChanged ), "PriceScheduleItemEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemOccurrenceRelations.PriceScheduleItemEntityUsingPriceScheduleItemIdStatic, true, signalRelatedEntity, "PriceScheduleItemOccurrenceCollection", resetFKFields, new int[] { (int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId } );		
			_priceScheduleItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceScheduleItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceScheduleItemEntity(IEntityCore relatedEntity)
		{
			if(_priceScheduleItemEntity!=relatedEntity)
			{		
				DesetupSyncPriceScheduleItemEntity(true, true);
				_priceScheduleItemEntity = (PriceScheduleItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceScheduleItemEntity, new PropertyChangedEventHandler( OnPriceScheduleItemEntityPropertyChanged ), "PriceScheduleItemEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemOccurrenceRelations.PriceScheduleItemEntityUsingPriceScheduleItemIdStatic, true, ref _alreadyFetchedPriceScheduleItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceScheduleItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="priceScheduleItemOccurrenceId">PK value for PriceScheduleItemOccurrence which data should be fetched into this PriceScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 priceScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId].ForcedCurrentValueWrite(priceScheduleItemOccurrenceId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePriceScheduleItemOccurrenceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PriceScheduleItemOccurrenceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PriceScheduleItemOccurrenceRelations Relations
		{
			get	{ return new PriceScheduleItemOccurrenceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceScheduleItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleItemCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleItemEntity")[0], (int)Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity, (int)Obymobi.Data.EntityType.PriceScheduleItemEntity, 0, null, null, null, "PriceScheduleItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PriceScheduleItemOccurrenceId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."PriceScheduleItemOccurrenceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceScheduleItemOccurrenceId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemOccurrenceId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The PriceScheduleItemId property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."PriceScheduleItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceScheduleItemId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.PriceScheduleItemId, value, true); }
		}

		/// <summary> The StartTime property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."StartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTime, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTime, value, true); }
		}

		/// <summary> The EndTime property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."EndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTime, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTime, value, true); }
		}

		/// <summary> The Recurring property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."Recurring"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Recurring
		{
			get { return (System.Boolean)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.Recurring, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.Recurring, value, true); }
		}

		/// <summary> The RecurrenceType property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceType
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceType, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceType, value, true); }
		}

		/// <summary> The RecurrenceRange property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceRange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceRange
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceRange, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceRange, value, true); }
		}

		/// <summary> The RecurrenceStart property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceStart"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStart
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStart, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStart, value, true); }
		}

		/// <summary> The RecurrenceEnd property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceEnd"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEnd, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEnd, value, true); }
		}

		/// <summary> The RecurrenceOccurrenceCount property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceOccurrenceCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceOccurrenceCount
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceOccurrenceCount, value, true); }
		}

		/// <summary> The RecurrencePeriodicity property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrencePeriodicity"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrencePeriodicity
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrencePeriodicity, value, true); }
		}

		/// <summary> The RecurrenceDayNumber property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceDayNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceDayNumber
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceDayNumber, value, true); }
		}

		/// <summary> The RecurrenceWeekDays property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceWeekDays"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekDays
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekDays, value, true); }
		}

		/// <summary> The RecurrenceWeekOfMonth property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceWeekOfMonth"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceWeekOfMonth
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceWeekOfMonth, value, true); }
		}

		/// <summary> The RecurrenceMonth property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceMonth"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecurrenceMonth
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceMonth, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceMonth, value, true); }
		}

		/// <summary> The BackgroundColor property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BackgroundColor
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.BackgroundColor, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.BackgroundColor, value, true); }
		}

		/// <summary> The TextColor property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."TextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextColor
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.TextColor, true); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.TextColor, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The StartTimeUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."StartTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.StartTimeUTC, value, true); }
		}

		/// <summary> The EndTimeUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."EndTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.EndTimeUTC, value, true); }
		}

		/// <summary> The RecurrenceStartUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceStartUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceStartUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceStartUTC, value, true); }
		}

		/// <summary> The RecurrenceEndUTC property of the Entity PriceScheduleItemOccurrence<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItemOccurrence"."RecurrenceEndUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurrenceEndUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, false); }
			set	{ SetValue((int)PriceScheduleItemOccurrenceFieldIndex.RecurrenceEndUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PriceScheduleItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceScheduleItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceScheduleItemEntity PriceScheduleItemEntity
		{
			get	{ return GetSinglePriceScheduleItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceScheduleItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceScheduleItemOccurrenceCollection", "PriceScheduleItemEntity", _priceScheduleItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleItemEntity. When set to true, PriceScheduleItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleItemEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceScheduleItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleItemEntity
		{
			get	{ return _alwaysFetchPriceScheduleItemEntity; }
			set	{ _alwaysFetchPriceScheduleItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleItemEntity already has been fetched. Setting this property to false when PriceScheduleItemEntity has been fetched
		/// will set PriceScheduleItemEntity to null as well. Setting this property to true while PriceScheduleItemEntity hasn't been fetched disables lazy loading for PriceScheduleItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleItemEntity
		{
			get { return _alreadyFetchedPriceScheduleItemEntity;}
			set 
			{
				if(_alreadyFetchedPriceScheduleItemEntity && !value)
				{
					this.PriceScheduleItemEntity = null;
				}
				_alreadyFetchedPriceScheduleItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceScheduleItemEntity is not found
		/// in the database. When set to true, PriceScheduleItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceScheduleItemEntityReturnsNewIfNotFound
		{
			get	{ return _priceScheduleItemEntityReturnsNewIfNotFound; }
			set { _priceScheduleItemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
