﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'MessagegroupDeliverypoint'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MessagegroupDeliverypointEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MessagegroupDeliverypointEntity"; }
		}
	
		#region Class Member Declarations
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private MessagegroupEntity _messagegroupEntity;
		private bool	_alwaysFetchMessagegroupEntity, _alreadyFetchedMessagegroupEntity, _messagegroupEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name MessagegroupEntity</summary>
			public static readonly string MessagegroupEntity = "MessagegroupEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessagegroupDeliverypointEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MessagegroupDeliverypointEntityBase() :base("MessagegroupDeliverypointEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		protected MessagegroupDeliverypointEntityBase(System.Int32 messagegroupDeliverypointId):base("MessagegroupDeliverypointEntity")
		{
			InitClassFetch(messagegroupDeliverypointId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MessagegroupDeliverypointEntityBase(System.Int32 messagegroupDeliverypointId, IPrefetchPath prefetchPathToUse): base("MessagegroupDeliverypointEntity")
		{
			InitClassFetch(messagegroupDeliverypointId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this MessagegroupDeliverypointEntity</param>
		protected MessagegroupDeliverypointEntityBase(System.Int32 messagegroupDeliverypointId, IValidator validator):base("MessagegroupDeliverypointEntity")
		{
			InitClassFetch(messagegroupDeliverypointId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessagegroupDeliverypointEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_messagegroupEntity = (MessagegroupEntity)info.GetValue("_messagegroupEntity", typeof(MessagegroupEntity));
			if(_messagegroupEntity!=null)
			{
				_messagegroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messagegroupEntityReturnsNewIfNotFound = info.GetBoolean("_messagegroupEntityReturnsNewIfNotFound");
			_alwaysFetchMessagegroupEntity = info.GetBoolean("_alwaysFetchMessagegroupEntity");
			_alreadyFetchedMessagegroupEntity = info.GetBoolean("_alreadyFetchedMessagegroupEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MessagegroupDeliverypointFieldIndex)fieldIndex)
			{
				case MessagegroupDeliverypointFieldIndex.MessagegroupId:
					DesetupSyncMessagegroupEntity(true, false);
					_alreadyFetchedMessagegroupEntity = false;
					break;
				case MessagegroupDeliverypointFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedMessagegroupEntity = (_messagegroupEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "MessagegroupEntity":
					toReturn.Add(Relations.MessagegroupEntityUsingMessagegroupId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_messagegroupEntity", (!this.MarkedForDeletion?_messagegroupEntity:null));
			info.AddValue("_messagegroupEntityReturnsNewIfNotFound", _messagegroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessagegroupEntity", _alwaysFetchMessagegroupEntity);
			info.AddValue("_alreadyFetchedMessagegroupEntity", _alreadyFetchedMessagegroupEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "MessagegroupEntity":
					_alreadyFetchedMessagegroupEntity = true;
					this.MessagegroupEntity = (MessagegroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "MessagegroupEntity":
					SetupSyncMessagegroupEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "MessagegroupEntity":
					DesetupSyncMessagegroupEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_messagegroupEntity!=null)
			{
				toReturn.Add(_messagegroupEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="messagegroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMessagegroupIdDeliverypointId(System.Int32 messagegroupId, System.Int32 deliverypointId)
		{
			return FetchUsingUCMessagegroupIdDeliverypointId( messagegroupId,  deliverypointId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="messagegroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMessagegroupIdDeliverypointId(System.Int32 messagegroupId, System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCMessagegroupIdDeliverypointId( messagegroupId,  deliverypointId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="messagegroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMessagegroupIdDeliverypointId(System.Int32 messagegroupId, System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCMessagegroupIdDeliverypointId( messagegroupId,  deliverypointId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="messagegroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMessagegroupIdDeliverypointId(System.Int32 messagegroupId, System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((MessagegroupDeliverypointDAO)CreateDAOInstance()).FetchMessagegroupDeliverypointUsingUCMessagegroupIdDeliverypointId(this, this.Transaction, messagegroupId, deliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupDeliverypointId)
		{
			return FetchUsingPK(messagegroupDeliverypointId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupDeliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messagegroupDeliverypointId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messagegroupDeliverypointId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messagegroupDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messagegroupDeliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessagegroupDeliverypointId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessagegroupDeliverypointRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId);
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public MessagegroupEntity GetSingleMessagegroupEntity()
		{
			return GetSingleMessagegroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MessagegroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessagegroupEntity' which is related to this entity.</returns>
		public virtual MessagegroupEntity GetSingleMessagegroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessagegroupEntity || forceFetch || _alwaysFetchMessagegroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessagegroupEntityUsingMessagegroupId);
				MessagegroupEntity newEntity = new MessagegroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessagegroupId);
				}
				if(fetchResult)
				{
					newEntity = (MessagegroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messagegroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MessagegroupEntity = newEntity;
				_alreadyFetchedMessagegroupEntity = fetchResult;
			}
			return _messagegroupEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("MessagegroupEntity", _messagegroupEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="validator">The validator object for this MessagegroupDeliverypointEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 messagegroupDeliverypointId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messagegroupDeliverypointId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_deliverypointEntityReturnsNewIfNotFound = true;
			_messagegroupEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagegroupDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagegroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticMessagegroupDeliverypointRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "MessagegroupDeliverypointCollection", resetFKFields, new int[] { (int)MessagegroupDeliverypointFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticMessagegroupDeliverypointRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _messagegroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessagegroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticMessagegroupDeliverypointRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, signalRelatedEntity, "MessagegroupDeliverypointCollection", resetFKFields, new int[] { (int)MessagegroupDeliverypointFieldIndex.MessagegroupId } );		
			_messagegroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _messagegroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessagegroupEntity(IEntityCore relatedEntity)
		{
			if(_messagegroupEntity!=relatedEntity)
			{		
				DesetupSyncMessagegroupEntity(true, true);
				_messagegroupEntity = (MessagegroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _messagegroupEntity, new PropertyChangedEventHandler( OnMessagegroupEntityPropertyChanged ), "MessagegroupEntity", Obymobi.Data.RelationClasses.StaticMessagegroupDeliverypointRelations.MessagegroupEntityUsingMessagegroupIdStatic, true, ref _alreadyFetchedMessagegroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessagegroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messagegroupDeliverypointId">PK value for MessagegroupDeliverypoint which data should be fetched into this MessagegroupDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 messagegroupDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessagegroupDeliverypointFieldIndex.MessagegroupDeliverypointId].ForcedCurrentValueWrite(messagegroupDeliverypointId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessagegroupDeliverypointDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessagegroupDeliverypointEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessagegroupDeliverypointRelations Relations
		{
			get	{ return new MessagegroupDeliverypointRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.MessagegroupDeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Messagegroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessagegroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessagegroupCollection(), (IEntityRelation)GetRelationsForField("MessagegroupEntity")[0], (int)Obymobi.Data.EntityType.MessagegroupDeliverypointEntity, (int)Obymobi.Data.EntityType.MessagegroupEntity, 0, null, null, null, "MessagegroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MessagegroupDeliverypointId property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."MessagegroupDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MessagegroupDeliverypointId
		{
			get { return (System.Int32)GetValue((int)MessagegroupDeliverypointFieldIndex.MessagegroupDeliverypointId, true); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.MessagegroupDeliverypointId, value, true); }
		}

		/// <summary> The MessagegroupId property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."MessagegroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagegroupId
		{
			get { return (System.Int32)GetValue((int)MessagegroupDeliverypointFieldIndex.MessagegroupId, true); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.MessagegroupId, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)MessagegroupDeliverypointFieldIndex.DeliverypointId, true); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessagegroupDeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessagegroupDeliverypointFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessagegroupDeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessagegroupDeliverypointFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity MessagegroupDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessagegroupDeliverypoint"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)MessagegroupDeliverypointFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)MessagegroupDeliverypointFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessagegroupDeliverypointCollection", "DeliverypointEntity", _deliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessagegroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessagegroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MessagegroupEntity MessagegroupEntity
		{
			get	{ return GetSingleMessagegroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessagegroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessagegroupDeliverypointCollection", "MessagegroupEntity", _messagegroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MessagegroupEntity. When set to true, MessagegroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessagegroupEntity is accessed. You can always execute a forced fetch by calling GetSingleMessagegroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessagegroupEntity
		{
			get	{ return _alwaysFetchMessagegroupEntity; }
			set	{ _alwaysFetchMessagegroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessagegroupEntity already has been fetched. Setting this property to false when MessagegroupEntity has been fetched
		/// will set MessagegroupEntity to null as well. Setting this property to true while MessagegroupEntity hasn't been fetched disables lazy loading for MessagegroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessagegroupEntity
		{
			get { return _alreadyFetchedMessagegroupEntity;}
			set 
			{
				if(_alreadyFetchedMessagegroupEntity && !value)
				{
					this.MessagegroupEntity = null;
				}
				_alreadyFetchedMessagegroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MessagegroupEntity is not found
		/// in the database. When set to true, MessagegroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MessagegroupEntityReturnsNewIfNotFound
		{
			get	{ return _messagegroupEntityReturnsNewIfNotFound; }
			set { _messagegroupEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MessagegroupDeliverypointEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
