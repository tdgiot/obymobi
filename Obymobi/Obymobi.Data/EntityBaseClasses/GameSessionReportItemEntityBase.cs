﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'GameSessionReportItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GameSessionReportItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GameSessionReportItemEntity"; }
		}
	
		#region Class Member Declarations
		private GameSessionReportEntity _gameSessionReportEntity;
		private bool	_alwaysFetchGameSessionReportEntity, _alreadyFetchedGameSessionReportEntity, _gameSessionReportEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name GameSessionReportEntity</summary>
			public static readonly string GameSessionReportEntity = "GameSessionReportEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GameSessionReportItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GameSessionReportItemEntityBase() :base("GameSessionReportItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		protected GameSessionReportItemEntityBase(System.Int32 gameSessionReportItemId):base("GameSessionReportItemEntity")
		{
			InitClassFetch(gameSessionReportItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GameSessionReportItemEntityBase(System.Int32 gameSessionReportItemId, IPrefetchPath prefetchPathToUse): base("GameSessionReportItemEntity")
		{
			InitClassFetch(gameSessionReportItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="validator">The custom validator object for this GameSessionReportItemEntity</param>
		protected GameSessionReportItemEntityBase(System.Int32 gameSessionReportItemId, IValidator validator):base("GameSessionReportItemEntity")
		{
			InitClassFetch(gameSessionReportItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GameSessionReportItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_gameSessionReportEntity = (GameSessionReportEntity)info.GetValue("_gameSessionReportEntity", typeof(GameSessionReportEntity));
			if(_gameSessionReportEntity!=null)
			{
				_gameSessionReportEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_gameSessionReportEntityReturnsNewIfNotFound = info.GetBoolean("_gameSessionReportEntityReturnsNewIfNotFound");
			_alwaysFetchGameSessionReportEntity = info.GetBoolean("_alwaysFetchGameSessionReportEntity");
			_alreadyFetchedGameSessionReportEntity = info.GetBoolean("_alreadyFetchedGameSessionReportEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GameSessionReportItemFieldIndex)fieldIndex)
			{
				case GameSessionReportItemFieldIndex.GameSessionReportId:
					DesetupSyncGameSessionReportEntity(true, false);
					_alreadyFetchedGameSessionReportEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedGameSessionReportEntity = (_gameSessionReportEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "GameSessionReportEntity":
					toReturn.Add(Relations.GameSessionReportEntityUsingGameSessionReportId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_gameSessionReportEntity", (!this.MarkedForDeletion?_gameSessionReportEntity:null));
			info.AddValue("_gameSessionReportEntityReturnsNewIfNotFound", _gameSessionReportEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGameSessionReportEntity", _alwaysFetchGameSessionReportEntity);
			info.AddValue("_alreadyFetchedGameSessionReportEntity", _alreadyFetchedGameSessionReportEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "GameSessionReportEntity":
					_alreadyFetchedGameSessionReportEntity = true;
					this.GameSessionReportEntity = (GameSessionReportEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "GameSessionReportEntity":
					SetupSyncGameSessionReportEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "GameSessionReportEntity":
					DesetupSyncGameSessionReportEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_gameSessionReportEntity!=null)
			{
				toReturn.Add(_gameSessionReportEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 gameSessionReportItemId)
		{
			return FetchUsingPK(gameSessionReportItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 gameSessionReportItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(gameSessionReportItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 gameSessionReportItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(gameSessionReportItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 gameSessionReportItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(gameSessionReportItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GameSessionReportItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GameSessionReportItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'GameSessionReportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GameSessionReportEntity' which is related to this entity.</returns>
		public GameSessionReportEntity GetSingleGameSessionReportEntity()
		{
			return GetSingleGameSessionReportEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GameSessionReportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GameSessionReportEntity' which is related to this entity.</returns>
		public virtual GameSessionReportEntity GetSingleGameSessionReportEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGameSessionReportEntity || forceFetch || _alwaysFetchGameSessionReportEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GameSessionReportEntityUsingGameSessionReportId);
				GameSessionReportEntity newEntity = new GameSessionReportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GameSessionReportId);
				}
				if(fetchResult)
				{
					newEntity = (GameSessionReportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_gameSessionReportEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GameSessionReportEntity = newEntity;
				_alreadyFetchedGameSessionReportEntity = fetchResult;
			}
			return _gameSessionReportEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("GameSessionReportEntity", _gameSessionReportEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="validator">The validator object for this GameSessionReportItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 gameSessionReportItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(gameSessionReportItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_gameSessionReportEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameSessionReportItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameSessionReportId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameSessionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GameName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Length", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _gameSessionReportEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGameSessionReportEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _gameSessionReportEntity, new PropertyChangedEventHandler( OnGameSessionReportEntityPropertyChanged ), "GameSessionReportEntity", Obymobi.Data.RelationClasses.StaticGameSessionReportItemRelations.GameSessionReportEntityUsingGameSessionReportIdStatic, true, signalRelatedEntity, "GameSessionReportItemCollection", resetFKFields, new int[] { (int)GameSessionReportItemFieldIndex.GameSessionReportId } );		
			_gameSessionReportEntity = null;
		}
		
		/// <summary> setups the sync logic for member _gameSessionReportEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGameSessionReportEntity(IEntityCore relatedEntity)
		{
			if(_gameSessionReportEntity!=relatedEntity)
			{		
				DesetupSyncGameSessionReportEntity(true, true);
				_gameSessionReportEntity = (GameSessionReportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _gameSessionReportEntity, new PropertyChangedEventHandler( OnGameSessionReportEntityPropertyChanged ), "GameSessionReportEntity", Obymobi.Data.RelationClasses.StaticGameSessionReportItemRelations.GameSessionReportEntityUsingGameSessionReportIdStatic, true, ref _alreadyFetchedGameSessionReportEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGameSessionReportEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="gameSessionReportItemId">PK value for GameSessionReportItem which data should be fetched into this GameSessionReportItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 gameSessionReportItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GameSessionReportItemFieldIndex.GameSessionReportItemId].ForcedCurrentValueWrite(gameSessionReportItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGameSessionReportItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GameSessionReportItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GameSessionReportItemRelations Relations
		{
			get	{ return new GameSessionReportItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GameSessionReport'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGameSessionReportEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GameSessionReportCollection(), (IEntityRelation)GetRelationsForField("GameSessionReportEntity")[0], (int)Obymobi.Data.EntityType.GameSessionReportItemEntity, (int)Obymobi.Data.EntityType.GameSessionReportEntity, 0, null, null, null, "GameSessionReportEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GameSessionReportItemId property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."GameSessionReportItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GameSessionReportItemId
		{
			get { return (System.Int32)GetValue((int)GameSessionReportItemFieldIndex.GameSessionReportItemId, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.GameSessionReportItemId, value, true); }
		}

		/// <summary> The GameSessionReportId property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."GameSessionReportId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GameSessionReportId
		{
			get { return (System.Int32)GetValue((int)GameSessionReportItemFieldIndex.GameSessionReportId, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.GameSessionReportId, value, true); }
		}

		/// <summary> The GameSessionId property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."Length"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GameSessionId
		{
			get { return (System.Int32)GetValue((int)GameSessionReportItemFieldIndex.GameSessionId, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.GameSessionId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)GameSessionReportItemFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GameSessionReportItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)GameSessionReportItemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The DeliverypointName property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."DeliverypointName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DeliverypointName
		{
			get { return (System.String)GetValue((int)GameSessionReportItemFieldIndex.DeliverypointName, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.DeliverypointName, value, true); }
		}

		/// <summary> The GameName property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."GameName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String GameName
		{
			get { return (System.String)GetValue((int)GameSessionReportItemFieldIndex.GameName, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.GameName, value, true); }
		}

		/// <summary> The StartUTC property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."StartUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime StartUTC
		{
			get { return (System.DateTime)GetValue((int)GameSessionReportItemFieldIndex.StartUTC, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.StartUTC, value, true); }
		}

		/// <summary> The EndUTC property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."EndUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EndUTC
		{
			get { return (System.DateTime)GetValue((int)GameSessionReportItemFieldIndex.EndUTC, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.EndUTC, value, true); }
		}

		/// <summary> The Length property of the Entity GameSessionReportItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GameSessionReportItem"."Length"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Length
		{
			get { return (System.Int32)GetValue((int)GameSessionReportItemFieldIndex.Length, true); }
			set	{ SetValue((int)GameSessionReportItemFieldIndex.Length, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'GameSessionReportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGameSessionReportEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GameSessionReportEntity GameSessionReportEntity
		{
			get	{ return GetSingleGameSessionReportEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGameSessionReportEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GameSessionReportItemCollection", "GameSessionReportEntity", _gameSessionReportEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GameSessionReportEntity. When set to true, GameSessionReportEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GameSessionReportEntity is accessed. You can always execute a forced fetch by calling GetSingleGameSessionReportEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGameSessionReportEntity
		{
			get	{ return _alwaysFetchGameSessionReportEntity; }
			set	{ _alwaysFetchGameSessionReportEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GameSessionReportEntity already has been fetched. Setting this property to false when GameSessionReportEntity has been fetched
		/// will set GameSessionReportEntity to null as well. Setting this property to true while GameSessionReportEntity hasn't been fetched disables lazy loading for GameSessionReportEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGameSessionReportEntity
		{
			get { return _alreadyFetchedGameSessionReportEntity;}
			set 
			{
				if(_alreadyFetchedGameSessionReportEntity && !value)
				{
					this.GameSessionReportEntity = null;
				}
				_alreadyFetchedGameSessionReportEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GameSessionReportEntity is not found
		/// in the database. When set to true, GameSessionReportEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GameSessionReportEntityReturnsNewIfNotFound
		{
			get	{ return _gameSessionReportEntityReturnsNewIfNotFound; }
			set { _gameSessionReportEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GameSessionReportItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
