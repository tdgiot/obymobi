﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'EntertainmentFile'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntertainmentFileEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "EntertainmentFileEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection	_cloudProcessingTaskCollection;
		private bool	_alwaysFetchCloudProcessingTaskCollection, _alreadyFetchedCloudProcessingTaskCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection	_entertainmentCollection;
		private bool	_alwaysFetchEntertainmentCollection, _alreadyFetchedEntertainmentCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CloudProcessingTaskCollection</summary>
			public static readonly string CloudProcessingTaskCollection = "CloudProcessingTaskCollection";
			/// <summary>Member name EntertainmentCollection</summary>
			public static readonly string EntertainmentCollection = "EntertainmentCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntertainmentFileEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntertainmentFileEntityBase() :base("EntertainmentFileEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		protected EntertainmentFileEntityBase(System.Int32 entertainmentFileId):base("EntertainmentFileEntity")
		{
			InitClassFetch(entertainmentFileId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntertainmentFileEntityBase(System.Int32 entertainmentFileId, IPrefetchPath prefetchPathToUse): base("EntertainmentFileEntity")
		{
			InitClassFetch(entertainmentFileId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="validator">The custom validator object for this EntertainmentFileEntity</param>
		protected EntertainmentFileEntityBase(System.Int32 entertainmentFileId, IValidator validator):base("EntertainmentFileEntity")
		{
			InitClassFetch(entertainmentFileId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentFileEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cloudProcessingTaskCollection = (Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection)info.GetValue("_cloudProcessingTaskCollection", typeof(Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection));
			_alwaysFetchCloudProcessingTaskCollection = info.GetBoolean("_alwaysFetchCloudProcessingTaskCollection");
			_alreadyFetchedCloudProcessingTaskCollection = info.GetBoolean("_alreadyFetchedCloudProcessingTaskCollection");

			_entertainmentCollection = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollection", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollection = info.GetBoolean("_alwaysFetchEntertainmentCollection");
			_alreadyFetchedEntertainmentCollection = info.GetBoolean("_alreadyFetchedEntertainmentCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCloudProcessingTaskCollection = (_cloudProcessingTaskCollection.Count > 0);
			_alreadyFetchedEntertainmentCollection = (_entertainmentCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CloudProcessingTaskCollection":
					toReturn.Add(Relations.CloudProcessingTaskEntityUsingEntertainmentFileId);
					break;
				case "EntertainmentCollection":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentFileId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cloudProcessingTaskCollection", (!this.MarkedForDeletion?_cloudProcessingTaskCollection:null));
			info.AddValue("_alwaysFetchCloudProcessingTaskCollection", _alwaysFetchCloudProcessingTaskCollection);
			info.AddValue("_alreadyFetchedCloudProcessingTaskCollection", _alreadyFetchedCloudProcessingTaskCollection);
			info.AddValue("_entertainmentCollection", (!this.MarkedForDeletion?_entertainmentCollection:null));
			info.AddValue("_alwaysFetchEntertainmentCollection", _alwaysFetchEntertainmentCollection);
			info.AddValue("_alreadyFetchedEntertainmentCollection", _alreadyFetchedEntertainmentCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CloudProcessingTaskCollection":
					_alreadyFetchedCloudProcessingTaskCollection = true;
					if(entity!=null)
					{
						this.CloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)entity);
					}
					break;
				case "EntertainmentCollection":
					_alreadyFetchedEntertainmentCollection = true;
					if(entity!=null)
					{
						this.EntertainmentCollection.Add((EntertainmentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CloudProcessingTaskCollection":
					_cloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)relatedEntity);
					break;
				case "EntertainmentCollection":
					_entertainmentCollection.Add((EntertainmentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CloudProcessingTaskCollection":
					this.PerformRelatedEntityRemoval(_cloudProcessingTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntertainmentCollection":
					this.PerformRelatedEntityRemoval(_entertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cloudProcessingTaskCollection);
			toReturn.Add(_entertainmentCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentFileId)
		{
			return FetchUsingPK(entertainmentFileId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentFileId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entertainmentFileId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentFileId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entertainmentFileId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entertainmentFileId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntertainmentFileId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntertainmentFileRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCloudProcessingTaskCollection || forceFetch || _alwaysFetchCloudProcessingTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cloudProcessingTaskCollection);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_cloudProcessingTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_cloudProcessingTaskCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCloudProcessingTaskCollection = true;
			}
			return _cloudProcessingTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CloudProcessingTaskCollection'. These settings will be taken into account
		/// when the property CloudProcessingTaskCollection is requested or GetMultiCloudProcessingTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCloudProcessingTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cloudProcessingTaskCollection.SortClauses=sortClauses;
			_cloudProcessingTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntertainmentCollection || forceFetch || _alwaysFetchEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollection);
				_entertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_entertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollection = true;
			}
			return _entertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollection'. These settings will be taken into account
		/// when the property EntertainmentCollection is requested or GetMultiEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollection.SortClauses=sortClauses;
			_entertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CloudProcessingTaskCollection", _cloudProcessingTaskCollection);
			toReturn.Add("EntertainmentCollection", _entertainmentCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="validator">The validator object for this EntertainmentFileEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entertainmentFileId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entertainmentFileId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cloudProcessingTaskCollection = new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection();
			_cloudProcessingTaskCollection.SetContainingEntityInfo(this, "EntertainmentFileEntity");

			_entertainmentCollection = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollection.SetContainingEntityInfo(this, "EntertainmentFileEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Blob", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Filename", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entertainmentFileId">PK value for EntertainmentFile which data should be fetched into this EntertainmentFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entertainmentFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntertainmentFileFieldIndex.EntertainmentFileId].ForcedCurrentValueWrite(entertainmentFileId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntertainmentFileDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntertainmentFileEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntertainmentFileRelations Relations
		{
			get	{ return new EntertainmentFileRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloudProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloudProcessingTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("CloudProcessingTaskCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentFileEntity, (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, 0, null, null, null, "CloudProcessingTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentFileEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntertainmentFileId property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."EntertainmentFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntertainmentFileId
		{
			get { return (System.Int32)GetValue((int)EntertainmentFileFieldIndex.EntertainmentFileId, true); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.EntertainmentFileId, value, true); }
		}

		/// <summary> The Blob property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."Blob"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarBinary, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] Blob
		{
			get { return (System.Byte[])GetValue((int)EntertainmentFileFieldIndex.Blob, true); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.Blob, value, true); }
		}

		/// <summary> The Filename property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."Filename"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Filename
		{
			get { return (System.String)GetValue((int)EntertainmentFileFieldIndex.Filename, true); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.Filename, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)EntertainmentFileFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)EntertainmentFileFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentFileFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity EntertainmentFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentFile"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentFileFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntertainmentFileFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCloudProcessingTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection CloudProcessingTaskCollection
		{
			get	{ return GetMultiCloudProcessingTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CloudProcessingTaskCollection. When set to true, CloudProcessingTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloudProcessingTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCloudProcessingTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloudProcessingTaskCollection
		{
			get	{ return _alwaysFetchCloudProcessingTaskCollection; }
			set	{ _alwaysFetchCloudProcessingTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloudProcessingTaskCollection already has been fetched. Setting this property to false when CloudProcessingTaskCollection has been fetched
		/// will clear the CloudProcessingTaskCollection collection well. Setting this property to true while CloudProcessingTaskCollection hasn't been fetched disables lazy loading for CloudProcessingTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloudProcessingTaskCollection
		{
			get { return _alreadyFetchedCloudProcessingTaskCollection;}
			set 
			{
				if(_alreadyFetchedCloudProcessingTaskCollection && !value && (_cloudProcessingTaskCollection != null))
				{
					_cloudProcessingTaskCollection.Clear();
				}
				_alreadyFetchedCloudProcessingTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollection
		{
			get	{ return GetMultiEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollection. When set to true, EntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollection
		{
			get	{ return _alwaysFetchEntertainmentCollection; }
			set	{ _alwaysFetchEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollection already has been fetched. Setting this property to false when EntertainmentCollection has been fetched
		/// will clear the EntertainmentCollection collection well. Setting this property to true while EntertainmentCollection hasn't been fetched disables lazy loading for EntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollection
		{
			get { return _alreadyFetchedEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollection && !value && (_entertainmentCollection != null))
				{
					_entertainmentCollection.Clear();
				}
				_alreadyFetchedEntertainmentCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.EntertainmentFileEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
