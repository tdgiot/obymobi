﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SiteTemplate'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SiteTemplateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SiteTemplateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateCollection	_pageTemplateCollection;
		private bool	_alwaysFetchPageTemplateCollection, _alreadyFetchedPageTemplateCollection;
		private Obymobi.Data.CollectionClasses.SiteCollection	_siteCollection;
		private bool	_alwaysFetchSiteCollection, _alreadyFetchedSiteCollection;
		private Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection	_siteTemplateCultureCollection;
		private bool	_alwaysFetchSiteTemplateCultureCollection, _alreadyFetchedSiteTemplateCultureCollection;
		private Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection	_siteTemplateLanguageCollection;
		private bool	_alwaysFetchSiteTemplateLanguageCollection, _alreadyFetchedSiteTemplateLanguageCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name PageTemplateCollection</summary>
			public static readonly string PageTemplateCollection = "PageTemplateCollection";
			/// <summary>Member name SiteCollection</summary>
			public static readonly string SiteCollection = "SiteCollection";
			/// <summary>Member name SiteTemplateCultureCollection</summary>
			public static readonly string SiteTemplateCultureCollection = "SiteTemplateCultureCollection";
			/// <summary>Member name SiteTemplateLanguageCollection</summary>
			public static readonly string SiteTemplateLanguageCollection = "SiteTemplateLanguageCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SiteTemplateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SiteTemplateEntityBase() :base("SiteTemplateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		protected SiteTemplateEntityBase(System.Int32 siteTemplateId):base("SiteTemplateEntity")
		{
			InitClassFetch(siteTemplateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SiteTemplateEntityBase(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse): base("SiteTemplateEntity")
		{
			InitClassFetch(siteTemplateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="validator">The custom validator object for this SiteTemplateEntity</param>
		protected SiteTemplateEntityBase(System.Int32 siteTemplateId, IValidator validator):base("SiteTemplateEntity")
		{
			InitClassFetch(siteTemplateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteTemplateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_pageTemplateCollection = (Obymobi.Data.CollectionClasses.PageTemplateCollection)info.GetValue("_pageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateCollection));
			_alwaysFetchPageTemplateCollection = info.GetBoolean("_alwaysFetchPageTemplateCollection");
			_alreadyFetchedPageTemplateCollection = info.GetBoolean("_alreadyFetchedPageTemplateCollection");

			_siteCollection = (Obymobi.Data.CollectionClasses.SiteCollection)info.GetValue("_siteCollection", typeof(Obymobi.Data.CollectionClasses.SiteCollection));
			_alwaysFetchSiteCollection = info.GetBoolean("_alwaysFetchSiteCollection");
			_alreadyFetchedSiteCollection = info.GetBoolean("_alreadyFetchedSiteCollection");

			_siteTemplateCultureCollection = (Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection)info.GetValue("_siteTemplateCultureCollection", typeof(Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection));
			_alwaysFetchSiteTemplateCultureCollection = info.GetBoolean("_alwaysFetchSiteTemplateCultureCollection");
			_alreadyFetchedSiteTemplateCultureCollection = info.GetBoolean("_alreadyFetchedSiteTemplateCultureCollection");

			_siteTemplateLanguageCollection = (Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection)info.GetValue("_siteTemplateLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection));
			_alwaysFetchSiteTemplateLanguageCollection = info.GetBoolean("_alwaysFetchSiteTemplateLanguageCollection");
			_alreadyFetchedSiteTemplateLanguageCollection = info.GetBoolean("_alreadyFetchedSiteTemplateLanguageCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedPageTemplateCollection = (_pageTemplateCollection.Count > 0);
			_alreadyFetchedSiteCollection = (_siteCollection.Count > 0);
			_alreadyFetchedSiteTemplateCultureCollection = (_siteTemplateCultureCollection.Count > 0);
			_alreadyFetchedSiteTemplateLanguageCollection = (_siteTemplateLanguageCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingSiteTemplateId);
					break;
				case "PageTemplateCollection":
					toReturn.Add(Relations.PageTemplateEntityUsingSiteTemplateId);
					break;
				case "SiteCollection":
					toReturn.Add(Relations.SiteEntityUsingSiteTemplateId);
					break;
				case "SiteTemplateCultureCollection":
					toReturn.Add(Relations.SiteTemplateCultureEntityUsingSiteTemplateId);
					break;
				case "SiteTemplateLanguageCollection":
					toReturn.Add(Relations.SiteTemplateLanguageEntityUsingSiteTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_pageTemplateCollection", (!this.MarkedForDeletion?_pageTemplateCollection:null));
			info.AddValue("_alwaysFetchPageTemplateCollection", _alwaysFetchPageTemplateCollection);
			info.AddValue("_alreadyFetchedPageTemplateCollection", _alreadyFetchedPageTemplateCollection);
			info.AddValue("_siteCollection", (!this.MarkedForDeletion?_siteCollection:null));
			info.AddValue("_alwaysFetchSiteCollection", _alwaysFetchSiteCollection);
			info.AddValue("_alreadyFetchedSiteCollection", _alreadyFetchedSiteCollection);
			info.AddValue("_siteTemplateCultureCollection", (!this.MarkedForDeletion?_siteTemplateCultureCollection:null));
			info.AddValue("_alwaysFetchSiteTemplateCultureCollection", _alwaysFetchSiteTemplateCultureCollection);
			info.AddValue("_alreadyFetchedSiteTemplateCultureCollection", _alreadyFetchedSiteTemplateCultureCollection);
			info.AddValue("_siteTemplateLanguageCollection", (!this.MarkedForDeletion?_siteTemplateLanguageCollection:null));
			info.AddValue("_alwaysFetchSiteTemplateLanguageCollection", _alwaysFetchSiteTemplateLanguageCollection);
			info.AddValue("_alreadyFetchedSiteTemplateLanguageCollection", _alreadyFetchedSiteTemplateLanguageCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "PageTemplateCollection":
					_alreadyFetchedPageTemplateCollection = true;
					if(entity!=null)
					{
						this.PageTemplateCollection.Add((PageTemplateEntity)entity);
					}
					break;
				case "SiteCollection":
					_alreadyFetchedSiteCollection = true;
					if(entity!=null)
					{
						this.SiteCollection.Add((SiteEntity)entity);
					}
					break;
				case "SiteTemplateCultureCollection":
					_alreadyFetchedSiteTemplateCultureCollection = true;
					if(entity!=null)
					{
						this.SiteTemplateCultureCollection.Add((SiteTemplateCultureEntity)entity);
					}
					break;
				case "SiteTemplateLanguageCollection":
					_alreadyFetchedSiteTemplateLanguageCollection = true;
					if(entity!=null)
					{
						this.SiteTemplateLanguageCollection.Add((SiteTemplateLanguageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "PageTemplateCollection":
					_pageTemplateCollection.Add((PageTemplateEntity)relatedEntity);
					break;
				case "SiteCollection":
					_siteCollection.Add((SiteEntity)relatedEntity);
					break;
				case "SiteTemplateCultureCollection":
					_siteTemplateCultureCollection.Add((SiteTemplateCultureEntity)relatedEntity);
					break;
				case "SiteTemplateLanguageCollection":
					_siteTemplateLanguageCollection.Add((SiteTemplateLanguageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteCollection":
					this.PerformRelatedEntityRemoval(_siteCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteTemplateCultureCollection":
					this.PerformRelatedEntityRemoval(_siteTemplateCultureCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteTemplateLanguageCollection":
					this.PerformRelatedEntityRemoval(_siteTemplateLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_pageTemplateCollection);
			toReturn.Add(_siteCollection);
			toReturn.Add(_siteTemplateCultureCollection);
			toReturn.Add(_siteTemplateLanguageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateId)
		{
			return FetchUsingPK(siteTemplateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(siteTemplateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(siteTemplateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(siteTemplateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SiteTemplateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SiteTemplateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch)
		{
			return GetMultiPageTemplateCollection(forceFetch, _pageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateCollection(forceFetch, _pageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateCollection GetMultiPageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateCollection || forceFetch || _alwaysFetchPageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateCollection);
				_pageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateCollection.GetMultiManyToOne(null, this, filter);
				_pageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateCollection = true;
			}
			return _pageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateCollection'. These settings will be taken into account
		/// when the property PageTemplateCollection is requested or GetMultiPageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateCollection.SortClauses=sortClauses;
			_pageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteCollection GetMultiSiteCollection(bool forceFetch)
		{
			return GetMultiSiteCollection(forceFetch, _siteCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteCollection GetMultiSiteCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteCollection(forceFetch, _siteCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteCollection GetMultiSiteCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteCollection GetMultiSiteCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteCollection || forceFetch || _alwaysFetchSiteCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteCollection);
				_siteCollection.SuppressClearInGetMulti=!forceFetch;
				_siteCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteCollection.GetMultiManyToOne(null, this, filter);
				_siteCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteCollection = true;
			}
			return _siteCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteCollection'. These settings will be taken into account
		/// when the property SiteCollection is requested or GetMultiSiteCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteCollection.SortClauses=sortClauses;
			_siteCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection GetMultiSiteTemplateCultureCollection(bool forceFetch)
		{
			return GetMultiSiteTemplateCultureCollection(forceFetch, _siteTemplateCultureCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection GetMultiSiteTemplateCultureCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteTemplateCultureCollection(forceFetch, _siteTemplateCultureCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection GetMultiSiteTemplateCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteTemplateCultureCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection GetMultiSiteTemplateCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteTemplateCultureCollection || forceFetch || _alwaysFetchSiteTemplateCultureCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteTemplateCultureCollection);
				_siteTemplateCultureCollection.SuppressClearInGetMulti=!forceFetch;
				_siteTemplateCultureCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteTemplateCultureCollection.GetMultiManyToOne(this, filter);
				_siteTemplateCultureCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteTemplateCultureCollection = true;
			}
			return _siteTemplateCultureCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteTemplateCultureCollection'. These settings will be taken into account
		/// when the property SiteTemplateCultureCollection is requested or GetMultiSiteTemplateCultureCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteTemplateCultureCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteTemplateCultureCollection.SortClauses=sortClauses;
			_siteTemplateCultureCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, _siteTemplateLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, _siteTemplateLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteTemplateLanguageCollection || forceFetch || _alwaysFetchSiteTemplateLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteTemplateLanguageCollection);
				_siteTemplateLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_siteTemplateLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteTemplateLanguageCollection.GetMultiManyToOne(null, this, filter);
				_siteTemplateLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteTemplateLanguageCollection = true;
			}
			return _siteTemplateLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteTemplateLanguageCollection'. These settings will be taken into account
		/// when the property SiteTemplateLanguageCollection is requested or GetMultiSiteTemplateLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteTemplateLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteTemplateLanguageCollection.SortClauses=sortClauses;
			_siteTemplateLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("PageTemplateCollection", _pageTemplateCollection);
			toReturn.Add("SiteCollection", _siteCollection);
			toReturn.Add("SiteTemplateCultureCollection", _siteTemplateCultureCollection);
			toReturn.Add("SiteTemplateLanguageCollection", _siteTemplateLanguageCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="validator">The validator object for this SiteTemplateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 siteTemplateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(siteTemplateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "SiteTemplateEntity");

			_pageTemplateCollection = new Obymobi.Data.CollectionClasses.PageTemplateCollection();
			_pageTemplateCollection.SetContainingEntityInfo(this, "SiteTemplateEntity");

			_siteCollection = new Obymobi.Data.CollectionClasses.SiteCollection();
			_siteCollection.SetContainingEntityInfo(this, "SiteTemplateEntity");

			_siteTemplateCultureCollection = new Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection();
			_siteTemplateCultureCollection.SetContainingEntityInfo(this, "SiteTemplateEntity");

			_siteTemplateLanguageCollection = new Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection();
			_siteTemplateLanguageCollection.SetContainingEntityInfo(this, "SiteTemplateEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SiteTemplateFieldIndex.SiteTemplateId].ForcedCurrentValueWrite(siteTemplateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSiteTemplateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SiteTemplateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SiteTemplateRelations Relations
		{
			get	{ return new SiteTemplateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.SiteTemplateEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateCollection")[0], (int)Obymobi.Data.EntityType.SiteTemplateEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteCollection")[0], (int)Obymobi.Data.EntityType.SiteTemplateEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplateCulture' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateCultureCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateCultureCollection")[0], (int)Obymobi.Data.EntityType.SiteTemplateEntity, (int)Obymobi.Data.EntityType.SiteTemplateCultureEntity, 0, null, null, null, "SiteTemplateCultureCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplateLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateLanguageCollection")[0], (int)Obymobi.Data.EntityType.SiteTemplateEntity, (int)Obymobi.Data.EntityType.SiteTemplateLanguageEntity, 0, null, null, null, "SiteTemplateLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SiteTemplateId property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."SiteTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SiteTemplateId
		{
			get { return (System.Int32)GetValue((int)SiteTemplateFieldIndex.SiteTemplateId, true); }
			set	{ SetValue((int)SiteTemplateFieldIndex.SiteTemplateId, value, true); }
		}

		/// <summary> The Name property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SiteTemplateFieldIndex.Name, true); }
			set	{ SetValue((int)SiteTemplateFieldIndex.Name, value, true); }
		}

		/// <summary> The SiteType property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."SiteType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteType
		{
			get { return (System.Int32)GetValue((int)SiteTemplateFieldIndex.SiteType, true); }
			set	{ SetValue((int)SiteTemplateFieldIndex.SiteType, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SiteTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SiteTemplateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SiteTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SiteTemplateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SiteTemplateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SiteTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SiteTemplate"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SiteTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SiteTemplateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateCollection PageTemplateCollection
		{
			get	{ return GetMultiPageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateCollection. When set to true, PageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateCollection
		{
			get	{ return _alwaysFetchPageTemplateCollection; }
			set	{ _alwaysFetchPageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateCollection already has been fetched. Setting this property to false when PageTemplateCollection has been fetched
		/// will clear the PageTemplateCollection collection well. Setting this property to true while PageTemplateCollection hasn't been fetched disables lazy loading for PageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateCollection
		{
			get { return _alreadyFetchedPageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateCollection && !value && (_pageTemplateCollection != null))
				{
					_pageTemplateCollection.Clear();
				}
				_alreadyFetchedPageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteCollection SiteCollection
		{
			get	{ return GetMultiSiteCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteCollection. When set to true, SiteCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteCollection
		{
			get	{ return _alwaysFetchSiteCollection; }
			set	{ _alwaysFetchSiteCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteCollection already has been fetched. Setting this property to false when SiteCollection has been fetched
		/// will clear the SiteCollection collection well. Setting this property to true while SiteCollection hasn't been fetched disables lazy loading for SiteCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteCollection
		{
			get { return _alreadyFetchedSiteCollection;}
			set 
			{
				if(_alreadyFetchedSiteCollection && !value && (_siteCollection != null))
				{
					_siteCollection.Clear();
				}
				_alreadyFetchedSiteCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteTemplateCultureEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteTemplateCultureCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateCultureCollection SiteTemplateCultureCollection
		{
			get	{ return GetMultiSiteTemplateCultureCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateCultureCollection. When set to true, SiteTemplateCultureCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateCultureCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteTemplateCultureCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateCultureCollection
		{
			get	{ return _alwaysFetchSiteTemplateCultureCollection; }
			set	{ _alwaysFetchSiteTemplateCultureCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateCultureCollection already has been fetched. Setting this property to false when SiteTemplateCultureCollection has been fetched
		/// will clear the SiteTemplateCultureCollection collection well. Setting this property to true while SiteTemplateCultureCollection hasn't been fetched disables lazy loading for SiteTemplateCultureCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateCultureCollection
		{
			get { return _alreadyFetchedSiteTemplateCultureCollection;}
			set 
			{
				if(_alreadyFetchedSiteTemplateCultureCollection && !value && (_siteTemplateCultureCollection != null))
				{
					_siteTemplateCultureCollection.Clear();
				}
				_alreadyFetchedSiteTemplateCultureCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteTemplateLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection SiteTemplateLanguageCollection
		{
			get	{ return GetMultiSiteTemplateLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateLanguageCollection. When set to true, SiteTemplateLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteTemplateLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateLanguageCollection
		{
			get	{ return _alwaysFetchSiteTemplateLanguageCollection; }
			set	{ _alwaysFetchSiteTemplateLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateLanguageCollection already has been fetched. Setting this property to false when SiteTemplateLanguageCollection has been fetched
		/// will clear the SiteTemplateLanguageCollection collection well. Setting this property to true while SiteTemplateLanguageCollection hasn't been fetched disables lazy loading for SiteTemplateLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateLanguageCollection
		{
			get { return _alreadyFetchedSiteTemplateLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSiteTemplateLanguageCollection && !value && (_siteTemplateLanguageCollection != null))
				{
					_siteTemplateLanguageCollection.Clear();
				}
				_alreadyFetchedSiteTemplateLanguageCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SiteTemplateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
