﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'CustomText'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	[DebuggerDisplay("CustomTextId={CustomTextId}, Text={Text}, CultureCode={CultureCode}")]
	public abstract partial class CustomTextEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CustomTextEntity"; }
		}
	
		#region Class Member Declarations
		private ActionButtonEntity _actionButtonEntity;
		private bool	_alwaysFetchActionButtonEntity, _alreadyFetchedActionButtonEntity, _actionButtonEntityReturnsNewIfNotFound;
		private AdvertisementEntity _advertisementEntity;
		private bool	_alwaysFetchAdvertisementEntity, _alreadyFetchedAdvertisementEntity, _advertisementEntityReturnsNewIfNotFound;
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationoptionEntity _alterationoptionEntity;
		private bool	_alwaysFetchAlterationoptionEntity, _alreadyFetchedAlterationoptionEntity, _alterationoptionEntityReturnsNewIfNotFound;
		private AmenityEntity _amenityEntity;
		private bool	_alwaysFetchAmenityEntity, _alreadyFetchedAmenityEntity, _amenityEntityReturnsNewIfNotFound;
		private AnnouncementEntity _announcementEntity;
		private bool	_alwaysFetchAnnouncementEntity, _alreadyFetchedAnnouncementEntity, _announcementEntityReturnsNewIfNotFound;
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;
		private CarouselItemEntity _carouselItemEntity;
		private bool	_alwaysFetchCarouselItemEntity, _alreadyFetchedCarouselItemEntity, _carouselItemEntityReturnsNewIfNotFound;
		private LandingPageEntity _landingPageEntity;
		private bool	_alwaysFetchLandingPageEntity, _alreadyFetchedLandingPageEntity, _landingPageEntityReturnsNewIfNotFound;
		private NavigationMenuItemEntity _navigationMenuItemEntity;
		private bool	_alwaysFetchNavigationMenuItemEntity, _alreadyFetchedNavigationMenuItemEntity, _navigationMenuItemEntityReturnsNewIfNotFound;
		private WidgetEntity _widgetEntity;
		private bool	_alwaysFetchWidgetEntity, _alreadyFetchedWidgetEntity, _widgetEntityReturnsNewIfNotFound;
		private AttachmentEntity _attachmentEntity;
		private bool	_alwaysFetchAttachmentEntity, _alreadyFetchedAttachmentEntity, _attachmentEntityReturnsNewIfNotFound;
		private AvailabilityEntity _availabilityEntity;
		private bool	_alwaysFetchAvailabilityEntity, _alreadyFetchedAvailabilityEntity, _availabilityEntityReturnsNewIfNotFound;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private CheckoutMethodEntity _checkoutMethodEntity;
		private bool	_alwaysFetchCheckoutMethodEntity, _alreadyFetchedCheckoutMethodEntity, _checkoutMethodEntityReturnsNewIfNotFound;
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private EntertainmentcategoryEntity _entertainmentcategoryEntity;
		private bool	_alwaysFetchEntertainmentcategoryEntity, _alreadyFetchedEntertainmentcategoryEntity, _entertainmentcategoryEntityReturnsNewIfNotFound;
		private GenericcategoryEntity _genericcategoryEntity;
		private bool	_alwaysFetchGenericcategoryEntity, _alreadyFetchedGenericcategoryEntity, _genericcategoryEntityReturnsNewIfNotFound;
		private GenericproductEntity _genericproductEntity;
		private bool	_alwaysFetchGenericproductEntity, _alreadyFetchedGenericproductEntity, _genericproductEntityReturnsNewIfNotFound;
		private InfraredCommandEntity _infraredCommandEntity;
		private bool	_alwaysFetchInfraredCommandEntity, _alreadyFetchedInfraredCommandEntity, _infraredCommandEntityReturnsNewIfNotFound;
		private LanguageEntity _languageEntity;
		private bool	_alwaysFetchLanguageEntity, _alreadyFetchedLanguageEntity, _languageEntityReturnsNewIfNotFound;
		private OutletEntity _outletEntity;
		private bool	_alwaysFetchOutletEntity, _alreadyFetchedOutletEntity, _outletEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private PageTemplateEntity _pageTemplateEntity;
		private bool	_alwaysFetchPageTemplateEntity, _alreadyFetchedPageTemplateEntity, _pageTemplateEntityReturnsNewIfNotFound;
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductgroupEntity _productgroupEntity;
		private bool	_alwaysFetchProductgroupEntity, _alreadyFetchedProductgroupEntity, _productgroupEntityReturnsNewIfNotFound;
		private RoomControlAreaEntity _roomControlAreaEntity;
		private bool	_alwaysFetchRoomControlAreaEntity, _alreadyFetchedRoomControlAreaEntity, _roomControlAreaEntityReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity;
		private bool	_alwaysFetchRoomControlComponentEntity, _alreadyFetchedRoomControlComponentEntity, _roomControlComponentEntityReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;
		private RoomControlSectionItemEntity _roomControlSectionItemEntity;
		private bool	_alwaysFetchRoomControlSectionItemEntity, _alreadyFetchedRoomControlSectionItemEntity, _roomControlSectionItemEntityReturnsNewIfNotFound;
		private RoomControlWidgetEntity _roomControlWidgetEntity;
		private bool	_alwaysFetchRoomControlWidgetEntity, _alreadyFetchedRoomControlWidgetEntity, _roomControlWidgetEntityReturnsNewIfNotFound;
		private RoutestephandlerEntity _routestephandlerEntity;
		private bool	_alwaysFetchRoutestephandlerEntity, _alreadyFetchedRoutestephandlerEntity, _routestephandlerEntityReturnsNewIfNotFound;
		private ScheduledMessageEntity _scheduledMessageEntity;
		private bool	_alwaysFetchScheduledMessageEntity, _alreadyFetchedScheduledMessageEntity, _scheduledMessageEntityReturnsNewIfNotFound;
		private ServiceMethodEntity _serviceMethodEntity;
		private bool	_alwaysFetchServiceMethodEntity, _alreadyFetchedServiceMethodEntity, _serviceMethodEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;
		private SiteTemplateEntity _siteTemplateEntity;
		private bool	_alwaysFetchSiteTemplateEntity, _alreadyFetchedSiteTemplateEntity, _siteTemplateEntityReturnsNewIfNotFound;
		private StationEntity _stationEntity;
		private bool	_alwaysFetchStationEntity, _alreadyFetchedStationEntity, _stationEntityReturnsNewIfNotFound;
		private UIFooterItemEntity _uIFooterItemEntity;
		private bool	_alwaysFetchUIFooterItemEntity, _alreadyFetchedUIFooterItemEntity, _uIFooterItemEntityReturnsNewIfNotFound;
		private UITabEntity _uITabEntity;
		private bool	_alwaysFetchUITabEntity, _alreadyFetchedUITabEntity, _uITabEntityReturnsNewIfNotFound;
		private UIWidgetEntity _uIWidgetEntity;
		private bool	_alwaysFetchUIWidgetEntity, _alreadyFetchedUIWidgetEntity, _uIWidgetEntityReturnsNewIfNotFound;
		private VenueCategoryEntity _venueCategoryEntity;
		private bool	_alwaysFetchVenueCategoryEntity, _alreadyFetchedVenueCategoryEntity, _venueCategoryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ActionButtonEntity</summary>
			public static readonly string ActionButtonEntity = "ActionButtonEntity";
			/// <summary>Member name AdvertisementEntity</summary>
			public static readonly string AdvertisementEntity = "AdvertisementEntity";
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationoptionEntity</summary>
			public static readonly string AlterationoptionEntity = "AlterationoptionEntity";
			/// <summary>Member name AmenityEntity</summary>
			public static readonly string AmenityEntity = "AmenityEntity";
			/// <summary>Member name AnnouncementEntity</summary>
			public static readonly string AnnouncementEntity = "AnnouncementEntity";
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name CarouselItemEntity</summary>
			public static readonly string CarouselItemEntity = "CarouselItemEntity";
			/// <summary>Member name LandingPageEntity</summary>
			public static readonly string LandingPageEntity = "LandingPageEntity";
			/// <summary>Member name NavigationMenuItemEntity</summary>
			public static readonly string NavigationMenuItemEntity = "NavigationMenuItemEntity";
			/// <summary>Member name WidgetEntity</summary>
			public static readonly string WidgetEntity = "WidgetEntity";
			/// <summary>Member name AttachmentEntity</summary>
			public static readonly string AttachmentEntity = "AttachmentEntity";
			/// <summary>Member name AvailabilityEntity</summary>
			public static readonly string AvailabilityEntity = "AvailabilityEntity";
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name CheckoutMethodEntity</summary>
			public static readonly string CheckoutMethodEntity = "CheckoutMethodEntity";
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name EntertainmentcategoryEntity</summary>
			public static readonly string EntertainmentcategoryEntity = "EntertainmentcategoryEntity";
			/// <summary>Member name GenericcategoryEntity</summary>
			public static readonly string GenericcategoryEntity = "GenericcategoryEntity";
			/// <summary>Member name GenericproductEntity</summary>
			public static readonly string GenericproductEntity = "GenericproductEntity";
			/// <summary>Member name InfraredCommandEntity</summary>
			public static readonly string InfraredCommandEntity = "InfraredCommandEntity";
			/// <summary>Member name LanguageEntity</summary>
			public static readonly string LanguageEntity = "LanguageEntity";
			/// <summary>Member name OutletEntity</summary>
			public static readonly string OutletEntity = "OutletEntity";
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name PageTemplateEntity</summary>
			public static readonly string PageTemplateEntity = "PageTemplateEntity";
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductgroupEntity</summary>
			public static readonly string ProductgroupEntity = "ProductgroupEntity";
			/// <summary>Member name RoomControlAreaEntity</summary>
			public static readonly string RoomControlAreaEntity = "RoomControlAreaEntity";
			/// <summary>Member name RoomControlComponentEntity</summary>
			public static readonly string RoomControlComponentEntity = "RoomControlComponentEntity";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name RoomControlSectionItemEntity</summary>
			public static readonly string RoomControlSectionItemEntity = "RoomControlSectionItemEntity";
			/// <summary>Member name RoomControlWidgetEntity</summary>
			public static readonly string RoomControlWidgetEntity = "RoomControlWidgetEntity";
			/// <summary>Member name RoutestephandlerEntity</summary>
			public static readonly string RoutestephandlerEntity = "RoutestephandlerEntity";
			/// <summary>Member name ScheduledMessageEntity</summary>
			public static readonly string ScheduledMessageEntity = "ScheduledMessageEntity";
			/// <summary>Member name ServiceMethodEntity</summary>
			public static readonly string ServiceMethodEntity = "ServiceMethodEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name SiteTemplateEntity</summary>
			public static readonly string SiteTemplateEntity = "SiteTemplateEntity";
			/// <summary>Member name StationEntity</summary>
			public static readonly string StationEntity = "StationEntity";
			/// <summary>Member name UIFooterItemEntity</summary>
			public static readonly string UIFooterItemEntity = "UIFooterItemEntity";
			/// <summary>Member name UITabEntity</summary>
			public static readonly string UITabEntity = "UITabEntity";
			/// <summary>Member name UIWidgetEntity</summary>
			public static readonly string UIWidgetEntity = "UIWidgetEntity";
			/// <summary>Member name VenueCategoryEntity</summary>
			public static readonly string VenueCategoryEntity = "VenueCategoryEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomTextEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CustomTextEntityBase() :base("CustomTextEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		protected CustomTextEntityBase(System.Int32 customTextId):base("CustomTextEntity")
		{
			InitClassFetch(customTextId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CustomTextEntityBase(System.Int32 customTextId, IPrefetchPath prefetchPathToUse): base("CustomTextEntity")
		{
			InitClassFetch(customTextId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="validator">The custom validator object for this CustomTextEntity</param>
		protected CustomTextEntityBase(System.Int32 customTextId, IValidator validator):base("CustomTextEntity")
		{
			InitClassFetch(customTextId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomTextEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_actionButtonEntity = (ActionButtonEntity)info.GetValue("_actionButtonEntity", typeof(ActionButtonEntity));
			if(_actionButtonEntity!=null)
			{
				_actionButtonEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionButtonEntityReturnsNewIfNotFound = info.GetBoolean("_actionButtonEntityReturnsNewIfNotFound");
			_alwaysFetchActionButtonEntity = info.GetBoolean("_alwaysFetchActionButtonEntity");
			_alreadyFetchedActionButtonEntity = info.GetBoolean("_alreadyFetchedActionButtonEntity");

			_advertisementEntity = (AdvertisementEntity)info.GetValue("_advertisementEntity", typeof(AdvertisementEntity));
			if(_advertisementEntity!=null)
			{
				_advertisementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementEntity = info.GetBoolean("_alwaysFetchAdvertisementEntity");
			_alreadyFetchedAdvertisementEntity = info.GetBoolean("_alreadyFetchedAdvertisementEntity");

			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationoptionEntity = (AlterationoptionEntity)info.GetValue("_alterationoptionEntity", typeof(AlterationoptionEntity));
			if(_alterationoptionEntity!=null)
			{
				_alterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_alterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationoptionEntity = info.GetBoolean("_alwaysFetchAlterationoptionEntity");
			_alreadyFetchedAlterationoptionEntity = info.GetBoolean("_alreadyFetchedAlterationoptionEntity");

			_amenityEntity = (AmenityEntity)info.GetValue("_amenityEntity", typeof(AmenityEntity));
			if(_amenityEntity!=null)
			{
				_amenityEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_amenityEntityReturnsNewIfNotFound = info.GetBoolean("_amenityEntityReturnsNewIfNotFound");
			_alwaysFetchAmenityEntity = info.GetBoolean("_alwaysFetchAmenityEntity");
			_alreadyFetchedAmenityEntity = info.GetBoolean("_alreadyFetchedAmenityEntity");

			_announcementEntity = (AnnouncementEntity)info.GetValue("_announcementEntity", typeof(AnnouncementEntity));
			if(_announcementEntity!=null)
			{
				_announcementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_announcementEntityReturnsNewIfNotFound = info.GetBoolean("_announcementEntityReturnsNewIfNotFound");
			_alwaysFetchAnnouncementEntity = info.GetBoolean("_alwaysFetchAnnouncementEntity");
			_alreadyFetchedAnnouncementEntity = info.GetBoolean("_alreadyFetchedAnnouncementEntity");

			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");

			_carouselItemEntity = (CarouselItemEntity)info.GetValue("_carouselItemEntity", typeof(CarouselItemEntity));
			if(_carouselItemEntity!=null)
			{
				_carouselItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_carouselItemEntityReturnsNewIfNotFound = info.GetBoolean("_carouselItemEntityReturnsNewIfNotFound");
			_alwaysFetchCarouselItemEntity = info.GetBoolean("_alwaysFetchCarouselItemEntity");
			_alreadyFetchedCarouselItemEntity = info.GetBoolean("_alreadyFetchedCarouselItemEntity");

			_landingPageEntity = (LandingPageEntity)info.GetValue("_landingPageEntity", typeof(LandingPageEntity));
			if(_landingPageEntity!=null)
			{
				_landingPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_landingPageEntityReturnsNewIfNotFound = info.GetBoolean("_landingPageEntityReturnsNewIfNotFound");
			_alwaysFetchLandingPageEntity = info.GetBoolean("_alwaysFetchLandingPageEntity");
			_alreadyFetchedLandingPageEntity = info.GetBoolean("_alreadyFetchedLandingPageEntity");

			_navigationMenuItemEntity = (NavigationMenuItemEntity)info.GetValue("_navigationMenuItemEntity", typeof(NavigationMenuItemEntity));
			if(_navigationMenuItemEntity!=null)
			{
				_navigationMenuItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_navigationMenuItemEntityReturnsNewIfNotFound = info.GetBoolean("_navigationMenuItemEntityReturnsNewIfNotFound");
			_alwaysFetchNavigationMenuItemEntity = info.GetBoolean("_alwaysFetchNavigationMenuItemEntity");
			_alreadyFetchedNavigationMenuItemEntity = info.GetBoolean("_alreadyFetchedNavigationMenuItemEntity");

			_widgetEntity = (WidgetEntity)info.GetValue("_widgetEntity", typeof(WidgetEntity));
			if(_widgetEntity!=null)
			{
				_widgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetEntityReturnsNewIfNotFound = info.GetBoolean("_widgetEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetEntity = info.GetBoolean("_alwaysFetchWidgetEntity");
			_alreadyFetchedWidgetEntity = info.GetBoolean("_alreadyFetchedWidgetEntity");

			_attachmentEntity = (AttachmentEntity)info.GetValue("_attachmentEntity", typeof(AttachmentEntity));
			if(_attachmentEntity!=null)
			{
				_attachmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attachmentEntityReturnsNewIfNotFound = info.GetBoolean("_attachmentEntityReturnsNewIfNotFound");
			_alwaysFetchAttachmentEntity = info.GetBoolean("_alwaysFetchAttachmentEntity");
			_alreadyFetchedAttachmentEntity = info.GetBoolean("_alreadyFetchedAttachmentEntity");

			_availabilityEntity = (AvailabilityEntity)info.GetValue("_availabilityEntity", typeof(AvailabilityEntity));
			if(_availabilityEntity!=null)
			{
				_availabilityEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_availabilityEntityReturnsNewIfNotFound = info.GetBoolean("_availabilityEntityReturnsNewIfNotFound");
			_alwaysFetchAvailabilityEntity = info.GetBoolean("_alwaysFetchAvailabilityEntity");
			_alreadyFetchedAvailabilityEntity = info.GetBoolean("_alreadyFetchedAvailabilityEntity");

			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_checkoutMethodEntity = (CheckoutMethodEntity)info.GetValue("_checkoutMethodEntity", typeof(CheckoutMethodEntity));
			if(_checkoutMethodEntity!=null)
			{
				_checkoutMethodEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_checkoutMethodEntityReturnsNewIfNotFound = info.GetBoolean("_checkoutMethodEntityReturnsNewIfNotFound");
			_alwaysFetchCheckoutMethodEntity = info.GetBoolean("_alwaysFetchCheckoutMethodEntity");
			_alreadyFetchedCheckoutMethodEntity = info.GetBoolean("_alreadyFetchedCheckoutMethodEntity");

			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_entertainmentcategoryEntity = (EntertainmentcategoryEntity)info.GetValue("_entertainmentcategoryEntity", typeof(EntertainmentcategoryEntity));
			if(_entertainmentcategoryEntity!=null)
			{
				_entertainmentcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentcategoryEntity = info.GetBoolean("_alwaysFetchEntertainmentcategoryEntity");
			_alreadyFetchedEntertainmentcategoryEntity = info.GetBoolean("_alreadyFetchedEntertainmentcategoryEntity");

			_genericcategoryEntity = (GenericcategoryEntity)info.GetValue("_genericcategoryEntity", typeof(GenericcategoryEntity));
			if(_genericcategoryEntity!=null)
			{
				_genericcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_genericcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchGenericcategoryEntity = info.GetBoolean("_alwaysFetchGenericcategoryEntity");
			_alreadyFetchedGenericcategoryEntity = info.GetBoolean("_alreadyFetchedGenericcategoryEntity");

			_genericproductEntity = (GenericproductEntity)info.GetValue("_genericproductEntity", typeof(GenericproductEntity));
			if(_genericproductEntity!=null)
			{
				_genericproductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericproductEntityReturnsNewIfNotFound = info.GetBoolean("_genericproductEntityReturnsNewIfNotFound");
			_alwaysFetchGenericproductEntity = info.GetBoolean("_alwaysFetchGenericproductEntity");
			_alreadyFetchedGenericproductEntity = info.GetBoolean("_alreadyFetchedGenericproductEntity");

			_infraredCommandEntity = (InfraredCommandEntity)info.GetValue("_infraredCommandEntity", typeof(InfraredCommandEntity));
			if(_infraredCommandEntity!=null)
			{
				_infraredCommandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_infraredCommandEntityReturnsNewIfNotFound = info.GetBoolean("_infraredCommandEntityReturnsNewIfNotFound");
			_alwaysFetchInfraredCommandEntity = info.GetBoolean("_alwaysFetchInfraredCommandEntity");
			_alreadyFetchedInfraredCommandEntity = info.GetBoolean("_alreadyFetchedInfraredCommandEntity");

			_languageEntity = (LanguageEntity)info.GetValue("_languageEntity", typeof(LanguageEntity));
			if(_languageEntity!=null)
			{
				_languageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageEntityReturnsNewIfNotFound = info.GetBoolean("_languageEntityReturnsNewIfNotFound");
			_alwaysFetchLanguageEntity = info.GetBoolean("_alwaysFetchLanguageEntity");
			_alreadyFetchedLanguageEntity = info.GetBoolean("_alreadyFetchedLanguageEntity");

			_outletEntity = (OutletEntity)info.GetValue("_outletEntity", typeof(OutletEntity));
			if(_outletEntity!=null)
			{
				_outletEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletEntityReturnsNewIfNotFound = info.GetBoolean("_outletEntityReturnsNewIfNotFound");
			_alwaysFetchOutletEntity = info.GetBoolean("_alwaysFetchOutletEntity");
			_alreadyFetchedOutletEntity = info.GetBoolean("_alreadyFetchedOutletEntity");

			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_pageTemplateEntity = (PageTemplateEntity)info.GetValue("_pageTemplateEntity", typeof(PageTemplateEntity));
			if(_pageTemplateEntity!=null)
			{
				_pageTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_pageTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchPageTemplateEntity = info.GetBoolean("_alwaysFetchPageTemplateEntity");
			_alreadyFetchedPageTemplateEntity = info.GetBoolean("_alreadyFetchedPageTemplateEntity");

			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productgroupEntity = (ProductgroupEntity)info.GetValue("_productgroupEntity", typeof(ProductgroupEntity));
			if(_productgroupEntity!=null)
			{
				_productgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productgroupEntityReturnsNewIfNotFound = info.GetBoolean("_productgroupEntityReturnsNewIfNotFound");
			_alwaysFetchProductgroupEntity = info.GetBoolean("_alwaysFetchProductgroupEntity");
			_alreadyFetchedProductgroupEntity = info.GetBoolean("_alreadyFetchedProductgroupEntity");

			_roomControlAreaEntity = (RoomControlAreaEntity)info.GetValue("_roomControlAreaEntity", typeof(RoomControlAreaEntity));
			if(_roomControlAreaEntity!=null)
			{
				_roomControlAreaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlAreaEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlAreaEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlAreaEntity = info.GetBoolean("_alwaysFetchRoomControlAreaEntity");
			_alreadyFetchedRoomControlAreaEntity = info.GetBoolean("_alreadyFetchedRoomControlAreaEntity");

			_roomControlComponentEntity = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity!=null)
			{
				_roomControlComponentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity = info.GetBoolean("_alwaysFetchRoomControlComponentEntity");
			_alreadyFetchedRoomControlComponentEntity = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");

			_roomControlSectionItemEntity = (RoomControlSectionItemEntity)info.GetValue("_roomControlSectionItemEntity", typeof(RoomControlSectionItemEntity));
			if(_roomControlSectionItemEntity!=null)
			{
				_roomControlSectionItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionItemEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionItemEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionItemEntity = info.GetBoolean("_alwaysFetchRoomControlSectionItemEntity");
			_alreadyFetchedRoomControlSectionItemEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionItemEntity");

			_roomControlWidgetEntity = (RoomControlWidgetEntity)info.GetValue("_roomControlWidgetEntity", typeof(RoomControlWidgetEntity));
			if(_roomControlWidgetEntity!=null)
			{
				_roomControlWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlWidgetEntity = info.GetBoolean("_alwaysFetchRoomControlWidgetEntity");
			_alreadyFetchedRoomControlWidgetEntity = info.GetBoolean("_alreadyFetchedRoomControlWidgetEntity");

			_routestephandlerEntity = (RoutestephandlerEntity)info.GetValue("_routestephandlerEntity", typeof(RoutestephandlerEntity));
			if(_routestephandlerEntity!=null)
			{
				_routestephandlerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routestephandlerEntityReturnsNewIfNotFound = info.GetBoolean("_routestephandlerEntityReturnsNewIfNotFound");
			_alwaysFetchRoutestephandlerEntity = info.GetBoolean("_alwaysFetchRoutestephandlerEntity");
			_alreadyFetchedRoutestephandlerEntity = info.GetBoolean("_alreadyFetchedRoutestephandlerEntity");

			_scheduledMessageEntity = (ScheduledMessageEntity)info.GetValue("_scheduledMessageEntity", typeof(ScheduledMessageEntity));
			if(_scheduledMessageEntity!=null)
			{
				_scheduledMessageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduledMessageEntityReturnsNewIfNotFound = info.GetBoolean("_scheduledMessageEntityReturnsNewIfNotFound");
			_alwaysFetchScheduledMessageEntity = info.GetBoolean("_alwaysFetchScheduledMessageEntity");
			_alreadyFetchedScheduledMessageEntity = info.GetBoolean("_alreadyFetchedScheduledMessageEntity");

			_serviceMethodEntity = (ServiceMethodEntity)info.GetValue("_serviceMethodEntity", typeof(ServiceMethodEntity));
			if(_serviceMethodEntity!=null)
			{
				_serviceMethodEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_serviceMethodEntityReturnsNewIfNotFound = info.GetBoolean("_serviceMethodEntityReturnsNewIfNotFound");
			_alwaysFetchServiceMethodEntity = info.GetBoolean("_alwaysFetchServiceMethodEntity");
			_alreadyFetchedServiceMethodEntity = info.GetBoolean("_alreadyFetchedServiceMethodEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");

			_siteTemplateEntity = (SiteTemplateEntity)info.GetValue("_siteTemplateEntity", typeof(SiteTemplateEntity));
			if(_siteTemplateEntity!=null)
			{
				_siteTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_siteTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchSiteTemplateEntity = info.GetBoolean("_alwaysFetchSiteTemplateEntity");
			_alreadyFetchedSiteTemplateEntity = info.GetBoolean("_alreadyFetchedSiteTemplateEntity");

			_stationEntity = (StationEntity)info.GetValue("_stationEntity", typeof(StationEntity));
			if(_stationEntity!=null)
			{
				_stationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_stationEntityReturnsNewIfNotFound = info.GetBoolean("_stationEntityReturnsNewIfNotFound");
			_alwaysFetchStationEntity = info.GetBoolean("_alwaysFetchStationEntity");
			_alreadyFetchedStationEntity = info.GetBoolean("_alreadyFetchedStationEntity");

			_uIFooterItemEntity = (UIFooterItemEntity)info.GetValue("_uIFooterItemEntity", typeof(UIFooterItemEntity));
			if(_uIFooterItemEntity!=null)
			{
				_uIFooterItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIFooterItemEntityReturnsNewIfNotFound = info.GetBoolean("_uIFooterItemEntityReturnsNewIfNotFound");
			_alwaysFetchUIFooterItemEntity = info.GetBoolean("_alwaysFetchUIFooterItemEntity");
			_alreadyFetchedUIFooterItemEntity = info.GetBoolean("_alreadyFetchedUIFooterItemEntity");

			_uITabEntity = (UITabEntity)info.GetValue("_uITabEntity", typeof(UITabEntity));
			if(_uITabEntity!=null)
			{
				_uITabEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uITabEntityReturnsNewIfNotFound = info.GetBoolean("_uITabEntityReturnsNewIfNotFound");
			_alwaysFetchUITabEntity = info.GetBoolean("_alwaysFetchUITabEntity");
			_alreadyFetchedUITabEntity = info.GetBoolean("_alreadyFetchedUITabEntity");

			_uIWidgetEntity = (UIWidgetEntity)info.GetValue("_uIWidgetEntity", typeof(UIWidgetEntity));
			if(_uIWidgetEntity!=null)
			{
				_uIWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_uIWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchUIWidgetEntity = info.GetBoolean("_alwaysFetchUIWidgetEntity");
			_alreadyFetchedUIWidgetEntity = info.GetBoolean("_alreadyFetchedUIWidgetEntity");

			_venueCategoryEntity = (VenueCategoryEntity)info.GetValue("_venueCategoryEntity", typeof(VenueCategoryEntity));
			if(_venueCategoryEntity!=null)
			{
				_venueCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_venueCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_venueCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchVenueCategoryEntity = info.GetBoolean("_alwaysFetchVenueCategoryEntity");
			_alreadyFetchedVenueCategoryEntity = info.GetBoolean("_alreadyFetchedVenueCategoryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomTextFieldIndex)fieldIndex)
			{
				case CustomTextFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case CustomTextFieldIndex.LanguageId:
					DesetupSyncLanguageEntity(true, false);
					_alreadyFetchedLanguageEntity = false;
					break;
				case CustomTextFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case CustomTextFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case CustomTextFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case CustomTextFieldIndex.AdvertisementId:
					DesetupSyncAdvertisementEntity(true, false);
					_alreadyFetchedAdvertisementEntity = false;
					break;
				case CustomTextFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case CustomTextFieldIndex.AlterationoptionId:
					DesetupSyncAlterationoptionEntity(true, false);
					_alreadyFetchedAlterationoptionEntity = false;
					break;
				case CustomTextFieldIndex.AnnouncementId:
					DesetupSyncAnnouncementEntity(true, false);
					_alreadyFetchedAnnouncementEntity = false;
					break;
				case CustomTextFieldIndex.ActionButtonId:
					DesetupSyncActionButtonEntity(true, false);
					_alreadyFetchedActionButtonEntity = false;
					break;
				case CustomTextFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				case CustomTextFieldIndex.EntertainmentcategoryId:
					DesetupSyncEntertainmentcategoryEntity(true, false);
					_alreadyFetchedEntertainmentcategoryEntity = false;
					break;
				case CustomTextFieldIndex.GenericproductId:
					DesetupSyncGenericproductEntity(true, false);
					_alreadyFetchedGenericproductEntity = false;
					break;
				case CustomTextFieldIndex.PageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case CustomTextFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case CustomTextFieldIndex.AmenityId:
					DesetupSyncAmenityEntity(true, false);
					_alreadyFetchedAmenityEntity = false;
					break;
				case CustomTextFieldIndex.AttachmentId:
					DesetupSyncAttachmentEntity(true, false);
					_alreadyFetchedAttachmentEntity = false;
					break;
				case CustomTextFieldIndex.PageTemplateId:
					DesetupSyncPageTemplateEntity(true, false);
					_alreadyFetchedPageTemplateEntity = false;
					break;
				case CustomTextFieldIndex.GenericcategoryId:
					DesetupSyncGenericcategoryEntity(true, false);
					_alreadyFetchedGenericcategoryEntity = false;
					break;
				case CustomTextFieldIndex.RoomControlAreaId:
					DesetupSyncRoomControlAreaEntity(true, false);
					_alreadyFetchedRoomControlAreaEntity = false;
					break;
				case CustomTextFieldIndex.VenueCategoryId:
					DesetupSyncVenueCategoryEntity(true, false);
					_alreadyFetchedVenueCategoryEntity = false;
					break;
				case CustomTextFieldIndex.RoomControlComponentId:
					DesetupSyncRoomControlComponentEntity(true, false);
					_alreadyFetchedRoomControlComponentEntity = false;
					break;
				case CustomTextFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				case CustomTextFieldIndex.RoomControlSectionItemId:
					DesetupSyncRoomControlSectionItemEntity(true, false);
					_alreadyFetchedRoomControlSectionItemEntity = false;
					break;
				case CustomTextFieldIndex.RoomControlWidgetId:
					DesetupSyncRoomControlWidgetEntity(true, false);
					_alreadyFetchedRoomControlWidgetEntity = false;
					break;
				case CustomTextFieldIndex.ScheduledMessageId:
					DesetupSyncScheduledMessageEntity(true, false);
					_alreadyFetchedScheduledMessageEntity = false;
					break;
				case CustomTextFieldIndex.SiteTemplateId:
					DesetupSyncSiteTemplateEntity(true, false);
					_alreadyFetchedSiteTemplateEntity = false;
					break;
				case CustomTextFieldIndex.StationId:
					DesetupSyncStationEntity(true, false);
					_alreadyFetchedStationEntity = false;
					break;
				case CustomTextFieldIndex.UIFooterItemId:
					DesetupSyncUIFooterItemEntity(true, false);
					_alreadyFetchedUIFooterItemEntity = false;
					break;
				case CustomTextFieldIndex.UITabId:
					DesetupSyncUITabEntity(true, false);
					_alreadyFetchedUITabEntity = false;
					break;
				case CustomTextFieldIndex.UIWidgetId:
					DesetupSyncUIWidgetEntity(true, false);
					_alreadyFetchedUIWidgetEntity = false;
					break;
				case CustomTextFieldIndex.AvailabilityId:
					DesetupSyncAvailabilityEntity(true, false);
					_alreadyFetchedAvailabilityEntity = false;
					break;
				case CustomTextFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case CustomTextFieldIndex.ProductgroupId:
					DesetupSyncProductgroupEntity(true, false);
					_alreadyFetchedProductgroupEntity = false;
					break;
				case CustomTextFieldIndex.RoutestephandlerId:
					DesetupSyncRoutestephandlerEntity(true, false);
					_alreadyFetchedRoutestephandlerEntity = false;
					break;
				case CustomTextFieldIndex.InfraredCommandId:
					DesetupSyncInfraredCommandEntity(true, false);
					_alreadyFetchedInfraredCommandEntity = false;
					break;
				case CustomTextFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				case CustomTextFieldIndex.NavigationMenuItemId:
					DesetupSyncNavigationMenuItemEntity(true, false);
					_alreadyFetchedNavigationMenuItemEntity = false;
					break;
				case CustomTextFieldIndex.CarouselItemId:
					DesetupSyncCarouselItemEntity(true, false);
					_alreadyFetchedCarouselItemEntity = false;
					break;
				case CustomTextFieldIndex.WidgetId:
					DesetupSyncWidgetEntity(true, false);
					_alreadyFetchedWidgetEntity = false;
					break;
				case CustomTextFieldIndex.LandingPageId:
					DesetupSyncLandingPageEntity(true, false);
					_alreadyFetchedLandingPageEntity = false;
					break;
				case CustomTextFieldIndex.OutletId:
					DesetupSyncOutletEntity(true, false);
					_alreadyFetchedOutletEntity = false;
					break;
				case CustomTextFieldIndex.ServiceMethodId:
					DesetupSyncServiceMethodEntity(true, false);
					_alreadyFetchedServiceMethodEntity = false;
					break;
				case CustomTextFieldIndex.CheckoutMethodId:
					DesetupSyncCheckoutMethodEntity(true, false);
					_alreadyFetchedCheckoutMethodEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedActionButtonEntity = (_actionButtonEntity != null);
			_alreadyFetchedAdvertisementEntity = (_advertisementEntity != null);
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationoptionEntity = (_alterationoptionEntity != null);
			_alreadyFetchedAmenityEntity = (_amenityEntity != null);
			_alreadyFetchedAnnouncementEntity = (_announcementEntity != null);
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
			_alreadyFetchedCarouselItemEntity = (_carouselItemEntity != null);
			_alreadyFetchedLandingPageEntity = (_landingPageEntity != null);
			_alreadyFetchedNavigationMenuItemEntity = (_navigationMenuItemEntity != null);
			_alreadyFetchedWidgetEntity = (_widgetEntity != null);
			_alreadyFetchedAttachmentEntity = (_attachmentEntity != null);
			_alreadyFetchedAvailabilityEntity = (_availabilityEntity != null);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedCheckoutMethodEntity = (_checkoutMethodEntity != null);
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedEntertainmentcategoryEntity = (_entertainmentcategoryEntity != null);
			_alreadyFetchedGenericcategoryEntity = (_genericcategoryEntity != null);
			_alreadyFetchedGenericproductEntity = (_genericproductEntity != null);
			_alreadyFetchedInfraredCommandEntity = (_infraredCommandEntity != null);
			_alreadyFetchedLanguageEntity = (_languageEntity != null);
			_alreadyFetchedOutletEntity = (_outletEntity != null);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedPageTemplateEntity = (_pageTemplateEntity != null);
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductgroupEntity = (_productgroupEntity != null);
			_alreadyFetchedRoomControlAreaEntity = (_roomControlAreaEntity != null);
			_alreadyFetchedRoomControlComponentEntity = (_roomControlComponentEntity != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
			_alreadyFetchedRoomControlSectionItemEntity = (_roomControlSectionItemEntity != null);
			_alreadyFetchedRoomControlWidgetEntity = (_roomControlWidgetEntity != null);
			_alreadyFetchedRoutestephandlerEntity = (_routestephandlerEntity != null);
			_alreadyFetchedScheduledMessageEntity = (_scheduledMessageEntity != null);
			_alreadyFetchedServiceMethodEntity = (_serviceMethodEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
			_alreadyFetchedSiteTemplateEntity = (_siteTemplateEntity != null);
			_alreadyFetchedStationEntity = (_stationEntity != null);
			_alreadyFetchedUIFooterItemEntity = (_uIFooterItemEntity != null);
			_alreadyFetchedUITabEntity = (_uITabEntity != null);
			_alreadyFetchedUIWidgetEntity = (_uIWidgetEntity != null);
			_alreadyFetchedVenueCategoryEntity = (_venueCategoryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ActionButtonEntity":
					toReturn.Add(Relations.ActionButtonEntityUsingActionButtonId);
					break;
				case "AdvertisementEntity":
					toReturn.Add(Relations.AdvertisementEntityUsingAdvertisementId);
					break;
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationoptionEntity":
					toReturn.Add(Relations.AlterationoptionEntityUsingAlterationoptionId);
					break;
				case "AmenityEntity":
					toReturn.Add(Relations.AmenityEntityUsingAmenityId);
					break;
				case "AnnouncementEntity":
					toReturn.Add(Relations.AnnouncementEntityUsingAnnouncementId);
					break;
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "CarouselItemEntity":
					toReturn.Add(Relations.CarouselItemEntityUsingCarouselItemId);
					break;
				case "LandingPageEntity":
					toReturn.Add(Relations.LandingPageEntityUsingLandingPageId);
					break;
				case "NavigationMenuItemEntity":
					toReturn.Add(Relations.NavigationMenuItemEntityUsingNavigationMenuItemId);
					break;
				case "WidgetEntity":
					toReturn.Add(Relations.WidgetEntityUsingWidgetId);
					break;
				case "AttachmentEntity":
					toReturn.Add(Relations.AttachmentEntityUsingAttachmentId);
					break;
				case "AvailabilityEntity":
					toReturn.Add(Relations.AvailabilityEntityUsingAvailabilityId);
					break;
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "CheckoutMethodEntity":
					toReturn.Add(Relations.CheckoutMethodEntityUsingCheckoutMethodId);
					break;
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "EntertainmentcategoryEntity":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
					break;
				case "GenericcategoryEntity":
					toReturn.Add(Relations.GenericcategoryEntityUsingGenericcategoryId);
					break;
				case "GenericproductEntity":
					toReturn.Add(Relations.GenericproductEntityUsingGenericproductId);
					break;
				case "InfraredCommandEntity":
					toReturn.Add(Relations.InfraredCommandEntityUsingInfraredCommandId);
					break;
				case "LanguageEntity":
					toReturn.Add(Relations.LanguageEntityUsingLanguageId);
					break;
				case "OutletEntity":
					toReturn.Add(Relations.OutletEntityUsingOutletId);
					break;
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageId);
					break;
				case "PageTemplateEntity":
					toReturn.Add(Relations.PageTemplateEntityUsingPageTemplateId);
					break;
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductgroupEntity":
					toReturn.Add(Relations.ProductgroupEntityUsingProductgroupId);
					break;
				case "RoomControlAreaEntity":
					toReturn.Add(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
					break;
				case "RoomControlComponentEntity":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "RoomControlSectionItemEntity":
					toReturn.Add(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
					break;
				case "RoomControlWidgetEntity":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlWidgetId);
					break;
				case "RoutestephandlerEntity":
					toReturn.Add(Relations.RoutestephandlerEntityUsingRoutestephandlerId);
					break;
				case "ScheduledMessageEntity":
					toReturn.Add(Relations.ScheduledMessageEntityUsingScheduledMessageId);
					break;
				case "ServiceMethodEntity":
					toReturn.Add(Relations.ServiceMethodEntityUsingServiceMethodId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "SiteTemplateEntity":
					toReturn.Add(Relations.SiteTemplateEntityUsingSiteTemplateId);
					break;
				case "StationEntity":
					toReturn.Add(Relations.StationEntityUsingStationId);
					break;
				case "UIFooterItemEntity":
					toReturn.Add(Relations.UIFooterItemEntityUsingUIFooterItemId);
					break;
				case "UITabEntity":
					toReturn.Add(Relations.UITabEntityUsingUITabId);
					break;
				case "UIWidgetEntity":
					toReturn.Add(Relations.UIWidgetEntityUsingUIWidgetId);
					break;
				case "VenueCategoryEntity":
					toReturn.Add(Relations.VenueCategoryEntityUsingVenueCategoryId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_actionButtonEntity", (!this.MarkedForDeletion?_actionButtonEntity:null));
			info.AddValue("_actionButtonEntityReturnsNewIfNotFound", _actionButtonEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionButtonEntity", _alwaysFetchActionButtonEntity);
			info.AddValue("_alreadyFetchedActionButtonEntity", _alreadyFetchedActionButtonEntity);
			info.AddValue("_advertisementEntity", (!this.MarkedForDeletion?_advertisementEntity:null));
			info.AddValue("_advertisementEntityReturnsNewIfNotFound", _advertisementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementEntity", _alwaysFetchAdvertisementEntity);
			info.AddValue("_alreadyFetchedAdvertisementEntity", _alreadyFetchedAdvertisementEntity);
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationoptionEntity", (!this.MarkedForDeletion?_alterationoptionEntity:null));
			info.AddValue("_alterationoptionEntityReturnsNewIfNotFound", _alterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationoptionEntity", _alwaysFetchAlterationoptionEntity);
			info.AddValue("_alreadyFetchedAlterationoptionEntity", _alreadyFetchedAlterationoptionEntity);
			info.AddValue("_amenityEntity", (!this.MarkedForDeletion?_amenityEntity:null));
			info.AddValue("_amenityEntityReturnsNewIfNotFound", _amenityEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAmenityEntity", _alwaysFetchAmenityEntity);
			info.AddValue("_alreadyFetchedAmenityEntity", _alreadyFetchedAmenityEntity);
			info.AddValue("_announcementEntity", (!this.MarkedForDeletion?_announcementEntity:null));
			info.AddValue("_announcementEntityReturnsNewIfNotFound", _announcementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAnnouncementEntity", _alwaysFetchAnnouncementEntity);
			info.AddValue("_alreadyFetchedAnnouncementEntity", _alreadyFetchedAnnouncementEntity);
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);
			info.AddValue("_carouselItemEntity", (!this.MarkedForDeletion?_carouselItemEntity:null));
			info.AddValue("_carouselItemEntityReturnsNewIfNotFound", _carouselItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCarouselItemEntity", _alwaysFetchCarouselItemEntity);
			info.AddValue("_alreadyFetchedCarouselItemEntity", _alreadyFetchedCarouselItemEntity);
			info.AddValue("_landingPageEntity", (!this.MarkedForDeletion?_landingPageEntity:null));
			info.AddValue("_landingPageEntityReturnsNewIfNotFound", _landingPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLandingPageEntity", _alwaysFetchLandingPageEntity);
			info.AddValue("_alreadyFetchedLandingPageEntity", _alreadyFetchedLandingPageEntity);
			info.AddValue("_navigationMenuItemEntity", (!this.MarkedForDeletion?_navigationMenuItemEntity:null));
			info.AddValue("_navigationMenuItemEntityReturnsNewIfNotFound", _navigationMenuItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNavigationMenuItemEntity", _alwaysFetchNavigationMenuItemEntity);
			info.AddValue("_alreadyFetchedNavigationMenuItemEntity", _alreadyFetchedNavigationMenuItemEntity);
			info.AddValue("_widgetEntity", (!this.MarkedForDeletion?_widgetEntity:null));
			info.AddValue("_widgetEntityReturnsNewIfNotFound", _widgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetEntity", _alwaysFetchWidgetEntity);
			info.AddValue("_alreadyFetchedWidgetEntity", _alreadyFetchedWidgetEntity);
			info.AddValue("_attachmentEntity", (!this.MarkedForDeletion?_attachmentEntity:null));
			info.AddValue("_attachmentEntityReturnsNewIfNotFound", _attachmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttachmentEntity", _alwaysFetchAttachmentEntity);
			info.AddValue("_alreadyFetchedAttachmentEntity", _alreadyFetchedAttachmentEntity);
			info.AddValue("_availabilityEntity", (!this.MarkedForDeletion?_availabilityEntity:null));
			info.AddValue("_availabilityEntityReturnsNewIfNotFound", _availabilityEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAvailabilityEntity", _alwaysFetchAvailabilityEntity);
			info.AddValue("_alreadyFetchedAvailabilityEntity", _alreadyFetchedAvailabilityEntity);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_checkoutMethodEntity", (!this.MarkedForDeletion?_checkoutMethodEntity:null));
			info.AddValue("_checkoutMethodEntityReturnsNewIfNotFound", _checkoutMethodEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCheckoutMethodEntity", _alwaysFetchCheckoutMethodEntity);
			info.AddValue("_alreadyFetchedCheckoutMethodEntity", _alreadyFetchedCheckoutMethodEntity);
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_entertainmentcategoryEntity", (!this.MarkedForDeletion?_entertainmentcategoryEntity:null));
			info.AddValue("_entertainmentcategoryEntityReturnsNewIfNotFound", _entertainmentcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentcategoryEntity", _alwaysFetchEntertainmentcategoryEntity);
			info.AddValue("_alreadyFetchedEntertainmentcategoryEntity", _alreadyFetchedEntertainmentcategoryEntity);
			info.AddValue("_genericcategoryEntity", (!this.MarkedForDeletion?_genericcategoryEntity:null));
			info.AddValue("_genericcategoryEntityReturnsNewIfNotFound", _genericcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericcategoryEntity", _alwaysFetchGenericcategoryEntity);
			info.AddValue("_alreadyFetchedGenericcategoryEntity", _alreadyFetchedGenericcategoryEntity);
			info.AddValue("_genericproductEntity", (!this.MarkedForDeletion?_genericproductEntity:null));
			info.AddValue("_genericproductEntityReturnsNewIfNotFound", _genericproductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericproductEntity", _alwaysFetchGenericproductEntity);
			info.AddValue("_alreadyFetchedGenericproductEntity", _alreadyFetchedGenericproductEntity);
			info.AddValue("_infraredCommandEntity", (!this.MarkedForDeletion?_infraredCommandEntity:null));
			info.AddValue("_infraredCommandEntityReturnsNewIfNotFound", _infraredCommandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInfraredCommandEntity", _alwaysFetchInfraredCommandEntity);
			info.AddValue("_alreadyFetchedInfraredCommandEntity", _alreadyFetchedInfraredCommandEntity);
			info.AddValue("_languageEntity", (!this.MarkedForDeletion?_languageEntity:null));
			info.AddValue("_languageEntityReturnsNewIfNotFound", _languageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguageEntity", _alwaysFetchLanguageEntity);
			info.AddValue("_alreadyFetchedLanguageEntity", _alreadyFetchedLanguageEntity);
			info.AddValue("_outletEntity", (!this.MarkedForDeletion?_outletEntity:null));
			info.AddValue("_outletEntityReturnsNewIfNotFound", _outletEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletEntity", _alwaysFetchOutletEntity);
			info.AddValue("_alreadyFetchedOutletEntity", _alreadyFetchedOutletEntity);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_pageTemplateEntity", (!this.MarkedForDeletion?_pageTemplateEntity:null));
			info.AddValue("_pageTemplateEntityReturnsNewIfNotFound", _pageTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageTemplateEntity", _alwaysFetchPageTemplateEntity);
			info.AddValue("_alreadyFetchedPageTemplateEntity", _alreadyFetchedPageTemplateEntity);
			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productgroupEntity", (!this.MarkedForDeletion?_productgroupEntity:null));
			info.AddValue("_productgroupEntityReturnsNewIfNotFound", _productgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductgroupEntity", _alwaysFetchProductgroupEntity);
			info.AddValue("_alreadyFetchedProductgroupEntity", _alreadyFetchedProductgroupEntity);
			info.AddValue("_roomControlAreaEntity", (!this.MarkedForDeletion?_roomControlAreaEntity:null));
			info.AddValue("_roomControlAreaEntityReturnsNewIfNotFound", _roomControlAreaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlAreaEntity", _alwaysFetchRoomControlAreaEntity);
			info.AddValue("_alreadyFetchedRoomControlAreaEntity", _alreadyFetchedRoomControlAreaEntity);
			info.AddValue("_roomControlComponentEntity", (!this.MarkedForDeletion?_roomControlComponentEntity:null));
			info.AddValue("_roomControlComponentEntityReturnsNewIfNotFound", _roomControlComponentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity", _alwaysFetchRoomControlComponentEntity);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity", _alreadyFetchedRoomControlComponentEntity);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);
			info.AddValue("_roomControlSectionItemEntity", (!this.MarkedForDeletion?_roomControlSectionItemEntity:null));
			info.AddValue("_roomControlSectionItemEntityReturnsNewIfNotFound", _roomControlSectionItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionItemEntity", _alwaysFetchRoomControlSectionItemEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionItemEntity", _alreadyFetchedRoomControlSectionItemEntity);
			info.AddValue("_roomControlWidgetEntity", (!this.MarkedForDeletion?_roomControlWidgetEntity:null));
			info.AddValue("_roomControlWidgetEntityReturnsNewIfNotFound", _roomControlWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlWidgetEntity", _alwaysFetchRoomControlWidgetEntity);
			info.AddValue("_alreadyFetchedRoomControlWidgetEntity", _alreadyFetchedRoomControlWidgetEntity);
			info.AddValue("_routestephandlerEntity", (!this.MarkedForDeletion?_routestephandlerEntity:null));
			info.AddValue("_routestephandlerEntityReturnsNewIfNotFound", _routestephandlerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoutestephandlerEntity", _alwaysFetchRoutestephandlerEntity);
			info.AddValue("_alreadyFetchedRoutestephandlerEntity", _alreadyFetchedRoutestephandlerEntity);
			info.AddValue("_scheduledMessageEntity", (!this.MarkedForDeletion?_scheduledMessageEntity:null));
			info.AddValue("_scheduledMessageEntityReturnsNewIfNotFound", _scheduledMessageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduledMessageEntity", _alwaysFetchScheduledMessageEntity);
			info.AddValue("_alreadyFetchedScheduledMessageEntity", _alreadyFetchedScheduledMessageEntity);
			info.AddValue("_serviceMethodEntity", (!this.MarkedForDeletion?_serviceMethodEntity:null));
			info.AddValue("_serviceMethodEntityReturnsNewIfNotFound", _serviceMethodEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchServiceMethodEntity", _alwaysFetchServiceMethodEntity);
			info.AddValue("_alreadyFetchedServiceMethodEntity", _alreadyFetchedServiceMethodEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);
			info.AddValue("_siteTemplateEntity", (!this.MarkedForDeletion?_siteTemplateEntity:null));
			info.AddValue("_siteTemplateEntityReturnsNewIfNotFound", _siteTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteTemplateEntity", _alwaysFetchSiteTemplateEntity);
			info.AddValue("_alreadyFetchedSiteTemplateEntity", _alreadyFetchedSiteTemplateEntity);
			info.AddValue("_stationEntity", (!this.MarkedForDeletion?_stationEntity:null));
			info.AddValue("_stationEntityReturnsNewIfNotFound", _stationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchStationEntity", _alwaysFetchStationEntity);
			info.AddValue("_alreadyFetchedStationEntity", _alreadyFetchedStationEntity);
			info.AddValue("_uIFooterItemEntity", (!this.MarkedForDeletion?_uIFooterItemEntity:null));
			info.AddValue("_uIFooterItemEntityReturnsNewIfNotFound", _uIFooterItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIFooterItemEntity", _alwaysFetchUIFooterItemEntity);
			info.AddValue("_alreadyFetchedUIFooterItemEntity", _alreadyFetchedUIFooterItemEntity);
			info.AddValue("_uITabEntity", (!this.MarkedForDeletion?_uITabEntity:null));
			info.AddValue("_uITabEntityReturnsNewIfNotFound", _uITabEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUITabEntity", _alwaysFetchUITabEntity);
			info.AddValue("_alreadyFetchedUITabEntity", _alreadyFetchedUITabEntity);
			info.AddValue("_uIWidgetEntity", (!this.MarkedForDeletion?_uIWidgetEntity:null));
			info.AddValue("_uIWidgetEntityReturnsNewIfNotFound", _uIWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIWidgetEntity", _alwaysFetchUIWidgetEntity);
			info.AddValue("_alreadyFetchedUIWidgetEntity", _alreadyFetchedUIWidgetEntity);
			info.AddValue("_venueCategoryEntity", (!this.MarkedForDeletion?_venueCategoryEntity:null));
			info.AddValue("_venueCategoryEntityReturnsNewIfNotFound", _venueCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVenueCategoryEntity", _alwaysFetchVenueCategoryEntity);
			info.AddValue("_alreadyFetchedVenueCategoryEntity", _alreadyFetchedVenueCategoryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ActionButtonEntity":
					_alreadyFetchedActionButtonEntity = true;
					this.ActionButtonEntity = (ActionButtonEntity)entity;
					break;
				case "AdvertisementEntity":
					_alreadyFetchedAdvertisementEntity = true;
					this.AdvertisementEntity = (AdvertisementEntity)entity;
					break;
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationoptionEntity":
					_alreadyFetchedAlterationoptionEntity = true;
					this.AlterationoptionEntity = (AlterationoptionEntity)entity;
					break;
				case "AmenityEntity":
					_alreadyFetchedAmenityEntity = true;
					this.AmenityEntity = (AmenityEntity)entity;
					break;
				case "AnnouncementEntity":
					_alreadyFetchedAnnouncementEntity = true;
					this.AnnouncementEntity = (AnnouncementEntity)entity;
					break;
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "CarouselItemEntity":
					_alreadyFetchedCarouselItemEntity = true;
					this.CarouselItemEntity = (CarouselItemEntity)entity;
					break;
				case "LandingPageEntity":
					_alreadyFetchedLandingPageEntity = true;
					this.LandingPageEntity = (LandingPageEntity)entity;
					break;
				case "NavigationMenuItemEntity":
					_alreadyFetchedNavigationMenuItemEntity = true;
					this.NavigationMenuItemEntity = (NavigationMenuItemEntity)entity;
					break;
				case "WidgetEntity":
					_alreadyFetchedWidgetEntity = true;
					this.WidgetEntity = (WidgetEntity)entity;
					break;
				case "AttachmentEntity":
					_alreadyFetchedAttachmentEntity = true;
					this.AttachmentEntity = (AttachmentEntity)entity;
					break;
				case "AvailabilityEntity":
					_alreadyFetchedAvailabilityEntity = true;
					this.AvailabilityEntity = (AvailabilityEntity)entity;
					break;
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "CheckoutMethodEntity":
					_alreadyFetchedCheckoutMethodEntity = true;
					this.CheckoutMethodEntity = (CheckoutMethodEntity)entity;
					break;
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "EntertainmentcategoryEntity":
					_alreadyFetchedEntertainmentcategoryEntity = true;
					this.EntertainmentcategoryEntity = (EntertainmentcategoryEntity)entity;
					break;
				case "GenericcategoryEntity":
					_alreadyFetchedGenericcategoryEntity = true;
					this.GenericcategoryEntity = (GenericcategoryEntity)entity;
					break;
				case "GenericproductEntity":
					_alreadyFetchedGenericproductEntity = true;
					this.GenericproductEntity = (GenericproductEntity)entity;
					break;
				case "InfraredCommandEntity":
					_alreadyFetchedInfraredCommandEntity = true;
					this.InfraredCommandEntity = (InfraredCommandEntity)entity;
					break;
				case "LanguageEntity":
					_alreadyFetchedLanguageEntity = true;
					this.LanguageEntity = (LanguageEntity)entity;
					break;
				case "OutletEntity":
					_alreadyFetchedOutletEntity = true;
					this.OutletEntity = (OutletEntity)entity;
					break;
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "PageTemplateEntity":
					_alreadyFetchedPageTemplateEntity = true;
					this.PageTemplateEntity = (PageTemplateEntity)entity;
					break;
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductgroupEntity":
					_alreadyFetchedProductgroupEntity = true;
					this.ProductgroupEntity = (ProductgroupEntity)entity;
					break;
				case "RoomControlAreaEntity":
					_alreadyFetchedRoomControlAreaEntity = true;
					this.RoomControlAreaEntity = (RoomControlAreaEntity)entity;
					break;
				case "RoomControlComponentEntity":
					_alreadyFetchedRoomControlComponentEntity = true;
					this.RoomControlComponentEntity = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "RoomControlSectionItemEntity":
					_alreadyFetchedRoomControlSectionItemEntity = true;
					this.RoomControlSectionItemEntity = (RoomControlSectionItemEntity)entity;
					break;
				case "RoomControlWidgetEntity":
					_alreadyFetchedRoomControlWidgetEntity = true;
					this.RoomControlWidgetEntity = (RoomControlWidgetEntity)entity;
					break;
				case "RoutestephandlerEntity":
					_alreadyFetchedRoutestephandlerEntity = true;
					this.RoutestephandlerEntity = (RoutestephandlerEntity)entity;
					break;
				case "ScheduledMessageEntity":
					_alreadyFetchedScheduledMessageEntity = true;
					this.ScheduledMessageEntity = (ScheduledMessageEntity)entity;
					break;
				case "ServiceMethodEntity":
					_alreadyFetchedServiceMethodEntity = true;
					this.ServiceMethodEntity = (ServiceMethodEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "SiteTemplateEntity":
					_alreadyFetchedSiteTemplateEntity = true;
					this.SiteTemplateEntity = (SiteTemplateEntity)entity;
					break;
				case "StationEntity":
					_alreadyFetchedStationEntity = true;
					this.StationEntity = (StationEntity)entity;
					break;
				case "UIFooterItemEntity":
					_alreadyFetchedUIFooterItemEntity = true;
					this.UIFooterItemEntity = (UIFooterItemEntity)entity;
					break;
				case "UITabEntity":
					_alreadyFetchedUITabEntity = true;
					this.UITabEntity = (UITabEntity)entity;
					break;
				case "UIWidgetEntity":
					_alreadyFetchedUIWidgetEntity = true;
					this.UIWidgetEntity = (UIWidgetEntity)entity;
					break;
				case "VenueCategoryEntity":
					_alreadyFetchedVenueCategoryEntity = true;
					this.VenueCategoryEntity = (VenueCategoryEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ActionButtonEntity":
					SetupSyncActionButtonEntity(relatedEntity);
					break;
				case "AdvertisementEntity":
					SetupSyncAdvertisementEntity(relatedEntity);
					break;
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationoptionEntity":
					SetupSyncAlterationoptionEntity(relatedEntity);
					break;
				case "AmenityEntity":
					SetupSyncAmenityEntity(relatedEntity);
					break;
				case "AnnouncementEntity":
					SetupSyncAnnouncementEntity(relatedEntity);
					break;
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "CarouselItemEntity":
					SetupSyncCarouselItemEntity(relatedEntity);
					break;
				case "LandingPageEntity":
					SetupSyncLandingPageEntity(relatedEntity);
					break;
				case "NavigationMenuItemEntity":
					SetupSyncNavigationMenuItemEntity(relatedEntity);
					break;
				case "WidgetEntity":
					SetupSyncWidgetEntity(relatedEntity);
					break;
				case "AttachmentEntity":
					SetupSyncAttachmentEntity(relatedEntity);
					break;
				case "AvailabilityEntity":
					SetupSyncAvailabilityEntity(relatedEntity);
					break;
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "CheckoutMethodEntity":
					SetupSyncCheckoutMethodEntity(relatedEntity);
					break;
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "EntertainmentcategoryEntity":
					SetupSyncEntertainmentcategoryEntity(relatedEntity);
					break;
				case "GenericcategoryEntity":
					SetupSyncGenericcategoryEntity(relatedEntity);
					break;
				case "GenericproductEntity":
					SetupSyncGenericproductEntity(relatedEntity);
					break;
				case "InfraredCommandEntity":
					SetupSyncInfraredCommandEntity(relatedEntity);
					break;
				case "LanguageEntity":
					SetupSyncLanguageEntity(relatedEntity);
					break;
				case "OutletEntity":
					SetupSyncOutletEntity(relatedEntity);
					break;
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "PageTemplateEntity":
					SetupSyncPageTemplateEntity(relatedEntity);
					break;
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductgroupEntity":
					SetupSyncProductgroupEntity(relatedEntity);
					break;
				case "RoomControlAreaEntity":
					SetupSyncRoomControlAreaEntity(relatedEntity);
					break;
				case "RoomControlComponentEntity":
					SetupSyncRoomControlComponentEntity(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "RoomControlSectionItemEntity":
					SetupSyncRoomControlSectionItemEntity(relatedEntity);
					break;
				case "RoomControlWidgetEntity":
					SetupSyncRoomControlWidgetEntity(relatedEntity);
					break;
				case "RoutestephandlerEntity":
					SetupSyncRoutestephandlerEntity(relatedEntity);
					break;
				case "ScheduledMessageEntity":
					SetupSyncScheduledMessageEntity(relatedEntity);
					break;
				case "ServiceMethodEntity":
					SetupSyncServiceMethodEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "SiteTemplateEntity":
					SetupSyncSiteTemplateEntity(relatedEntity);
					break;
				case "StationEntity":
					SetupSyncStationEntity(relatedEntity);
					break;
				case "UIFooterItemEntity":
					SetupSyncUIFooterItemEntity(relatedEntity);
					break;
				case "UITabEntity":
					SetupSyncUITabEntity(relatedEntity);
					break;
				case "UIWidgetEntity":
					SetupSyncUIWidgetEntity(relatedEntity);
					break;
				case "VenueCategoryEntity":
					SetupSyncVenueCategoryEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ActionButtonEntity":
					DesetupSyncActionButtonEntity(false, true);
					break;
				case "AdvertisementEntity":
					DesetupSyncAdvertisementEntity(false, true);
					break;
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationoptionEntity":
					DesetupSyncAlterationoptionEntity(false, true);
					break;
				case "AmenityEntity":
					DesetupSyncAmenityEntity(false, true);
					break;
				case "AnnouncementEntity":
					DesetupSyncAnnouncementEntity(false, true);
					break;
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "CarouselItemEntity":
					DesetupSyncCarouselItemEntity(false, true);
					break;
				case "LandingPageEntity":
					DesetupSyncLandingPageEntity(false, true);
					break;
				case "NavigationMenuItemEntity":
					DesetupSyncNavigationMenuItemEntity(false, true);
					break;
				case "WidgetEntity":
					DesetupSyncWidgetEntity(false, true);
					break;
				case "AttachmentEntity":
					DesetupSyncAttachmentEntity(false, true);
					break;
				case "AvailabilityEntity":
					DesetupSyncAvailabilityEntity(false, true);
					break;
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "CheckoutMethodEntity":
					DesetupSyncCheckoutMethodEntity(false, true);
					break;
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "EntertainmentcategoryEntity":
					DesetupSyncEntertainmentcategoryEntity(false, true);
					break;
				case "GenericcategoryEntity":
					DesetupSyncGenericcategoryEntity(false, true);
					break;
				case "GenericproductEntity":
					DesetupSyncGenericproductEntity(false, true);
					break;
				case "InfraredCommandEntity":
					DesetupSyncInfraredCommandEntity(false, true);
					break;
				case "LanguageEntity":
					DesetupSyncLanguageEntity(false, true);
					break;
				case "OutletEntity":
					DesetupSyncOutletEntity(false, true);
					break;
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "PageTemplateEntity":
					DesetupSyncPageTemplateEntity(false, true);
					break;
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductgroupEntity":
					DesetupSyncProductgroupEntity(false, true);
					break;
				case "RoomControlAreaEntity":
					DesetupSyncRoomControlAreaEntity(false, true);
					break;
				case "RoomControlComponentEntity":
					DesetupSyncRoomControlComponentEntity(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "RoomControlSectionItemEntity":
					DesetupSyncRoomControlSectionItemEntity(false, true);
					break;
				case "RoomControlWidgetEntity":
					DesetupSyncRoomControlWidgetEntity(false, true);
					break;
				case "RoutestephandlerEntity":
					DesetupSyncRoutestephandlerEntity(false, true);
					break;
				case "ScheduledMessageEntity":
					DesetupSyncScheduledMessageEntity(false, true);
					break;
				case "ServiceMethodEntity":
					DesetupSyncServiceMethodEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "SiteTemplateEntity":
					DesetupSyncSiteTemplateEntity(false, true);
					break;
				case "StationEntity":
					DesetupSyncStationEntity(false, true);
					break;
				case "UIFooterItemEntity":
					DesetupSyncUIFooterItemEntity(false, true);
					break;
				case "UITabEntity":
					DesetupSyncUITabEntity(false, true);
					break;
				case "UIWidgetEntity":
					DesetupSyncUIWidgetEntity(false, true);
					break;
				case "VenueCategoryEntity":
					DesetupSyncVenueCategoryEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_actionButtonEntity!=null)
			{
				toReturn.Add(_actionButtonEntity);
			}
			if(_advertisementEntity!=null)
			{
				toReturn.Add(_advertisementEntity);
			}
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationoptionEntity!=null)
			{
				toReturn.Add(_alterationoptionEntity);
			}
			if(_amenityEntity!=null)
			{
				toReturn.Add(_amenityEntity);
			}
			if(_announcementEntity!=null)
			{
				toReturn.Add(_announcementEntity);
			}
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			if(_carouselItemEntity!=null)
			{
				toReturn.Add(_carouselItemEntity);
			}
			if(_landingPageEntity!=null)
			{
				toReturn.Add(_landingPageEntity);
			}
			if(_navigationMenuItemEntity!=null)
			{
				toReturn.Add(_navigationMenuItemEntity);
			}
			if(_widgetEntity!=null)
			{
				toReturn.Add(_widgetEntity);
			}
			if(_attachmentEntity!=null)
			{
				toReturn.Add(_attachmentEntity);
			}
			if(_availabilityEntity!=null)
			{
				toReturn.Add(_availabilityEntity);
			}
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_checkoutMethodEntity!=null)
			{
				toReturn.Add(_checkoutMethodEntity);
			}
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_entertainmentcategoryEntity!=null)
			{
				toReturn.Add(_entertainmentcategoryEntity);
			}
			if(_genericcategoryEntity!=null)
			{
				toReturn.Add(_genericcategoryEntity);
			}
			if(_genericproductEntity!=null)
			{
				toReturn.Add(_genericproductEntity);
			}
			if(_infraredCommandEntity!=null)
			{
				toReturn.Add(_infraredCommandEntity);
			}
			if(_languageEntity!=null)
			{
				toReturn.Add(_languageEntity);
			}
			if(_outletEntity!=null)
			{
				toReturn.Add(_outletEntity);
			}
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_pageTemplateEntity!=null)
			{
				toReturn.Add(_pageTemplateEntity);
			}
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productgroupEntity!=null)
			{
				toReturn.Add(_productgroupEntity);
			}
			if(_roomControlAreaEntity!=null)
			{
				toReturn.Add(_roomControlAreaEntity);
			}
			if(_roomControlComponentEntity!=null)
			{
				toReturn.Add(_roomControlComponentEntity);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			if(_roomControlSectionItemEntity!=null)
			{
				toReturn.Add(_roomControlSectionItemEntity);
			}
			if(_roomControlWidgetEntity!=null)
			{
				toReturn.Add(_roomControlWidgetEntity);
			}
			if(_routestephandlerEntity!=null)
			{
				toReturn.Add(_routestephandlerEntity);
			}
			if(_scheduledMessageEntity!=null)
			{
				toReturn.Add(_scheduledMessageEntity);
			}
			if(_serviceMethodEntity!=null)
			{
				toReturn.Add(_serviceMethodEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			if(_siteTemplateEntity!=null)
			{
				toReturn.Add(_siteTemplateEntity);
			}
			if(_stationEntity!=null)
			{
				toReturn.Add(_stationEntity);
			}
			if(_uIFooterItemEntity!=null)
			{
				toReturn.Add(_uIFooterItemEntity);
			}
			if(_uITabEntity!=null)
			{
				toReturn.Add(_uITabEntity);
			}
			if(_uIWidgetEntity!=null)
			{
				toReturn.Add(_uIWidgetEntity);
			}
			if(_venueCategoryEntity!=null)
			{
				toReturn.Add(_venueCategoryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customTextId)
		{
			return FetchUsingPK(customTextId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customTextId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customTextId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customTextId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customTextId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customTextId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customTextId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomTextId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomTextRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ActionButtonEntity' which is related to this entity.</returns>
		public ActionButtonEntity GetSingleActionButtonEntity()
		{
			return GetSingleActionButtonEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ActionButtonEntity' which is related to this entity.</returns>
		public virtual ActionButtonEntity GetSingleActionButtonEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionButtonEntity || forceFetch || _alwaysFetchActionButtonEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ActionButtonEntityUsingActionButtonId);
				ActionButtonEntity newEntity = new ActionButtonEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionButtonId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ActionButtonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionButtonEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionButtonEntity = newEntity;
				_alreadyFetchedActionButtonEntity = fetchResult;
			}
			return _actionButtonEntity;
		}


		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public AdvertisementEntity GetSingleAdvertisementEntity()
		{
			return GetSingleAdvertisementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public virtual AdvertisementEntity GetSingleAdvertisementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementEntity || forceFetch || _alwaysFetchAdvertisementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementEntityUsingAdvertisementId);
				AdvertisementEntity newEntity = new AdvertisementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementEntity = newEntity;
				_alreadyFetchedAdvertisementEntity = fetchResult;
			}
			return _advertisementEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public AlterationoptionEntity GetSingleAlterationoptionEntity()
		{
			return GetSingleAlterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public virtual AlterationoptionEntity GetSingleAlterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationoptionEntity || forceFetch || _alwaysFetchAlterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationoptionEntityUsingAlterationoptionId);
				AlterationoptionEntity newEntity = new AlterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationoptionEntity = newEntity;
				_alreadyFetchedAlterationoptionEntity = fetchResult;
			}
			return _alterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'AmenityEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AmenityEntity' which is related to this entity.</returns>
		public AmenityEntity GetSingleAmenityEntity()
		{
			return GetSingleAmenityEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AmenityEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AmenityEntity' which is related to this entity.</returns>
		public virtual AmenityEntity GetSingleAmenityEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAmenityEntity || forceFetch || _alwaysFetchAmenityEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AmenityEntityUsingAmenityId);
				AmenityEntity newEntity = new AmenityEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AmenityId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AmenityEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_amenityEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AmenityEntity = newEntity;
				_alreadyFetchedAmenityEntity = fetchResult;
			}
			return _amenityEntity;
		}


		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public AnnouncementEntity GetSingleAnnouncementEntity()
		{
			return GetSingleAnnouncementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public virtual AnnouncementEntity GetSingleAnnouncementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAnnouncementEntity || forceFetch || _alwaysFetchAnnouncementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AnnouncementEntityUsingAnnouncementId);
				AnnouncementEntity newEntity = new AnnouncementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AnnouncementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AnnouncementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_announcementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AnnouncementEntity = newEntity;
				_alreadyFetchedAnnouncementEntity = fetchResult;
			}
			return _announcementEntity;
		}


		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CarouselItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CarouselItemEntity' which is related to this entity.</returns>
		public CarouselItemEntity GetSingleCarouselItemEntity()
		{
			return GetSingleCarouselItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CarouselItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CarouselItemEntity' which is related to this entity.</returns>
		public virtual CarouselItemEntity GetSingleCarouselItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCarouselItemEntity || forceFetch || _alwaysFetchCarouselItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CarouselItemEntityUsingCarouselItemId);
				CarouselItemEntity newEntity = new CarouselItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CarouselItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CarouselItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_carouselItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CarouselItemEntity = newEntity;
				_alreadyFetchedCarouselItemEntity = fetchResult;
			}
			return _carouselItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public LandingPageEntity GetSingleLandingPageEntity()
		{
			return GetSingleLandingPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public virtual LandingPageEntity GetSingleLandingPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLandingPageEntity || forceFetch || _alwaysFetchLandingPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LandingPageEntityUsingLandingPageId);
				LandingPageEntity newEntity = new LandingPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LandingPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LandingPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_landingPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LandingPageEntity = newEntity;
				_alreadyFetchedLandingPageEntity = fetchResult;
			}
			return _landingPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'NavigationMenuItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NavigationMenuItemEntity' which is related to this entity.</returns>
		public NavigationMenuItemEntity GetSingleNavigationMenuItemEntity()
		{
			return GetSingleNavigationMenuItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'NavigationMenuItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NavigationMenuItemEntity' which is related to this entity.</returns>
		public virtual NavigationMenuItemEntity GetSingleNavigationMenuItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedNavigationMenuItemEntity || forceFetch || _alwaysFetchNavigationMenuItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NavigationMenuItemEntityUsingNavigationMenuItemId);
				NavigationMenuItemEntity newEntity = new NavigationMenuItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NavigationMenuItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (NavigationMenuItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_navigationMenuItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NavigationMenuItemEntity = newEntity;
				_alreadyFetchedNavigationMenuItemEntity = fetchResult;
			}
			return _navigationMenuItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public WidgetEntity GetSingleWidgetEntity()
		{
			return GetSingleWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public virtual WidgetEntity GetSingleWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetEntity || forceFetch || _alwaysFetchWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetEntityUsingWidgetId);
				WidgetEntity newEntity = (WidgetEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetEntity.FetchPolymorphic(this.Transaction, this.WidgetId.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetEntity = newEntity;
				_alreadyFetchedWidgetEntity = fetchResult;
			}
			return _widgetEntity;
		}


		/// <summary> Retrieves the related entity of type 'AttachmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttachmentEntity' which is related to this entity.</returns>
		public AttachmentEntity GetSingleAttachmentEntity()
		{
			return GetSingleAttachmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AttachmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttachmentEntity' which is related to this entity.</returns>
		public virtual AttachmentEntity GetSingleAttachmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttachmentEntity || forceFetch || _alwaysFetchAttachmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttachmentEntityUsingAttachmentId);
				AttachmentEntity newEntity = new AttachmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttachmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttachmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attachmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttachmentEntity = newEntity;
				_alreadyFetchedAttachmentEntity = fetchResult;
			}
			return _attachmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'AvailabilityEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AvailabilityEntity' which is related to this entity.</returns>
		public AvailabilityEntity GetSingleAvailabilityEntity()
		{
			return GetSingleAvailabilityEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AvailabilityEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AvailabilityEntity' which is related to this entity.</returns>
		public virtual AvailabilityEntity GetSingleAvailabilityEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAvailabilityEntity || forceFetch || _alwaysFetchAvailabilityEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AvailabilityEntityUsingAvailabilityId);
				AvailabilityEntity newEntity = new AvailabilityEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AvailabilityId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AvailabilityEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_availabilityEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AvailabilityEntity = newEntity;
				_alreadyFetchedAvailabilityEntity = fetchResult;
			}
			return _availabilityEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CheckoutMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CheckoutMethodEntity' which is related to this entity.</returns>
		public CheckoutMethodEntity GetSingleCheckoutMethodEntity()
		{
			return GetSingleCheckoutMethodEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CheckoutMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CheckoutMethodEntity' which is related to this entity.</returns>
		public virtual CheckoutMethodEntity GetSingleCheckoutMethodEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCheckoutMethodEntity || forceFetch || _alwaysFetchCheckoutMethodEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CheckoutMethodEntityUsingCheckoutMethodId);
				CheckoutMethodEntity newEntity = new CheckoutMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CheckoutMethodId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CheckoutMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_checkoutMethodEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CheckoutMethodEntity = newEntity;
				_alreadyFetchedCheckoutMethodEntity = fetchResult;
			}
			return _checkoutMethodEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity()
		{
			return GetSingleEntertainmentcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentcategoryEntity || forceFetch || _alwaysFetchEntertainmentcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentcategoryEntity = newEntity;
				_alreadyFetchedEntertainmentcategoryEntity = fetchResult;
			}
			return _entertainmentcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public GenericcategoryEntity GetSingleGenericcategoryEntity()
		{
			return GetSingleGenericcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public virtual GenericcategoryEntity GetSingleGenericcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericcategoryEntity || forceFetch || _alwaysFetchGenericcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericcategoryEntityUsingGenericcategoryId);
				GenericcategoryEntity newEntity = new GenericcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericcategoryEntity = newEntity;
				_alreadyFetchedGenericcategoryEntity = fetchResult;
			}
			return _genericcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericproductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericproductEntity' which is related to this entity.</returns>
		public GenericproductEntity GetSingleGenericproductEntity()
		{
			return GetSingleGenericproductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericproductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericproductEntity' which is related to this entity.</returns>
		public virtual GenericproductEntity GetSingleGenericproductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericproductEntity || forceFetch || _alwaysFetchGenericproductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericproductEntityUsingGenericproductId);
				GenericproductEntity newEntity = new GenericproductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericproductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericproductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericproductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericproductEntity = newEntity;
				_alreadyFetchedGenericproductEntity = fetchResult;
			}
			return _genericproductEntity;
		}


		/// <summary> Retrieves the related entity of type 'InfraredCommandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InfraredCommandEntity' which is related to this entity.</returns>
		public InfraredCommandEntity GetSingleInfraredCommandEntity()
		{
			return GetSingleInfraredCommandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'InfraredCommandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InfraredCommandEntity' which is related to this entity.</returns>
		public virtual InfraredCommandEntity GetSingleInfraredCommandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedInfraredCommandEntity || forceFetch || _alwaysFetchInfraredCommandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InfraredCommandEntityUsingInfraredCommandId);
				InfraredCommandEntity newEntity = new InfraredCommandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InfraredCommandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InfraredCommandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_infraredCommandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InfraredCommandEntity = newEntity;
				_alreadyFetchedInfraredCommandEntity = fetchResult;
			}
			return _infraredCommandEntity;
		}


		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguageEntity()
		{
			return GetSingleLanguageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguageEntity || forceFetch || _alwaysFetchLanguageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageId);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LanguageEntity = newEntity;
				_alreadyFetchedLanguageEntity = fetchResult;
			}
			return _languageEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public OutletEntity GetSingleOutletEntity()
		{
			return GetSingleOutletEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public virtual OutletEntity GetSingleOutletEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletEntity || forceFetch || _alwaysFetchOutletEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletEntityUsingOutletId);
				OutletEntity newEntity = new OutletEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletEntity = newEntity;
				_alreadyFetchedOutletEntity = fetchResult;
			}
			return _outletEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public PageTemplateEntity GetSinglePageTemplateEntity()
		{
			return GetSinglePageTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public virtual PageTemplateEntity GetSinglePageTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageTemplateEntity || forceFetch || _alwaysFetchPageTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageTemplateEntityUsingPageTemplateId);
				PageTemplateEntity newEntity = new PageTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageTemplateEntity = newEntity;
				_alreadyFetchedPageTemplateEntity = fetchResult;
			}
			return _pageTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public ProductgroupEntity GetSingleProductgroupEntity()
		{
			return GetSingleProductgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public virtual ProductgroupEntity GetSingleProductgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductgroupEntity || forceFetch || _alwaysFetchProductgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductgroupEntityUsingProductgroupId);
				ProductgroupEntity newEntity = new ProductgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductgroupEntity = newEntity;
				_alreadyFetchedProductgroupEntity = fetchResult;
			}
			return _productgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public RoomControlAreaEntity GetSingleRoomControlAreaEntity()
		{
			return GetSingleRoomControlAreaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public virtual RoomControlAreaEntity GetSingleRoomControlAreaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlAreaEntity || forceFetch || _alwaysFetchRoomControlAreaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
				RoomControlAreaEntity newEntity = new RoomControlAreaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlAreaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlAreaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlAreaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlAreaEntity = newEntity;
				_alreadyFetchedRoomControlAreaEntity = fetchResult;
			}
			return _roomControlAreaEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity()
		{
			return GetSingleRoomControlComponentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity || forceFetch || _alwaysFetchRoomControlComponentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity = newEntity;
				_alreadyFetchedRoomControlComponentEntity = fetchResult;
			}
			return _roomControlComponentEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity()
		{
			return GetSingleRoomControlSectionItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionItemEntity || forceFetch || _alwaysFetchRoomControlSectionItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
				RoomControlSectionItemEntity newEntity = new RoomControlSectionItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionItemEntity = newEntity;
				_alreadyFetchedRoomControlSectionItemEntity = fetchResult;
			}
			return _roomControlSectionItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlWidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlWidgetEntity' which is related to this entity.</returns>
		public RoomControlWidgetEntity GetSingleRoomControlWidgetEntity()
		{
			return GetSingleRoomControlWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlWidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlWidgetEntity' which is related to this entity.</returns>
		public virtual RoomControlWidgetEntity GetSingleRoomControlWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlWidgetEntity || forceFetch || _alwaysFetchRoomControlWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlWidgetEntityUsingRoomControlWidgetId);
				RoomControlWidgetEntity newEntity = new RoomControlWidgetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlWidgetId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlWidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlWidgetEntity = newEntity;
				_alreadyFetchedRoomControlWidgetEntity = fetchResult;
			}
			return _roomControlWidgetEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoutestephandlerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoutestephandlerEntity' which is related to this entity.</returns>
		public RoutestephandlerEntity GetSingleRoutestephandlerEntity()
		{
			return GetSingleRoutestephandlerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoutestephandlerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoutestephandlerEntity' which is related to this entity.</returns>
		public virtual RoutestephandlerEntity GetSingleRoutestephandlerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoutestephandlerEntity || forceFetch || _alwaysFetchRoutestephandlerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoutestephandlerEntityUsingRoutestephandlerId);
				RoutestephandlerEntity newEntity = new RoutestephandlerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoutestephandlerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoutestephandlerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routestephandlerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoutestephandlerEntity = newEntity;
				_alreadyFetchedRoutestephandlerEntity = fetchResult;
			}
			return _routestephandlerEntity;
		}


		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public ScheduledMessageEntity GetSingleScheduledMessageEntity()
		{
			return GetSingleScheduledMessageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public virtual ScheduledMessageEntity GetSingleScheduledMessageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduledMessageEntity || forceFetch || _alwaysFetchScheduledMessageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduledMessageEntityUsingScheduledMessageId);
				ScheduledMessageEntity newEntity = new ScheduledMessageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduledMessageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ScheduledMessageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduledMessageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduledMessageEntity = newEntity;
				_alreadyFetchedScheduledMessageEntity = fetchResult;
			}
			return _scheduledMessageEntity;
		}


		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public ServiceMethodEntity GetSingleServiceMethodEntity()
		{
			return GetSingleServiceMethodEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public virtual ServiceMethodEntity GetSingleServiceMethodEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedServiceMethodEntity || forceFetch || _alwaysFetchServiceMethodEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ServiceMethodEntityUsingServiceMethodId);
				ServiceMethodEntity newEntity = new ServiceMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceMethodId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ServiceMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_serviceMethodEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ServiceMethodEntity = newEntity;
				_alreadyFetchedServiceMethodEntity = fetchResult;
			}
			return _serviceMethodEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public SiteTemplateEntity GetSingleSiteTemplateEntity()
		{
			return GetSingleSiteTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteTemplateEntity' which is related to this entity.</returns>
		public virtual SiteTemplateEntity GetSingleSiteTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteTemplateEntity || forceFetch || _alwaysFetchSiteTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteTemplateEntityUsingSiteTemplateId);
				SiteTemplateEntity newEntity = new SiteTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteTemplateEntity = newEntity;
				_alreadyFetchedSiteTemplateEntity = fetchResult;
			}
			return _siteTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'StationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StationEntity' which is related to this entity.</returns>
		public StationEntity GetSingleStationEntity()
		{
			return GetSingleStationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'StationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StationEntity' which is related to this entity.</returns>
		public virtual StationEntity GetSingleStationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedStationEntity || forceFetch || _alwaysFetchStationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StationEntityUsingStationId);
				StationEntity newEntity = new StationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (StationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_stationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.StationEntity = newEntity;
				_alreadyFetchedStationEntity = fetchResult;
			}
			return _stationEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIFooterItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIFooterItemEntity' which is related to this entity.</returns>
		public UIFooterItemEntity GetSingleUIFooterItemEntity()
		{
			return GetSingleUIFooterItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIFooterItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIFooterItemEntity' which is related to this entity.</returns>
		public virtual UIFooterItemEntity GetSingleUIFooterItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIFooterItemEntity || forceFetch || _alwaysFetchUIFooterItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIFooterItemEntityUsingUIFooterItemId);
				UIFooterItemEntity newEntity = new UIFooterItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIFooterItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIFooterItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIFooterItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIFooterItemEntity = newEntity;
				_alreadyFetchedUIFooterItemEntity = fetchResult;
			}
			return _uIFooterItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public UITabEntity GetSingleUITabEntity()
		{
			return GetSingleUITabEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UITabEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UITabEntity' which is related to this entity.</returns>
		public virtual UITabEntity GetSingleUITabEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUITabEntity || forceFetch || _alwaysFetchUITabEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UITabEntityUsingUITabId);
				UITabEntity newEntity = new UITabEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UITabId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UITabEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uITabEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UITabEntity = newEntity;
				_alreadyFetchedUITabEntity = fetchResult;
			}
			return _uITabEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public UIWidgetEntity GetSingleUIWidgetEntity()
		{
			return GetSingleUIWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public virtual UIWidgetEntity GetSingleUIWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIWidgetEntity || forceFetch || _alwaysFetchUIWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIWidgetEntityUsingUIWidgetId);
				UIWidgetEntity newEntity = new UIWidgetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIWidgetId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIWidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIWidgetEntity = newEntity;
				_alreadyFetchedUIWidgetEntity = fetchResult;
			}
			return _uIWidgetEntity;
		}


		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public VenueCategoryEntity GetSingleVenueCategoryEntity()
		{
			return GetSingleVenueCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'VenueCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VenueCategoryEntity' which is related to this entity.</returns>
		public virtual VenueCategoryEntity GetSingleVenueCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedVenueCategoryEntity || forceFetch || _alwaysFetchVenueCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VenueCategoryEntityUsingVenueCategoryId);
				VenueCategoryEntity newEntity = new VenueCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VenueCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VenueCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_venueCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VenueCategoryEntity = newEntity;
				_alreadyFetchedVenueCategoryEntity = fetchResult;
			}
			return _venueCategoryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ActionButtonEntity", _actionButtonEntity);
			toReturn.Add("AdvertisementEntity", _advertisementEntity);
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationoptionEntity", _alterationoptionEntity);
			toReturn.Add("AmenityEntity", _amenityEntity);
			toReturn.Add("AnnouncementEntity", _announcementEntity);
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("CarouselItemEntity", _carouselItemEntity);
			toReturn.Add("LandingPageEntity", _landingPageEntity);
			toReturn.Add("NavigationMenuItemEntity", _navigationMenuItemEntity);
			toReturn.Add("WidgetEntity", _widgetEntity);
			toReturn.Add("AttachmentEntity", _attachmentEntity);
			toReturn.Add("AvailabilityEntity", _availabilityEntity);
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("CheckoutMethodEntity", _checkoutMethodEntity);
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("EntertainmentcategoryEntity", _entertainmentcategoryEntity);
			toReturn.Add("GenericcategoryEntity", _genericcategoryEntity);
			toReturn.Add("GenericproductEntity", _genericproductEntity);
			toReturn.Add("InfraredCommandEntity", _infraredCommandEntity);
			toReturn.Add("LanguageEntity", _languageEntity);
			toReturn.Add("OutletEntity", _outletEntity);
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("PageTemplateEntity", _pageTemplateEntity);
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductgroupEntity", _productgroupEntity);
			toReturn.Add("RoomControlAreaEntity", _roomControlAreaEntity);
			toReturn.Add("RoomControlComponentEntity", _roomControlComponentEntity);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("RoomControlSectionItemEntity", _roomControlSectionItemEntity);
			toReturn.Add("RoomControlWidgetEntity", _roomControlWidgetEntity);
			toReturn.Add("RoutestephandlerEntity", _routestephandlerEntity);
			toReturn.Add("ScheduledMessageEntity", _scheduledMessageEntity);
			toReturn.Add("ServiceMethodEntity", _serviceMethodEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("SiteTemplateEntity", _siteTemplateEntity);
			toReturn.Add("StationEntity", _stationEntity);
			toReturn.Add("UIFooterItemEntity", _uIFooterItemEntity);
			toReturn.Add("UITabEntity", _uITabEntity);
			toReturn.Add("UIWidgetEntity", _uIWidgetEntity);
			toReturn.Add("VenueCategoryEntity", _venueCategoryEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="validator">The validator object for this CustomTextEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 customTextId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customTextId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_actionButtonEntityReturnsNewIfNotFound = true;
			_advertisementEntityReturnsNewIfNotFound = true;
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationoptionEntityReturnsNewIfNotFound = true;
			_amenityEntityReturnsNewIfNotFound = true;
			_announcementEntityReturnsNewIfNotFound = true;
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			_carouselItemEntityReturnsNewIfNotFound = true;
			_landingPageEntityReturnsNewIfNotFound = true;
			_navigationMenuItemEntityReturnsNewIfNotFound = true;
			_widgetEntityReturnsNewIfNotFound = true;
			_attachmentEntityReturnsNewIfNotFound = true;
			_availabilityEntityReturnsNewIfNotFound = true;
			_categoryEntityReturnsNewIfNotFound = true;
			_checkoutMethodEntityReturnsNewIfNotFound = true;
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_entertainmentcategoryEntityReturnsNewIfNotFound = true;
			_genericcategoryEntityReturnsNewIfNotFound = true;
			_genericproductEntityReturnsNewIfNotFound = true;
			_infraredCommandEntityReturnsNewIfNotFound = true;
			_languageEntityReturnsNewIfNotFound = true;
			_outletEntityReturnsNewIfNotFound = true;
			_pageEntityReturnsNewIfNotFound = true;
			_pageTemplateEntityReturnsNewIfNotFound = true;
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_productgroupEntityReturnsNewIfNotFound = true;
			_roomControlAreaEntityReturnsNewIfNotFound = true;
			_roomControlComponentEntityReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			_roomControlSectionItemEntityReturnsNewIfNotFound = true;
			_roomControlWidgetEntityReturnsNewIfNotFound = true;
			_routestephandlerEntityReturnsNewIfNotFound = true;
			_scheduledMessageEntityReturnsNewIfNotFound = true;
			_serviceMethodEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			_siteTemplateEntityReturnsNewIfNotFound = true;
			_stationEntityReturnsNewIfNotFound = true;
			_uIFooterItemEntityReturnsNewIfNotFound = true;
			_uITabEntityReturnsNewIfNotFound = true;
			_uIWidgetEntityReturnsNewIfNotFound = true;
			_venueCategoryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomTextId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnnouncementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericproductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmenityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttachmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VenueCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduledMessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIFooterItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UITabId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailabilityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoutestephandlerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredCommandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationMenuItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CarouselItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LandingPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _actionButtonEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionButtonEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionButtonEntity, new PropertyChangedEventHandler( OnActionButtonEntityPropertyChanged ), "ActionButtonEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ActionButtonEntityUsingActionButtonIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ActionButtonId } );		
			_actionButtonEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionButtonEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionButtonEntity(IEntityCore relatedEntity)
		{
			if(_actionButtonEntity!=relatedEntity)
			{		
				DesetupSyncActionButtonEntity(true, true);
				_actionButtonEntity = (ActionButtonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionButtonEntity, new PropertyChangedEventHandler( OnActionButtonEntityPropertyChanged ), "ActionButtonEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ActionButtonEntityUsingActionButtonIdStatic, true, ref _alreadyFetchedActionButtonEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionButtonEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _advertisementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AdvertisementId } );		
			_advertisementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementEntity(IEntityCore relatedEntity)
		{
			if(_advertisementEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementEntity(true, true);
				_advertisementEntity = (AdvertisementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, ref _alreadyFetchedAdvertisementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AlterationoptionId } );		
			_alterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_alterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncAlterationoptionEntity(true, true);
				_alterationoptionEntity = (AlterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, ref _alreadyFetchedAlterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _amenityEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAmenityEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _amenityEntity, new PropertyChangedEventHandler( OnAmenityEntityPropertyChanged ), "AmenityEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AmenityEntityUsingAmenityIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AmenityId } );		
			_amenityEntity = null;
		}
		
		/// <summary> setups the sync logic for member _amenityEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAmenityEntity(IEntityCore relatedEntity)
		{
			if(_amenityEntity!=relatedEntity)
			{		
				DesetupSyncAmenityEntity(true, true);
				_amenityEntity = (AmenityEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _amenityEntity, new PropertyChangedEventHandler( OnAmenityEntityPropertyChanged ), "AmenityEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AmenityEntityUsingAmenityIdStatic, true, ref _alreadyFetchedAmenityEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAmenityEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _announcementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAnnouncementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AnnouncementEntityUsingAnnouncementIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AnnouncementId } );		
			_announcementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _announcementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAnnouncementEntity(IEntityCore relatedEntity)
		{
			if(_announcementEntity!=relatedEntity)
			{		
				DesetupSyncAnnouncementEntity(true, true);
				_announcementEntity = (AnnouncementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AnnouncementEntityUsingAnnouncementIdStatic, true, ref _alreadyFetchedAnnouncementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAnnouncementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _carouselItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCarouselItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _carouselItemEntity, new PropertyChangedEventHandler( OnCarouselItemEntityPropertyChanged ), "CarouselItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CarouselItemEntityUsingCarouselItemIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.CarouselItemId } );		
			_carouselItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _carouselItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCarouselItemEntity(IEntityCore relatedEntity)
		{
			if(_carouselItemEntity!=relatedEntity)
			{		
				DesetupSyncCarouselItemEntity(true, true);
				_carouselItemEntity = (CarouselItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _carouselItemEntity, new PropertyChangedEventHandler( OnCarouselItemEntityPropertyChanged ), "CarouselItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CarouselItemEntityUsingCarouselItemIdStatic, true, ref _alreadyFetchedCarouselItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCarouselItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _landingPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLandingPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.LandingPageEntityUsingLandingPageIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.LandingPageId } );		
			_landingPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _landingPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLandingPageEntity(IEntityCore relatedEntity)
		{
			if(_landingPageEntity!=relatedEntity)
			{		
				DesetupSyncLandingPageEntity(true, true);
				_landingPageEntity = (LandingPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.LandingPageEntityUsingLandingPageIdStatic, true, ref _alreadyFetchedLandingPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLandingPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _navigationMenuItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNavigationMenuItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _navigationMenuItemEntity, new PropertyChangedEventHandler( OnNavigationMenuItemEntityPropertyChanged ), "NavigationMenuItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.NavigationMenuItemEntityUsingNavigationMenuItemIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.NavigationMenuItemId } );		
			_navigationMenuItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _navigationMenuItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNavigationMenuItemEntity(IEntityCore relatedEntity)
		{
			if(_navigationMenuItemEntity!=relatedEntity)
			{		
				DesetupSyncNavigationMenuItemEntity(true, true);
				_navigationMenuItemEntity = (NavigationMenuItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _navigationMenuItemEntity, new PropertyChangedEventHandler( OnNavigationMenuItemEntityPropertyChanged ), "NavigationMenuItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.NavigationMenuItemEntityUsingNavigationMenuItemIdStatic, true, ref _alreadyFetchedNavigationMenuItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNavigationMenuItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.WidgetEntityUsingWidgetIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.WidgetId } );		
			_widgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetEntity(IEntityCore relatedEntity)
		{
			if(_widgetEntity!=relatedEntity)
			{		
				DesetupSyncWidgetEntity(true, true);
				_widgetEntity = (WidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.WidgetEntityUsingWidgetIdStatic, true, ref _alreadyFetchedWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attachmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttachmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attachmentEntity, new PropertyChangedEventHandler( OnAttachmentEntityPropertyChanged ), "AttachmentEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AttachmentEntityUsingAttachmentIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AttachmentId } );		
			_attachmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _attachmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttachmentEntity(IEntityCore relatedEntity)
		{
			if(_attachmentEntity!=relatedEntity)
			{		
				DesetupSyncAttachmentEntity(true, true);
				_attachmentEntity = (AttachmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attachmentEntity, new PropertyChangedEventHandler( OnAttachmentEntityPropertyChanged ), "AttachmentEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AttachmentEntityUsingAttachmentIdStatic, true, ref _alreadyFetchedAttachmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttachmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _availabilityEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAvailabilityEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _availabilityEntity, new PropertyChangedEventHandler( OnAvailabilityEntityPropertyChanged ), "AvailabilityEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AvailabilityEntityUsingAvailabilityIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.AvailabilityId } );		
			_availabilityEntity = null;
		}
		
		/// <summary> setups the sync logic for member _availabilityEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAvailabilityEntity(IEntityCore relatedEntity)
		{
			if(_availabilityEntity!=relatedEntity)
			{		
				DesetupSyncAvailabilityEntity(true, true);
				_availabilityEntity = (AvailabilityEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _availabilityEntity, new PropertyChangedEventHandler( OnAvailabilityEntityPropertyChanged ), "AvailabilityEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.AvailabilityEntityUsingAvailabilityIdStatic, true, ref _alreadyFetchedAvailabilityEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAvailabilityEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _checkoutMethodEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCheckoutMethodEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _checkoutMethodEntity, new PropertyChangedEventHandler( OnCheckoutMethodEntityPropertyChanged ), "CheckoutMethodEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.CheckoutMethodId } );		
			_checkoutMethodEntity = null;
		}
		
		/// <summary> setups the sync logic for member _checkoutMethodEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCheckoutMethodEntity(IEntityCore relatedEntity)
		{
			if(_checkoutMethodEntity!=relatedEntity)
			{		
				DesetupSyncCheckoutMethodEntity(true, true);
				_checkoutMethodEntity = (CheckoutMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _checkoutMethodEntity, new PropertyChangedEventHandler( OnCheckoutMethodEntityPropertyChanged ), "CheckoutMethodEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, true, ref _alreadyFetchedCheckoutMethodEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCheckoutMethodEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.EntertainmentcategoryId } );		
			_entertainmentcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentcategoryEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentcategoryEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentcategoryEntity(true, true);
				_entertainmentcategoryEntity = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, ref _alreadyFetchedEntertainmentcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.GenericcategoryId } );		
			_genericcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericcategoryEntity(IEntityCore relatedEntity)
		{
			if(_genericcategoryEntity!=relatedEntity)
			{		
				DesetupSyncGenericcategoryEntity(true, true);
				_genericcategoryEntity = (GenericcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, ref _alreadyFetchedGenericcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericproductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericproductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericproductEntity, new PropertyChangedEventHandler( OnGenericproductEntityPropertyChanged ), "GenericproductEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.GenericproductEntityUsingGenericproductIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.GenericproductId } );		
			_genericproductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericproductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericproductEntity(IEntityCore relatedEntity)
		{
			if(_genericproductEntity!=relatedEntity)
			{		
				DesetupSyncGenericproductEntity(true, true);
				_genericproductEntity = (GenericproductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericproductEntity, new PropertyChangedEventHandler( OnGenericproductEntityPropertyChanged ), "GenericproductEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.GenericproductEntityUsingGenericproductIdStatic, true, ref _alreadyFetchedGenericproductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericproductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _infraredCommandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInfraredCommandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _infraredCommandEntity, new PropertyChangedEventHandler( OnInfraredCommandEntityPropertyChanged ), "InfraredCommandEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.InfraredCommandEntityUsingInfraredCommandIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.InfraredCommandId } );		
			_infraredCommandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _infraredCommandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInfraredCommandEntity(IEntityCore relatedEntity)
		{
			if(_infraredCommandEntity!=relatedEntity)
			{		
				DesetupSyncInfraredCommandEntity(true, true);
				_infraredCommandEntity = (InfraredCommandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _infraredCommandEntity, new PropertyChangedEventHandler( OnInfraredCommandEntityPropertyChanged ), "InfraredCommandEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.InfraredCommandEntityUsingInfraredCommandIdStatic, true, ref _alreadyFetchedInfraredCommandEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfraredCommandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _languageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.LanguageEntityUsingLanguageIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.LanguageId } );		
			_languageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _languageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguageEntity(IEntityCore relatedEntity)
		{
			if(_languageEntity!=relatedEntity)
			{		
				DesetupSyncLanguageEntity(true, true);
				_languageEntity = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.LanguageEntityUsingLanguageIdStatic, true, ref _alreadyFetchedLanguageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.OutletEntityUsingOutletIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.OutletId } );		
			_outletEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletEntity(IEntityCore relatedEntity)
		{
			if(_outletEntity!=relatedEntity)
			{		
				DesetupSyncOutletEntity(true, true);
				_outletEntity = (OutletEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.OutletEntityUsingOutletIdStatic, true, ref _alreadyFetchedOutletEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PageEntityUsingPageIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.PageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PageEntityUsingPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.PageTemplateId } );		
			_pageTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageTemplateEntity(IEntityCore relatedEntity)
		{
			if(_pageTemplateEntity!=relatedEntity)
			{		
				DesetupSyncPageTemplateEntity(true, true);
				_pageTemplateEntity = (PageTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, ref _alreadyFetchedPageTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.PointOfInterestId } );		
			_pointOfInterestEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{		
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ProductgroupEntityUsingProductgroupIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ProductgroupId } );		
			_productgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductgroupEntity(IEntityCore relatedEntity)
		{
			if(_productgroupEntity!=relatedEntity)
			{		
				DesetupSyncProductgroupEntity(true, true);
				_productgroupEntity = (ProductgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ProductgroupEntityUsingProductgroupIdStatic, true, ref _alreadyFetchedProductgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlAreaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoomControlAreaId } );		
			_roomControlAreaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlAreaEntity(IEntityCore relatedEntity)
		{
			if(_roomControlAreaEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlAreaEntity(true, true);
				_roomControlAreaEntity = (RoomControlAreaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, ref _alreadyFetchedRoomControlAreaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlAreaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity, new PropertyChangedEventHandler( OnRoomControlComponentEntityPropertyChanged ), "RoomControlComponentEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlComponentEntityUsingRoomControlComponentIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoomControlComponentId } );		
			_roomControlComponentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity(true, true);
				_roomControlComponentEntity = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity, new PropertyChangedEventHandler( OnRoomControlComponentEntityPropertyChanged ), "RoomControlComponentEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlComponentEntityUsingRoomControlComponentIdStatic, true, ref _alreadyFetchedRoomControlComponentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoomControlSectionItemId } );		
			_roomControlSectionItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionItemEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionItemEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionItemEntity(true, true);
				_roomControlSectionItemEntity = (RoomControlSectionItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, ref _alreadyFetchedRoomControlSectionItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlWidgetEntity, new PropertyChangedEventHandler( OnRoomControlWidgetEntityPropertyChanged ), "RoomControlWidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlWidgetEntityUsingRoomControlWidgetIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoomControlWidgetId } );		
			_roomControlWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlWidgetEntity(IEntityCore relatedEntity)
		{
			if(_roomControlWidgetEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlWidgetEntity(true, true);
				_roomControlWidgetEntity = (RoomControlWidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlWidgetEntity, new PropertyChangedEventHandler( OnRoomControlWidgetEntityPropertyChanged ), "RoomControlWidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoomControlWidgetEntityUsingRoomControlWidgetIdStatic, true, ref _alreadyFetchedRoomControlWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routestephandlerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoutestephandlerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routestephandlerEntity, new PropertyChangedEventHandler( OnRoutestephandlerEntityPropertyChanged ), "RoutestephandlerEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoutestephandlerEntityUsingRoutestephandlerIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.RoutestephandlerId } );		
			_routestephandlerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routestephandlerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoutestephandlerEntity(IEntityCore relatedEntity)
		{
			if(_routestephandlerEntity!=relatedEntity)
			{		
				DesetupSyncRoutestephandlerEntity(true, true);
				_routestephandlerEntity = (RoutestephandlerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routestephandlerEntity, new PropertyChangedEventHandler( OnRoutestephandlerEntityPropertyChanged ), "RoutestephandlerEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.RoutestephandlerEntityUsingRoutestephandlerIdStatic, true, ref _alreadyFetchedRoutestephandlerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoutestephandlerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduledMessageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ScheduledMessageId } );		
			_scheduledMessageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduledMessageEntity(IEntityCore relatedEntity)
		{
			if(_scheduledMessageEntity!=relatedEntity)
			{		
				DesetupSyncScheduledMessageEntity(true, true);
				_scheduledMessageEntity = (ScheduledMessageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, ref _alreadyFetchedScheduledMessageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduledMessageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _serviceMethodEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncServiceMethodEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.ServiceMethodId } );		
			_serviceMethodEntity = null;
		}
		
		/// <summary> setups the sync logic for member _serviceMethodEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncServiceMethodEntity(IEntityCore relatedEntity)
		{
			if(_serviceMethodEntity!=relatedEntity)
			{		
				DesetupSyncServiceMethodEntity(true, true);
				_serviceMethodEntity = (ServiceMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, ref _alreadyFetchedServiceMethodEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnServiceMethodEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.SiteTemplateId } );		
			_siteTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteTemplateEntity(IEntityCore relatedEntity)
		{
			if(_siteTemplateEntity!=relatedEntity)
			{		
				DesetupSyncSiteTemplateEntity(true, true);
				_siteTemplateEntity = (SiteTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteTemplateEntity, new PropertyChangedEventHandler( OnSiteTemplateEntityPropertyChanged ), "SiteTemplateEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.SiteTemplateEntityUsingSiteTemplateIdStatic, true, ref _alreadyFetchedSiteTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _stationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncStationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _stationEntity, new PropertyChangedEventHandler( OnStationEntityPropertyChanged ), "StationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.StationEntityUsingStationIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.StationId } );		
			_stationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _stationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncStationEntity(IEntityCore relatedEntity)
		{
			if(_stationEntity!=relatedEntity)
			{		
				DesetupSyncStationEntity(true, true);
				_stationEntity = (StationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _stationEntity, new PropertyChangedEventHandler( OnStationEntityPropertyChanged ), "StationEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.StationEntityUsingStationIdStatic, true, ref _alreadyFetchedStationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIFooterItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIFooterItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIFooterItemEntity, new PropertyChangedEventHandler( OnUIFooterItemEntityPropertyChanged ), "UIFooterItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UIFooterItemEntityUsingUIFooterItemIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.UIFooterItemId } );		
			_uIFooterItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIFooterItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIFooterItemEntity(IEntityCore relatedEntity)
		{
			if(_uIFooterItemEntity!=relatedEntity)
			{		
				DesetupSyncUIFooterItemEntity(true, true);
				_uIFooterItemEntity = (UIFooterItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIFooterItemEntity, new PropertyChangedEventHandler( OnUIFooterItemEntityPropertyChanged ), "UIFooterItemEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UIFooterItemEntityUsingUIFooterItemIdStatic, true, ref _alreadyFetchedUIFooterItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIFooterItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uITabEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUITabEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uITabEntity, new PropertyChangedEventHandler( OnUITabEntityPropertyChanged ), "UITabEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UITabEntityUsingUITabIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.UITabId } );		
			_uITabEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uITabEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUITabEntity(IEntityCore relatedEntity)
		{
			if(_uITabEntity!=relatedEntity)
			{		
				DesetupSyncUITabEntity(true, true);
				_uITabEntity = (UITabEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uITabEntity, new PropertyChangedEventHandler( OnUITabEntityPropertyChanged ), "UITabEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UITabEntityUsingUITabIdStatic, true, ref _alreadyFetchedUITabEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUITabEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.UIWidgetId } );		
			_uIWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIWidgetEntity(IEntityCore relatedEntity)
		{
			if(_uIWidgetEntity!=relatedEntity)
			{		
				DesetupSyncUIWidgetEntity(true, true);
				_uIWidgetEntity = (UIWidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, ref _alreadyFetchedUIWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _venueCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVenueCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _venueCategoryEntity, new PropertyChangedEventHandler( OnVenueCategoryEntityPropertyChanged ), "VenueCategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.VenueCategoryEntityUsingVenueCategoryIdStatic, true, signalRelatedEntity, "CustomTextCollection", resetFKFields, new int[] { (int)CustomTextFieldIndex.VenueCategoryId } );		
			_venueCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _venueCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVenueCategoryEntity(IEntityCore relatedEntity)
		{
			if(_venueCategoryEntity!=relatedEntity)
			{		
				DesetupSyncVenueCategoryEntity(true, true);
				_venueCategoryEntity = (VenueCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _venueCategoryEntity, new PropertyChangedEventHandler( OnVenueCategoryEntityPropertyChanged ), "VenueCategoryEntity", Obymobi.Data.RelationClasses.StaticCustomTextRelations.VenueCategoryEntityUsingVenueCategoryIdStatic, true, ref _alreadyFetchedVenueCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVenueCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 customTextId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomTextFieldIndex.CustomTextId].ForcedCurrentValueWrite(customTextId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomTextDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomTextEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomTextRelations Relations
		{
			get	{ return new CustomTextRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ActionButton'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionButtonEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionButtonCollection(), (IEntityRelation)GetRelationsForField("ActionButtonEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ActionButtonEntity, 0, null, null, null, "ActionButtonEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Amenity'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmenityEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AmenityCollection(), (IEntityRelation)GetRelationsForField("AmenityEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AmenityEntity, 0, null, null, null, "AmenityEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CarouselItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCarouselItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CarouselItemCollection(), (IEntityRelation)GetRelationsForField("CarouselItemEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.CarouselItemEntity, 0, null, null, null, "CarouselItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenuItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuItemCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuItemEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, 0, null, null, null, "NavigationMenuItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("WidgetEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "WidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attachment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentCollection(), (IEntityRelation)GetRelationsForField("AttachmentEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AttachmentEntity, 0, null, null, null, "AttachmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.CheckoutMethodEntity, 0, null, null, null, "CheckoutMethodEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "EntertainmentcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredCommand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredCommandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredCommandCollection(), (IEntityRelation)GetRelationsForField("InfraredCommandEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.InfraredCommandEntity, 0, null, null, null, "InfraredCommandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("LanguageEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, null, "LanguageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Productgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductgroupCollection(), (IEntityRelation)GetRelationsForField("ProductgroupEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ProductgroupEntity, 0, null, null, null, "ProductgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlArea'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoomControlAreaEntity, 0, null, null, null, "RoomControlAreaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, 0, null, null, null, "RoomControlSectionItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.ServiceMethodEntity, 0, null, null, null, "ServiceMethodEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.SiteTemplateEntity, 0, null, null, null, "SiteTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Station'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.StationCollection(), (IEntityRelation)GetRelationsForField("StationEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.StationEntity, 0, null, null, null, "StationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIFooterItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIFooterItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIFooterItemCollection(), (IEntityRelation)GetRelationsForField("UIFooterItemEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.UIFooterItemEntity, 0, null, null, null, "UIFooterItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("UITabEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "UITabEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVenueCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryCollection(), (IEntityRelation)GetRelationsForField("VenueCategoryEntity")[0], (int)Obymobi.Data.EntityType.CustomTextEntity, (int)Obymobi.Data.EntityType.VenueCategoryEntity, 0, null, null, null, "VenueCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomTextId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CustomTextId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CustomTextId
		{
			get { return (System.Int32)GetValue((int)CustomTextFieldIndex.CustomTextId, true); }
			set	{ SetValue((int)CustomTextFieldIndex.CustomTextId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.CompanyId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The LanguageId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LanguageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.LanguageId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The Text property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."Text"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)CustomTextFieldIndex.Text, true); }
			set	{ SetValue((int)CustomTextFieldIndex.Text, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.CategoryId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The ProductId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ProductId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ProductId, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CustomTextFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CustomTextFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CustomTextFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CustomTextFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomTextFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CustomTextFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomTextFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CustomTextFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The Type property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.CustomTextType Type
		{
			get { return (Obymobi.Enums.CustomTextType)GetValue((int)CustomTextFieldIndex.Type, true); }
			set	{ SetValue((int)CustomTextFieldIndex.Type, value, true); }
		}

		/// <summary> The AdvertisementId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AdvertisementId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AdvertisementId, value, true); }
		}

		/// <summary> The AlterationId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AlterationId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The AlterationoptionId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The AnnouncementId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AnnouncementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AnnouncementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AnnouncementId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AnnouncementId, value, true); }
		}

		/// <summary> The ActionButtonId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ActionButtonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionButtonId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ActionButtonId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ActionButtonId, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The EntertainmentcategoryId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."EntertainmentcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.EntertainmentcategoryId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.EntertainmentcategoryId, value, true); }
		}

		/// <summary> The GenericproductId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."GenericproductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.GenericproductId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.GenericproductId, value, true); }
		}

		/// <summary> The PageId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.PageId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.PageId, value, true); }
		}

		/// <summary> The SiteId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.SiteId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.SiteId, value, true); }
		}

		/// <summary> The AmenityId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AmenityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AmenityId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AmenityId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AmenityId, value, true); }
		}

		/// <summary> The AttachmentId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AttachmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AttachmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AttachmentId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AttachmentId, value, true); }
		}

		/// <summary> The PageTemplateId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."PageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.PageTemplateId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.PageTemplateId, value, true); }
		}

		/// <summary> The GenericcategoryId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."GenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.GenericcategoryId, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The VenueCategoryId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."VenueCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VenueCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.VenueCategoryId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.VenueCategoryId, value, true); }
		}

		/// <summary> The RoomControlComponentId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoomControlComponentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoomControlComponentId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoomControlComponentId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The RoomControlSectionItemId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoomControlSectionItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoomControlSectionItemId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoomControlSectionItemId, value, true); }
		}

		/// <summary> The RoomControlWidgetId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoomControlWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlWidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoomControlWidgetId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoomControlWidgetId, value, true); }
		}

		/// <summary> The ScheduledMessageId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ScheduledMessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduledMessageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ScheduledMessageId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ScheduledMessageId, value, true); }
		}

		/// <summary> The SiteTemplateId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."SiteTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.SiteTemplateId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.SiteTemplateId, value, true); }
		}

		/// <summary> The StationId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."StationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.StationId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.StationId, value, true); }
		}

		/// <summary> The UIFooterItemId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."UIFooterItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIFooterItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.UIFooterItemId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.UIFooterItemId, value, true); }
		}

		/// <summary> The UITabId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."UITabId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UITabId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.UITabId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.UITabId, value, true); }
		}

		/// <summary> The UIWidgetId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."UIWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIWidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.UIWidgetId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.UIWidgetId, value, true); }
		}

		/// <summary> The AvailabilityId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."AvailabilityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AvailabilityId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.AvailabilityId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.AvailabilityId, value, true); }
		}

		/// <summary> The CultureCode property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)CustomTextFieldIndex.CultureCode, true); }
			set	{ SetValue((int)CustomTextFieldIndex.CultureCode, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The ProductgroupId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ProductgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ProductgroupId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ProductgroupId, value, true); }
		}

		/// <summary> The RoutestephandlerId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."RoutestephandlerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoutestephandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.RoutestephandlerId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.RoutestephandlerId, value, true); }
		}

		/// <summary> The InfraredCommandId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."InfraredCommandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InfraredCommandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.InfraredCommandId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.InfraredCommandId, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ApplicationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ApplicationConfigurationId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> The NavigationMenuItemId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."NavigationMenuItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NavigationMenuItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.NavigationMenuItemId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.NavigationMenuItemId, value, true); }
		}

		/// <summary> The CarouselItemId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CarouselItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CarouselItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.CarouselItemId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.CarouselItemId, value, true); }
		}

		/// <summary> The WidgetId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.WidgetId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The LandingPageId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."LandingPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.LandingPageId, value, true); }
		}

		/// <summary> The OutletId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."OutletId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.OutletId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.OutletId, value, true); }
		}

		/// <summary> The ServiceMethodId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."ServiceMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.ServiceMethodId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.ServiceMethodId, value, true); }
		}

		/// <summary> The CheckoutMethodId property of the Entity CustomText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CustomText"."CheckoutMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CheckoutMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomTextFieldIndex.CheckoutMethodId, false); }
			set	{ SetValue((int)CustomTextFieldIndex.CheckoutMethodId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ActionButtonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionButtonEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ActionButtonEntity ActionButtonEntity
		{
			get	{ return GetSingleActionButtonEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionButtonEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ActionButtonEntity", _actionButtonEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionButtonEntity. When set to true, ActionButtonEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionButtonEntity is accessed. You can always execute a forced fetch by calling GetSingleActionButtonEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionButtonEntity
		{
			get	{ return _alwaysFetchActionButtonEntity; }
			set	{ _alwaysFetchActionButtonEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionButtonEntity already has been fetched. Setting this property to false when ActionButtonEntity has been fetched
		/// will set ActionButtonEntity to null as well. Setting this property to true while ActionButtonEntity hasn't been fetched disables lazy loading for ActionButtonEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionButtonEntity
		{
			get { return _alreadyFetchedActionButtonEntity;}
			set 
			{
				if(_alreadyFetchedActionButtonEntity && !value)
				{
					this.ActionButtonEntity = null;
				}
				_alreadyFetchedActionButtonEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionButtonEntity is not found
		/// in the database. When set to true, ActionButtonEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionButtonEntityReturnsNewIfNotFound
		{
			get	{ return _actionButtonEntityReturnsNewIfNotFound; }
			set { _actionButtonEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementEntity AdvertisementEntity
		{
			get	{ return GetSingleAdvertisementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AdvertisementEntity", _advertisementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementEntity. When set to true, AdvertisementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementEntity
		{
			get	{ return _alwaysFetchAdvertisementEntity; }
			set	{ _alwaysFetchAdvertisementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementEntity already has been fetched. Setting this property to false when AdvertisementEntity has been fetched
		/// will set AdvertisementEntity to null as well. Setting this property to true while AdvertisementEntity hasn't been fetched disables lazy loading for AdvertisementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementEntity
		{
			get { return _alreadyFetchedAdvertisementEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementEntity && !value)
				{
					this.AdvertisementEntity = null;
				}
				_alreadyFetchedAdvertisementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementEntity is not found
		/// in the database. When set to true, AdvertisementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementEntityReturnsNewIfNotFound; }
			set { _advertisementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationoptionEntity AlterationoptionEntity
		{
			get	{ return GetSingleAlterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AlterationoptionEntity", _alterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionEntity. When set to true, AlterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionEntity
		{
			get	{ return _alwaysFetchAlterationoptionEntity; }
			set	{ _alwaysFetchAlterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionEntity already has been fetched. Setting this property to false when AlterationoptionEntity has been fetched
		/// will set AlterationoptionEntity to null as well. Setting this property to true while AlterationoptionEntity hasn't been fetched disables lazy loading for AlterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionEntity
		{
			get { return _alreadyFetchedAlterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedAlterationoptionEntity && !value)
				{
					this.AlterationoptionEntity = null;
				}
				_alreadyFetchedAlterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationoptionEntity is not found
		/// in the database. When set to true, AlterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _alterationoptionEntityReturnsNewIfNotFound; }
			set { _alterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AmenityEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAmenityEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AmenityEntity AmenityEntity
		{
			get	{ return GetSingleAmenityEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAmenityEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AmenityEntity", _amenityEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AmenityEntity. When set to true, AmenityEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmenityEntity is accessed. You can always execute a forced fetch by calling GetSingleAmenityEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmenityEntity
		{
			get	{ return _alwaysFetchAmenityEntity; }
			set	{ _alwaysFetchAmenityEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmenityEntity already has been fetched. Setting this property to false when AmenityEntity has been fetched
		/// will set AmenityEntity to null as well. Setting this property to true while AmenityEntity hasn't been fetched disables lazy loading for AmenityEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmenityEntity
		{
			get { return _alreadyFetchedAmenityEntity;}
			set 
			{
				if(_alreadyFetchedAmenityEntity && !value)
				{
					this.AmenityEntity = null;
				}
				_alreadyFetchedAmenityEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AmenityEntity is not found
		/// in the database. When set to true, AmenityEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AmenityEntityReturnsNewIfNotFound
		{
			get	{ return _amenityEntityReturnsNewIfNotFound; }
			set { _amenityEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AnnouncementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAnnouncementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AnnouncementEntity AnnouncementEntity
		{
			get	{ return GetSingleAnnouncementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAnnouncementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AnnouncementEntity", _announcementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementEntity. When set to true, AnnouncementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementEntity is accessed. You can always execute a forced fetch by calling GetSingleAnnouncementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementEntity
		{
			get	{ return _alwaysFetchAnnouncementEntity; }
			set	{ _alwaysFetchAnnouncementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementEntity already has been fetched. Setting this property to false when AnnouncementEntity has been fetched
		/// will set AnnouncementEntity to null as well. Setting this property to true while AnnouncementEntity hasn't been fetched disables lazy loading for AnnouncementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementEntity
		{
			get { return _alreadyFetchedAnnouncementEntity;}
			set 
			{
				if(_alreadyFetchedAnnouncementEntity && !value)
				{
					this.AnnouncementEntity = null;
				}
				_alreadyFetchedAnnouncementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AnnouncementEntity is not found
		/// in the database. When set to true, AnnouncementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AnnouncementEntityReturnsNewIfNotFound
		{
			get	{ return _announcementEntityReturnsNewIfNotFound; }
			set { _announcementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CarouselItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCarouselItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CarouselItemEntity CarouselItemEntity
		{
			get	{ return GetSingleCarouselItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCarouselItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "CarouselItemEntity", _carouselItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CarouselItemEntity. When set to true, CarouselItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CarouselItemEntity is accessed. You can always execute a forced fetch by calling GetSingleCarouselItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCarouselItemEntity
		{
			get	{ return _alwaysFetchCarouselItemEntity; }
			set	{ _alwaysFetchCarouselItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CarouselItemEntity already has been fetched. Setting this property to false when CarouselItemEntity has been fetched
		/// will set CarouselItemEntity to null as well. Setting this property to true while CarouselItemEntity hasn't been fetched disables lazy loading for CarouselItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCarouselItemEntity
		{
			get { return _alreadyFetchedCarouselItemEntity;}
			set 
			{
				if(_alreadyFetchedCarouselItemEntity && !value)
				{
					this.CarouselItemEntity = null;
				}
				_alreadyFetchedCarouselItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CarouselItemEntity is not found
		/// in the database. When set to true, CarouselItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CarouselItemEntityReturnsNewIfNotFound
		{
			get	{ return _carouselItemEntityReturnsNewIfNotFound; }
			set { _carouselItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LandingPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLandingPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LandingPageEntity LandingPageEntity
		{
			get	{ return GetSingleLandingPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLandingPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "LandingPageEntity", _landingPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageEntity. When set to true, LandingPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageEntity is accessed. You can always execute a forced fetch by calling GetSingleLandingPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageEntity
		{
			get	{ return _alwaysFetchLandingPageEntity; }
			set	{ _alwaysFetchLandingPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageEntity already has been fetched. Setting this property to false when LandingPageEntity has been fetched
		/// will set LandingPageEntity to null as well. Setting this property to true while LandingPageEntity hasn't been fetched disables lazy loading for LandingPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageEntity
		{
			get { return _alreadyFetchedLandingPageEntity;}
			set 
			{
				if(_alreadyFetchedLandingPageEntity && !value)
				{
					this.LandingPageEntity = null;
				}
				_alreadyFetchedLandingPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LandingPageEntity is not found
		/// in the database. When set to true, LandingPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LandingPageEntityReturnsNewIfNotFound
		{
			get	{ return _landingPageEntityReturnsNewIfNotFound; }
			set { _landingPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'NavigationMenuItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNavigationMenuItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual NavigationMenuItemEntity NavigationMenuItemEntity
		{
			get	{ return GetSingleNavigationMenuItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNavigationMenuItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "NavigationMenuItemEntity", _navigationMenuItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuItemEntity. When set to true, NavigationMenuItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuItemEntity is accessed. You can always execute a forced fetch by calling GetSingleNavigationMenuItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuItemEntity
		{
			get	{ return _alwaysFetchNavigationMenuItemEntity; }
			set	{ _alwaysFetchNavigationMenuItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuItemEntity already has been fetched. Setting this property to false when NavigationMenuItemEntity has been fetched
		/// will set NavigationMenuItemEntity to null as well. Setting this property to true while NavigationMenuItemEntity hasn't been fetched disables lazy loading for NavigationMenuItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuItemEntity
		{
			get { return _alreadyFetchedNavigationMenuItemEntity;}
			set 
			{
				if(_alreadyFetchedNavigationMenuItemEntity && !value)
				{
					this.NavigationMenuItemEntity = null;
				}
				_alreadyFetchedNavigationMenuItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NavigationMenuItemEntity is not found
		/// in the database. When set to true, NavigationMenuItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool NavigationMenuItemEntityReturnsNewIfNotFound
		{
			get	{ return _navigationMenuItemEntityReturnsNewIfNotFound; }
			set { _navigationMenuItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetEntity WidgetEntity
		{
			get	{ return GetSingleWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "WidgetEntity", _widgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetEntity. When set to true, WidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetEntity
		{
			get	{ return _alwaysFetchWidgetEntity; }
			set	{ _alwaysFetchWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetEntity already has been fetched. Setting this property to false when WidgetEntity has been fetched
		/// will set WidgetEntity to null as well. Setting this property to true while WidgetEntity hasn't been fetched disables lazy loading for WidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetEntity
		{
			get { return _alreadyFetchedWidgetEntity;}
			set 
			{
				if(_alreadyFetchedWidgetEntity && !value)
				{
					this.WidgetEntity = null;
				}
				_alreadyFetchedWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetEntity is not found
		/// in the database. When set to true, WidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetEntityReturnsNewIfNotFound
		{
			get	{ return _widgetEntityReturnsNewIfNotFound; }
			set { _widgetEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttachmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttachmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AttachmentEntity AttachmentEntity
		{
			get	{ return GetSingleAttachmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttachmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AttachmentEntity", _attachmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentEntity. When set to true, AttachmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentEntity is accessed. You can always execute a forced fetch by calling GetSingleAttachmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentEntity
		{
			get	{ return _alwaysFetchAttachmentEntity; }
			set	{ _alwaysFetchAttachmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentEntity already has been fetched. Setting this property to false when AttachmentEntity has been fetched
		/// will set AttachmentEntity to null as well. Setting this property to true while AttachmentEntity hasn't been fetched disables lazy loading for AttachmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentEntity
		{
			get { return _alreadyFetchedAttachmentEntity;}
			set 
			{
				if(_alreadyFetchedAttachmentEntity && !value)
				{
					this.AttachmentEntity = null;
				}
				_alreadyFetchedAttachmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttachmentEntity is not found
		/// in the database. When set to true, AttachmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AttachmentEntityReturnsNewIfNotFound
		{
			get	{ return _attachmentEntityReturnsNewIfNotFound; }
			set { _attachmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AvailabilityEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAvailabilityEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AvailabilityEntity AvailabilityEntity
		{
			get	{ return GetSingleAvailabilityEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAvailabilityEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "AvailabilityEntity", _availabilityEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityEntity. When set to true, AvailabilityEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityEntity is accessed. You can always execute a forced fetch by calling GetSingleAvailabilityEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityEntity
		{
			get	{ return _alwaysFetchAvailabilityEntity; }
			set	{ _alwaysFetchAvailabilityEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityEntity already has been fetched. Setting this property to false when AvailabilityEntity has been fetched
		/// will set AvailabilityEntity to null as well. Setting this property to true while AvailabilityEntity hasn't been fetched disables lazy loading for AvailabilityEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityEntity
		{
			get { return _alreadyFetchedAvailabilityEntity;}
			set 
			{
				if(_alreadyFetchedAvailabilityEntity && !value)
				{
					this.AvailabilityEntity = null;
				}
				_alreadyFetchedAvailabilityEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AvailabilityEntity is not found
		/// in the database. When set to true, AvailabilityEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AvailabilityEntityReturnsNewIfNotFound
		{
			get	{ return _availabilityEntityReturnsNewIfNotFound; }
			set { _availabilityEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CheckoutMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCheckoutMethodEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CheckoutMethodEntity CheckoutMethodEntity
		{
			get	{ return GetSingleCheckoutMethodEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCheckoutMethodEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "CheckoutMethodEntity", _checkoutMethodEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodEntity. When set to true, CheckoutMethodEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodEntity is accessed. You can always execute a forced fetch by calling GetSingleCheckoutMethodEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodEntity
		{
			get	{ return _alwaysFetchCheckoutMethodEntity; }
			set	{ _alwaysFetchCheckoutMethodEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodEntity already has been fetched. Setting this property to false when CheckoutMethodEntity has been fetched
		/// will set CheckoutMethodEntity to null as well. Setting this property to true while CheckoutMethodEntity hasn't been fetched disables lazy loading for CheckoutMethodEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodEntity
		{
			get { return _alreadyFetchedCheckoutMethodEntity;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodEntity && !value)
				{
					this.CheckoutMethodEntity = null;
				}
				_alreadyFetchedCheckoutMethodEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CheckoutMethodEntity is not found
		/// in the database. When set to true, CheckoutMethodEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CheckoutMethodEntityReturnsNewIfNotFound
		{
			get	{ return _checkoutMethodEntityReturnsNewIfNotFound; }
			set { _checkoutMethodEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity EntertainmentcategoryEntity
		{
			get	{ return GetSingleEntertainmentcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "EntertainmentcategoryEntity", _entertainmentcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryEntity. When set to true, EntertainmentcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryEntity
		{
			get	{ return _alwaysFetchEntertainmentcategoryEntity; }
			set	{ _alwaysFetchEntertainmentcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryEntity already has been fetched. Setting this property to false when EntertainmentcategoryEntity has been fetched
		/// will set EntertainmentcategoryEntity to null as well. Setting this property to true while EntertainmentcategoryEntity hasn't been fetched disables lazy loading for EntertainmentcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryEntity
		{
			get { return _alreadyFetchedEntertainmentcategoryEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryEntity && !value)
				{
					this.EntertainmentcategoryEntity = null;
				}
				_alreadyFetchedEntertainmentcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentcategoryEntity is not found
		/// in the database. When set to true, EntertainmentcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentcategoryEntityReturnsNewIfNotFound; }
			set { _entertainmentcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericcategoryEntity GenericcategoryEntity
		{
			get	{ return GetSingleGenericcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "GenericcategoryEntity", _genericcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryEntity. When set to true, GenericcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryEntity
		{
			get	{ return _alwaysFetchGenericcategoryEntity; }
			set	{ _alwaysFetchGenericcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryEntity already has been fetched. Setting this property to false when GenericcategoryEntity has been fetched
		/// will set GenericcategoryEntity to null as well. Setting this property to true while GenericcategoryEntity hasn't been fetched disables lazy loading for GenericcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryEntity
		{
			get { return _alreadyFetchedGenericcategoryEntity;}
			set 
			{
				if(_alreadyFetchedGenericcategoryEntity && !value)
				{
					this.GenericcategoryEntity = null;
				}
				_alreadyFetchedGenericcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericcategoryEntity is not found
		/// in the database. When set to true, GenericcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _genericcategoryEntityReturnsNewIfNotFound; }
			set { _genericcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericproductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericproductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericproductEntity GenericproductEntity
		{
			get	{ return GetSingleGenericproductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericproductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "GenericproductEntity", _genericproductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductEntity. When set to true, GenericproductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericproductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductEntity
		{
			get	{ return _alwaysFetchGenericproductEntity; }
			set	{ _alwaysFetchGenericproductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductEntity already has been fetched. Setting this property to false when GenericproductEntity has been fetched
		/// will set GenericproductEntity to null as well. Setting this property to true while GenericproductEntity hasn't been fetched disables lazy loading for GenericproductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductEntity
		{
			get { return _alreadyFetchedGenericproductEntity;}
			set 
			{
				if(_alreadyFetchedGenericproductEntity && !value)
				{
					this.GenericproductEntity = null;
				}
				_alreadyFetchedGenericproductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericproductEntity is not found
		/// in the database. When set to true, GenericproductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericproductEntityReturnsNewIfNotFound
		{
			get	{ return _genericproductEntityReturnsNewIfNotFound; }
			set { _genericproductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InfraredCommandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInfraredCommandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual InfraredCommandEntity InfraredCommandEntity
		{
			get	{ return GetSingleInfraredCommandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInfraredCommandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "InfraredCommandEntity", _infraredCommandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredCommandEntity. When set to true, InfraredCommandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredCommandEntity is accessed. You can always execute a forced fetch by calling GetSingleInfraredCommandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredCommandEntity
		{
			get	{ return _alwaysFetchInfraredCommandEntity; }
			set	{ _alwaysFetchInfraredCommandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredCommandEntity already has been fetched. Setting this property to false when InfraredCommandEntity has been fetched
		/// will set InfraredCommandEntity to null as well. Setting this property to true while InfraredCommandEntity hasn't been fetched disables lazy loading for InfraredCommandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredCommandEntity
		{
			get { return _alreadyFetchedInfraredCommandEntity;}
			set 
			{
				if(_alreadyFetchedInfraredCommandEntity && !value)
				{
					this.InfraredCommandEntity = null;
				}
				_alreadyFetchedInfraredCommandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InfraredCommandEntity is not found
		/// in the database. When set to true, InfraredCommandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool InfraredCommandEntityReturnsNewIfNotFound
		{
			get	{ return _infraredCommandEntityReturnsNewIfNotFound; }
			set { _infraredCommandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LanguageEntity LanguageEntity
		{
			get	{ return GetSingleLanguageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "LanguageEntity", _languageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageEntity. When set to true, LanguageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageEntity is accessed. You can always execute a forced fetch by calling GetSingleLanguageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageEntity
		{
			get	{ return _alwaysFetchLanguageEntity; }
			set	{ _alwaysFetchLanguageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageEntity already has been fetched. Setting this property to false when LanguageEntity has been fetched
		/// will set LanguageEntity to null as well. Setting this property to true while LanguageEntity hasn't been fetched disables lazy loading for LanguageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageEntity
		{
			get { return _alreadyFetchedLanguageEntity;}
			set 
			{
				if(_alreadyFetchedLanguageEntity && !value)
				{
					this.LanguageEntity = null;
				}
				_alreadyFetchedLanguageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LanguageEntity is not found
		/// in the database. When set to true, LanguageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LanguageEntityReturnsNewIfNotFound
		{
			get	{ return _languageEntityReturnsNewIfNotFound; }
			set { _languageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletEntity OutletEntity
		{
			get	{ return GetSingleOutletEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "OutletEntity", _outletEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletEntity. When set to true, OutletEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletEntity
		{
			get	{ return _alwaysFetchOutletEntity; }
			set	{ _alwaysFetchOutletEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletEntity already has been fetched. Setting this property to false when OutletEntity has been fetched
		/// will set OutletEntity to null as well. Setting this property to true while OutletEntity hasn't been fetched disables lazy loading for OutletEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletEntity
		{
			get { return _alreadyFetchedOutletEntity;}
			set 
			{
				if(_alreadyFetchedOutletEntity && !value)
				{
					this.OutletEntity = null;
				}
				_alreadyFetchedOutletEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletEntity is not found
		/// in the database. When set to true, OutletEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletEntityReturnsNewIfNotFound
		{
			get	{ return _outletEntityReturnsNewIfNotFound; }
			set { _outletEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageTemplateEntity PageTemplateEntity
		{
			get	{ return GetSinglePageTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "PageTemplateEntity", _pageTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateEntity. When set to true, PageTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateEntity is accessed. You can always execute a forced fetch by calling GetSinglePageTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateEntity
		{
			get	{ return _alwaysFetchPageTemplateEntity; }
			set	{ _alwaysFetchPageTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateEntity already has been fetched. Setting this property to false when PageTemplateEntity has been fetched
		/// will set PageTemplateEntity to null as well. Setting this property to true while PageTemplateEntity hasn't been fetched disables lazy loading for PageTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateEntity
		{
			get { return _alreadyFetchedPageTemplateEntity;}
			set 
			{
				if(_alreadyFetchedPageTemplateEntity && !value)
				{
					this.PageTemplateEntity = null;
				}
				_alreadyFetchedPageTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageTemplateEntity is not found
		/// in the database. When set to true, PageTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _pageTemplateEntityReturnsNewIfNotFound; }
			set { _pageTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "PointOfInterestEntity", _pointOfInterestEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set { _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductgroupEntity ProductgroupEntity
		{
			get	{ return GetSingleProductgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ProductgroupEntity", _productgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductgroupEntity. When set to true, ProductgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleProductgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductgroupEntity
		{
			get	{ return _alwaysFetchProductgroupEntity; }
			set	{ _alwaysFetchProductgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductgroupEntity already has been fetched. Setting this property to false when ProductgroupEntity has been fetched
		/// will set ProductgroupEntity to null as well. Setting this property to true while ProductgroupEntity hasn't been fetched disables lazy loading for ProductgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductgroupEntity
		{
			get { return _alreadyFetchedProductgroupEntity;}
			set 
			{
				if(_alreadyFetchedProductgroupEntity && !value)
				{
					this.ProductgroupEntity = null;
				}
				_alreadyFetchedProductgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductgroupEntity is not found
		/// in the database. When set to true, ProductgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductgroupEntityReturnsNewIfNotFound
		{
			get	{ return _productgroupEntityReturnsNewIfNotFound; }
			set { _productgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlAreaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlAreaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlAreaEntity RoomControlAreaEntity
		{
			get	{ return GetSingleRoomControlAreaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlAreaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoomControlAreaEntity", _roomControlAreaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaEntity. When set to true, RoomControlAreaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlAreaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaEntity
		{
			get	{ return _alwaysFetchRoomControlAreaEntity; }
			set	{ _alwaysFetchRoomControlAreaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaEntity already has been fetched. Setting this property to false when RoomControlAreaEntity has been fetched
		/// will set RoomControlAreaEntity to null as well. Setting this property to true while RoomControlAreaEntity hasn't been fetched disables lazy loading for RoomControlAreaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaEntity
		{
			get { return _alreadyFetchedRoomControlAreaEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaEntity && !value)
				{
					this.RoomControlAreaEntity = null;
				}
				_alreadyFetchedRoomControlAreaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlAreaEntity is not found
		/// in the database. When set to true, RoomControlAreaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlAreaEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlAreaEntityReturnsNewIfNotFound; }
			set { _roomControlAreaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity
		{
			get	{ return GetSingleRoomControlComponentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoomControlComponentEntity", _roomControlComponentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity. When set to true, RoomControlComponentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity
		{
			get	{ return _alwaysFetchRoomControlComponentEntity; }
			set	{ _alwaysFetchRoomControlComponentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity already has been fetched. Setting this property to false when RoomControlComponentEntity has been fetched
		/// will set RoomControlComponentEntity to null as well. Setting this property to true while RoomControlComponentEntity hasn't been fetched disables lazy loading for RoomControlComponentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity
		{
			get { return _alreadyFetchedRoomControlComponentEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity && !value)
				{
					this.RoomControlComponentEntity = null;
				}
				_alreadyFetchedRoomControlComponentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity is not found
		/// in the database. When set to true, RoomControlComponentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntityReturnsNewIfNotFound; }
			set { _roomControlComponentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionItemEntity RoomControlSectionItemEntity
		{
			get	{ return GetSingleRoomControlSectionItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoomControlSectionItemEntity", _roomControlSectionItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemEntity. When set to true, RoomControlSectionItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemEntity
		{
			get	{ return _alwaysFetchRoomControlSectionItemEntity; }
			set	{ _alwaysFetchRoomControlSectionItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemEntity already has been fetched. Setting this property to false when RoomControlSectionItemEntity has been fetched
		/// will set RoomControlSectionItemEntity to null as well. Setting this property to true while RoomControlSectionItemEntity hasn't been fetched disables lazy loading for RoomControlSectionItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemEntity
		{
			get { return _alreadyFetchedRoomControlSectionItemEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemEntity && !value)
				{
					this.RoomControlSectionItemEntity = null;
				}
				_alreadyFetchedRoomControlSectionItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionItemEntity is not found
		/// in the database. When set to true, RoomControlSectionItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionItemEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionItemEntityReturnsNewIfNotFound; }
			set { _roomControlSectionItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlWidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlWidgetEntity RoomControlWidgetEntity
		{
			get	{ return GetSingleRoomControlWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoomControlWidgetEntity", _roomControlWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetEntity. When set to true, RoomControlWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetEntity
		{
			get	{ return _alwaysFetchRoomControlWidgetEntity; }
			set	{ _alwaysFetchRoomControlWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetEntity already has been fetched. Setting this property to false when RoomControlWidgetEntity has been fetched
		/// will set RoomControlWidgetEntity to null as well. Setting this property to true while RoomControlWidgetEntity hasn't been fetched disables lazy loading for RoomControlWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetEntity
		{
			get { return _alreadyFetchedRoomControlWidgetEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetEntity && !value)
				{
					this.RoomControlWidgetEntity = null;
				}
				_alreadyFetchedRoomControlWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlWidgetEntity is not found
		/// in the database. When set to true, RoomControlWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlWidgetEntityReturnsNewIfNotFound; }
			set { _roomControlWidgetEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoutestephandlerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoutestephandlerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoutestephandlerEntity RoutestephandlerEntity
		{
			get	{ return GetSingleRoutestephandlerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoutestephandlerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "RoutestephandlerEntity", _routestephandlerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerEntity. When set to true, RoutestephandlerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerEntity is accessed. You can always execute a forced fetch by calling GetSingleRoutestephandlerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerEntity
		{
			get	{ return _alwaysFetchRoutestephandlerEntity; }
			set	{ _alwaysFetchRoutestephandlerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerEntity already has been fetched. Setting this property to false when RoutestephandlerEntity has been fetched
		/// will set RoutestephandlerEntity to null as well. Setting this property to true while RoutestephandlerEntity hasn't been fetched disables lazy loading for RoutestephandlerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerEntity
		{
			get { return _alreadyFetchedRoutestephandlerEntity;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerEntity && !value)
				{
					this.RoutestephandlerEntity = null;
				}
				_alreadyFetchedRoutestephandlerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoutestephandlerEntity is not found
		/// in the database. When set to true, RoutestephandlerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoutestephandlerEntityReturnsNewIfNotFound
		{
			get	{ return _routestephandlerEntityReturnsNewIfNotFound; }
			set { _routestephandlerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ScheduledMessageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduledMessageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduledMessageEntity ScheduledMessageEntity
		{
			get	{ return GetSingleScheduledMessageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduledMessageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ScheduledMessageEntity", _scheduledMessageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageEntity. When set to true, ScheduledMessageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduledMessageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageEntity
		{
			get	{ return _alwaysFetchScheduledMessageEntity; }
			set	{ _alwaysFetchScheduledMessageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageEntity already has been fetched. Setting this property to false when ScheduledMessageEntity has been fetched
		/// will set ScheduledMessageEntity to null as well. Setting this property to true while ScheduledMessageEntity hasn't been fetched disables lazy loading for ScheduledMessageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageEntity
		{
			get { return _alreadyFetchedScheduledMessageEntity;}
			set 
			{
				if(_alreadyFetchedScheduledMessageEntity && !value)
				{
					this.ScheduledMessageEntity = null;
				}
				_alreadyFetchedScheduledMessageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduledMessageEntity is not found
		/// in the database. When set to true, ScheduledMessageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduledMessageEntityReturnsNewIfNotFound
		{
			get	{ return _scheduledMessageEntityReturnsNewIfNotFound; }
			set { _scheduledMessageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ServiceMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleServiceMethodEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ServiceMethodEntity ServiceMethodEntity
		{
			get	{ return GetSingleServiceMethodEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncServiceMethodEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "ServiceMethodEntity", _serviceMethodEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodEntity. When set to true, ServiceMethodEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodEntity is accessed. You can always execute a forced fetch by calling GetSingleServiceMethodEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodEntity
		{
			get	{ return _alwaysFetchServiceMethodEntity; }
			set	{ _alwaysFetchServiceMethodEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodEntity already has been fetched. Setting this property to false when ServiceMethodEntity has been fetched
		/// will set ServiceMethodEntity to null as well. Setting this property to true while ServiceMethodEntity hasn't been fetched disables lazy loading for ServiceMethodEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodEntity
		{
			get { return _alreadyFetchedServiceMethodEntity;}
			set 
			{
				if(_alreadyFetchedServiceMethodEntity && !value)
				{
					this.ServiceMethodEntity = null;
				}
				_alreadyFetchedServiceMethodEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ServiceMethodEntity is not found
		/// in the database. When set to true, ServiceMethodEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ServiceMethodEntityReturnsNewIfNotFound
		{
			get	{ return _serviceMethodEntityReturnsNewIfNotFound; }
			set { _serviceMethodEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteTemplateEntity SiteTemplateEntity
		{
			get	{ return GetSingleSiteTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "SiteTemplateEntity", _siteTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateEntity. When set to true, SiteTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateEntity
		{
			get	{ return _alwaysFetchSiteTemplateEntity; }
			set	{ _alwaysFetchSiteTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateEntity already has been fetched. Setting this property to false when SiteTemplateEntity has been fetched
		/// will set SiteTemplateEntity to null as well. Setting this property to true while SiteTemplateEntity hasn't been fetched disables lazy loading for SiteTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateEntity
		{
			get { return _alreadyFetchedSiteTemplateEntity;}
			set 
			{
				if(_alreadyFetchedSiteTemplateEntity && !value)
				{
					this.SiteTemplateEntity = null;
				}
				_alreadyFetchedSiteTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteTemplateEntity is not found
		/// in the database. When set to true, SiteTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _siteTemplateEntityReturnsNewIfNotFound; }
			set { _siteTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleStationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual StationEntity StationEntity
		{
			get	{ return GetSingleStationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncStationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "StationEntity", _stationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for StationEntity. When set to true, StationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StationEntity is accessed. You can always execute a forced fetch by calling GetSingleStationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStationEntity
		{
			get	{ return _alwaysFetchStationEntity; }
			set	{ _alwaysFetchStationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StationEntity already has been fetched. Setting this property to false when StationEntity has been fetched
		/// will set StationEntity to null as well. Setting this property to true while StationEntity hasn't been fetched disables lazy loading for StationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStationEntity
		{
			get { return _alreadyFetchedStationEntity;}
			set 
			{
				if(_alreadyFetchedStationEntity && !value)
				{
					this.StationEntity = null;
				}
				_alreadyFetchedStationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property StationEntity is not found
		/// in the database. When set to true, StationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool StationEntityReturnsNewIfNotFound
		{
			get	{ return _stationEntityReturnsNewIfNotFound; }
			set { _stationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIFooterItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIFooterItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIFooterItemEntity UIFooterItemEntity
		{
			get	{ return GetSingleUIFooterItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIFooterItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "UIFooterItemEntity", _uIFooterItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIFooterItemEntity. When set to true, UIFooterItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIFooterItemEntity is accessed. You can always execute a forced fetch by calling GetSingleUIFooterItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIFooterItemEntity
		{
			get	{ return _alwaysFetchUIFooterItemEntity; }
			set	{ _alwaysFetchUIFooterItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIFooterItemEntity already has been fetched. Setting this property to false when UIFooterItemEntity has been fetched
		/// will set UIFooterItemEntity to null as well. Setting this property to true while UIFooterItemEntity hasn't been fetched disables lazy loading for UIFooterItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIFooterItemEntity
		{
			get { return _alreadyFetchedUIFooterItemEntity;}
			set 
			{
				if(_alreadyFetchedUIFooterItemEntity && !value)
				{
					this.UIFooterItemEntity = null;
				}
				_alreadyFetchedUIFooterItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIFooterItemEntity is not found
		/// in the database. When set to true, UIFooterItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIFooterItemEntityReturnsNewIfNotFound
		{
			get	{ return _uIFooterItemEntityReturnsNewIfNotFound; }
			set { _uIFooterItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UITabEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUITabEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UITabEntity UITabEntity
		{
			get	{ return GetSingleUITabEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUITabEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "UITabEntity", _uITabEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UITabEntity. When set to true, UITabEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabEntity is accessed. You can always execute a forced fetch by calling GetSingleUITabEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabEntity
		{
			get	{ return _alwaysFetchUITabEntity; }
			set	{ _alwaysFetchUITabEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabEntity already has been fetched. Setting this property to false when UITabEntity has been fetched
		/// will set UITabEntity to null as well. Setting this property to true while UITabEntity hasn't been fetched disables lazy loading for UITabEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabEntity
		{
			get { return _alreadyFetchedUITabEntity;}
			set 
			{
				if(_alreadyFetchedUITabEntity && !value)
				{
					this.UITabEntity = null;
				}
				_alreadyFetchedUITabEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UITabEntity is not found
		/// in the database. When set to true, UITabEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UITabEntityReturnsNewIfNotFound
		{
			get	{ return _uITabEntityReturnsNewIfNotFound; }
			set { _uITabEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIWidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIWidgetEntity UIWidgetEntity
		{
			get	{ return GetSingleUIWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "UIWidgetEntity", _uIWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetEntity. When set to true, UIWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleUIWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetEntity
		{
			get	{ return _alwaysFetchUIWidgetEntity; }
			set	{ _alwaysFetchUIWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetEntity already has been fetched. Setting this property to false when UIWidgetEntity has been fetched
		/// will set UIWidgetEntity to null as well. Setting this property to true while UIWidgetEntity hasn't been fetched disables lazy loading for UIWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetEntity
		{
			get { return _alreadyFetchedUIWidgetEntity;}
			set 
			{
				if(_alreadyFetchedUIWidgetEntity && !value)
				{
					this.UIWidgetEntity = null;
				}
				_alreadyFetchedUIWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIWidgetEntity is not found
		/// in the database. When set to true, UIWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _uIWidgetEntityReturnsNewIfNotFound; }
			set { _uIWidgetEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VenueCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVenueCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual VenueCategoryEntity VenueCategoryEntity
		{
			get	{ return GetSingleVenueCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVenueCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomTextCollection", "VenueCategoryEntity", _venueCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VenueCategoryEntity. When set to true, VenueCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VenueCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleVenueCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVenueCategoryEntity
		{
			get	{ return _alwaysFetchVenueCategoryEntity; }
			set	{ _alwaysFetchVenueCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VenueCategoryEntity already has been fetched. Setting this property to false when VenueCategoryEntity has been fetched
		/// will set VenueCategoryEntity to null as well. Setting this property to true while VenueCategoryEntity hasn't been fetched disables lazy loading for VenueCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVenueCategoryEntity
		{
			get { return _alreadyFetchedVenueCategoryEntity;}
			set 
			{
				if(_alreadyFetchedVenueCategoryEntity && !value)
				{
					this.VenueCategoryEntity = null;
				}
				_alreadyFetchedVenueCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VenueCategoryEntity is not found
		/// in the database. When set to true, VenueCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool VenueCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _venueCategoryEntityReturnsNewIfNotFound; }
			set { _venueCategoryEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CustomTextEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
