﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ExternalSystem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ExternalSystemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ExternalSystemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection	_externalDeliverypointCollection;
		private bool	_alwaysFetchExternalDeliverypointCollection, _alreadyFetchedExternalDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.ExternalMenuCollection	_externalMenuCollection;
		private bool	_alwaysFetchExternalMenuCollection, _alreadyFetchedExternalMenuCollection;
		private Obymobi.Data.CollectionClasses.ExternalProductCollection	_externalProductCollection;
		private bool	_alwaysFetchExternalProductCollection, _alreadyFetchedExternalProductCollection;
		private Obymobi.Data.CollectionClasses.ExternalSystemLogCollection	_externalSystemLogCollection;
		private bool	_alwaysFetchExternalSystemLogCollection, _alreadyFetchedExternalSystemLogCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection	_orderRoutestephandlerCollection;
		private bool	_alwaysFetchOrderRoutestephandlerCollection, _alreadyFetchedOrderRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection	_orderRoutestephandlerHistoryCollection;
		private bool	_alwaysFetchOrderRoutestephandlerHistoryCollection, _alreadyFetchedOrderRoutestephandlerHistoryCollection;
		private Obymobi.Data.CollectionClasses.RoutestephandlerCollection	_routestephandlerCollection;
		private bool	_alwaysFetchRoutestephandlerCollection, _alreadyFetchedRoutestephandlerCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ExternalDeliverypointCollection</summary>
			public static readonly string ExternalDeliverypointCollection = "ExternalDeliverypointCollection";
			/// <summary>Member name ExternalMenuCollection</summary>
			public static readonly string ExternalMenuCollection = "ExternalMenuCollection";
			/// <summary>Member name ExternalProductCollection</summary>
			public static readonly string ExternalProductCollection = "ExternalProductCollection";
			/// <summary>Member name ExternalSystemLogCollection</summary>
			public static readonly string ExternalSystemLogCollection = "ExternalSystemLogCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalSystemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ExternalSystemEntityBase() :base("ExternalSystemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		protected ExternalSystemEntityBase(System.Int32 externalSystemId):base("ExternalSystemEntity")
		{
			InitClassFetch(externalSystemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ExternalSystemEntityBase(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse): base("ExternalSystemEntity")
		{
			InitClassFetch(externalSystemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="validator">The custom validator object for this ExternalSystemEntity</param>
		protected ExternalSystemEntityBase(System.Int32 externalSystemId, IValidator validator):base("ExternalSystemEntity")
		{
			InitClassFetch(externalSystemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSystemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalDeliverypointCollection = (Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection)info.GetValue("_externalDeliverypointCollection", typeof(Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection));
			_alwaysFetchExternalDeliverypointCollection = info.GetBoolean("_alwaysFetchExternalDeliverypointCollection");
			_alreadyFetchedExternalDeliverypointCollection = info.GetBoolean("_alreadyFetchedExternalDeliverypointCollection");

			_externalMenuCollection = (Obymobi.Data.CollectionClasses.ExternalMenuCollection)info.GetValue("_externalMenuCollection", typeof(Obymobi.Data.CollectionClasses.ExternalMenuCollection));
			_alwaysFetchExternalMenuCollection = info.GetBoolean("_alwaysFetchExternalMenuCollection");
			_alreadyFetchedExternalMenuCollection = info.GetBoolean("_alreadyFetchedExternalMenuCollection");

			_externalProductCollection = (Obymobi.Data.CollectionClasses.ExternalProductCollection)info.GetValue("_externalProductCollection", typeof(Obymobi.Data.CollectionClasses.ExternalProductCollection));
			_alwaysFetchExternalProductCollection = info.GetBoolean("_alwaysFetchExternalProductCollection");
			_alreadyFetchedExternalProductCollection = info.GetBoolean("_alreadyFetchedExternalProductCollection");

			_externalSystemLogCollection = (Obymobi.Data.CollectionClasses.ExternalSystemLogCollection)info.GetValue("_externalSystemLogCollection", typeof(Obymobi.Data.CollectionClasses.ExternalSystemLogCollection));
			_alwaysFetchExternalSystemLogCollection = info.GetBoolean("_alwaysFetchExternalSystemLogCollection");
			_alreadyFetchedExternalSystemLogCollection = info.GetBoolean("_alreadyFetchedExternalSystemLogCollection");

			_orderRoutestephandlerCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection)info.GetValue("_orderRoutestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection));
			_alwaysFetchOrderRoutestephandlerCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerCollection");
			_alreadyFetchedOrderRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerCollection");

			_orderRoutestephandlerHistoryCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection)info.GetValue("_orderRoutestephandlerHistoryCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection));
			_alwaysFetchOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerHistoryCollection");
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerHistoryCollection");

			_routestephandlerCollection = (Obymobi.Data.CollectionClasses.RoutestephandlerCollection)info.GetValue("_routestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.RoutestephandlerCollection));
			_alwaysFetchRoutestephandlerCollection = info.GetBoolean("_alwaysFetchRoutestephandlerCollection");
			_alreadyFetchedRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedRoutestephandlerCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalSystemFieldIndex)fieldIndex)
			{
				case ExternalSystemFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalDeliverypointCollection = (_externalDeliverypointCollection.Count > 0);
			_alreadyFetchedExternalMenuCollection = (_externalMenuCollection.Count > 0);
			_alreadyFetchedExternalProductCollection = (_externalProductCollection.Count > 0);
			_alreadyFetchedExternalSystemLogCollection = (_externalSystemLogCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerCollection = (_orderRoutestephandlerCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = (_orderRoutestephandlerHistoryCollection.Count > 0);
			_alreadyFetchedRoutestephandlerCollection = (_routestephandlerCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ExternalDeliverypointCollection":
					toReturn.Add(Relations.ExternalDeliverypointEntityUsingExternalSystemId);
					break;
				case "ExternalMenuCollection":
					toReturn.Add(Relations.ExternalMenuEntityUsingExternalSystemId);
					break;
				case "ExternalProductCollection":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalSystemId);
					break;
				case "ExternalSystemLogCollection":
					toReturn.Add(Relations.ExternalSystemLogEntityUsingExternalSystemId);
					break;
				case "OrderRoutestephandlerCollection":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingExternalSystemId);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingExternalSystemId);
					break;
				case "RoutestephandlerCollection":
					toReturn.Add(Relations.RoutestephandlerEntityUsingExternalSystemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalDeliverypointCollection", (!this.MarkedForDeletion?_externalDeliverypointCollection:null));
			info.AddValue("_alwaysFetchExternalDeliverypointCollection", _alwaysFetchExternalDeliverypointCollection);
			info.AddValue("_alreadyFetchedExternalDeliverypointCollection", _alreadyFetchedExternalDeliverypointCollection);
			info.AddValue("_externalMenuCollection", (!this.MarkedForDeletion?_externalMenuCollection:null));
			info.AddValue("_alwaysFetchExternalMenuCollection", _alwaysFetchExternalMenuCollection);
			info.AddValue("_alreadyFetchedExternalMenuCollection", _alreadyFetchedExternalMenuCollection);
			info.AddValue("_externalProductCollection", (!this.MarkedForDeletion?_externalProductCollection:null));
			info.AddValue("_alwaysFetchExternalProductCollection", _alwaysFetchExternalProductCollection);
			info.AddValue("_alreadyFetchedExternalProductCollection", _alreadyFetchedExternalProductCollection);
			info.AddValue("_externalSystemLogCollection", (!this.MarkedForDeletion?_externalSystemLogCollection:null));
			info.AddValue("_alwaysFetchExternalSystemLogCollection", _alwaysFetchExternalSystemLogCollection);
			info.AddValue("_alreadyFetchedExternalSystemLogCollection", _alreadyFetchedExternalSystemLogCollection);
			info.AddValue("_orderRoutestephandlerCollection", (!this.MarkedForDeletion?_orderRoutestephandlerCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerCollection", _alwaysFetchOrderRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerCollection", _alreadyFetchedOrderRoutestephandlerCollection);
			info.AddValue("_orderRoutestephandlerHistoryCollection", (!this.MarkedForDeletion?_orderRoutestephandlerHistoryCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerHistoryCollection", _alwaysFetchOrderRoutestephandlerHistoryCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerHistoryCollection", _alreadyFetchedOrderRoutestephandlerHistoryCollection);
			info.AddValue("_routestephandlerCollection", (!this.MarkedForDeletion?_routestephandlerCollection:null));
			info.AddValue("_alwaysFetchRoutestephandlerCollection", _alwaysFetchRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedRoutestephandlerCollection", _alreadyFetchedRoutestephandlerCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ExternalDeliverypointCollection":
					_alreadyFetchedExternalDeliverypointCollection = true;
					if(entity!=null)
					{
						this.ExternalDeliverypointCollection.Add((ExternalDeliverypointEntity)entity);
					}
					break;
				case "ExternalMenuCollection":
					_alreadyFetchedExternalMenuCollection = true;
					if(entity!=null)
					{
						this.ExternalMenuCollection.Add((ExternalMenuEntity)entity);
					}
					break;
				case "ExternalProductCollection":
					_alreadyFetchedExternalProductCollection = true;
					if(entity!=null)
					{
						this.ExternalProductCollection.Add((ExternalProductEntity)entity);
					}
					break;
				case "ExternalSystemLogCollection":
					_alreadyFetchedExternalSystemLogCollection = true;
					if(entity!=null)
					{
						this.ExternalSystemLogCollection.Add((ExternalSystemLogEntity)entity);
					}
					break;
				case "OrderRoutestephandlerCollection":
					_alreadyFetchedOrderRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)entity);
					}
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)entity);
					}
					break;
				case "RoutestephandlerCollection":
					_alreadyFetchedRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.RoutestephandlerCollection.Add((RoutestephandlerEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ExternalDeliverypointCollection":
					_externalDeliverypointCollection.Add((ExternalDeliverypointEntity)relatedEntity);
					break;
				case "ExternalMenuCollection":
					_externalMenuCollection.Add((ExternalMenuEntity)relatedEntity);
					break;
				case "ExternalProductCollection":
					_externalProductCollection.Add((ExternalProductEntity)relatedEntity);
					break;
				case "ExternalSystemLogCollection":
					_externalSystemLogCollection.Add((ExternalSystemLogEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerCollection":
					_orderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_orderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)relatedEntity);
					break;
				case "RoutestephandlerCollection":
					_routestephandlerCollection.Add((RoutestephandlerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ExternalDeliverypointCollection":
					this.PerformRelatedEntityRemoval(_externalDeliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalMenuCollection":
					this.PerformRelatedEntityRemoval(_externalMenuCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalProductCollection":
					this.PerformRelatedEntityRemoval(_externalProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalSystemLogCollection":
					this.PerformRelatedEntityRemoval(_externalSystemLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_routestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_externalDeliverypointCollection);
			toReturn.Add(_externalMenuCollection);
			toReturn.Add(_externalProductCollection);
			toReturn.Add(_externalSystemLogCollection);
			toReturn.Add(_orderRoutestephandlerCollection);
			toReturn.Add(_orderRoutestephandlerHistoryCollection);
			toReturn.Add(_routestephandlerCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalSystemId)
		{
			return FetchUsingPK(externalSystemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(externalSystemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(externalSystemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(externalSystemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ExternalSystemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalSystemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection GetMultiExternalDeliverypointCollection(bool forceFetch)
		{
			return GetMultiExternalDeliverypointCollection(forceFetch, _externalDeliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalDeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection GetMultiExternalDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalDeliverypointCollection(forceFetch, _externalDeliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection GetMultiExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalDeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection GetMultiExternalDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalDeliverypointCollection || forceFetch || _alwaysFetchExternalDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalDeliverypointCollection);
				_externalDeliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_externalDeliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalDeliverypointCollection.GetMultiManyToOne(this, filter);
				_externalDeliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalDeliverypointCollection = true;
			}
			return _externalDeliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalDeliverypointCollection'. These settings will be taken into account
		/// when the property ExternalDeliverypointCollection is requested or GetMultiExternalDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalDeliverypointCollection.SortClauses=sortClauses;
			_externalDeliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalMenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalMenuCollection GetMultiExternalMenuCollection(bool forceFetch)
		{
			return GetMultiExternalMenuCollection(forceFetch, _externalMenuCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalMenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalMenuCollection GetMultiExternalMenuCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalMenuCollection(forceFetch, _externalMenuCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalMenuCollection GetMultiExternalMenuCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalMenuCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalMenuCollection GetMultiExternalMenuCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalMenuCollection || forceFetch || _alwaysFetchExternalMenuCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalMenuCollection);
				_externalMenuCollection.SuppressClearInGetMulti=!forceFetch;
				_externalMenuCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalMenuCollection.GetMultiManyToOne(null, this, filter);
				_externalMenuCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalMenuCollection = true;
			}
			return _externalMenuCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalMenuCollection'. These settings will be taken into account
		/// when the property ExternalMenuCollection is requested or GetMultiExternalMenuCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalMenuCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalMenuCollection.SortClauses=sortClauses;
			_externalMenuCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch)
		{
			return GetMultiExternalProductCollection(forceFetch, _externalProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalProductCollection(forceFetch, _externalProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalProductCollection || forceFetch || _alwaysFetchExternalProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalProductCollection);
				_externalProductCollection.SuppressClearInGetMulti=!forceFetch;
				_externalProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalProductCollection.GetMultiManyToOne(null, this, filter);
				_externalProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalProductCollection = true;
			}
			return _externalProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalProductCollection'. These settings will be taken into account
		/// when the property ExternalProductCollection is requested or GetMultiExternalProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalProductCollection.SortClauses=sortClauses;
			_externalProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSystemLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, _externalSystemLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSystemLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, _externalSystemLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalSystemLogCollection || forceFetch || _alwaysFetchExternalSystemLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalSystemLogCollection);
				_externalSystemLogCollection.SuppressClearInGetMulti=!forceFetch;
				_externalSystemLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalSystemLogCollection.GetMultiManyToOne(this, null, filter);
				_externalSystemLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalSystemLogCollection = true;
			}
			return _externalSystemLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalSystemLogCollection'. These settings will be taken into account
		/// when the property ExternalSystemLogCollection is requested or GetMultiExternalSystemLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalSystemLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalSystemLogCollection.SortClauses=sortClauses;
			_externalSystemLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerCollection || forceFetch || _alwaysFetchOrderRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerCollection);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerCollection = true;
			}
			return _orderRoutestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerCollection is requested or GetMultiOrderRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerCollection.SortClauses=sortClauses;
			_orderRoutestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerHistoryCollection || forceFetch || _alwaysFetchOrderRoutestephandlerHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerHistoryCollection);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerHistoryCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
			}
			return _orderRoutestephandlerHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerHistoryCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerHistoryCollection is requested or GetMultiOrderRoutestephandlerHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerHistoryCollection.SortClauses=sortClauses;
			_orderRoutestephandlerHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutestephandlerCollection || forceFetch || _alwaysFetchRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestephandlerCollection);
				_routestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_routestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_routestephandlerCollection.GetMultiManyToOne(this, null, null, null, filter);
				_routestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestephandlerCollection = true;
			}
			return _routestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestephandlerCollection'. These settings will be taken into account
		/// when the property RoutestephandlerCollection is requested or GetMultiRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestephandlerCollection.SortClauses=sortClauses;
			_routestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ExternalDeliverypointCollection", _externalDeliverypointCollection);
			toReturn.Add("ExternalMenuCollection", _externalMenuCollection);
			toReturn.Add("ExternalProductCollection", _externalProductCollection);
			toReturn.Add("ExternalSystemLogCollection", _externalSystemLogCollection);
			toReturn.Add("OrderRoutestephandlerCollection", _orderRoutestephandlerCollection);
			toReturn.Add("OrderRoutestephandlerHistoryCollection", _orderRoutestephandlerHistoryCollection);
			toReturn.Add("RoutestephandlerCollection", _routestephandlerCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="validator">The validator object for this ExternalSystemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 externalSystemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(externalSystemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_externalDeliverypointCollection = new Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection();
			_externalDeliverypointCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_externalMenuCollection = new Obymobi.Data.CollectionClasses.ExternalMenuCollection();
			_externalMenuCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_externalProductCollection = new Obymobi.Data.CollectionClasses.ExternalProductCollection();
			_externalProductCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_externalSystemLogCollection = new Obymobi.Data.CollectionClasses.ExternalSystemLogCollection();
			_externalSystemLogCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_orderRoutestephandlerCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection();
			_orderRoutestephandlerCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_orderRoutestephandlerHistoryCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection();
			_orderRoutestephandlerHistoryCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");

			_routestephandlerCollection = new Obymobi.Data.CollectionClasses.RoutestephandlerCollection();
			_routestephandlerCollection.SetContainingEntityInfo(this, "ExternalSystemEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSystemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StringValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExpiryDateUtc", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Environment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoolValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBusy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticExternalSystemRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ExternalSystemCollection", resetFKFields, new int[] { (int)ExternalSystemFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticExternalSystemRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalSystemFieldIndex.ExternalSystemId].ForcedCurrentValueWrite(externalSystemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalSystemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalSystemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalSystemRelations Relations
		{
			get	{ return new ExternalSystemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalDeliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection(), (IEntityRelation)GetRelationsForField("ExternalDeliverypointCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.ExternalDeliverypointEntity, 0, null, null, null, "ExternalDeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalMenu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalMenuCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalMenuCollection(), (IEntityRelation)GetRelationsForField("ExternalMenuCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.ExternalMenuEntity, 0, null, null, null, "ExternalMenuCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystemLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemLogCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemLogCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.ExternalSystemLogEntity, 0, null, null, null, "ExternalSystemLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, 0, null, null, null, "OrderRoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerHistoryCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity, 0, null, null, null, "OrderRoutestephandlerHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ExternalSystemEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ExternalSystemId property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."ExternalSystemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.ExternalSystemId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalSystemFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Name, value, true); }
		}

		/// <summary> The Type property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ExternalSystemType Type
		{
			get { return (Obymobi.Enums.ExternalSystemType)GetValue((int)ExternalSystemFieldIndex.Type, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Type, value, true); }
		}

		/// <summary> The StringValue1 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue1
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue1, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue1, value, true); }
		}

		/// <summary> The StringValue2 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue2
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue2, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue2, value, true); }
		}

		/// <summary> The StringValue3 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue3
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue3, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue3, value, true); }
		}

		/// <summary> The StringValue4 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue4
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue4, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue4, value, true); }
		}

		/// <summary> The StringValue5 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."StringValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StringValue5
		{
			get { return (System.String)GetValue((int)ExternalSystemFieldIndex.StringValue5, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.StringValue5, value, true); }
		}

		/// <summary> The ExpiryDateUtc property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."ExpiryDateUtc"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExpiryDateUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSystemFieldIndex.ExpiryDateUtc, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.ExpiryDateUtc, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalSystemFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalSystemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSystemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ExternalSystemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Environment property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."Environment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ExternalSystemEnvironment Environment
		{
			get { return (Obymobi.Enums.ExternalSystemEnvironment)GetValue((int)ExternalSystemFieldIndex.Environment, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.Environment, value, true); }
		}

		/// <summary> The BoolValue1 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue1
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue1, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue1, value, true); }
		}

		/// <summary> The BoolValue2 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue2
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue2, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue2, value, true); }
		}

		/// <summary> The BoolValue3 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue3
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue3, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue3, value, true); }
		}

		/// <summary> The BoolValue4 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue4
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue4, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue4, value, true); }
		}

		/// <summary> The BoolValue5 property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."BoolValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BoolValue5
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ExternalSystemFieldIndex.BoolValue5, false); }
			set	{ SetValue((int)ExternalSystemFieldIndex.BoolValue5, value, true); }
		}

		/// <summary> The IsBusy property of the Entity ExternalSystem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSystem"."IsBusy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBusy
		{
			get { return (System.Boolean)GetValue((int)ExternalSystemFieldIndex.IsBusy, true); }
			set	{ SetValue((int)ExternalSystemFieldIndex.IsBusy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ExternalDeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalDeliverypointCollection ExternalDeliverypointCollection
		{
			get	{ return GetMultiExternalDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalDeliverypointCollection. When set to true, ExternalDeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalDeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalDeliverypointCollection
		{
			get	{ return _alwaysFetchExternalDeliverypointCollection; }
			set	{ _alwaysFetchExternalDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalDeliverypointCollection already has been fetched. Setting this property to false when ExternalDeliverypointCollection has been fetched
		/// will clear the ExternalDeliverypointCollection collection well. Setting this property to true while ExternalDeliverypointCollection hasn't been fetched disables lazy loading for ExternalDeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalDeliverypointCollection
		{
			get { return _alreadyFetchedExternalDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedExternalDeliverypointCollection && !value && (_externalDeliverypointCollection != null))
				{
					_externalDeliverypointCollection.Clear();
				}
				_alreadyFetchedExternalDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalMenuEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalMenuCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalMenuCollection ExternalMenuCollection
		{
			get	{ return GetMultiExternalMenuCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalMenuCollection. When set to true, ExternalMenuCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalMenuCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalMenuCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalMenuCollection
		{
			get	{ return _alwaysFetchExternalMenuCollection; }
			set	{ _alwaysFetchExternalMenuCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalMenuCollection already has been fetched. Setting this property to false when ExternalMenuCollection has been fetched
		/// will clear the ExternalMenuCollection collection well. Setting this property to true while ExternalMenuCollection hasn't been fetched disables lazy loading for ExternalMenuCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalMenuCollection
		{
			get { return _alreadyFetchedExternalMenuCollection;}
			set 
			{
				if(_alreadyFetchedExternalMenuCollection && !value && (_externalMenuCollection != null))
				{
					_externalMenuCollection.Clear();
				}
				_alreadyFetchedExternalMenuCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalProductCollection ExternalProductCollection
		{
			get	{ return GetMultiExternalProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductCollection. When set to true, ExternalProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductCollection
		{
			get	{ return _alwaysFetchExternalProductCollection; }
			set	{ _alwaysFetchExternalProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductCollection already has been fetched. Setting this property to false when ExternalProductCollection has been fetched
		/// will clear the ExternalProductCollection collection well. Setting this property to true while ExternalProductCollection hasn't been fetched disables lazy loading for ExternalProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductCollection
		{
			get { return _alreadyFetchedExternalProductCollection;}
			set 
			{
				if(_alreadyFetchedExternalProductCollection && !value && (_externalProductCollection != null))
				{
					_externalProductCollection.Clear();
				}
				_alreadyFetchedExternalProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalSystemLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalSystemLogCollection ExternalSystemLogCollection
		{
			get	{ return GetMultiExternalSystemLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemLogCollection. When set to true, ExternalSystemLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalSystemLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemLogCollection
		{
			get	{ return _alwaysFetchExternalSystemLogCollection; }
			set	{ _alwaysFetchExternalSystemLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemLogCollection already has been fetched. Setting this property to false when ExternalSystemLogCollection has been fetched
		/// will clear the ExternalSystemLogCollection collection well. Setting this property to true while ExternalSystemLogCollection hasn't been fetched disables lazy loading for ExternalSystemLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemLogCollection
		{
			get { return _alreadyFetchedExternalSystemLogCollection;}
			set 
			{
				if(_alreadyFetchedExternalSystemLogCollection && !value && (_externalSystemLogCollection != null))
				{
					_externalSystemLogCollection.Clear();
				}
				_alreadyFetchedExternalSystemLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection OrderRoutestephandlerCollection
		{
			get	{ return GetMultiOrderRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerCollection. When set to true, OrderRoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerCollection already has been fetched. Setting this property to false when OrderRoutestephandlerCollection has been fetched
		/// will clear the OrderRoutestephandlerCollection collection well. Setting this property to true while OrderRoutestephandlerCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerCollection && !value && (_orderRoutestephandlerCollection != null))
				{
					_orderRoutestephandlerCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection OrderRoutestephandlerHistoryCollection
		{
			get	{ return GetMultiOrderRoutestephandlerHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerHistoryCollection. When set to true, OrderRoutestephandlerHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerHistoryCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerHistoryCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerHistoryCollection already has been fetched. Setting this property to false when OrderRoutestephandlerHistoryCollection has been fetched
		/// will clear the OrderRoutestephandlerHistoryCollection collection well. Setting this property to true while OrderRoutestephandlerHistoryCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerHistoryCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerHistoryCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerHistoryCollection && !value && (_orderRoutestephandlerHistoryCollection != null))
				{
					_orderRoutestephandlerHistoryCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection RoutestephandlerCollection
		{
			get	{ return GetMultiRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerCollection. When set to true, RoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerCollection
		{
			get	{ return _alwaysFetchRoutestephandlerCollection; }
			set	{ _alwaysFetchRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerCollection already has been fetched. Setting this property to false when RoutestephandlerCollection has been fetched
		/// will clear the RoutestephandlerCollection collection well. Setting this property to true while RoutestephandlerCollection hasn't been fetched disables lazy loading for RoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerCollection
		{
			get { return _alreadyFetchedRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerCollection && !value && (_routestephandlerCollection != null))
				{
					_routestephandlerCollection.Clear();
				}
				_alreadyFetchedRoutestephandlerCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalSystemCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ExternalSystemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
