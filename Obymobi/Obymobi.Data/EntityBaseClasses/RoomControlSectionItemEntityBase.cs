﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'RoomControlSectionItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoomControlSectionItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoomControlSectionItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection	_roomControlSectionItemLanguageCollection;
		private bool	_alwaysFetchRoomControlSectionItemLanguageCollection, _alreadyFetchedRoomControlSectionItemLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection;
		private bool	_alwaysFetchRoomControlWidgetCollection, _alreadyFetchedRoomControlWidgetCollection;
		private InfraredConfigurationEntity _infraredConfigurationEntity;
		private bool	_alwaysFetchInfraredConfigurationEntity, _alreadyFetchedInfraredConfigurationEntity, _infraredConfigurationEntityReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;
		private StationListEntity _stationListEntity;
		private bool	_alwaysFetchStationListEntity, _alreadyFetchedStationListEntity, _stationListEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name InfraredConfigurationEntity</summary>
			public static readonly string InfraredConfigurationEntity = "InfraredConfigurationEntity";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name StationListEntity</summary>
			public static readonly string StationListEntity = "StationListEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name RoomControlSectionItemLanguageCollection</summary>
			public static readonly string RoomControlSectionItemLanguageCollection = "RoomControlSectionItemLanguageCollection";
			/// <summary>Member name RoomControlWidgetCollection</summary>
			public static readonly string RoomControlWidgetCollection = "RoomControlWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoomControlSectionItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoomControlSectionItemEntityBase() :base("RoomControlSectionItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		protected RoomControlSectionItemEntityBase(System.Int32 roomControlSectionItemId):base("RoomControlSectionItemEntity")
		{
			InitClassFetch(roomControlSectionItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoomControlSectionItemEntityBase(System.Int32 roomControlSectionItemId, IPrefetchPath prefetchPathToUse): base("RoomControlSectionItemEntity")
		{
			InitClassFetch(roomControlSectionItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="validator">The custom validator object for this RoomControlSectionItemEntity</param>
		protected RoomControlSectionItemEntityBase(System.Int32 roomControlSectionItemId, IValidator validator):base("RoomControlSectionItemEntity")
		{
			InitClassFetch(roomControlSectionItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlSectionItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_roomControlSectionItemLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection)info.GetValue("_roomControlSectionItemLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection));
			_alwaysFetchRoomControlSectionItemLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlSectionItemLanguageCollection");
			_alreadyFetchedRoomControlSectionItemLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionItemLanguageCollection");

			_roomControlWidgetCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection");
			_alreadyFetchedRoomControlWidgetCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection");
			_infraredConfigurationEntity = (InfraredConfigurationEntity)info.GetValue("_infraredConfigurationEntity", typeof(InfraredConfigurationEntity));
			if(_infraredConfigurationEntity!=null)
			{
				_infraredConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_infraredConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_infraredConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchInfraredConfigurationEntity = info.GetBoolean("_alwaysFetchInfraredConfigurationEntity");
			_alreadyFetchedInfraredConfigurationEntity = info.GetBoolean("_alreadyFetchedInfraredConfigurationEntity");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");

			_stationListEntity = (StationListEntity)info.GetValue("_stationListEntity", typeof(StationListEntity));
			if(_stationListEntity!=null)
			{
				_stationListEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_stationListEntityReturnsNewIfNotFound = info.GetBoolean("_stationListEntityReturnsNewIfNotFound");
			_alwaysFetchStationListEntity = info.GetBoolean("_alwaysFetchStationListEntity");
			_alreadyFetchedStationListEntity = info.GetBoolean("_alreadyFetchedStationListEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoomControlSectionItemFieldIndex)fieldIndex)
			{
				case RoomControlSectionItemFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				case RoomControlSectionItemFieldIndex.StationListId:
					DesetupSyncStationListEntity(true, false);
					_alreadyFetchedStationListEntity = false;
					break;
				case RoomControlSectionItemFieldIndex.InfraredConfigurationId:
					DesetupSyncInfraredConfigurationEntity(true, false);
					_alreadyFetchedInfraredConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedRoomControlSectionItemLanguageCollection = (_roomControlSectionItemLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection = (_roomControlWidgetCollection.Count > 0);
			_alreadyFetchedInfraredConfigurationEntity = (_infraredConfigurationEntity != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
			_alreadyFetchedStationListEntity = (_stationListEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					toReturn.Add(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "StationListEntity":
					toReturn.Add(Relations.StationListEntityUsingStationListId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingRoomControlSectionItemId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingRoomControlSectionItemId);
					break;
				case "RoomControlSectionItemLanguageCollection":
					toReturn.Add(Relations.RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemId);
					break;
				case "RoomControlWidgetCollection":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlSectionItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_roomControlSectionItemLanguageCollection", (!this.MarkedForDeletion?_roomControlSectionItemLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionItemLanguageCollection", _alwaysFetchRoomControlSectionItemLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionItemLanguageCollection", _alreadyFetchedRoomControlSectionItemLanguageCollection);
			info.AddValue("_roomControlWidgetCollection", (!this.MarkedForDeletion?_roomControlWidgetCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection", _alwaysFetchRoomControlWidgetCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection", _alreadyFetchedRoomControlWidgetCollection);
			info.AddValue("_infraredConfigurationEntity", (!this.MarkedForDeletion?_infraredConfigurationEntity:null));
			info.AddValue("_infraredConfigurationEntityReturnsNewIfNotFound", _infraredConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInfraredConfigurationEntity", _alwaysFetchInfraredConfigurationEntity);
			info.AddValue("_alreadyFetchedInfraredConfigurationEntity", _alreadyFetchedInfraredConfigurationEntity);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);
			info.AddValue("_stationListEntity", (!this.MarkedForDeletion?_stationListEntity:null));
			info.AddValue("_stationListEntityReturnsNewIfNotFound", _stationListEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchStationListEntity", _alwaysFetchStationListEntity);
			info.AddValue("_alreadyFetchedStationListEntity", _alreadyFetchedStationListEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "InfraredConfigurationEntity":
					_alreadyFetchedInfraredConfigurationEntity = true;
					this.InfraredConfigurationEntity = (InfraredConfigurationEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "StationListEntity":
					_alreadyFetchedStationListEntity = true;
					this.StationListEntity = (StationListEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "RoomControlSectionItemLanguageCollection":
					_alreadyFetchedRoomControlSectionItemLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionItemLanguageCollection.Add((RoomControlSectionItemLanguageEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection":
					_alreadyFetchedRoomControlWidgetCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection.Add((RoomControlWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					SetupSyncInfraredConfigurationEntity(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "StationListEntity":
					SetupSyncStationListEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "RoomControlSectionItemLanguageCollection":
					_roomControlSectionItemLanguageCollection.Add((RoomControlSectionItemLanguageEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection":
					_roomControlWidgetCollection.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					DesetupSyncInfraredConfigurationEntity(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "StationListEntity":
					DesetupSyncStationListEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionItemLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionItemLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_infraredConfigurationEntity!=null)
			{
				toReturn.Add(_infraredConfigurationEntity);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			if(_stationListEntity!=null)
			{
				toReturn.Add(_stationListEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_roomControlSectionItemLanguageCollection);
			toReturn.Add(_roomControlWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionItemId)
		{
			return FetchUsingPK(roomControlSectionItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roomControlSectionItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roomControlSectionItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlSectionItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roomControlSectionItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoomControlSectionItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoomControlSectionItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, _roomControlSectionItemLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, _roomControlSectionItemLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionItemLanguageCollection || forceFetch || _alwaysFetchRoomControlSectionItemLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionItemLanguageCollection);
				_roomControlSectionItemLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionItemLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionItemLanguageCollection.GetMultiManyToOne(null, this, filter);
				_roomControlSectionItemLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionItemLanguageCollection = true;
			}
			return _roomControlSectionItemLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionItemLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlSectionItemLanguageCollection is requested or GetMultiRoomControlSectionItemLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionItemLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionItemLanguageCollection.SortClauses=sortClauses;
			_roomControlSectionItemLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection || forceFetch || _alwaysFetchRoomControlWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection);
				_roomControlWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_roomControlWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection = true;
			}
			return _roomControlWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection is requested or GetMultiRoomControlWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection.SortClauses=sortClauses;
			_roomControlWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public InfraredConfigurationEntity GetSingleInfraredConfigurationEntity()
		{
			return GetSingleInfraredConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public virtual InfraredConfigurationEntity GetSingleInfraredConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedInfraredConfigurationEntity || forceFetch || _alwaysFetchInfraredConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
				InfraredConfigurationEntity newEntity = new InfraredConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InfraredConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InfraredConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_infraredConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InfraredConfigurationEntity = newEntity;
				_alreadyFetchedInfraredConfigurationEntity = fetchResult;
			}
			return _infraredConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId);
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary> Retrieves the related entity of type 'StationListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StationListEntity' which is related to this entity.</returns>
		public StationListEntity GetSingleStationListEntity()
		{
			return GetSingleStationListEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'StationListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StationListEntity' which is related to this entity.</returns>
		public virtual StationListEntity GetSingleStationListEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedStationListEntity || forceFetch || _alwaysFetchStationListEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StationListEntityUsingStationListId);
				StationListEntity newEntity = new StationListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StationListId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (StationListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_stationListEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.StationListEntity = newEntity;
				_alreadyFetchedStationListEntity = fetchResult;
			}
			return _stationListEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("InfraredConfigurationEntity", _infraredConfigurationEntity);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("StationListEntity", _stationListEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("RoomControlSectionItemLanguageCollection", _roomControlSectionItemLanguageCollection);
			toReturn.Add("RoomControlWidgetCollection", _roomControlWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="validator">The validator object for this RoomControlSectionItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roomControlSectionItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roomControlSectionItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "RoomControlSectionItemEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "RoomControlSectionItemEntity");

			_roomControlSectionItemLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection();
			_roomControlSectionItemLanguageCollection.SetContainingEntityInfo(this, "RoomControlSectionItemEntity");

			_roomControlWidgetCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection.SetContainingEntityInfo(this, "RoomControlSectionItemEntity");
			_infraredConfigurationEntityReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			_stationListEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StationListId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredConfigurationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInfraredConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, signalRelatedEntity, "RoomControlSectionItemCollection", resetFKFields, new int[] { (int)RoomControlSectionItemFieldIndex.InfraredConfigurationId } );		
			_infraredConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInfraredConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_infraredConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncInfraredConfigurationEntity(true, true);
				_infraredConfigurationEntity = (InfraredConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, ref _alreadyFetchedInfraredConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfraredConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "RoomControlSectionItemCollection", resetFKFields, new int[] { (int)RoomControlSectionItemFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _stationListEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncStationListEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _stationListEntity, new PropertyChangedEventHandler( OnStationListEntityPropertyChanged ), "StationListEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.StationListEntityUsingStationListIdStatic, true, signalRelatedEntity, "RoomControlSectionItemCollection", resetFKFields, new int[] { (int)RoomControlSectionItemFieldIndex.StationListId } );		
			_stationListEntity = null;
		}
		
		/// <summary> setups the sync logic for member _stationListEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncStationListEntity(IEntityCore relatedEntity)
		{
			if(_stationListEntity!=relatedEntity)
			{		
				DesetupSyncStationListEntity(true, true);
				_stationListEntity = (StationListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _stationListEntity, new PropertyChangedEventHandler( OnStationListEntityPropertyChanged ), "StationListEntity", Obymobi.Data.RelationClasses.StaticRoomControlSectionItemRelations.StationListEntityUsingStationListIdStatic, true, ref _alreadyFetchedStationListEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStationListEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roomControlSectionItemId">PK value for RoomControlSectionItem which data should be fetched into this RoomControlSectionItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roomControlSectionItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoomControlSectionItemFieldIndex.RoomControlSectionItemId].ForcedCurrentValueWrite(roomControlSectionItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlSectionItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoomControlSectionItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoomControlSectionItemRelations Relations
		{
			get	{ return new RoomControlSectionItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItemLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemLanguageCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity, 0, null, null, null, "RoomControlSectionItemLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredConfigurationCollection(), (IEntityRelation)GetRelationsForField("InfraredConfigurationEntity")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, 0, null, null, null, "InfraredConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StationList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStationListEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.StationListCollection(), (IEntityRelation)GetRelationsForField("StationListEntity")[0], (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, (int)Obymobi.Data.EntityType.StationListEntity, 0, null, null, null, "StationListEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoomControlSectionItemId property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."RoomControlSectionItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoomControlSectionItemId
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionItemFieldIndex.RoomControlSectionItemId, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.RoomControlSectionItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSectionId
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionItemFieldIndex.RoomControlSectionId, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The Name property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.Name, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.Name, value, true); }
		}

		/// <summary> The Type property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RoomControlSectionItemType Type
		{
			get { return (Obymobi.Enums.RoomControlSectionItemType)GetValue((int)RoomControlSectionItemFieldIndex.Type, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.Type, value, true); }
		}

		/// <summary> The SortOrder property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)RoomControlSectionItemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlSectionItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlSectionItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The StationListId property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."StationListId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StationListId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionItemFieldIndex.StationListId, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.StationListId, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)RoomControlSectionItemFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The Visible property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)RoomControlSectionItemFieldIndex.Visible, true); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.Visible, value, true); }
		}

		/// <summary> The InfraredConfigurationId property of the Entity RoomControlSectionItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlSectionItem"."InfraredConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InfraredConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlSectionItemFieldIndex.InfraredConfigurationId, false); }
			set	{ SetValue((int)RoomControlSectionItemFieldIndex.InfraredConfigurationId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionItemLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection RoomControlSectionItemLanguageCollection
		{
			get	{ return GetMultiRoomControlSectionItemLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemLanguageCollection. When set to true, RoomControlSectionItemLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionItemLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlSectionItemLanguageCollection; }
			set	{ _alwaysFetchRoomControlSectionItemLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemLanguageCollection already has been fetched. Setting this property to false when RoomControlSectionItemLanguageCollection has been fetched
		/// will clear the RoomControlSectionItemLanguageCollection collection well. Setting this property to true while RoomControlSectionItemLanguageCollection hasn't been fetched disables lazy loading for RoomControlSectionItemLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemLanguageCollection
		{
			get { return _alreadyFetchedRoomControlSectionItemLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemLanguageCollection && !value && (_roomControlSectionItemLanguageCollection != null))
				{
					_roomControlSectionItemLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionItemLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection
		{
			get	{ return GetMultiRoomControlWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection. When set to true, RoomControlWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection; }
			set	{ _alwaysFetchRoomControlWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection already has been fetched. Setting this property to false when RoomControlWidgetCollection has been fetched
		/// will clear the RoomControlWidgetCollection collection well. Setting this property to true while RoomControlWidgetCollection hasn't been fetched disables lazy loading for RoomControlWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection
		{
			get { return _alreadyFetchedRoomControlWidgetCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection && !value && (_roomControlWidgetCollection != null))
				{
					_roomControlWidgetCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'InfraredConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInfraredConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual InfraredConfigurationEntity InfraredConfigurationEntity
		{
			get	{ return GetSingleInfraredConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInfraredConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlSectionItemCollection", "InfraredConfigurationEntity", _infraredConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredConfigurationEntity. When set to true, InfraredConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleInfraredConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredConfigurationEntity
		{
			get	{ return _alwaysFetchInfraredConfigurationEntity; }
			set	{ _alwaysFetchInfraredConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredConfigurationEntity already has been fetched. Setting this property to false when InfraredConfigurationEntity has been fetched
		/// will set InfraredConfigurationEntity to null as well. Setting this property to true while InfraredConfigurationEntity hasn't been fetched disables lazy loading for InfraredConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredConfigurationEntity
		{
			get { return _alreadyFetchedInfraredConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedInfraredConfigurationEntity && !value)
				{
					this.InfraredConfigurationEntity = null;
				}
				_alreadyFetchedInfraredConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InfraredConfigurationEntity is not found
		/// in the database. When set to true, InfraredConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool InfraredConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _infraredConfigurationEntityReturnsNewIfNotFound; }
			set { _infraredConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlSectionItemCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StationListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleStationListEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual StationListEntity StationListEntity
		{
			get	{ return GetSingleStationListEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncStationListEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlSectionItemCollection", "StationListEntity", _stationListEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for StationListEntity. When set to true, StationListEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StationListEntity is accessed. You can always execute a forced fetch by calling GetSingleStationListEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStationListEntity
		{
			get	{ return _alwaysFetchStationListEntity; }
			set	{ _alwaysFetchStationListEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StationListEntity already has been fetched. Setting this property to false when StationListEntity has been fetched
		/// will set StationListEntity to null as well. Setting this property to true while StationListEntity hasn't been fetched disables lazy loading for StationListEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStationListEntity
		{
			get { return _alreadyFetchedStationListEntity;}
			set 
			{
				if(_alreadyFetchedStationListEntity && !value)
				{
					this.StationListEntity = null;
				}
				_alreadyFetchedStationListEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property StationListEntity is not found
		/// in the database. When set to true, StationListEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool StationListEntityReturnsNewIfNotFound
		{
			get	{ return _stationListEntityReturnsNewIfNotFound; }
			set { _stationListEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
