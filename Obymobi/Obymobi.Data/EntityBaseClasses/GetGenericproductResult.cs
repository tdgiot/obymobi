﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Data.EntityBaseClasses
{
    public enum GetGenericproductResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Generic product not found for specified code
        /// </summary>
        [StringValue("Geen generiek product gevonden voor opgegeven code.")]
        GenericproductNotFound = 201,

        /// <summary>
        /// Multiple generic products found for specified code
        /// </summary>
        [StringValue("Meerdere generieke producten gevonden voor opgegeven code.")]
        MultipleGenericproductsFound = 202,

        /// <summary>
        /// No product found that is connected to the specified generic product
        /// </summary>
        [StringValue("Er is geen product gevonden dat is gekoppeld aan het opgegeven generieke product.")]
        ProductNotFoundForGenericproduct = 203,

        /// <summary>
        /// Multiple products found for generic product
        /// </summary>
        [StringValue("Meerdere producten gevonden die zijn gekoppeld aan het opgegeven generieke product.")]
        MultipleProductsFoundForGenericproduct = 204,
    }
}
