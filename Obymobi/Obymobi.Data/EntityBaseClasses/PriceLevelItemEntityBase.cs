﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PriceLevelItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PriceLevelItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PriceLevelItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.OrderitemCollection	_orderitemCollection;
		private bool	_alwaysFetchOrderitemCollection, _alreadyFetchedOrderitemCollection;
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection	_orderitemAlterationitemCollection;
		private bool	_alwaysFetchOrderitemAlterationitemCollection, _alreadyFetchedOrderitemAlterationitemCollection;
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationoptionEntity _alterationoptionEntity;
		private bool	_alwaysFetchAlterationoptionEntity, _alreadyFetchedAlterationoptionEntity, _alterationoptionEntityReturnsNewIfNotFound;
		private PriceLevelEntity _priceLevelEntity;
		private bool	_alwaysFetchPriceLevelEntity, _alreadyFetchedPriceLevelEntity, _priceLevelEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationoptionEntity</summary>
			public static readonly string AlterationoptionEntity = "AlterationoptionEntity";
			/// <summary>Member name PriceLevelEntity</summary>
			public static readonly string PriceLevelEntity = "PriceLevelEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PriceLevelItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PriceLevelItemEntityBase() :base("PriceLevelItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		protected PriceLevelItemEntityBase(System.Int32 priceLevelItemId):base("PriceLevelItemEntity")
		{
			InitClassFetch(priceLevelItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PriceLevelItemEntityBase(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse): base("PriceLevelItemEntity")
		{
			InitClassFetch(priceLevelItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="validator">The custom validator object for this PriceLevelItemEntity</param>
		protected PriceLevelItemEntityBase(System.Int32 priceLevelItemId, IValidator validator):base("PriceLevelItemEntity")
		{
			InitClassFetch(priceLevelItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceLevelItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderitemCollection = (Obymobi.Data.CollectionClasses.OrderitemCollection)info.GetValue("_orderitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemCollection));
			_alwaysFetchOrderitemCollection = info.GetBoolean("_alwaysFetchOrderitemCollection");
			_alreadyFetchedOrderitemCollection = info.GetBoolean("_alreadyFetchedOrderitemCollection");

			_orderitemAlterationitemCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection)info.GetValue("_orderitemAlterationitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection));
			_alwaysFetchOrderitemAlterationitemCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemCollection");
			_alreadyFetchedOrderitemAlterationitemCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemCollection");
			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationoptionEntity = (AlterationoptionEntity)info.GetValue("_alterationoptionEntity", typeof(AlterationoptionEntity));
			if(_alterationoptionEntity!=null)
			{
				_alterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_alterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationoptionEntity = info.GetBoolean("_alwaysFetchAlterationoptionEntity");
			_alreadyFetchedAlterationoptionEntity = info.GetBoolean("_alreadyFetchedAlterationoptionEntity");

			_priceLevelEntity = (PriceLevelEntity)info.GetValue("_priceLevelEntity", typeof(PriceLevelEntity));
			if(_priceLevelEntity!=null)
			{
				_priceLevelEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceLevelEntityReturnsNewIfNotFound = info.GetBoolean("_priceLevelEntityReturnsNewIfNotFound");
			_alwaysFetchPriceLevelEntity = info.GetBoolean("_alwaysFetchPriceLevelEntity");
			_alreadyFetchedPriceLevelEntity = info.GetBoolean("_alreadyFetchedPriceLevelEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PriceLevelItemFieldIndex)fieldIndex)
			{
				case PriceLevelItemFieldIndex.PriceLevelId:
					DesetupSyncPriceLevelEntity(true, false);
					_alreadyFetchedPriceLevelEntity = false;
					break;
				case PriceLevelItemFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case PriceLevelItemFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case PriceLevelItemFieldIndex.AlterationoptionId:
					DesetupSyncAlterationoptionEntity(true, false);
					_alreadyFetchedAlterationoptionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderitemCollection = (_orderitemCollection.Count > 0);
			_alreadyFetchedOrderitemAlterationitemCollection = (_orderitemAlterationitemCollection.Count > 0);
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationoptionEntity = (_alterationoptionEntity != null);
			_alreadyFetchedPriceLevelEntity = (_priceLevelEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationoptionEntity":
					toReturn.Add(Relations.AlterationoptionEntityUsingAlterationoptionId);
					break;
				case "PriceLevelEntity":
					toReturn.Add(Relations.PriceLevelEntityUsingPriceLevelId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "OrderitemCollection":
					toReturn.Add(Relations.OrderitemEntityUsingPriceLevelItemId);
					break;
				case "OrderitemAlterationitemCollection":
					toReturn.Add(Relations.OrderitemAlterationitemEntityUsingPriceLevelItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderitemCollection", (!this.MarkedForDeletion?_orderitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemCollection", _alwaysFetchOrderitemCollection);
			info.AddValue("_alreadyFetchedOrderitemCollection", _alreadyFetchedOrderitemCollection);
			info.AddValue("_orderitemAlterationitemCollection", (!this.MarkedForDeletion?_orderitemAlterationitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemCollection", _alwaysFetchOrderitemAlterationitemCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemCollection", _alreadyFetchedOrderitemAlterationitemCollection);
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationoptionEntity", (!this.MarkedForDeletion?_alterationoptionEntity:null));
			info.AddValue("_alterationoptionEntityReturnsNewIfNotFound", _alterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationoptionEntity", _alwaysFetchAlterationoptionEntity);
			info.AddValue("_alreadyFetchedAlterationoptionEntity", _alreadyFetchedAlterationoptionEntity);
			info.AddValue("_priceLevelEntity", (!this.MarkedForDeletion?_priceLevelEntity:null));
			info.AddValue("_priceLevelEntityReturnsNewIfNotFound", _priceLevelEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceLevelEntity", _alwaysFetchPriceLevelEntity);
			info.AddValue("_alreadyFetchedPriceLevelEntity", _alreadyFetchedPriceLevelEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationoptionEntity":
					_alreadyFetchedAlterationoptionEntity = true;
					this.AlterationoptionEntity = (AlterationoptionEntity)entity;
					break;
				case "PriceLevelEntity":
					_alreadyFetchedPriceLevelEntity = true;
					this.PriceLevelEntity = (PriceLevelEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "OrderitemCollection":
					_alreadyFetchedOrderitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemCollection.Add((OrderitemEntity)entity);
					}
					break;
				case "OrderitemAlterationitemCollection":
					_alreadyFetchedOrderitemAlterationitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationoptionEntity":
					SetupSyncAlterationoptionEntity(relatedEntity);
					break;
				case "PriceLevelEntity":
					SetupSyncPriceLevelEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "OrderitemCollection":
					_orderitemCollection.Add((OrderitemEntity)relatedEntity);
					break;
				case "OrderitemAlterationitemCollection":
					_orderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationoptionEntity":
					DesetupSyncAlterationoptionEntity(false, true);
					break;
				case "PriceLevelEntity":
					DesetupSyncPriceLevelEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "OrderitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemAlterationitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationoptionEntity!=null)
			{
				toReturn.Add(_alterationoptionEntity);
			}
			if(_priceLevelEntity!=null)
			{
				toReturn.Add(_priceLevelEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_orderitemCollection);
			toReturn.Add(_orderitemAlterationitemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelItemId)
		{
			return FetchUsingPK(priceLevelItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(priceLevelItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(priceLevelItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(priceLevelItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PriceLevelItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PriceLevelItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemCollection || forceFetch || _alwaysFetchOrderitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemCollection);
				_orderitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_orderitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemCollection = true;
			}
			return _orderitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemCollection'. These settings will be taken into account
		/// when the property OrderitemCollection is requested or GetMultiOrderitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemCollection.SortClauses=sortClauses;
			_orderitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemCollection || forceFetch || _alwaysFetchOrderitemAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemCollection);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemCollection.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemCollection = true;
			}
			return _orderitemAlterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemCollection is requested or GetMultiOrderitemAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemCollection.SortClauses=sortClauses;
			_orderitemAlterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public AlterationoptionEntity GetSingleAlterationoptionEntity()
		{
			return GetSingleAlterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public virtual AlterationoptionEntity GetSingleAlterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationoptionEntity || forceFetch || _alwaysFetchAlterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationoptionEntityUsingAlterationoptionId);
				AlterationoptionEntity newEntity = new AlterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationoptionEntity = newEntity;
				_alreadyFetchedAlterationoptionEntity = fetchResult;
			}
			return _alterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceLevelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceLevelEntity' which is related to this entity.</returns>
		public PriceLevelEntity GetSinglePriceLevelEntity()
		{
			return GetSinglePriceLevelEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceLevelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceLevelEntity' which is related to this entity.</returns>
		public virtual PriceLevelEntity GetSinglePriceLevelEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceLevelEntity || forceFetch || _alwaysFetchPriceLevelEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceLevelEntityUsingPriceLevelId);
				PriceLevelEntity newEntity = new PriceLevelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceLevelId);
				}
				if(fetchResult)
				{
					newEntity = (PriceLevelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceLevelEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceLevelEntity = newEntity;
				_alreadyFetchedPriceLevelEntity = fetchResult;
			}
			return _priceLevelEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationoptionEntity", _alterationoptionEntity);
			toReturn.Add("PriceLevelEntity", _priceLevelEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("OrderitemCollection", _orderitemCollection);
			toReturn.Add("OrderitemAlterationitemCollection", _orderitemAlterationitemCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="validator">The validator object for this PriceLevelItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 priceLevelItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(priceLevelItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_orderitemCollection = new Obymobi.Data.CollectionClasses.OrderitemCollection();
			_orderitemCollection.SetContainingEntityInfo(this, "PriceLevelItemEntity");

			_orderitemAlterationitemCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection();
			_orderitemAlterationitemCollection.SetContainingEntityInfo(this, "PriceLevelItemEntity");
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationoptionEntityReturnsNewIfNotFound = true;
			_priceLevelEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "PriceLevelItemCollection", resetFKFields, new int[] { (int)PriceLevelItemFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, signalRelatedEntity, "PriceLevelItemCollection", resetFKFields, new int[] { (int)PriceLevelItemFieldIndex.AlterationoptionId } );		
			_alterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_alterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncAlterationoptionEntity(true, true);
				_alterationoptionEntity = (AlterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, ref _alreadyFetchedAlterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceLevelEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceLevelEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceLevelEntity, new PropertyChangedEventHandler( OnPriceLevelEntityPropertyChanged ), "PriceLevelEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, true, signalRelatedEntity, "PriceLevelItemCollection", resetFKFields, new int[] { (int)PriceLevelItemFieldIndex.PriceLevelId } );		
			_priceLevelEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceLevelEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceLevelEntity(IEntityCore relatedEntity)
		{
			if(_priceLevelEntity!=relatedEntity)
			{		
				DesetupSyncPriceLevelEntity(true, true);
				_priceLevelEntity = (PriceLevelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceLevelEntity, new PropertyChangedEventHandler( OnPriceLevelEntityPropertyChanged ), "PriceLevelEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, true, ref _alreadyFetchedPriceLevelEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceLevelEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "PriceLevelItemCollection", resetFKFields, new int[] { (int)PriceLevelItemFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticPriceLevelItemRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PriceLevelItemFieldIndex.PriceLevelItemId].ForcedCurrentValueWrite(priceLevelItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePriceLevelItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PriceLevelItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PriceLevelItemRelations Relations
		{
			get	{ return new PriceLevelItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemCollection")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.OrderitemEntity, 0, null, null, null, "OrderitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemCollection")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, 0, null, null, null, "OrderitemAlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionEntity")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelCollection(), (IEntityRelation)GetRelationsForField("PriceLevelEntity")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.PriceLevelEntity, 0, null, null, null, "PriceLevelEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.PriceLevelItemEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PriceLevelItemId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."PriceLevelItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceLevelItemId
		{
			get { return (System.Int32)GetValue((int)PriceLevelItemFieldIndex.PriceLevelItemId, true); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.PriceLevelItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The PriceLevelId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."PriceLevelId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceLevelItemFieldIndex.PriceLevelId, true); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.PriceLevelId, value, true); }
		}

		/// <summary> The ProductId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.ProductId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.ProductId, value, true); }
		}

		/// <summary> The AlterationId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.AlterationId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The AlterationoptionId property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.AlterationoptionId, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Price property of the Entity PriceLevelItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevelItem"."Price"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)PriceLevelItemFieldIndex.Price, true); }
			set	{ SetValue((int)PriceLevelItemFieldIndex.Price, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection OrderitemCollection
		{
			get	{ return GetMultiOrderitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemCollection. When set to true, OrderitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemCollection
		{
			get	{ return _alwaysFetchOrderitemCollection; }
			set	{ _alwaysFetchOrderitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemCollection already has been fetched. Setting this property to false when OrderitemCollection has been fetched
		/// will clear the OrderitemCollection collection well. Setting this property to true while OrderitemCollection hasn't been fetched disables lazy loading for OrderitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemCollection
		{
			get { return _alreadyFetchedOrderitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemCollection && !value && (_orderitemCollection != null))
				{
					_orderitemCollection.Clear();
				}
				_alreadyFetchedOrderitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection OrderitemAlterationitemCollection
		{
			get	{ return GetMultiOrderitemAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemCollection. When set to true, OrderitemAlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemCollection already has been fetched. Setting this property to false when OrderitemAlterationitemCollection has been fetched
		/// will clear the OrderitemAlterationitemCollection collection well. Setting this property to true while OrderitemAlterationitemCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemCollection && !value && (_orderitemAlterationitemCollection != null))
				{
					_orderitemAlterationitemCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceLevelItemCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationoptionEntity AlterationoptionEntity
		{
			get	{ return GetSingleAlterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceLevelItemCollection", "AlterationoptionEntity", _alterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionEntity. When set to true, AlterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionEntity
		{
			get	{ return _alwaysFetchAlterationoptionEntity; }
			set	{ _alwaysFetchAlterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionEntity already has been fetched. Setting this property to false when AlterationoptionEntity has been fetched
		/// will set AlterationoptionEntity to null as well. Setting this property to true while AlterationoptionEntity hasn't been fetched disables lazy loading for AlterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionEntity
		{
			get { return _alreadyFetchedAlterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedAlterationoptionEntity && !value)
				{
					this.AlterationoptionEntity = null;
				}
				_alreadyFetchedAlterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationoptionEntity is not found
		/// in the database. When set to true, AlterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _alterationoptionEntityReturnsNewIfNotFound; }
			set { _alterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceLevelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceLevelEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceLevelEntity PriceLevelEntity
		{
			get	{ return GetSinglePriceLevelEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceLevelEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceLevelItemCollection", "PriceLevelEntity", _priceLevelEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelEntity. When set to true, PriceLevelEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceLevelEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelEntity
		{
			get	{ return _alwaysFetchPriceLevelEntity; }
			set	{ _alwaysFetchPriceLevelEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelEntity already has been fetched. Setting this property to false when PriceLevelEntity has been fetched
		/// will set PriceLevelEntity to null as well. Setting this property to true while PriceLevelEntity hasn't been fetched disables lazy loading for PriceLevelEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelEntity
		{
			get { return _alreadyFetchedPriceLevelEntity;}
			set 
			{
				if(_alreadyFetchedPriceLevelEntity && !value)
				{
					this.PriceLevelEntity = null;
				}
				_alreadyFetchedPriceLevelEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceLevelEntity is not found
		/// in the database. When set to true, PriceLevelEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceLevelEntityReturnsNewIfNotFound
		{
			get	{ return _priceLevelEntityReturnsNewIfNotFound; }
			set { _priceLevelEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceLevelItemCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PriceLevelItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
