﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Terminal'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class TerminalEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "TerminalEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection	_icrtouchprintermappingCollection;
		private bool	_alwaysFetchIcrtouchprintermappingCollection, _alreadyFetchedIcrtouchprintermappingCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_receivedNetmessageCollection;
		private bool	_alwaysFetchReceivedNetmessageCollection, _alreadyFetchedReceivedNetmessageCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_sentNetmessageCollection;
		private bool	_alwaysFetchSentNetmessageCollection, _alreadyFetchedSentNetmessageCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection	_orderRoutestephandlerCollection_;
		private bool	_alwaysFetchOrderRoutestephandlerCollection_, _alreadyFetchedOrderRoutestephandlerCollection_;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection	_orderRoutestephandlerCollection;
		private bool	_alwaysFetchOrderRoutestephandlerCollection, _alreadyFetchedOrderRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection	_orderRoutestephandlerHistoryCollection_;
		private bool	_alwaysFetchOrderRoutestephandlerHistoryCollection_, _alreadyFetchedOrderRoutestephandlerHistoryCollection_;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection	_orderRoutestephandlerHistoryCollection;
		private bool	_alwaysFetchOrderRoutestephandlerHistoryCollection, _alreadyFetchedOrderRoutestephandlerHistoryCollection;
		private Obymobi.Data.CollectionClasses.RoutestephandlerCollection	_routestephandlerCollection;
		private bool	_alwaysFetchRoutestephandlerCollection, _alreadyFetchedRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.ScheduledCommandCollection	_scheduledCommandCollection;
		private bool	_alwaysFetchScheduledCommandCollection, _alreadyFetchedScheduledCommandCollection;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_forwardedFromTerminalCollection;
		private bool	_alwaysFetchForwardedFromTerminalCollection, _alreadyFetchedForwardedFromTerminalCollection;
		private Obymobi.Data.CollectionClasses.TerminalConfigurationCollection	_terminalConfigurationCollection;
		private bool	_alwaysFetchTerminalConfigurationCollection, _alreadyFetchedTerminalConfigurationCollection;
		private Obymobi.Data.CollectionClasses.TerminalLogCollection	_terminalLogCollection;
		private bool	_alwaysFetchTerminalLogCollection, _alreadyFetchedTerminalLogCollection;
		private Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection	_terminalMessageTemplateCategoryCollection;
		private bool	_alwaysFetchTerminalMessageTemplateCategoryCollection, _alreadyFetchedTerminalMessageTemplateCategoryCollection;
		private Obymobi.Data.CollectionClasses.TerminalStateCollection	_terminalStateCollection;
		private bool	_alwaysFetchTerminalStateCollection, _alreadyFetchedTerminalStateCollection;
		private Obymobi.Data.CollectionClasses.TimestampCollection	_timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaNetmessage;
		private bool	_alwaysFetchClientCollectionViaNetmessage, _alreadyFetchedClientCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup, _alreadyFetchedCompanyCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage, _alreadyFetchedCompanyCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage_;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage_, _alreadyFetchedCompanyCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage__;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage__, _alreadyFetchedCompanyCollectionViaNetmessage__;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage___;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage___, _alreadyFetchedCompanyCollectionViaNetmessage___;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaTerminal;
		private bool	_alwaysFetchCompanyCollectionViaTerminal, _alreadyFetchedCompanyCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage, _alreadyFetchedCustomerCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage_;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage_, _alreadyFetchedCustomerCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage__;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage__, _alreadyFetchedCustomerCollectionViaNetmessage__;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage___;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage___, _alreadyFetchedCustomerCollectionViaNetmessage___;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage, _alreadyFetchedDeliverypointCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage_;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage_, _alreadyFetchedDeliverypointCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage__;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage__, _alreadyFetchedDeliverypointCollectionViaNetmessage__;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage___;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage___, _alreadyFetchedDeliverypointCollectionViaNetmessage___;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal, _alreadyFetchedDeliverypointCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal_, _alreadyFetchedDeliverypointCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage_, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal, _alreadyFetchedDeliverypointgroupCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal;
		private bool	_alwaysFetchDeviceCollectionViaTerminal, _alreadyFetchedDeviceCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection _icrtouchprintermappingCollectionViaTerminal;
		private bool	_alwaysFetchIcrtouchprintermappingCollectionViaTerminal, _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup, _alreadyFetchedMenuCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandler, _alreadyFetchedOrderCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandler_;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandler_, _alreadyFetchedOrderCollectionViaOrderRoutestephandler_;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandlerHistory_;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_, _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal;
		private bool	_alwaysFetchProductCollectionViaTerminal, _alreadyFetchedProductCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal_;
		private bool	_alwaysFetchProductCollectionViaTerminal_, _alreadyFetchedProductCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal__;
		private bool	_alwaysFetchProductCollectionViaTerminal__, _alreadyFetchedProductCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal___;
		private bool	_alwaysFetchProductCollectionViaTerminal___, _alreadyFetchedProductCollectionViaTerminal___;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup, _alreadyFetchedRouteCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_, _alreadyFetchedRouteCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.RoutestepCollection _routestepCollectionViaRoutestephandler;
		private bool	_alwaysFetchRoutestepCollectionViaRoutestephandler, _alreadyFetchedRoutestepCollectionViaRoutestephandler;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandler_;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandlerHistory_;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaRoutestephandler;
		private bool	_alwaysFetchSupportpoolCollectionViaRoutestephandler, _alreadyFetchedSupportpoolCollectionViaRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage, _alreadyFetchedTerminalCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage_;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage_, _alreadyFetchedTerminalCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;
		private Obymobi.Data.CollectionClasses.TerminalLogFileCollection _terminalLogFileCollectionViaTerminalLog;
		private bool	_alwaysFetchTerminalLogFileCollectionViaTerminalLog, _alreadyFetchedTerminalLogFileCollectionViaTerminalLog;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup__, _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal;
		private bool	_alwaysFetchUIModeCollectionViaTerminal, _alreadyFetchedUIModeCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup, _alreadyFetchedUIModeCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UserCollection _automaticSignOnUserCollectionViaTerminal;
		private bool	_alwaysFetchAutomaticSignOnUserCollectionViaTerminal, _alreadyFetchedAutomaticSignOnUserCollectionViaTerminal;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointEntity _altSystemMessagesDeliverypointEntity;
		private bool	_alwaysFetchAltSystemMessagesDeliverypointEntity, _alreadyFetchedAltSystemMessagesDeliverypointEntity, _altSystemMessagesDeliverypointEntityReturnsNewIfNotFound;
		private DeliverypointEntity _systemMessagesDeliverypointEntity;
		private bool	_alwaysFetchSystemMessagesDeliverypointEntity, _alreadyFetchedSystemMessagesDeliverypointEntity, _systemMessagesDeliverypointEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private DeviceEntity _deviceEntity;
		private bool	_alwaysFetchDeviceEntity, _alreadyFetchedDeviceEntity, _deviceEntityReturnsNewIfNotFound;
		private EntertainmentEntity _browser1EntertainmentEntity;
		private bool	_alwaysFetchBrowser1EntertainmentEntity, _alreadyFetchedBrowser1EntertainmentEntity, _browser1EntertainmentEntityReturnsNewIfNotFound;
		private EntertainmentEntity _browser2EntertainmentEntity;
		private bool	_alwaysFetchBrowser2EntertainmentEntity, _alreadyFetchedBrowser2EntertainmentEntity, _browser2EntertainmentEntityReturnsNewIfNotFound;
		private EntertainmentEntity _cmsPageEntertainmentEntity;
		private bool	_alwaysFetchCmsPageEntertainmentEntity, _alreadyFetchedCmsPageEntertainmentEntity, _cmsPageEntertainmentEntityReturnsNewIfNotFound;
		private IcrtouchprintermappingEntity _icrtouchprintermappingEntity;
		private bool	_alwaysFetchIcrtouchprintermappingEntity, _alreadyFetchedIcrtouchprintermappingEntity, _icrtouchprintermappingEntityReturnsNewIfNotFound;
		private ProductEntity _batteryLowProductEntity;
		private bool	_alwaysFetchBatteryLowProductEntity, _alreadyFetchedBatteryLowProductEntity, _batteryLowProductEntityReturnsNewIfNotFound;
		private ProductEntity _clientDisconnectedProductEntity;
		private bool	_alwaysFetchClientDisconnectedProductEntity, _alreadyFetchedClientDisconnectedProductEntity, _clientDisconnectedProductEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductEntity _unlockDeliverypointProductEntity;
		private bool	_alwaysFetchUnlockDeliverypointProductEntity, _alreadyFetchedUnlockDeliverypointProductEntity, _unlockDeliverypointProductEntityReturnsNewIfNotFound;
		private TerminalEntity _forwardToTerminalEntity;
		private bool	_alwaysFetchForwardToTerminalEntity, _alreadyFetchedForwardToTerminalEntity, _forwardToTerminalEntityReturnsNewIfNotFound;
		private TerminalConfigurationEntity _terminalConfigurationEntity;
		private bool	_alwaysFetchTerminalConfigurationEntity, _alreadyFetchedTerminalConfigurationEntity, _terminalConfigurationEntityReturnsNewIfNotFound;
		private UIModeEntity _uIModeEntity;
		private bool	_alwaysFetchUIModeEntity, _alreadyFetchedUIModeEntity, _uIModeEntityReturnsNewIfNotFound;
		private UserEntity _automaticSignOnUserEntity;
		private bool	_alwaysFetchAutomaticSignOnUserEntity, _alreadyFetchedAutomaticSignOnUserEntity, _automaticSignOnUserEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name AltSystemMessagesDeliverypointEntity</summary>
			public static readonly string AltSystemMessagesDeliverypointEntity = "AltSystemMessagesDeliverypointEntity";
			/// <summary>Member name SystemMessagesDeliverypointEntity</summary>
			public static readonly string SystemMessagesDeliverypointEntity = "SystemMessagesDeliverypointEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name DeviceEntity</summary>
			public static readonly string DeviceEntity = "DeviceEntity";
			/// <summary>Member name Browser1EntertainmentEntity</summary>
			public static readonly string Browser1EntertainmentEntity = "Browser1EntertainmentEntity";
			/// <summary>Member name Browser2EntertainmentEntity</summary>
			public static readonly string Browser2EntertainmentEntity = "Browser2EntertainmentEntity";
			/// <summary>Member name CmsPageEntertainmentEntity</summary>
			public static readonly string CmsPageEntertainmentEntity = "CmsPageEntertainmentEntity";
			/// <summary>Member name IcrtouchprintermappingEntity</summary>
			public static readonly string IcrtouchprintermappingEntity = "IcrtouchprintermappingEntity";
			/// <summary>Member name BatteryLowProductEntity</summary>
			public static readonly string BatteryLowProductEntity = "BatteryLowProductEntity";
			/// <summary>Member name ClientDisconnectedProductEntity</summary>
			public static readonly string ClientDisconnectedProductEntity = "ClientDisconnectedProductEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name UnlockDeliverypointProductEntity</summary>
			public static readonly string UnlockDeliverypointProductEntity = "UnlockDeliverypointProductEntity";
			/// <summary>Member name ForwardToTerminalEntity</summary>
			public static readonly string ForwardToTerminalEntity = "ForwardToTerminalEntity";
			/// <summary>Member name TerminalConfigurationEntity</summary>
			public static readonly string TerminalConfigurationEntity = "TerminalConfigurationEntity";
			/// <summary>Member name UIModeEntity</summary>
			public static readonly string UIModeEntity = "UIModeEntity";
			/// <summary>Member name AutomaticSignOnUserEntity</summary>
			public static readonly string AutomaticSignOnUserEntity = "AutomaticSignOnUserEntity";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name IcrtouchprintermappingCollection</summary>
			public static readonly string IcrtouchprintermappingCollection = "IcrtouchprintermappingCollection";
			/// <summary>Member name ReceivedNetmessageCollection</summary>
			public static readonly string ReceivedNetmessageCollection = "ReceivedNetmessageCollection";
			/// <summary>Member name SentNetmessageCollection</summary>
			public static readonly string SentNetmessageCollection = "SentNetmessageCollection";
			/// <summary>Member name OrderRoutestephandlerCollection_</summary>
			public static readonly string OrderRoutestephandlerCollection_ = "OrderRoutestephandlerCollection_";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection_</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection_ = "OrderRoutestephandlerHistoryCollection_";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
			/// <summary>Member name ScheduledCommandCollection</summary>
			public static readonly string ScheduledCommandCollection = "ScheduledCommandCollection";
			/// <summary>Member name ForwardedFromTerminalCollection</summary>
			public static readonly string ForwardedFromTerminalCollection = "ForwardedFromTerminalCollection";
			/// <summary>Member name TerminalConfigurationCollection</summary>
			public static readonly string TerminalConfigurationCollection = "TerminalConfigurationCollection";
			/// <summary>Member name TerminalLogCollection</summary>
			public static readonly string TerminalLogCollection = "TerminalLogCollection";
			/// <summary>Member name TerminalMessageTemplateCategoryCollection</summary>
			public static readonly string TerminalMessageTemplateCategoryCollection = "TerminalMessageTemplateCategoryCollection";
			/// <summary>Member name TerminalStateCollection</summary>
			public static readonly string TerminalStateCollection = "TerminalStateCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup = "AnnouncementCollectionViaDeliverypointgroup";
			/// <summary>Member name ClientCollectionViaNetmessage</summary>
			public static readonly string ClientCollectionViaNetmessage = "ClientCollectionViaNetmessage";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup = "CompanyCollectionViaDeliverypointgroup";
			/// <summary>Member name CompanyCollectionViaNetmessage</summary>
			public static readonly string CompanyCollectionViaNetmessage = "CompanyCollectionViaNetmessage";
			/// <summary>Member name CompanyCollectionViaNetmessage_</summary>
			public static readonly string CompanyCollectionViaNetmessage_ = "CompanyCollectionViaNetmessage_";
			/// <summary>Member name CompanyCollectionViaNetmessage__</summary>
			public static readonly string CompanyCollectionViaNetmessage__ = "CompanyCollectionViaNetmessage__";
			/// <summary>Member name CompanyCollectionViaNetmessage___</summary>
			public static readonly string CompanyCollectionViaNetmessage___ = "CompanyCollectionViaNetmessage___";
			/// <summary>Member name CompanyCollectionViaTerminal</summary>
			public static readonly string CompanyCollectionViaTerminal = "CompanyCollectionViaTerminal";
			/// <summary>Member name CustomerCollectionViaNetmessage</summary>
			public static readonly string CustomerCollectionViaNetmessage = "CustomerCollectionViaNetmessage";
			/// <summary>Member name CustomerCollectionViaNetmessage_</summary>
			public static readonly string CustomerCollectionViaNetmessage_ = "CustomerCollectionViaNetmessage_";
			/// <summary>Member name CustomerCollectionViaNetmessage__</summary>
			public static readonly string CustomerCollectionViaNetmessage__ = "CustomerCollectionViaNetmessage__";
			/// <summary>Member name CustomerCollectionViaNetmessage___</summary>
			public static readonly string CustomerCollectionViaNetmessage___ = "CustomerCollectionViaNetmessage___";
			/// <summary>Member name DeliverypointCollectionViaNetmessage</summary>
			public static readonly string DeliverypointCollectionViaNetmessage = "DeliverypointCollectionViaNetmessage";
			/// <summary>Member name DeliverypointCollectionViaNetmessage_</summary>
			public static readonly string DeliverypointCollectionViaNetmessage_ = "DeliverypointCollectionViaNetmessage_";
			/// <summary>Member name DeliverypointCollectionViaNetmessage__</summary>
			public static readonly string DeliverypointCollectionViaNetmessage__ = "DeliverypointCollectionViaNetmessage__";
			/// <summary>Member name DeliverypointCollectionViaNetmessage___</summary>
			public static readonly string DeliverypointCollectionViaNetmessage___ = "DeliverypointCollectionViaNetmessage___";
			/// <summary>Member name DeliverypointCollectionViaTerminal</summary>
			public static readonly string DeliverypointCollectionViaTerminal = "DeliverypointCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal_</summary>
			public static readonly string DeliverypointCollectionViaTerminal_ = "DeliverypointCollectionViaTerminal_";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage = "DeliverypointgroupCollectionViaNetmessage";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage_</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage_ = "DeliverypointgroupCollectionViaNetmessage_";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal = "DeliverypointgroupCollectionViaTerminal";
			/// <summary>Member name DeviceCollectionViaTerminal</summary>
			public static readonly string DeviceCollectionViaTerminal = "DeviceCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name IcrtouchprintermappingCollectionViaTerminal</summary>
			public static readonly string IcrtouchprintermappingCollectionViaTerminal = "IcrtouchprintermappingCollectionViaTerminal";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup = "MenuCollectionViaDeliverypointgroup";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandler</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandler = "OrderCollectionViaOrderRoutestephandler";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandler_</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandler_ = "OrderCollectionViaOrderRoutestephandler_";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandlerHistory = "OrderCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandlerHistory_</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandlerHistory_ = "OrderCollectionViaOrderRoutestephandlerHistory_";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup = "PosdeliverypointgroupCollectionViaDeliverypointgroup";
			/// <summary>Member name ProductCollectionViaTerminal</summary>
			public static readonly string ProductCollectionViaTerminal = "ProductCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal_</summary>
			public static readonly string ProductCollectionViaTerminal_ = "ProductCollectionViaTerminal_";
			/// <summary>Member name ProductCollectionViaTerminal__</summary>
			public static readonly string ProductCollectionViaTerminal__ = "ProductCollectionViaTerminal__";
			/// <summary>Member name ProductCollectionViaTerminal___</summary>
			public static readonly string ProductCollectionViaTerminal___ = "ProductCollectionViaTerminal___";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup = "RouteCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_ = "RouteCollectionViaDeliverypointgroup_";
			/// <summary>Member name RoutestepCollectionViaRoutestephandler</summary>
			public static readonly string RoutestepCollectionViaRoutestephandler = "RoutestepCollectionViaRoutestephandler";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandler</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandler = "SupportpoolCollectionViaOrderRoutestephandler";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandler_</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandler_ = "SupportpoolCollectionViaOrderRoutestephandler_";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandlerHistory = "SupportpoolCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandlerHistory_</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandlerHistory_ = "SupportpoolCollectionViaOrderRoutestephandlerHistory_";
			/// <summary>Member name SupportpoolCollectionViaRoutestephandler</summary>
			public static readonly string SupportpoolCollectionViaRoutestephandler = "SupportpoolCollectionViaRoutestephandler";
			/// <summary>Member name TerminalCollectionViaNetmessage</summary>
			public static readonly string TerminalCollectionViaNetmessage = "TerminalCollectionViaNetmessage";
			/// <summary>Member name TerminalCollectionViaNetmessage_</summary>
			public static readonly string TerminalCollectionViaNetmessage_ = "TerminalCollectionViaNetmessage_";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler = "TerminalCollectionViaOrderRoutestephandler";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler_ = "TerminalCollectionViaOrderRoutestephandler_";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory = "TerminalCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory_ = "TerminalCollectionViaOrderRoutestephandlerHistory_";
			/// <summary>Member name TerminalLogFileCollectionViaTerminalLog</summary>
			public static readonly string TerminalLogFileCollectionViaTerminalLog = "TerminalLogFileCollectionViaTerminalLog";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_ = "UIModeCollectionViaDeliverypointgroup_";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup__</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup__ = "UIModeCollectionViaDeliverypointgroup__";
			/// <summary>Member name UIModeCollectionViaTerminal</summary>
			public static readonly string UIModeCollectionViaTerminal = "UIModeCollectionViaTerminal";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup = "UIModeCollectionViaDeliverypointgroup";
			/// <summary>Member name AutomaticSignOnUserCollectionViaTerminal</summary>
			public static readonly string AutomaticSignOnUserCollectionViaTerminal = "AutomaticSignOnUserCollectionViaTerminal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TerminalEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected TerminalEntityBase() :base("TerminalEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		protected TerminalEntityBase(System.Int32 terminalId):base("TerminalEntity")
		{
			InitClassFetch(terminalId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected TerminalEntityBase(System.Int32 terminalId, IPrefetchPath prefetchPathToUse): base("TerminalEntity")
		{
			InitClassFetch(terminalId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="validator">The custom validator object for this TerminalEntity</param>
		protected TerminalEntityBase(System.Int32 terminalId, IValidator validator):base("TerminalEntity")
		{
			InitClassFetch(terminalId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_icrtouchprintermappingCollection = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollection", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollection = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollection");
			_alreadyFetchedIcrtouchprintermappingCollection = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollection");

			_receivedNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_receivedNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchReceivedNetmessageCollection = info.GetBoolean("_alwaysFetchReceivedNetmessageCollection");
			_alreadyFetchedReceivedNetmessageCollection = info.GetBoolean("_alreadyFetchedReceivedNetmessageCollection");

			_sentNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_sentNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchSentNetmessageCollection = info.GetBoolean("_alwaysFetchSentNetmessageCollection");
			_alreadyFetchedSentNetmessageCollection = info.GetBoolean("_alreadyFetchedSentNetmessageCollection");

			_orderRoutestephandlerCollection_ = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection)info.GetValue("_orderRoutestephandlerCollection_", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection));
			_alwaysFetchOrderRoutestephandlerCollection_ = info.GetBoolean("_alwaysFetchOrderRoutestephandlerCollection_");
			_alreadyFetchedOrderRoutestephandlerCollection_ = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerCollection_");

			_orderRoutestephandlerCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection)info.GetValue("_orderRoutestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection));
			_alwaysFetchOrderRoutestephandlerCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerCollection");
			_alreadyFetchedOrderRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerCollection");

			_orderRoutestephandlerHistoryCollection_ = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection)info.GetValue("_orderRoutestephandlerHistoryCollection_", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection));
			_alwaysFetchOrderRoutestephandlerHistoryCollection_ = info.GetBoolean("_alwaysFetchOrderRoutestephandlerHistoryCollection_");
			_alreadyFetchedOrderRoutestephandlerHistoryCollection_ = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerHistoryCollection_");

			_orderRoutestephandlerHistoryCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection)info.GetValue("_orderRoutestephandlerHistoryCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection));
			_alwaysFetchOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerHistoryCollection");
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerHistoryCollection");

			_routestephandlerCollection = (Obymobi.Data.CollectionClasses.RoutestephandlerCollection)info.GetValue("_routestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.RoutestephandlerCollection));
			_alwaysFetchRoutestephandlerCollection = info.GetBoolean("_alwaysFetchRoutestephandlerCollection");
			_alreadyFetchedRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedRoutestephandlerCollection");

			_scheduledCommandCollection = (Obymobi.Data.CollectionClasses.ScheduledCommandCollection)info.GetValue("_scheduledCommandCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledCommandCollection));
			_alwaysFetchScheduledCommandCollection = info.GetBoolean("_alwaysFetchScheduledCommandCollection");
			_alreadyFetchedScheduledCommandCollection = info.GetBoolean("_alreadyFetchedScheduledCommandCollection");

			_forwardedFromTerminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_forwardedFromTerminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchForwardedFromTerminalCollection = info.GetBoolean("_alwaysFetchForwardedFromTerminalCollection");
			_alreadyFetchedForwardedFromTerminalCollection = info.GetBoolean("_alreadyFetchedForwardedFromTerminalCollection");

			_terminalConfigurationCollection = (Obymobi.Data.CollectionClasses.TerminalConfigurationCollection)info.GetValue("_terminalConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.TerminalConfigurationCollection));
			_alwaysFetchTerminalConfigurationCollection = info.GetBoolean("_alwaysFetchTerminalConfigurationCollection");
			_alreadyFetchedTerminalConfigurationCollection = info.GetBoolean("_alreadyFetchedTerminalConfigurationCollection");

			_terminalLogCollection = (Obymobi.Data.CollectionClasses.TerminalLogCollection)info.GetValue("_terminalLogCollection", typeof(Obymobi.Data.CollectionClasses.TerminalLogCollection));
			_alwaysFetchTerminalLogCollection = info.GetBoolean("_alwaysFetchTerminalLogCollection");
			_alreadyFetchedTerminalLogCollection = info.GetBoolean("_alreadyFetchedTerminalLogCollection");

			_terminalMessageTemplateCategoryCollection = (Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection)info.GetValue("_terminalMessageTemplateCategoryCollection", typeof(Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection));
			_alwaysFetchTerminalMessageTemplateCategoryCollection = info.GetBoolean("_alwaysFetchTerminalMessageTemplateCategoryCollection");
			_alreadyFetchedTerminalMessageTemplateCategoryCollection = info.GetBoolean("_alreadyFetchedTerminalMessageTemplateCategoryCollection");

			_terminalStateCollection = (Obymobi.Data.CollectionClasses.TerminalStateCollection)info.GetValue("_terminalStateCollection", typeof(Obymobi.Data.CollectionClasses.TerminalStateCollection));
			_alwaysFetchTerminalStateCollection = info.GetBoolean("_alwaysFetchTerminalStateCollection");
			_alreadyFetchedTerminalStateCollection = info.GetBoolean("_alreadyFetchedTerminalStateCollection");

			_timestampCollection = (Obymobi.Data.CollectionClasses.TimestampCollection)info.GetValue("_timestampCollection", typeof(Obymobi.Data.CollectionClasses.TimestampCollection));
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			_announcementCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup");

			_clientCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaNetmessage = info.GetBoolean("_alwaysFetchClientCollectionViaNetmessage");
			_alreadyFetchedClientCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedClientCollectionViaNetmessage");

			_companyCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup");

			_companyCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage");
			_alreadyFetchedCompanyCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage");

			_companyCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage_");
			_alreadyFetchedCompanyCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage_");

			_companyCollectionViaNetmessage__ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage__", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage__ = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage__");
			_alreadyFetchedCompanyCollectionViaNetmessage__ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage__");

			_companyCollectionViaNetmessage___ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage___", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage___ = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage___");
			_alreadyFetchedCompanyCollectionViaNetmessage___ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage___");

			_companyCollectionViaTerminal = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaTerminal = info.GetBoolean("_alwaysFetchCompanyCollectionViaTerminal");
			_alreadyFetchedCompanyCollectionViaTerminal = info.GetBoolean("_alreadyFetchedCompanyCollectionViaTerminal");

			_customerCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage");
			_alreadyFetchedCustomerCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage");

			_customerCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage_");
			_alreadyFetchedCustomerCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage_");

			_customerCollectionViaNetmessage__ = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage__", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage__ = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage__");
			_alreadyFetchedCustomerCollectionViaNetmessage__ = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage__");

			_customerCollectionViaNetmessage___ = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage___", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage___ = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage___");
			_alreadyFetchedCustomerCollectionViaNetmessage___ = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage___");

			_deliverypointCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage");
			_alreadyFetchedDeliverypointCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage");

			_deliverypointCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage_");
			_alreadyFetchedDeliverypointCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage_");

			_deliverypointCollectionViaNetmessage__ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage__", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage__ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage__");
			_alreadyFetchedDeliverypointCollectionViaNetmessage__ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage__");

			_deliverypointCollectionViaNetmessage___ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage___", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage___ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage___");
			_alreadyFetchedDeliverypointCollectionViaNetmessage___ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage___");

			_deliverypointCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal");
			_alreadyFetchedDeliverypointCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal");

			_deliverypointCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal_");
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal_");

			_deliverypointgroupCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage");

			_deliverypointgroupCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_");

			_deliverypointgroupCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal");

			_deviceCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal");
			_alreadyFetchedDeviceCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_icrtouchprintermappingCollectionViaTerminal = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal");
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal");

			_menuCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup");

			_orderCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandler");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandler");

			_orderCollectionViaOrderRoutestephandler_ = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandler_", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandler_");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandler_");

			_orderCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory");

			_orderCollectionViaOrderRoutestephandlerHistory_ = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandlerHistory_", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_");

			_posdeliverypointgroupCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup");

			_productCollectionViaTerminal = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal");
			_alreadyFetchedProductCollectionViaTerminal = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal");

			_productCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal_");
			_alreadyFetchedProductCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal_");

			_productCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal__");
			_alreadyFetchedProductCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal__");

			_productCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal___");
			_alreadyFetchedProductCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal___");

			_routeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_");

			_routestepCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.RoutestepCollection)info.GetValue("_routestepCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.RoutestepCollection));
			_alwaysFetchRoutestepCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchRoutestepCollectionViaRoutestephandler");
			_alreadyFetchedRoutestepCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedRoutestepCollectionViaRoutestephandler");

			_supportpoolCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler");

			_supportpoolCollectionViaOrderRoutestephandler_ = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandler_", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_");

			_supportpoolCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory");

			_supportpoolCollectionViaOrderRoutestephandlerHistory_ = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandlerHistory_", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_");

			_supportpoolCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaRoutestephandler");
			_alreadyFetchedSupportpoolCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaRoutestephandler");

			_terminalCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage");
			_alreadyFetchedTerminalCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage");

			_terminalCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage_");
			_alreadyFetchedTerminalCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage_");

			_terminalCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler");

			_terminalCollectionViaOrderRoutestephandler_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_");

			_terminalCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory");

			_terminalCollectionViaOrderRoutestephandlerHistory_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_");

			_terminalLogFileCollectionViaTerminalLog = (Obymobi.Data.CollectionClasses.TerminalLogFileCollection)info.GetValue("_terminalLogFileCollectionViaTerminalLog", typeof(Obymobi.Data.CollectionClasses.TerminalLogFileCollection));
			_alwaysFetchTerminalLogFileCollectionViaTerminalLog = info.GetBoolean("_alwaysFetchTerminalLogFileCollectionViaTerminalLog");
			_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = info.GetBoolean("_alreadyFetchedTerminalLogFileCollectionViaTerminalLog");

			_uIModeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_");

			_uIModeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup__");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__");

			_uIModeCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal");
			_alreadyFetchedUIModeCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal");

			_uIModeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup");

			_automaticSignOnUserCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_automaticSignOnUserCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchAutomaticSignOnUserCollectionViaTerminal = info.GetBoolean("_alwaysFetchAutomaticSignOnUserCollectionViaTerminal");
			_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal = info.GetBoolean("_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_altSystemMessagesDeliverypointEntity = (DeliverypointEntity)info.GetValue("_altSystemMessagesDeliverypointEntity", typeof(DeliverypointEntity));
			if(_altSystemMessagesDeliverypointEntity!=null)
			{
				_altSystemMessagesDeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_altSystemMessagesDeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_altSystemMessagesDeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchAltSystemMessagesDeliverypointEntity = info.GetBoolean("_alwaysFetchAltSystemMessagesDeliverypointEntity");
			_alreadyFetchedAltSystemMessagesDeliverypointEntity = info.GetBoolean("_alreadyFetchedAltSystemMessagesDeliverypointEntity");

			_systemMessagesDeliverypointEntity = (DeliverypointEntity)info.GetValue("_systemMessagesDeliverypointEntity", typeof(DeliverypointEntity));
			if(_systemMessagesDeliverypointEntity!=null)
			{
				_systemMessagesDeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_systemMessagesDeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_systemMessagesDeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchSystemMessagesDeliverypointEntity = info.GetBoolean("_alwaysFetchSystemMessagesDeliverypointEntity");
			_alreadyFetchedSystemMessagesDeliverypointEntity = info.GetBoolean("_alreadyFetchedSystemMessagesDeliverypointEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_deviceEntity = (DeviceEntity)info.GetValue("_deviceEntity", typeof(DeviceEntity));
			if(_deviceEntity!=null)
			{
				_deviceEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceEntityReturnsNewIfNotFound = info.GetBoolean("_deviceEntityReturnsNewIfNotFound");
			_alwaysFetchDeviceEntity = info.GetBoolean("_alwaysFetchDeviceEntity");
			_alreadyFetchedDeviceEntity = info.GetBoolean("_alreadyFetchedDeviceEntity");

			_browser1EntertainmentEntity = (EntertainmentEntity)info.GetValue("_browser1EntertainmentEntity", typeof(EntertainmentEntity));
			if(_browser1EntertainmentEntity!=null)
			{
				_browser1EntertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_browser1EntertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_browser1EntertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchBrowser1EntertainmentEntity = info.GetBoolean("_alwaysFetchBrowser1EntertainmentEntity");
			_alreadyFetchedBrowser1EntertainmentEntity = info.GetBoolean("_alreadyFetchedBrowser1EntertainmentEntity");

			_browser2EntertainmentEntity = (EntertainmentEntity)info.GetValue("_browser2EntertainmentEntity", typeof(EntertainmentEntity));
			if(_browser2EntertainmentEntity!=null)
			{
				_browser2EntertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_browser2EntertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_browser2EntertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchBrowser2EntertainmentEntity = info.GetBoolean("_alwaysFetchBrowser2EntertainmentEntity");
			_alreadyFetchedBrowser2EntertainmentEntity = info.GetBoolean("_alreadyFetchedBrowser2EntertainmentEntity");

			_cmsPageEntertainmentEntity = (EntertainmentEntity)info.GetValue("_cmsPageEntertainmentEntity", typeof(EntertainmentEntity));
			if(_cmsPageEntertainmentEntity!=null)
			{
				_cmsPageEntertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cmsPageEntertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_cmsPageEntertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchCmsPageEntertainmentEntity = info.GetBoolean("_alwaysFetchCmsPageEntertainmentEntity");
			_alreadyFetchedCmsPageEntertainmentEntity = info.GetBoolean("_alreadyFetchedCmsPageEntertainmentEntity");

			_icrtouchprintermappingEntity = (IcrtouchprintermappingEntity)info.GetValue("_icrtouchprintermappingEntity", typeof(IcrtouchprintermappingEntity));
			if(_icrtouchprintermappingEntity!=null)
			{
				_icrtouchprintermappingEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_icrtouchprintermappingEntityReturnsNewIfNotFound = info.GetBoolean("_icrtouchprintermappingEntityReturnsNewIfNotFound");
			_alwaysFetchIcrtouchprintermappingEntity = info.GetBoolean("_alwaysFetchIcrtouchprintermappingEntity");
			_alreadyFetchedIcrtouchprintermappingEntity = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingEntity");

			_batteryLowProductEntity = (ProductEntity)info.GetValue("_batteryLowProductEntity", typeof(ProductEntity));
			if(_batteryLowProductEntity!=null)
			{
				_batteryLowProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_batteryLowProductEntityReturnsNewIfNotFound = info.GetBoolean("_batteryLowProductEntityReturnsNewIfNotFound");
			_alwaysFetchBatteryLowProductEntity = info.GetBoolean("_alwaysFetchBatteryLowProductEntity");
			_alreadyFetchedBatteryLowProductEntity = info.GetBoolean("_alreadyFetchedBatteryLowProductEntity");

			_clientDisconnectedProductEntity = (ProductEntity)info.GetValue("_clientDisconnectedProductEntity", typeof(ProductEntity));
			if(_clientDisconnectedProductEntity!=null)
			{
				_clientDisconnectedProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientDisconnectedProductEntityReturnsNewIfNotFound = info.GetBoolean("_clientDisconnectedProductEntityReturnsNewIfNotFound");
			_alwaysFetchClientDisconnectedProductEntity = info.GetBoolean("_alwaysFetchClientDisconnectedProductEntity");
			_alreadyFetchedClientDisconnectedProductEntity = info.GetBoolean("_alreadyFetchedClientDisconnectedProductEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_unlockDeliverypointProductEntity = (ProductEntity)info.GetValue("_unlockDeliverypointProductEntity", typeof(ProductEntity));
			if(_unlockDeliverypointProductEntity!=null)
			{
				_unlockDeliverypointProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unlockDeliverypointProductEntityReturnsNewIfNotFound = info.GetBoolean("_unlockDeliverypointProductEntityReturnsNewIfNotFound");
			_alwaysFetchUnlockDeliverypointProductEntity = info.GetBoolean("_alwaysFetchUnlockDeliverypointProductEntity");
			_alreadyFetchedUnlockDeliverypointProductEntity = info.GetBoolean("_alreadyFetchedUnlockDeliverypointProductEntity");

			_forwardToTerminalEntity = (TerminalEntity)info.GetValue("_forwardToTerminalEntity", typeof(TerminalEntity));
			if(_forwardToTerminalEntity!=null)
			{
				_forwardToTerminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_forwardToTerminalEntityReturnsNewIfNotFound = info.GetBoolean("_forwardToTerminalEntityReturnsNewIfNotFound");
			_alwaysFetchForwardToTerminalEntity = info.GetBoolean("_alwaysFetchForwardToTerminalEntity");
			_alreadyFetchedForwardToTerminalEntity = info.GetBoolean("_alreadyFetchedForwardToTerminalEntity");

			_terminalConfigurationEntity = (TerminalConfigurationEntity)info.GetValue("_terminalConfigurationEntity", typeof(TerminalConfigurationEntity));
			if(_terminalConfigurationEntity!=null)
			{
				_terminalConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_terminalConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalConfigurationEntity = info.GetBoolean("_alwaysFetchTerminalConfigurationEntity");
			_alreadyFetchedTerminalConfigurationEntity = info.GetBoolean("_alreadyFetchedTerminalConfigurationEntity");

			_uIModeEntity = (UIModeEntity)info.GetValue("_uIModeEntity", typeof(UIModeEntity));
			if(_uIModeEntity!=null)
			{
				_uIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIModeEntityReturnsNewIfNotFound = info.GetBoolean("_uIModeEntityReturnsNewIfNotFound");
			_alwaysFetchUIModeEntity = info.GetBoolean("_alwaysFetchUIModeEntity");
			_alreadyFetchedUIModeEntity = info.GetBoolean("_alreadyFetchedUIModeEntity");

			_automaticSignOnUserEntity = (UserEntity)info.GetValue("_automaticSignOnUserEntity", typeof(UserEntity));
			if(_automaticSignOnUserEntity!=null)
			{
				_automaticSignOnUserEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_automaticSignOnUserEntityReturnsNewIfNotFound = info.GetBoolean("_automaticSignOnUserEntityReturnsNewIfNotFound");
			_alwaysFetchAutomaticSignOnUserEntity = info.GetBoolean("_alwaysFetchAutomaticSignOnUserEntity");
			_alreadyFetchedAutomaticSignOnUserEntity = info.GetBoolean("_alreadyFetchedAutomaticSignOnUserEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TerminalFieldIndex)fieldIndex)
			{
				case TerminalFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case TerminalFieldIndex.UnlockDeliverypointProductId:
					DesetupSyncUnlockDeliverypointProductEntity(true, false);
					_alreadyFetchedUnlockDeliverypointProductEntity = false;
					break;
				case TerminalFieldIndex.BatteryLowProductId:
					DesetupSyncBatteryLowProductEntity(true, false);
					_alreadyFetchedBatteryLowProductEntity = false;
					break;
				case TerminalFieldIndex.ClientDisconnectedProductId:
					DesetupSyncClientDisconnectedProductEntity(true, false);
					_alreadyFetchedClientDisconnectedProductEntity = false;
					break;
				case TerminalFieldIndex.OrderFailedProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case TerminalFieldIndex.SystemMessagesDeliverypointId:
					DesetupSyncSystemMessagesDeliverypointEntity(true, false);
					_alreadyFetchedSystemMessagesDeliverypointEntity = false;
					break;
				case TerminalFieldIndex.AltSystemMessagesDeliverypointId:
					DesetupSyncAltSystemMessagesDeliverypointEntity(true, false);
					_alreadyFetchedAltSystemMessagesDeliverypointEntity = false;
					break;
				case TerminalFieldIndex.IcrtouchprintermappingId:
					DesetupSyncIcrtouchprintermappingEntity(true, false);
					_alreadyFetchedIcrtouchprintermappingEntity = false;
					break;
				case TerminalFieldIndex.ForwardToTerminalId:
					DesetupSyncForwardToTerminalEntity(true, false);
					_alreadyFetchedForwardToTerminalEntity = false;
					break;
				case TerminalFieldIndex.DeviceId:
					DesetupSyncDeviceEntity(true, false);
					_alreadyFetchedDeviceEntity = false;
					break;
				case TerminalFieldIndex.Browser1:
					DesetupSyncBrowser1EntertainmentEntity(true, false);
					_alreadyFetchedBrowser1EntertainmentEntity = false;
					break;
				case TerminalFieldIndex.Browser2:
					DesetupSyncBrowser2EntertainmentEntity(true, false);
					_alreadyFetchedBrowser2EntertainmentEntity = false;
					break;
				case TerminalFieldIndex.CmsPage:
					DesetupSyncCmsPageEntertainmentEntity(true, false);
					_alreadyFetchedCmsPageEntertainmentEntity = false;
					break;
				case TerminalFieldIndex.AutomaticSignOnUserId:
					DesetupSyncAutomaticSignOnUserEntity(true, false);
					_alreadyFetchedAutomaticSignOnUserEntity = false;
					break;
				case TerminalFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case TerminalFieldIndex.UIModeId:
					DesetupSyncUIModeEntity(true, false);
					_alreadyFetchedUIModeEntity = false;
					break;
				case TerminalFieldIndex.TerminalConfigurationId:
					DesetupSyncTerminalConfigurationEntity(true, false);
					_alreadyFetchedTerminalConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollection = (_icrtouchprintermappingCollection.Count > 0);
			_alreadyFetchedReceivedNetmessageCollection = (_receivedNetmessageCollection.Count > 0);
			_alreadyFetchedSentNetmessageCollection = (_sentNetmessageCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerCollection_ = (_orderRoutestephandlerCollection_.Count > 0);
			_alreadyFetchedOrderRoutestephandlerCollection = (_orderRoutestephandlerCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerHistoryCollection_ = (_orderRoutestephandlerHistoryCollection_.Count > 0);
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = (_orderRoutestephandlerHistoryCollection.Count > 0);
			_alreadyFetchedRoutestephandlerCollection = (_routestephandlerCollection.Count > 0);
			_alreadyFetchedScheduledCommandCollection = (_scheduledCommandCollection.Count > 0);
			_alreadyFetchedForwardedFromTerminalCollection = (_forwardedFromTerminalCollection.Count > 0);
			_alreadyFetchedTerminalConfigurationCollection = (_terminalConfigurationCollection.Count > 0);
			_alreadyFetchedTerminalLogCollection = (_terminalLogCollection.Count > 0);
			_alreadyFetchedTerminalMessageTemplateCategoryCollection = (_terminalMessageTemplateCategoryCollection.Count > 0);
			_alreadyFetchedTerminalStateCollection = (_terminalStateCollection.Count > 0);
			_alreadyFetchedTimestampCollection = (_timestampCollection.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = (_announcementCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedClientCollectionViaNetmessage = (_clientCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = (_companyCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage = (_companyCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage_ = (_companyCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage__ = (_companyCollectionViaNetmessage__.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage___ = (_companyCollectionViaNetmessage___.Count > 0);
			_alreadyFetchedCompanyCollectionViaTerminal = (_companyCollectionViaTerminal.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage = (_customerCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage_ = (_customerCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage__ = (_customerCollectionViaNetmessage__.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage___ = (_customerCollectionViaNetmessage___.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage = (_deliverypointCollectionViaNetmessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage_ = (_deliverypointCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage__ = (_deliverypointCollectionViaNetmessage__.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage___ = (_deliverypointCollectionViaNetmessage___.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal = (_deliverypointCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = (_deliverypointCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = (_deliverypointgroupCollectionViaNetmessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = (_deliverypointgroupCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = (_deliverypointgroupCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal = (_deviceCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = (_icrtouchprintermappingCollectionViaTerminal.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = (_menuCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler = (_orderCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ = (_orderCollectionViaOrderRoutestephandler_.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = (_orderCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ = (_orderCollectionViaOrderRoutestephandlerHistory_.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = (_posdeliverypointgroupCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal = (_productCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal_ = (_productCollectionViaTerminal_.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal__ = (_productCollectionViaTerminal__.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal___ = (_productCollectionViaTerminal___.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = (_routeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = (_routeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedRoutestepCollectionViaRoutestephandler = (_routestepCollectionViaRoutestephandler.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = (_supportpoolCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ = (_supportpoolCollectionViaOrderRoutestephandler_.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = (_supportpoolCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ = (_supportpoolCollectionViaOrderRoutestephandlerHistory_.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaRoutestephandler = (_supportpoolCollectionViaRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage = (_terminalCollectionViaNetmessage.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage_ = (_terminalCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = (_terminalCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = (_terminalCollectionViaOrderRoutestephandler_.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = (_terminalCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = (_terminalCollectionViaOrderRoutestephandlerHistory_.Count > 0);
			_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = (_terminalLogFileCollectionViaTerminalLog.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = (_uIModeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = (_uIModeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal = (_uIModeCollectionViaTerminal.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = (_uIModeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal = (_automaticSignOnUserCollectionViaTerminal.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedAltSystemMessagesDeliverypointEntity = (_altSystemMessagesDeliverypointEntity != null);
			_alreadyFetchedSystemMessagesDeliverypointEntity = (_systemMessagesDeliverypointEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedDeviceEntity = (_deviceEntity != null);
			_alreadyFetchedBrowser1EntertainmentEntity = (_browser1EntertainmentEntity != null);
			_alreadyFetchedBrowser2EntertainmentEntity = (_browser2EntertainmentEntity != null);
			_alreadyFetchedCmsPageEntertainmentEntity = (_cmsPageEntertainmentEntity != null);
			_alreadyFetchedIcrtouchprintermappingEntity = (_icrtouchprintermappingEntity != null);
			_alreadyFetchedBatteryLowProductEntity = (_batteryLowProductEntity != null);
			_alreadyFetchedClientDisconnectedProductEntity = (_clientDisconnectedProductEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedUnlockDeliverypointProductEntity = (_unlockDeliverypointProductEntity != null);
			_alreadyFetchedForwardToTerminalEntity = (_forwardToTerminalEntity != null);
			_alreadyFetchedTerminalConfigurationEntity = (_terminalConfigurationEntity != null);
			_alreadyFetchedUIModeEntity = (_uIModeEntity != null);
			_alreadyFetchedAutomaticSignOnUserEntity = (_automaticSignOnUserEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "AltSystemMessagesDeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId);
					break;
				case "SystemMessagesDeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "DeviceEntity":
					toReturn.Add(Relations.DeviceEntityUsingDeviceId);
					break;
				case "Browser1EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingBrowser1);
					break;
				case "Browser2EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingBrowser2);
					break;
				case "CmsPageEntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingCmsPage);
					break;
				case "IcrtouchprintermappingEntity":
					toReturn.Add(Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
					break;
				case "BatteryLowProductEntity":
					toReturn.Add(Relations.ProductEntityUsingBatteryLowProductId);
					break;
				case "ClientDisconnectedProductEntity":
					toReturn.Add(Relations.ProductEntityUsingClientDisconnectedProductId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingOrderFailedProductId);
					break;
				case "UnlockDeliverypointProductEntity":
					toReturn.Add(Relations.ProductEntityUsingUnlockDeliverypointProductId);
					break;
				case "ForwardToTerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingTerminalIdForwardToTerminalId);
					break;
				case "TerminalConfigurationEntity":
					toReturn.Add(Relations.TerminalConfigurationEntityUsingTerminalConfigurationId);
					break;
				case "UIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingUIModeId);
					break;
				case "AutomaticSignOnUserEntity":
					toReturn.Add(Relations.UserEntityUsingAutomaticSignOnUserId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId);
					break;
				case "IcrtouchprintermappingCollection":
					toReturn.Add(Relations.IcrtouchprintermappingEntityUsingTerminalId);
					break;
				case "ReceivedNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId);
					break;
				case "SentNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId);
					break;
				case "OrderRoutestephandlerCollection_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingForwardedFromTerminalId);
					break;
				case "OrderRoutestephandlerCollection":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingTerminalId);
					break;
				case "OrderRoutestephandlerHistoryCollection_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId);
					break;
				case "RoutestephandlerCollection":
					toReturn.Add(Relations.RoutestephandlerEntityUsingTerminalId);
					break;
				case "ScheduledCommandCollection":
					toReturn.Add(Relations.ScheduledCommandEntityUsingTerminalId);
					break;
				case "ForwardedFromTerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId);
					break;
				case "TerminalConfigurationCollection":
					toReturn.Add(Relations.TerminalConfigurationEntityUsingTerminalId);
					break;
				case "TerminalLogCollection":
					toReturn.Add(Relations.TerminalLogEntityUsingTerminalId);
					break;
				case "TerminalMessageTemplateCategoryCollection":
					toReturn.Add(Relations.TerminalMessageTemplateCategoryEntityUsingTerminalId);
					break;
				case "TerminalStateCollection":
					toReturn.Add(Relations.TerminalStateEntityUsingTerminalId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingTerminalId);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.ClientEntityUsingSenderClientId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingSenderCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingSenderCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage__":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingReceiverCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage___":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingReceiverCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingSenderCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingSenderCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage__":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingReceiverCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage___":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingReceiverCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingSenderDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingSenderDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage__":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingReceiverDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage___":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingReceiverDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandler_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingForwardedFromTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingBatteryLowProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingClientDisconnectedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingOrderFailedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingUnlockDeliverypointProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RoutestepCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.RoutestepEntityUsingRoutestepId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingForwardedFromTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderTerminalId, "TerminalEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId, "TerminalEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalLogFileCollectionViaTerminalLog":
					toReturn.Add(Relations.TerminalLogEntityUsingTerminalId, "TerminalEntity__", "TerminalLog_", JoinHint.None);
					toReturn.Add(TerminalLogEntity.Relations.TerminalLogFileEntityUsingTerminalLogFileId, "TerminalLog_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingXTerminalId, "TerminalEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "AutomaticSignOnUserCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingForwardToTerminalId, "TerminalEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_icrtouchprintermappingCollection", (!this.MarkedForDeletion?_icrtouchprintermappingCollection:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollection", _alwaysFetchIcrtouchprintermappingCollection);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollection", _alreadyFetchedIcrtouchprintermappingCollection);
			info.AddValue("_receivedNetmessageCollection", (!this.MarkedForDeletion?_receivedNetmessageCollection:null));
			info.AddValue("_alwaysFetchReceivedNetmessageCollection", _alwaysFetchReceivedNetmessageCollection);
			info.AddValue("_alreadyFetchedReceivedNetmessageCollection", _alreadyFetchedReceivedNetmessageCollection);
			info.AddValue("_sentNetmessageCollection", (!this.MarkedForDeletion?_sentNetmessageCollection:null));
			info.AddValue("_alwaysFetchSentNetmessageCollection", _alwaysFetchSentNetmessageCollection);
			info.AddValue("_alreadyFetchedSentNetmessageCollection", _alreadyFetchedSentNetmessageCollection);
			info.AddValue("_orderRoutestephandlerCollection_", (!this.MarkedForDeletion?_orderRoutestephandlerCollection_:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerCollection_", _alwaysFetchOrderRoutestephandlerCollection_);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerCollection_", _alreadyFetchedOrderRoutestephandlerCollection_);
			info.AddValue("_orderRoutestephandlerCollection", (!this.MarkedForDeletion?_orderRoutestephandlerCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerCollection", _alwaysFetchOrderRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerCollection", _alreadyFetchedOrderRoutestephandlerCollection);
			info.AddValue("_orderRoutestephandlerHistoryCollection_", (!this.MarkedForDeletion?_orderRoutestephandlerHistoryCollection_:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerHistoryCollection_", _alwaysFetchOrderRoutestephandlerHistoryCollection_);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerHistoryCollection_", _alreadyFetchedOrderRoutestephandlerHistoryCollection_);
			info.AddValue("_orderRoutestephandlerHistoryCollection", (!this.MarkedForDeletion?_orderRoutestephandlerHistoryCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerHistoryCollection", _alwaysFetchOrderRoutestephandlerHistoryCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerHistoryCollection", _alreadyFetchedOrderRoutestephandlerHistoryCollection);
			info.AddValue("_routestephandlerCollection", (!this.MarkedForDeletion?_routestephandlerCollection:null));
			info.AddValue("_alwaysFetchRoutestephandlerCollection", _alwaysFetchRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedRoutestephandlerCollection", _alreadyFetchedRoutestephandlerCollection);
			info.AddValue("_scheduledCommandCollection", (!this.MarkedForDeletion?_scheduledCommandCollection:null));
			info.AddValue("_alwaysFetchScheduledCommandCollection", _alwaysFetchScheduledCommandCollection);
			info.AddValue("_alreadyFetchedScheduledCommandCollection", _alreadyFetchedScheduledCommandCollection);
			info.AddValue("_forwardedFromTerminalCollection", (!this.MarkedForDeletion?_forwardedFromTerminalCollection:null));
			info.AddValue("_alwaysFetchForwardedFromTerminalCollection", _alwaysFetchForwardedFromTerminalCollection);
			info.AddValue("_alreadyFetchedForwardedFromTerminalCollection", _alreadyFetchedForwardedFromTerminalCollection);
			info.AddValue("_terminalConfigurationCollection", (!this.MarkedForDeletion?_terminalConfigurationCollection:null));
			info.AddValue("_alwaysFetchTerminalConfigurationCollection", _alwaysFetchTerminalConfigurationCollection);
			info.AddValue("_alreadyFetchedTerminalConfigurationCollection", _alreadyFetchedTerminalConfigurationCollection);
			info.AddValue("_terminalLogCollection", (!this.MarkedForDeletion?_terminalLogCollection:null));
			info.AddValue("_alwaysFetchTerminalLogCollection", _alwaysFetchTerminalLogCollection);
			info.AddValue("_alreadyFetchedTerminalLogCollection", _alreadyFetchedTerminalLogCollection);
			info.AddValue("_terminalMessageTemplateCategoryCollection", (!this.MarkedForDeletion?_terminalMessageTemplateCategoryCollection:null));
			info.AddValue("_alwaysFetchTerminalMessageTemplateCategoryCollection", _alwaysFetchTerminalMessageTemplateCategoryCollection);
			info.AddValue("_alreadyFetchedTerminalMessageTemplateCategoryCollection", _alreadyFetchedTerminalMessageTemplateCategoryCollection);
			info.AddValue("_terminalStateCollection", (!this.MarkedForDeletion?_terminalStateCollection:null));
			info.AddValue("_alwaysFetchTerminalStateCollection", _alwaysFetchTerminalStateCollection);
			info.AddValue("_alreadyFetchedTerminalStateCollection", _alreadyFetchedTerminalStateCollection);
			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);
			info.AddValue("_announcementCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_clientCollectionViaNetmessage", (!this.MarkedForDeletion?_clientCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchClientCollectionViaNetmessage", _alwaysFetchClientCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedClientCollectionViaNetmessage", _alreadyFetchedClientCollectionViaNetmessage);
			info.AddValue("_companyCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup", _alwaysFetchCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup", _alreadyFetchedCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_companyCollectionViaNetmessage", (!this.MarkedForDeletion?_companyCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage", _alwaysFetchCompanyCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage", _alreadyFetchedCompanyCollectionViaNetmessage);
			info.AddValue("_companyCollectionViaNetmessage_", (!this.MarkedForDeletion?_companyCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage_", _alwaysFetchCompanyCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage_", _alreadyFetchedCompanyCollectionViaNetmessage_);
			info.AddValue("_companyCollectionViaNetmessage__", (!this.MarkedForDeletion?_companyCollectionViaNetmessage__:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage__", _alwaysFetchCompanyCollectionViaNetmessage__);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage__", _alreadyFetchedCompanyCollectionViaNetmessage__);
			info.AddValue("_companyCollectionViaNetmessage___", (!this.MarkedForDeletion?_companyCollectionViaNetmessage___:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage___", _alwaysFetchCompanyCollectionViaNetmessage___);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage___", _alreadyFetchedCompanyCollectionViaNetmessage___);
			info.AddValue("_companyCollectionViaTerminal", (!this.MarkedForDeletion?_companyCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaTerminal", _alwaysFetchCompanyCollectionViaTerminal);
			info.AddValue("_alreadyFetchedCompanyCollectionViaTerminal", _alreadyFetchedCompanyCollectionViaTerminal);
			info.AddValue("_customerCollectionViaNetmessage", (!this.MarkedForDeletion?_customerCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage", _alwaysFetchCustomerCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage", _alreadyFetchedCustomerCollectionViaNetmessage);
			info.AddValue("_customerCollectionViaNetmessage_", (!this.MarkedForDeletion?_customerCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage_", _alwaysFetchCustomerCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage_", _alreadyFetchedCustomerCollectionViaNetmessage_);
			info.AddValue("_customerCollectionViaNetmessage__", (!this.MarkedForDeletion?_customerCollectionViaNetmessage__:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage__", _alwaysFetchCustomerCollectionViaNetmessage__);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage__", _alreadyFetchedCustomerCollectionViaNetmessage__);
			info.AddValue("_customerCollectionViaNetmessage___", (!this.MarkedForDeletion?_customerCollectionViaNetmessage___:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage___", _alwaysFetchCustomerCollectionViaNetmessage___);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage___", _alreadyFetchedCustomerCollectionViaNetmessage___);
			info.AddValue("_deliverypointCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage", _alwaysFetchDeliverypointCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage", _alreadyFetchedDeliverypointCollectionViaNetmessage);
			info.AddValue("_deliverypointCollectionViaNetmessage_", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage_", _alwaysFetchDeliverypointCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage_", _alreadyFetchedDeliverypointCollectionViaNetmessage_);
			info.AddValue("_deliverypointCollectionViaNetmessage__", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage__:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage__", _alwaysFetchDeliverypointCollectionViaNetmessage__);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage__", _alreadyFetchedDeliverypointCollectionViaNetmessage__);
			info.AddValue("_deliverypointCollectionViaNetmessage___", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage___:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage___", _alwaysFetchDeliverypointCollectionViaNetmessage___);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage___", _alreadyFetchedDeliverypointCollectionViaNetmessage___);
			info.AddValue("_deliverypointCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal", _alwaysFetchDeliverypointCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal", _alreadyFetchedDeliverypointCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal_", _alwaysFetchDeliverypointCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal_", _alreadyFetchedDeliverypointCollectionViaTerminal_);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage", _alwaysFetchDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_", _alwaysFetchDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_deliverypointgroupCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal", _alwaysFetchDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal", _alreadyFetchedDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_deviceCollectionViaTerminal", (!this.MarkedForDeletion?_deviceCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal", _alwaysFetchDeviceCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal", _alreadyFetchedDeviceCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_icrtouchprintermappingCollectionViaTerminal", (!this.MarkedForDeletion?_icrtouchprintermappingCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal", _alwaysFetchIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal", _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_menuCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup", _alwaysFetchMenuCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup", _alreadyFetchedMenuCollectionViaDeliverypointgroup);
			info.AddValue("_orderCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandler", _alwaysFetchOrderCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandler", _alreadyFetchedOrderCollectionViaOrderRoutestephandler);
			info.AddValue("_orderCollectionViaOrderRoutestephandler_", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandler_:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandler_", _alwaysFetchOrderCollectionViaOrderRoutestephandler_);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandler_", _alreadyFetchedOrderCollectionViaOrderRoutestephandler_);
			info.AddValue("_orderCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory", _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_orderCollectionViaOrderRoutestephandlerHistory_", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandlerHistory_:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_", _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_", _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_productCollectionViaTerminal", (!this.MarkedForDeletion?_productCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal", _alwaysFetchProductCollectionViaTerminal);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal", _alreadyFetchedProductCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal_", (!this.MarkedForDeletion?_productCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal_", _alwaysFetchProductCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal_", _alreadyFetchedProductCollectionViaTerminal_);
			info.AddValue("_productCollectionViaTerminal__", (!this.MarkedForDeletion?_productCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal__", _alwaysFetchProductCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal__", _alreadyFetchedProductCollectionViaTerminal__);
			info.AddValue("_productCollectionViaTerminal___", (!this.MarkedForDeletion?_productCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal___", _alwaysFetchProductCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal___", _alreadyFetchedProductCollectionViaTerminal___);
			info.AddValue("_routeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup", _alwaysFetchRouteCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup", _alreadyFetchedRouteCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_", _alwaysFetchRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_", _alreadyFetchedRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_routestepCollectionViaRoutestephandler", (!this.MarkedForDeletion?_routestepCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchRoutestepCollectionViaRoutestephandler", _alwaysFetchRoutestepCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedRoutestepCollectionViaRoutestephandler", _alreadyFetchedRoutestepCollectionViaRoutestephandler);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandler_", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandler_:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandlerHistory_", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandlerHistory_:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_supportpoolCollectionViaRoutestephandler", (!this.MarkedForDeletion?_supportpoolCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaRoutestephandler", _alwaysFetchSupportpoolCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaRoutestephandler", _alreadyFetchedSupportpoolCollectionViaRoutestephandler);
			info.AddValue("_terminalCollectionViaNetmessage", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage", _alwaysFetchTerminalCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage", _alreadyFetchedTerminalCollectionViaNetmessage);
			info.AddValue("_terminalCollectionViaNetmessage_", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage_", _alwaysFetchTerminalCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage_", _alreadyFetchedTerminalCollectionViaNetmessage_);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler", _alwaysFetchTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_", _alwaysFetchTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_terminalLogFileCollectionViaTerminalLog", (!this.MarkedForDeletion?_terminalLogFileCollectionViaTerminalLog:null));
			info.AddValue("_alwaysFetchTerminalLogFileCollectionViaTerminalLog", _alwaysFetchTerminalLogFileCollectionViaTerminalLog);
			info.AddValue("_alreadyFetchedTerminalLogFileCollectionViaTerminalLog", _alreadyFetchedTerminalLogFileCollectionViaTerminalLog);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_", _alwaysFetchUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup__", _alwaysFetchUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__", _alreadyFetchedUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_uIModeCollectionViaTerminal", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal", _alwaysFetchUIModeCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal", _alreadyFetchedUIModeCollectionViaTerminal);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup", _alwaysFetchUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup", _alreadyFetchedUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_automaticSignOnUserCollectionViaTerminal", (!this.MarkedForDeletion?_automaticSignOnUserCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchAutomaticSignOnUserCollectionViaTerminal", _alwaysFetchAutomaticSignOnUserCollectionViaTerminal);
			info.AddValue("_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal", _alreadyFetchedAutomaticSignOnUserCollectionViaTerminal);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_altSystemMessagesDeliverypointEntity", (!this.MarkedForDeletion?_altSystemMessagesDeliverypointEntity:null));
			info.AddValue("_altSystemMessagesDeliverypointEntityReturnsNewIfNotFound", _altSystemMessagesDeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAltSystemMessagesDeliverypointEntity", _alwaysFetchAltSystemMessagesDeliverypointEntity);
			info.AddValue("_alreadyFetchedAltSystemMessagesDeliverypointEntity", _alreadyFetchedAltSystemMessagesDeliverypointEntity);
			info.AddValue("_systemMessagesDeliverypointEntity", (!this.MarkedForDeletion?_systemMessagesDeliverypointEntity:null));
			info.AddValue("_systemMessagesDeliverypointEntityReturnsNewIfNotFound", _systemMessagesDeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSystemMessagesDeliverypointEntity", _alwaysFetchSystemMessagesDeliverypointEntity);
			info.AddValue("_alreadyFetchedSystemMessagesDeliverypointEntity", _alreadyFetchedSystemMessagesDeliverypointEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_deviceEntity", (!this.MarkedForDeletion?_deviceEntity:null));
			info.AddValue("_deviceEntityReturnsNewIfNotFound", _deviceEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceEntity", _alwaysFetchDeviceEntity);
			info.AddValue("_alreadyFetchedDeviceEntity", _alreadyFetchedDeviceEntity);
			info.AddValue("_browser1EntertainmentEntity", (!this.MarkedForDeletion?_browser1EntertainmentEntity:null));
			info.AddValue("_browser1EntertainmentEntityReturnsNewIfNotFound", _browser1EntertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrowser1EntertainmentEntity", _alwaysFetchBrowser1EntertainmentEntity);
			info.AddValue("_alreadyFetchedBrowser1EntertainmentEntity", _alreadyFetchedBrowser1EntertainmentEntity);
			info.AddValue("_browser2EntertainmentEntity", (!this.MarkedForDeletion?_browser2EntertainmentEntity:null));
			info.AddValue("_browser2EntertainmentEntityReturnsNewIfNotFound", _browser2EntertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrowser2EntertainmentEntity", _alwaysFetchBrowser2EntertainmentEntity);
			info.AddValue("_alreadyFetchedBrowser2EntertainmentEntity", _alreadyFetchedBrowser2EntertainmentEntity);
			info.AddValue("_cmsPageEntertainmentEntity", (!this.MarkedForDeletion?_cmsPageEntertainmentEntity:null));
			info.AddValue("_cmsPageEntertainmentEntityReturnsNewIfNotFound", _cmsPageEntertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCmsPageEntertainmentEntity", _alwaysFetchCmsPageEntertainmentEntity);
			info.AddValue("_alreadyFetchedCmsPageEntertainmentEntity", _alreadyFetchedCmsPageEntertainmentEntity);
			info.AddValue("_icrtouchprintermappingEntity", (!this.MarkedForDeletion?_icrtouchprintermappingEntity:null));
			info.AddValue("_icrtouchprintermappingEntityReturnsNewIfNotFound", _icrtouchprintermappingEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIcrtouchprintermappingEntity", _alwaysFetchIcrtouchprintermappingEntity);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingEntity", _alreadyFetchedIcrtouchprintermappingEntity);
			info.AddValue("_batteryLowProductEntity", (!this.MarkedForDeletion?_batteryLowProductEntity:null));
			info.AddValue("_batteryLowProductEntityReturnsNewIfNotFound", _batteryLowProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBatteryLowProductEntity", _alwaysFetchBatteryLowProductEntity);
			info.AddValue("_alreadyFetchedBatteryLowProductEntity", _alreadyFetchedBatteryLowProductEntity);
			info.AddValue("_clientDisconnectedProductEntity", (!this.MarkedForDeletion?_clientDisconnectedProductEntity:null));
			info.AddValue("_clientDisconnectedProductEntityReturnsNewIfNotFound", _clientDisconnectedProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientDisconnectedProductEntity", _alwaysFetchClientDisconnectedProductEntity);
			info.AddValue("_alreadyFetchedClientDisconnectedProductEntity", _alreadyFetchedClientDisconnectedProductEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_unlockDeliverypointProductEntity", (!this.MarkedForDeletion?_unlockDeliverypointProductEntity:null));
			info.AddValue("_unlockDeliverypointProductEntityReturnsNewIfNotFound", _unlockDeliverypointProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnlockDeliverypointProductEntity", _alwaysFetchUnlockDeliverypointProductEntity);
			info.AddValue("_alreadyFetchedUnlockDeliverypointProductEntity", _alreadyFetchedUnlockDeliverypointProductEntity);
			info.AddValue("_forwardToTerminalEntity", (!this.MarkedForDeletion?_forwardToTerminalEntity:null));
			info.AddValue("_forwardToTerminalEntityReturnsNewIfNotFound", _forwardToTerminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForwardToTerminalEntity", _alwaysFetchForwardToTerminalEntity);
			info.AddValue("_alreadyFetchedForwardToTerminalEntity", _alreadyFetchedForwardToTerminalEntity);
			info.AddValue("_terminalConfigurationEntity", (!this.MarkedForDeletion?_terminalConfigurationEntity:null));
			info.AddValue("_terminalConfigurationEntityReturnsNewIfNotFound", _terminalConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalConfigurationEntity", _alwaysFetchTerminalConfigurationEntity);
			info.AddValue("_alreadyFetchedTerminalConfigurationEntity", _alreadyFetchedTerminalConfigurationEntity);
			info.AddValue("_uIModeEntity", (!this.MarkedForDeletion?_uIModeEntity:null));
			info.AddValue("_uIModeEntityReturnsNewIfNotFound", _uIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIModeEntity", _alwaysFetchUIModeEntity);
			info.AddValue("_alreadyFetchedUIModeEntity", _alreadyFetchedUIModeEntity);
			info.AddValue("_automaticSignOnUserEntity", (!this.MarkedForDeletion?_automaticSignOnUserEntity:null));
			info.AddValue("_automaticSignOnUserEntityReturnsNewIfNotFound", _automaticSignOnUserEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutomaticSignOnUserEntity", _alwaysFetchAutomaticSignOnUserEntity);
			info.AddValue("_alreadyFetchedAutomaticSignOnUserEntity", _alreadyFetchedAutomaticSignOnUserEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "AltSystemMessagesDeliverypointEntity":
					_alreadyFetchedAltSystemMessagesDeliverypointEntity = true;
					this.AltSystemMessagesDeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "SystemMessagesDeliverypointEntity":
					_alreadyFetchedSystemMessagesDeliverypointEntity = true;
					this.SystemMessagesDeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "DeviceEntity":
					_alreadyFetchedDeviceEntity = true;
					this.DeviceEntity = (DeviceEntity)entity;
					break;
				case "Browser1EntertainmentEntity":
					_alreadyFetchedBrowser1EntertainmentEntity = true;
					this.Browser1EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "Browser2EntertainmentEntity":
					_alreadyFetchedBrowser2EntertainmentEntity = true;
					this.Browser2EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "CmsPageEntertainmentEntity":
					_alreadyFetchedCmsPageEntertainmentEntity = true;
					this.CmsPageEntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "IcrtouchprintermappingEntity":
					_alreadyFetchedIcrtouchprintermappingEntity = true;
					this.IcrtouchprintermappingEntity = (IcrtouchprintermappingEntity)entity;
					break;
				case "BatteryLowProductEntity":
					_alreadyFetchedBatteryLowProductEntity = true;
					this.BatteryLowProductEntity = (ProductEntity)entity;
					break;
				case "ClientDisconnectedProductEntity":
					_alreadyFetchedClientDisconnectedProductEntity = true;
					this.ClientDisconnectedProductEntity = (ProductEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "UnlockDeliverypointProductEntity":
					_alreadyFetchedUnlockDeliverypointProductEntity = true;
					this.UnlockDeliverypointProductEntity = (ProductEntity)entity;
					break;
				case "ForwardToTerminalEntity":
					_alreadyFetchedForwardToTerminalEntity = true;
					this.ForwardToTerminalEntity = (TerminalEntity)entity;
					break;
				case "TerminalConfigurationEntity":
					_alreadyFetchedTerminalConfigurationEntity = true;
					this.TerminalConfigurationEntity = (TerminalConfigurationEntity)entity;
					break;
				case "UIModeEntity":
					_alreadyFetchedUIModeEntity = true;
					this.UIModeEntity = (UIModeEntity)entity;
					break;
				case "AutomaticSignOnUserEntity":
					_alreadyFetchedAutomaticSignOnUserEntity = true;
					this.AutomaticSignOnUserEntity = (UserEntity)entity;
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollection":
					_alreadyFetchedIcrtouchprintermappingCollection = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollection.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "ReceivedNetmessageCollection":
					_alreadyFetchedReceivedNetmessageCollection = true;
					if(entity!=null)
					{
						this.ReceivedNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "SentNetmessageCollection":
					_alreadyFetchedSentNetmessageCollection = true;
					if(entity!=null)
					{
						this.SentNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "OrderRoutestephandlerCollection_":
					_alreadyFetchedOrderRoutestephandlerCollection_ = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerCollection_.Add((OrderRoutestephandlerEntity)entity);
					}
					break;
				case "OrderRoutestephandlerCollection":
					_alreadyFetchedOrderRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)entity);
					}
					break;
				case "OrderRoutestephandlerHistoryCollection_":
					_alreadyFetchedOrderRoutestephandlerHistoryCollection_ = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerHistoryCollection_.Add((OrderRoutestephandlerHistoryEntity)entity);
					}
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)entity);
					}
					break;
				case "RoutestephandlerCollection":
					_alreadyFetchedRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.RoutestephandlerCollection.Add((RoutestephandlerEntity)entity);
					}
					break;
				case "ScheduledCommandCollection":
					_alreadyFetchedScheduledCommandCollection = true;
					if(entity!=null)
					{
						this.ScheduledCommandCollection.Add((ScheduledCommandEntity)entity);
					}
					break;
				case "ForwardedFromTerminalCollection":
					_alreadyFetchedForwardedFromTerminalCollection = true;
					if(entity!=null)
					{
						this.ForwardedFromTerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalConfigurationCollection":
					_alreadyFetchedTerminalConfigurationCollection = true;
					if(entity!=null)
					{
						this.TerminalConfigurationCollection.Add((TerminalConfigurationEntity)entity);
					}
					break;
				case "TerminalLogCollection":
					_alreadyFetchedTerminalLogCollection = true;
					if(entity!=null)
					{
						this.TerminalLogCollection.Add((TerminalLogEntity)entity);
					}
					break;
				case "TerminalMessageTemplateCategoryCollection":
					_alreadyFetchedTerminalMessageTemplateCategoryCollection = true;
					if(entity!=null)
					{
						this.TerminalMessageTemplateCategoryCollection.Add((TerminalMessageTemplateCategoryEntity)entity);
					}
					break;
				case "TerminalStateCollection":
					_alreadyFetchedTerminalStateCollection = true;
					if(entity!=null)
					{
						this.TerminalStateCollection.Add((TerminalStateEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					if(entity!=null)
					{
						this.TimestampCollection.Add((TimestampEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup.Add((AnnouncementEntity)entity);
					}
					break;
				case "ClientCollectionViaNetmessage":
					_alreadyFetchedClientCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.ClientCollectionViaNetmessage.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage":
					_alreadyFetchedCompanyCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage_":
					_alreadyFetchedCompanyCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage_.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage__":
					_alreadyFetchedCompanyCollectionViaNetmessage__ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage__.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage___":
					_alreadyFetchedCompanyCollectionViaNetmessage___ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage___.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaTerminal":
					_alreadyFetchedCompanyCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaTerminal.Add((CompanyEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage":
					_alreadyFetchedCustomerCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage.Add((CustomerEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage_":
					_alreadyFetchedCustomerCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage_.Add((CustomerEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage__":
					_alreadyFetchedCustomerCollectionViaNetmessage__ = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage__.Add((CustomerEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage___":
					_alreadyFetchedCustomerCollectionViaNetmessage___ = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage___.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage":
					_alreadyFetchedDeliverypointCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage_":
					_alreadyFetchedDeliverypointCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage__":
					_alreadyFetchedDeliverypointCollectionViaNetmessage__ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage__.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage___":
					_alreadyFetchedDeliverypointCollectionViaNetmessage___ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage___.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal":
					_alreadyFetchedDeliverypointCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal_":
					_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal":
					_alreadyFetchedDeviceCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal.Add((DeviceEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollectionViaTerminal.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup.Add((MenuEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandler":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandler.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandler_":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandler_.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandlerHistory.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory_":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandlerHistory_.Add((OrderEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal":
					_alreadyFetchedProductCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal_":
					_alreadyFetchedProductCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal__":
					_alreadyFetchedProductCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal___":
					_alreadyFetchedProductCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal___.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_.Add((RouteEntity)entity);
					}
					break;
				case "RoutestepCollectionViaRoutestephandler":
					_alreadyFetchedRoutestepCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.RoutestepCollectionViaRoutestephandler.Add((RoutestepEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandler.Add((SupportpoolEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler_":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandler_.Add((SupportpoolEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandlerHistory.Add((SupportpoolEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory_":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandlerHistory_.Add((SupportpoolEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaRoutestephandler":
					_alreadyFetchedSupportpoolCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaRoutestephandler.Add((SupportpoolEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage":
					_alreadyFetchedTerminalCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage_":
					_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalLogFileCollectionViaTerminalLog":
					_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = true;
					if(entity!=null)
					{
						this.TerminalLogFileCollectionViaTerminalLog.Add((TerminalLogFileEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup__.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal":
					_alreadyFetchedUIModeCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup.Add((UIModeEntity)entity);
					}
					break;
				case "AutomaticSignOnUserCollectionViaTerminal":
					_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.AutomaticSignOnUserCollectionViaTerminal.Add((UserEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "AltSystemMessagesDeliverypointEntity":
					SetupSyncAltSystemMessagesDeliverypointEntity(relatedEntity);
					break;
				case "SystemMessagesDeliverypointEntity":
					SetupSyncSystemMessagesDeliverypointEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "DeviceEntity":
					SetupSyncDeviceEntity(relatedEntity);
					break;
				case "Browser1EntertainmentEntity":
					SetupSyncBrowser1EntertainmentEntity(relatedEntity);
					break;
				case "Browser2EntertainmentEntity":
					SetupSyncBrowser2EntertainmentEntity(relatedEntity);
					break;
				case "CmsPageEntertainmentEntity":
					SetupSyncCmsPageEntertainmentEntity(relatedEntity);
					break;
				case "IcrtouchprintermappingEntity":
					SetupSyncIcrtouchprintermappingEntity(relatedEntity);
					break;
				case "BatteryLowProductEntity":
					SetupSyncBatteryLowProductEntity(relatedEntity);
					break;
				case "ClientDisconnectedProductEntity":
					SetupSyncClientDisconnectedProductEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "UnlockDeliverypointProductEntity":
					SetupSyncUnlockDeliverypointProductEntity(relatedEntity);
					break;
				case "ForwardToTerminalEntity":
					SetupSyncForwardToTerminalEntity(relatedEntity);
					break;
				case "TerminalConfigurationEntity":
					SetupSyncTerminalConfigurationEntity(relatedEntity);
					break;
				case "UIModeEntity":
					SetupSyncUIModeEntity(relatedEntity);
					break;
				case "AutomaticSignOnUserEntity":
					SetupSyncAutomaticSignOnUserEntity(relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "IcrtouchprintermappingCollection":
					_icrtouchprintermappingCollection.Add((IcrtouchprintermappingEntity)relatedEntity);
					break;
				case "ReceivedNetmessageCollection":
					_receivedNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "SentNetmessageCollection":
					_sentNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerCollection_":
					_orderRoutestephandlerCollection_.Add((OrderRoutestephandlerEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerCollection":
					_orderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerHistoryCollection_":
					_orderRoutestephandlerHistoryCollection_.Add((OrderRoutestephandlerHistoryEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_orderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)relatedEntity);
					break;
				case "RoutestephandlerCollection":
					_routestephandlerCollection.Add((RoutestephandlerEntity)relatedEntity);
					break;
				case "ScheduledCommandCollection":
					_scheduledCommandCollection.Add((ScheduledCommandEntity)relatedEntity);
					break;
				case "ForwardedFromTerminalCollection":
					_forwardedFromTerminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				case "TerminalConfigurationCollection":
					_terminalConfigurationCollection.Add((TerminalConfigurationEntity)relatedEntity);
					break;
				case "TerminalLogCollection":
					_terminalLogCollection.Add((TerminalLogEntity)relatedEntity);
					break;
				case "TerminalMessageTemplateCategoryCollection":
					_terminalMessageTemplateCategoryCollection.Add((TerminalMessageTemplateCategoryEntity)relatedEntity);
					break;
				case "TerminalStateCollection":
					_terminalStateCollection.Add((TerminalStateEntity)relatedEntity);
					break;
				case "TimestampCollection":
					_timestampCollection.Add((TimestampEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "AltSystemMessagesDeliverypointEntity":
					DesetupSyncAltSystemMessagesDeliverypointEntity(false, true);
					break;
				case "SystemMessagesDeliverypointEntity":
					DesetupSyncSystemMessagesDeliverypointEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "DeviceEntity":
					DesetupSyncDeviceEntity(false, true);
					break;
				case "Browser1EntertainmentEntity":
					DesetupSyncBrowser1EntertainmentEntity(false, true);
					break;
				case "Browser2EntertainmentEntity":
					DesetupSyncBrowser2EntertainmentEntity(false, true);
					break;
				case "CmsPageEntertainmentEntity":
					DesetupSyncCmsPageEntertainmentEntity(false, true);
					break;
				case "IcrtouchprintermappingEntity":
					DesetupSyncIcrtouchprintermappingEntity(false, true);
					break;
				case "BatteryLowProductEntity":
					DesetupSyncBatteryLowProductEntity(false, true);
					break;
				case "ClientDisconnectedProductEntity":
					DesetupSyncClientDisconnectedProductEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "UnlockDeliverypointProductEntity":
					DesetupSyncUnlockDeliverypointProductEntity(false, true);
					break;
				case "ForwardToTerminalEntity":
					DesetupSyncForwardToTerminalEntity(false, true);
					break;
				case "TerminalConfigurationEntity":
					DesetupSyncTerminalConfigurationEntity(false, true);
					break;
				case "UIModeEntity":
					DesetupSyncUIModeEntity(false, true);
					break;
				case "AutomaticSignOnUserEntity":
					DesetupSyncAutomaticSignOnUserEntity(false, true);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "IcrtouchprintermappingCollection":
					this.PerformRelatedEntityRemoval(_icrtouchprintermappingCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceivedNetmessageCollection":
					this.PerformRelatedEntityRemoval(_receivedNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SentNetmessageCollection":
					this.PerformRelatedEntityRemoval(_sentNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerCollection_":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerHistoryCollection_":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerHistoryCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_routestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledCommandCollection":
					this.PerformRelatedEntityRemoval(_scheduledCommandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ForwardedFromTerminalCollection":
					this.PerformRelatedEntityRemoval(_forwardedFromTerminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalConfigurationCollection":
					this.PerformRelatedEntityRemoval(_terminalConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalLogCollection":
					this.PerformRelatedEntityRemoval(_terminalLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalMessageTemplateCategoryCollection":
					this.PerformRelatedEntityRemoval(_terminalMessageTemplateCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalStateCollection":
					this.PerformRelatedEntityRemoval(_terminalStateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					this.PerformRelatedEntityRemoval(_timestampCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_altSystemMessagesDeliverypointEntity!=null)
			{
				toReturn.Add(_altSystemMessagesDeliverypointEntity);
			}
			if(_systemMessagesDeliverypointEntity!=null)
			{
				toReturn.Add(_systemMessagesDeliverypointEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_deviceEntity!=null)
			{
				toReturn.Add(_deviceEntity);
			}
			if(_browser1EntertainmentEntity!=null)
			{
				toReturn.Add(_browser1EntertainmentEntity);
			}
			if(_browser2EntertainmentEntity!=null)
			{
				toReturn.Add(_browser2EntertainmentEntity);
			}
			if(_cmsPageEntertainmentEntity!=null)
			{
				toReturn.Add(_cmsPageEntertainmentEntity);
			}
			if(_icrtouchprintermappingEntity!=null)
			{
				toReturn.Add(_icrtouchprintermappingEntity);
			}
			if(_batteryLowProductEntity!=null)
			{
				toReturn.Add(_batteryLowProductEntity);
			}
			if(_clientDisconnectedProductEntity!=null)
			{
				toReturn.Add(_clientDisconnectedProductEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_unlockDeliverypointProductEntity!=null)
			{
				toReturn.Add(_unlockDeliverypointProductEntity);
			}
			if(_forwardToTerminalEntity!=null)
			{
				toReturn.Add(_forwardToTerminalEntity);
			}
			if(_terminalConfigurationEntity!=null)
			{
				toReturn.Add(_terminalConfigurationEntity);
			}
			if(_uIModeEntity!=null)
			{
				toReturn.Add(_uIModeEntity);
			}
			if(_automaticSignOnUserEntity!=null)
			{
				toReturn.Add(_automaticSignOnUserEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_icrtouchprintermappingCollection);
			toReturn.Add(_receivedNetmessageCollection);
			toReturn.Add(_sentNetmessageCollection);
			toReturn.Add(_orderRoutestephandlerCollection_);
			toReturn.Add(_orderRoutestephandlerCollection);
			toReturn.Add(_orderRoutestephandlerHistoryCollection_);
			toReturn.Add(_orderRoutestephandlerHistoryCollection);
			toReturn.Add(_routestephandlerCollection);
			toReturn.Add(_scheduledCommandCollection);
			toReturn.Add(_forwardedFromTerminalCollection);
			toReturn.Add(_terminalConfigurationCollection);
			toReturn.Add(_terminalLogCollection);
			toReturn.Add(_terminalMessageTemplateCategoryCollection);
			toReturn.Add(_terminalStateCollection);
			toReturn.Add(_timestampCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalId)
		{
			return FetchUsingPK(terminalId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(terminalId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(terminalId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(terminalId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TerminalId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TerminalRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollection(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollection(forceFetch, _icrtouchprintermappingCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiIcrtouchprintermappingCollection(forceFetch, _icrtouchprintermappingCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiIcrtouchprintermappingCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollection || forceFetch || _alwaysFetchIcrtouchprintermappingCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollection);
				_icrtouchprintermappingCollection.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollection.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollection.GetMultiManyToOne(this, filter);
				_icrtouchprintermappingCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollection = true;
			}
			return _icrtouchprintermappingCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollection'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollection is requested or GetMultiIcrtouchprintermappingCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollection.SortClauses=sortClauses;
			_icrtouchprintermappingCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceivedNetmessageCollection || forceFetch || _alwaysFetchReceivedNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receivedNetmessageCollection);
				_receivedNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_receivedNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_receivedNetmessageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, filter);
				_receivedNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceivedNetmessageCollection = true;
			}
			return _receivedNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceivedNetmessageCollection'. These settings will be taken into account
		/// when the property ReceivedNetmessageCollection is requested or GetMultiReceivedNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceivedNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receivedNetmessageCollection.SortClauses=sortClauses;
			_receivedNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSentNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSentNetmessageCollection || forceFetch || _alwaysFetchSentNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sentNetmessageCollection);
				_sentNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_sentNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_sentNetmessageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, filter);
				_sentNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSentNetmessageCollection = true;
			}
			return _sentNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SentNetmessageCollection'. These settings will be taken into account
		/// when the property SentNetmessageCollection is requested or GetMultiSentNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSentNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sentNetmessageCollection.SortClauses=sortClauses;
			_sentNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection_(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerCollection_(forceFetch, _orderRoutestephandlerCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerCollection_(forceFetch, _orderRoutestephandlerCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerCollection_ || forceFetch || _alwaysFetchOrderRoutestephandlerCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerCollection_);
				_orderRoutestephandlerCollection_.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerCollection_.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerCollection_.GetMultiManyToOne(null, null, null, this, null, filter);
				_orderRoutestephandlerCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerCollection_ = true;
			}
			return _orderRoutestephandlerCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerCollection_'. These settings will be taken into account
		/// when the property OrderRoutestephandlerCollection_ is requested or GetMultiOrderRoutestephandlerCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerCollection_.SortClauses=sortClauses;
			_orderRoutestephandlerCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerCollection || forceFetch || _alwaysFetchOrderRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerCollection);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerCollection = true;
			}
			return _orderRoutestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerCollection is requested or GetMultiOrderRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerCollection.SortClauses=sortClauses;
			_orderRoutestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection_(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection_(forceFetch, _orderRoutestephandlerHistoryCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection_(forceFetch, _orderRoutestephandlerHistoryCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerHistoryCollection_ || forceFetch || _alwaysFetchOrderRoutestephandlerHistoryCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerHistoryCollection_);
				_orderRoutestephandlerHistoryCollection_.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerHistoryCollection_.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerHistoryCollection_.GetMultiManyToOne(null, null, null, this, null, filter);
				_orderRoutestephandlerHistoryCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerHistoryCollection_ = true;
			}
			return _orderRoutestephandlerHistoryCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerHistoryCollection_'. These settings will be taken into account
		/// when the property OrderRoutestephandlerHistoryCollection_ is requested or GetMultiOrderRoutestephandlerHistoryCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerHistoryCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerHistoryCollection_.SortClauses=sortClauses;
			_orderRoutestephandlerHistoryCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerHistoryCollection || forceFetch || _alwaysFetchOrderRoutestephandlerHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerHistoryCollection);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerHistoryCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
			}
			return _orderRoutestephandlerHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerHistoryCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerHistoryCollection is requested or GetMultiOrderRoutestephandlerHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerHistoryCollection.SortClauses=sortClauses;
			_orderRoutestephandlerHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutestephandlerCollection || forceFetch || _alwaysFetchRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestephandlerCollection);
				_routestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_routestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_routestephandlerCollection.GetMultiManyToOne(null, null, null, this, filter);
				_routestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestephandlerCollection = true;
			}
			return _routestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestephandlerCollection'. These settings will be taken into account
		/// when the property RoutestephandlerCollection is requested or GetMultiRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestephandlerCollection.SortClauses=sortClauses;
			_routestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch)
		{
			return GetMultiScheduledCommandCollection(forceFetch, _scheduledCommandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledCommandCollection(forceFetch, _scheduledCommandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledCommandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledCommandCollection || forceFetch || _alwaysFetchScheduledCommandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledCommandCollection);
				_scheduledCommandCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledCommandCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledCommandCollection.GetMultiManyToOne(null, null, this, filter);
				_scheduledCommandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledCommandCollection = true;
			}
			return _scheduledCommandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledCommandCollection'. These settings will be taken into account
		/// when the property ScheduledCommandCollection is requested or GetMultiScheduledCommandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledCommandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledCommandCollection.SortClauses=sortClauses;
			_scheduledCommandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiForwardedFromTerminalCollection(bool forceFetch)
		{
			return GetMultiForwardedFromTerminalCollection(forceFetch, _forwardedFromTerminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiForwardedFromTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiForwardedFromTerminalCollection(forceFetch, _forwardedFromTerminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiForwardedFromTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiForwardedFromTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiForwardedFromTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedForwardedFromTerminalCollection || forceFetch || _alwaysFetchForwardedFromTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_forwardedFromTerminalCollection);
				_forwardedFromTerminalCollection.SuppressClearInGetMulti=!forceFetch;
				_forwardedFromTerminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_forwardedFromTerminalCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_forwardedFromTerminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedForwardedFromTerminalCollection = true;
			}
			return _forwardedFromTerminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ForwardedFromTerminalCollection'. These settings will be taken into account
		/// when the property ForwardedFromTerminalCollection is requested or GetMultiForwardedFromTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersForwardedFromTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_forwardedFromTerminalCollection.SortClauses=sortClauses;
			_forwardedFromTerminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, _terminalConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, _terminalConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalConfigurationCollection GetMultiTerminalConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalConfigurationCollection || forceFetch || _alwaysFetchTerminalConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalConfigurationCollection);
				_terminalConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalConfigurationCollection.GetMultiManyToOne(null, this, null, filter);
				_terminalConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalConfigurationCollection = true;
			}
			return _terminalConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalConfigurationCollection'. These settings will be taken into account
		/// when the property TerminalConfigurationCollection is requested or GetMultiTerminalConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalConfigurationCollection.SortClauses=sortClauses;
			_terminalConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalLogCollection || forceFetch || _alwaysFetchTerminalLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalLogCollection);
				_terminalLogCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalLogCollection.GetMultiManyToOne(null, this, null, filter);
				_terminalLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalLogCollection = true;
			}
			return _terminalLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalLogCollection'. These settings will be taken into account
		/// when the property TerminalLogCollection is requested or GetMultiTerminalLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalLogCollection.SortClauses=sortClauses;
			_terminalLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalMessageTemplateCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalMessageTemplateCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection GetMultiTerminalMessageTemplateCategoryCollection(bool forceFetch)
		{
			return GetMultiTerminalMessageTemplateCategoryCollection(forceFetch, _terminalMessageTemplateCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalMessageTemplateCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalMessageTemplateCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection GetMultiTerminalMessageTemplateCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalMessageTemplateCategoryCollection(forceFetch, _terminalMessageTemplateCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalMessageTemplateCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection GetMultiTerminalMessageTemplateCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalMessageTemplateCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalMessageTemplateCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection GetMultiTerminalMessageTemplateCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalMessageTemplateCategoryCollection || forceFetch || _alwaysFetchTerminalMessageTemplateCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalMessageTemplateCategoryCollection);
				_terminalMessageTemplateCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalMessageTemplateCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalMessageTemplateCategoryCollection.GetMultiManyToOne(null, this, filter);
				_terminalMessageTemplateCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalMessageTemplateCategoryCollection = true;
			}
			return _terminalMessageTemplateCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalMessageTemplateCategoryCollection'. These settings will be taken into account
		/// when the property TerminalMessageTemplateCategoryCollection is requested or GetMultiTerminalMessageTemplateCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalMessageTemplateCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalMessageTemplateCategoryCollection.SortClauses=sortClauses;
			_terminalMessageTemplateCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalStateEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalStateCollection GetMultiTerminalStateCollection(bool forceFetch)
		{
			return GetMultiTerminalStateCollection(forceFetch, _terminalStateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalStateEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalStateCollection GetMultiTerminalStateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalStateCollection(forceFetch, _terminalStateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalStateCollection GetMultiTerminalStateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalStateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalStateCollection GetMultiTerminalStateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalStateCollection || forceFetch || _alwaysFetchTerminalStateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalStateCollection);
				_terminalStateCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalStateCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalStateCollection.GetMultiManyToOne(this, filter);
				_terminalStateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalStateCollection = true;
			}
			return _terminalStateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalStateCollection'. These settings will be taken into account
		/// when the property TerminalStateCollection is requested or GetMultiTerminalStateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalStateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalStateCollection.SortClauses=sortClauses;
			_terminalStateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTimestampCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_timestampCollection);
				_timestampCollection.SuppressClearInGetMulti=!forceFetch;
				_timestampCollection.EntityFactoryToUse = entityFactoryToUse;
				_timestampCollection.GetMultiManyToOne(this, null, null, null, filter);
				_timestampCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTimestampCollection = true;
			}
			return _timestampCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TimestampCollection'. These settings will be taken into account
		/// when the property TimestampCollection is requested or GetMultiTimestampCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTimestampCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_timestampCollection.SortClauses=sortClauses;
			_timestampCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup(forceFetch, _announcementCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
			}
			return _announcementCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiClientCollectionViaNetmessage(forceFetch, _clientCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaNetmessage || forceFetch || _alwaysFetchClientCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_clientCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("ClientCollectionViaNetmessage"));
				_clientCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaNetmessage = true;
			}
			return _clientCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaNetmessage'. These settings will be taken into account
		/// when the property ClientCollectionViaNetmessage is requested or GetMultiClientCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaNetmessage.SortClauses=sortClauses;
			_clientCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup(forceFetch, _companyCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
			}
			return _companyCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup is requested or GetMultiCompanyCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage(forceFetch, _companyCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage"));
				_companyCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage = true;
			}
			return _companyCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage is requested or GetMultiCompanyCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage.SortClauses=sortClauses;
			_companyCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage_(forceFetch, _companyCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage_ || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage_"));
				_companyCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage_ = true;
			}
			return _companyCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage_ is requested or GetMultiCompanyCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage_.SortClauses=sortClauses;
			_companyCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage__(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage__(forceFetch, _companyCollectionViaNetmessage__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage__ || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaNetmessage__.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage__.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage__.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage__"));
				_companyCollectionViaNetmessage__.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage__ = true;
			}
			return _companyCollectionViaNetmessage__;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage__'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage__ is requested or GetMultiCompanyCollectionViaNetmessage__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage__.SortClauses=sortClauses;
			_companyCollectionViaNetmessage__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage___(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage___(forceFetch, _companyCollectionViaNetmessage___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage___ || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaNetmessage___.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage___.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage___.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage___"));
				_companyCollectionViaNetmessage___.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage___ = true;
			}
			return _companyCollectionViaNetmessage___;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage___'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage___ is requested or GetMultiCompanyCollectionViaNetmessage___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage___.SortClauses=sortClauses;
			_companyCollectionViaNetmessage___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaTerminal(forceFetch, _companyCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaTerminal || forceFetch || _alwaysFetchCompanyCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaTerminal.GetMulti(filter, GetRelationsForField("CompanyCollectionViaTerminal"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaTerminal = true;
			}
			return _companyCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaTerminal'. These settings will be taken into account
		/// when the property CompanyCollectionViaTerminal is requested or GetMultiCompanyCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaTerminal.SortClauses=sortClauses;
			_companyCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage(forceFetch, _customerCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_customerCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage"));
				_customerCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage = true;
			}
			return _customerCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage is requested or GetMultiCustomerCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage.SortClauses=sortClauses;
			_customerCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage_(forceFetch, _customerCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage_ || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_customerCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage_"));
				_customerCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage_ = true;
			}
			return _customerCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage_ is requested or GetMultiCustomerCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage_.SortClauses=sortClauses;
			_customerCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage__(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage__(forceFetch, _customerCollectionViaNetmessage__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage__ || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_customerCollectionViaNetmessage__.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage__.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage__.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage__"));
				_customerCollectionViaNetmessage__.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage__ = true;
			}
			return _customerCollectionViaNetmessage__;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage__'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage__ is requested or GetMultiCustomerCollectionViaNetmessage__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage__.SortClauses=sortClauses;
			_customerCollectionViaNetmessage__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage___(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage___(forceFetch, _customerCollectionViaNetmessage___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage___ || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_customerCollectionViaNetmessage___.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage___.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage___.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage___"));
				_customerCollectionViaNetmessage___.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage___ = true;
			}
			return _customerCollectionViaNetmessage___;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage___'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage___ is requested or GetMultiCustomerCollectionViaNetmessage___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage___.SortClauses=sortClauses;
			_customerCollectionViaNetmessage___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage(forceFetch, _deliverypointCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage"));
				_deliverypointCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage = true;
			}
			return _deliverypointCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage is requested or GetMultiDeliverypointCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage_(forceFetch, _deliverypointCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage_ || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage_"));
				_deliverypointCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage_ = true;
			}
			return _deliverypointCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage_ is requested or GetMultiDeliverypointCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage_.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage__(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage__(forceFetch, _deliverypointCollectionViaNetmessage__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage__ || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaNetmessage__.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage__.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage__.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage__"));
				_deliverypointCollectionViaNetmessage__.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage__ = true;
			}
			return _deliverypointCollectionViaNetmessage__;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage__'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage__ is requested or GetMultiDeliverypointCollectionViaNetmessage__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage__.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage___(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage___(forceFetch, _deliverypointCollectionViaNetmessage___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage___ || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaNetmessage___.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage___.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage___.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage___"));
				_deliverypointCollectionViaNetmessage___.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage___ = true;
			}
			return _deliverypointCollectionViaNetmessage___;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage___'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage___ is requested or GetMultiDeliverypointCollectionViaNetmessage___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage___.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal(forceFetch, _deliverypointCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal = true;
			}
			return _deliverypointCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal is requested or GetMultiDeliverypointCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal_(forceFetch, _deliverypointCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal_"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
			}
			return _deliverypointCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal_ is requested or GetMultiDeliverypointCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage(forceFetch, _deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
			}
			return _deliverypointgroupCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage is requested or GetMultiDeliverypointgroupCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage_(forceFetch, _deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
			}
			return _deliverypointgroupCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage_ is requested or GetMultiDeliverypointgroupCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal(forceFetch, _deliverypointgroupCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
			}
			return _deliverypointgroupCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal is requested or GetMultiDeliverypointgroupCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal(forceFetch, _deviceCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal || forceFetch || _alwaysFetchDeviceCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal = true;
			}
			return _deviceCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal is requested or GetMultiDeviceCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal.SortClauses=sortClauses;
			_deviceCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollectionViaTerminal(forceFetch, _icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal || forceFetch || _alwaysFetchIcrtouchprintermappingCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollectionViaTerminal.GetMulti(filter, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
			}
			return _icrtouchprintermappingCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollectionViaTerminal'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollectionViaTerminal is requested or GetMultiIcrtouchprintermappingCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollectionViaTerminal.SortClauses=sortClauses;
			_icrtouchprintermappingCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup(forceFetch, _menuCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
			}
			return _menuCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup is requested or GetMultiMenuCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandler(forceFetch, _orderCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_orderCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandler"));
				_orderCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler = true;
			}
			return _orderCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandler is requested or GetMultiOrderCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler_(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandler_(forceFetch, _orderCollectionViaOrderRoutestephandler_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandler_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandler_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_orderCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandler_.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandler_.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandler_"));
				_orderCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ = true;
			}
			return _orderCollectionViaOrderRoutestephandler_;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandler_'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandler_ is requested or GetMultiOrderCollectionViaOrderRoutestephandler_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandler_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandler_.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandler_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandlerHistory(forceFetch, _orderCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_orderCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory"));
				_orderCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _orderCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandlerHistory is requested or GetMultiOrderCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory_(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandlerHistory_(forceFetch, _orderCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandlerHistory_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_orderCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandlerHistory_.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory_"));
				_orderCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ = true;
			}
			return _orderCollectionViaOrderRoutestephandlerHistory_;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandlerHistory_'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandlerHistory_ is requested or GetMultiOrderCollectionViaOrderRoutestephandlerHistory_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandlerHistory_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandlerHistory_.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandlerHistory_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal(forceFetch, _productCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal || forceFetch || _alwaysFetchProductCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal = true;
			}
			return _productCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal is requested or GetMultiProductCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal.SortClauses=sortClauses;
			_productCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal_(forceFetch, _productCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal_ || forceFetch || _alwaysFetchProductCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal_"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal_ = true;
			}
			return _productCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal_'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal_ is requested or GetMultiProductCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal_.SortClauses=sortClauses;
			_productCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal__(forceFetch, _productCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal__ || forceFetch || _alwaysFetchProductCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal__ = true;
			}
			return _productCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal__'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal__ is requested or GetMultiProductCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal__.SortClauses=sortClauses;
			_productCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal___(forceFetch, _productCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal___ || forceFetch || _alwaysFetchProductCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal___"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal___ = true;
			}
			return _productCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal___'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal___ is requested or GetMultiProductCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal___.SortClauses=sortClauses;
			_productCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup(forceFetch, _routeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
			}
			return _routeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup is requested or GetMultiRouteCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_(forceFetch, _routeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
			}
			return _routeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_ is requested or GetMultiRouteCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestepEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiRoutestepCollectionViaRoutestephandler(forceFetch, _routestepCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRoutestepCollectionViaRoutestephandler || forceFetch || _alwaysFetchRoutestepCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestepCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_routestepCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_routestepCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_routestepCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("RoutestepCollectionViaRoutestephandler"));
				_routestepCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestepCollectionViaRoutestephandler = true;
			}
			return _routestepCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestepCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property RoutestepCollectionViaRoutestephandler is requested or GetMultiRoutestepCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestepCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestepCollectionViaRoutestephandler.SortClauses=sortClauses;
			_routestepCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandler(forceFetch, _supportpoolCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_supportpoolCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler"));
				_supportpoolCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandler is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler_(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandler_(forceFetch, _supportpoolCollectionViaOrderRoutestephandler_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandler_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_supportpoolCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandler_.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandler_.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler_"));
				_supportpoolCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandler_;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandler_'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandler_ is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandler_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandler_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandler_.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandler_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(forceFetch, _supportpoolCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandlerHistory is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_(forceFetch, _supportpoolCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandlerHistory_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandlerHistory_.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory_"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandlerHistory_;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandlerHistory_'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandlerHistory_ is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandlerHistory_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandlerHistory_.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandlerHistory_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaRoutestephandler(forceFetch, _supportpoolCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaRoutestephandler || forceFetch || _alwaysFetchSupportpoolCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_supportpoolCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaRoutestephandler"));
				_supportpoolCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaRoutestephandler = true;
			}
			return _supportpoolCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaRoutestephandler is requested or GetMultiSupportpoolCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaRoutestephandler.SortClauses=sortClauses;
			_supportpoolCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage(forceFetch, _terminalCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage = true;
			}
			return _terminalCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage is requested or GetMultiTerminalCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage_(forceFetch, _terminalCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage_ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage_"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
			}
			return _terminalCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage_ is requested or GetMultiTerminalCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage_.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler(forceFetch, _terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
			}
			return _terminalCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler is requested or GetMultiTerminalCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler_(forceFetch, _terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandler_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandler_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogFileEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogFileCollection GetMultiTerminalLogFileCollectionViaTerminalLog(bool forceFetch)
		{
			return GetMultiTerminalLogFileCollectionViaTerminalLog(forceFetch, _terminalLogFileCollectionViaTerminalLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogFileCollection GetMultiTerminalLogFileCollectionViaTerminalLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalLogFileCollectionViaTerminalLog || forceFetch || _alwaysFetchTerminalLogFileCollectionViaTerminalLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalLogFileCollectionViaTerminalLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_terminalLogFileCollectionViaTerminalLog.SuppressClearInGetMulti=!forceFetch;
				_terminalLogFileCollectionViaTerminalLog.EntityFactoryToUse = entityFactoryToUse;
				_terminalLogFileCollectionViaTerminalLog.GetMulti(filter, GetRelationsForField("TerminalLogFileCollectionViaTerminalLog"));
				_terminalLogFileCollectionViaTerminalLog.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = true;
			}
			return _terminalLogFileCollectionViaTerminalLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalLogFileCollectionViaTerminalLog'. These settings will be taken into account
		/// when the property TerminalLogFileCollectionViaTerminalLog is requested or GetMultiTerminalLogFileCollectionViaTerminalLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalLogFileCollectionViaTerminalLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalLogFileCollectionViaTerminalLog.SortClauses=sortClauses;
			_terminalLogFileCollectionViaTerminalLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_(forceFetch, _uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup__(forceFetch, _uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup__ is requested or GetMultiUIModeCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal(forceFetch, _uIModeCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal || forceFetch || _alwaysFetchUIModeCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal = true;
			}
			return _uIModeCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal is requested or GetMultiUIModeCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup(forceFetch, _uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
			}
			return _uIModeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup is requested or GetMultiUIModeCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiAutomaticSignOnUserCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiAutomaticSignOnUserCollectionViaTerminal(forceFetch, _automaticSignOnUserCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiAutomaticSignOnUserCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal || forceFetch || _alwaysFetchAutomaticSignOnUserCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_automaticSignOnUserCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, this.TerminalId, "TerminalEntity__"));
				_automaticSignOnUserCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_automaticSignOnUserCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_automaticSignOnUserCollectionViaTerminal.GetMulti(filter, GetRelationsForField("AutomaticSignOnUserCollectionViaTerminal"));
				_automaticSignOnUserCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal = true;
			}
			return _automaticSignOnUserCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutomaticSignOnUserCollectionViaTerminal'. These settings will be taken into account
		/// when the property AutomaticSignOnUserCollectionViaTerminal is requested or GetMultiAutomaticSignOnUserCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutomaticSignOnUserCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_automaticSignOnUserCollectionViaTerminal.SortClauses=sortClauses;
			_automaticSignOnUserCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleAltSystemMessagesDeliverypointEntity()
		{
			return GetSingleAltSystemMessagesDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleAltSystemMessagesDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAltSystemMessagesDeliverypointEntity || forceFetch || _alwaysFetchAltSystemMessagesDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AltSystemMessagesDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_altSystemMessagesDeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AltSystemMessagesDeliverypointEntity = newEntity;
				_alreadyFetchedAltSystemMessagesDeliverypointEntity = fetchResult;
			}
			return _altSystemMessagesDeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleSystemMessagesDeliverypointEntity()
		{
			return GetSingleSystemMessagesDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleSystemMessagesDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSystemMessagesDeliverypointEntity || forceFetch || _alwaysFetchSystemMessagesDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SystemMessagesDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_systemMessagesDeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SystemMessagesDeliverypointEntity = newEntity;
				_alreadyFetchedSystemMessagesDeliverypointEntity = fetchResult;
			}
			return _systemMessagesDeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public DeviceEntity GetSingleDeviceEntity()
		{
			return GetSingleDeviceEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public virtual DeviceEntity GetSingleDeviceEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceEntity || forceFetch || _alwaysFetchDeviceEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceEntityUsingDeviceId);
				DeviceEntity newEntity = new DeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceEntity = newEntity;
				_alreadyFetchedDeviceEntity = fetchResult;
			}
			return _deviceEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleBrowser1EntertainmentEntity()
		{
			return GetSingleBrowser1EntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleBrowser1EntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrowser1EntertainmentEntity || forceFetch || _alwaysFetchBrowser1EntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingBrowser1);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Browser1.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_browser1EntertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Browser1EntertainmentEntity = newEntity;
				_alreadyFetchedBrowser1EntertainmentEntity = fetchResult;
			}
			return _browser1EntertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleBrowser2EntertainmentEntity()
		{
			return GetSingleBrowser2EntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleBrowser2EntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrowser2EntertainmentEntity || forceFetch || _alwaysFetchBrowser2EntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingBrowser2);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Browser2.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_browser2EntertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Browser2EntertainmentEntity = newEntity;
				_alreadyFetchedBrowser2EntertainmentEntity = fetchResult;
			}
			return _browser2EntertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleCmsPageEntertainmentEntity()
		{
			return GetSingleCmsPageEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleCmsPageEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCmsPageEntertainmentEntity || forceFetch || _alwaysFetchCmsPageEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingCmsPage);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CmsPage.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cmsPageEntertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CmsPageEntertainmentEntity = newEntity;
				_alreadyFetchedCmsPageEntertainmentEntity = fetchResult;
			}
			return _cmsPageEntertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'IcrtouchprintermappingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'IcrtouchprintermappingEntity' which is related to this entity.</returns>
		public IcrtouchprintermappingEntity GetSingleIcrtouchprintermappingEntity()
		{
			return GetSingleIcrtouchprintermappingEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'IcrtouchprintermappingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'IcrtouchprintermappingEntity' which is related to this entity.</returns>
		public virtual IcrtouchprintermappingEntity GetSingleIcrtouchprintermappingEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedIcrtouchprintermappingEntity || forceFetch || _alwaysFetchIcrtouchprintermappingEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
				IcrtouchprintermappingEntity newEntity = new IcrtouchprintermappingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IcrtouchprintermappingId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (IcrtouchprintermappingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_icrtouchprintermappingEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IcrtouchprintermappingEntity = newEntity;
				_alreadyFetchedIcrtouchprintermappingEntity = fetchResult;
			}
			return _icrtouchprintermappingEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleBatteryLowProductEntity()
		{
			return GetSingleBatteryLowProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleBatteryLowProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBatteryLowProductEntity || forceFetch || _alwaysFetchBatteryLowProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingBatteryLowProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BatteryLowProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_batteryLowProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BatteryLowProductEntity = newEntity;
				_alreadyFetchedBatteryLowProductEntity = fetchResult;
			}
			return _batteryLowProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleClientDisconnectedProductEntity()
		{
			return GetSingleClientDisconnectedProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleClientDisconnectedProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientDisconnectedProductEntity || forceFetch || _alwaysFetchClientDisconnectedProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingClientDisconnectedProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientDisconnectedProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientDisconnectedProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientDisconnectedProductEntity = newEntity;
				_alreadyFetchedClientDisconnectedProductEntity = fetchResult;
			}
			return _clientDisconnectedProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingOrderFailedProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderFailedProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleUnlockDeliverypointProductEntity()
		{
			return GetSingleUnlockDeliverypointProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleUnlockDeliverypointProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnlockDeliverypointProductEntity || forceFetch || _alwaysFetchUnlockDeliverypointProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingUnlockDeliverypointProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnlockDeliverypointProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unlockDeliverypointProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UnlockDeliverypointProductEntity = newEntity;
				_alreadyFetchedUnlockDeliverypointProductEntity = fetchResult;
			}
			return _unlockDeliverypointProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleForwardToTerminalEntity()
		{
			return GetSingleForwardToTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleForwardToTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedForwardToTerminalEntity || forceFetch || _alwaysFetchForwardToTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingTerminalIdForwardToTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ForwardToTerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_forwardToTerminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ForwardToTerminalEntity = newEntity;
				_alreadyFetchedForwardToTerminalEntity = fetchResult;
			}
			return _forwardToTerminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalConfigurationEntity' which is related to this entity.</returns>
		public TerminalConfigurationEntity GetSingleTerminalConfigurationEntity()
		{
			return GetSingleTerminalConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalConfigurationEntity' which is related to this entity.</returns>
		public virtual TerminalConfigurationEntity GetSingleTerminalConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalConfigurationEntity || forceFetch || _alwaysFetchTerminalConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalConfigurationEntityUsingTerminalConfigurationId);
				TerminalConfigurationEntity newEntity = new TerminalConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalConfigurationEntity = newEntity;
				_alreadyFetchedTerminalConfigurationEntity = fetchResult;
			}
			return _terminalConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleUIModeEntity()
		{
			return GetSingleUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIModeEntity || forceFetch || _alwaysFetchUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIModeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIModeEntity = newEntity;
				_alreadyFetchedUIModeEntity = fetchResult;
			}
			return _uIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public UserEntity GetSingleAutomaticSignOnUserEntity()
		{
			return GetSingleAutomaticSignOnUserEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UserEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserEntity' which is related to this entity.</returns>
		public virtual UserEntity GetSingleAutomaticSignOnUserEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutomaticSignOnUserEntity || forceFetch || _alwaysFetchAutomaticSignOnUserEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserEntityUsingAutomaticSignOnUserId);
				UserEntity newEntity = new UserEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutomaticSignOnUserId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_automaticSignOnUserEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AutomaticSignOnUserEntity = newEntity;
				_alreadyFetchedAutomaticSignOnUserEntity = fetchResult;
			}
			return _automaticSignOnUserEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("AltSystemMessagesDeliverypointEntity", _altSystemMessagesDeliverypointEntity);
			toReturn.Add("SystemMessagesDeliverypointEntity", _systemMessagesDeliverypointEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("DeviceEntity", _deviceEntity);
			toReturn.Add("Browser1EntertainmentEntity", _browser1EntertainmentEntity);
			toReturn.Add("Browser2EntertainmentEntity", _browser2EntertainmentEntity);
			toReturn.Add("CmsPageEntertainmentEntity", _cmsPageEntertainmentEntity);
			toReturn.Add("IcrtouchprintermappingEntity", _icrtouchprintermappingEntity);
			toReturn.Add("BatteryLowProductEntity", _batteryLowProductEntity);
			toReturn.Add("ClientDisconnectedProductEntity", _clientDisconnectedProductEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("UnlockDeliverypointProductEntity", _unlockDeliverypointProductEntity);
			toReturn.Add("ForwardToTerminalEntity", _forwardToTerminalEntity);
			toReturn.Add("TerminalConfigurationEntity", _terminalConfigurationEntity);
			toReturn.Add("UIModeEntity", _uIModeEntity);
			toReturn.Add("AutomaticSignOnUserEntity", _automaticSignOnUserEntity);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("IcrtouchprintermappingCollection", _icrtouchprintermappingCollection);
			toReturn.Add("ReceivedNetmessageCollection", _receivedNetmessageCollection);
			toReturn.Add("SentNetmessageCollection", _sentNetmessageCollection);
			toReturn.Add("OrderRoutestephandlerCollection_", _orderRoutestephandlerCollection_);
			toReturn.Add("OrderRoutestephandlerCollection", _orderRoutestephandlerCollection);
			toReturn.Add("OrderRoutestephandlerHistoryCollection_", _orderRoutestephandlerHistoryCollection_);
			toReturn.Add("OrderRoutestephandlerHistoryCollection", _orderRoutestephandlerHistoryCollection);
			toReturn.Add("RoutestephandlerCollection", _routestephandlerCollection);
			toReturn.Add("ScheduledCommandCollection", _scheduledCommandCollection);
			toReturn.Add("ForwardedFromTerminalCollection", _forwardedFromTerminalCollection);
			toReturn.Add("TerminalConfigurationCollection", _terminalConfigurationCollection);
			toReturn.Add("TerminalLogCollection", _terminalLogCollection);
			toReturn.Add("TerminalMessageTemplateCategoryCollection", _terminalMessageTemplateCategoryCollection);
			toReturn.Add("TerminalStateCollection", _terminalStateCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup", _announcementCollectionViaDeliverypointgroup);
			toReturn.Add("ClientCollectionViaNetmessage", _clientCollectionViaNetmessage);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup", _companyCollectionViaDeliverypointgroup);
			toReturn.Add("CompanyCollectionViaNetmessage", _companyCollectionViaNetmessage);
			toReturn.Add("CompanyCollectionViaNetmessage_", _companyCollectionViaNetmessage_);
			toReturn.Add("CompanyCollectionViaNetmessage__", _companyCollectionViaNetmessage__);
			toReturn.Add("CompanyCollectionViaNetmessage___", _companyCollectionViaNetmessage___);
			toReturn.Add("CompanyCollectionViaTerminal", _companyCollectionViaTerminal);
			toReturn.Add("CustomerCollectionViaNetmessage", _customerCollectionViaNetmessage);
			toReturn.Add("CustomerCollectionViaNetmessage_", _customerCollectionViaNetmessage_);
			toReturn.Add("CustomerCollectionViaNetmessage__", _customerCollectionViaNetmessage__);
			toReturn.Add("CustomerCollectionViaNetmessage___", _customerCollectionViaNetmessage___);
			toReturn.Add("DeliverypointCollectionViaNetmessage", _deliverypointCollectionViaNetmessage);
			toReturn.Add("DeliverypointCollectionViaNetmessage_", _deliverypointCollectionViaNetmessage_);
			toReturn.Add("DeliverypointCollectionViaNetmessage__", _deliverypointCollectionViaNetmessage__);
			toReturn.Add("DeliverypointCollectionViaNetmessage___", _deliverypointCollectionViaNetmessage___);
			toReturn.Add("DeliverypointCollectionViaTerminal", _deliverypointCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal_", _deliverypointCollectionViaTerminal_);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage", _deliverypointgroupCollectionViaNetmessage);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage_", _deliverypointgroupCollectionViaNetmessage_);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal", _deliverypointgroupCollectionViaTerminal);
			toReturn.Add("DeviceCollectionViaTerminal", _deviceCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("IcrtouchprintermappingCollectionViaTerminal", _icrtouchprintermappingCollectionViaTerminal);
			toReturn.Add("MenuCollectionViaDeliverypointgroup", _menuCollectionViaDeliverypointgroup);
			toReturn.Add("OrderCollectionViaOrderRoutestephandler", _orderCollectionViaOrderRoutestephandler);
			toReturn.Add("OrderCollectionViaOrderRoutestephandler_", _orderCollectionViaOrderRoutestephandler_);
			toReturn.Add("OrderCollectionViaOrderRoutestephandlerHistory", _orderCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("OrderCollectionViaOrderRoutestephandlerHistory_", _orderCollectionViaOrderRoutestephandlerHistory_);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup", _posdeliverypointgroupCollectionViaDeliverypointgroup);
			toReturn.Add("ProductCollectionViaTerminal", _productCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal_", _productCollectionViaTerminal_);
			toReturn.Add("ProductCollectionViaTerminal__", _productCollectionViaTerminal__);
			toReturn.Add("ProductCollectionViaTerminal___", _productCollectionViaTerminal___);
			toReturn.Add("RouteCollectionViaDeliverypointgroup", _routeCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_", _routeCollectionViaDeliverypointgroup_);
			toReturn.Add("RoutestepCollectionViaRoutestephandler", _routestepCollectionViaRoutestephandler);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandler", _supportpoolCollectionViaOrderRoutestephandler);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandler_", _supportpoolCollectionViaOrderRoutestephandler_);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandlerHistory", _supportpoolCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandlerHistory_", _supportpoolCollectionViaOrderRoutestephandlerHistory_);
			toReturn.Add("SupportpoolCollectionViaRoutestephandler", _supportpoolCollectionViaRoutestephandler);
			toReturn.Add("TerminalCollectionViaNetmessage", _terminalCollectionViaNetmessage);
			toReturn.Add("TerminalCollectionViaNetmessage_", _terminalCollectionViaNetmessage_);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler", _terminalCollectionViaOrderRoutestephandler);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler_", _terminalCollectionViaOrderRoutestephandler_);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory", _terminalCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory_", _terminalCollectionViaOrderRoutestephandlerHistory_);
			toReturn.Add("TerminalLogFileCollectionViaTerminalLog", _terminalLogFileCollectionViaTerminalLog);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_", _uIModeCollectionViaDeliverypointgroup_);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup__", _uIModeCollectionViaDeliverypointgroup__);
			toReturn.Add("UIModeCollectionViaTerminal", _uIModeCollectionViaTerminal);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup", _uIModeCollectionViaDeliverypointgroup);
			toReturn.Add("AutomaticSignOnUserCollectionViaTerminal", _automaticSignOnUserCollectionViaTerminal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="validator">The validator object for this TerminalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 terminalId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(terminalId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_icrtouchprintermappingCollection = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_icrtouchprintermappingCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_receivedNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_receivedNetmessageCollection.SetContainingEntityInfo(this, "ReceiverTerminalEntity");

			_sentNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_sentNetmessageCollection.SetContainingEntityInfo(this, "SenderTerminalEntity");

			_orderRoutestephandlerCollection_ = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection();
			_orderRoutestephandlerCollection_.SetContainingEntityInfo(this, "ForwardedFromTerminalEntity");

			_orderRoutestephandlerCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection();
			_orderRoutestephandlerCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_orderRoutestephandlerHistoryCollection_ = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection();
			_orderRoutestephandlerHistoryCollection_.SetContainingEntityInfo(this, "ForwardedFromTerminalEntity");

			_orderRoutestephandlerHistoryCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection();
			_orderRoutestephandlerHistoryCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_routestephandlerCollection = new Obymobi.Data.CollectionClasses.RoutestephandlerCollection();
			_routestephandlerCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_scheduledCommandCollection = new Obymobi.Data.CollectionClasses.ScheduledCommandCollection();
			_scheduledCommandCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_forwardedFromTerminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_forwardedFromTerminalCollection.SetContainingEntityInfo(this, "ForwardToTerminalEntity");

			_terminalConfigurationCollection = new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection();
			_terminalConfigurationCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_terminalLogCollection = new Obymobi.Data.CollectionClasses.TerminalLogCollection();
			_terminalLogCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_terminalMessageTemplateCategoryCollection = new Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection();
			_terminalMessageTemplateCategoryCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_terminalStateCollection = new Obymobi.Data.CollectionClasses.TerminalStateCollection();
			_terminalStateCollection.SetContainingEntityInfo(this, "TerminalEntity");

			_timestampCollection = new Obymobi.Data.CollectionClasses.TimestampCollection();
			_timestampCollection.SetContainingEntityInfo(this, "TerminalEntity");
			_announcementCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_clientCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage__ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage___ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaTerminal = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_customerCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_customerCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_customerCollectionViaNetmessage__ = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_customerCollectionViaNetmessage___ = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaNetmessage__ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaNetmessage___ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deviceCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_icrtouchprintermappingCollectionViaTerminal = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_menuCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.MenuCollection();
			_orderCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrderRoutestephandler_ = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrderRoutestephandlerHistory_ = new Obymobi.Data.CollectionClasses.OrderCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_productCollectionViaTerminal = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routestepCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.RoutestepCollection();
			_supportpoolCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_supportpoolCollectionViaOrderRoutestephandler_ = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_supportpoolCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_supportpoolCollectionViaOrderRoutestephandlerHistory_ = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_supportpoolCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_terminalCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandler_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalLogFileCollectionViaTerminalLog = new Obymobi.Data.CollectionClasses.TerminalLogFileCollection();
			_uIModeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_automaticSignOnUserCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UserCollection();
			_companyEntityReturnsNewIfNotFound = true;
			_altSystemMessagesDeliverypointEntityReturnsNewIfNotFound = true;
			_systemMessagesDeliverypointEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_deviceEntityReturnsNewIfNotFound = true;
			_browser1EntertainmentEntityReturnsNewIfNotFound = true;
			_browser2EntertainmentEntityReturnsNewIfNotFound = true;
			_cmsPageEntertainmentEntityReturnsNewIfNotFound = true;
			_icrtouchprintermappingEntityReturnsNewIfNotFound = true;
			_batteryLowProductEntityReturnsNewIfNotFound = true;
			_clientDisconnectedProductEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_unlockDeliverypointProductEntityReturnsNewIfNotFound = true;
			_forwardToTerminalEntityReturnsNewIfNotFound = true;
			_terminalConfigurationEntityReturnsNewIfNotFound = true;
			_uIModeEntityReturnsNewIfNotFound = true;
			_automaticSignOnUserEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStatusMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptTypes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiagnoseInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrinterName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecordAllRequestsToRequestLog", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TeamviewerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxDaysLogHistory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("POSConnectorType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PMSConnectorType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronizeWithPOSOnStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxStatusReports", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnlockDeliverypointProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BatteryLowProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientDisconnectedProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderFailedProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemMessagesDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AltSystemMessagesDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IcrtouchprintermappingId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperationState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HandlingMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationForNewOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationForOverdueOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxProcessTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue11", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue12", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue13", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue14", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue15", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue16", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue17", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue18", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue19", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosValue20", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnnouncementDuration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForwardToTerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnsiteServerDeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeNotificationsSent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Browser1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Browser2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CmsPage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomaticSignOnUserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyResultNotifications", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintingEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStateOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStateOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseMonitoring", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterTab", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResetTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrinterConnected", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseHardKeyboard", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxVolume", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OfflineNotificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadedSuccessfully", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeNotificationLastSentUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusUpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronizeWithPMSOnStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastSyncUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsTerminalOfflineNotificationEnabled", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _altSystemMessagesDeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAltSystemMessagesDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _altSystemMessagesDeliverypointEntity, new PropertyChangedEventHandler( OnAltSystemMessagesDeliverypointEntityPropertyChanged ), "AltSystemMessagesDeliverypointEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingAltSystemMessagesDeliverypointIdStatic, true, signalRelatedEntity, "TerminalCollection_", resetFKFields, new int[] { (int)TerminalFieldIndex.AltSystemMessagesDeliverypointId } );		
			_altSystemMessagesDeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _altSystemMessagesDeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAltSystemMessagesDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_altSystemMessagesDeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncAltSystemMessagesDeliverypointEntity(true, true);
				_altSystemMessagesDeliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _altSystemMessagesDeliverypointEntity, new PropertyChangedEventHandler( OnAltSystemMessagesDeliverypointEntityPropertyChanged ), "AltSystemMessagesDeliverypointEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingAltSystemMessagesDeliverypointIdStatic, true, ref _alreadyFetchedAltSystemMessagesDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAltSystemMessagesDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _systemMessagesDeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSystemMessagesDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _systemMessagesDeliverypointEntity, new PropertyChangedEventHandler( OnSystemMessagesDeliverypointEntityPropertyChanged ), "SystemMessagesDeliverypointEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingSystemMessagesDeliverypointIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.SystemMessagesDeliverypointId } );		
			_systemMessagesDeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _systemMessagesDeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSystemMessagesDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_systemMessagesDeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncSystemMessagesDeliverypointEntity(true, true);
				_systemMessagesDeliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _systemMessagesDeliverypointEntity, new PropertyChangedEventHandler( OnSystemMessagesDeliverypointEntityPropertyChanged ), "SystemMessagesDeliverypointEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointEntityUsingSystemMessagesDeliverypointIdStatic, true, ref _alreadyFetchedSystemMessagesDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSystemMessagesDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeviceEntityUsingDeviceIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.DeviceId } );		
			_deviceEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deviceEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceEntity(IEntityCore relatedEntity)
		{
			if(_deviceEntity!=relatedEntity)
			{		
				DesetupSyncDeviceEntity(true, true);
				_deviceEntity = (DeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.DeviceEntityUsingDeviceIdStatic, true, ref _alreadyFetchedDeviceEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _browser1EntertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrowser1EntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _browser1EntertainmentEntity, new PropertyChangedEventHandler( OnBrowser1EntertainmentEntityPropertyChanged ), "Browser1EntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingBrowser1Static, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.Browser1 } );		
			_browser1EntertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _browser1EntertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrowser1EntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_browser1EntertainmentEntity!=relatedEntity)
			{		
				DesetupSyncBrowser1EntertainmentEntity(true, true);
				_browser1EntertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _browser1EntertainmentEntity, new PropertyChangedEventHandler( OnBrowser1EntertainmentEntityPropertyChanged ), "Browser1EntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingBrowser1Static, true, ref _alreadyFetchedBrowser1EntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrowser1EntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _browser2EntertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrowser2EntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _browser2EntertainmentEntity, new PropertyChangedEventHandler( OnBrowser2EntertainmentEntityPropertyChanged ), "Browser2EntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingBrowser2Static, true, signalRelatedEntity, "TerminalCollection_", resetFKFields, new int[] { (int)TerminalFieldIndex.Browser2 } );		
			_browser2EntertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _browser2EntertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrowser2EntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_browser2EntertainmentEntity!=relatedEntity)
			{		
				DesetupSyncBrowser2EntertainmentEntity(true, true);
				_browser2EntertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _browser2EntertainmentEntity, new PropertyChangedEventHandler( OnBrowser2EntertainmentEntityPropertyChanged ), "Browser2EntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingBrowser2Static, true, ref _alreadyFetchedBrowser2EntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrowser2EntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cmsPageEntertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCmsPageEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cmsPageEntertainmentEntity, new PropertyChangedEventHandler( OnCmsPageEntertainmentEntityPropertyChanged ), "CmsPageEntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingCmsPageStatic, true, signalRelatedEntity, "TerminalCollection__", resetFKFields, new int[] { (int)TerminalFieldIndex.CmsPage } );		
			_cmsPageEntertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _cmsPageEntertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCmsPageEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_cmsPageEntertainmentEntity!=relatedEntity)
			{		
				DesetupSyncCmsPageEntertainmentEntity(true, true);
				_cmsPageEntertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cmsPageEntertainmentEntity, new PropertyChangedEventHandler( OnCmsPageEntertainmentEntityPropertyChanged ), "CmsPageEntertainmentEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.EntertainmentEntityUsingCmsPageStatic, true, ref _alreadyFetchedCmsPageEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCmsPageEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _icrtouchprintermappingEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIcrtouchprintermappingEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _icrtouchprintermappingEntity, new PropertyChangedEventHandler( OnIcrtouchprintermappingEntityPropertyChanged ), "IcrtouchprintermappingEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.IcrtouchprintermappingId } );		
			_icrtouchprintermappingEntity = null;
		}
		
		/// <summary> setups the sync logic for member _icrtouchprintermappingEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIcrtouchprintermappingEntity(IEntityCore relatedEntity)
		{
			if(_icrtouchprintermappingEntity!=relatedEntity)
			{		
				DesetupSyncIcrtouchprintermappingEntity(true, true);
				_icrtouchprintermappingEntity = (IcrtouchprintermappingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _icrtouchprintermappingEntity, new PropertyChangedEventHandler( OnIcrtouchprintermappingEntityPropertyChanged ), "IcrtouchprintermappingEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic, true, ref _alreadyFetchedIcrtouchprintermappingEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIcrtouchprintermappingEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _batteryLowProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBatteryLowProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _batteryLowProductEntity, new PropertyChangedEventHandler( OnBatteryLowProductEntityPropertyChanged ), "BatteryLowProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingBatteryLowProductIdStatic, true, signalRelatedEntity, "TerminalCollection_", resetFKFields, new int[] { (int)TerminalFieldIndex.BatteryLowProductId } );		
			_batteryLowProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _batteryLowProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBatteryLowProductEntity(IEntityCore relatedEntity)
		{
			if(_batteryLowProductEntity!=relatedEntity)
			{		
				DesetupSyncBatteryLowProductEntity(true, true);
				_batteryLowProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _batteryLowProductEntity, new PropertyChangedEventHandler( OnBatteryLowProductEntityPropertyChanged ), "BatteryLowProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingBatteryLowProductIdStatic, true, ref _alreadyFetchedBatteryLowProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBatteryLowProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientDisconnectedProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientDisconnectedProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientDisconnectedProductEntity, new PropertyChangedEventHandler( OnClientDisconnectedProductEntityPropertyChanged ), "ClientDisconnectedProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingClientDisconnectedProductIdStatic, true, signalRelatedEntity, "TerminalCollection__", resetFKFields, new int[] { (int)TerminalFieldIndex.ClientDisconnectedProductId } );		
			_clientDisconnectedProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientDisconnectedProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientDisconnectedProductEntity(IEntityCore relatedEntity)
		{
			if(_clientDisconnectedProductEntity!=relatedEntity)
			{		
				DesetupSyncClientDisconnectedProductEntity(true, true);
				_clientDisconnectedProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientDisconnectedProductEntity, new PropertyChangedEventHandler( OnClientDisconnectedProductEntityPropertyChanged ), "ClientDisconnectedProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingClientDisconnectedProductIdStatic, true, ref _alreadyFetchedClientDisconnectedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientDisconnectedProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingOrderFailedProductIdStatic, true, signalRelatedEntity, "TerminalCollection___", resetFKFields, new int[] { (int)TerminalFieldIndex.OrderFailedProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingOrderFailedProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _unlockDeliverypointProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnlockDeliverypointProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unlockDeliverypointProductEntity, new PropertyChangedEventHandler( OnUnlockDeliverypointProductEntityPropertyChanged ), "UnlockDeliverypointProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingUnlockDeliverypointProductIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.UnlockDeliverypointProductId } );		
			_unlockDeliverypointProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _unlockDeliverypointProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnlockDeliverypointProductEntity(IEntityCore relatedEntity)
		{
			if(_unlockDeliverypointProductEntity!=relatedEntity)
			{		
				DesetupSyncUnlockDeliverypointProductEntity(true, true);
				_unlockDeliverypointProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unlockDeliverypointProductEntity, new PropertyChangedEventHandler( OnUnlockDeliverypointProductEntityPropertyChanged ), "UnlockDeliverypointProductEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.ProductEntityUsingUnlockDeliverypointProductIdStatic, true, ref _alreadyFetchedUnlockDeliverypointProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnlockDeliverypointProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _forwardToTerminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForwardToTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _forwardToTerminalEntity, new PropertyChangedEventHandler( OnForwardToTerminalEntityPropertyChanged ), "ForwardToTerminalEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.TerminalEntityUsingTerminalIdForwardToTerminalIdStatic, true, signalRelatedEntity, "ForwardedFromTerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.ForwardToTerminalId } );		
			_forwardToTerminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _forwardToTerminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForwardToTerminalEntity(IEntityCore relatedEntity)
		{
			if(_forwardToTerminalEntity!=relatedEntity)
			{		
				DesetupSyncForwardToTerminalEntity(true, true);
				_forwardToTerminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _forwardToTerminalEntity, new PropertyChangedEventHandler( OnForwardToTerminalEntityPropertyChanged ), "ForwardToTerminalEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.TerminalEntityUsingTerminalIdForwardToTerminalIdStatic, true, ref _alreadyFetchedForwardToTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnForwardToTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalConfigurationEntity, new PropertyChangedEventHandler( OnTerminalConfigurationEntityPropertyChanged ), "TerminalConfigurationEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.TerminalConfigurationEntityUsingTerminalConfigurationIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.TerminalConfigurationId } );		
			_terminalConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_terminalConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncTerminalConfigurationEntity(true, true);
				_terminalConfigurationEntity = (TerminalConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalConfigurationEntity, new PropertyChangedEventHandler( OnTerminalConfigurationEntityPropertyChanged ), "TerminalConfigurationEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.TerminalConfigurationEntityUsingTerminalConfigurationIdStatic, true, ref _alreadyFetchedTerminalConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.UIModeEntityUsingUIModeIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.UIModeId } );		
			_uIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIModeEntity(IEntityCore relatedEntity)
		{
			if(_uIModeEntity!=relatedEntity)
			{		
				DesetupSyncUIModeEntity(true, true);
				_uIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.UIModeEntityUsingUIModeIdStatic, true, ref _alreadyFetchedUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _automaticSignOnUserEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutomaticSignOnUserEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _automaticSignOnUserEntity, new PropertyChangedEventHandler( OnAutomaticSignOnUserEntityPropertyChanged ), "AutomaticSignOnUserEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.UserEntityUsingAutomaticSignOnUserIdStatic, true, signalRelatedEntity, "TerminalCollection", resetFKFields, new int[] { (int)TerminalFieldIndex.AutomaticSignOnUserId } );		
			_automaticSignOnUserEntity = null;
		}
		
		/// <summary> setups the sync logic for member _automaticSignOnUserEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutomaticSignOnUserEntity(IEntityCore relatedEntity)
		{
			if(_automaticSignOnUserEntity!=relatedEntity)
			{		
				DesetupSyncAutomaticSignOnUserEntity(true, true);
				_automaticSignOnUserEntity = (UserEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _automaticSignOnUserEntity, new PropertyChangedEventHandler( OnAutomaticSignOnUserEntityPropertyChanged ), "AutomaticSignOnUserEntity", Obymobi.Data.RelationClasses.StaticTerminalRelations.UserEntityUsingAutomaticSignOnUserIdStatic, true, ref _alreadyFetchedAutomaticSignOnUserEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutomaticSignOnUserEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 terminalId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TerminalFieldIndex.TerminalId].ForcedCurrentValueWrite(terminalId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTerminalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TerminalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TerminalRelations Relations
		{
			get	{ return new TerminalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), (IEntityRelation)GetRelationsForField("IcrtouchprintermappingCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, null, "IcrtouchprintermappingCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceivedNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("ReceivedNetmessageCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "ReceivedNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSentNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("SentNetmessageCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "SentNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerCollection_")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, 0, null, null, null, "OrderRoutestephandlerCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, 0, null, null, null, "OrderRoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerHistoryCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerHistoryCollection_")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity, 0, null, null, null, "OrderRoutestephandlerHistoryCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerHistoryCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity, 0, null, null, null, "OrderRoutestephandlerHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledCommand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledCommandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledCommandCollection(), (IEntityRelation)GetRelationsForField("ScheduledCommandCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ScheduledCommandEntity, 0, null, null, null, "ScheduledCommandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForwardedFromTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("ForwardedFromTerminalCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "ForwardedFromTerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection(), (IEntityRelation)GetRelationsForField("TerminalConfigurationCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalConfigurationEntity, 0, null, null, null, "TerminalConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogCollection(), (IEntityRelation)GetRelationsForField("TerminalLogCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalLogEntity, 0, null, null, null, "TerminalLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalMessageTemplateCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalMessageTemplateCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection(), (IEntityRelation)GetRelationsForField("TerminalMessageTemplateCategoryCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalMessageTemplateCategoryEntity, 0, null, null, null, "TerminalMessageTemplateCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalState' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalStateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalStateCollection(), (IEntityRelation)GetRelationsForField("TerminalStateCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalStateEntity, 0, null, null, null, "TerminalStateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"), "AnnouncementCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaNetmessage"), "ClientCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"), "CompanyCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage"), "CompanyCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage_"), "CompanyCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage__"), "CompanyCollectionViaNetmessage__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage___"), "CompanyCollectionViaNetmessage___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaTerminal"), "CompanyCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage"), "CustomerCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage_"), "CustomerCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage__"), "CustomerCollectionViaNetmessage__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage___"), "CustomerCollectionViaNetmessage___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage"), "DeliverypointCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage_"), "DeliverypointCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage__"), "DeliverypointCollectionViaNetmessage__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage___"), "DeliverypointCollectionViaNetmessage___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal"), "DeliverypointCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal_"), "DeliverypointCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"), "DeliverypointgroupCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"), "DeliverypointgroupCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"), "DeliverypointgroupCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal"), "DeviceCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"), "IcrtouchprintermappingCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup"), "MenuCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandler"), "OrderCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandler_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingForwardedFromTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandler_"), "OrderCollectionViaOrderRoutestephandler_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory"), "OrderCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandlerHistory_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory_"), "OrderCollectionViaOrderRoutestephandlerHistory_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"), "PosdeliverypointgroupCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal"), "ProductCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal_"), "ProductCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal__"), "ProductCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal___"), "ProductCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup"), "RouteCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"), "RouteCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestep'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestepCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestepCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.RoutestepEntity, 0, null, null, GetRelationsForField("RoutestepCollectionViaRoutestephandler"), "RoutestepCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingForwardedFromTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler"), "SupportpoolCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandler_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler_"), "SupportpoolCollectionViaOrderRoutestephandler_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory"), "SupportpoolCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandlerHistory_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory_"), "SupportpoolCollectionViaOrderRoutestephandlerHistory_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaRoutestephandler"), "SupportpoolCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage"), "TerminalCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage_"), "TerminalCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"), "TerminalCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"), "TerminalCollectionViaOrderRoutestephandler_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"), "TerminalCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"), "TerminalCollectionViaOrderRoutestephandlerHistory_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLogFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogFileCollectionViaTerminalLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalLogEntityUsingTerminalId;
				intermediateRelation.SetAliases(string.Empty, "TerminalLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogFileCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalLogFileEntity, 0, null, null, GetRelationsForField("TerminalLogFileCollectionViaTerminalLog"), "TerminalLogFileCollectionViaTerminalLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"), "UIModeCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"), "UIModeCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal"), "UIModeCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingXTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"), "UIModeCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomaticSignOnUserCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingForwardToTerminalId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("AutomaticSignOnUserCollectionViaTerminal"), "AutomaticSignOnUserCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAltSystemMessagesDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("AltSystemMessagesDeliverypointEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "AltSystemMessagesDeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemMessagesDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("SystemMessagesDeliverypointEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "SystemMessagesDeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("DeviceEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, null, "DeviceEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrowser1EntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("Browser1EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "Browser1EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrowser2EntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("Browser2EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "Browser2EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCmsPageEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("CmsPageEntertainmentEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "CmsPageEntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), (IEntityRelation)GetRelationsForField("IcrtouchprintermappingEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, null, "IcrtouchprintermappingEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBatteryLowProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("BatteryLowProductEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "BatteryLowProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientDisconnectedProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ClientDisconnectedProductEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ClientDisconnectedProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnlockDeliverypointProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("UnlockDeliverypointProductEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "UnlockDeliverypointProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForwardToTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("ForwardToTerminalEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "ForwardToTerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection(), (IEntityRelation)GetRelationsForField("TerminalConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.TerminalConfigurationEntity, 0, null, null, null, "TerminalConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomaticSignOnUserEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), (IEntityRelation)GetRelationsForField("AutomaticSignOnUserEntity")[0], (int)Obymobi.Data.EntityType.TerminalEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, null, "AutomaticSignOnUserEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The TerminalId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TerminalId
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.TerminalId, true); }
			set	{ SetValue((int)TerminalFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.CompanyId, true); }
			set	{ SetValue((int)TerminalFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.Name, true); }
			set	{ SetValue((int)TerminalFieldIndex.Name, value, true); }
		}

		/// <summary> The LastStatus property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastStatus
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.LastStatus, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatus, value, true); }
		}

		/// <summary> The LastStatusText property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusText
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LastStatusText, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatusText, value, true); }
		}

		/// <summary> The LastStatusMessage property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStatusMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusMessage
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LastStatusMessage, true); }
			set	{ SetValue((int)TerminalFieldIndex.LastStatusMessage, value, true); }
		}

		/// <summary> The ReceiptTypes property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ReceiptTypes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptTypes
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.ReceiptTypes, true); }
			set	{ SetValue((int)TerminalFieldIndex.ReceiptTypes, value, true); }
		}

		/// <summary> The RequestInterval property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."RequestInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RequestInterval
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.RequestInterval, true); }
			set	{ SetValue((int)TerminalFieldIndex.RequestInterval, value, true); }
		}

		/// <summary> The DiagnoseInterval property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DiagnoseInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DiagnoseInterval
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.DiagnoseInterval, true); }
			set	{ SetValue((int)TerminalFieldIndex.DiagnoseInterval, value, true); }
		}

		/// <summary> The PrinterName property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrinterName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrinterName
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PrinterName, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrinterName, value, true); }
		}

		/// <summary> The RecordAllRequestsToRequestLog property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."RecordAllRequestsToRequestLog"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RecordAllRequestsToRequestLog
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.RecordAllRequestsToRequestLog, true); }
			set	{ SetValue((int)TerminalFieldIndex.RecordAllRequestsToRequestLog, value, true); }
		}

		/// <summary> The TeamviewerId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."TeamviewerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TeamviewerId
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.TeamviewerId, true); }
			set	{ SetValue((int)TerminalFieldIndex.TeamviewerId, value, true); }
		}

		/// <summary> The MaxDaysLogHistory property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxDaysLogHistory"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxDaysLogHistory
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxDaysLogHistory, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxDaysLogHistory, value, true); }
		}

		/// <summary> The POSConnectorType property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."POSConnectorType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 POSConnectorType
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.POSConnectorType, true); }
			set	{ SetValue((int)TerminalFieldIndex.POSConnectorType, value, true); }
		}

		/// <summary> The PMSConnectorType property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PMSConnectorType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PMSConnectorType
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.PMSConnectorType, true); }
			set	{ SetValue((int)TerminalFieldIndex.PMSConnectorType, value, true); }
		}

		/// <summary> The SynchronizeWithPOSOnStart property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SynchronizeWithPOSOnStart"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SynchronizeWithPOSOnStart
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SynchronizeWithPOSOnStart, true); }
			set	{ SetValue((int)TerminalFieldIndex.SynchronizeWithPOSOnStart, value, true); }
		}

		/// <summary> The MaxStatusReports property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxStatusReports"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxStatusReports
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxStatusReports, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxStatusReports, value, true); }
		}

		/// <summary> The Active property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.Active, true); }
			set	{ SetValue((int)TerminalFieldIndex.Active, value, true); }
		}

		/// <summary> The LanguageCode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LanguageCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LanguageCode
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.LanguageCode, true); }
			set	{ SetValue((int)TerminalFieldIndex.LanguageCode, value, true); }
		}

		/// <summary> The UnlockDeliverypointProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UnlockDeliverypointProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UnlockDeliverypointProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.UnlockDeliverypointProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.UnlockDeliverypointProductId, value, true); }
		}

		/// <summary> The BatteryLowProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."BatteryLowProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BatteryLowProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.BatteryLowProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.BatteryLowProductId, value, true); }
		}

		/// <summary> The ClientDisconnectedProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ClientDisconnectedProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientDisconnectedProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.ClientDisconnectedProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.ClientDisconnectedProductId, value, true); }
		}

		/// <summary> The OrderFailedProductId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OrderFailedProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderFailedProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.OrderFailedProductId, false); }
			set	{ SetValue((int)TerminalFieldIndex.OrderFailedProductId, value, true); }
		}

		/// <summary> The SystemMessagesDeliverypointId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SystemMessagesDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SystemMessagesDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.SystemMessagesDeliverypointId, false); }
			set	{ SetValue((int)TerminalFieldIndex.SystemMessagesDeliverypointId, value, true); }
		}

		/// <summary> The AltSystemMessagesDeliverypointId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."AltSystemMessagesDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AltSystemMessagesDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.AltSystemMessagesDeliverypointId, false); }
			set	{ SetValue((int)TerminalFieldIndex.AltSystemMessagesDeliverypointId, value, true); }
		}

		/// <summary> The IcrtouchprintermappingId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."IcrtouchprintermappingId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> IcrtouchprintermappingId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.IcrtouchprintermappingId, false); }
			set	{ SetValue((int)TerminalFieldIndex.IcrtouchprintermappingId, value, true); }
		}

		/// <summary> The OperationMode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OperationMode
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.OperationMode, true); }
			set	{ SetValue((int)TerminalFieldIndex.OperationMode, value, true); }
		}

		/// <summary> The OperationState property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OperationState"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OperationState
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.OperationState, true); }
			set	{ SetValue((int)TerminalFieldIndex.OperationState, value, true); }
		}

		/// <summary> The HandlingMethod property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."HandlingMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HandlingMethod
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.HandlingMethod, true); }
			set	{ SetValue((int)TerminalFieldIndex.HandlingMethod, value, true); }
		}

		/// <summary> The NotificationForNewOrder property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."NotificationForNewOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NotificationForNewOrder
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.NotificationForNewOrder, true); }
			set	{ SetValue((int)TerminalFieldIndex.NotificationForNewOrder, value, true); }
		}

		/// <summary> The NotificationForOverdueOrder property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."NotificationForOverdueOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NotificationForOverdueOrder
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.NotificationForOverdueOrder, true); }
			set	{ SetValue((int)TerminalFieldIndex.NotificationForOverdueOrder, value, true); }
		}

		/// <summary> The MaxProcessTime property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxProcessTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxProcessTime
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxProcessTime, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxProcessTime, value, true); }
		}

		/// <summary> The PosValue1 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue1
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue1, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue1, value, true); }
		}

		/// <summary> The PosValue2 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue2
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue2, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue2, value, true); }
		}

		/// <summary> The PosValue3 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue3
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue3, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue3, value, true); }
		}

		/// <summary> The PosValue4 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue4
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue4, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue4, value, true); }
		}

		/// <summary> The PosValue5 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue5
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue5, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue5, value, true); }
		}

		/// <summary> The PosValue6 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue6
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue6, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue6, value, true); }
		}

		/// <summary> The PosValue7 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue7
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue7, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue7, value, true); }
		}

		/// <summary> The PosValue8 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue8
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue8, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue8, value, true); }
		}

		/// <summary> The PosValue9 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue9
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue9, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue9, value, true); }
		}

		/// <summary> The PosValue10 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue10
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue10, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue10, value, true); }
		}

		/// <summary> The PosValue11 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue11"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue11
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue11, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue11, value, true); }
		}

		/// <summary> The PosValue12 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue12"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue12
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue12, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue12, value, true); }
		}

		/// <summary> The PosValue13 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue13"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue13
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue13, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue13, value, true); }
		}

		/// <summary> The PosValue14 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue14"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue14
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue14, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue14, value, true); }
		}

		/// <summary> The PosValue15 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue15"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue15
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue15, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue15, value, true); }
		}

		/// <summary> The PosValue16 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue16"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue16
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue16, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue16, value, true); }
		}

		/// <summary> The PosValue17 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue17"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue17
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue17, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue17, value, true); }
		}

		/// <summary> The PosValue18 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue18"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue18
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue18, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue18, value, true); }
		}

		/// <summary> The PosValue19 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue19"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue19
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue19, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue19, value, true); }
		}

		/// <summary> The PosValue20 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PosValue20"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PosValue20
		{
			get { return (System.String)GetValue((int)TerminalFieldIndex.PosValue20, true); }
			set	{ SetValue((int)TerminalFieldIndex.PosValue20, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)TerminalFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)TerminalFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The AnnouncementDuration property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."AnnouncementDuration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AnnouncementDuration
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.AnnouncementDuration, true); }
			set	{ SetValue((int)TerminalFieldIndex.AnnouncementDuration, value, true); }
		}

		/// <summary> The ForwardToTerminalId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ForwardToTerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ForwardToTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.ForwardToTerminalId, false); }
			set	{ SetValue((int)TerminalFieldIndex.ForwardToTerminalId, value, true); }
		}

		/// <summary> The DeviceId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DeviceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.DeviceId, false); }
			set	{ SetValue((int)TerminalFieldIndex.DeviceId, value, true); }
		}

		/// <summary> The OnsiteServerDeliverypointgroupId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OnsiteServerDeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnsiteServerDeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.OnsiteServerDeliverypointgroupId, false); }
			set	{ SetValue((int)TerminalFieldIndex.OnsiteServerDeliverypointgroupId, value, true); }
		}

		/// <summary> The OutOfChargeNotificationsSent property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OutOfChargeNotificationsSent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationsSent
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.OutOfChargeNotificationsSent, true); }
			set	{ SetValue((int)TerminalFieldIndex.OutOfChargeNotificationsSent, value, true); }
		}

		/// <summary> The Browser1 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Browser1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Browser1
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.Browser1, false); }
			set	{ SetValue((int)TerminalFieldIndex.Browser1, value, true); }
		}

		/// <summary> The Browser2 property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Browser2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Browser2
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.Browser2, false); }
			set	{ SetValue((int)TerminalFieldIndex.Browser2, value, true); }
		}

		/// <summary> The CmsPage property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CmsPage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CmsPage
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.CmsPage, false); }
			set	{ SetValue((int)TerminalFieldIndex.CmsPage, value, true); }
		}

		/// <summary> The AutomaticSignOnUserId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."AutomaticSignOnUserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AutomaticSignOnUserId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.AutomaticSignOnUserId, false); }
			set	{ SetValue((int)TerminalFieldIndex.AutomaticSignOnUserId, value, true); }
		}

		/// <summary> The SurveyResultNotifications property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SurveyResultNotifications"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SurveyResultNotifications
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SurveyResultNotifications, true); }
			set	{ SetValue((int)TerminalFieldIndex.SurveyResultNotifications, value, true); }
		}

		/// <summary> The PrintingEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrintingEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrintingEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PrintingEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrintingEnabled, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)TerminalFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The LastStateOnline property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStateOnline"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> LastStateOnline
		{
			get { return (Nullable<System.Boolean>)GetValue((int)TerminalFieldIndex.LastStateOnline, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStateOnline, value, true); }
		}

		/// <summary> The LastStateOperationMode property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastStateOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastStateOperationMode
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.LastStateOperationMode, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastStateOperationMode, value, true); }
		}

		/// <summary> The UIModeId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.UIModeId, false); }
			set	{ SetValue((int)TerminalFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The UseMonitoring property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UseMonitoring"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseMonitoring
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.UseMonitoring, true); }
			set	{ SetValue((int)TerminalFieldIndex.UseMonitoring, value, true); }
		}

		/// <summary> The MasterTab property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MasterTab"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MasterTab
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.MasterTab, true); }
			set	{ SetValue((int)TerminalFieldIndex.MasterTab, value, true); }
		}

		/// <summary> The ResetTimeout property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."ResetTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)TerminalFieldIndex.ResetTimeout, value, true); }
		}

		/// <summary> The PrinterConnected property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PrinterConnected"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrinterConnected
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PrinterConnected, true); }
			set	{ SetValue((int)TerminalFieldIndex.PrinterConnected, value, true); }
		}

		/// <summary> The UseHardKeyboard property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UseHardKeyboard"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.HardwareKeyboardType UseHardKeyboard
		{
			get { return (Obymobi.Enums.HardwareKeyboardType)GetValue((int)TerminalFieldIndex.UseHardKeyboard, true); }
			set	{ SetValue((int)TerminalFieldIndex.UseHardKeyboard, value, true); }
		}

		/// <summary> The MaxVolume property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."MaxVolume"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxVolume
		{
			get { return (System.Int32)GetValue((int)TerminalFieldIndex.MaxVolume, true); }
			set	{ SetValue((int)TerminalFieldIndex.MaxVolume, value, true); }
		}

		/// <summary> The OfflineNotificationEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OfflineNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OfflineNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.OfflineNotificationEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.OfflineNotificationEnabled, value, true); }
		}

		/// <summary> The LoadedSuccessfully property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LoadedSuccessfully"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LoadedSuccessfully
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.LoadedSuccessfully, true); }
			set	{ SetValue((int)TerminalFieldIndex.LoadedSuccessfully, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The OutOfChargeNotificationLastSentUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."OutOfChargeNotificationLastSentUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> OutOfChargeNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.OutOfChargeNotificationLastSentUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.OutOfChargeNotificationLastSentUTC, value, true); }
		}

		/// <summary> The Type property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Type
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.Type, false); }
			set	{ SetValue((int)TerminalFieldIndex.Type, value, true); }
		}

		/// <summary> The StatusUpdatedUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."StatusUpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StatusUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.StatusUpdatedUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.StatusUpdatedUTC, value, true); }
		}

		/// <summary> The SynchronizeWithPMSOnStart property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."SynchronizeWithPMSOnStart"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SynchronizeWithPMSOnStart
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.SynchronizeWithPMSOnStart, true); }
			set	{ SetValue((int)TerminalFieldIndex.SynchronizeWithPMSOnStart, value, true); }
		}

		/// <summary> The TerminalConfigurationId property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."TerminalConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalFieldIndex.TerminalConfigurationId, false); }
			set	{ SetValue((int)TerminalFieldIndex.TerminalConfigurationId, value, true); }
		}

		/// <summary> The LastSyncUTC property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."LastSyncUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastSyncUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalFieldIndex.LastSyncUTC, false); }
			set	{ SetValue((int)TerminalFieldIndex.LastSyncUTC, value, true); }
		}

		/// <summary> The PmsTerminalOfflineNotificationEnabled property of the Entity Terminal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal"."PmsTerminalOfflineNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsTerminalOfflineNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)TerminalFieldIndex.PmsTerminalOfflineNotificationEnabled, true); }
			set	{ SetValue((int)TerminalFieldIndex.PmsTerminalOfflineNotificationEnabled, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollection
		{
			get	{ return GetMultiIcrtouchprintermappingCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollection. When set to true, IcrtouchprintermappingCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollection is accessed. You can always execute/ a forced fetch by calling GetMultiIcrtouchprintermappingCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollection
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollection; }
			set	{ _alwaysFetchIcrtouchprintermappingCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollection already has been fetched. Setting this property to false when IcrtouchprintermappingCollection has been fetched
		/// will clear the IcrtouchprintermappingCollection collection well. Setting this property to true while IcrtouchprintermappingCollection hasn't been fetched disables lazy loading for IcrtouchprintermappingCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollection
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollection;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollection && !value && (_icrtouchprintermappingCollection != null))
				{
					_icrtouchprintermappingCollection.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceivedNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection ReceivedNetmessageCollection
		{
			get	{ return GetMultiReceivedNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceivedNetmessageCollection. When set to true, ReceivedNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceivedNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceivedNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceivedNetmessageCollection
		{
			get	{ return _alwaysFetchReceivedNetmessageCollection; }
			set	{ _alwaysFetchReceivedNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceivedNetmessageCollection already has been fetched. Setting this property to false when ReceivedNetmessageCollection has been fetched
		/// will clear the ReceivedNetmessageCollection collection well. Setting this property to true while ReceivedNetmessageCollection hasn't been fetched disables lazy loading for ReceivedNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceivedNetmessageCollection
		{
			get { return _alreadyFetchedReceivedNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedReceivedNetmessageCollection && !value && (_receivedNetmessageCollection != null))
				{
					_receivedNetmessageCollection.Clear();
				}
				_alreadyFetchedReceivedNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSentNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection SentNetmessageCollection
		{
			get	{ return GetMultiSentNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SentNetmessageCollection. When set to true, SentNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SentNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSentNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSentNetmessageCollection
		{
			get	{ return _alwaysFetchSentNetmessageCollection; }
			set	{ _alwaysFetchSentNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SentNetmessageCollection already has been fetched. Setting this property to false when SentNetmessageCollection has been fetched
		/// will clear the SentNetmessageCollection collection well. Setting this property to true while SentNetmessageCollection hasn't been fetched disables lazy loading for SentNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSentNetmessageCollection
		{
			get { return _alreadyFetchedSentNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedSentNetmessageCollection && !value && (_sentNetmessageCollection != null))
				{
					_sentNetmessageCollection.Clear();
				}
				_alreadyFetchedSentNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection OrderRoutestephandlerCollection_
		{
			get	{ return GetMultiOrderRoutestephandlerCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerCollection_. When set to true, OrderRoutestephandlerCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerCollection_
		{
			get	{ return _alwaysFetchOrderRoutestephandlerCollection_; }
			set	{ _alwaysFetchOrderRoutestephandlerCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerCollection_ already has been fetched. Setting this property to false when OrderRoutestephandlerCollection_ has been fetched
		/// will clear the OrderRoutestephandlerCollection_ collection well. Setting this property to true while OrderRoutestephandlerCollection_ hasn't been fetched disables lazy loading for OrderRoutestephandlerCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerCollection_
		{
			get { return _alreadyFetchedOrderRoutestephandlerCollection_;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerCollection_ && !value && (_orderRoutestephandlerCollection_ != null))
				{
					_orderRoutestephandlerCollection_.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection OrderRoutestephandlerCollection
		{
			get	{ return GetMultiOrderRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerCollection. When set to true, OrderRoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerCollection already has been fetched. Setting this property to false when OrderRoutestephandlerCollection has been fetched
		/// will clear the OrderRoutestephandlerCollection collection well. Setting this property to true while OrderRoutestephandlerCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerCollection && !value && (_orderRoutestephandlerCollection != null))
				{
					_orderRoutestephandlerCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerHistoryCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection OrderRoutestephandlerHistoryCollection_
		{
			get	{ return GetMultiOrderRoutestephandlerHistoryCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerHistoryCollection_. When set to true, OrderRoutestephandlerHistoryCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerHistoryCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerHistoryCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerHistoryCollection_
		{
			get	{ return _alwaysFetchOrderRoutestephandlerHistoryCollection_; }
			set	{ _alwaysFetchOrderRoutestephandlerHistoryCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerHistoryCollection_ already has been fetched. Setting this property to false when OrderRoutestephandlerHistoryCollection_ has been fetched
		/// will clear the OrderRoutestephandlerHistoryCollection_ collection well. Setting this property to true while OrderRoutestephandlerHistoryCollection_ hasn't been fetched disables lazy loading for OrderRoutestephandlerHistoryCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerHistoryCollection_
		{
			get { return _alreadyFetchedOrderRoutestephandlerHistoryCollection_;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerHistoryCollection_ && !value && (_orderRoutestephandlerHistoryCollection_ != null))
				{
					_orderRoutestephandlerHistoryCollection_.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerHistoryCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection OrderRoutestephandlerHistoryCollection
		{
			get	{ return GetMultiOrderRoutestephandlerHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerHistoryCollection. When set to true, OrderRoutestephandlerHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerHistoryCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerHistoryCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerHistoryCollection already has been fetched. Setting this property to false when OrderRoutestephandlerHistoryCollection has been fetched
		/// will clear the OrderRoutestephandlerHistoryCollection collection well. Setting this property to true while OrderRoutestephandlerHistoryCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerHistoryCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerHistoryCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerHistoryCollection && !value && (_orderRoutestephandlerHistoryCollection != null))
				{
					_orderRoutestephandlerHistoryCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection RoutestephandlerCollection
		{
			get	{ return GetMultiRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerCollection. When set to true, RoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerCollection
		{
			get	{ return _alwaysFetchRoutestephandlerCollection; }
			set	{ _alwaysFetchRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerCollection already has been fetched. Setting this property to false when RoutestephandlerCollection has been fetched
		/// will clear the RoutestephandlerCollection collection well. Setting this property to true while RoutestephandlerCollection hasn't been fetched disables lazy loading for RoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerCollection
		{
			get { return _alreadyFetchedRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerCollection && !value && (_routestephandlerCollection != null))
				{
					_routestephandlerCollection.Clear();
				}
				_alreadyFetchedRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledCommandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledCommandCollection ScheduledCommandCollection
		{
			get	{ return GetMultiScheduledCommandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledCommandCollection. When set to true, ScheduledCommandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledCommandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledCommandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledCommandCollection
		{
			get	{ return _alwaysFetchScheduledCommandCollection; }
			set	{ _alwaysFetchScheduledCommandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledCommandCollection already has been fetched. Setting this property to false when ScheduledCommandCollection has been fetched
		/// will clear the ScheduledCommandCollection collection well. Setting this property to true while ScheduledCommandCollection hasn't been fetched disables lazy loading for ScheduledCommandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledCommandCollection
		{
			get { return _alreadyFetchedScheduledCommandCollection;}
			set 
			{
				if(_alreadyFetchedScheduledCommandCollection && !value && (_scheduledCommandCollection != null))
				{
					_scheduledCommandCollection.Clear();
				}
				_alreadyFetchedScheduledCommandCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiForwardedFromTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection ForwardedFromTerminalCollection
		{
			get	{ return GetMultiForwardedFromTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ForwardedFromTerminalCollection. When set to true, ForwardedFromTerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ForwardedFromTerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiForwardedFromTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForwardedFromTerminalCollection
		{
			get	{ return _alwaysFetchForwardedFromTerminalCollection; }
			set	{ _alwaysFetchForwardedFromTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ForwardedFromTerminalCollection already has been fetched. Setting this property to false when ForwardedFromTerminalCollection has been fetched
		/// will clear the ForwardedFromTerminalCollection collection well. Setting this property to true while ForwardedFromTerminalCollection hasn't been fetched disables lazy loading for ForwardedFromTerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForwardedFromTerminalCollection
		{
			get { return _alreadyFetchedForwardedFromTerminalCollection;}
			set 
			{
				if(_alreadyFetchedForwardedFromTerminalCollection && !value && (_forwardedFromTerminalCollection != null))
				{
					_forwardedFromTerminalCollection.Clear();
				}
				_alreadyFetchedForwardedFromTerminalCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalConfigurationCollection TerminalConfigurationCollection
		{
			get	{ return GetMultiTerminalConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalConfigurationCollection. When set to true, TerminalConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalConfigurationCollection
		{
			get	{ return _alwaysFetchTerminalConfigurationCollection; }
			set	{ _alwaysFetchTerminalConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalConfigurationCollection already has been fetched. Setting this property to false when TerminalConfigurationCollection has been fetched
		/// will clear the TerminalConfigurationCollection collection well. Setting this property to true while TerminalConfigurationCollection hasn't been fetched disables lazy loading for TerminalConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalConfigurationCollection
		{
			get { return _alreadyFetchedTerminalConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedTerminalConfigurationCollection && !value && (_terminalConfigurationCollection != null))
				{
					_terminalConfigurationCollection.Clear();
				}
				_alreadyFetchedTerminalConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection TerminalLogCollection
		{
			get	{ return GetMultiTerminalLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogCollection. When set to true, TerminalLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogCollection
		{
			get	{ return _alwaysFetchTerminalLogCollection; }
			set	{ _alwaysFetchTerminalLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogCollection already has been fetched. Setting this property to false when TerminalLogCollection has been fetched
		/// will clear the TerminalLogCollection collection well. Setting this property to true while TerminalLogCollection hasn't been fetched disables lazy loading for TerminalLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogCollection
		{
			get { return _alreadyFetchedTerminalLogCollection;}
			set 
			{
				if(_alreadyFetchedTerminalLogCollection && !value && (_terminalLogCollection != null))
				{
					_terminalLogCollection.Clear();
				}
				_alreadyFetchedTerminalLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalMessageTemplateCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalMessageTemplateCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalMessageTemplateCategoryCollection TerminalMessageTemplateCategoryCollection
		{
			get	{ return GetMultiTerminalMessageTemplateCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalMessageTemplateCategoryCollection. When set to true, TerminalMessageTemplateCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalMessageTemplateCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalMessageTemplateCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalMessageTemplateCategoryCollection
		{
			get	{ return _alwaysFetchTerminalMessageTemplateCategoryCollection; }
			set	{ _alwaysFetchTerminalMessageTemplateCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalMessageTemplateCategoryCollection already has been fetched. Setting this property to false when TerminalMessageTemplateCategoryCollection has been fetched
		/// will clear the TerminalMessageTemplateCategoryCollection collection well. Setting this property to true while TerminalMessageTemplateCategoryCollection hasn't been fetched disables lazy loading for TerminalMessageTemplateCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalMessageTemplateCategoryCollection
		{
			get { return _alreadyFetchedTerminalMessageTemplateCategoryCollection;}
			set 
			{
				if(_alreadyFetchedTerminalMessageTemplateCategoryCollection && !value && (_terminalMessageTemplateCategoryCollection != null))
				{
					_terminalMessageTemplateCategoryCollection.Clear();
				}
				_alreadyFetchedTerminalMessageTemplateCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalStateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalStateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalStateCollection TerminalStateCollection
		{
			get	{ return GetMultiTerminalStateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalStateCollection. When set to true, TerminalStateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalStateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalStateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalStateCollection
		{
			get	{ return _alwaysFetchTerminalStateCollection; }
			set	{ _alwaysFetchTerminalStateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalStateCollection already has been fetched. Setting this property to false when TerminalStateCollection has been fetched
		/// will clear the TerminalStateCollection collection well. Setting this property to true while TerminalStateCollection hasn't been fetched disables lazy loading for TerminalStateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalStateCollection
		{
			get { return _alreadyFetchedTerminalStateCollection;}
			set 
			{
				if(_alreadyFetchedTerminalStateCollection && !value && (_terminalStateCollection != null))
				{
					_terminalStateCollection.Clear();
				}
				_alreadyFetchedTerminalStateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection TimestampCollection
		{
			get	{ return GetMultiTimestampCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will clear the TimestampCollection collection well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value && (_timestampCollection != null))
				{
					_timestampCollection.Clear();
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup. When set to true, AnnouncementCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup && !value && (_announcementCollectionViaDeliverypointgroup != null))
				{
					_announcementCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaNetmessage
		{
			get { return GetMultiClientCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaNetmessage. When set to true, ClientCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaNetmessage
		{
			get	{ return _alwaysFetchClientCollectionViaNetmessage; }
			set	{ _alwaysFetchClientCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaNetmessage already has been fetched. Setting this property to false when ClientCollectionViaNetmessage has been fetched
		/// will clear the ClientCollectionViaNetmessage collection well. Setting this property to true while ClientCollectionViaNetmessage hasn't been fetched disables lazy loading for ClientCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaNetmessage
		{
			get { return _alreadyFetchedClientCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaNetmessage && !value && (_clientCollectionViaNetmessage != null))
				{
					_clientCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedClientCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup. When set to true, CompanyCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup && !value && (_companyCollectionViaDeliverypointgroup != null))
				{
					_companyCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage
		{
			get { return GetMultiCompanyCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage. When set to true, CompanyCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage has been fetched
		/// will clear the CompanyCollectionViaNetmessage collection well. Setting this property to true while CompanyCollectionViaNetmessage hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage && !value && (_companyCollectionViaNetmessage != null))
				{
					_companyCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage_
		{
			get { return GetMultiCompanyCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage_. When set to true, CompanyCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage_; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage_ already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage_ has been fetched
		/// will clear the CompanyCollectionViaNetmessage_ collection well. Setting this property to true while CompanyCollectionViaNetmessage_ hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage_
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage_ && !value && (_companyCollectionViaNetmessage_ != null))
				{
					_companyCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage__
		{
			get { return GetMultiCompanyCollectionViaNetmessage__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage__. When set to true, CompanyCollectionViaNetmessage__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage__ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage__
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage__; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage__ already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage__ has been fetched
		/// will clear the CompanyCollectionViaNetmessage__ collection well. Setting this property to true while CompanyCollectionViaNetmessage__ hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage__
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage__;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage__ && !value && (_companyCollectionViaNetmessage__ != null))
				{
					_companyCollectionViaNetmessage__.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage___
		{
			get { return GetMultiCompanyCollectionViaNetmessage___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage___. When set to true, CompanyCollectionViaNetmessage___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage___ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage___
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage___; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage___ already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage___ has been fetched
		/// will clear the CompanyCollectionViaNetmessage___ collection well. Setting this property to true while CompanyCollectionViaNetmessage___ hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage___
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage___;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage___ && !value && (_companyCollectionViaNetmessage___ != null))
				{
					_companyCollectionViaNetmessage___.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaTerminal
		{
			get { return GetMultiCompanyCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaTerminal. When set to true, CompanyCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaTerminal
		{
			get	{ return _alwaysFetchCompanyCollectionViaTerminal; }
			set	{ _alwaysFetchCompanyCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaTerminal already has been fetched. Setting this property to false when CompanyCollectionViaTerminal has been fetched
		/// will clear the CompanyCollectionViaTerminal collection well. Setting this property to true while CompanyCollectionViaTerminal hasn't been fetched disables lazy loading for CompanyCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaTerminal
		{
			get { return _alreadyFetchedCompanyCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaTerminal && !value && (_companyCollectionViaTerminal != null))
				{
					_companyCollectionViaTerminal.Clear();
				}
				_alreadyFetchedCompanyCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage
		{
			get { return GetMultiCustomerCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage. When set to true, CustomerCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage has been fetched
		/// will clear the CustomerCollectionViaNetmessage collection well. Setting this property to true while CustomerCollectionViaNetmessage hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage && !value && (_customerCollectionViaNetmessage != null))
				{
					_customerCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage_
		{
			get { return GetMultiCustomerCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage_. When set to true, CustomerCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage_; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage_ already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage_ has been fetched
		/// will clear the CustomerCollectionViaNetmessage_ collection well. Setting this property to true while CustomerCollectionViaNetmessage_ hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage_
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage_ && !value && (_customerCollectionViaNetmessage_ != null))
				{
					_customerCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage__
		{
			get { return GetMultiCustomerCollectionViaNetmessage__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage__. When set to true, CustomerCollectionViaNetmessage__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage__ is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage__
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage__; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage__ already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage__ has been fetched
		/// will clear the CustomerCollectionViaNetmessage__ collection well. Setting this property to true while CustomerCollectionViaNetmessage__ hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage__
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage__;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage__ && !value && (_customerCollectionViaNetmessage__ != null))
				{
					_customerCollectionViaNetmessage__.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage___
		{
			get { return GetMultiCustomerCollectionViaNetmessage___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage___. When set to true, CustomerCollectionViaNetmessage___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage___ is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage___
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage___; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage___ already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage___ has been fetched
		/// will clear the CustomerCollectionViaNetmessage___ collection well. Setting this property to true while CustomerCollectionViaNetmessage___ hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage___
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage___;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage___ && !value && (_customerCollectionViaNetmessage___ != null))
				{
					_customerCollectionViaNetmessage___.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage. When set to true, DeliverypointCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage collection well. Setting this property to true while DeliverypointCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage && !value && (_deliverypointCollectionViaNetmessage != null))
				{
					_deliverypointCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage_
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage_. When set to true, DeliverypointCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage_; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage_ already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage_ has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage_ collection well. Setting this property to true while DeliverypointCollectionViaNetmessage_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage_ && !value && (_deliverypointCollectionViaNetmessage_ != null))
				{
					_deliverypointCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage__
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage__. When set to true, DeliverypointCollectionViaNetmessage__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage__ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage__
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage__; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage__ already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage__ has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage__ collection well. Setting this property to true while DeliverypointCollectionViaNetmessage__ hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage__
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage__;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage__ && !value && (_deliverypointCollectionViaNetmessage__ != null))
				{
					_deliverypointCollectionViaNetmessage__.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage___
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage___. When set to true, DeliverypointCollectionViaNetmessage___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage___ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage___
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage___; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage___ already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage___ has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage___ collection well. Setting this property to true while DeliverypointCollectionViaNetmessage___ hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage___
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage___;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage___ && !value && (_deliverypointCollectionViaNetmessage___ != null))
				{
					_deliverypointCollectionViaNetmessage___.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal
		{
			get { return GetMultiDeliverypointCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal. When set to true, DeliverypointCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal has been fetched
		/// will clear the DeliverypointCollectionViaTerminal collection well. Setting this property to true while DeliverypointCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal && !value && (_deliverypointCollectionViaTerminal != null))
				{
					_deliverypointCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal_. When set to true, DeliverypointCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal_ && !value && (_deliverypointCollectionViaTerminal_ != null))
				{
					_deliverypointCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage. When set to true, DeliverypointgroupCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage && !value && (_deliverypointgroupCollectionViaNetmessage != null))
				{
					_deliverypointgroupCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage_
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage_. When set to true, DeliverypointgroupCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage_ collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ && !value && (_deliverypointgroupCollectionViaNetmessage_ != null))
				{
					_deliverypointgroupCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal. When set to true, DeliverypointgroupCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal && !value && (_deliverypointgroupCollectionViaTerminal != null))
				{
					_deliverypointgroupCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal
		{
			get { return GetMultiDeviceCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal. When set to true, DeviceCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal already has been fetched. Setting this property to false when DeviceCollectionViaTerminal has been fetched
		/// will clear the DeviceCollectionViaTerminal collection well. Setting this property to true while DeviceCollectionViaTerminal hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal && !value && (_deviceCollectionViaTerminal != null))
				{
					_deviceCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollectionViaTerminal
		{
			get { return GetMultiIcrtouchprintermappingCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollectionViaTerminal. When set to true, IcrtouchprintermappingCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiIcrtouchprintermappingCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollectionViaTerminal
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollectionViaTerminal; }
			set	{ _alwaysFetchIcrtouchprintermappingCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollectionViaTerminal already has been fetched. Setting this property to false when IcrtouchprintermappingCollectionViaTerminal has been fetched
		/// will clear the IcrtouchprintermappingCollectionViaTerminal collection well. Setting this property to true while IcrtouchprintermappingCollectionViaTerminal hasn't been fetched disables lazy loading for IcrtouchprintermappingCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollectionViaTerminal
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal && !value && (_icrtouchprintermappingCollectionViaTerminal != null))
				{
					_icrtouchprintermappingCollectionViaTerminal.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup. When set to true, MenuCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup && !value && (_menuCollectionViaDeliverypointgroup != null))
				{
					_menuCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandler
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandler. When set to true, OrderCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandler has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandler collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandler && !value && (_orderCollectionViaOrderRoutestephandler != null))
				{
					_orderCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandler_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandler_
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandler_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandler_. When set to true, OrderCollectionViaOrderRoutestephandler_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandler_ is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandler_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandler_
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandler_; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandler_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandler_ already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandler_ has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandler_ collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandler_ hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandler_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandler_
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandler_;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ && !value && (_orderCollectionViaOrderRoutestephandler_ != null))
				{
					_orderCollectionViaOrderRoutestephandler_.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandlerHistory. When set to true, OrderCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory && !value && (_orderCollectionViaOrderRoutestephandlerHistory != null))
				{
					_orderCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandlerHistory_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandlerHistory_
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandlerHistory_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandlerHistory_. When set to true, OrderCollectionViaOrderRoutestephandlerHistory_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandlerHistory_ is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandlerHistory_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandlerHistory_ already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandlerHistory_ has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandlerHistory_ collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandlerHistory_ hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandlerHistory_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ && !value && (_orderCollectionViaOrderRoutestephandlerHistory_ != null))
				{
					_orderCollectionViaOrderRoutestephandlerHistory_.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal
		{
			get { return GetMultiProductCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal. When set to true, ProductCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal; }
			set	{ _alwaysFetchProductCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal already has been fetched. Setting this property to false when ProductCollectionViaTerminal has been fetched
		/// will clear the ProductCollectionViaTerminal collection well. Setting this property to true while ProductCollectionViaTerminal hasn't been fetched disables lazy loading for ProductCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal
		{
			get { return _alreadyFetchedProductCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal && !value && (_productCollectionViaTerminal != null))
				{
					_productCollectionViaTerminal.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal_
		{
			get { return GetMultiProductCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal_. When set to true, ProductCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal_
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal_; }
			set	{ _alwaysFetchProductCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal_ already has been fetched. Setting this property to false when ProductCollectionViaTerminal_ has been fetched
		/// will clear the ProductCollectionViaTerminal_ collection well. Setting this property to true while ProductCollectionViaTerminal_ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal_
		{
			get { return _alreadyFetchedProductCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal_ && !value && (_productCollectionViaTerminal_ != null))
				{
					_productCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal__
		{
			get { return GetMultiProductCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal__. When set to true, ProductCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal__
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal__; }
			set	{ _alwaysFetchProductCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal__ already has been fetched. Setting this property to false when ProductCollectionViaTerminal__ has been fetched
		/// will clear the ProductCollectionViaTerminal__ collection well. Setting this property to true while ProductCollectionViaTerminal__ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal__
		{
			get { return _alreadyFetchedProductCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal__ && !value && (_productCollectionViaTerminal__ != null))
				{
					_productCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal___
		{
			get { return GetMultiProductCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal___. When set to true, ProductCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal___
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal___; }
			set	{ _alwaysFetchProductCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal___ already has been fetched. Setting this property to false when ProductCollectionViaTerminal___ has been fetched
		/// will clear the ProductCollectionViaTerminal___ collection well. Setting this property to true while ProductCollectionViaTerminal___ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal___
		{
			get { return _alreadyFetchedProductCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal___ && !value && (_productCollectionViaTerminal___ != null))
				{
					_productCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup. When set to true, RouteCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup && !value && (_routeCollectionViaDeliverypointgroup != null))
				{
					_routeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_. When set to true, RouteCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_ && !value && (_routeCollectionViaDeliverypointgroup_ != null))
				{
					_routeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestepCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestepCollection RoutestepCollectionViaRoutestephandler
		{
			get { return GetMultiRoutestepCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestepCollectionViaRoutestephandler. When set to true, RoutestepCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestepCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiRoutestepCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestepCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchRoutestepCollectionViaRoutestephandler; }
			set	{ _alwaysFetchRoutestepCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestepCollectionViaRoutestephandler already has been fetched. Setting this property to false when RoutestepCollectionViaRoutestephandler has been fetched
		/// will clear the RoutestepCollectionViaRoutestephandler collection well. Setting this property to true while RoutestepCollectionViaRoutestephandler hasn't been fetched disables lazy loading for RoutestepCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestepCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedRoutestepCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedRoutestepCollectionViaRoutestephandler && !value && (_routestepCollectionViaRoutestephandler != null))
				{
					_routestepCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedRoutestepCollectionViaRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandler
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandler. When set to true, SupportpoolCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandler has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandler collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler && !value && (_supportpoolCollectionViaOrderRoutestephandler != null))
				{
					_supportpoolCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandler_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandler_
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandler_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandler_. When set to true, SupportpoolCollectionViaOrderRoutestephandler_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandler_ is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandler_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandler_
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandler_ already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandler_ has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandler_ collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandler_ hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandler_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ && !value && (_supportpoolCollectionViaOrderRoutestephandler_ != null))
				{
					_supportpoolCollectionViaOrderRoutestephandler_.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandlerHistory. When set to true, SupportpoolCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory && !value && (_supportpoolCollectionViaOrderRoutestephandlerHistory != null))
				{
					_supportpoolCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandlerHistory_
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandlerHistory_. When set to true, SupportpoolCollectionViaOrderRoutestephandlerHistory_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandlerHistory_ is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandlerHistory_ already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandlerHistory_ has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandlerHistory_ collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandlerHistory_ hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandlerHistory_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ && !value && (_supportpoolCollectionViaOrderRoutestephandlerHistory_ != null))
				{
					_supportpoolCollectionViaOrderRoutestephandlerHistory_.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaRoutestephandler
		{
			get { return GetMultiSupportpoolCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaRoutestephandler. When set to true, SupportpoolCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaRoutestephandler; }
			set	{ _alwaysFetchSupportpoolCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaRoutestephandler already has been fetched. Setting this property to false when SupportpoolCollectionViaRoutestephandler has been fetched
		/// will clear the SupportpoolCollectionViaRoutestephandler collection well. Setting this property to true while SupportpoolCollectionViaRoutestephandler hasn't been fetched disables lazy loading for SupportpoolCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedSupportpoolCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaRoutestephandler && !value && (_supportpoolCollectionViaRoutestephandler != null))
				{
					_supportpoolCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage
		{
			get { return GetMultiTerminalCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage. When set to true, TerminalCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage has been fetched
		/// will clear the TerminalCollectionViaNetmessage collection well. Setting this property to true while TerminalCollectionViaNetmessage hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage && !value && (_terminalCollectionViaNetmessage != null))
				{
					_terminalCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage_
		{
			get { return GetMultiTerminalCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage_. When set to true, TerminalCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage_; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage_ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage_ has been fetched
		/// will clear the TerminalCollectionViaNetmessage_ collection well. Setting this property to true while TerminalCollectionViaNetmessage_ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage_
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage_ && !value && (_terminalCollectionViaNetmessage_ != null))
				{
					_terminalCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler. When set to true, TerminalCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler && !value && (_terminalCollectionViaOrderRoutestephandler != null))
				{
					_terminalCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler_. When set to true, TerminalCollectionViaOrderRoutestephandler_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ && !value && (_terminalCollectionViaOrderRoutestephandler_ != null))
				{
					_terminalCollectionViaOrderRoutestephandler_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory && !value && (_terminalCollectionViaOrderRoutestephandlerHistory != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory_. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ && !value && (_terminalCollectionViaOrderRoutestephandlerHistory_ != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalLogFileCollectionViaTerminalLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogFileCollection TerminalLogFileCollectionViaTerminalLog
		{
			get { return GetMultiTerminalLogFileCollectionViaTerminalLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogFileCollectionViaTerminalLog. When set to true, TerminalLogFileCollectionViaTerminalLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogFileCollectionViaTerminalLog is accessed. You can always execute a forced fetch by calling GetMultiTerminalLogFileCollectionViaTerminalLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogFileCollectionViaTerminalLog
		{
			get	{ return _alwaysFetchTerminalLogFileCollectionViaTerminalLog; }
			set	{ _alwaysFetchTerminalLogFileCollectionViaTerminalLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogFileCollectionViaTerminalLog already has been fetched. Setting this property to false when TerminalLogFileCollectionViaTerminalLog has been fetched
		/// will clear the TerminalLogFileCollectionViaTerminalLog collection well. Setting this property to true while TerminalLogFileCollectionViaTerminalLog hasn't been fetched disables lazy loading for TerminalLogFileCollectionViaTerminalLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogFileCollectionViaTerminalLog
		{
			get { return _alreadyFetchedTerminalLogFileCollectionViaTerminalLog;}
			set 
			{
				if(_alreadyFetchedTerminalLogFileCollectionViaTerminalLog && !value && (_terminalLogFileCollectionViaTerminalLog != null))
				{
					_terminalLogFileCollectionViaTerminalLog.Clear();
				}
				_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_. When set to true, UIModeCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ && !value && (_uIModeCollectionViaDeliverypointgroup_ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup__
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup__. When set to true, UIModeCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup__ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ && !value && (_uIModeCollectionViaDeliverypointgroup__ != null))
				{
					_uIModeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal
		{
			get { return GetMultiUIModeCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal. When set to true, UIModeCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal already has been fetched. Setting this property to false when UIModeCollectionViaTerminal has been fetched
		/// will clear the UIModeCollectionViaTerminal collection well. Setting this property to true while UIModeCollectionViaTerminal hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal && !value && (_uIModeCollectionViaTerminal != null))
				{
					_uIModeCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup. When set to true, UIModeCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup && !value && (_uIModeCollectionViaDeliverypointgroup != null))
				{
					_uIModeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutomaticSignOnUserCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection AutomaticSignOnUserCollectionViaTerminal
		{
			get { return GetMultiAutomaticSignOnUserCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutomaticSignOnUserCollectionViaTerminal. When set to true, AutomaticSignOnUserCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutomaticSignOnUserCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiAutomaticSignOnUserCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomaticSignOnUserCollectionViaTerminal
		{
			get	{ return _alwaysFetchAutomaticSignOnUserCollectionViaTerminal; }
			set	{ _alwaysFetchAutomaticSignOnUserCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutomaticSignOnUserCollectionViaTerminal already has been fetched. Setting this property to false when AutomaticSignOnUserCollectionViaTerminal has been fetched
		/// will clear the AutomaticSignOnUserCollectionViaTerminal collection well. Setting this property to true while AutomaticSignOnUserCollectionViaTerminal hasn't been fetched disables lazy loading for AutomaticSignOnUserCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomaticSignOnUserCollectionViaTerminal
		{
			get { return _alreadyFetchedAutomaticSignOnUserCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal && !value && (_automaticSignOnUserCollectionViaTerminal != null))
				{
					_automaticSignOnUserCollectionViaTerminal.Clear();
				}
				_alreadyFetchedAutomaticSignOnUserCollectionViaTerminal = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAltSystemMessagesDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity AltSystemMessagesDeliverypointEntity
		{
			get	{ return GetSingleAltSystemMessagesDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAltSystemMessagesDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection_", "AltSystemMessagesDeliverypointEntity", _altSystemMessagesDeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AltSystemMessagesDeliverypointEntity. When set to true, AltSystemMessagesDeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AltSystemMessagesDeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleAltSystemMessagesDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAltSystemMessagesDeliverypointEntity
		{
			get	{ return _alwaysFetchAltSystemMessagesDeliverypointEntity; }
			set	{ _alwaysFetchAltSystemMessagesDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AltSystemMessagesDeliverypointEntity already has been fetched. Setting this property to false when AltSystemMessagesDeliverypointEntity has been fetched
		/// will set AltSystemMessagesDeliverypointEntity to null as well. Setting this property to true while AltSystemMessagesDeliverypointEntity hasn't been fetched disables lazy loading for AltSystemMessagesDeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAltSystemMessagesDeliverypointEntity
		{
			get { return _alreadyFetchedAltSystemMessagesDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedAltSystemMessagesDeliverypointEntity && !value)
				{
					this.AltSystemMessagesDeliverypointEntity = null;
				}
				_alreadyFetchedAltSystemMessagesDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AltSystemMessagesDeliverypointEntity is not found
		/// in the database. When set to true, AltSystemMessagesDeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AltSystemMessagesDeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _altSystemMessagesDeliverypointEntityReturnsNewIfNotFound; }
			set { _altSystemMessagesDeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSystemMessagesDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity SystemMessagesDeliverypointEntity
		{
			get	{ return GetSingleSystemMessagesDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSystemMessagesDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "SystemMessagesDeliverypointEntity", _systemMessagesDeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SystemMessagesDeliverypointEntity. When set to true, SystemMessagesDeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemMessagesDeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleSystemMessagesDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemMessagesDeliverypointEntity
		{
			get	{ return _alwaysFetchSystemMessagesDeliverypointEntity; }
			set	{ _alwaysFetchSystemMessagesDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemMessagesDeliverypointEntity already has been fetched. Setting this property to false when SystemMessagesDeliverypointEntity has been fetched
		/// will set SystemMessagesDeliverypointEntity to null as well. Setting this property to true while SystemMessagesDeliverypointEntity hasn't been fetched disables lazy loading for SystemMessagesDeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemMessagesDeliverypointEntity
		{
			get { return _alreadyFetchedSystemMessagesDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedSystemMessagesDeliverypointEntity && !value)
				{
					this.SystemMessagesDeliverypointEntity = null;
				}
				_alreadyFetchedSystemMessagesDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SystemMessagesDeliverypointEntity is not found
		/// in the database. When set to true, SystemMessagesDeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SystemMessagesDeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _systemMessagesDeliverypointEntityReturnsNewIfNotFound; }
			set { _systemMessagesDeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeviceEntity DeviceEntity
		{
			get	{ return GetSingleDeviceEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "DeviceEntity", _deviceEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceEntity. When set to true, DeviceEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceEntity is accessed. You can always execute a forced fetch by calling GetSingleDeviceEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceEntity
		{
			get	{ return _alwaysFetchDeviceEntity; }
			set	{ _alwaysFetchDeviceEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceEntity already has been fetched. Setting this property to false when DeviceEntity has been fetched
		/// will set DeviceEntity to null as well. Setting this property to true while DeviceEntity hasn't been fetched disables lazy loading for DeviceEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceEntity
		{
			get { return _alreadyFetchedDeviceEntity;}
			set 
			{
				if(_alreadyFetchedDeviceEntity && !value)
				{
					this.DeviceEntity = null;
				}
				_alreadyFetchedDeviceEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceEntity is not found
		/// in the database. When set to true, DeviceEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeviceEntityReturnsNewIfNotFound
		{
			get	{ return _deviceEntityReturnsNewIfNotFound; }
			set { _deviceEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrowser1EntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity Browser1EntertainmentEntity
		{
			get	{ return GetSingleBrowser1EntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrowser1EntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "Browser1EntertainmentEntity", _browser1EntertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Browser1EntertainmentEntity. When set to true, Browser1EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Browser1EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleBrowser1EntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrowser1EntertainmentEntity
		{
			get	{ return _alwaysFetchBrowser1EntertainmentEntity; }
			set	{ _alwaysFetchBrowser1EntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Browser1EntertainmentEntity already has been fetched. Setting this property to false when Browser1EntertainmentEntity has been fetched
		/// will set Browser1EntertainmentEntity to null as well. Setting this property to true while Browser1EntertainmentEntity hasn't been fetched disables lazy loading for Browser1EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrowser1EntertainmentEntity
		{
			get { return _alreadyFetchedBrowser1EntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedBrowser1EntertainmentEntity && !value)
				{
					this.Browser1EntertainmentEntity = null;
				}
				_alreadyFetchedBrowser1EntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Browser1EntertainmentEntity is not found
		/// in the database. When set to true, Browser1EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool Browser1EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _browser1EntertainmentEntityReturnsNewIfNotFound; }
			set { _browser1EntertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrowser2EntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity Browser2EntertainmentEntity
		{
			get	{ return GetSingleBrowser2EntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrowser2EntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection_", "Browser2EntertainmentEntity", _browser2EntertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Browser2EntertainmentEntity. When set to true, Browser2EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Browser2EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleBrowser2EntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrowser2EntertainmentEntity
		{
			get	{ return _alwaysFetchBrowser2EntertainmentEntity; }
			set	{ _alwaysFetchBrowser2EntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Browser2EntertainmentEntity already has been fetched. Setting this property to false when Browser2EntertainmentEntity has been fetched
		/// will set Browser2EntertainmentEntity to null as well. Setting this property to true while Browser2EntertainmentEntity hasn't been fetched disables lazy loading for Browser2EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrowser2EntertainmentEntity
		{
			get { return _alreadyFetchedBrowser2EntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedBrowser2EntertainmentEntity && !value)
				{
					this.Browser2EntertainmentEntity = null;
				}
				_alreadyFetchedBrowser2EntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Browser2EntertainmentEntity is not found
		/// in the database. When set to true, Browser2EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool Browser2EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _browser2EntertainmentEntityReturnsNewIfNotFound; }
			set { _browser2EntertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCmsPageEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity CmsPageEntertainmentEntity
		{
			get	{ return GetSingleCmsPageEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCmsPageEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection__", "CmsPageEntertainmentEntity", _cmsPageEntertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CmsPageEntertainmentEntity. When set to true, CmsPageEntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CmsPageEntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleCmsPageEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCmsPageEntertainmentEntity
		{
			get	{ return _alwaysFetchCmsPageEntertainmentEntity; }
			set	{ _alwaysFetchCmsPageEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CmsPageEntertainmentEntity already has been fetched. Setting this property to false when CmsPageEntertainmentEntity has been fetched
		/// will set CmsPageEntertainmentEntity to null as well. Setting this property to true while CmsPageEntertainmentEntity hasn't been fetched disables lazy loading for CmsPageEntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCmsPageEntertainmentEntity
		{
			get { return _alreadyFetchedCmsPageEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedCmsPageEntertainmentEntity && !value)
				{
					this.CmsPageEntertainmentEntity = null;
				}
				_alreadyFetchedCmsPageEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CmsPageEntertainmentEntity is not found
		/// in the database. When set to true, CmsPageEntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CmsPageEntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _cmsPageEntertainmentEntityReturnsNewIfNotFound; }
			set { _cmsPageEntertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'IcrtouchprintermappingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIcrtouchprintermappingEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual IcrtouchprintermappingEntity IcrtouchprintermappingEntity
		{
			get	{ return GetSingleIcrtouchprintermappingEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIcrtouchprintermappingEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "IcrtouchprintermappingEntity", _icrtouchprintermappingEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingEntity. When set to true, IcrtouchprintermappingEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingEntity is accessed. You can always execute a forced fetch by calling GetSingleIcrtouchprintermappingEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingEntity
		{
			get	{ return _alwaysFetchIcrtouchprintermappingEntity; }
			set	{ _alwaysFetchIcrtouchprintermappingEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingEntity already has been fetched. Setting this property to false when IcrtouchprintermappingEntity has been fetched
		/// will set IcrtouchprintermappingEntity to null as well. Setting this property to true while IcrtouchprintermappingEntity hasn't been fetched disables lazy loading for IcrtouchprintermappingEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingEntity
		{
			get { return _alreadyFetchedIcrtouchprintermappingEntity;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingEntity && !value)
				{
					this.IcrtouchprintermappingEntity = null;
				}
				_alreadyFetchedIcrtouchprintermappingEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IcrtouchprintermappingEntity is not found
		/// in the database. When set to true, IcrtouchprintermappingEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool IcrtouchprintermappingEntityReturnsNewIfNotFound
		{
			get	{ return _icrtouchprintermappingEntityReturnsNewIfNotFound; }
			set { _icrtouchprintermappingEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBatteryLowProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity BatteryLowProductEntity
		{
			get	{ return GetSingleBatteryLowProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBatteryLowProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection_", "BatteryLowProductEntity", _batteryLowProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BatteryLowProductEntity. When set to true, BatteryLowProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BatteryLowProductEntity is accessed. You can always execute a forced fetch by calling GetSingleBatteryLowProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBatteryLowProductEntity
		{
			get	{ return _alwaysFetchBatteryLowProductEntity; }
			set	{ _alwaysFetchBatteryLowProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BatteryLowProductEntity already has been fetched. Setting this property to false when BatteryLowProductEntity has been fetched
		/// will set BatteryLowProductEntity to null as well. Setting this property to true while BatteryLowProductEntity hasn't been fetched disables lazy loading for BatteryLowProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBatteryLowProductEntity
		{
			get { return _alreadyFetchedBatteryLowProductEntity;}
			set 
			{
				if(_alreadyFetchedBatteryLowProductEntity && !value)
				{
					this.BatteryLowProductEntity = null;
				}
				_alreadyFetchedBatteryLowProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BatteryLowProductEntity is not found
		/// in the database. When set to true, BatteryLowProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BatteryLowProductEntityReturnsNewIfNotFound
		{
			get	{ return _batteryLowProductEntityReturnsNewIfNotFound; }
			set { _batteryLowProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientDisconnectedProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ClientDisconnectedProductEntity
		{
			get	{ return GetSingleClientDisconnectedProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientDisconnectedProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection__", "ClientDisconnectedProductEntity", _clientDisconnectedProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientDisconnectedProductEntity. When set to true, ClientDisconnectedProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientDisconnectedProductEntity is accessed. You can always execute a forced fetch by calling GetSingleClientDisconnectedProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientDisconnectedProductEntity
		{
			get	{ return _alwaysFetchClientDisconnectedProductEntity; }
			set	{ _alwaysFetchClientDisconnectedProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientDisconnectedProductEntity already has been fetched. Setting this property to false when ClientDisconnectedProductEntity has been fetched
		/// will set ClientDisconnectedProductEntity to null as well. Setting this property to true while ClientDisconnectedProductEntity hasn't been fetched disables lazy loading for ClientDisconnectedProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientDisconnectedProductEntity
		{
			get { return _alreadyFetchedClientDisconnectedProductEntity;}
			set 
			{
				if(_alreadyFetchedClientDisconnectedProductEntity && !value)
				{
					this.ClientDisconnectedProductEntity = null;
				}
				_alreadyFetchedClientDisconnectedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientDisconnectedProductEntity is not found
		/// in the database. When set to true, ClientDisconnectedProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientDisconnectedProductEntityReturnsNewIfNotFound
		{
			get	{ return _clientDisconnectedProductEntityReturnsNewIfNotFound; }
			set { _clientDisconnectedProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection___", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnlockDeliverypointProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity UnlockDeliverypointProductEntity
		{
			get	{ return GetSingleUnlockDeliverypointProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnlockDeliverypointProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "UnlockDeliverypointProductEntity", _unlockDeliverypointProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UnlockDeliverypointProductEntity. When set to true, UnlockDeliverypointProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnlockDeliverypointProductEntity is accessed. You can always execute a forced fetch by calling GetSingleUnlockDeliverypointProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnlockDeliverypointProductEntity
		{
			get	{ return _alwaysFetchUnlockDeliverypointProductEntity; }
			set	{ _alwaysFetchUnlockDeliverypointProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnlockDeliverypointProductEntity already has been fetched. Setting this property to false when UnlockDeliverypointProductEntity has been fetched
		/// will set UnlockDeliverypointProductEntity to null as well. Setting this property to true while UnlockDeliverypointProductEntity hasn't been fetched disables lazy loading for UnlockDeliverypointProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnlockDeliverypointProductEntity
		{
			get { return _alreadyFetchedUnlockDeliverypointProductEntity;}
			set 
			{
				if(_alreadyFetchedUnlockDeliverypointProductEntity && !value)
				{
					this.UnlockDeliverypointProductEntity = null;
				}
				_alreadyFetchedUnlockDeliverypointProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UnlockDeliverypointProductEntity is not found
		/// in the database. When set to true, UnlockDeliverypointProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UnlockDeliverypointProductEntityReturnsNewIfNotFound
		{
			get	{ return _unlockDeliverypointProductEntityReturnsNewIfNotFound; }
			set { _unlockDeliverypointProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForwardToTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity ForwardToTerminalEntity
		{
			get	{ return GetSingleForwardToTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForwardToTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ForwardedFromTerminalCollection", "ForwardToTerminalEntity", _forwardToTerminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ForwardToTerminalEntity. When set to true, ForwardToTerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ForwardToTerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleForwardToTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForwardToTerminalEntity
		{
			get	{ return _alwaysFetchForwardToTerminalEntity; }
			set	{ _alwaysFetchForwardToTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ForwardToTerminalEntity already has been fetched. Setting this property to false when ForwardToTerminalEntity has been fetched
		/// will set ForwardToTerminalEntity to null as well. Setting this property to true while ForwardToTerminalEntity hasn't been fetched disables lazy loading for ForwardToTerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForwardToTerminalEntity
		{
			get { return _alreadyFetchedForwardToTerminalEntity;}
			set 
			{
				if(_alreadyFetchedForwardToTerminalEntity && !value)
				{
					this.ForwardToTerminalEntity = null;
				}
				_alreadyFetchedForwardToTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ForwardToTerminalEntity is not found
		/// in the database. When set to true, ForwardToTerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ForwardToTerminalEntityReturnsNewIfNotFound
		{
			get	{ return _forwardToTerminalEntityReturnsNewIfNotFound; }
			set { _forwardToTerminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalConfigurationEntity TerminalConfigurationEntity
		{
			get	{ return GetSingleTerminalConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "TerminalConfigurationEntity", _terminalConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalConfigurationEntity. When set to true, TerminalConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalConfigurationEntity
		{
			get	{ return _alwaysFetchTerminalConfigurationEntity; }
			set	{ _alwaysFetchTerminalConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalConfigurationEntity already has been fetched. Setting this property to false when TerminalConfigurationEntity has been fetched
		/// will set TerminalConfigurationEntity to null as well. Setting this property to true while TerminalConfigurationEntity hasn't been fetched disables lazy loading for TerminalConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalConfigurationEntity
		{
			get { return _alreadyFetchedTerminalConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedTerminalConfigurationEntity && !value)
				{
					this.TerminalConfigurationEntity = null;
				}
				_alreadyFetchedTerminalConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalConfigurationEntity is not found
		/// in the database. When set to true, TerminalConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _terminalConfigurationEntityReturnsNewIfNotFound; }
			set { _terminalConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity UIModeEntity
		{
			get	{ return GetSingleUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "UIModeEntity", _uIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeEntity. When set to true, UIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeEntity
		{
			get	{ return _alwaysFetchUIModeEntity; }
			set	{ _alwaysFetchUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeEntity already has been fetched. Setting this property to false when UIModeEntity has been fetched
		/// will set UIModeEntity to null as well. Setting this property to true while UIModeEntity hasn't been fetched disables lazy loading for UIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeEntity
		{
			get { return _alreadyFetchedUIModeEntity;}
			set 
			{
				if(_alreadyFetchedUIModeEntity && !value)
				{
					this.UIModeEntity = null;
				}
				_alreadyFetchedUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIModeEntity is not found
		/// in the database. When set to true, UIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIModeEntityReturnsNewIfNotFound
		{
			get	{ return _uIModeEntityReturnsNewIfNotFound; }
			set { _uIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutomaticSignOnUserEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UserEntity AutomaticSignOnUserEntity
		{
			get	{ return GetSingleAutomaticSignOnUserEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutomaticSignOnUserEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalCollection", "AutomaticSignOnUserEntity", _automaticSignOnUserEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AutomaticSignOnUserEntity. When set to true, AutomaticSignOnUserEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutomaticSignOnUserEntity is accessed. You can always execute a forced fetch by calling GetSingleAutomaticSignOnUserEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomaticSignOnUserEntity
		{
			get	{ return _alwaysFetchAutomaticSignOnUserEntity; }
			set	{ _alwaysFetchAutomaticSignOnUserEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutomaticSignOnUserEntity already has been fetched. Setting this property to false when AutomaticSignOnUserEntity has been fetched
		/// will set AutomaticSignOnUserEntity to null as well. Setting this property to true while AutomaticSignOnUserEntity hasn't been fetched disables lazy loading for AutomaticSignOnUserEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomaticSignOnUserEntity
		{
			get { return _alreadyFetchedAutomaticSignOnUserEntity;}
			set 
			{
				if(_alreadyFetchedAutomaticSignOnUserEntity && !value)
				{
					this.AutomaticSignOnUserEntity = null;
				}
				_alreadyFetchedAutomaticSignOnUserEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AutomaticSignOnUserEntity is not found
		/// in the database. When set to true, AutomaticSignOnUserEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AutomaticSignOnUserEntityReturnsNewIfNotFound
		{
			get	{ return _automaticSignOnUserEntityReturnsNewIfNotFound; }
			set { _automaticSignOnUserEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.TerminalEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
