﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Genericalterationoption'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GenericalterationoptionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GenericalterationoptionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection	_alterationoptionCollection;
		private bool	_alwaysFetchAlterationoptionCollection, _alreadyFetchedAlterationoptionCollection;
		private Obymobi.Data.CollectionClasses.GenericalterationitemCollection	_genericalterationitemCollection;
		private bool	_alwaysFetchGenericalterationitemCollection, _alreadyFetchedGenericalterationitemCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAlterationoption;
		private bool	_alwaysFetchCompanyCollectionViaAlterationoption, _alreadyFetchedCompanyCollectionViaAlterationoption;
		private Obymobi.Data.CollectionClasses.GenericalterationCollection _genericalterationCollectionViaGenericalterationitem;
		private bool	_alwaysFetchGenericalterationCollectionViaGenericalterationitem, _alreadyFetchedGenericalterationCollectionViaGenericalterationitem;
		private Obymobi.Data.CollectionClasses.PosalterationoptionCollection _posalterationoptionCollectionViaAlterationoption;
		private bool	_alwaysFetchPosalterationoptionCollectionViaAlterationoption, _alreadyFetchedPosalterationoptionCollectionViaAlterationoption;
		private Obymobi.Data.CollectionClasses.PosproductCollection _posproductCollectionViaAlterationoption;
		private bool	_alwaysFetchPosproductCollectionViaAlterationoption, _alreadyFetchedPosproductCollectionViaAlterationoption;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name GenericalterationitemCollection</summary>
			public static readonly string GenericalterationitemCollection = "GenericalterationitemCollection";
			/// <summary>Member name CompanyCollectionViaAlterationoption</summary>
			public static readonly string CompanyCollectionViaAlterationoption = "CompanyCollectionViaAlterationoption";
			/// <summary>Member name GenericalterationCollectionViaGenericalterationitem</summary>
			public static readonly string GenericalterationCollectionViaGenericalterationitem = "GenericalterationCollectionViaGenericalterationitem";
			/// <summary>Member name PosalterationoptionCollectionViaAlterationoption</summary>
			public static readonly string PosalterationoptionCollectionViaAlterationoption = "PosalterationoptionCollectionViaAlterationoption";
			/// <summary>Member name PosproductCollectionViaAlterationoption</summary>
			public static readonly string PosproductCollectionViaAlterationoption = "PosproductCollectionViaAlterationoption";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GenericalterationoptionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GenericalterationoptionEntityBase() :base("GenericalterationoptionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		protected GenericalterationoptionEntityBase(System.Int32 genericalterationoptionId):base("GenericalterationoptionEntity")
		{
			InitClassFetch(genericalterationoptionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GenericalterationoptionEntityBase(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse): base("GenericalterationoptionEntity")
		{
			InitClassFetch(genericalterationoptionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="validator">The custom validator object for this GenericalterationoptionEntity</param>
		protected GenericalterationoptionEntityBase(System.Int32 genericalterationoptionId, IValidator validator):base("GenericalterationoptionEntity")
		{
			InitClassFetch(genericalterationoptionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericalterationoptionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationoptionCollection = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollection = info.GetBoolean("_alwaysFetchAlterationoptionCollection");
			_alreadyFetchedAlterationoptionCollection = info.GetBoolean("_alreadyFetchedAlterationoptionCollection");

			_genericalterationitemCollection = (Obymobi.Data.CollectionClasses.GenericalterationitemCollection)info.GetValue("_genericalterationitemCollection", typeof(Obymobi.Data.CollectionClasses.GenericalterationitemCollection));
			_alwaysFetchGenericalterationitemCollection = info.GetBoolean("_alwaysFetchGenericalterationitemCollection");
			_alreadyFetchedGenericalterationitemCollection = info.GetBoolean("_alreadyFetchedGenericalterationitemCollection");
			_companyCollectionViaAlterationoption = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAlterationoption", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAlterationoption = info.GetBoolean("_alwaysFetchCompanyCollectionViaAlterationoption");
			_alreadyFetchedCompanyCollectionViaAlterationoption = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAlterationoption");

			_genericalterationCollectionViaGenericalterationitem = (Obymobi.Data.CollectionClasses.GenericalterationCollection)info.GetValue("_genericalterationCollectionViaGenericalterationitem", typeof(Obymobi.Data.CollectionClasses.GenericalterationCollection));
			_alwaysFetchGenericalterationCollectionViaGenericalterationitem = info.GetBoolean("_alwaysFetchGenericalterationCollectionViaGenericalterationitem");
			_alreadyFetchedGenericalterationCollectionViaGenericalterationitem = info.GetBoolean("_alreadyFetchedGenericalterationCollectionViaGenericalterationitem");

			_posalterationoptionCollectionViaAlterationoption = (Obymobi.Data.CollectionClasses.PosalterationoptionCollection)info.GetValue("_posalterationoptionCollectionViaAlterationoption", typeof(Obymobi.Data.CollectionClasses.PosalterationoptionCollection));
			_alwaysFetchPosalterationoptionCollectionViaAlterationoption = info.GetBoolean("_alwaysFetchPosalterationoptionCollectionViaAlterationoption");
			_alreadyFetchedPosalterationoptionCollectionViaAlterationoption = info.GetBoolean("_alreadyFetchedPosalterationoptionCollectionViaAlterationoption");

			_posproductCollectionViaAlterationoption = (Obymobi.Data.CollectionClasses.PosproductCollection)info.GetValue("_posproductCollectionViaAlterationoption", typeof(Obymobi.Data.CollectionClasses.PosproductCollection));
			_alwaysFetchPosproductCollectionViaAlterationoption = info.GetBoolean("_alwaysFetchPosproductCollectionViaAlterationoption");
			_alreadyFetchedPosproductCollectionViaAlterationoption = info.GetBoolean("_alreadyFetchedPosproductCollectionViaAlterationoption");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationoptionCollection = (_alterationoptionCollection.Count > 0);
			_alreadyFetchedGenericalterationitemCollection = (_genericalterationitemCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaAlterationoption = (_companyCollectionViaAlterationoption.Count > 0);
			_alreadyFetchedGenericalterationCollectionViaGenericalterationitem = (_genericalterationCollectionViaGenericalterationitem.Count > 0);
			_alreadyFetchedPosalterationoptionCollectionViaAlterationoption = (_posalterationoptionCollectionViaAlterationoption.Count > 0);
			_alreadyFetchedPosproductCollectionViaAlterationoption = (_posproductCollectionViaAlterationoption.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationoptionCollection":
					toReturn.Add(Relations.AlterationoptionEntityUsingGenericalterationoptionId);
					break;
				case "GenericalterationitemCollection":
					toReturn.Add(Relations.GenericalterationitemEntityUsingGenericalterationoptionId);
					break;
				case "CompanyCollectionViaAlterationoption":
					toReturn.Add(Relations.AlterationoptionEntityUsingGenericalterationoptionId, "GenericalterationoptionEntity__", "Alterationoption_", JoinHint.None);
					toReturn.Add(AlterationoptionEntity.Relations.CompanyEntityUsingCompanyId, "Alterationoption_", string.Empty, JoinHint.None);
					break;
				case "GenericalterationCollectionViaGenericalterationitem":
					toReturn.Add(Relations.GenericalterationitemEntityUsingGenericalterationoptionId, "GenericalterationoptionEntity__", "Genericalterationitem_", JoinHint.None);
					toReturn.Add(GenericalterationitemEntity.Relations.GenericalterationEntityUsingGenericalterationId, "Genericalterationitem_", string.Empty, JoinHint.None);
					break;
				case "PosalterationoptionCollectionViaAlterationoption":
					toReturn.Add(Relations.AlterationoptionEntityUsingGenericalterationoptionId, "GenericalterationoptionEntity__", "Alterationoption_", JoinHint.None);
					toReturn.Add(AlterationoptionEntity.Relations.PosalterationoptionEntityUsingPosalterationoptionId, "Alterationoption_", string.Empty, JoinHint.None);
					break;
				case "PosproductCollectionViaAlterationoption":
					toReturn.Add(Relations.AlterationoptionEntityUsingGenericalterationoptionId, "GenericalterationoptionEntity__", "Alterationoption_", JoinHint.None);
					toReturn.Add(AlterationoptionEntity.Relations.PosproductEntityUsingPosproductId, "Alterationoption_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationoptionCollection", (!this.MarkedForDeletion?_alterationoptionCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionCollection", _alwaysFetchAlterationoptionCollection);
			info.AddValue("_alreadyFetchedAlterationoptionCollection", _alreadyFetchedAlterationoptionCollection);
			info.AddValue("_genericalterationitemCollection", (!this.MarkedForDeletion?_genericalterationitemCollection:null));
			info.AddValue("_alwaysFetchGenericalterationitemCollection", _alwaysFetchGenericalterationitemCollection);
			info.AddValue("_alreadyFetchedGenericalterationitemCollection", _alreadyFetchedGenericalterationitemCollection);
			info.AddValue("_companyCollectionViaAlterationoption", (!this.MarkedForDeletion?_companyCollectionViaAlterationoption:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAlterationoption", _alwaysFetchCompanyCollectionViaAlterationoption);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAlterationoption", _alreadyFetchedCompanyCollectionViaAlterationoption);
			info.AddValue("_genericalterationCollectionViaGenericalterationitem", (!this.MarkedForDeletion?_genericalterationCollectionViaGenericalterationitem:null));
			info.AddValue("_alwaysFetchGenericalterationCollectionViaGenericalterationitem", _alwaysFetchGenericalterationCollectionViaGenericalterationitem);
			info.AddValue("_alreadyFetchedGenericalterationCollectionViaGenericalterationitem", _alreadyFetchedGenericalterationCollectionViaGenericalterationitem);
			info.AddValue("_posalterationoptionCollectionViaAlterationoption", (!this.MarkedForDeletion?_posalterationoptionCollectionViaAlterationoption:null));
			info.AddValue("_alwaysFetchPosalterationoptionCollectionViaAlterationoption", _alwaysFetchPosalterationoptionCollectionViaAlterationoption);
			info.AddValue("_alreadyFetchedPosalterationoptionCollectionViaAlterationoption", _alreadyFetchedPosalterationoptionCollectionViaAlterationoption);
			info.AddValue("_posproductCollectionViaAlterationoption", (!this.MarkedForDeletion?_posproductCollectionViaAlterationoption:null));
			info.AddValue("_alwaysFetchPosproductCollectionViaAlterationoption", _alwaysFetchPosproductCollectionViaAlterationoption);
			info.AddValue("_alreadyFetchedPosproductCollectionViaAlterationoption", _alreadyFetchedPosproductCollectionViaAlterationoption);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationoptionCollection":
					_alreadyFetchedAlterationoptionCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionCollection.Add((AlterationoptionEntity)entity);
					}
					break;
				case "GenericalterationitemCollection":
					_alreadyFetchedGenericalterationitemCollection = true;
					if(entity!=null)
					{
						this.GenericalterationitemCollection.Add((GenericalterationitemEntity)entity);
					}
					break;
				case "CompanyCollectionViaAlterationoption":
					_alreadyFetchedCompanyCollectionViaAlterationoption = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAlterationoption.Add((CompanyEntity)entity);
					}
					break;
				case "GenericalterationCollectionViaGenericalterationitem":
					_alreadyFetchedGenericalterationCollectionViaGenericalterationitem = true;
					if(entity!=null)
					{
						this.GenericalterationCollectionViaGenericalterationitem.Add((GenericalterationEntity)entity);
					}
					break;
				case "PosalterationoptionCollectionViaAlterationoption":
					_alreadyFetchedPosalterationoptionCollectionViaAlterationoption = true;
					if(entity!=null)
					{
						this.PosalterationoptionCollectionViaAlterationoption.Add((PosalterationoptionEntity)entity);
					}
					break;
				case "PosproductCollectionViaAlterationoption":
					_alreadyFetchedPosproductCollectionViaAlterationoption = true;
					if(entity!=null)
					{
						this.PosproductCollectionViaAlterationoption.Add((PosproductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationoptionCollection":
					_alterationoptionCollection.Add((AlterationoptionEntity)relatedEntity);
					break;
				case "GenericalterationitemCollection":
					_genericalterationitemCollection.Add((GenericalterationitemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationoptionCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericalterationitemCollection":
					this.PerformRelatedEntityRemoval(_genericalterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationoptionCollection);
			toReturn.Add(_genericalterationitemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationoptionId)
		{
			return FetchUsingPK(genericalterationoptionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(genericalterationoptionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(genericalterationoptionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(genericalterationoptionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GenericalterationoptionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GenericalterationoptionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollection || forceFetch || _alwaysFetchAlterationoptionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollection);
				_alterationoptionCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_alterationoptionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollection = true;
			}
			return _alterationoptionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollection'. These settings will be taken into account
		/// when the property AlterationoptionCollection is requested or GetMultiAlterationoptionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollection.SortClauses=sortClauses;
			_alterationoptionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, _genericalterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, _genericalterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericalterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationitemCollection GetMultiGenericalterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericalterationitemCollection || forceFetch || _alwaysFetchGenericalterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationitemCollection);
				_genericalterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_genericalterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationitemCollection.GetMultiManyToOne(null, this, filter);
				_genericalterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationitemCollection = true;
			}
			return _genericalterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationitemCollection'. These settings will be taken into account
		/// when the property GenericalterationitemCollection is requested or GetMultiGenericalterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationitemCollection.SortClauses=sortClauses;
			_genericalterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAlterationoption(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAlterationoption(forceFetch, _companyCollectionViaAlterationoption.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAlterationoption(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAlterationoption || forceFetch || _alwaysFetchCompanyCollectionViaAlterationoption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAlterationoption);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationoptionFields.GenericalterationoptionId, ComparisonOperator.Equal, this.GenericalterationoptionId, "GenericalterationoptionEntity__"));
				_companyCollectionViaAlterationoption.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAlterationoption.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAlterationoption.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAlterationoption"));
				_companyCollectionViaAlterationoption.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAlterationoption = true;
			}
			return _companyCollectionViaAlterationoption;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAlterationoption'. These settings will be taken into account
		/// when the property CompanyCollectionViaAlterationoption is requested or GetMultiCompanyCollectionViaAlterationoption is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAlterationoption(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAlterationoption.SortClauses=sortClauses;
			_companyCollectionViaAlterationoption.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaGenericalterationitem(bool forceFetch)
		{
			return GetMultiGenericalterationCollectionViaGenericalterationitem(forceFetch, _genericalterationCollectionViaGenericalterationitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaGenericalterationitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericalterationCollectionViaGenericalterationitem || forceFetch || _alwaysFetchGenericalterationCollectionViaGenericalterationitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationCollectionViaGenericalterationitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationoptionFields.GenericalterationoptionId, ComparisonOperator.Equal, this.GenericalterationoptionId, "GenericalterationoptionEntity__"));
				_genericalterationCollectionViaGenericalterationitem.SuppressClearInGetMulti=!forceFetch;
				_genericalterationCollectionViaGenericalterationitem.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationCollectionViaGenericalterationitem.GetMulti(filter, GetRelationsForField("GenericalterationCollectionViaGenericalterationitem"));
				_genericalterationCollectionViaGenericalterationitem.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationCollectionViaGenericalterationitem = true;
			}
			return _genericalterationCollectionViaGenericalterationitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationCollectionViaGenericalterationitem'. These settings will be taken into account
		/// when the property GenericalterationCollectionViaGenericalterationitem is requested or GetMultiGenericalterationCollectionViaGenericalterationitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationCollectionViaGenericalterationitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationCollectionViaGenericalterationitem.SortClauses=sortClauses;
			_genericalterationCollectionViaGenericalterationitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosalterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosalterationoptionCollection GetMultiPosalterationoptionCollectionViaAlterationoption(bool forceFetch)
		{
			return GetMultiPosalterationoptionCollectionViaAlterationoption(forceFetch, _posalterationoptionCollectionViaAlterationoption.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosalterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosalterationoptionCollection GetMultiPosalterationoptionCollectionViaAlterationoption(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosalterationoptionCollectionViaAlterationoption || forceFetch || _alwaysFetchPosalterationoptionCollectionViaAlterationoption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posalterationoptionCollectionViaAlterationoption);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationoptionFields.GenericalterationoptionId, ComparisonOperator.Equal, this.GenericalterationoptionId, "GenericalterationoptionEntity__"));
				_posalterationoptionCollectionViaAlterationoption.SuppressClearInGetMulti=!forceFetch;
				_posalterationoptionCollectionViaAlterationoption.EntityFactoryToUse = entityFactoryToUse;
				_posalterationoptionCollectionViaAlterationoption.GetMulti(filter, GetRelationsForField("PosalterationoptionCollectionViaAlterationoption"));
				_posalterationoptionCollectionViaAlterationoption.SuppressClearInGetMulti=false;
				_alreadyFetchedPosalterationoptionCollectionViaAlterationoption = true;
			}
			return _posalterationoptionCollectionViaAlterationoption;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosalterationoptionCollectionViaAlterationoption'. These settings will be taken into account
		/// when the property PosalterationoptionCollectionViaAlterationoption is requested or GetMultiPosalterationoptionCollectionViaAlterationoption is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosalterationoptionCollectionViaAlterationoption(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posalterationoptionCollectionViaAlterationoption.SortClauses=sortClauses;
			_posalterationoptionCollectionViaAlterationoption.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosproductCollection GetMultiPosproductCollectionViaAlterationoption(bool forceFetch)
		{
			return GetMultiPosproductCollectionViaAlterationoption(forceFetch, _posproductCollectionViaAlterationoption.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosproductCollection GetMultiPosproductCollectionViaAlterationoption(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosproductCollectionViaAlterationoption || forceFetch || _alwaysFetchPosproductCollectionViaAlterationoption) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posproductCollectionViaAlterationoption);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericalterationoptionFields.GenericalterationoptionId, ComparisonOperator.Equal, this.GenericalterationoptionId, "GenericalterationoptionEntity__"));
				_posproductCollectionViaAlterationoption.SuppressClearInGetMulti=!forceFetch;
				_posproductCollectionViaAlterationoption.EntityFactoryToUse = entityFactoryToUse;
				_posproductCollectionViaAlterationoption.GetMulti(filter, GetRelationsForField("PosproductCollectionViaAlterationoption"));
				_posproductCollectionViaAlterationoption.SuppressClearInGetMulti=false;
				_alreadyFetchedPosproductCollectionViaAlterationoption = true;
			}
			return _posproductCollectionViaAlterationoption;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosproductCollectionViaAlterationoption'. These settings will be taken into account
		/// when the property PosproductCollectionViaAlterationoption is requested or GetMultiPosproductCollectionViaAlterationoption is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosproductCollectionViaAlterationoption(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posproductCollectionViaAlterationoption.SortClauses=sortClauses;
			_posproductCollectionViaAlterationoption.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationoptionCollection", _alterationoptionCollection);
			toReturn.Add("GenericalterationitemCollection", _genericalterationitemCollection);
			toReturn.Add("CompanyCollectionViaAlterationoption", _companyCollectionViaAlterationoption);
			toReturn.Add("GenericalterationCollectionViaGenericalterationitem", _genericalterationCollectionViaGenericalterationitem);
			toReturn.Add("PosalterationoptionCollectionViaAlterationoption", _posalterationoptionCollectionViaAlterationoption);
			toReturn.Add("PosproductCollectionViaAlterationoption", _posproductCollectionViaAlterationoption);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="validator">The validator object for this GenericalterationoptionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 genericalterationoptionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(genericalterationoptionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationoptionCollection = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_alterationoptionCollection.SetContainingEntityInfo(this, "GenericalterationoptionEntity");

			_genericalterationitemCollection = new Obymobi.Data.CollectionClasses.GenericalterationitemCollection();
			_genericalterationitemCollection.SetContainingEntityInfo(this, "GenericalterationoptionEntity");
			_companyCollectionViaAlterationoption = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_genericalterationCollectionViaGenericalterationitem = new Obymobi.Data.CollectionClasses.GenericalterationCollection();
			_posalterationoptionCollectionViaAlterationoption = new Obymobi.Data.CollectionClasses.PosalterationoptionCollection();
			_posproductCollectionViaAlterationoption = new Obymobi.Data.CollectionClasses.PosproductCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationoptionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceAddition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GenericalterationoptionFieldIndex.GenericalterationoptionId].ForcedCurrentValueWrite(genericalterationoptionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericalterationoptionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GenericalterationoptionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GenericalterationoptionRelations Relations
		{
			get	{ return new GenericalterationoptionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionCollection")[0], (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationitemCollection(), (IEntityRelation)GetRelationsForField("GenericalterationitemCollection")[0], (int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.GenericalterationitemEntity, 0, null, null, null, "GenericalterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAlterationoption
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationoptionEntityUsingGenericalterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Alterationoption_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAlterationoption"), "CompanyCollectionViaAlterationoption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationCollectionViaGenericalterationitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.GenericalterationitemEntityUsingGenericalterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Genericalterationitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, GetRelationsForField("GenericalterationCollectionViaGenericalterationitem"), "GenericalterationCollectionViaGenericalterationitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posalterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosalterationoptionCollectionViaAlterationoption
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationoptionEntityUsingGenericalterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Alterationoption_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosalterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.PosalterationoptionEntity, 0, null, null, GetRelationsForField("PosalterationoptionCollectionViaAlterationoption"), "PosalterationoptionCollectionViaAlterationoption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosproductCollectionViaAlterationoption
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationoptionEntityUsingGenericalterationoptionId;
				intermediateRelation.SetAliases(string.Empty, "Alterationoption_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericalterationoptionEntity, (int)Obymobi.Data.EntityType.PosproductEntity, 0, null, null, GetRelationsForField("PosproductCollectionViaAlterationoption"), "PosproductCollectionViaAlterationoption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GenericalterationoptionId property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."GenericalterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GenericalterationoptionId
		{
			get { return (System.Int32)GetValue((int)GenericalterationoptionFieldIndex.GenericalterationoptionId, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.GenericalterationoptionId, value, true); }
		}

		/// <summary> The Name property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GenericalterationoptionFieldIndex.Name, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.Name, value, true); }
		}

		/// <summary> The GenericalterationoptionType property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."GenericalterationoptionType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationoptionType
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericalterationoptionFieldIndex.GenericalterationoptionType, false); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.GenericalterationoptionType, value, true); }
		}

		/// <summary> The PriceIn property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceIn
		{
			get { return (System.Decimal)GetValue((int)GenericalterationoptionFieldIndex.PriceIn, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.PriceIn, value, true); }
		}

		/// <summary> The PriceAddition property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."PriceAddition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> PriceAddition
		{
			get { return (Nullable<System.Decimal>)GetValue((int)GenericalterationoptionFieldIndex.PriceAddition, false); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.PriceAddition, value, true); }
		}

		/// <summary> The Description property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)GenericalterationoptionFieldIndex.Description, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.Description, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationoptionFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)GenericalterationoptionFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationoptionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Genericalterationoption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericalterationoption"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericalterationoptionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GenericalterationoptionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollection
		{
			get	{ return GetMultiAlterationoptionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollection. When set to true, AlterationoptionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollection
		{
			get	{ return _alwaysFetchAlterationoptionCollection; }
			set	{ _alwaysFetchAlterationoptionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollection already has been fetched. Setting this property to false when AlterationoptionCollection has been fetched
		/// will clear the AlterationoptionCollection collection well. Setting this property to true while AlterationoptionCollection hasn't been fetched disables lazy loading for AlterationoptionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollection
		{
			get { return _alreadyFetchedAlterationoptionCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollection && !value && (_alterationoptionCollection != null))
				{
					_alterationoptionCollection.Clear();
				}
				_alreadyFetchedAlterationoptionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericalterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationitemCollection GenericalterationitemCollection
		{
			get	{ return GetMultiGenericalterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationitemCollection. When set to true, GenericalterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericalterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationitemCollection
		{
			get	{ return _alwaysFetchGenericalterationitemCollection; }
			set	{ _alwaysFetchGenericalterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationitemCollection already has been fetched. Setting this property to false when GenericalterationitemCollection has been fetched
		/// will clear the GenericalterationitemCollection collection well. Setting this property to true while GenericalterationitemCollection hasn't been fetched disables lazy loading for GenericalterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationitemCollection
		{
			get { return _alreadyFetchedGenericalterationitemCollection;}
			set 
			{
				if(_alreadyFetchedGenericalterationitemCollection && !value && (_genericalterationitemCollection != null))
				{
					_genericalterationitemCollection.Clear();
				}
				_alreadyFetchedGenericalterationitemCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAlterationoption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAlterationoption
		{
			get { return GetMultiCompanyCollectionViaAlterationoption(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAlterationoption. When set to true, CompanyCollectionViaAlterationoption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAlterationoption is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAlterationoption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAlterationoption
		{
			get	{ return _alwaysFetchCompanyCollectionViaAlterationoption; }
			set	{ _alwaysFetchCompanyCollectionViaAlterationoption = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAlterationoption already has been fetched. Setting this property to false when CompanyCollectionViaAlterationoption has been fetched
		/// will clear the CompanyCollectionViaAlterationoption collection well. Setting this property to true while CompanyCollectionViaAlterationoption hasn't been fetched disables lazy loading for CompanyCollectionViaAlterationoption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAlterationoption
		{
			get { return _alreadyFetchedCompanyCollectionViaAlterationoption;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAlterationoption && !value && (_companyCollectionViaAlterationoption != null))
				{
					_companyCollectionViaAlterationoption.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAlterationoption = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationCollectionViaGenericalterationitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationCollection GenericalterationCollectionViaGenericalterationitem
		{
			get { return GetMultiGenericalterationCollectionViaGenericalterationitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationCollectionViaGenericalterationitem. When set to true, GenericalterationCollectionViaGenericalterationitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationCollectionViaGenericalterationitem is accessed. You can always execute a forced fetch by calling GetMultiGenericalterationCollectionViaGenericalterationitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationCollectionViaGenericalterationitem
		{
			get	{ return _alwaysFetchGenericalterationCollectionViaGenericalterationitem; }
			set	{ _alwaysFetchGenericalterationCollectionViaGenericalterationitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationCollectionViaGenericalterationitem already has been fetched. Setting this property to false when GenericalterationCollectionViaGenericalterationitem has been fetched
		/// will clear the GenericalterationCollectionViaGenericalterationitem collection well. Setting this property to true while GenericalterationCollectionViaGenericalterationitem hasn't been fetched disables lazy loading for GenericalterationCollectionViaGenericalterationitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationCollectionViaGenericalterationitem
		{
			get { return _alreadyFetchedGenericalterationCollectionViaGenericalterationitem;}
			set 
			{
				if(_alreadyFetchedGenericalterationCollectionViaGenericalterationitem && !value && (_genericalterationCollectionViaGenericalterationitem != null))
				{
					_genericalterationCollectionViaGenericalterationitem.Clear();
				}
				_alreadyFetchedGenericalterationCollectionViaGenericalterationitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosalterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosalterationoptionCollectionViaAlterationoption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosalterationoptionCollection PosalterationoptionCollectionViaAlterationoption
		{
			get { return GetMultiPosalterationoptionCollectionViaAlterationoption(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosalterationoptionCollectionViaAlterationoption. When set to true, PosalterationoptionCollectionViaAlterationoption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosalterationoptionCollectionViaAlterationoption is accessed. You can always execute a forced fetch by calling GetMultiPosalterationoptionCollectionViaAlterationoption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosalterationoptionCollectionViaAlterationoption
		{
			get	{ return _alwaysFetchPosalterationoptionCollectionViaAlterationoption; }
			set	{ _alwaysFetchPosalterationoptionCollectionViaAlterationoption = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosalterationoptionCollectionViaAlterationoption already has been fetched. Setting this property to false when PosalterationoptionCollectionViaAlterationoption has been fetched
		/// will clear the PosalterationoptionCollectionViaAlterationoption collection well. Setting this property to true while PosalterationoptionCollectionViaAlterationoption hasn't been fetched disables lazy loading for PosalterationoptionCollectionViaAlterationoption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosalterationoptionCollectionViaAlterationoption
		{
			get { return _alreadyFetchedPosalterationoptionCollectionViaAlterationoption;}
			set 
			{
				if(_alreadyFetchedPosalterationoptionCollectionViaAlterationoption && !value && (_posalterationoptionCollectionViaAlterationoption != null))
				{
					_posalterationoptionCollectionViaAlterationoption.Clear();
				}
				_alreadyFetchedPosalterationoptionCollectionViaAlterationoption = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosproductCollectionViaAlterationoption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosproductCollection PosproductCollectionViaAlterationoption
		{
			get { return GetMultiPosproductCollectionViaAlterationoption(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosproductCollectionViaAlterationoption. When set to true, PosproductCollectionViaAlterationoption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosproductCollectionViaAlterationoption is accessed. You can always execute a forced fetch by calling GetMultiPosproductCollectionViaAlterationoption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosproductCollectionViaAlterationoption
		{
			get	{ return _alwaysFetchPosproductCollectionViaAlterationoption; }
			set	{ _alwaysFetchPosproductCollectionViaAlterationoption = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosproductCollectionViaAlterationoption already has been fetched. Setting this property to false when PosproductCollectionViaAlterationoption has been fetched
		/// will clear the PosproductCollectionViaAlterationoption collection well. Setting this property to true while PosproductCollectionViaAlterationoption hasn't been fetched disables lazy loading for PosproductCollectionViaAlterationoption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosproductCollectionViaAlterationoption
		{
			get { return _alreadyFetchedPosproductCollectionViaAlterationoption;}
			set 
			{
				if(_alreadyFetchedPosproductCollectionViaAlterationoption && !value && (_posproductCollectionViaAlterationoption != null))
				{
					_posproductCollectionViaAlterationoption.Clear();
				}
				_alreadyFetchedPosproductCollectionViaAlterationoption = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GenericalterationoptionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
