﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Order'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	[DebuggerDisplay("OrderId={OrderId}")]
	public abstract partial class OrderEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OrderEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection	_analyticsProcessingTaskCollection;
		private bool	_alwaysFetchAnalyticsProcessingTaskCollection, _alreadyFetchedAnalyticsProcessingTaskCollection;
		private Obymobi.Data.CollectionClasses.ExternalSystemLogCollection	_externalSystemLogCollection;
		private bool	_alwaysFetchExternalSystemLogCollection, _alreadyFetchedExternalSystemLogCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.OrderitemCollection	_orderitemCollection;
		private bool	_alwaysFetchOrderitemCollection, _alreadyFetchedOrderitemCollection;
		private Obymobi.Data.CollectionClasses.OrderNotificationLogCollection	_orderNotificationLogCollection;
		private bool	_alwaysFetchOrderNotificationLogCollection, _alreadyFetchedOrderNotificationLogCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection	_orderRoutestephandlerCollection;
		private bool	_alwaysFetchOrderRoutestephandlerCollection, _alreadyFetchedOrderRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection	_orderRoutestephandlerHistoryCollection;
		private bool	_alwaysFetchOrderRoutestephandlerHistoryCollection, _alreadyFetchedOrderRoutestephandlerHistoryCollection;
		private Obymobi.Data.CollectionClasses.PaymentTransactionCollection	_paymentTransactionCollection;
		private bool	_alwaysFetchPaymentTransactionCollection, _alreadyFetchedPaymentTransactionCollection;
		private Obymobi.Data.CollectionClasses.ReceiptCollection	_receiptCollection;
		private bool	_alwaysFetchReceiptCollection, _alreadyFetchedReceiptCollection;
		private Obymobi.Data.CollectionClasses.TerminalLogCollection	_terminalLogCollection;
		private bool	_alwaysFetchTerminalLogCollection, _alreadyFetchedTerminalLogCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessage;
		private bool	_alwaysFetchCategoryCollectionViaMessage, _alreadyFetchedCategoryCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaOrderitem;
		private bool	_alwaysFetchCategoryCollectionViaOrderitem, _alreadyFetchedCategoryCollectionViaOrderitem;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaOrder;
		private bool	_alwaysFetchClientCollectionViaOrder, _alreadyFetchedClientCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaOrder;
		private bool	_alwaysFetchCompanyCollectionViaOrder, _alreadyFetchedCompanyCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.CurrencyCollection _currencyCollectionViaOrder;
		private bool	_alwaysFetchCurrencyCollectionViaOrder, _alreadyFetchedCurrencyCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaOrder;
		private bool	_alwaysFetchCustomerCollectionViaOrder, _alreadyFetchedCustomerCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessage;
		private bool	_alwaysFetchDeliverypointCollectionViaMessage, _alreadyFetchedDeliverypointCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaOrder;
		private bool	_alwaysFetchDeliverypointCollectionViaOrder, _alreadyFetchedDeliverypointCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessage;
		private bool	_alwaysFetchMediaCollectionViaMessage, _alreadyFetchedMediaCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaOrderitem;
		private bool	_alwaysFetchProductCollectionViaOrderitem, _alreadyFetchedProductCollectionViaOrderitem;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;
		private Obymobi.Data.CollectionClasses.TerminalLogFileCollection _terminalLogFileCollectionViaTerminalLog;
		private bool	_alwaysFetchTerminalLogFileCollectionViaTerminalLog, _alreadyFetchedTerminalLogFileCollectionViaTerminalLog;
		private CheckoutMethodEntity _checkoutMethodEntity;
		private bool	_alwaysFetchCheckoutMethodEntity, _alreadyFetchedCheckoutMethodEntity, _checkoutMethodEntityReturnsNewIfNotFound;
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private CurrencyEntity _currencyEntity;
		private bool	_alwaysFetchCurrencyEntity, _alreadyFetchedCurrencyEntity, _currencyEntityReturnsNewIfNotFound;
		private CustomerEntity _customerEntity;
		private bool	_alwaysFetchCustomerEntity, _alreadyFetchedCustomerEntity, _customerEntityReturnsNewIfNotFound;
		private DeliveryInformationEntity _deliveryInformationEntity;
		private bool	_alwaysFetchDeliveryInformationEntity, _alreadyFetchedDeliveryInformationEntity, _deliveryInformationEntityReturnsNewIfNotFound;
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private DeliverypointEntity _deliverypointEntity_;
		private bool	_alwaysFetchDeliverypointEntity_, _alreadyFetchedDeliverypointEntity_, _deliverypointEntity_ReturnsNewIfNotFound;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private OutletEntity _outletEntity;
		private bool	_alwaysFetchOutletEntity, _alreadyFetchedOutletEntity, _outletEntityReturnsNewIfNotFound;
		private ServiceMethodEntity _serviceMethodEntity;
		private bool	_alwaysFetchServiceMethodEntity, _alreadyFetchedServiceMethodEntity, _serviceMethodEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CheckoutMethodEntity</summary>
			public static readonly string CheckoutMethodEntity = "CheckoutMethodEntity";
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name CurrencyEntity</summary>
			public static readonly string CurrencyEntity = "CurrencyEntity";
			/// <summary>Member name CustomerEntity</summary>
			public static readonly string CustomerEntity = "CustomerEntity";
			/// <summary>Member name DeliveryInformationEntity</summary>
			public static readonly string DeliveryInformationEntity = "DeliveryInformationEntity";
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name DeliverypointEntity_</summary>
			public static readonly string DeliverypointEntity_ = "DeliverypointEntity_";
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name OutletEntity</summary>
			public static readonly string OutletEntity = "OutletEntity";
			/// <summary>Member name ServiceMethodEntity</summary>
			public static readonly string ServiceMethodEntity = "ServiceMethodEntity";
			/// <summary>Member name AnalyticsProcessingTaskCollection</summary>
			public static readonly string AnalyticsProcessingTaskCollection = "AnalyticsProcessingTaskCollection";
			/// <summary>Member name ExternalSystemLogCollection</summary>
			public static readonly string ExternalSystemLogCollection = "ExternalSystemLogCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name OrderNotificationLogCollection</summary>
			public static readonly string OrderNotificationLogCollection = "OrderNotificationLogCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name PaymentTransactionCollection</summary>
			public static readonly string PaymentTransactionCollection = "PaymentTransactionCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
			/// <summary>Member name TerminalLogCollection</summary>
			public static readonly string TerminalLogCollection = "TerminalLogCollection";
			/// <summary>Member name CategoryCollectionViaMessage</summary>
			public static readonly string CategoryCollectionViaMessage = "CategoryCollectionViaMessage";
			/// <summary>Member name CategoryCollectionViaOrderitem</summary>
			public static readonly string CategoryCollectionViaOrderitem = "CategoryCollectionViaOrderitem";
			/// <summary>Member name ClientCollectionViaOrder</summary>
			public static readonly string ClientCollectionViaOrder = "ClientCollectionViaOrder";
			/// <summary>Member name CompanyCollectionViaOrder</summary>
			public static readonly string CompanyCollectionViaOrder = "CompanyCollectionViaOrder";
			/// <summary>Member name CurrencyCollectionViaOrder</summary>
			public static readonly string CurrencyCollectionViaOrder = "CurrencyCollectionViaOrder";
			/// <summary>Member name CustomerCollectionViaOrder</summary>
			public static readonly string CustomerCollectionViaOrder = "CustomerCollectionViaOrder";
			/// <summary>Member name DeliverypointCollectionViaMessage</summary>
			public static readonly string DeliverypointCollectionViaMessage = "DeliverypointCollectionViaMessage";
			/// <summary>Member name DeliverypointCollectionViaOrder</summary>
			public static readonly string DeliverypointCollectionViaOrder = "DeliverypointCollectionViaOrder";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name MediaCollectionViaMessage</summary>
			public static readonly string MediaCollectionViaMessage = "MediaCollectionViaMessage";
			/// <summary>Member name ProductCollectionViaOrderitem</summary>
			public static readonly string ProductCollectionViaOrderitem = "ProductCollectionViaOrderitem";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandler</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandler = "SupportpoolCollectionViaOrderRoutestephandler";
			/// <summary>Member name SupportpoolCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string SupportpoolCollectionViaOrderRoutestephandlerHistory = "SupportpoolCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler = "TerminalCollectionViaOrderRoutestephandler";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler_ = "TerminalCollectionViaOrderRoutestephandler_";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory = "TerminalCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory_ = "TerminalCollectionViaOrderRoutestephandlerHistory_";
			/// <summary>Member name TerminalLogFileCollectionViaTerminalLog</summary>
			public static readonly string TerminalLogFileCollectionViaTerminalLog = "TerminalLogFileCollectionViaTerminalLog";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OrderEntityBase() :base("OrderEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		protected OrderEntityBase(System.Int32 orderId):base("OrderEntity")
		{
			InitClassFetch(orderId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OrderEntityBase(System.Int32 orderId, IPrefetchPath prefetchPathToUse): base("OrderEntity")
		{
			InitClassFetch(orderId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The custom validator object for this OrderEntity</param>
		protected OrderEntityBase(System.Int32 orderId, IValidator validator):base("OrderEntity")
		{
			InitClassFetch(orderId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_analyticsProcessingTaskCollection = (Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection)info.GetValue("_analyticsProcessingTaskCollection", typeof(Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection));
			_alwaysFetchAnalyticsProcessingTaskCollection = info.GetBoolean("_alwaysFetchAnalyticsProcessingTaskCollection");
			_alreadyFetchedAnalyticsProcessingTaskCollection = info.GetBoolean("_alreadyFetchedAnalyticsProcessingTaskCollection");

			_externalSystemLogCollection = (Obymobi.Data.CollectionClasses.ExternalSystemLogCollection)info.GetValue("_externalSystemLogCollection", typeof(Obymobi.Data.CollectionClasses.ExternalSystemLogCollection));
			_alwaysFetchExternalSystemLogCollection = info.GetBoolean("_alwaysFetchExternalSystemLogCollection");
			_alreadyFetchedExternalSystemLogCollection = info.GetBoolean("_alreadyFetchedExternalSystemLogCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_orderitemCollection = (Obymobi.Data.CollectionClasses.OrderitemCollection)info.GetValue("_orderitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemCollection));
			_alwaysFetchOrderitemCollection = info.GetBoolean("_alwaysFetchOrderitemCollection");
			_alreadyFetchedOrderitemCollection = info.GetBoolean("_alreadyFetchedOrderitemCollection");

			_orderNotificationLogCollection = (Obymobi.Data.CollectionClasses.OrderNotificationLogCollection)info.GetValue("_orderNotificationLogCollection", typeof(Obymobi.Data.CollectionClasses.OrderNotificationLogCollection));
			_alwaysFetchOrderNotificationLogCollection = info.GetBoolean("_alwaysFetchOrderNotificationLogCollection");
			_alreadyFetchedOrderNotificationLogCollection = info.GetBoolean("_alreadyFetchedOrderNotificationLogCollection");

			_orderRoutestephandlerCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection)info.GetValue("_orderRoutestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection));
			_alwaysFetchOrderRoutestephandlerCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerCollection");
			_alreadyFetchedOrderRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerCollection");

			_orderRoutestephandlerHistoryCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection)info.GetValue("_orderRoutestephandlerHistoryCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection));
			_alwaysFetchOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerHistoryCollection");
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerHistoryCollection");

			_paymentTransactionCollection = (Obymobi.Data.CollectionClasses.PaymentTransactionCollection)info.GetValue("_paymentTransactionCollection", typeof(Obymobi.Data.CollectionClasses.PaymentTransactionCollection));
			_alwaysFetchPaymentTransactionCollection = info.GetBoolean("_alwaysFetchPaymentTransactionCollection");
			_alreadyFetchedPaymentTransactionCollection = info.GetBoolean("_alreadyFetchedPaymentTransactionCollection");

			_receiptCollection = (Obymobi.Data.CollectionClasses.ReceiptCollection)info.GetValue("_receiptCollection", typeof(Obymobi.Data.CollectionClasses.ReceiptCollection));
			_alwaysFetchReceiptCollection = info.GetBoolean("_alwaysFetchReceiptCollection");
			_alreadyFetchedReceiptCollection = info.GetBoolean("_alreadyFetchedReceiptCollection");

			_terminalLogCollection = (Obymobi.Data.CollectionClasses.TerminalLogCollection)info.GetValue("_terminalLogCollection", typeof(Obymobi.Data.CollectionClasses.TerminalLogCollection));
			_alwaysFetchTerminalLogCollection = info.GetBoolean("_alwaysFetchTerminalLogCollection");
			_alreadyFetchedTerminalLogCollection = info.GetBoolean("_alreadyFetchedTerminalLogCollection");
			_categoryCollectionViaMessage = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessage = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessage");
			_alreadyFetchedCategoryCollectionViaMessage = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessage");

			_categoryCollectionViaOrderitem = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaOrderitem", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaOrderitem = info.GetBoolean("_alwaysFetchCategoryCollectionViaOrderitem");
			_alreadyFetchedCategoryCollectionViaOrderitem = info.GetBoolean("_alreadyFetchedCategoryCollectionViaOrderitem");

			_clientCollectionViaOrder = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaOrder = info.GetBoolean("_alwaysFetchClientCollectionViaOrder");
			_alreadyFetchedClientCollectionViaOrder = info.GetBoolean("_alreadyFetchedClientCollectionViaOrder");

			_companyCollectionViaOrder = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaOrder = info.GetBoolean("_alwaysFetchCompanyCollectionViaOrder");
			_alreadyFetchedCompanyCollectionViaOrder = info.GetBoolean("_alreadyFetchedCompanyCollectionViaOrder");

			_currencyCollectionViaOrder = (Obymobi.Data.CollectionClasses.CurrencyCollection)info.GetValue("_currencyCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.CurrencyCollection));
			_alwaysFetchCurrencyCollectionViaOrder = info.GetBoolean("_alwaysFetchCurrencyCollectionViaOrder");
			_alreadyFetchedCurrencyCollectionViaOrder = info.GetBoolean("_alreadyFetchedCurrencyCollectionViaOrder");

			_customerCollectionViaOrder = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaOrder = info.GetBoolean("_alwaysFetchCustomerCollectionViaOrder");
			_alreadyFetchedCustomerCollectionViaOrder = info.GetBoolean("_alreadyFetchedCustomerCollectionViaOrder");

			_deliverypointCollectionViaMessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessage");
			_alreadyFetchedDeliverypointCollectionViaMessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessage");

			_deliverypointCollectionViaOrder = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaOrder = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaOrder");
			_alreadyFetchedDeliverypointCollectionViaOrder = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaOrder");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_mediaCollectionViaMessage = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessage = info.GetBoolean("_alwaysFetchMediaCollectionViaMessage");
			_alreadyFetchedMediaCollectionViaMessage = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessage");

			_productCollectionViaOrderitem = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaOrderitem", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaOrderitem = info.GetBoolean("_alwaysFetchProductCollectionViaOrderitem");
			_alreadyFetchedProductCollectionViaOrderitem = info.GetBoolean("_alreadyFetchedProductCollectionViaOrderitem");

			_supportpoolCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler");

			_supportpoolCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory");

			_terminalCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler");

			_terminalCollectionViaOrderRoutestephandler_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_");

			_terminalCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory");

			_terminalCollectionViaOrderRoutestephandlerHistory_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_");

			_terminalLogFileCollectionViaTerminalLog = (Obymobi.Data.CollectionClasses.TerminalLogFileCollection)info.GetValue("_terminalLogFileCollectionViaTerminalLog", typeof(Obymobi.Data.CollectionClasses.TerminalLogFileCollection));
			_alwaysFetchTerminalLogFileCollectionViaTerminalLog = info.GetBoolean("_alwaysFetchTerminalLogFileCollectionViaTerminalLog");
			_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = info.GetBoolean("_alreadyFetchedTerminalLogFileCollectionViaTerminalLog");
			_checkoutMethodEntity = (CheckoutMethodEntity)info.GetValue("_checkoutMethodEntity", typeof(CheckoutMethodEntity));
			if(_checkoutMethodEntity!=null)
			{
				_checkoutMethodEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_checkoutMethodEntityReturnsNewIfNotFound = info.GetBoolean("_checkoutMethodEntityReturnsNewIfNotFound");
			_alwaysFetchCheckoutMethodEntity = info.GetBoolean("_alwaysFetchCheckoutMethodEntity");
			_alreadyFetchedCheckoutMethodEntity = info.GetBoolean("_alreadyFetchedCheckoutMethodEntity");

			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_currencyEntity = (CurrencyEntity)info.GetValue("_currencyEntity", typeof(CurrencyEntity));
			if(_currencyEntity!=null)
			{
				_currencyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_currencyEntityReturnsNewIfNotFound = info.GetBoolean("_currencyEntityReturnsNewIfNotFound");
			_alwaysFetchCurrencyEntity = info.GetBoolean("_alwaysFetchCurrencyEntity");
			_alreadyFetchedCurrencyEntity = info.GetBoolean("_alreadyFetchedCurrencyEntity");

			_customerEntity = (CustomerEntity)info.GetValue("_customerEntity", typeof(CustomerEntity));
			if(_customerEntity!=null)
			{
				_customerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerEntityReturnsNewIfNotFound = info.GetBoolean("_customerEntityReturnsNewIfNotFound");
			_alwaysFetchCustomerEntity = info.GetBoolean("_alwaysFetchCustomerEntity");
			_alreadyFetchedCustomerEntity = info.GetBoolean("_alreadyFetchedCustomerEntity");

			_deliveryInformationEntity = (DeliveryInformationEntity)info.GetValue("_deliveryInformationEntity", typeof(DeliveryInformationEntity));
			if(_deliveryInformationEntity!=null)
			{
				_deliveryInformationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliveryInformationEntityReturnsNewIfNotFound = info.GetBoolean("_deliveryInformationEntityReturnsNewIfNotFound");
			_alwaysFetchDeliveryInformationEntity = info.GetBoolean("_alwaysFetchDeliveryInformationEntity");
			_alreadyFetchedDeliveryInformationEntity = info.GetBoolean("_alreadyFetchedDeliveryInformationEntity");

			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_deliverypointEntity_ = (DeliverypointEntity)info.GetValue("_deliverypointEntity_", typeof(DeliverypointEntity));
			if(_deliverypointEntity_!=null)
			{
				_deliverypointEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntity_ReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntity_ReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity_ = info.GetBoolean("_alwaysFetchDeliverypointEntity_");
			_alreadyFetchedDeliverypointEntity_ = info.GetBoolean("_alreadyFetchedDeliverypointEntity_");

			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_outletEntity = (OutletEntity)info.GetValue("_outletEntity", typeof(OutletEntity));
			if(_outletEntity!=null)
			{
				_outletEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletEntityReturnsNewIfNotFound = info.GetBoolean("_outletEntityReturnsNewIfNotFound");
			_alwaysFetchOutletEntity = info.GetBoolean("_alwaysFetchOutletEntity");
			_alreadyFetchedOutletEntity = info.GetBoolean("_alreadyFetchedOutletEntity");

			_serviceMethodEntity = (ServiceMethodEntity)info.GetValue("_serviceMethodEntity", typeof(ServiceMethodEntity));
			if(_serviceMethodEntity!=null)
			{
				_serviceMethodEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_serviceMethodEntityReturnsNewIfNotFound = info.GetBoolean("_serviceMethodEntityReturnsNewIfNotFound");
			_alwaysFetchServiceMethodEntity = info.GetBoolean("_alwaysFetchServiceMethodEntity");
			_alreadyFetchedServiceMethodEntity = info.GetBoolean("_alreadyFetchedServiceMethodEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderFieldIndex)fieldIndex)
			{
				case OrderFieldIndex.CustomerId:
					DesetupSyncCustomerEntity(true, false);
					_alreadyFetchedCustomerEntity = false;
					break;
				case OrderFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				case OrderFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case OrderFieldIndex.CurrencyId:
					DesetupSyncCurrencyEntity(true, false);
					_alreadyFetchedCurrencyEntity = false;
					break;
				case OrderFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				case OrderFieldIndex.MasterOrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case OrderFieldIndex.ServiceMethodId:
					DesetupSyncServiceMethodEntity(true, false);
					_alreadyFetchedServiceMethodEntity = false;
					break;
				case OrderFieldIndex.CheckoutMethodId:
					DesetupSyncCheckoutMethodEntity(true, false);
					_alreadyFetchedCheckoutMethodEntity = false;
					break;
				case OrderFieldIndex.DeliveryInformationId:
					DesetupSyncDeliveryInformationEntity(true, false);
					_alreadyFetchedDeliveryInformationEntity = false;
					break;
				case OrderFieldIndex.OutletId:
					DesetupSyncOutletEntity(true, false);
					_alreadyFetchedOutletEntity = false;
					break;
				case OrderFieldIndex.ChargeToDeliverypointId:
					DesetupSyncDeliverypointEntity_(true, false);
					_alreadyFetchedDeliverypointEntity_ = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAnalyticsProcessingTaskCollection = (_analyticsProcessingTaskCollection.Count > 0);
			_alreadyFetchedExternalSystemLogCollection = (_externalSystemLogCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedOrderitemCollection = (_orderitemCollection.Count > 0);
			_alreadyFetchedOrderNotificationLogCollection = (_orderNotificationLogCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerCollection = (_orderRoutestephandlerCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = (_orderRoutestephandlerHistoryCollection.Count > 0);
			_alreadyFetchedPaymentTransactionCollection = (_paymentTransactionCollection.Count > 0);
			_alreadyFetchedReceiptCollection = (_receiptCollection.Count > 0);
			_alreadyFetchedTerminalLogCollection = (_terminalLogCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessage = (_categoryCollectionViaMessage.Count > 0);
			_alreadyFetchedCategoryCollectionViaOrderitem = (_categoryCollectionViaOrderitem.Count > 0);
			_alreadyFetchedClientCollectionViaOrder = (_clientCollectionViaOrder.Count > 0);
			_alreadyFetchedCompanyCollectionViaOrder = (_companyCollectionViaOrder.Count > 0);
			_alreadyFetchedCurrencyCollectionViaOrder = (_currencyCollectionViaOrder.Count > 0);
			_alreadyFetchedCustomerCollectionViaOrder = (_customerCollectionViaOrder.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessage = (_deliverypointCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaOrder = (_deliverypointCollectionViaOrder.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedMediaCollectionViaMessage = (_mediaCollectionViaMessage.Count > 0);
			_alreadyFetchedProductCollectionViaOrderitem = (_productCollectionViaOrderitem.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = (_supportpoolCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = (_supportpoolCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = (_terminalCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = (_terminalCollectionViaOrderRoutestephandler_.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = (_terminalCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = (_terminalCollectionViaOrderRoutestephandlerHistory_.Count > 0);
			_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = (_terminalLogFileCollectionViaTerminalLog.Count > 0);
			_alreadyFetchedCheckoutMethodEntity = (_checkoutMethodEntity != null);
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedCurrencyEntity = (_currencyEntity != null);
			_alreadyFetchedCustomerEntity = (_customerEntity != null);
			_alreadyFetchedDeliveryInformationEntity = (_deliveryInformationEntity != null);
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedDeliverypointEntity_ = (_deliverypointEntity_ != null);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedOutletEntity = (_outletEntity != null);
			_alreadyFetchedServiceMethodEntity = (_serviceMethodEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CheckoutMethodEntity":
					toReturn.Add(Relations.CheckoutMethodEntityUsingCheckoutMethodId);
					break;
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "CurrencyEntity":
					toReturn.Add(Relations.CurrencyEntityUsingCurrencyId);
					break;
				case "CustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingCustomerId);
					break;
				case "DeliveryInformationEntity":
					toReturn.Add(Relations.DeliveryInformationEntityUsingDeliveryInformationId);
					break;
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "DeliverypointEntity_":
					toReturn.Add(Relations.DeliverypointEntityUsingChargeToDeliverypointId);
					break;
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderIdMasterOrderId);
					break;
				case "OutletEntity":
					toReturn.Add(Relations.OutletEntityUsingOutletId);
					break;
				case "ServiceMethodEntity":
					toReturn.Add(Relations.ServiceMethodEntityUsingServiceMethodId);
					break;
				case "AnalyticsProcessingTaskCollection":
					toReturn.Add(Relations.AnalyticsProcessingTaskEntityUsingOrderId);
					break;
				case "ExternalSystemLogCollection":
					toReturn.Add(Relations.ExternalSystemLogEntityUsingOrderId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingOrderId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId);
					break;
				case "OrderitemCollection":
					toReturn.Add(Relations.OrderitemEntityUsingOrderId);
					break;
				case "OrderNotificationLogCollection":
					toReturn.Add(Relations.OrderNotificationLogEntityUsingOrderId);
					break;
				case "OrderRoutestephandlerCollection":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingOrderId);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingOrderId);
					break;
				case "PaymentTransactionCollection":
					toReturn.Add(Relations.PaymentTransactionEntityUsingOrderId);
					break;
				case "ReceiptCollection":
					toReturn.Add(Relations.ReceiptEntityUsingOrderId);
					break;
				case "TerminalLogCollection":
					toReturn.Add(Relations.TerminalLogEntityUsingOrderId);
					break;
				case "CategoryCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingOrderId, "OrderEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaOrderitem":
					toReturn.Add(Relations.OrderitemEntityUsingOrderId, "OrderEntity__", "Orderitem_", JoinHint.None);
					toReturn.Add(OrderitemEntity.Relations.CategoryEntityUsingCategoryId, "Orderitem_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId, "OrderEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.ClientEntityUsingClientId, "Order_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId, "OrderEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.CompanyEntityUsingCompanyId, "Order_", string.Empty, JoinHint.None);
					break;
				case "CurrencyCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId, "OrderEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.CurrencyEntityUsingCurrencyId, "Order_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId, "OrderEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.CustomerEntityUsingCustomerId, "Order_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingOrderId, "OrderEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingMasterOrderId, "OrderEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Order_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingOrderId, "OrderEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingOrderId, "OrderEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.MediaEntityUsingMediaId, "Message_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaOrderitem":
					toReturn.Add(Relations.OrderitemEntityUsingOrderId, "OrderEntity__", "Orderitem_", JoinHint.None);
					toReturn.Add(OrderitemEntity.Relations.ProductEntityUsingProductId, "Orderitem_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.SupportpoolEntityUsingSupportpoolId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingOrderId, "OrderEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalLogFileCollectionViaTerminalLog":
					toReturn.Add(Relations.TerminalLogEntityUsingOrderId, "OrderEntity__", "TerminalLog_", JoinHint.None);
					toReturn.Add(TerminalLogEntity.Relations.TerminalLogFileEntityUsingTerminalLogFileId, "TerminalLog_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_analyticsProcessingTaskCollection", (!this.MarkedForDeletion?_analyticsProcessingTaskCollection:null));
			info.AddValue("_alwaysFetchAnalyticsProcessingTaskCollection", _alwaysFetchAnalyticsProcessingTaskCollection);
			info.AddValue("_alreadyFetchedAnalyticsProcessingTaskCollection", _alreadyFetchedAnalyticsProcessingTaskCollection);
			info.AddValue("_externalSystemLogCollection", (!this.MarkedForDeletion?_externalSystemLogCollection:null));
			info.AddValue("_alwaysFetchExternalSystemLogCollection", _alwaysFetchExternalSystemLogCollection);
			info.AddValue("_alreadyFetchedExternalSystemLogCollection", _alreadyFetchedExternalSystemLogCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_orderitemCollection", (!this.MarkedForDeletion?_orderitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemCollection", _alwaysFetchOrderitemCollection);
			info.AddValue("_alreadyFetchedOrderitemCollection", _alreadyFetchedOrderitemCollection);
			info.AddValue("_orderNotificationLogCollection", (!this.MarkedForDeletion?_orderNotificationLogCollection:null));
			info.AddValue("_alwaysFetchOrderNotificationLogCollection", _alwaysFetchOrderNotificationLogCollection);
			info.AddValue("_alreadyFetchedOrderNotificationLogCollection", _alreadyFetchedOrderNotificationLogCollection);
			info.AddValue("_orderRoutestephandlerCollection", (!this.MarkedForDeletion?_orderRoutestephandlerCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerCollection", _alwaysFetchOrderRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerCollection", _alreadyFetchedOrderRoutestephandlerCollection);
			info.AddValue("_orderRoutestephandlerHistoryCollection", (!this.MarkedForDeletion?_orderRoutestephandlerHistoryCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerHistoryCollection", _alwaysFetchOrderRoutestephandlerHistoryCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerHistoryCollection", _alreadyFetchedOrderRoutestephandlerHistoryCollection);
			info.AddValue("_paymentTransactionCollection", (!this.MarkedForDeletion?_paymentTransactionCollection:null));
			info.AddValue("_alwaysFetchPaymentTransactionCollection", _alwaysFetchPaymentTransactionCollection);
			info.AddValue("_alreadyFetchedPaymentTransactionCollection", _alreadyFetchedPaymentTransactionCollection);
			info.AddValue("_receiptCollection", (!this.MarkedForDeletion?_receiptCollection:null));
			info.AddValue("_alwaysFetchReceiptCollection", _alwaysFetchReceiptCollection);
			info.AddValue("_alreadyFetchedReceiptCollection", _alreadyFetchedReceiptCollection);
			info.AddValue("_terminalLogCollection", (!this.MarkedForDeletion?_terminalLogCollection:null));
			info.AddValue("_alwaysFetchTerminalLogCollection", _alwaysFetchTerminalLogCollection);
			info.AddValue("_alreadyFetchedTerminalLogCollection", _alreadyFetchedTerminalLogCollection);
			info.AddValue("_categoryCollectionViaMessage", (!this.MarkedForDeletion?_categoryCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessage", _alwaysFetchCategoryCollectionViaMessage);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessage", _alreadyFetchedCategoryCollectionViaMessage);
			info.AddValue("_categoryCollectionViaOrderitem", (!this.MarkedForDeletion?_categoryCollectionViaOrderitem:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaOrderitem", _alwaysFetchCategoryCollectionViaOrderitem);
			info.AddValue("_alreadyFetchedCategoryCollectionViaOrderitem", _alreadyFetchedCategoryCollectionViaOrderitem);
			info.AddValue("_clientCollectionViaOrder", (!this.MarkedForDeletion?_clientCollectionViaOrder:null));
			info.AddValue("_alwaysFetchClientCollectionViaOrder", _alwaysFetchClientCollectionViaOrder);
			info.AddValue("_alreadyFetchedClientCollectionViaOrder", _alreadyFetchedClientCollectionViaOrder);
			info.AddValue("_companyCollectionViaOrder", (!this.MarkedForDeletion?_companyCollectionViaOrder:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaOrder", _alwaysFetchCompanyCollectionViaOrder);
			info.AddValue("_alreadyFetchedCompanyCollectionViaOrder", _alreadyFetchedCompanyCollectionViaOrder);
			info.AddValue("_currencyCollectionViaOrder", (!this.MarkedForDeletion?_currencyCollectionViaOrder:null));
			info.AddValue("_alwaysFetchCurrencyCollectionViaOrder", _alwaysFetchCurrencyCollectionViaOrder);
			info.AddValue("_alreadyFetchedCurrencyCollectionViaOrder", _alreadyFetchedCurrencyCollectionViaOrder);
			info.AddValue("_customerCollectionViaOrder", (!this.MarkedForDeletion?_customerCollectionViaOrder:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaOrder", _alwaysFetchCustomerCollectionViaOrder);
			info.AddValue("_alreadyFetchedCustomerCollectionViaOrder", _alreadyFetchedCustomerCollectionViaOrder);
			info.AddValue("_deliverypointCollectionViaMessage", (!this.MarkedForDeletion?_deliverypointCollectionViaMessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessage", _alwaysFetchDeliverypointCollectionViaMessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessage", _alreadyFetchedDeliverypointCollectionViaMessage);
			info.AddValue("_deliverypointCollectionViaOrder", (!this.MarkedForDeletion?_deliverypointCollectionViaOrder:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaOrder", _alwaysFetchDeliverypointCollectionViaOrder);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaOrder", _alreadyFetchedDeliverypointCollectionViaOrder);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_mediaCollectionViaMessage", (!this.MarkedForDeletion?_mediaCollectionViaMessage:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessage", _alwaysFetchMediaCollectionViaMessage);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessage", _alreadyFetchedMediaCollectionViaMessage);
			info.AddValue("_productCollectionViaOrderitem", (!this.MarkedForDeletion?_productCollectionViaOrderitem:null));
			info.AddValue("_alwaysFetchProductCollectionViaOrderitem", _alwaysFetchProductCollectionViaOrderitem);
			info.AddValue("_alreadyFetchedProductCollectionViaOrderitem", _alreadyFetchedProductCollectionViaOrderitem);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandler", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler);
			info.AddValue("_supportpoolCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_supportpoolCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory", _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler", _alwaysFetchTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_", _alwaysFetchTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_terminalLogFileCollectionViaTerminalLog", (!this.MarkedForDeletion?_terminalLogFileCollectionViaTerminalLog:null));
			info.AddValue("_alwaysFetchTerminalLogFileCollectionViaTerminalLog", _alwaysFetchTerminalLogFileCollectionViaTerminalLog);
			info.AddValue("_alreadyFetchedTerminalLogFileCollectionViaTerminalLog", _alreadyFetchedTerminalLogFileCollectionViaTerminalLog);
			info.AddValue("_checkoutMethodEntity", (!this.MarkedForDeletion?_checkoutMethodEntity:null));
			info.AddValue("_checkoutMethodEntityReturnsNewIfNotFound", _checkoutMethodEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCheckoutMethodEntity", _alwaysFetchCheckoutMethodEntity);
			info.AddValue("_alreadyFetchedCheckoutMethodEntity", _alreadyFetchedCheckoutMethodEntity);
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_currencyEntity", (!this.MarkedForDeletion?_currencyEntity:null));
			info.AddValue("_currencyEntityReturnsNewIfNotFound", _currencyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCurrencyEntity", _alwaysFetchCurrencyEntity);
			info.AddValue("_alreadyFetchedCurrencyEntity", _alreadyFetchedCurrencyEntity);
			info.AddValue("_customerEntity", (!this.MarkedForDeletion?_customerEntity:null));
			info.AddValue("_customerEntityReturnsNewIfNotFound", _customerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerEntity", _alwaysFetchCustomerEntity);
			info.AddValue("_alreadyFetchedCustomerEntity", _alreadyFetchedCustomerEntity);
			info.AddValue("_deliveryInformationEntity", (!this.MarkedForDeletion?_deliveryInformationEntity:null));
			info.AddValue("_deliveryInformationEntityReturnsNewIfNotFound", _deliveryInformationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliveryInformationEntity", _alwaysFetchDeliveryInformationEntity);
			info.AddValue("_alreadyFetchedDeliveryInformationEntity", _alreadyFetchedDeliveryInformationEntity);
			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_deliverypointEntity_", (!this.MarkedForDeletion?_deliverypointEntity_:null));
			info.AddValue("_deliverypointEntity_ReturnsNewIfNotFound", _deliverypointEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity_", _alwaysFetchDeliverypointEntity_);
			info.AddValue("_alreadyFetchedDeliverypointEntity_", _alreadyFetchedDeliverypointEntity_);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_outletEntity", (!this.MarkedForDeletion?_outletEntity:null));
			info.AddValue("_outletEntityReturnsNewIfNotFound", _outletEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletEntity", _alwaysFetchOutletEntity);
			info.AddValue("_alreadyFetchedOutletEntity", _alreadyFetchedOutletEntity);
			info.AddValue("_serviceMethodEntity", (!this.MarkedForDeletion?_serviceMethodEntity:null));
			info.AddValue("_serviceMethodEntityReturnsNewIfNotFound", _serviceMethodEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchServiceMethodEntity", _alwaysFetchServiceMethodEntity);
			info.AddValue("_alreadyFetchedServiceMethodEntity", _alreadyFetchedServiceMethodEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CheckoutMethodEntity":
					_alreadyFetchedCheckoutMethodEntity = true;
					this.CheckoutMethodEntity = (CheckoutMethodEntity)entity;
					break;
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "CurrencyEntity":
					_alreadyFetchedCurrencyEntity = true;
					this.CurrencyEntity = (CurrencyEntity)entity;
					break;
				case "CustomerEntity":
					_alreadyFetchedCustomerEntity = true;
					this.CustomerEntity = (CustomerEntity)entity;
					break;
				case "DeliveryInformationEntity":
					_alreadyFetchedDeliveryInformationEntity = true;
					this.DeliveryInformationEntity = (DeliveryInformationEntity)entity;
					break;
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "DeliverypointEntity_":
					_alreadyFetchedDeliverypointEntity_ = true;
					this.DeliverypointEntity_ = (DeliverypointEntity)entity;
					break;
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "OutletEntity":
					_alreadyFetchedOutletEntity = true;
					this.OutletEntity = (OutletEntity)entity;
					break;
				case "ServiceMethodEntity":
					_alreadyFetchedServiceMethodEntity = true;
					this.ServiceMethodEntity = (ServiceMethodEntity)entity;
					break;
				case "AnalyticsProcessingTaskCollection":
					_alreadyFetchedAnalyticsProcessingTaskCollection = true;
					if(entity!=null)
					{
						this.AnalyticsProcessingTaskCollection.Add((AnalyticsProcessingTaskEntity)entity);
					}
					break;
				case "ExternalSystemLogCollection":
					_alreadyFetchedExternalSystemLogCollection = true;
					if(entity!=null)
					{
						this.ExternalSystemLogCollection.Add((ExternalSystemLogEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "OrderitemCollection":
					_alreadyFetchedOrderitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemCollection.Add((OrderitemEntity)entity);
					}
					break;
				case "OrderNotificationLogCollection":
					_alreadyFetchedOrderNotificationLogCollection = true;
					if(entity!=null)
					{
						this.OrderNotificationLogCollection.Add((OrderNotificationLogEntity)entity);
					}
					break;
				case "OrderRoutestephandlerCollection":
					_alreadyFetchedOrderRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)entity);
					}
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)entity);
					}
					break;
				case "PaymentTransactionCollection":
					_alreadyFetchedPaymentTransactionCollection = true;
					if(entity!=null)
					{
						this.PaymentTransactionCollection.Add((PaymentTransactionEntity)entity);
					}
					break;
				case "ReceiptCollection":
					_alreadyFetchedReceiptCollection = true;
					if(entity!=null)
					{
						this.ReceiptCollection.Add((ReceiptEntity)entity);
					}
					break;
				case "TerminalLogCollection":
					_alreadyFetchedTerminalLogCollection = true;
					if(entity!=null)
					{
						this.TerminalLogCollection.Add((TerminalLogEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessage":
					_alreadyFetchedCategoryCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessage.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaOrderitem":
					_alreadyFetchedCategoryCollectionViaOrderitem = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaOrderitem.Add((CategoryEntity)entity);
					}
					break;
				case "ClientCollectionViaOrder":
					_alreadyFetchedClientCollectionViaOrder = true;
					if(entity!=null)
					{
						this.ClientCollectionViaOrder.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaOrder":
					_alreadyFetchedCompanyCollectionViaOrder = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaOrder.Add((CompanyEntity)entity);
					}
					break;
				case "CurrencyCollectionViaOrder":
					_alreadyFetchedCurrencyCollectionViaOrder = true;
					if(entity!=null)
					{
						this.CurrencyCollectionViaOrder.Add((CurrencyEntity)entity);
					}
					break;
				case "CustomerCollectionViaOrder":
					_alreadyFetchedCustomerCollectionViaOrder = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaOrder.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessage":
					_alreadyFetchedDeliverypointCollectionViaMessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaOrder":
					_alreadyFetchedDeliverypointCollectionViaOrder = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaOrder.Add((DeliverypointEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "MediaCollectionViaMessage":
					_alreadyFetchedMediaCollectionViaMessage = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessage.Add((MediaEntity)entity);
					}
					break;
				case "ProductCollectionViaOrderitem":
					_alreadyFetchedProductCollectionViaOrderitem = true;
					if(entity!=null)
					{
						this.ProductCollectionViaOrderitem.Add((ProductEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandler":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandler.Add((SupportpoolEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaOrderRoutestephandlerHistory.Add((SupportpoolEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalLogFileCollectionViaTerminalLog":
					_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = true;
					if(entity!=null)
					{
						this.TerminalLogFileCollectionViaTerminalLog.Add((TerminalLogFileEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CheckoutMethodEntity":
					SetupSyncCheckoutMethodEntity(relatedEntity);
					break;
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "CurrencyEntity":
					SetupSyncCurrencyEntity(relatedEntity);
					break;
				case "CustomerEntity":
					SetupSyncCustomerEntity(relatedEntity);
					break;
				case "DeliveryInformationEntity":
					SetupSyncDeliveryInformationEntity(relatedEntity);
					break;
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "DeliverypointEntity_":
					SetupSyncDeliverypointEntity_(relatedEntity);
					break;
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "OutletEntity":
					SetupSyncOutletEntity(relatedEntity);
					break;
				case "ServiceMethodEntity":
					SetupSyncServiceMethodEntity(relatedEntity);
					break;
				case "AnalyticsProcessingTaskCollection":
					_analyticsProcessingTaskCollection.Add((AnalyticsProcessingTaskEntity)relatedEntity);
					break;
				case "ExternalSystemLogCollection":
					_externalSystemLogCollection.Add((ExternalSystemLogEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "OrderitemCollection":
					_orderitemCollection.Add((OrderitemEntity)relatedEntity);
					break;
				case "OrderNotificationLogCollection":
					_orderNotificationLogCollection.Add((OrderNotificationLogEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerCollection":
					_orderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_orderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)relatedEntity);
					break;
				case "PaymentTransactionCollection":
					_paymentTransactionCollection.Add((PaymentTransactionEntity)relatedEntity);
					break;
				case "ReceiptCollection":
					_receiptCollection.Add((ReceiptEntity)relatedEntity);
					break;
				case "TerminalLogCollection":
					_terminalLogCollection.Add((TerminalLogEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CheckoutMethodEntity":
					DesetupSyncCheckoutMethodEntity(false, true);
					break;
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "CurrencyEntity":
					DesetupSyncCurrencyEntity(false, true);
					break;
				case "CustomerEntity":
					DesetupSyncCustomerEntity(false, true);
					break;
				case "DeliveryInformationEntity":
					DesetupSyncDeliveryInformationEntity(false, true);
					break;
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "DeliverypointEntity_":
					DesetupSyncDeliverypointEntity_(false, true);
					break;
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "OutletEntity":
					DesetupSyncOutletEntity(false, true);
					break;
				case "ServiceMethodEntity":
					DesetupSyncServiceMethodEntity(false, true);
					break;
				case "AnalyticsProcessingTaskCollection":
					this.PerformRelatedEntityRemoval(_analyticsProcessingTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalSystemLogCollection":
					this.PerformRelatedEntityRemoval(_externalSystemLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderNotificationLogCollection":
					this.PerformRelatedEntityRemoval(_orderNotificationLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentTransactionCollection":
					this.PerformRelatedEntityRemoval(_paymentTransactionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceiptCollection":
					this.PerformRelatedEntityRemoval(_receiptCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalLogCollection":
					this.PerformRelatedEntityRemoval(_terminalLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_checkoutMethodEntity!=null)
			{
				toReturn.Add(_checkoutMethodEntity);
			}
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_currencyEntity!=null)
			{
				toReturn.Add(_currencyEntity);
			}
			if(_customerEntity!=null)
			{
				toReturn.Add(_customerEntity);
			}
			if(_deliveryInformationEntity!=null)
			{
				toReturn.Add(_deliveryInformationEntity);
			}
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_deliverypointEntity_!=null)
			{
				toReturn.Add(_deliverypointEntity_);
			}
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_outletEntity!=null)
			{
				toReturn.Add(_outletEntity);
			}
			if(_serviceMethodEntity!=null)
			{
				toReturn.Add(_serviceMethodEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_analyticsProcessingTaskCollection);
			toReturn.Add(_externalSystemLogCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_orderitemCollection);
			toReturn.Add(_orderNotificationLogCollection);
			toReturn.Add(_orderRoutestephandlerCollection);
			toReturn.Add(_orderRoutestephandlerHistoryCollection);
			toReturn.Add(_paymentTransactionCollection);
			toReturn.Add(_receiptCollection);
			toReturn.Add(_terminalLogCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderId)
		{
			return FetchUsingPK(orderId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AnalyticsProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnalyticsProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection GetMultiAnalyticsProcessingTaskCollection(bool forceFetch)
		{
			return GetMultiAnalyticsProcessingTaskCollection(forceFetch, _analyticsProcessingTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnalyticsProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnalyticsProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection GetMultiAnalyticsProcessingTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnalyticsProcessingTaskCollection(forceFetch, _analyticsProcessingTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnalyticsProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection GetMultiAnalyticsProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnalyticsProcessingTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnalyticsProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection GetMultiAnalyticsProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnalyticsProcessingTaskCollection || forceFetch || _alwaysFetchAnalyticsProcessingTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_analyticsProcessingTaskCollection);
				_analyticsProcessingTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_analyticsProcessingTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_analyticsProcessingTaskCollection.GetMultiManyToOne(this, filter);
				_analyticsProcessingTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnalyticsProcessingTaskCollection = true;
			}
			return _analyticsProcessingTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnalyticsProcessingTaskCollection'. These settings will be taken into account
		/// when the property AnalyticsProcessingTaskCollection is requested or GetMultiAnalyticsProcessingTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnalyticsProcessingTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_analyticsProcessingTaskCollection.SortClauses=sortClauses;
			_analyticsProcessingTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSystemLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, _externalSystemLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalSystemLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, _externalSystemLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalSystemLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalSystemLogCollection GetMultiExternalSystemLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalSystemLogCollection || forceFetch || _alwaysFetchExternalSystemLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalSystemLogCollection);
				_externalSystemLogCollection.SuppressClearInGetMulti=!forceFetch;
				_externalSystemLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalSystemLogCollection.GetMultiManyToOne(null, this, filter);
				_externalSystemLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalSystemLogCollection = true;
			}
			return _externalSystemLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalSystemLogCollection'. These settings will be taken into account
		/// when the property ExternalSystemLogCollection is requested or GetMultiExternalSystemLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalSystemLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalSystemLogCollection.SortClauses=sortClauses;
			_externalSystemLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemCollection || forceFetch || _alwaysFetchOrderitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemCollection);
				_orderitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_orderitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemCollection = true;
			}
			return _orderitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemCollection'. These settings will be taken into account
		/// when the property OrderitemCollection is requested or GetMultiOrderitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemCollection.SortClauses=sortClauses;
			_orderitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderNotificationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderNotificationLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderNotificationLogCollection GetMultiOrderNotificationLogCollection(bool forceFetch)
		{
			return GetMultiOrderNotificationLogCollection(forceFetch, _orderNotificationLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderNotificationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderNotificationLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderNotificationLogCollection GetMultiOrderNotificationLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderNotificationLogCollection(forceFetch, _orderNotificationLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderNotificationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderNotificationLogCollection GetMultiOrderNotificationLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderNotificationLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderNotificationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderNotificationLogCollection GetMultiOrderNotificationLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderNotificationLogCollection || forceFetch || _alwaysFetchOrderNotificationLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderNotificationLogCollection);
				_orderNotificationLogCollection.SuppressClearInGetMulti=!forceFetch;
				_orderNotificationLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderNotificationLogCollection.GetMultiManyToOne(this, filter);
				_orderNotificationLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderNotificationLogCollection = true;
			}
			return _orderNotificationLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderNotificationLogCollection'. These settings will be taken into account
		/// when the property OrderNotificationLogCollection is requested or GetMultiOrderNotificationLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderNotificationLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderNotificationLogCollection.SortClauses=sortClauses;
			_orderNotificationLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerCollection || forceFetch || _alwaysFetchOrderRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerCollection);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerCollection = true;
			}
			return _orderRoutestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerCollection is requested or GetMultiOrderRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerCollection.SortClauses=sortClauses;
			_orderRoutestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerHistoryCollection || forceFetch || _alwaysFetchOrderRoutestephandlerHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerHistoryCollection);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerHistoryCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
			}
			return _orderRoutestephandlerHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerHistoryCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerHistoryCollection is requested or GetMultiOrderRoutestephandlerHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerHistoryCollection.SortClauses=sortClauses;
			_orderRoutestephandlerHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, _paymentTransactionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentTransactionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection GetMultiPaymentTransactionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentTransactionCollection || forceFetch || _alwaysFetchPaymentTransactionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentTransactionCollection);
				_paymentTransactionCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentTransactionCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentTransactionCollection.GetMultiManyToOne(this, null, null, filter);
				_paymentTransactionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentTransactionCollection = true;
			}
			return _paymentTransactionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentTransactionCollection'. These settings will be taken into account
		/// when the property PaymentTransactionCollection is requested or GetMultiPaymentTransactionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentTransactionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentTransactionCollection.SortClauses=sortClauses;
			_paymentTransactionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceiptCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceiptCollection || forceFetch || _alwaysFetchReceiptCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receiptCollection);
				_receiptCollection.SuppressClearInGetMulti=!forceFetch;
				_receiptCollection.EntityFactoryToUse = entityFactoryToUse;
				_receiptCollection.GetMultiManyToOne(null, this, null, null, filter);
				_receiptCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceiptCollection = true;
			}
			return _receiptCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceiptCollection'. These settings will be taken into account
		/// when the property ReceiptCollection is requested or GetMultiReceiptCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceiptCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receiptCollection.SortClauses=sortClauses;
			_receiptCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalLogCollection || forceFetch || _alwaysFetchTerminalLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalLogCollection);
				_terminalLogCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalLogCollection.GetMultiManyToOne(this, null, null, filter);
				_terminalLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalLogCollection = true;
			}
			return _terminalLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalLogCollection'. These settings will be taken into account
		/// when the property TerminalLogCollection is requested or GetMultiTerminalLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalLogCollection.SortClauses=sortClauses;
			_terminalLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessage(forceFetch, _categoryCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessage || forceFetch || _alwaysFetchCategoryCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessage.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessage"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessage = true;
			}
			return _categoryCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessage'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessage is requested or GetMultiCategoryCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessage.SortClauses=sortClauses;
			_categoryCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaOrderitem(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaOrderitem(forceFetch, _categoryCollectionViaOrderitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaOrderitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaOrderitem || forceFetch || _alwaysFetchCategoryCollectionViaOrderitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaOrderitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_categoryCollectionViaOrderitem.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaOrderitem.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaOrderitem.GetMulti(filter, GetRelationsForField("CategoryCollectionViaOrderitem"));
				_categoryCollectionViaOrderitem.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaOrderitem = true;
			}
			return _categoryCollectionViaOrderitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaOrderitem'. These settings will be taken into account
		/// when the property CategoryCollectionViaOrderitem is requested or GetMultiCategoryCollectionViaOrderitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaOrderitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaOrderitem.SortClauses=sortClauses;
			_categoryCollectionViaOrderitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaOrder(bool forceFetch)
		{
			return GetMultiClientCollectionViaOrder(forceFetch, _clientCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaOrder || forceFetch || _alwaysFetchClientCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_clientCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaOrder.GetMulti(filter, GetRelationsForField("ClientCollectionViaOrder"));
				_clientCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaOrder = true;
			}
			return _clientCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaOrder'. These settings will be taken into account
		/// when the property ClientCollectionViaOrder is requested or GetMultiClientCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaOrder.SortClauses=sortClauses;
			_clientCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaOrder(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaOrder(forceFetch, _companyCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaOrder || forceFetch || _alwaysFetchCompanyCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_companyCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaOrder.GetMulti(filter, GetRelationsForField("CompanyCollectionViaOrder"));
				_companyCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaOrder = true;
			}
			return _companyCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaOrder'. These settings will be taken into account
		/// when the property CompanyCollectionViaOrder is requested or GetMultiCompanyCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaOrder.SortClauses=sortClauses;
			_companyCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CurrencyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaOrder(bool forceFetch)
		{
			return GetMultiCurrencyCollectionViaOrder(forceFetch, _currencyCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCurrencyCollectionViaOrder || forceFetch || _alwaysFetchCurrencyCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_currencyCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_currencyCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_currencyCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_currencyCollectionViaOrder.GetMulti(filter, GetRelationsForField("CurrencyCollectionViaOrder"));
				_currencyCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedCurrencyCollectionViaOrder = true;
			}
			return _currencyCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'CurrencyCollectionViaOrder'. These settings will be taken into account
		/// when the property CurrencyCollectionViaOrder is requested or GetMultiCurrencyCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCurrencyCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_currencyCollectionViaOrder.SortClauses=sortClauses;
			_currencyCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaOrder(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaOrder(forceFetch, _customerCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaOrder || forceFetch || _alwaysFetchCustomerCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_customerCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaOrder.GetMulti(filter, GetRelationsForField("CustomerCollectionViaOrder"));
				_customerCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaOrder = true;
			}
			return _customerCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaOrder'. These settings will be taken into account
		/// when the property CustomerCollectionViaOrder is requested or GetMultiCustomerCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaOrder.SortClauses=sortClauses;
			_customerCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessage(forceFetch, _deliverypointCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessage || forceFetch || _alwaysFetchDeliverypointCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessage"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessage = true;
			}
			return _deliverypointCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessage is requested or GetMultiDeliverypointCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessage.SortClauses=sortClauses;
			_deliverypointCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaOrder(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaOrder(forceFetch, _deliverypointCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaOrder || forceFetch || _alwaysFetchDeliverypointCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_deliverypointCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaOrder.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaOrder"));
				_deliverypointCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaOrder = true;
			}
			return _deliverypointCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaOrder'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaOrder is requested or GetMultiDeliverypointCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaOrder.SortClauses=sortClauses;
			_deliverypointCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessage(forceFetch, _mediaCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessage || forceFetch || _alwaysFetchMediaCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessage.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessage"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessage = true;
			}
			return _mediaCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessage'. These settings will be taken into account
		/// when the property MediaCollectionViaMessage is requested or GetMultiMediaCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessage.SortClauses=sortClauses;
			_mediaCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaOrderitem(bool forceFetch)
		{
			return GetMultiProductCollectionViaOrderitem(forceFetch, _productCollectionViaOrderitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaOrderitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaOrderitem || forceFetch || _alwaysFetchProductCollectionViaOrderitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaOrderitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_productCollectionViaOrderitem.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaOrderitem.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaOrderitem.GetMulti(filter, GetRelationsForField("ProductCollectionViaOrderitem"));
				_productCollectionViaOrderitem.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaOrderitem = true;
			}
			return _productCollectionViaOrderitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaOrderitem'. These settings will be taken into account
		/// when the property ProductCollectionViaOrderitem is requested or GetMultiProductCollectionViaOrderitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaOrderitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaOrderitem.SortClauses=sortClauses;
			_productCollectionViaOrderitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandler(forceFetch, _supportpoolCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_supportpoolCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler"));
				_supportpoolCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandler is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(forceFetch, _supportpoolCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory"));
				_supportpoolCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _supportpoolCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaOrderRoutestephandlerHistory is requested or GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_supportpoolCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler(forceFetch, _terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
			}
			return _terminalCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler is requested or GetMultiTerminalCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler_(forceFetch, _terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandler_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandler_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogFileEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogFileCollection GetMultiTerminalLogFileCollectionViaTerminalLog(bool forceFetch)
		{
			return GetMultiTerminalLogFileCollectionViaTerminalLog(forceFetch, _terminalLogFileCollectionViaTerminalLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogFileCollection GetMultiTerminalLogFileCollectionViaTerminalLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalLogFileCollectionViaTerminalLog || forceFetch || _alwaysFetchTerminalLogFileCollectionViaTerminalLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalLogFileCollectionViaTerminalLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderFields.OrderId, ComparisonOperator.Equal, this.OrderId, "OrderEntity__"));
				_terminalLogFileCollectionViaTerminalLog.SuppressClearInGetMulti=!forceFetch;
				_terminalLogFileCollectionViaTerminalLog.EntityFactoryToUse = entityFactoryToUse;
				_terminalLogFileCollectionViaTerminalLog.GetMulti(filter, GetRelationsForField("TerminalLogFileCollectionViaTerminalLog"));
				_terminalLogFileCollectionViaTerminalLog.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = true;
			}
			return _terminalLogFileCollectionViaTerminalLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalLogFileCollectionViaTerminalLog'. These settings will be taken into account
		/// when the property TerminalLogFileCollectionViaTerminalLog is requested or GetMultiTerminalLogFileCollectionViaTerminalLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalLogFileCollectionViaTerminalLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalLogFileCollectionViaTerminalLog.SortClauses=sortClauses;
			_terminalLogFileCollectionViaTerminalLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CheckoutMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CheckoutMethodEntity' which is related to this entity.</returns>
		public CheckoutMethodEntity GetSingleCheckoutMethodEntity()
		{
			return GetSingleCheckoutMethodEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CheckoutMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CheckoutMethodEntity' which is related to this entity.</returns>
		public virtual CheckoutMethodEntity GetSingleCheckoutMethodEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCheckoutMethodEntity || forceFetch || _alwaysFetchCheckoutMethodEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CheckoutMethodEntityUsingCheckoutMethodId);
				CheckoutMethodEntity newEntity = new CheckoutMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CheckoutMethodId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CheckoutMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_checkoutMethodEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CheckoutMethodEntity = newEntity;
				_alreadyFetchedCheckoutMethodEntity = fetchResult;
			}
			return _checkoutMethodEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public CurrencyEntity GetSingleCurrencyEntity()
		{
			return GetSingleCurrencyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public virtual CurrencyEntity GetSingleCurrencyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCurrencyEntity || forceFetch || _alwaysFetchCurrencyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CurrencyEntityUsingCurrencyId);
				CurrencyEntity newEntity = new CurrencyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CurrencyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CurrencyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_currencyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CurrencyEntity = newEntity;
				_alreadyFetchedCurrencyEntity = fetchResult;
			}
			return _currencyEntity;
		}


		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleCustomerEntity()
		{
			return GetSingleCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerEntity || forceFetch || _alwaysFetchCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerEntity = newEntity;
				_alreadyFetchedCustomerEntity = fetchResult;
			}
			return _customerEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliveryInformationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliveryInformationEntity' which is related to this entity.</returns>
		public DeliveryInformationEntity GetSingleDeliveryInformationEntity()
		{
			return GetSingleDeliveryInformationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliveryInformationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliveryInformationEntity' which is related to this entity.</returns>
		public virtual DeliveryInformationEntity GetSingleDeliveryInformationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliveryInformationEntity || forceFetch || _alwaysFetchDeliveryInformationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliveryInformationEntityUsingDeliveryInformationId);
				DeliveryInformationEntity newEntity = new DeliveryInformationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliveryInformationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliveryInformationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliveryInformationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliveryInformationEntity = newEntity;
				_alreadyFetchedDeliveryInformationEntity = fetchResult;
			}
			return _deliveryInformationEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity_()
		{
			return GetSingleDeliverypointEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity_ || forceFetch || _alwaysFetchDeliverypointEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingChargeToDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ChargeToDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity_ = newEntity;
				_alreadyFetchedDeliverypointEntity_ = fetchResult;
			}
			return _deliverypointEntity_;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderIdMasterOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MasterOrderId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public OutletEntity GetSingleOutletEntity()
		{
			return GetSingleOutletEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public virtual OutletEntity GetSingleOutletEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletEntity || forceFetch || _alwaysFetchOutletEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletEntityUsingOutletId);
				OutletEntity newEntity = new OutletEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletEntity = newEntity;
				_alreadyFetchedOutletEntity = fetchResult;
			}
			return _outletEntity;
		}


		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public ServiceMethodEntity GetSingleServiceMethodEntity()
		{
			return GetSingleServiceMethodEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public virtual ServiceMethodEntity GetSingleServiceMethodEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedServiceMethodEntity || forceFetch || _alwaysFetchServiceMethodEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ServiceMethodEntityUsingServiceMethodId);
				ServiceMethodEntity newEntity = new ServiceMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceMethodId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ServiceMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_serviceMethodEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ServiceMethodEntity = newEntity;
				_alreadyFetchedServiceMethodEntity = fetchResult;
			}
			return _serviceMethodEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CheckoutMethodEntity", _checkoutMethodEntity);
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("CurrencyEntity", _currencyEntity);
			toReturn.Add("CustomerEntity", _customerEntity);
			toReturn.Add("DeliveryInformationEntity", _deliveryInformationEntity);
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("DeliverypointEntity_", _deliverypointEntity_);
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("OutletEntity", _outletEntity);
			toReturn.Add("ServiceMethodEntity", _serviceMethodEntity);
			toReturn.Add("AnalyticsProcessingTaskCollection", _analyticsProcessingTaskCollection);
			toReturn.Add("ExternalSystemLogCollection", _externalSystemLogCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("OrderitemCollection", _orderitemCollection);
			toReturn.Add("OrderNotificationLogCollection", _orderNotificationLogCollection);
			toReturn.Add("OrderRoutestephandlerCollection", _orderRoutestephandlerCollection);
			toReturn.Add("OrderRoutestephandlerHistoryCollection", _orderRoutestephandlerHistoryCollection);
			toReturn.Add("PaymentTransactionCollection", _paymentTransactionCollection);
			toReturn.Add("ReceiptCollection", _receiptCollection);
			toReturn.Add("TerminalLogCollection", _terminalLogCollection);
			toReturn.Add("CategoryCollectionViaMessage", _categoryCollectionViaMessage);
			toReturn.Add("CategoryCollectionViaOrderitem", _categoryCollectionViaOrderitem);
			toReturn.Add("ClientCollectionViaOrder", _clientCollectionViaOrder);
			toReturn.Add("CompanyCollectionViaOrder", _companyCollectionViaOrder);
			toReturn.Add("CurrencyCollectionViaOrder", _currencyCollectionViaOrder);
			toReturn.Add("CustomerCollectionViaOrder", _customerCollectionViaOrder);
			toReturn.Add("DeliverypointCollectionViaMessage", _deliverypointCollectionViaMessage);
			toReturn.Add("DeliverypointCollectionViaOrder", _deliverypointCollectionViaOrder);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("MediaCollectionViaMessage", _mediaCollectionViaMessage);
			toReturn.Add("ProductCollectionViaOrderitem", _productCollectionViaOrderitem);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandler", _supportpoolCollectionViaOrderRoutestephandler);
			toReturn.Add("SupportpoolCollectionViaOrderRoutestephandlerHistory", _supportpoolCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler", _terminalCollectionViaOrderRoutestephandler);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler_", _terminalCollectionViaOrderRoutestephandler_);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory", _terminalCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory_", _terminalCollectionViaOrderRoutestephandlerHistory_);
			toReturn.Add("TerminalLogFileCollectionViaTerminalLog", _terminalLogFileCollectionViaTerminalLog);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The validator object for this OrderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 orderId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_analyticsProcessingTaskCollection = new Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection();
			_analyticsProcessingTaskCollection.SetContainingEntityInfo(this, "OrderEntity");

			_externalSystemLogCollection = new Obymobi.Data.CollectionClasses.ExternalSystemLogCollection();
			_externalSystemLogCollection.SetContainingEntityInfo(this, "OrderEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "OrderEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "OrderEntity");

			_orderitemCollection = new Obymobi.Data.CollectionClasses.OrderitemCollection();
			_orderitemCollection.SetContainingEntityInfo(this, "OrderEntity");

			_orderNotificationLogCollection = new Obymobi.Data.CollectionClasses.OrderNotificationLogCollection();
			_orderNotificationLogCollection.SetContainingEntityInfo(this, "OrderEntity");

			_orderRoutestephandlerCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection();
			_orderRoutestephandlerCollection.SetContainingEntityInfo(this, "OrderEntity");

			_orderRoutestephandlerHistoryCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection();
			_orderRoutestephandlerHistoryCollection.SetContainingEntityInfo(this, "OrderEntity");

			_paymentTransactionCollection = new Obymobi.Data.CollectionClasses.PaymentTransactionCollection();
			_paymentTransactionCollection.SetContainingEntityInfo(this, "OrderEntity");

			_receiptCollection = new Obymobi.Data.CollectionClasses.ReceiptCollection();
			_receiptCollection.SetContainingEntityInfo(this, "OrderEntity");

			_terminalLogCollection = new Obymobi.Data.CollectionClasses.TerminalLogCollection();
			_terminalLogCollection.SetContainingEntityInfo(this, "OrderEntity");
			_categoryCollectionViaMessage = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaOrderitem = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientCollectionViaOrder = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaOrder = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_currencyCollectionViaOrder = new Obymobi.Data.CollectionClasses.CurrencyCollection();
			_customerCollectionViaOrder = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaMessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaOrder = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_mediaCollectionViaMessage = new Obymobi.Data.CollectionClasses.MediaCollection();
			_productCollectionViaOrderitem = new Obymobi.Data.CollectionClasses.ProductCollection();
			_supportpoolCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_supportpoolCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_terminalCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandler_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalLogFileCollectionViaTerminalLog = new Obymobi.Data.CollectionClasses.TerminalLogFileCollection();
			_checkoutMethodEntityReturnsNewIfNotFound = true;
			_clientEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_currencyEntityReturnsNewIfNotFound = true;
			_customerEntityReturnsNewIfNotFound = true;
			_deliveryInformationEntityReturnsNewIfNotFound = true;
			_deliverypointEntityReturnsNewIfNotFound = true;
			_deliverypointEntity_ReturnsNewIfNotFound = true;
			_orderEntityReturnsNewIfNotFound = true;
			_outletEntityReturnsNewIfNotFound = true;
			_serviceMethodEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerFirstname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerLastname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerLastnamePrefix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerPhonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientMacAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfirmationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AgeVerificationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BenchmarkInformation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorSentByEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorSentBySMS", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Guid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoutingLog", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterOrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotSOSServiceOrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCustomerVip", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BenchmarkProcessedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsPayLoad", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsTrackingIds", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneOlsonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointEnableAnalytics", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PlaceTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalOrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Total", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubTotal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliveryInformationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeToDeliverypointName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeToDeliverypointNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeToDeliverypointgroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PricesIncludeTaxes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeToDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OptInStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxBreakdown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoversCount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _checkoutMethodEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCheckoutMethodEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _checkoutMethodEntity, new PropertyChangedEventHandler( OnCheckoutMethodEntityPropertyChanged ), "CheckoutMethodEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.CheckoutMethodId } );		
			_checkoutMethodEntity = null;
		}
		
		/// <summary> setups the sync logic for member _checkoutMethodEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCheckoutMethodEntity(IEntityCore relatedEntity)
		{
			if(_checkoutMethodEntity!=relatedEntity)
			{		
				DesetupSyncCheckoutMethodEntity(true, true);
				_checkoutMethodEntity = (CheckoutMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _checkoutMethodEntity, new PropertyChangedEventHandler( OnCheckoutMethodEntityPropertyChanged ), "CheckoutMethodEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CheckoutMethodEntityUsingCheckoutMethodIdStatic, true, ref _alreadyFetchedCheckoutMethodEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCheckoutMethodEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _currencyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCurrencyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CurrencyEntityUsingCurrencyIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.CurrencyId } );		
			_currencyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _currencyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCurrencyEntity(IEntityCore relatedEntity)
		{
			if(_currencyEntity!=relatedEntity)
			{		
				DesetupSyncCurrencyEntity(true, true);
				_currencyEntity = (CurrencyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CurrencyEntityUsingCurrencyIdStatic, true, ref _alreadyFetchedCurrencyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCurrencyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _customerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CustomerEntityUsingCustomerIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.CustomerId } );		
			_customerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _customerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerEntity(IEntityCore relatedEntity)
		{
			if(_customerEntity!=relatedEntity)
			{		
				DesetupSyncCustomerEntity(true, true);
				_customerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.CustomerEntityUsingCustomerIdStatic, true, ref _alreadyFetchedCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliveryInformationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliveryInformationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliveryInformationEntity, new PropertyChangedEventHandler( OnDeliveryInformationEntityPropertyChanged ), "DeliveryInformationEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliveryInformationEntityUsingDeliveryInformationIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.DeliveryInformationId } );		
			_deliveryInformationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliveryInformationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliveryInformationEntity(IEntityCore relatedEntity)
		{
			if(_deliveryInformationEntity!=relatedEntity)
			{		
				DesetupSyncDeliveryInformationEntity(true, true);
				_deliveryInformationEntity = (DeliveryInformationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliveryInformationEntity, new PropertyChangedEventHandler( OnDeliveryInformationEntityPropertyChanged ), "DeliveryInformationEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliveryInformationEntityUsingDeliveryInformationIdStatic, true, ref _alreadyFetchedDeliveryInformationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliveryInformationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity_, new PropertyChangedEventHandler( OnDeliverypointEntity_PropertyChanged ), "DeliverypointEntity_", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingChargeToDeliverypointIdStatic, true, signalRelatedEntity, "OrderCollection_", resetFKFields, new int[] { (int)OrderFieldIndex.ChargeToDeliverypointId } );		
			_deliverypointEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity_(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity_!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity_(true, true);
				_deliverypointEntity_ = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity_, new PropertyChangedEventHandler( OnDeliverypointEntity_PropertyChanged ), "DeliverypointEntity_", Obymobi.Data.RelationClasses.StaticOrderRelations.DeliverypointEntityUsingChargeToDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.OrderEntityUsingOrderIdMasterOrderIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.MasterOrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.OrderEntityUsingOrderIdMasterOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.OutletEntityUsingOutletIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.OutletId } );		
			_outletEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletEntity(IEntityCore relatedEntity)
		{
			if(_outletEntity!=relatedEntity)
			{		
				DesetupSyncOutletEntity(true, true);
				_outletEntity = (OutletEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.OutletEntityUsingOutletIdStatic, true, ref _alreadyFetchedOutletEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _serviceMethodEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncServiceMethodEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, signalRelatedEntity, "OrderCollection", resetFKFields, new int[] { (int)OrderFieldIndex.ServiceMethodId } );		
			_serviceMethodEntity = null;
		}
		
		/// <summary> setups the sync logic for member _serviceMethodEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncServiceMethodEntity(IEntityCore relatedEntity)
		{
			if(_serviceMethodEntity!=relatedEntity)
			{		
				DesetupSyncServiceMethodEntity(true, true);
				_serviceMethodEntity = (ServiceMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticOrderRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, ref _alreadyFetchedServiceMethodEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnServiceMethodEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 orderId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderFieldIndex.OrderId].ForcedCurrentValueWrite(orderId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderRelations Relations
		{
			get	{ return new OrderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AnalyticsProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnalyticsProcessingTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("AnalyticsProcessingTaskCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.AnalyticsProcessingTaskEntity, 0, null, null, null, "AnalyticsProcessingTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystemLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemLogCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemLogCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ExternalSystemLogEntity, 0, null, null, null, "ExternalSystemLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderitemEntity, 0, null, null, null, "OrderitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderNotificationLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderNotificationLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderNotificationLogCollection(), (IEntityRelation)GetRelationsForField("OrderNotificationLogCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderNotificationLogEntity, 0, null, null, null, "OrderNotificationLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, 0, null, null, null, "OrderRoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerHistoryCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity, 0, null, null, null, "OrderRoutestephandlerHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.PaymentTransactionEntity, 0, null, null, null, "PaymentTransactionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReceiptCollection(), (IEntityRelation)GetRelationsForField("ReceiptCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ReceiptEntity, 0, null, null, null, "ReceiptCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogCollection(), (IEntityRelation)GetRelationsForField("TerminalLogCollection")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalLogEntity, 0, null, null, null, "TerminalLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessage"), "CategoryCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaOrderitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderitemEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Orderitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaOrderitem"), "CategoryCollectionViaOrderitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingMasterOrderId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaOrder"), "ClientCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingMasterOrderId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaOrder"), "CompanyCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingMasterOrderId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, GetRelationsForField("CurrencyCollectionViaOrder"), "CurrencyCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingMasterOrderId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaOrder"), "CustomerCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessage"), "DeliverypointCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingMasterOrderId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaOrder"), "DeliverypointCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessage"), "MediaCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaOrderitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderitemEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "Orderitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaOrderitem"), "ProductCollectionViaOrderitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandler"), "SupportpoolCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaOrderRoutestephandlerHistory"), "SupportpoolCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"), "TerminalCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"), "TerminalCollectionViaOrderRoutestephandler_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"), "TerminalCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"), "TerminalCollectionViaOrderRoutestephandlerHistory_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLogFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogFileCollectionViaTerminalLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalLogEntityUsingOrderId;
				intermediateRelation.SetAliases(string.Empty, "TerminalLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogFileCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.TerminalLogFileEntity, 0, null, null, GetRelationsForField("TerminalLogFileCollectionViaTerminalLog"), "TerminalLogFileCollectionViaTerminalLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CheckoutMethodEntity, 0, null, null, null, "CheckoutMethodEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), (IEntityRelation)GetRelationsForField("CurrencyEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, null, "CurrencyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("CustomerEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "CustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliveryInformation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliveryInformationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliveryInformationCollection(), (IEntityRelation)GetRelationsForField("DeliveryInformationEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.DeliveryInformationEntity, 0, null, null, null, "DeliveryInformationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity_")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodEntity")[0], (int)Obymobi.Data.EntityType.OrderEntity, (int)Obymobi.Data.EntityType.ServiceMethodEntity, 0, null, null, null, "ServiceMethodEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderFieldIndex.OrderId, value, true); }
		}

		/// <summary> The CustomerId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CustomerId, false); }
			set	{ SetValue((int)OrderFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The CustomerFirstname property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerFirstname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerFirstname
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerFirstname, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerFirstname, value, true); }
		}

		/// <summary> The CustomerLastname property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerLastname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerLastname
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerLastname, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerLastname, value, true); }
		}

		/// <summary> The CustomerLastnamePrefix property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerLastnamePrefix"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerLastnamePrefix
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerLastnamePrefix, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerLastnamePrefix, value, true); }
		}

		/// <summary> The CustomerPhonenumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CustomerPhonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomerPhonenumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CustomerPhonenumber, true); }
			set	{ SetValue((int)OrderFieldIndex.CustomerPhonenumber, value, true); }
		}

		/// <summary> The ClientId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ClientId, false); }
			set	{ SetValue((int)OrderFieldIndex.ClientId, value, true); }
		}

		/// <summary> The ClientMacAddress property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ClientMacAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientMacAddress
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ClientMacAddress, true); }
			set	{ SetValue((int)OrderFieldIndex.ClientMacAddress, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.CompanyId, true); }
			set	{ SetValue((int)OrderFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CompanyName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CompanyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CompanyName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CompanyName, true); }
			set	{ SetValue((int)OrderFieldIndex.CompanyName, value, true); }
		}

		/// <summary> The CurrencyId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CurrencyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CurrencyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CurrencyId, false); }
			set	{ SetValue((int)OrderFieldIndex.CurrencyId, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The DeliverypointName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointName, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointName, value, true); }
		}

		/// <summary> The DeliverypointNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointNumber, value, true); }
		}

		/// <summary> The ConfirmationCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ConfirmationCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 5<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ConfirmationCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ConfirmationCode, true); }
			set	{ SetValue((int)OrderFieldIndex.ConfirmationCode, value, true); }
		}

		/// <summary> The AgeVerificationType property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."AgeVerificationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AgeVerificationType
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.AgeVerificationType, true); }
			set	{ SetValue((int)OrderFieldIndex.AgeVerificationType, value, true); }
		}

		/// <summary> The Notes property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Notes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Notes, true); }
			set	{ SetValue((int)OrderFieldIndex.Notes, value, true); }
		}

		/// <summary> The BenchmarkInformation property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."BenchmarkInformation"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BenchmarkInformation
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.BenchmarkInformation, true); }
			set	{ SetValue((int)OrderFieldIndex.BenchmarkInformation, value, true); }
		}

		/// <summary> The Type property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.Type, true); }
			set	{ SetValue((int)OrderFieldIndex.Type, value, true); }
		}

		/// <summary> The TypeText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TypeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.TypeText, true); }
			set	{ SetValue((int)OrderFieldIndex.TypeText, value, true); }
		}

		/// <summary> The Status property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Status
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.Status, true); }
			set	{ SetValue((int)OrderFieldIndex.Status, value, true); }
		}

		/// <summary> The StatusText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."StatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.StatusText, true); }
			set	{ SetValue((int)OrderFieldIndex.StatusText, value, true); }
		}

		/// <summary> The ErrorSentByEmail property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorSentByEmail"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ErrorSentByEmail
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.ErrorSentByEmail, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorSentByEmail, value, true); }
		}

		/// <summary> The ErrorSentBySMS property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorSentBySMS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ErrorSentBySMS
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.ErrorSentBySMS, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorSentBySMS, value, true); }
		}

		/// <summary> The Guid property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Guid"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderFieldIndex.Guid, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The RoutingLog property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."RoutingLog"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoutingLog
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.RoutingLog, true); }
			set	{ SetValue((int)OrderFieldIndex.RoutingLog, value, true); }
		}

		/// <summary> The ErrorCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ErrorCode
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.ErrorCode, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorCode, value, true); }
		}

		/// <summary> The ErrorText property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ErrorText"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorText
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ErrorText, true); }
			set	{ SetValue((int)OrderFieldIndex.ErrorText, value, true); }
		}

		/// <summary> The Printed property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Printed"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Printed
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.Printed, true); }
			set	{ SetValue((int)OrderFieldIndex.Printed, value, true); }
		}

		/// <summary> The MasterOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."MasterOrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MasterOrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.MasterOrderId, false); }
			set	{ SetValue((int)OrderFieldIndex.MasterOrderId, value, true); }
		}

		/// <summary> The MobileOrder property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."MobileOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MobileOrder
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.MobileOrder, true); }
			set	{ SetValue((int)OrderFieldIndex.MobileOrder, value, true); }
		}

		/// <summary> The Email property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.Email, true); }
			set	{ SetValue((int)OrderFieldIndex.Email, value, true); }
		}

		/// <summary> The HotSOSServiceOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."HotSOSServiceOrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotSOSServiceOrderId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.HotSOSServiceOrderId, true); }
			set	{ SetValue((int)OrderFieldIndex.HotSOSServiceOrderId, value, true); }
		}

		/// <summary> The DeliverypointgroupName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointgroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointgroupName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.DeliverypointgroupName, true); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointgroupName, value, true); }
		}

		/// <summary> The IsCustomerVip property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."IsCustomerVip"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCustomerVip
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.IsCustomerVip, true); }
			set	{ SetValue((int)OrderFieldIndex.IsCustomerVip, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The BenchmarkProcessedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."BenchmarkProcessedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BenchmarkProcessedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.BenchmarkProcessedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.BenchmarkProcessedUTC, value, true); }
		}

		/// <summary> The ProcessedUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ProcessedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProcessedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.ProcessedUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.ProcessedUTC, value, true); }
		}

		/// <summary> The CurrencyCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CurrencyCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CurrencyCode, value, true); }
		}

		/// <summary> The AnalyticsPayLoad property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."AnalyticsPayLoad"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AnalyticsPayLoad
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.AnalyticsPayLoad, true); }
			set	{ SetValue((int)OrderFieldIndex.AnalyticsPayLoad, value, true); }
		}

		/// <summary> The AnalyticsTrackingIds property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."AnalyticsTrackingIds"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AnalyticsTrackingIds
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.AnalyticsTrackingIds, true); }
			set	{ SetValue((int)OrderFieldIndex.AnalyticsTrackingIds, value, true); }
		}

		/// <summary> The TimeZoneOlsonId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TimeZoneOlsonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)OrderFieldIndex.TimeZoneOlsonId, value, true); }
		}

		/// <summary> The DeliverypointEnableAnalytics property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliverypointEnableAnalytics"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> DeliverypointEnableAnalytics
		{
			get { return (Nullable<System.Boolean>)GetValue((int)OrderFieldIndex.DeliverypointEnableAnalytics, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliverypointEnableAnalytics, value, true); }
		}

		/// <summary> The PlaceTimeUTC property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."PlaceTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PlaceTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderFieldIndex.PlaceTimeUTC, false); }
			set	{ SetValue((int)OrderFieldIndex.PlaceTimeUTC, value, true); }
		}

		/// <summary> The ExternalOrderId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ExternalOrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalOrderId
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ExternalOrderId, true); }
			set	{ SetValue((int)OrderFieldIndex.ExternalOrderId, value, true); }
		}

		/// <summary> The ServiceMethodId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ServiceMethodId, false); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodId, value, true); }
		}

		/// <summary> The CheckoutMethodId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CheckoutMethodId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CheckoutMethodId, false); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodId, value, true); }
		}

		/// <summary> The CheckoutMethodName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CheckoutMethodName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CheckoutMethodName, true); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodName, value, true); }
		}

		/// <summary> The CheckoutMethodType property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CheckoutMethodType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CheckoutMethodType
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CheckoutMethodType, false); }
			set	{ SetValue((int)OrderFieldIndex.CheckoutMethodType, value, true); }
		}

		/// <summary> The ServiceMethodName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceMethodName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ServiceMethodName, true); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodName, value, true); }
		}

		/// <summary> The ServiceMethodType property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ServiceMethodType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ServiceMethodType
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ServiceMethodType, false); }
			set	{ SetValue((int)OrderFieldIndex.ServiceMethodType, value, true); }
		}

		/// <summary> The CultureCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CultureCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CultureCode, value, true); }
		}

		/// <summary> The Total property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."Total"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Total
		{
			get { return (System.Decimal)GetValue((int)OrderFieldIndex.Total, true); }
			set	{ SetValue((int)OrderFieldIndex.Total, value, true); }
		}

		/// <summary> The SubTotal property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."SubTotal"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal SubTotal
		{
			get { return (System.Decimal)GetValue((int)OrderFieldIndex.SubTotal, true); }
			set	{ SetValue((int)OrderFieldIndex.SubTotal, value, true); }
		}

		/// <summary> The DeliveryInformationId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."DeliveryInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliveryInformationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.DeliveryInformationId, false); }
			set	{ SetValue((int)OrderFieldIndex.DeliveryInformationId, value, true); }
		}

		/// <summary> The ChargeToDeliverypointName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointName, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointName, value, true); }
		}

		/// <summary> The ChargeToDeliverypointNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointNumber, value, true); }
		}

		/// <summary> The ChargeToDeliverypointgroupName property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointgroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargeToDeliverypointgroupName
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.ChargeToDeliverypointgroupName, true); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointgroupName, value, true); }
		}

		/// <summary> The PricesIncludeTaxes property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."PricesIncludeTaxes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PricesIncludeTaxes
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.PricesIncludeTaxes, true); }
			set	{ SetValue((int)OrderFieldIndex.PricesIncludeTaxes, value, true); }
		}

		/// <summary> The OutletId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OutletId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.OutletId, false); }
			set	{ SetValue((int)OrderFieldIndex.OutletId, value, true); }
		}

		/// <summary> The ChargeToDeliverypointId property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."ChargeToDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ChargeToDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.ChargeToDeliverypointId, false); }
			set	{ SetValue((int)OrderFieldIndex.ChargeToDeliverypointId, value, true); }
		}

		/// <summary> The OptInStatus property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."OptInStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OptInStatus OptInStatus
		{
			get { return (Obymobi.Enums.OptInStatus)GetValue((int)OrderFieldIndex.OptInStatus, true); }
			set	{ SetValue((int)OrderFieldIndex.OptInStatus, value, true); }
		}

		/// <summary> The TaxBreakdown property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."TaxBreakdown"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TaxBreakdown
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)OrderFieldIndex.TaxBreakdown, value, true); }
		}

		/// <summary> The CountryCode property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CountryCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.CountryCode, true); }
			set	{ SetValue((int)OrderFieldIndex.CountryCode, value, true); }
		}

		/// <summary> The CoversCount property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Order"."CoversCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CoversCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.CoversCount, false); }
			set	{ SetValue((int)OrderFieldIndex.CoversCount, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AnalyticsProcessingTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnalyticsProcessingTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnalyticsProcessingTaskCollection AnalyticsProcessingTaskCollection
		{
			get	{ return GetMultiAnalyticsProcessingTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnalyticsProcessingTaskCollection. When set to true, AnalyticsProcessingTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnalyticsProcessingTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnalyticsProcessingTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnalyticsProcessingTaskCollection
		{
			get	{ return _alwaysFetchAnalyticsProcessingTaskCollection; }
			set	{ _alwaysFetchAnalyticsProcessingTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnalyticsProcessingTaskCollection already has been fetched. Setting this property to false when AnalyticsProcessingTaskCollection has been fetched
		/// will clear the AnalyticsProcessingTaskCollection collection well. Setting this property to true while AnalyticsProcessingTaskCollection hasn't been fetched disables lazy loading for AnalyticsProcessingTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnalyticsProcessingTaskCollection
		{
			get { return _alreadyFetchedAnalyticsProcessingTaskCollection;}
			set 
			{
				if(_alreadyFetchedAnalyticsProcessingTaskCollection && !value && (_analyticsProcessingTaskCollection != null))
				{
					_analyticsProcessingTaskCollection.Clear();
				}
				_alreadyFetchedAnalyticsProcessingTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalSystemLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalSystemLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalSystemLogCollection ExternalSystemLogCollection
		{
			get	{ return GetMultiExternalSystemLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemLogCollection. When set to true, ExternalSystemLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalSystemLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemLogCollection
		{
			get	{ return _alwaysFetchExternalSystemLogCollection; }
			set	{ _alwaysFetchExternalSystemLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemLogCollection already has been fetched. Setting this property to false when ExternalSystemLogCollection has been fetched
		/// will clear the ExternalSystemLogCollection collection well. Setting this property to true while ExternalSystemLogCollection hasn't been fetched disables lazy loading for ExternalSystemLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemLogCollection
		{
			get { return _alreadyFetchedExternalSystemLogCollection;}
			set 
			{
				if(_alreadyFetchedExternalSystemLogCollection && !value && (_externalSystemLogCollection != null))
				{
					_externalSystemLogCollection.Clear();
				}
				_alreadyFetchedExternalSystemLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection OrderitemCollection
		{
			get	{ return GetMultiOrderitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemCollection. When set to true, OrderitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemCollection
		{
			get	{ return _alwaysFetchOrderitemCollection; }
			set	{ _alwaysFetchOrderitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemCollection already has been fetched. Setting this property to false when OrderitemCollection has been fetched
		/// will clear the OrderitemCollection collection well. Setting this property to true while OrderitemCollection hasn't been fetched disables lazy loading for OrderitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemCollection
		{
			get { return _alreadyFetchedOrderitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemCollection && !value && (_orderitemCollection != null))
				{
					_orderitemCollection.Clear();
				}
				_alreadyFetchedOrderitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderNotificationLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderNotificationLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderNotificationLogCollection OrderNotificationLogCollection
		{
			get	{ return GetMultiOrderNotificationLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderNotificationLogCollection. When set to true, OrderNotificationLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderNotificationLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderNotificationLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderNotificationLogCollection
		{
			get	{ return _alwaysFetchOrderNotificationLogCollection; }
			set	{ _alwaysFetchOrderNotificationLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderNotificationLogCollection already has been fetched. Setting this property to false when OrderNotificationLogCollection has been fetched
		/// will clear the OrderNotificationLogCollection collection well. Setting this property to true while OrderNotificationLogCollection hasn't been fetched disables lazy loading for OrderNotificationLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderNotificationLogCollection
		{
			get { return _alreadyFetchedOrderNotificationLogCollection;}
			set 
			{
				if(_alreadyFetchedOrderNotificationLogCollection && !value && (_orderNotificationLogCollection != null))
				{
					_orderNotificationLogCollection.Clear();
				}
				_alreadyFetchedOrderNotificationLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection OrderRoutestephandlerCollection
		{
			get	{ return GetMultiOrderRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerCollection. When set to true, OrderRoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerCollection already has been fetched. Setting this property to false when OrderRoutestephandlerCollection has been fetched
		/// will clear the OrderRoutestephandlerCollection collection well. Setting this property to true while OrderRoutestephandlerCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerCollection && !value && (_orderRoutestephandlerCollection != null))
				{
					_orderRoutestephandlerCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection OrderRoutestephandlerHistoryCollection
		{
			get	{ return GetMultiOrderRoutestephandlerHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerHistoryCollection. When set to true, OrderRoutestephandlerHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerHistoryCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerHistoryCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerHistoryCollection already has been fetched. Setting this property to false when OrderRoutestephandlerHistoryCollection has been fetched
		/// will clear the OrderRoutestephandlerHistoryCollection collection well. Setting this property to true while OrderRoutestephandlerHistoryCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerHistoryCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerHistoryCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerHistoryCollection && !value && (_orderRoutestephandlerHistoryCollection != null))
				{
					_orderRoutestephandlerHistoryCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentTransactionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionCollection PaymentTransactionCollection
		{
			get	{ return GetMultiPaymentTransactionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionCollection. When set to true, PaymentTransactionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentTransactionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionCollection
		{
			get	{ return _alwaysFetchPaymentTransactionCollection; }
			set	{ _alwaysFetchPaymentTransactionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionCollection already has been fetched. Setting this property to false when PaymentTransactionCollection has been fetched
		/// will clear the PaymentTransactionCollection collection well. Setting this property to true while PaymentTransactionCollection hasn't been fetched disables lazy loading for PaymentTransactionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionCollection
		{
			get { return _alreadyFetchedPaymentTransactionCollection;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionCollection && !value && (_paymentTransactionCollection != null))
				{
					_paymentTransactionCollection.Clear();
				}
				_alreadyFetchedPaymentTransactionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceiptCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection ReceiptCollection
		{
			get	{ return GetMultiReceiptCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptCollection. When set to true, ReceiptCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceiptCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptCollection
		{
			get	{ return _alwaysFetchReceiptCollection; }
			set	{ _alwaysFetchReceiptCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptCollection already has been fetched. Setting this property to false when ReceiptCollection has been fetched
		/// will clear the ReceiptCollection collection well. Setting this property to true while ReceiptCollection hasn't been fetched disables lazy loading for ReceiptCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptCollection
		{
			get { return _alreadyFetchedReceiptCollection;}
			set 
			{
				if(_alreadyFetchedReceiptCollection && !value && (_receiptCollection != null))
				{
					_receiptCollection.Clear();
				}
				_alreadyFetchedReceiptCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection TerminalLogCollection
		{
			get	{ return GetMultiTerminalLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogCollection. When set to true, TerminalLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogCollection
		{
			get	{ return _alwaysFetchTerminalLogCollection; }
			set	{ _alwaysFetchTerminalLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogCollection already has been fetched. Setting this property to false when TerminalLogCollection has been fetched
		/// will clear the TerminalLogCollection collection well. Setting this property to true while TerminalLogCollection hasn't been fetched disables lazy loading for TerminalLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogCollection
		{
			get { return _alreadyFetchedTerminalLogCollection;}
			set 
			{
				if(_alreadyFetchedTerminalLogCollection && !value && (_terminalLogCollection != null))
				{
					_terminalLogCollection.Clear();
				}
				_alreadyFetchedTerminalLogCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessage
		{
			get { return GetMultiCategoryCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessage. When set to true, CategoryCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessage
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessage; }
			set	{ _alwaysFetchCategoryCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessage already has been fetched. Setting this property to false when CategoryCollectionViaMessage has been fetched
		/// will clear the CategoryCollectionViaMessage collection well. Setting this property to true while CategoryCollectionViaMessage hasn't been fetched disables lazy loading for CategoryCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessage
		{
			get { return _alreadyFetchedCategoryCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessage && !value && (_categoryCollectionViaMessage != null))
				{
					_categoryCollectionViaMessage.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaOrderitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaOrderitem
		{
			get { return GetMultiCategoryCollectionViaOrderitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaOrderitem. When set to true, CategoryCollectionViaOrderitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaOrderitem is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaOrderitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaOrderitem
		{
			get	{ return _alwaysFetchCategoryCollectionViaOrderitem; }
			set	{ _alwaysFetchCategoryCollectionViaOrderitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaOrderitem already has been fetched. Setting this property to false when CategoryCollectionViaOrderitem has been fetched
		/// will clear the CategoryCollectionViaOrderitem collection well. Setting this property to true while CategoryCollectionViaOrderitem hasn't been fetched disables lazy loading for CategoryCollectionViaOrderitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaOrderitem
		{
			get { return _alreadyFetchedCategoryCollectionViaOrderitem;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaOrderitem && !value && (_categoryCollectionViaOrderitem != null))
				{
					_categoryCollectionViaOrderitem.Clear();
				}
				_alreadyFetchedCategoryCollectionViaOrderitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaOrder
		{
			get { return GetMultiClientCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaOrder. When set to true, ClientCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaOrder
		{
			get	{ return _alwaysFetchClientCollectionViaOrder; }
			set	{ _alwaysFetchClientCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaOrder already has been fetched. Setting this property to false when ClientCollectionViaOrder has been fetched
		/// will clear the ClientCollectionViaOrder collection well. Setting this property to true while ClientCollectionViaOrder hasn't been fetched disables lazy loading for ClientCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaOrder
		{
			get { return _alreadyFetchedClientCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaOrder && !value && (_clientCollectionViaOrder != null))
				{
					_clientCollectionViaOrder.Clear();
				}
				_alreadyFetchedClientCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaOrder
		{
			get { return GetMultiCompanyCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaOrder. When set to true, CompanyCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaOrder
		{
			get	{ return _alwaysFetchCompanyCollectionViaOrder; }
			set	{ _alwaysFetchCompanyCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaOrder already has been fetched. Setting this property to false when CompanyCollectionViaOrder has been fetched
		/// will clear the CompanyCollectionViaOrder collection well. Setting this property to true while CompanyCollectionViaOrder hasn't been fetched disables lazy loading for CompanyCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaOrder
		{
			get { return _alreadyFetchedCompanyCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaOrder && !value && (_companyCollectionViaOrder != null))
				{
					_companyCollectionViaOrder.Clear();
				}
				_alreadyFetchedCompanyCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCurrencyCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CurrencyCollection CurrencyCollectionViaOrder
		{
			get { return GetMultiCurrencyCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyCollectionViaOrder. When set to true, CurrencyCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiCurrencyCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyCollectionViaOrder
		{
			get	{ return _alwaysFetchCurrencyCollectionViaOrder; }
			set	{ _alwaysFetchCurrencyCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyCollectionViaOrder already has been fetched. Setting this property to false when CurrencyCollectionViaOrder has been fetched
		/// will clear the CurrencyCollectionViaOrder collection well. Setting this property to true while CurrencyCollectionViaOrder hasn't been fetched disables lazy loading for CurrencyCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyCollectionViaOrder
		{
			get { return _alreadyFetchedCurrencyCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedCurrencyCollectionViaOrder && !value && (_currencyCollectionViaOrder != null))
				{
					_currencyCollectionViaOrder.Clear();
				}
				_alreadyFetchedCurrencyCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaOrder
		{
			get { return GetMultiCustomerCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaOrder. When set to true, CustomerCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaOrder
		{
			get	{ return _alwaysFetchCustomerCollectionViaOrder; }
			set	{ _alwaysFetchCustomerCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaOrder already has been fetched. Setting this property to false when CustomerCollectionViaOrder has been fetched
		/// will clear the CustomerCollectionViaOrder collection well. Setting this property to true while CustomerCollectionViaOrder hasn't been fetched disables lazy loading for CustomerCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaOrder
		{
			get { return _alreadyFetchedCustomerCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaOrder && !value && (_customerCollectionViaOrder != null))
				{
					_customerCollectionViaOrder.Clear();
				}
				_alreadyFetchedCustomerCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessage
		{
			get { return GetMultiDeliverypointCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessage. When set to true, DeliverypointCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessage already has been fetched. Setting this property to false when DeliverypointCollectionViaMessage has been fetched
		/// will clear the DeliverypointCollectionViaMessage collection well. Setting this property to true while DeliverypointCollectionViaMessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessage && !value && (_deliverypointCollectionViaMessage != null))
				{
					_deliverypointCollectionViaMessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaOrder
		{
			get { return GetMultiDeliverypointCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaOrder. When set to true, DeliverypointCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaOrder
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaOrder; }
			set	{ _alwaysFetchDeliverypointCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaOrder already has been fetched. Setting this property to false when DeliverypointCollectionViaOrder has been fetched
		/// will clear the DeliverypointCollectionViaOrder collection well. Setting this property to true while DeliverypointCollectionViaOrder hasn't been fetched disables lazy loading for DeliverypointCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaOrder
		{
			get { return _alreadyFetchedDeliverypointCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaOrder && !value && (_deliverypointCollectionViaOrder != null))
				{
					_deliverypointCollectionViaOrder.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessage
		{
			get { return GetMultiMediaCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessage. When set to true, MediaCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessage
		{
			get	{ return _alwaysFetchMediaCollectionViaMessage; }
			set	{ _alwaysFetchMediaCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessage already has been fetched. Setting this property to false when MediaCollectionViaMessage has been fetched
		/// will clear the MediaCollectionViaMessage collection well. Setting this property to true while MediaCollectionViaMessage hasn't been fetched disables lazy loading for MediaCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessage
		{
			get { return _alreadyFetchedMediaCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessage && !value && (_mediaCollectionViaMessage != null))
				{
					_mediaCollectionViaMessage.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaOrderitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaOrderitem
		{
			get { return GetMultiProductCollectionViaOrderitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaOrderitem. When set to true, ProductCollectionViaOrderitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaOrderitem is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaOrderitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaOrderitem
		{
			get	{ return _alwaysFetchProductCollectionViaOrderitem; }
			set	{ _alwaysFetchProductCollectionViaOrderitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaOrderitem already has been fetched. Setting this property to false when ProductCollectionViaOrderitem has been fetched
		/// will clear the ProductCollectionViaOrderitem collection well. Setting this property to true while ProductCollectionViaOrderitem hasn't been fetched disables lazy loading for ProductCollectionViaOrderitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaOrderitem
		{
			get { return _alreadyFetchedProductCollectionViaOrderitem;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaOrderitem && !value && (_productCollectionViaOrderitem != null))
				{
					_productCollectionViaOrderitem.Clear();
				}
				_alreadyFetchedProductCollectionViaOrderitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandler
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandler. When set to true, SupportpoolCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandler has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandler collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler && !value && (_supportpoolCollectionViaOrderRoutestephandler != null))
				{
					_supportpoolCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaOrderRoutestephandlerHistory. When set to true, SupportpoolCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchSupportpoolCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when SupportpoolCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the SupportpoolCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while SupportpoolCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for SupportpoolCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory && !value && (_supportpoolCollectionViaOrderRoutestephandlerHistory != null))
				{
					_supportpoolCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler. When set to true, TerminalCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler && !value && (_terminalCollectionViaOrderRoutestephandler != null))
				{
					_terminalCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler_. When set to true, TerminalCollectionViaOrderRoutestephandler_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ && !value && (_terminalCollectionViaOrderRoutestephandler_ != null))
				{
					_terminalCollectionViaOrderRoutestephandler_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory && !value && (_terminalCollectionViaOrderRoutestephandlerHistory != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory_. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ && !value && (_terminalCollectionViaOrderRoutestephandlerHistory_ != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogFileEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalLogFileCollectionViaTerminalLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogFileCollection TerminalLogFileCollectionViaTerminalLog
		{
			get { return GetMultiTerminalLogFileCollectionViaTerminalLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogFileCollectionViaTerminalLog. When set to true, TerminalLogFileCollectionViaTerminalLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogFileCollectionViaTerminalLog is accessed. You can always execute a forced fetch by calling GetMultiTerminalLogFileCollectionViaTerminalLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogFileCollectionViaTerminalLog
		{
			get	{ return _alwaysFetchTerminalLogFileCollectionViaTerminalLog; }
			set	{ _alwaysFetchTerminalLogFileCollectionViaTerminalLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogFileCollectionViaTerminalLog already has been fetched. Setting this property to false when TerminalLogFileCollectionViaTerminalLog has been fetched
		/// will clear the TerminalLogFileCollectionViaTerminalLog collection well. Setting this property to true while TerminalLogFileCollectionViaTerminalLog hasn't been fetched disables lazy loading for TerminalLogFileCollectionViaTerminalLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogFileCollectionViaTerminalLog
		{
			get { return _alreadyFetchedTerminalLogFileCollectionViaTerminalLog;}
			set 
			{
				if(_alreadyFetchedTerminalLogFileCollectionViaTerminalLog && !value && (_terminalLogFileCollectionViaTerminalLog != null))
				{
					_terminalLogFileCollectionViaTerminalLog.Clear();
				}
				_alreadyFetchedTerminalLogFileCollectionViaTerminalLog = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CheckoutMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCheckoutMethodEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CheckoutMethodEntity CheckoutMethodEntity
		{
			get	{ return GetSingleCheckoutMethodEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCheckoutMethodEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "CheckoutMethodEntity", _checkoutMethodEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodEntity. When set to true, CheckoutMethodEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodEntity is accessed. You can always execute a forced fetch by calling GetSingleCheckoutMethodEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodEntity
		{
			get	{ return _alwaysFetchCheckoutMethodEntity; }
			set	{ _alwaysFetchCheckoutMethodEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodEntity already has been fetched. Setting this property to false when CheckoutMethodEntity has been fetched
		/// will set CheckoutMethodEntity to null as well. Setting this property to true while CheckoutMethodEntity hasn't been fetched disables lazy loading for CheckoutMethodEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodEntity
		{
			get { return _alreadyFetchedCheckoutMethodEntity;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodEntity && !value)
				{
					this.CheckoutMethodEntity = null;
				}
				_alreadyFetchedCheckoutMethodEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CheckoutMethodEntity is not found
		/// in the database. When set to true, CheckoutMethodEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CheckoutMethodEntityReturnsNewIfNotFound
		{
			get	{ return _checkoutMethodEntityReturnsNewIfNotFound; }
			set { _checkoutMethodEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CurrencyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCurrencyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CurrencyEntity CurrencyEntity
		{
			get	{ return GetSingleCurrencyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCurrencyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "CurrencyEntity", _currencyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyEntity. When set to true, CurrencyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyEntity is accessed. You can always execute a forced fetch by calling GetSingleCurrencyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyEntity
		{
			get	{ return _alwaysFetchCurrencyEntity; }
			set	{ _alwaysFetchCurrencyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyEntity already has been fetched. Setting this property to false when CurrencyEntity has been fetched
		/// will set CurrencyEntity to null as well. Setting this property to true while CurrencyEntity hasn't been fetched disables lazy loading for CurrencyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyEntity
		{
			get { return _alreadyFetchedCurrencyEntity;}
			set 
			{
				if(_alreadyFetchedCurrencyEntity && !value)
				{
					this.CurrencyEntity = null;
				}
				_alreadyFetchedCurrencyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CurrencyEntity is not found
		/// in the database. When set to true, CurrencyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CurrencyEntityReturnsNewIfNotFound
		{
			get	{ return _currencyEntityReturnsNewIfNotFound; }
			set { _currencyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity CustomerEntity
		{
			get	{ return GetSingleCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "CustomerEntity", _customerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerEntity. When set to true, CustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerEntity
		{
			get	{ return _alwaysFetchCustomerEntity; }
			set	{ _alwaysFetchCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerEntity already has been fetched. Setting this property to false when CustomerEntity has been fetched
		/// will set CustomerEntity to null as well. Setting this property to true while CustomerEntity hasn't been fetched disables lazy loading for CustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerEntity
		{
			get { return _alreadyFetchedCustomerEntity;}
			set 
			{
				if(_alreadyFetchedCustomerEntity && !value)
				{
					this.CustomerEntity = null;
				}
				_alreadyFetchedCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerEntity is not found
		/// in the database. When set to true, CustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CustomerEntityReturnsNewIfNotFound
		{
			get	{ return _customerEntityReturnsNewIfNotFound; }
			set { _customerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliveryInformationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliveryInformationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliveryInformationEntity DeliveryInformationEntity
		{
			get	{ return GetSingleDeliveryInformationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliveryInformationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "DeliveryInformationEntity", _deliveryInformationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliveryInformationEntity. When set to true, DeliveryInformationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliveryInformationEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliveryInformationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliveryInformationEntity
		{
			get	{ return _alwaysFetchDeliveryInformationEntity; }
			set	{ _alwaysFetchDeliveryInformationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliveryInformationEntity already has been fetched. Setting this property to false when DeliveryInformationEntity has been fetched
		/// will set DeliveryInformationEntity to null as well. Setting this property to true while DeliveryInformationEntity hasn't been fetched disables lazy loading for DeliveryInformationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliveryInformationEntity
		{
			get { return _alreadyFetchedDeliveryInformationEntity;}
			set 
			{
				if(_alreadyFetchedDeliveryInformationEntity && !value)
				{
					this.DeliveryInformationEntity = null;
				}
				_alreadyFetchedDeliveryInformationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliveryInformationEntity is not found
		/// in the database. When set to true, DeliveryInformationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliveryInformationEntityReturnsNewIfNotFound
		{
			get	{ return _deliveryInformationEntityReturnsNewIfNotFound; }
			set { _deliveryInformationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "DeliverypointEntity", _deliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity_
		{
			get	{ return GetSingleDeliverypointEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection_", "DeliverypointEntity_", _deliverypointEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity_. When set to true, DeliverypointEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity_ is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity_
		{
			get	{ return _alwaysFetchDeliverypointEntity_; }
			set	{ _alwaysFetchDeliverypointEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity_ already has been fetched. Setting this property to false when DeliverypointEntity_ has been fetched
		/// will set DeliverypointEntity_ to null as well. Setting this property to true while DeliverypointEntity_ hasn't been fetched disables lazy loading for DeliverypointEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity_
		{
			get { return _alreadyFetchedDeliverypointEntity_;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity_ && !value)
				{
					this.DeliverypointEntity_ = null;
				}
				_alreadyFetchedDeliverypointEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity_ is not found
		/// in the database. When set to true, DeliverypointEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntity_ReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntity_ReturnsNewIfNotFound; }
			set { _deliverypointEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletEntity OutletEntity
		{
			get	{ return GetSingleOutletEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "OutletEntity", _outletEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletEntity. When set to true, OutletEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletEntity
		{
			get	{ return _alwaysFetchOutletEntity; }
			set	{ _alwaysFetchOutletEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletEntity already has been fetched. Setting this property to false when OutletEntity has been fetched
		/// will set OutletEntity to null as well. Setting this property to true while OutletEntity hasn't been fetched disables lazy loading for OutletEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletEntity
		{
			get { return _alreadyFetchedOutletEntity;}
			set 
			{
				if(_alreadyFetchedOutletEntity && !value)
				{
					this.OutletEntity = null;
				}
				_alreadyFetchedOutletEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletEntity is not found
		/// in the database. When set to true, OutletEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletEntityReturnsNewIfNotFound
		{
			get	{ return _outletEntityReturnsNewIfNotFound; }
			set { _outletEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ServiceMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleServiceMethodEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ServiceMethodEntity ServiceMethodEntity
		{
			get	{ return GetSingleServiceMethodEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncServiceMethodEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderCollection", "ServiceMethodEntity", _serviceMethodEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodEntity. When set to true, ServiceMethodEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodEntity is accessed. You can always execute a forced fetch by calling GetSingleServiceMethodEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodEntity
		{
			get	{ return _alwaysFetchServiceMethodEntity; }
			set	{ _alwaysFetchServiceMethodEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodEntity already has been fetched. Setting this property to false when ServiceMethodEntity has been fetched
		/// will set ServiceMethodEntity to null as well. Setting this property to true while ServiceMethodEntity hasn't been fetched disables lazy loading for ServiceMethodEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodEntity
		{
			get { return _alreadyFetchedServiceMethodEntity;}
			set 
			{
				if(_alreadyFetchedServiceMethodEntity && !value)
				{
					this.ServiceMethodEntity = null;
				}
				_alreadyFetchedServiceMethodEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ServiceMethodEntity is not found
		/// in the database. When set to true, ServiceMethodEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ServiceMethodEntityReturnsNewIfNotFound
		{
			get	{ return _serviceMethodEntityReturnsNewIfNotFound; }
			set { _serviceMethodEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OrderEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
