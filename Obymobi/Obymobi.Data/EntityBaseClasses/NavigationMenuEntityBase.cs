﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'NavigationMenu'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	[DebuggerDisplay("NavigationMenuId={NavigationMenuId}, Name={Name}")]
	public abstract partial class NavigationMenuEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "NavigationMenuEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.LandingPageCollection	_landingPageCollection;
		private bool	_alwaysFetchLandingPageCollection, _alreadyFetchedLandingPageCollection;
		private Obymobi.Data.CollectionClasses.NavigationMenuItemCollection	_navigationMenuItemCollection;
		private bool	_alwaysFetchNavigationMenuItemCollection, _alreadyFetchedNavigationMenuItemCollection;
		private Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection	_navigationMenuWidgetCollection;
		private bool	_alwaysFetchNavigationMenuWidgetCollection, _alreadyFetchedNavigationMenuWidgetCollection;
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name LandingPageCollection</summary>
			public static readonly string LandingPageCollection = "LandingPageCollection";
			/// <summary>Member name NavigationMenuItemCollection</summary>
			public static readonly string NavigationMenuItemCollection = "NavigationMenuItemCollection";
			/// <summary>Member name NavigationMenuWidgetCollection</summary>
			public static readonly string NavigationMenuWidgetCollection = "NavigationMenuWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NavigationMenuEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected NavigationMenuEntityBase() :base("NavigationMenuEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		protected NavigationMenuEntityBase(System.Int32 navigationMenuId):base("NavigationMenuEntity")
		{
			InitClassFetch(navigationMenuId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected NavigationMenuEntityBase(System.Int32 navigationMenuId, IPrefetchPath prefetchPathToUse): base("NavigationMenuEntity")
		{
			InitClassFetch(navigationMenuId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="validator">The custom validator object for this NavigationMenuEntity</param>
		protected NavigationMenuEntityBase(System.Int32 navigationMenuId, IValidator validator):base("NavigationMenuEntity")
		{
			InitClassFetch(navigationMenuId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NavigationMenuEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_landingPageCollection = (Obymobi.Data.CollectionClasses.LandingPageCollection)info.GetValue("_landingPageCollection", typeof(Obymobi.Data.CollectionClasses.LandingPageCollection));
			_alwaysFetchLandingPageCollection = info.GetBoolean("_alwaysFetchLandingPageCollection");
			_alreadyFetchedLandingPageCollection = info.GetBoolean("_alreadyFetchedLandingPageCollection");

			_navigationMenuItemCollection = (Obymobi.Data.CollectionClasses.NavigationMenuItemCollection)info.GetValue("_navigationMenuItemCollection", typeof(Obymobi.Data.CollectionClasses.NavigationMenuItemCollection));
			_alwaysFetchNavigationMenuItemCollection = info.GetBoolean("_alwaysFetchNavigationMenuItemCollection");
			_alreadyFetchedNavigationMenuItemCollection = info.GetBoolean("_alreadyFetchedNavigationMenuItemCollection");

			_navigationMenuWidgetCollection = (Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection)info.GetValue("_navigationMenuWidgetCollection", typeof(Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection));
			_alwaysFetchNavigationMenuWidgetCollection = info.GetBoolean("_alwaysFetchNavigationMenuWidgetCollection");
			_alreadyFetchedNavigationMenuWidgetCollection = info.GetBoolean("_alreadyFetchedNavigationMenuWidgetCollection");
			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NavigationMenuFieldIndex)fieldIndex)
			{
				case NavigationMenuFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLandingPageCollection = (_landingPageCollection.Count > 0);
			_alreadyFetchedNavigationMenuItemCollection = (_navigationMenuItemCollection.Count > 0);
			_alreadyFetchedNavigationMenuWidgetCollection = (_navigationMenuWidgetCollection.Count > 0);
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "LandingPageCollection":
					toReturn.Add(Relations.LandingPageEntityUsingNavigationMenuId);
					break;
				case "NavigationMenuItemCollection":
					toReturn.Add(Relations.NavigationMenuItemEntityUsingNavigationMenuId);
					break;
				case "NavigationMenuWidgetCollection":
					toReturn.Add(Relations.NavigationMenuWidgetEntityUsingNavigationMenuId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_landingPageCollection", (!this.MarkedForDeletion?_landingPageCollection:null));
			info.AddValue("_alwaysFetchLandingPageCollection", _alwaysFetchLandingPageCollection);
			info.AddValue("_alreadyFetchedLandingPageCollection", _alreadyFetchedLandingPageCollection);
			info.AddValue("_navigationMenuItemCollection", (!this.MarkedForDeletion?_navigationMenuItemCollection:null));
			info.AddValue("_alwaysFetchNavigationMenuItemCollection", _alwaysFetchNavigationMenuItemCollection);
			info.AddValue("_alreadyFetchedNavigationMenuItemCollection", _alreadyFetchedNavigationMenuItemCollection);
			info.AddValue("_navigationMenuWidgetCollection", (!this.MarkedForDeletion?_navigationMenuWidgetCollection:null));
			info.AddValue("_alwaysFetchNavigationMenuWidgetCollection", _alwaysFetchNavigationMenuWidgetCollection);
			info.AddValue("_alreadyFetchedNavigationMenuWidgetCollection", _alreadyFetchedNavigationMenuWidgetCollection);
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "LandingPageCollection":
					_alreadyFetchedLandingPageCollection = true;
					if(entity!=null)
					{
						this.LandingPageCollection.Add((LandingPageEntity)entity);
					}
					break;
				case "NavigationMenuItemCollection":
					_alreadyFetchedNavigationMenuItemCollection = true;
					if(entity!=null)
					{
						this.NavigationMenuItemCollection.Add((NavigationMenuItemEntity)entity);
					}
					break;
				case "NavigationMenuWidgetCollection":
					_alreadyFetchedNavigationMenuWidgetCollection = true;
					if(entity!=null)
					{
						this.NavigationMenuWidgetCollection.Add((NavigationMenuWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "LandingPageCollection":
					_landingPageCollection.Add((LandingPageEntity)relatedEntity);
					break;
				case "NavigationMenuItemCollection":
					_navigationMenuItemCollection.Add((NavigationMenuItemEntity)relatedEntity);
					break;
				case "NavigationMenuWidgetCollection":
					_navigationMenuWidgetCollection.Add((NavigationMenuWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "LandingPageCollection":
					this.PerformRelatedEntityRemoval(_landingPageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NavigationMenuItemCollection":
					this.PerformRelatedEntityRemoval(_navigationMenuItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NavigationMenuWidgetCollection":
					this.PerformRelatedEntityRemoval(_navigationMenuWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_landingPageCollection);
			toReturn.Add(_navigationMenuItemCollection);
			toReturn.Add(_navigationMenuWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId)
		{
			return FetchUsingPK(navigationMenuId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(navigationMenuId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(navigationMenuId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(navigationMenuId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NavigationMenuId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NavigationMenuRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLandingPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLandingPageCollection || forceFetch || _alwaysFetchLandingPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_landingPageCollection);
				_landingPageCollection.SuppressClearInGetMulti=!forceFetch;
				_landingPageCollection.EntityFactoryToUse = entityFactoryToUse;
				_landingPageCollection.GetMultiManyToOne(null, this, null, null, filter);
				_landingPageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedLandingPageCollection = true;
			}
			return _landingPageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'LandingPageCollection'. These settings will be taken into account
		/// when the property LandingPageCollection is requested or GetMultiLandingPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLandingPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_landingPageCollection.SortClauses=sortClauses;
			_landingPageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, _navigationMenuItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, _navigationMenuItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNavigationMenuItemCollection || forceFetch || _alwaysFetchNavigationMenuItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_navigationMenuItemCollection);
				_navigationMenuItemCollection.SuppressClearInGetMulti=!forceFetch;
				_navigationMenuItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_navigationMenuItemCollection.GetMultiManyToOne(null, this, filter);
				_navigationMenuItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNavigationMenuItemCollection = true;
			}
			return _navigationMenuItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NavigationMenuItemCollection'. These settings will be taken into account
		/// when the property NavigationMenuItemCollection is requested or GetMultiNavigationMenuItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNavigationMenuItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_navigationMenuItemCollection.SortClauses=sortClauses;
			_navigationMenuItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, _navigationMenuWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, _navigationMenuWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNavigationMenuWidgetCollection || forceFetch || _alwaysFetchNavigationMenuWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_navigationMenuWidgetCollection);
				_navigationMenuWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_navigationMenuWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_navigationMenuWidgetCollection.GetMultiManyToOne(this, null, filter);
				_navigationMenuWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNavigationMenuWidgetCollection = true;
			}
			return _navigationMenuWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NavigationMenuWidgetCollection'. These settings will be taken into account
		/// when the property NavigationMenuWidgetCollection is requested or GetMultiNavigationMenuWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNavigationMenuWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_navigationMenuWidgetCollection.SortClauses=sortClauses;
			_navigationMenuWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("LandingPageCollection", _landingPageCollection);
			toReturn.Add("NavigationMenuItemCollection", _navigationMenuItemCollection);
			toReturn.Add("NavigationMenuWidgetCollection", _navigationMenuWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="validator">The validator object for this NavigationMenuEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 navigationMenuId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(navigationMenuId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_landingPageCollection = new Obymobi.Data.CollectionClasses.LandingPageCollection();
			_landingPageCollection.SetContainingEntityInfo(this, "NavigationMenuEntity");

			_navigationMenuItemCollection = new Obymobi.Data.CollectionClasses.NavigationMenuItemCollection();
			_navigationMenuItemCollection.SetContainingEntityInfo(this, "NavigationMenuEntity");

			_navigationMenuWidgetCollection = new Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection();
			_navigationMenuWidgetCollection.SetContainingEntityInfo(this, "NavigationMenuEntity");
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationMenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "NavigationMenuCollection", resetFKFields, new int[] { (int)NavigationMenuFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="navigationMenuId">PK value for NavigationMenu which data should be fetched into this NavigationMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 navigationMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NavigationMenuFieldIndex.NavigationMenuId].ForcedCurrentValueWrite(navigationMenuId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNavigationMenuDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NavigationMenuEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NavigationMenuRelations Relations
		{
			get	{ return new NavigationMenuRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageCollection")[0], (int)Obymobi.Data.EntityType.NavigationMenuEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenuItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuItemCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuItemCollection")[0], (int)Obymobi.Data.EntityType.NavigationMenuEntity, (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, 0, null, null, null, "NavigationMenuItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenuWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuWidgetCollection")[0], (int)Obymobi.Data.EntityType.NavigationMenuEntity, (int)Obymobi.Data.EntityType.NavigationMenuWidgetEntity, 0, null, null, null, "NavigationMenuWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.NavigationMenuEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The NavigationMenuId property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."NavigationMenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 NavigationMenuId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuFieldIndex.NavigationMenuId, true); }
			set	{ SetValue((int)NavigationMenuFieldIndex.NavigationMenuId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuFieldIndex.CompanyId, true); }
			set	{ SetValue((int)NavigationMenuFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)NavigationMenuFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)NavigationMenuFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)NavigationMenuFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NavigationMenuFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)NavigationMenuFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)NavigationMenuFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Name property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)NavigationMenuFieldIndex.Name, true); }
			set	{ SetValue((int)NavigationMenuFieldIndex.Name, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity NavigationMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenu"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)NavigationMenuFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLandingPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection LandingPageCollection
		{
			get	{ return GetMultiLandingPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageCollection. When set to true, LandingPageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiLandingPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageCollection
		{
			get	{ return _alwaysFetchLandingPageCollection; }
			set	{ _alwaysFetchLandingPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageCollection already has been fetched. Setting this property to false when LandingPageCollection has been fetched
		/// will clear the LandingPageCollection collection well. Setting this property to true while LandingPageCollection hasn't been fetched disables lazy loading for LandingPageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageCollection
		{
			get { return _alreadyFetchedLandingPageCollection;}
			set 
			{
				if(_alreadyFetchedLandingPageCollection && !value && (_landingPageCollection != null))
				{
					_landingPageCollection.Clear();
				}
				_alreadyFetchedLandingPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNavigationMenuItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuItemCollection NavigationMenuItemCollection
		{
			get	{ return GetMultiNavigationMenuItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuItemCollection. When set to true, NavigationMenuItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNavigationMenuItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuItemCollection
		{
			get	{ return _alwaysFetchNavigationMenuItemCollection; }
			set	{ _alwaysFetchNavigationMenuItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuItemCollection already has been fetched. Setting this property to false when NavigationMenuItemCollection has been fetched
		/// will clear the NavigationMenuItemCollection collection well. Setting this property to true while NavigationMenuItemCollection hasn't been fetched disables lazy loading for NavigationMenuItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuItemCollection
		{
			get { return _alreadyFetchedNavigationMenuItemCollection;}
			set 
			{
				if(_alreadyFetchedNavigationMenuItemCollection && !value && (_navigationMenuItemCollection != null))
				{
					_navigationMenuItemCollection.Clear();
				}
				_alreadyFetchedNavigationMenuItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNavigationMenuWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection NavigationMenuWidgetCollection
		{
			get	{ return GetMultiNavigationMenuWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuWidgetCollection. When set to true, NavigationMenuWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNavigationMenuWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuWidgetCollection
		{
			get	{ return _alwaysFetchNavigationMenuWidgetCollection; }
			set	{ _alwaysFetchNavigationMenuWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuWidgetCollection already has been fetched. Setting this property to false when NavigationMenuWidgetCollection has been fetched
		/// will clear the NavigationMenuWidgetCollection collection well. Setting this property to true while NavigationMenuWidgetCollection hasn't been fetched disables lazy loading for NavigationMenuWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuWidgetCollection
		{
			get { return _alreadyFetchedNavigationMenuWidgetCollection;}
			set 
			{
				if(_alreadyFetchedNavigationMenuWidgetCollection && !value && (_navigationMenuWidgetCollection != null))
				{
					_navigationMenuWidgetCollection.Clear();
				}
				_alreadyFetchedNavigationMenuWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NavigationMenuCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.NavigationMenuEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
