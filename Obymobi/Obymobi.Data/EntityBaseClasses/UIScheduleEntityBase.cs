﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UISchedule'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIScheduleEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIScheduleEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientConfigurationCollection	_clientConfigurationCollection;
		private bool	_alwaysFetchClientConfigurationCollection, _alreadyFetchedClientConfigurationCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.TimestampCollection	_timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemCollection	_uIScheduleItemCollection;
		private bool	_alwaysFetchUIScheduleItemCollection, _alreadyFetchedUIScheduleItemCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection	_uIScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchUIScheduleItemOccurrenceCollection, _alreadyFetchedUIScheduleItemOccurrenceCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
			/// <summary>Member name UIScheduleItemCollection</summary>
			public static readonly string UIScheduleItemCollection = "UIScheduleItemCollection";
			/// <summary>Member name UIScheduleItemOccurrenceCollection</summary>
			public static readonly string UIScheduleItemOccurrenceCollection = "UIScheduleItemOccurrenceCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIScheduleEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIScheduleEntityBase() :base("UIScheduleEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		protected UIScheduleEntityBase(System.Int32 uIScheduleId):base("UIScheduleEntity")
		{
			InitClassFetch(uIScheduleId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIScheduleEntityBase(System.Int32 uIScheduleId, IPrefetchPath prefetchPathToUse): base("UIScheduleEntity")
		{
			InitClassFetch(uIScheduleId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="validator">The custom validator object for this UIScheduleEntity</param>
		protected UIScheduleEntityBase(System.Int32 uIScheduleId, IValidator validator):base("UIScheduleEntity")
		{
			InitClassFetch(uIScheduleId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIScheduleEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientConfigurationCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationCollection)info.GetValue("_clientConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationCollection));
			_alwaysFetchClientConfigurationCollection = info.GetBoolean("_alwaysFetchClientConfigurationCollection");
			_alreadyFetchedClientConfigurationCollection = info.GetBoolean("_alreadyFetchedClientConfigurationCollection");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_timestampCollection = (Obymobi.Data.CollectionClasses.TimestampCollection)info.GetValue("_timestampCollection", typeof(Obymobi.Data.CollectionClasses.TimestampCollection));
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");

			_uIScheduleItemCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemCollection)info.GetValue("_uIScheduleItemCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemCollection));
			_alwaysFetchUIScheduleItemCollection = info.GetBoolean("_alwaysFetchUIScheduleItemCollection");
			_alreadyFetchedUIScheduleItemCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemCollection");

			_uIScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection)info.GetValue("_uIScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection));
			_alwaysFetchUIScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchUIScheduleItemOccurrenceCollection");
			_alreadyFetchedUIScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemOccurrenceCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIScheduleFieldIndex)fieldIndex)
			{
				case UIScheduleFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case UIScheduleFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientConfigurationCollection = (_clientConfigurationCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedTimestampCollection = (_timestampCollection.Count > 0);
			_alreadyFetchedUIScheduleItemCollection = (_uIScheduleItemCollection.Count > 0);
			_alreadyFetchedUIScheduleItemOccurrenceCollection = (_uIScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "ClientConfigurationCollection":
					toReturn.Add(Relations.ClientConfigurationEntityUsingUIScheduleId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIScheduleId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingUIScheduleId);
					break;
				case "UIScheduleItemCollection":
					toReturn.Add(Relations.UIScheduleItemEntityUsingUIScheduleId);
					break;
				case "UIScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingUIScheduleId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientConfigurationCollection", (!this.MarkedForDeletion?_clientConfigurationCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationCollection", _alwaysFetchClientConfigurationCollection);
			info.AddValue("_alreadyFetchedClientConfigurationCollection", _alreadyFetchedClientConfigurationCollection);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);
			info.AddValue("_uIScheduleItemCollection", (!this.MarkedForDeletion?_uIScheduleItemCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemCollection", _alwaysFetchUIScheduleItemCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemCollection", _alreadyFetchedUIScheduleItemCollection);
			info.AddValue("_uIScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_uIScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemOccurrenceCollection", _alwaysFetchUIScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemOccurrenceCollection", _alreadyFetchedUIScheduleItemOccurrenceCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "ClientConfigurationCollection":
					_alreadyFetchedClientConfigurationCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationCollection.Add((ClientConfigurationEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					if(entity!=null)
					{
						this.TimestampCollection.Add((TimestampEntity)entity);
					}
					break;
				case "UIScheduleItemCollection":
					_alreadyFetchedUIScheduleItemCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemCollection.Add((UIScheduleItemEntity)entity);
					}
					break;
				case "UIScheduleItemOccurrenceCollection":
					_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "ClientConfigurationCollection":
					_clientConfigurationCollection.Add((ClientConfigurationEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "TimestampCollection":
					_timestampCollection.Add((TimestampEntity)relatedEntity);
					break;
				case "UIScheduleItemCollection":
					_uIScheduleItemCollection.Add((UIScheduleItemEntity)relatedEntity);
					break;
				case "UIScheduleItemOccurrenceCollection":
					_uIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "ClientConfigurationCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					this.PerformRelatedEntityRemoval(_timestampCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientConfigurationCollection);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_timestampCollection);
			toReturn.Add(_uIScheduleItemCollection);
			toReturn.Add(_uIScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleId)
		{
			return FetchUsingPK(uIScheduleId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIScheduleId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIScheduleId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIScheduleId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIScheduleId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIScheduleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationCollection || forceFetch || _alwaysFetchClientConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationCollection);
				_clientConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, filter);
				_clientConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationCollection = true;
			}
			return _clientConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationCollection'. These settings will be taken into account
		/// when the property ClientConfigurationCollection is requested or GetMultiClientConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationCollection.SortClauses=sortClauses;
			_clientConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTimestampCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_timestampCollection);
				_timestampCollection.SuppressClearInGetMulti=!forceFetch;
				_timestampCollection.EntityFactoryToUse = entityFactoryToUse;
				_timestampCollection.GetMultiManyToOne(null, null, this, null, filter);
				_timestampCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTimestampCollection = true;
			}
			return _timestampCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TimestampCollection'. These settings will be taken into account
		/// when the property TimestampCollection is requested or GetMultiTimestampCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTimestampCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_timestampCollection.SortClauses=sortClauses;
			_timestampCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemCollection || forceFetch || _alwaysFetchUIScheduleItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemCollection);
				_uIScheduleItemCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemCollection.GetMultiManyToOne(null, null, null, this, null, filter);
				_uIScheduleItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemCollection = true;
			}
			return _uIScheduleItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemCollection'. These settings will be taken into account
		/// when the property UIScheduleItemCollection is requested or GetMultiUIScheduleItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemCollection.SortClauses=sortClauses;
			_uIScheduleItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchUIScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemOccurrenceCollection);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemOccurrenceCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
			}
			return _uIScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property UIScheduleItemOccurrenceCollection is requested or GetMultiUIScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_uIScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("ClientConfigurationCollection", _clientConfigurationCollection);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			toReturn.Add("UIScheduleItemCollection", _uIScheduleItemCollection);
			toReturn.Add("UIScheduleItemOccurrenceCollection", _uIScheduleItemOccurrenceCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="validator">The validator object for this UIScheduleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIScheduleId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIScheduleId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientConfigurationCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationCollection();
			_clientConfigurationCollection.SetContainingEntityInfo(this, "UIScheduleEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "UIScheduleEntity");

			_timestampCollection = new Obymobi.Data.CollectionClasses.TimestampCollection();
			_timestampCollection.SetContainingEntityInfo(this, "UIScheduleEntity");

			_uIScheduleItemCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemCollection();
			_uIScheduleItemCollection.SetContainingEntityInfo(this, "UIScheduleEntity");

			_uIScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection();
			_uIScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "UIScheduleEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIScheduleRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "UIScheduleCollection", resetFKFields, new int[] { (int)UIScheduleFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIScheduleRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticUIScheduleRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "UIScheduleCollection", resetFKFields, new int[] { (int)UIScheduleFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticUIScheduleRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIScheduleId">PK value for UISchedule which data should be fetched into this UISchedule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIScheduleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIScheduleFieldIndex.UIScheduleId].ForcedCurrentValueWrite(uIScheduleId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIScheduleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIScheduleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIScheduleRelations Relations
		{
			get	{ return new UIScheduleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.UIScheduleItemEntity, 0, null, null, null, "UIScheduleItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "UIScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIScheduleId property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIScheduleId
		{
			get { return (System.Int32)GetValue((int)UIScheduleFieldIndex.UIScheduleId, true); }
			set	{ SetValue((int)UIScheduleFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)UIScheduleFieldIndex.CompanyId, true); }
			set	{ SetValue((int)UIScheduleFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)UIScheduleFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The Name property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UIScheduleFieldIndex.Name, true); }
			set	{ SetValue((int)UIScheduleFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIScheduleFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIScheduleFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIScheduleFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIScheduleFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Type property of the Entity UISchedule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UISchedule"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.UIScheduleType Type
		{
			get { return (Obymobi.Enums.UIScheduleType)GetValue((int)UIScheduleFieldIndex.Type, true); }
			set	{ SetValue((int)UIScheduleFieldIndex.Type, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection ClientConfigurationCollection
		{
			get	{ return GetMultiClientConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationCollection. When set to true, ClientConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationCollection
		{
			get	{ return _alwaysFetchClientConfigurationCollection; }
			set	{ _alwaysFetchClientConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationCollection already has been fetched. Setting this property to false when ClientConfigurationCollection has been fetched
		/// will clear the ClientConfigurationCollection collection well. Setting this property to true while ClientConfigurationCollection hasn't been fetched disables lazy loading for ClientConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationCollection
		{
			get { return _alreadyFetchedClientConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationCollection && !value && (_clientConfigurationCollection != null))
				{
					_clientConfigurationCollection.Clear();
				}
				_alreadyFetchedClientConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection TimestampCollection
		{
			get	{ return GetMultiTimestampCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will clear the TimestampCollection collection well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value && (_timestampCollection != null))
				{
					_timestampCollection.Clear();
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection UIScheduleItemCollection
		{
			get	{ return GetMultiUIScheduleItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemCollection. When set to true, UIScheduleItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemCollection
		{
			get	{ return _alwaysFetchUIScheduleItemCollection; }
			set	{ _alwaysFetchUIScheduleItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemCollection already has been fetched. Setting this property to false when UIScheduleItemCollection has been fetched
		/// will clear the UIScheduleItemCollection collection well. Setting this property to true while UIScheduleItemCollection hasn't been fetched disables lazy loading for UIScheduleItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemCollection
		{
			get { return _alreadyFetchedUIScheduleItemCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemCollection && !value && (_uIScheduleItemCollection != null))
				{
					_uIScheduleItemCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection UIScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiUIScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemOccurrenceCollection. When set to true, UIScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchUIScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchUIScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when UIScheduleItemOccurrenceCollection has been fetched
		/// will clear the UIScheduleItemOccurrenceCollection collection well. Setting this property to true while UIScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for UIScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedUIScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemOccurrenceCollection && !value && (_uIScheduleItemOccurrenceCollection != null))
				{
					_uIScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIScheduleEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
