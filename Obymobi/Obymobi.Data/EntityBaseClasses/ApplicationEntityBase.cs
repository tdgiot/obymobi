﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Application'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ApplicationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ApplicationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ReleaseCollection	_releaseCollection;
		private bool	_alwaysFetchReleaseCollection, _alreadyFetchedReleaseCollection;
		private ReleaseEntity _releaseEntity;
		private bool	_alwaysFetchReleaseEntity, _alreadyFetchedReleaseEntity, _releaseEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ReleaseEntity</summary>
			public static readonly string ReleaseEntity = "ReleaseEntity";
			/// <summary>Member name ReleaseCollection</summary>
			public static readonly string ReleaseCollection = "ReleaseCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ApplicationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ApplicationEntityBase() :base("ApplicationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		protected ApplicationEntityBase(System.Int32 applicationId):base("ApplicationEntity")
		{
			InitClassFetch(applicationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ApplicationEntityBase(System.Int32 applicationId, IPrefetchPath prefetchPathToUse): base("ApplicationEntity")
		{
			InitClassFetch(applicationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="validator">The custom validator object for this ApplicationEntity</param>
		protected ApplicationEntityBase(System.Int32 applicationId, IValidator validator):base("ApplicationEntity")
		{
			InitClassFetch(applicationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApplicationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_releaseCollection = (Obymobi.Data.CollectionClasses.ReleaseCollection)info.GetValue("_releaseCollection", typeof(Obymobi.Data.CollectionClasses.ReleaseCollection));
			_alwaysFetchReleaseCollection = info.GetBoolean("_alwaysFetchReleaseCollection");
			_alreadyFetchedReleaseCollection = info.GetBoolean("_alreadyFetchedReleaseCollection");
			_releaseEntity = (ReleaseEntity)info.GetValue("_releaseEntity", typeof(ReleaseEntity));
			if(_releaseEntity!=null)
			{
				_releaseEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_releaseEntityReturnsNewIfNotFound = info.GetBoolean("_releaseEntityReturnsNewIfNotFound");
			_alwaysFetchReleaseEntity = info.GetBoolean("_alwaysFetchReleaseEntity");
			_alreadyFetchedReleaseEntity = info.GetBoolean("_alreadyFetchedReleaseEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ApplicationFieldIndex)fieldIndex)
			{
				case ApplicationFieldIndex.ReleaseId:
					DesetupSyncReleaseEntity(true, false);
					_alreadyFetchedReleaseEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReleaseCollection = (_releaseCollection.Count > 0);
			_alreadyFetchedReleaseEntity = (_releaseEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ReleaseEntity":
					toReturn.Add(Relations.ReleaseEntityUsingReleaseId);
					break;
				case "ReleaseCollection":
					toReturn.Add(Relations.ReleaseEntityUsingApplicationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_releaseCollection", (!this.MarkedForDeletion?_releaseCollection:null));
			info.AddValue("_alwaysFetchReleaseCollection", _alwaysFetchReleaseCollection);
			info.AddValue("_alreadyFetchedReleaseCollection", _alreadyFetchedReleaseCollection);
			info.AddValue("_releaseEntity", (!this.MarkedForDeletion?_releaseEntity:null));
			info.AddValue("_releaseEntityReturnsNewIfNotFound", _releaseEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReleaseEntity", _alwaysFetchReleaseEntity);
			info.AddValue("_alreadyFetchedReleaseEntity", _alreadyFetchedReleaseEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ReleaseEntity":
					_alreadyFetchedReleaseEntity = true;
					this.ReleaseEntity = (ReleaseEntity)entity;
					break;
				case "ReleaseCollection":
					_alreadyFetchedReleaseCollection = true;
					if(entity!=null)
					{
						this.ReleaseCollection.Add((ReleaseEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ReleaseEntity":
					SetupSyncReleaseEntity(relatedEntity);
					break;
				case "ReleaseCollection":
					_releaseCollection.Add((ReleaseEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ReleaseEntity":
					DesetupSyncReleaseEntity(false, true);
					break;
				case "ReleaseCollection":
					this.PerformRelatedEntityRemoval(_releaseCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_releaseEntity!=null)
			{
				toReturn.Add(_releaseEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_releaseCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationId)
		{
			return FetchUsingPK(applicationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(applicationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(applicationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(applicationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ApplicationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ApplicationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiReleaseCollection(bool forceFetch)
		{
			return GetMultiReleaseCollection(forceFetch, _releaseCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReleaseEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiReleaseCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReleaseCollection(forceFetch, _releaseCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReleaseCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReleaseCollection GetMultiReleaseCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReleaseCollection || forceFetch || _alwaysFetchReleaseCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_releaseCollection);
				_releaseCollection.SuppressClearInGetMulti=!forceFetch;
				_releaseCollection.EntityFactoryToUse = entityFactoryToUse;
				_releaseCollection.GetMultiManyToOne(this, null, filter);
				_releaseCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReleaseCollection = true;
			}
			return _releaseCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReleaseCollection'. These settings will be taken into account
		/// when the property ReleaseCollection is requested or GetMultiReleaseCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReleaseCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_releaseCollection.SortClauses=sortClauses;
			_releaseCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public ReleaseEntity GetSingleReleaseEntity()
		{
			return GetSingleReleaseEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public virtual ReleaseEntity GetSingleReleaseEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReleaseEntity || forceFetch || _alwaysFetchReleaseEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReleaseEntityUsingReleaseId);
				ReleaseEntity newEntity = new ReleaseEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReleaseId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReleaseEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_releaseEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReleaseEntity = newEntity;
				_alreadyFetchedReleaseEntity = fetchResult;
			}
			return _releaseEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ReleaseEntity", _releaseEntity);
			toReturn.Add("ReleaseCollection", _releaseCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="validator">The validator object for this ApplicationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 applicationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(applicationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_releaseCollection = new Obymobi.Data.CollectionClasses.ReleaseCollection();
			_releaseCollection.SetContainingEntityInfo(this, "ApplicationEntity");
			_releaseEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceModel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _releaseEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReleaseEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _releaseEntity, new PropertyChangedEventHandler( OnReleaseEntityPropertyChanged ), "ReleaseEntity", Obymobi.Data.RelationClasses.StaticApplicationRelations.ReleaseEntityUsingReleaseIdStatic, true, signalRelatedEntity, "ApplicationCollection", resetFKFields, new int[] { (int)ApplicationFieldIndex.ReleaseId } );		
			_releaseEntity = null;
		}
		
		/// <summary> setups the sync logic for member _releaseEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReleaseEntity(IEntityCore relatedEntity)
		{
			if(_releaseEntity!=relatedEntity)
			{		
				DesetupSyncReleaseEntity(true, true);
				_releaseEntity = (ReleaseEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _releaseEntity, new PropertyChangedEventHandler( OnReleaseEntityPropertyChanged ), "ReleaseEntity", Obymobi.Data.RelationClasses.StaticApplicationRelations.ReleaseEntityUsingReleaseIdStatic, true, ref _alreadyFetchedReleaseEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReleaseEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 applicationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ApplicationFieldIndex.ApplicationId].ForcedCurrentValueWrite(applicationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateApplicationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ApplicationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ApplicationRelations Relations
		{
			get	{ return new ApplicationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Release' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReleaseCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReleaseCollection(), (IEntityRelation)GetRelationsForField("ReleaseCollection")[0], (int)Obymobi.Data.EntityType.ApplicationEntity, (int)Obymobi.Data.EntityType.ReleaseEntity, 0, null, null, null, "ReleaseCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Release'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReleaseEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReleaseCollection(), (IEntityRelation)GetRelationsForField("ReleaseEntity")[0], (int)Obymobi.Data.EntityType.ApplicationEntity, (int)Obymobi.Data.EntityType.ReleaseEntity, 0, null, null, null, "ReleaseEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ApplicationId property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."ApplicationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ApplicationId
		{
			get { return (System.Int32)GetValue((int)ApplicationFieldIndex.ApplicationId, true); }
			set	{ SetValue((int)ApplicationFieldIndex.ApplicationId, value, true); }
		}

		/// <summary> The Name property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ApplicationFieldIndex.Name, true); }
			set	{ SetValue((int)ApplicationFieldIndex.Name, value, true); }
		}

		/// <summary> The Code property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)ApplicationFieldIndex.Code, true); }
			set	{ SetValue((int)ApplicationFieldIndex.Code, value, true); }
		}

		/// <summary> The Description property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ApplicationFieldIndex.Description, true); }
			set	{ SetValue((int)ApplicationFieldIndex.Description, value, true); }
		}

		/// <summary> The ReleaseId property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."ReleaseId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReleaseId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationFieldIndex.ReleaseId, false); }
			set	{ SetValue((int)ApplicationFieldIndex.ReleaseId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ApplicationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ApplicationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ApplicationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ApplicationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The DeviceModel property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."DeviceModel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.DeviceModel DeviceModel
		{
			get { return (Obymobi.Enums.DeviceModel)GetValue((int)ApplicationFieldIndex.DeviceModel, true); }
			set	{ SetValue((int)ApplicationFieldIndex.DeviceModel, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ApplicationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Application<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Application"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ApplicationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReleaseCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReleaseCollection ReleaseCollection
		{
			get	{ return GetMultiReleaseCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReleaseCollection. When set to true, ReleaseCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReleaseCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReleaseCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReleaseCollection
		{
			get	{ return _alwaysFetchReleaseCollection; }
			set	{ _alwaysFetchReleaseCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReleaseCollection already has been fetched. Setting this property to false when ReleaseCollection has been fetched
		/// will clear the ReleaseCollection collection well. Setting this property to true while ReleaseCollection hasn't been fetched disables lazy loading for ReleaseCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReleaseCollection
		{
			get { return _alreadyFetchedReleaseCollection;}
			set 
			{
				if(_alreadyFetchedReleaseCollection && !value && (_releaseCollection != null))
				{
					_releaseCollection.Clear();
				}
				_alreadyFetchedReleaseCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ReleaseEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReleaseEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReleaseEntity ReleaseEntity
		{
			get	{ return GetSingleReleaseEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReleaseEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApplicationCollection", "ReleaseEntity", _releaseEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReleaseEntity. When set to true, ReleaseEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReleaseEntity is accessed. You can always execute a forced fetch by calling GetSingleReleaseEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReleaseEntity
		{
			get	{ return _alwaysFetchReleaseEntity; }
			set	{ _alwaysFetchReleaseEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReleaseEntity already has been fetched. Setting this property to false when ReleaseEntity has been fetched
		/// will set ReleaseEntity to null as well. Setting this property to true while ReleaseEntity hasn't been fetched disables lazy loading for ReleaseEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReleaseEntity
		{
			get { return _alreadyFetchedReleaseEntity;}
			set 
			{
				if(_alreadyFetchedReleaseEntity && !value)
				{
					this.ReleaseEntity = null;
				}
				_alreadyFetchedReleaseEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReleaseEntity is not found
		/// in the database. When set to true, ReleaseEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReleaseEntityReturnsNewIfNotFound
		{
			get	{ return _releaseEntityReturnsNewIfNotFound; }
			set { _releaseEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ApplicationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
