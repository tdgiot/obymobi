﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AccessCode'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AccessCodeEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AccessCodeEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection	_accessCodeCompanyCollection;
		private bool	_alwaysFetchAccessCodeCompanyCollection, _alreadyFetchedAccessCodeCompanyCollection;
		private Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection	_accessCodePointOfInterestCollection;
		private bool	_alwaysFetchAccessCodePointOfInterestCollection, _alreadyFetchedAccessCodePointOfInterestCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccessCodeCompanyCollection</summary>
			public static readonly string AccessCodeCompanyCollection = "AccessCodeCompanyCollection";
			/// <summary>Member name AccessCodePointOfInterestCollection</summary>
			public static readonly string AccessCodePointOfInterestCollection = "AccessCodePointOfInterestCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AccessCodeEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AccessCodeEntityBase() :base("AccessCodeEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		protected AccessCodeEntityBase(System.Int32 accessCodeId):base("AccessCodeEntity")
		{
			InitClassFetch(accessCodeId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AccessCodeEntityBase(System.Int32 accessCodeId, IPrefetchPath prefetchPathToUse): base("AccessCodeEntity")
		{
			InitClassFetch(accessCodeId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="validator">The custom validator object for this AccessCodeEntity</param>
		protected AccessCodeEntityBase(System.Int32 accessCodeId, IValidator validator):base("AccessCodeEntity")
		{
			InitClassFetch(accessCodeId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccessCodeEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accessCodeCompanyCollection = (Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection)info.GetValue("_accessCodeCompanyCollection", typeof(Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection));
			_alwaysFetchAccessCodeCompanyCollection = info.GetBoolean("_alwaysFetchAccessCodeCompanyCollection");
			_alreadyFetchedAccessCodeCompanyCollection = info.GetBoolean("_alreadyFetchedAccessCodeCompanyCollection");

			_accessCodePointOfInterestCollection = (Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection)info.GetValue("_accessCodePointOfInterestCollection", typeof(Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection));
			_alwaysFetchAccessCodePointOfInterestCollection = info.GetBoolean("_alwaysFetchAccessCodePointOfInterestCollection");
			_alreadyFetchedAccessCodePointOfInterestCollection = info.GetBoolean("_alreadyFetchedAccessCodePointOfInterestCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccessCodeCompanyCollection = (_accessCodeCompanyCollection.Count > 0);
			_alreadyFetchedAccessCodePointOfInterestCollection = (_accessCodePointOfInterestCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccessCodeCompanyCollection":
					toReturn.Add(Relations.AccessCodeCompanyEntityUsingAccessCodeId);
					break;
				case "AccessCodePointOfInterestCollection":
					toReturn.Add(Relations.AccessCodePointOfInterestEntityUsingAccessCodeId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accessCodeCompanyCollection", (!this.MarkedForDeletion?_accessCodeCompanyCollection:null));
			info.AddValue("_alwaysFetchAccessCodeCompanyCollection", _alwaysFetchAccessCodeCompanyCollection);
			info.AddValue("_alreadyFetchedAccessCodeCompanyCollection", _alreadyFetchedAccessCodeCompanyCollection);
			info.AddValue("_accessCodePointOfInterestCollection", (!this.MarkedForDeletion?_accessCodePointOfInterestCollection:null));
			info.AddValue("_alwaysFetchAccessCodePointOfInterestCollection", _alwaysFetchAccessCodePointOfInterestCollection);
			info.AddValue("_alreadyFetchedAccessCodePointOfInterestCollection", _alreadyFetchedAccessCodePointOfInterestCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccessCodeCompanyCollection":
					_alreadyFetchedAccessCodeCompanyCollection = true;
					if(entity!=null)
					{
						this.AccessCodeCompanyCollection.Add((AccessCodeCompanyEntity)entity);
					}
					break;
				case "AccessCodePointOfInterestCollection":
					_alreadyFetchedAccessCodePointOfInterestCollection = true;
					if(entity!=null)
					{
						this.AccessCodePointOfInterestCollection.Add((AccessCodePointOfInterestEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccessCodeCompanyCollection":
					_accessCodeCompanyCollection.Add((AccessCodeCompanyEntity)relatedEntity);
					break;
				case "AccessCodePointOfInterestCollection":
					_accessCodePointOfInterestCollection.Add((AccessCodePointOfInterestEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccessCodeCompanyCollection":
					this.PerformRelatedEntityRemoval(_accessCodeCompanyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AccessCodePointOfInterestCollection":
					this.PerformRelatedEntityRemoval(_accessCodePointOfInterestCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accessCodeCompanyCollection);
			toReturn.Add(_accessCodePointOfInterestCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="code">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCode(System.String code)
		{
			return FetchUsingUCCode( code, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="code">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCode(System.String code, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCCode( code, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="code">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCode(System.String code, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCCode( code, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="code">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCode(System.String code, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((AccessCodeDAO)CreateDAOInstance()).FetchAccessCodeUsingUCCode(this, this.Transaction, code, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accessCodeId)
		{
			return FetchUsingPK(accessCodeId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accessCodeId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(accessCodeId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accessCodeId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(accessCodeId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accessCodeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(accessCodeId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AccessCodeId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AccessCodeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccessCodeCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodeCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection GetMultiAccessCodeCompanyCollection(bool forceFetch)
		{
			return GetMultiAccessCodeCompanyCollection(forceFetch, _accessCodeCompanyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodeCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodeCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection GetMultiAccessCodeCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccessCodeCompanyCollection(forceFetch, _accessCodeCompanyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodeCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection GetMultiAccessCodeCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccessCodeCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodeCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection GetMultiAccessCodeCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccessCodeCompanyCollection || forceFetch || _alwaysFetchAccessCodeCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accessCodeCompanyCollection);
				_accessCodeCompanyCollection.SuppressClearInGetMulti=!forceFetch;
				_accessCodeCompanyCollection.EntityFactoryToUse = entityFactoryToUse;
				_accessCodeCompanyCollection.GetMultiManyToOne(this, null, filter);
				_accessCodeCompanyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAccessCodeCompanyCollection = true;
			}
			return _accessCodeCompanyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccessCodeCompanyCollection'. These settings will be taken into account
		/// when the property AccessCodeCompanyCollection is requested or GetMultiAccessCodeCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccessCodeCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accessCodeCompanyCollection.SortClauses=sortClauses;
			_accessCodeCompanyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodePointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, _accessCodePointOfInterestCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodePointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, _accessCodePointOfInterestCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccessCodePointOfInterestCollection || forceFetch || _alwaysFetchAccessCodePointOfInterestCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accessCodePointOfInterestCollection);
				_accessCodePointOfInterestCollection.SuppressClearInGetMulti=!forceFetch;
				_accessCodePointOfInterestCollection.EntityFactoryToUse = entityFactoryToUse;
				_accessCodePointOfInterestCollection.GetMultiManyToOne(this, null, filter);
				_accessCodePointOfInterestCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAccessCodePointOfInterestCollection = true;
			}
			return _accessCodePointOfInterestCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccessCodePointOfInterestCollection'. These settings will be taken into account
		/// when the property AccessCodePointOfInterestCollection is requested or GetMultiAccessCodePointOfInterestCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccessCodePointOfInterestCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accessCodePointOfInterestCollection.SortClauses=sortClauses;
			_accessCodePointOfInterestCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccessCodeCompanyCollection", _accessCodeCompanyCollection);
			toReturn.Add("AccessCodePointOfInterestCollection", _accessCodePointOfInterestCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="validator">The validator object for this AccessCodeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 accessCodeId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(accessCodeId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accessCodeCompanyCollection = new Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection();
			_accessCodeCompanyCollection.SetContainingEntityInfo(this, "AccessCodeEntity");

			_accessCodePointOfInterestCollection = new Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection();
			_accessCodePointOfInterestCollection.SetContainingEntityInfo(this, "AccessCodeEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
		    AnalyticsEnabled = true;
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccessCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifiedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="accessCodeId">PK value for AccessCode which data should be fetched into this AccessCode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 accessCodeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AccessCodeFieldIndex.AccessCodeId].ForcedCurrentValueWrite(accessCodeId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAccessCodeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AccessCodeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AccessCodeRelations Relations
		{
			get	{ return new AccessCodeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccessCodeCompany' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccessCodeCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection(), (IEntityRelation)GetRelationsForField("AccessCodeCompanyCollection")[0], (int)Obymobi.Data.EntityType.AccessCodeEntity, (int)Obymobi.Data.EntityType.AccessCodeCompanyEntity, 0, null, null, null, "AccessCodeCompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccessCodePointOfInterest' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccessCodePointOfInterestCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection(), (IEntityRelation)GetRelationsForField("AccessCodePointOfInterestCollection")[0], (int)Obymobi.Data.EntityType.AccessCodeEntity, (int)Obymobi.Data.EntityType.AccessCodePointOfInterestEntity, 0, null, null, null, "AccessCodePointOfInterestCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccessCodeId property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."AccessCodeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AccessCodeId
		{
			get { return (System.Int32)GetValue((int)AccessCodeFieldIndex.AccessCodeId, true); }
			set	{ SetValue((int)AccessCodeFieldIndex.AccessCodeId, value, true); }
		}

		/// <summary> The Code property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)AccessCodeFieldIndex.Code, true); }
			set	{ SetValue((int)AccessCodeFieldIndex.Code, value, true); }
		}

		/// <summary> The Type property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AccessCodeType Type
		{
			get { return (Obymobi.Enums.AccessCodeType)GetValue((int)AccessCodeFieldIndex.Type, true); }
			set	{ SetValue((int)AccessCodeFieldIndex.Type, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccessCodeFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AccessCodeFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccessCodeFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AccessCodeFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The AnalyticsEnabled property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."AnalyticsEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnalyticsEnabled
		{
			get { return (System.Boolean)GetValue((int)AccessCodeFieldIndex.AnalyticsEnabled, true); }
			set	{ SetValue((int)AccessCodeFieldIndex.AnalyticsEnabled, value, true); }
		}

		/// <summary> The LastModifiedUTC property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."LastModifiedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModifiedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccessCodeFieldIndex.LastModifiedUTC, false); }
			set	{ SetValue((int)AccessCodeFieldIndex.LastModifiedUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccessCodeFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AccessCodeFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AccessCode<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AccessCode"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccessCodeFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AccessCodeFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccessCodeCompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccessCodeCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AccessCodeCompanyCollection AccessCodeCompanyCollection
		{
			get	{ return GetMultiAccessCodeCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccessCodeCompanyCollection. When set to true, AccessCodeCompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccessCodeCompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAccessCodeCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccessCodeCompanyCollection
		{
			get	{ return _alwaysFetchAccessCodeCompanyCollection; }
			set	{ _alwaysFetchAccessCodeCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccessCodeCompanyCollection already has been fetched. Setting this property to false when AccessCodeCompanyCollection has been fetched
		/// will clear the AccessCodeCompanyCollection collection well. Setting this property to true while AccessCodeCompanyCollection hasn't been fetched disables lazy loading for AccessCodeCompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccessCodeCompanyCollection
		{
			get { return _alreadyFetchedAccessCodeCompanyCollection;}
			set 
			{
				if(_alreadyFetchedAccessCodeCompanyCollection && !value && (_accessCodeCompanyCollection != null))
				{
					_accessCodeCompanyCollection.Clear();
				}
				_alreadyFetchedAccessCodeCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccessCodePointOfInterestCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection AccessCodePointOfInterestCollection
		{
			get	{ return GetMultiAccessCodePointOfInterestCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccessCodePointOfInterestCollection. When set to true, AccessCodePointOfInterestCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccessCodePointOfInterestCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAccessCodePointOfInterestCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccessCodePointOfInterestCollection
		{
			get	{ return _alwaysFetchAccessCodePointOfInterestCollection; }
			set	{ _alwaysFetchAccessCodePointOfInterestCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccessCodePointOfInterestCollection already has been fetched. Setting this property to false when AccessCodePointOfInterestCollection has been fetched
		/// will clear the AccessCodePointOfInterestCollection collection well. Setting this property to true while AccessCodePointOfInterestCollection hasn't been fetched disables lazy loading for AccessCodePointOfInterestCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccessCodePointOfInterestCollection
		{
			get { return _alreadyFetchedAccessCodePointOfInterestCollection;}
			set 
			{
				if(_alreadyFetchedAccessCodePointOfInterestCollection && !value && (_accessCodePointOfInterestCollection != null))
				{
					_accessCodePointOfInterestCollection.Clear();
				}
				_alreadyFetchedAccessCodePointOfInterestCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AccessCodeEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
