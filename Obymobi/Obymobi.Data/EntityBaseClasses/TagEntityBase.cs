﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Tag'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	[DebuggerDisplay("TagId={TagId}, Name={Name}")]
	public abstract partial class TagEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "TagEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationoptionTagCollection	_alterationoptionTagCollection;
		private bool	_alwaysFetchAlterationoptionTagCollection, _alreadyFetchedAlterationoptionTagCollection;
		private Obymobi.Data.CollectionClasses.CategoryTagCollection	_categoryTagCollection;
		private bool	_alwaysFetchCategoryTagCollection, _alreadyFetchedCategoryTagCollection;
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection	_orderitemAlterationitemTagCollection;
		private bool	_alwaysFetchOrderitemAlterationitemTagCollection, _alreadyFetchedOrderitemAlterationitemTagCollection;
		private Obymobi.Data.CollectionClasses.OrderitemTagCollection	_orderitemTagCollection;
		private bool	_alwaysFetchOrderitemTagCollection, _alreadyFetchedOrderitemTagCollection;
		private Obymobi.Data.CollectionClasses.ProductCategoryTagCollection	_productCategoryTagCollection;
		private bool	_alwaysFetchProductCategoryTagCollection, _alreadyFetchedProductCategoryTagCollection;
		private Obymobi.Data.CollectionClasses.ProductTagCollection	_productTagCollection;
		private bool	_alwaysFetchProductTagCollection, _alreadyFetchedProductTagCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name AlterationoptionTagCollection</summary>
			public static readonly string AlterationoptionTagCollection = "AlterationoptionTagCollection";
			/// <summary>Member name CategoryTagCollection</summary>
			public static readonly string CategoryTagCollection = "CategoryTagCollection";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
			/// <summary>Member name ProductTagCollection</summary>
			public static readonly string ProductTagCollection = "ProductTagCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TagEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected TagEntityBase() :base("TagEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		protected TagEntityBase(System.Int32 tagId):base("TagEntity")
		{
			InitClassFetch(tagId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected TagEntityBase(System.Int32 tagId, IPrefetchPath prefetchPathToUse): base("TagEntity")
		{
			InitClassFetch(tagId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="validator">The custom validator object for this TagEntity</param>
		protected TagEntityBase(System.Int32 tagId, IValidator validator):base("TagEntity")
		{
			InitClassFetch(tagId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TagEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationoptionTagCollection = (Obymobi.Data.CollectionClasses.AlterationoptionTagCollection)info.GetValue("_alterationoptionTagCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionTagCollection));
			_alwaysFetchAlterationoptionTagCollection = info.GetBoolean("_alwaysFetchAlterationoptionTagCollection");
			_alreadyFetchedAlterationoptionTagCollection = info.GetBoolean("_alreadyFetchedAlterationoptionTagCollection");

			_categoryTagCollection = (Obymobi.Data.CollectionClasses.CategoryTagCollection)info.GetValue("_categoryTagCollection", typeof(Obymobi.Data.CollectionClasses.CategoryTagCollection));
			_alwaysFetchCategoryTagCollection = info.GetBoolean("_alwaysFetchCategoryTagCollection");
			_alreadyFetchedCategoryTagCollection = info.GetBoolean("_alreadyFetchedCategoryTagCollection");

			_orderitemAlterationitemTagCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection)info.GetValue("_orderitemAlterationitemTagCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection));
			_alwaysFetchOrderitemAlterationitemTagCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemTagCollection");
			_alreadyFetchedOrderitemAlterationitemTagCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemTagCollection");

			_orderitemTagCollection = (Obymobi.Data.CollectionClasses.OrderitemTagCollection)info.GetValue("_orderitemTagCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemTagCollection));
			_alwaysFetchOrderitemTagCollection = info.GetBoolean("_alwaysFetchOrderitemTagCollection");
			_alreadyFetchedOrderitemTagCollection = info.GetBoolean("_alreadyFetchedOrderitemTagCollection");

			_productCategoryTagCollection = (Obymobi.Data.CollectionClasses.ProductCategoryTagCollection)info.GetValue("_productCategoryTagCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategoryTagCollection));
			_alwaysFetchProductCategoryTagCollection = info.GetBoolean("_alwaysFetchProductCategoryTagCollection");
			_alreadyFetchedProductCategoryTagCollection = info.GetBoolean("_alreadyFetchedProductCategoryTagCollection");

			_productTagCollection = (Obymobi.Data.CollectionClasses.ProductTagCollection)info.GetValue("_productTagCollection", typeof(Obymobi.Data.CollectionClasses.ProductTagCollection));
			_alwaysFetchProductTagCollection = info.GetBoolean("_alwaysFetchProductTagCollection");
			_alreadyFetchedProductTagCollection = info.GetBoolean("_alreadyFetchedProductTagCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TagFieldIndex)fieldIndex)
			{
				case TagFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationoptionTagCollection = (_alterationoptionTagCollection.Count > 0);
			_alreadyFetchedCategoryTagCollection = (_categoryTagCollection.Count > 0);
			_alreadyFetchedOrderitemAlterationitemTagCollection = (_orderitemAlterationitemTagCollection.Count > 0);
			_alreadyFetchedOrderitemTagCollection = (_orderitemTagCollection.Count > 0);
			_alreadyFetchedProductCategoryTagCollection = (_productCategoryTagCollection.Count > 0);
			_alreadyFetchedProductTagCollection = (_productTagCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "AlterationoptionTagCollection":
					toReturn.Add(Relations.AlterationoptionTagEntityUsingTagId);
					break;
				case "CategoryTagCollection":
					toReturn.Add(Relations.CategoryTagEntityUsingTagId);
					break;
				case "OrderitemAlterationitemTagCollection":
					toReturn.Add(Relations.OrderitemAlterationitemTagEntityUsingTagId);
					break;
				case "OrderitemTagCollection":
					toReturn.Add(Relations.OrderitemTagEntityUsingTagId);
					break;
				case "ProductCategoryTagCollection":
					toReturn.Add(Relations.ProductCategoryTagEntityUsingTagId);
					break;
				case "ProductTagCollection":
					toReturn.Add(Relations.ProductTagEntityUsingTagId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationoptionTagCollection", (!this.MarkedForDeletion?_alterationoptionTagCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionTagCollection", _alwaysFetchAlterationoptionTagCollection);
			info.AddValue("_alreadyFetchedAlterationoptionTagCollection", _alreadyFetchedAlterationoptionTagCollection);
			info.AddValue("_categoryTagCollection", (!this.MarkedForDeletion?_categoryTagCollection:null));
			info.AddValue("_alwaysFetchCategoryTagCollection", _alwaysFetchCategoryTagCollection);
			info.AddValue("_alreadyFetchedCategoryTagCollection", _alreadyFetchedCategoryTagCollection);
			info.AddValue("_orderitemAlterationitemTagCollection", (!this.MarkedForDeletion?_orderitemAlterationitemTagCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemTagCollection", _alwaysFetchOrderitemAlterationitemTagCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemTagCollection", _alreadyFetchedOrderitemAlterationitemTagCollection);
			info.AddValue("_orderitemTagCollection", (!this.MarkedForDeletion?_orderitemTagCollection:null));
			info.AddValue("_alwaysFetchOrderitemTagCollection", _alwaysFetchOrderitemTagCollection);
			info.AddValue("_alreadyFetchedOrderitemTagCollection", _alreadyFetchedOrderitemTagCollection);
			info.AddValue("_productCategoryTagCollection", (!this.MarkedForDeletion?_productCategoryTagCollection:null));
			info.AddValue("_alwaysFetchProductCategoryTagCollection", _alwaysFetchProductCategoryTagCollection);
			info.AddValue("_alreadyFetchedProductCategoryTagCollection", _alreadyFetchedProductCategoryTagCollection);
			info.AddValue("_productTagCollection", (!this.MarkedForDeletion?_productTagCollection:null));
			info.AddValue("_alwaysFetchProductTagCollection", _alwaysFetchProductTagCollection);
			info.AddValue("_alreadyFetchedProductTagCollection", _alreadyFetchedProductTagCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "AlterationoptionTagCollection":
					_alreadyFetchedAlterationoptionTagCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionTagCollection.Add((AlterationoptionTagEntity)entity);
					}
					break;
				case "CategoryTagCollection":
					_alreadyFetchedCategoryTagCollection = true;
					if(entity!=null)
					{
						this.CategoryTagCollection.Add((CategoryTagEntity)entity);
					}
					break;
				case "OrderitemAlterationitemTagCollection":
					_alreadyFetchedOrderitemAlterationitemTagCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)entity);
					}
					break;
				case "OrderitemTagCollection":
					_alreadyFetchedOrderitemTagCollection = true;
					if(entity!=null)
					{
						this.OrderitemTagCollection.Add((OrderitemTagEntity)entity);
					}
					break;
				case "ProductCategoryTagCollection":
					_alreadyFetchedProductCategoryTagCollection = true;
					if(entity!=null)
					{
						this.ProductCategoryTagCollection.Add((ProductCategoryTagEntity)entity);
					}
					break;
				case "ProductTagCollection":
					_alreadyFetchedProductTagCollection = true;
					if(entity!=null)
					{
						this.ProductTagCollection.Add((ProductTagEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "AlterationoptionTagCollection":
					_alterationoptionTagCollection.Add((AlterationoptionTagEntity)relatedEntity);
					break;
				case "CategoryTagCollection":
					_categoryTagCollection.Add((CategoryTagEntity)relatedEntity);
					break;
				case "OrderitemAlterationitemTagCollection":
					_orderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)relatedEntity);
					break;
				case "OrderitemTagCollection":
					_orderitemTagCollection.Add((OrderitemTagEntity)relatedEntity);
					break;
				case "ProductCategoryTagCollection":
					_productCategoryTagCollection.Add((ProductCategoryTagEntity)relatedEntity);
					break;
				case "ProductTagCollection":
					_productTagCollection.Add((ProductTagEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "AlterationoptionTagCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryTagCollection":
					this.PerformRelatedEntityRemoval(_categoryTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemAlterationitemTagCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemTagCollection":
					this.PerformRelatedEntityRemoval(_orderitemTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCategoryTagCollection":
					this.PerformRelatedEntityRemoval(_productCategoryTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductTagCollection":
					this.PerformRelatedEntityRemoval(_productTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationoptionTagCollection);
			toReturn.Add(_categoryTagCollection);
			toReturn.Add(_orderitemAlterationitemTagCollection);
			toReturn.Add(_orderitemTagCollection);
			toReturn.Add(_productCategoryTagCollection);
			toReturn.Add(_productTagCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 tagId)
		{
			return FetchUsingPK(tagId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 tagId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tagId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 tagId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tagId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 tagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tagId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TagId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TagRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, _alterationoptionTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, _alterationoptionTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionTagCollection GetMultiAlterationoptionTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionTagCollection || forceFetch || _alwaysFetchAlterationoptionTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionTagCollection);
				_alterationoptionTagCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionTagCollection.GetMultiManyToOne(null, this, filter);
				_alterationoptionTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionTagCollection = true;
			}
			return _alterationoptionTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionTagCollection'. These settings will be taken into account
		/// when the property AlterationoptionTagCollection is requested or GetMultiAlterationoptionTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionTagCollection.SortClauses=sortClauses;
			_alterationoptionTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch)
		{
			return GetMultiCategoryTagCollection(forceFetch, _categoryTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryTagCollection(forceFetch, _categoryTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryTagCollection || forceFetch || _alwaysFetchCategoryTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryTagCollection);
				_categoryTagCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryTagCollection.GetMultiManyToOne(null, this, filter);
				_categoryTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryTagCollection = true;
			}
			return _categoryTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryTagCollection'. These settings will be taken into account
		/// when the property CategoryTagCollection is requested or GetMultiCategoryTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryTagCollection.SortClauses=sortClauses;
			_categoryTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemTagCollection || forceFetch || _alwaysFetchOrderitemAlterationitemTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemTagCollection);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemTagCollection.GetMultiManyToOne(null, null, this, filter);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemTagCollection = true;
			}
			return _orderitemAlterationitemTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemTagCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemTagCollection is requested or GetMultiOrderitemAlterationitemTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemTagCollection.SortClauses=sortClauses;
			_orderitemAlterationitemTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch)
		{
			return GetMultiOrderitemTagCollection(forceFetch, _orderitemTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemTagCollection(forceFetch, _orderitemTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemTagCollection || forceFetch || _alwaysFetchOrderitemTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemTagCollection);
				_orderitemTagCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemTagCollection.GetMultiManyToOne(null, null, null, this, filter);
				_orderitemTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemTagCollection = true;
			}
			return _orderitemTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemTagCollection'. These settings will be taken into account
		/// when the property OrderitemTagCollection is requested or GetMultiOrderitemTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemTagCollection.SortClauses=sortClauses;
			_orderitemTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCategoryTagCollection || forceFetch || _alwaysFetchProductCategoryTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCategoryTagCollection);
				_productCategoryTagCollection.SuppressClearInGetMulti=!forceFetch;
				_productCategoryTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCategoryTagCollection.GetMultiManyToOne(null, null, this, filter);
				_productCategoryTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCategoryTagCollection = true;
			}
			return _productCategoryTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCategoryTagCollection'. These settings will be taken into account
		/// when the property ProductCategoryTagCollection is requested or GetMultiProductCategoryTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCategoryTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCategoryTagCollection.SortClauses=sortClauses;
			_productCategoryTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductTagCollection GetMultiProductTagCollection(bool forceFetch)
		{
			return GetMultiProductTagCollection(forceFetch, _productTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductTagCollection GetMultiProductTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductTagCollection(forceFetch, _productTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductTagCollection GetMultiProductTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductTagCollection GetMultiProductTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductTagCollection || forceFetch || _alwaysFetchProductTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productTagCollection);
				_productTagCollection.SuppressClearInGetMulti=!forceFetch;
				_productTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_productTagCollection.GetMultiManyToOne(null, this, filter);
				_productTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductTagCollection = true;
			}
			return _productTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductTagCollection'. These settings will be taken into account
		/// when the property ProductTagCollection is requested or GetMultiProductTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productTagCollection.SortClauses=sortClauses;
			_productTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("AlterationoptionTagCollection", _alterationoptionTagCollection);
			toReturn.Add("CategoryTagCollection", _categoryTagCollection);
			toReturn.Add("OrderitemAlterationitemTagCollection", _orderitemAlterationitemTagCollection);
			toReturn.Add("OrderitemTagCollection", _orderitemTagCollection);
			toReturn.Add("ProductCategoryTagCollection", _productCategoryTagCollection);
			toReturn.Add("ProductTagCollection", _productTagCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="validator">The validator object for this TagEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 tagId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tagId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationoptionTagCollection = new Obymobi.Data.CollectionClasses.AlterationoptionTagCollection();
			_alterationoptionTagCollection.SetContainingEntityInfo(this, "TagEntity");

			_categoryTagCollection = new Obymobi.Data.CollectionClasses.CategoryTagCollection();
			_categoryTagCollection.SetContainingEntityInfo(this, "TagEntity");

			_orderitemAlterationitemTagCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection();
			_orderitemAlterationitemTagCollection.SetContainingEntityInfo(this, "TagEntity");

			_orderitemTagCollection = new Obymobi.Data.CollectionClasses.OrderitemTagCollection();
			_orderitemTagCollection.SetContainingEntityInfo(this, "TagEntity");

			_productCategoryTagCollection = new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection();
			_productCategoryTagCollection.SetContainingEntityInfo(this, "TagEntity");

			_productTagCollection = new Obymobi.Data.CollectionClasses.ProductTagCollection();
			_productTagCollection.SetContainingEntityInfo(this, "TagEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTagRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "TagCollection", resetFKFields, new int[] { (int)TagFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTagRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tagId">PK value for Tag which data should be fetched into this Tag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 tagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TagFieldIndex.TagId].ForcedCurrentValueWrite(tagId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTagDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TagEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TagRelations Relations
		{
			get	{ return new TagRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationoptionTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionTagCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.AlterationoptionTagEntity, 0, null, null, null, "AlterationoptionTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryTagCollection(), (IEntityRelation)GetRelationsForField("CategoryTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.CategoryTagEntity, 0, null, null, null, "CategoryTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity, 0, null, null, null, "OrderitemAlterationitemTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemTagCollection(), (IEntityRelation)GetRelationsForField("OrderitemTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.OrderitemTagEntity, 0, null, null, null, "OrderitemTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.ProductCategoryTagEntity, 0, null, null, null, "ProductCategoryTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductTagCollection(), (IEntityRelation)GetRelationsForField("ProductTagCollection")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.ProductTagEntity, 0, null, null, null, "ProductTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.TagEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The TagId property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."TagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)TagFieldIndex.TagId, true); }
			set	{ SetValue((int)TagFieldIndex.TagId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TagFieldIndex.CompanyId, false); }
			set	{ SetValue((int)TagFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TagFieldIndex.Name, true); }
			set	{ SetValue((int)TagFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)TagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)TagFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Tag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tag"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TagFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionTagCollection AlterationoptionTagCollection
		{
			get	{ return GetMultiAlterationoptionTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionTagCollection. When set to true, AlterationoptionTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionTagCollection
		{
			get	{ return _alwaysFetchAlterationoptionTagCollection; }
			set	{ _alwaysFetchAlterationoptionTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionTagCollection already has been fetched. Setting this property to false when AlterationoptionTagCollection has been fetched
		/// will clear the AlterationoptionTagCollection collection well. Setting this property to true while AlterationoptionTagCollection hasn't been fetched disables lazy loading for AlterationoptionTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionTagCollection
		{
			get { return _alreadyFetchedAlterationoptionTagCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionTagCollection && !value && (_alterationoptionTagCollection != null))
				{
					_alterationoptionTagCollection.Clear();
				}
				_alreadyFetchedAlterationoptionTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryTagCollection CategoryTagCollection
		{
			get	{ return GetMultiCategoryTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryTagCollection. When set to true, CategoryTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryTagCollection
		{
			get	{ return _alwaysFetchCategoryTagCollection; }
			set	{ _alwaysFetchCategoryTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryTagCollection already has been fetched. Setting this property to false when CategoryTagCollection has been fetched
		/// will clear the CategoryTagCollection collection well. Setting this property to true while CategoryTagCollection hasn't been fetched disables lazy loading for CategoryTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryTagCollection
		{
			get { return _alreadyFetchedCategoryTagCollection;}
			set 
			{
				if(_alreadyFetchedCategoryTagCollection && !value && (_categoryTagCollection != null))
				{
					_categoryTagCollection.Clear();
				}
				_alreadyFetchedCategoryTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection OrderitemAlterationitemTagCollection
		{
			get	{ return GetMultiOrderitemAlterationitemTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemTagCollection. When set to true, OrderitemAlterationitemTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemTagCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemTagCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemTagCollection already has been fetched. Setting this property to false when OrderitemAlterationitemTagCollection has been fetched
		/// will clear the OrderitemAlterationitemTagCollection collection well. Setting this property to true while OrderitemAlterationitemTagCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemTagCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemTagCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemTagCollection && !value && (_orderitemAlterationitemTagCollection != null))
				{
					_orderitemAlterationitemTagCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemTagCollection OrderitemTagCollection
		{
			get	{ return GetMultiOrderitemTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemTagCollection. When set to true, OrderitemTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemTagCollection
		{
			get	{ return _alwaysFetchOrderitemTagCollection; }
			set	{ _alwaysFetchOrderitemTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemTagCollection already has been fetched. Setting this property to false when OrderitemTagCollection has been fetched
		/// will clear the OrderitemTagCollection collection well. Setting this property to true while OrderitemTagCollection hasn't been fetched disables lazy loading for OrderitemTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemTagCollection
		{
			get { return _alreadyFetchedOrderitemTagCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemTagCollection && !value && (_orderitemTagCollection != null))
				{
					_orderitemTagCollection.Clear();
				}
				_alreadyFetchedOrderitemTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCategoryTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection ProductCategoryTagCollection
		{
			get	{ return GetMultiProductCategoryTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryTagCollection. When set to true, ProductCategoryTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCategoryTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryTagCollection
		{
			get	{ return _alwaysFetchProductCategoryTagCollection; }
			set	{ _alwaysFetchProductCategoryTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryTagCollection already has been fetched. Setting this property to false when ProductCategoryTagCollection has been fetched
		/// will clear the ProductCategoryTagCollection collection well. Setting this property to true while ProductCategoryTagCollection hasn't been fetched disables lazy loading for ProductCategoryTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryTagCollection
		{
			get { return _alreadyFetchedProductCategoryTagCollection;}
			set 
			{
				if(_alreadyFetchedProductCategoryTagCollection && !value && (_productCategoryTagCollection != null))
				{
					_productCategoryTagCollection.Clear();
				}
				_alreadyFetchedProductCategoryTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductTagCollection ProductTagCollection
		{
			get	{ return GetMultiProductTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTagCollection. When set to true, ProductTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTagCollection
		{
			get	{ return _alwaysFetchProductTagCollection; }
			set	{ _alwaysFetchProductTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTagCollection already has been fetched. Setting this property to false when ProductTagCollection has been fetched
		/// will clear the ProductTagCollection collection well. Setting this property to true while ProductTagCollection hasn't been fetched disables lazy loading for ProductTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTagCollection
		{
			get { return _alreadyFetchedProductTagCollection;}
			set 
			{
				if(_alreadyFetchedProductTagCollection && !value && (_productTagCollection != null))
				{
					_productTagCollection.Clear();
				}
				_alreadyFetchedProductTagCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TagCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.TagEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
