﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AdvertisementTag'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AdvertisementTagEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AdvertisementTagEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection	_advertisementTagAdvertisementCollection;
		private bool	_alwaysFetchAdvertisementTagAdvertisementCollection, _alreadyFetchedAdvertisementTagAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection	_advertisementTagCategoryCollection;
		private bool	_alwaysFetchAdvertisementTagCategoryCollection, _alreadyFetchedAdvertisementTagCategoryCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection	_advertisementTagEntertainmentCollection;
		private bool	_alwaysFetchAdvertisementTagEntertainmentCollection, _alreadyFetchedAdvertisementTagEntertainmentCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection	_advertisementTagGenericproductCollection;
		private bool	_alwaysFetchAdvertisementTagGenericproductCollection, _alreadyFetchedAdvertisementTagGenericproductCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection	_advertisementTagLanguageCollection;
		private bool	_alwaysFetchAdvertisementTagLanguageCollection, _alreadyFetchedAdvertisementTagLanguageCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection	_advertisementTagProductCollection;
		private bool	_alwaysFetchAdvertisementTagProductCollection, _alreadyFetchedAdvertisementTagProductCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementTagAdvertisementCollection</summary>
			public static readonly string AdvertisementTagAdvertisementCollection = "AdvertisementTagAdvertisementCollection";
			/// <summary>Member name AdvertisementTagCategoryCollection</summary>
			public static readonly string AdvertisementTagCategoryCollection = "AdvertisementTagCategoryCollection";
			/// <summary>Member name AdvertisementTagEntertainmentCollection</summary>
			public static readonly string AdvertisementTagEntertainmentCollection = "AdvertisementTagEntertainmentCollection";
			/// <summary>Member name AdvertisementTagGenericproductCollection</summary>
			public static readonly string AdvertisementTagGenericproductCollection = "AdvertisementTagGenericproductCollection";
			/// <summary>Member name AdvertisementTagLanguageCollection</summary>
			public static readonly string AdvertisementTagLanguageCollection = "AdvertisementTagLanguageCollection";
			/// <summary>Member name AdvertisementTagProductCollection</summary>
			public static readonly string AdvertisementTagProductCollection = "AdvertisementTagProductCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AdvertisementTagEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AdvertisementTagEntityBase() :base("AdvertisementTagEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		protected AdvertisementTagEntityBase(System.Int32 advertisementTagId):base("AdvertisementTagEntity")
		{
			InitClassFetch(advertisementTagId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AdvertisementTagEntityBase(System.Int32 advertisementTagId, IPrefetchPath prefetchPathToUse): base("AdvertisementTagEntity")
		{
			InitClassFetch(advertisementTagId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="validator">The custom validator object for this AdvertisementTagEntity</param>
		protected AdvertisementTagEntityBase(System.Int32 advertisementTagId, IValidator validator):base("AdvertisementTagEntity")
		{
			InitClassFetch(advertisementTagId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdvertisementTagEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementTagAdvertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection)info.GetValue("_advertisementTagAdvertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection));
			_alwaysFetchAdvertisementTagAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementTagAdvertisementCollection");
			_alreadyFetchedAdvertisementTagAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagAdvertisementCollection");

			_advertisementTagCategoryCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection)info.GetValue("_advertisementTagCategoryCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection));
			_alwaysFetchAdvertisementTagCategoryCollection = info.GetBoolean("_alwaysFetchAdvertisementTagCategoryCollection");
			_alreadyFetchedAdvertisementTagCategoryCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagCategoryCollection");

			_advertisementTagEntertainmentCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection)info.GetValue("_advertisementTagEntertainmentCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection));
			_alwaysFetchAdvertisementTagEntertainmentCollection = info.GetBoolean("_alwaysFetchAdvertisementTagEntertainmentCollection");
			_alreadyFetchedAdvertisementTagEntertainmentCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagEntertainmentCollection");

			_advertisementTagGenericproductCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection)info.GetValue("_advertisementTagGenericproductCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection));
			_alwaysFetchAdvertisementTagGenericproductCollection = info.GetBoolean("_alwaysFetchAdvertisementTagGenericproductCollection");
			_alreadyFetchedAdvertisementTagGenericproductCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagGenericproductCollection");

			_advertisementTagLanguageCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection)info.GetValue("_advertisementTagLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection));
			_alwaysFetchAdvertisementTagLanguageCollection = info.GetBoolean("_alwaysFetchAdvertisementTagLanguageCollection");
			_alreadyFetchedAdvertisementTagLanguageCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagLanguageCollection");

			_advertisementTagProductCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection)info.GetValue("_advertisementTagProductCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection));
			_alwaysFetchAdvertisementTagProductCollection = info.GetBoolean("_alwaysFetchAdvertisementTagProductCollection");
			_alreadyFetchedAdvertisementTagProductCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagProductCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementTagAdvertisementCollection = (_advertisementTagAdvertisementCollection.Count > 0);
			_alreadyFetchedAdvertisementTagCategoryCollection = (_advertisementTagCategoryCollection.Count > 0);
			_alreadyFetchedAdvertisementTagEntertainmentCollection = (_advertisementTagEntertainmentCollection.Count > 0);
			_alreadyFetchedAdvertisementTagGenericproductCollection = (_advertisementTagGenericproductCollection.Count > 0);
			_alreadyFetchedAdvertisementTagLanguageCollection = (_advertisementTagLanguageCollection.Count > 0);
			_alreadyFetchedAdvertisementTagProductCollection = (_advertisementTagProductCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementTagAdvertisementCollection":
					toReturn.Add(Relations.AdvertisementTagAdvertisementEntityUsingAdvertisementTagId);
					break;
				case "AdvertisementTagCategoryCollection":
					toReturn.Add(Relations.AdvertisementTagCategoryEntityUsingAdvertisementTagId);
					break;
				case "AdvertisementTagEntertainmentCollection":
					toReturn.Add(Relations.AdvertisementTagEntertainmentEntityUsingAdvertisementTagId);
					break;
				case "AdvertisementTagGenericproductCollection":
					toReturn.Add(Relations.AdvertisementTagGenericproductEntityUsingAdvertisementTagId);
					break;
				case "AdvertisementTagLanguageCollection":
					toReturn.Add(Relations.AdvertisementTagLanguageEntityUsingAdvertisementTagId);
					break;
				case "AdvertisementTagProductCollection":
					toReturn.Add(Relations.AdvertisementTagProductEntityUsingAdvertisementTagId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementTagAdvertisementCollection", (!this.MarkedForDeletion?_advertisementTagAdvertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagAdvertisementCollection", _alwaysFetchAdvertisementTagAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagAdvertisementCollection", _alreadyFetchedAdvertisementTagAdvertisementCollection);
			info.AddValue("_advertisementTagCategoryCollection", (!this.MarkedForDeletion?_advertisementTagCategoryCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagCategoryCollection", _alwaysFetchAdvertisementTagCategoryCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagCategoryCollection", _alreadyFetchedAdvertisementTagCategoryCollection);
			info.AddValue("_advertisementTagEntertainmentCollection", (!this.MarkedForDeletion?_advertisementTagEntertainmentCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagEntertainmentCollection", _alwaysFetchAdvertisementTagEntertainmentCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagEntertainmentCollection", _alreadyFetchedAdvertisementTagEntertainmentCollection);
			info.AddValue("_advertisementTagGenericproductCollection", (!this.MarkedForDeletion?_advertisementTagGenericproductCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagGenericproductCollection", _alwaysFetchAdvertisementTagGenericproductCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagGenericproductCollection", _alreadyFetchedAdvertisementTagGenericproductCollection);
			info.AddValue("_advertisementTagLanguageCollection", (!this.MarkedForDeletion?_advertisementTagLanguageCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagLanguageCollection", _alwaysFetchAdvertisementTagLanguageCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagLanguageCollection", _alreadyFetchedAdvertisementTagLanguageCollection);
			info.AddValue("_advertisementTagProductCollection", (!this.MarkedForDeletion?_advertisementTagProductCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagProductCollection", _alwaysFetchAdvertisementTagProductCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagProductCollection", _alreadyFetchedAdvertisementTagProductCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementTagAdvertisementCollection":
					_alreadyFetchedAdvertisementTagAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagAdvertisementCollection.Add((AdvertisementTagAdvertisementEntity)entity);
					}
					break;
				case "AdvertisementTagCategoryCollection":
					_alreadyFetchedAdvertisementTagCategoryCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagCategoryCollection.Add((AdvertisementTagCategoryEntity)entity);
					}
					break;
				case "AdvertisementTagEntertainmentCollection":
					_alreadyFetchedAdvertisementTagEntertainmentCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagEntertainmentCollection.Add((AdvertisementTagEntertainmentEntity)entity);
					}
					break;
				case "AdvertisementTagGenericproductCollection":
					_alreadyFetchedAdvertisementTagGenericproductCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagGenericproductCollection.Add((AdvertisementTagGenericproductEntity)entity);
					}
					break;
				case "AdvertisementTagLanguageCollection":
					_alreadyFetchedAdvertisementTagLanguageCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagLanguageCollection.Add((AdvertisementTagLanguageEntity)entity);
					}
					break;
				case "AdvertisementTagProductCollection":
					_alreadyFetchedAdvertisementTagProductCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagProductCollection.Add((AdvertisementTagProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementTagAdvertisementCollection":
					_advertisementTagAdvertisementCollection.Add((AdvertisementTagAdvertisementEntity)relatedEntity);
					break;
				case "AdvertisementTagCategoryCollection":
					_advertisementTagCategoryCollection.Add((AdvertisementTagCategoryEntity)relatedEntity);
					break;
				case "AdvertisementTagEntertainmentCollection":
					_advertisementTagEntertainmentCollection.Add((AdvertisementTagEntertainmentEntity)relatedEntity);
					break;
				case "AdvertisementTagGenericproductCollection":
					_advertisementTagGenericproductCollection.Add((AdvertisementTagGenericproductEntity)relatedEntity);
					break;
				case "AdvertisementTagLanguageCollection":
					_advertisementTagLanguageCollection.Add((AdvertisementTagLanguageEntity)relatedEntity);
					break;
				case "AdvertisementTagProductCollection":
					_advertisementTagProductCollection.Add((AdvertisementTagProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementTagAdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagAdvertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagCategoryCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagEntertainmentCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagEntertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagGenericproductCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagGenericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagLanguageCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagProductCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementTagAdvertisementCollection);
			toReturn.Add(_advertisementTagCategoryCollection);
			toReturn.Add(_advertisementTagEntertainmentCollection);
			toReturn.Add(_advertisementTagGenericproductCollection);
			toReturn.Add(_advertisementTagLanguageCollection);
			toReturn.Add(_advertisementTagProductCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagId)
		{
			return FetchUsingPK(advertisementTagId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(advertisementTagId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(advertisementTagId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(advertisementTagId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AdvertisementTagId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AdvertisementTagRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagAdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection GetMultiAdvertisementTagAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagAdvertisementCollection(forceFetch, _advertisementTagAdvertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagAdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection GetMultiAdvertisementTagAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagAdvertisementCollection(forceFetch, _advertisementTagAdvertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection GetMultiAdvertisementTagAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection GetMultiAdvertisementTagAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementTagAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagAdvertisementCollection);
				_advertisementTagAdvertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagAdvertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagAdvertisementCollection.GetMultiManyToOne(null, this, filter);
				_advertisementTagAdvertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagAdvertisementCollection = true;
			}
			return _advertisementTagAdvertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagAdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementTagAdvertisementCollection is requested or GetMultiAdvertisementTagAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagAdvertisementCollection.SortClauses=sortClauses;
			_advertisementTagAdvertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, _advertisementTagCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, _advertisementTagCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagCategoryCollection || forceFetch || _alwaysFetchAdvertisementTagCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagCategoryCollection);
				_advertisementTagCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagCategoryCollection.GetMultiManyToOne(this, null, filter);
				_advertisementTagCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagCategoryCollection = true;
			}
			return _advertisementTagCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagCategoryCollection'. These settings will be taken into account
		/// when the property AdvertisementTagCategoryCollection is requested or GetMultiAdvertisementTagCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagCategoryCollection.SortClauses=sortClauses;
			_advertisementTagCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection GetMultiAdvertisementTagEntertainmentCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagEntertainmentCollection(forceFetch, _advertisementTagEntertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection GetMultiAdvertisementTagEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagEntertainmentCollection(forceFetch, _advertisementTagEntertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection GetMultiAdvertisementTagEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection GetMultiAdvertisementTagEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagEntertainmentCollection || forceFetch || _alwaysFetchAdvertisementTagEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagEntertainmentCollection);
				_advertisementTagEntertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagEntertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagEntertainmentCollection.GetMultiManyToOne(this, null, filter);
				_advertisementTagEntertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagEntertainmentCollection = true;
			}
			return _advertisementTagEntertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagEntertainmentCollection'. These settings will be taken into account
		/// when the property AdvertisementTagEntertainmentCollection is requested or GetMultiAdvertisementTagEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagEntertainmentCollection.SortClauses=sortClauses;
			_advertisementTagEntertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagGenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, _advertisementTagGenericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagGenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, _advertisementTagGenericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection GetMultiAdvertisementTagGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagGenericproductCollection || forceFetch || _alwaysFetchAdvertisementTagGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagGenericproductCollection);
				_advertisementTagGenericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagGenericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagGenericproductCollection.GetMultiManyToOne(this, null, filter);
				_advertisementTagGenericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagGenericproductCollection = true;
			}
			return _advertisementTagGenericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagGenericproductCollection'. These settings will be taken into account
		/// when the property AdvertisementTagGenericproductCollection is requested or GetMultiAdvertisementTagGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagGenericproductCollection.SortClauses=sortClauses;
			_advertisementTagGenericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, _advertisementTagLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, _advertisementTagLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagLanguageCollection || forceFetch || _alwaysFetchAdvertisementTagLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagLanguageCollection);
				_advertisementTagLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagLanguageCollection.GetMultiManyToOne(this, null, filter);
				_advertisementTagLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagLanguageCollection = true;
			}
			return _advertisementTagLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagLanguageCollection'. These settings will be taken into account
		/// when the property AdvertisementTagLanguageCollection is requested or GetMultiAdvertisementTagLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagLanguageCollection.SortClauses=sortClauses;
			_advertisementTagLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection GetMultiAdvertisementTagProductCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagProductCollection(forceFetch, _advertisementTagProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection GetMultiAdvertisementTagProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagProductCollection(forceFetch, _advertisementTagProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection GetMultiAdvertisementTagProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection GetMultiAdvertisementTagProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagProductCollection || forceFetch || _alwaysFetchAdvertisementTagProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagProductCollection);
				_advertisementTagProductCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagProductCollection.GetMultiManyToOne(this, null, filter);
				_advertisementTagProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagProductCollection = true;
			}
			return _advertisementTagProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagProductCollection'. These settings will be taken into account
		/// when the property AdvertisementTagProductCollection is requested or GetMultiAdvertisementTagProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagProductCollection.SortClauses=sortClauses;
			_advertisementTagProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementTagAdvertisementCollection", _advertisementTagAdvertisementCollection);
			toReturn.Add("AdvertisementTagCategoryCollection", _advertisementTagCategoryCollection);
			toReturn.Add("AdvertisementTagEntertainmentCollection", _advertisementTagEntertainmentCollection);
			toReturn.Add("AdvertisementTagGenericproductCollection", _advertisementTagGenericproductCollection);
			toReturn.Add("AdvertisementTagLanguageCollection", _advertisementTagLanguageCollection);
			toReturn.Add("AdvertisementTagProductCollection", _advertisementTagProductCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="validator">The validator object for this AdvertisementTagEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 advertisementTagId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(advertisementTagId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementTagAdvertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection();
			_advertisementTagAdvertisementCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");

			_advertisementTagCategoryCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection();
			_advertisementTagCategoryCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");

			_advertisementTagEntertainmentCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection();
			_advertisementTagEntertainmentCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");

			_advertisementTagGenericproductCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection();
			_advertisementTagGenericproductCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");

			_advertisementTagLanguageCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection();
			_advertisementTagLanguageCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");

			_advertisementTagProductCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection();
			_advertisementTagProductCollection.SetContainingEntityInfo(this, "AdvertisementTagEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementTagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="advertisementTagId">PK value for AdvertisementTag which data should be fetched into this AdvertisementTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 advertisementTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AdvertisementTagFieldIndex.AdvertisementTagId].ForcedCurrentValueWrite(advertisementTagId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAdvertisementTagDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AdvertisementTagEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AdvertisementTagRelations Relations
		{
			get	{ return new AdvertisementTagRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagAdvertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagAdvertisementCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagAdvertisementEntity, 0, null, null, null, "AdvertisementTagAdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagCategoryCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagCategoryEntity, 0, null, null, null, "AdvertisementTagCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagEntertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagEntertainmentCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity, 0, null, null, null, "AdvertisementTagEntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagGenericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagGenericproductCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity, 0, null, null, null, "AdvertisementTagGenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagLanguageCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagLanguageEntity, 0, null, null, null, "AdvertisementTagLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagProductCollection")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntity, (int)Obymobi.Data.EntityType.AdvertisementTagProductEntity, 0, null, null, null, "AdvertisementTagProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdvertisementTagId property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."AdvertisementTagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AdvertisementTagId
		{
			get { return (System.Int32)GetValue((int)AdvertisementTagFieldIndex.AdvertisementTagId, true); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.AdvertisementTagId, value, true); }
		}

		/// <summary> The Name property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AdvertisementTagFieldIndex.Name, true); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementTagFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementTagFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)AdvertisementTagFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementTagFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AdvertisementTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTag"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AdvertisementTagFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagAdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagAdvertisementCollection AdvertisementTagAdvertisementCollection
		{
			get	{ return GetMultiAdvertisementTagAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagAdvertisementCollection. When set to true, AdvertisementTagAdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagAdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementTagAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementTagAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagAdvertisementCollection already has been fetched. Setting this property to false when AdvertisementTagAdvertisementCollection has been fetched
		/// will clear the AdvertisementTagAdvertisementCollection collection well. Setting this property to true while AdvertisementTagAdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementTagAdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementTagAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagAdvertisementCollection && !value && (_advertisementTagAdvertisementCollection != null))
				{
					_advertisementTagAdvertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection AdvertisementTagCategoryCollection
		{
			get	{ return GetMultiAdvertisementTagCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagCategoryCollection. When set to true, AdvertisementTagCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagCategoryCollection
		{
			get	{ return _alwaysFetchAdvertisementTagCategoryCollection; }
			set	{ _alwaysFetchAdvertisementTagCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagCategoryCollection already has been fetched. Setting this property to false when AdvertisementTagCategoryCollection has been fetched
		/// will clear the AdvertisementTagCategoryCollection collection well. Setting this property to true while AdvertisementTagCategoryCollection hasn't been fetched disables lazy loading for AdvertisementTagCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagCategoryCollection
		{
			get { return _alreadyFetchedAdvertisementTagCategoryCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagCategoryCollection && !value && (_advertisementTagCategoryCollection != null))
				{
					_advertisementTagCategoryCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagEntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagEntertainmentCollection AdvertisementTagEntertainmentCollection
		{
			get	{ return GetMultiAdvertisementTagEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagEntertainmentCollection. When set to true, AdvertisementTagEntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagEntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagEntertainmentCollection
		{
			get	{ return _alwaysFetchAdvertisementTagEntertainmentCollection; }
			set	{ _alwaysFetchAdvertisementTagEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagEntertainmentCollection already has been fetched. Setting this property to false when AdvertisementTagEntertainmentCollection has been fetched
		/// will clear the AdvertisementTagEntertainmentCollection collection well. Setting this property to true while AdvertisementTagEntertainmentCollection hasn't been fetched disables lazy loading for AdvertisementTagEntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagEntertainmentCollection
		{
			get { return _alreadyFetchedAdvertisementTagEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagEntertainmentCollection && !value && (_advertisementTagEntertainmentCollection != null))
				{
					_advertisementTagEntertainmentCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagEntertainmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagGenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagGenericproductCollection AdvertisementTagGenericproductCollection
		{
			get	{ return GetMultiAdvertisementTagGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagGenericproductCollection. When set to true, AdvertisementTagGenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagGenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagGenericproductCollection
		{
			get	{ return _alwaysFetchAdvertisementTagGenericproductCollection; }
			set	{ _alwaysFetchAdvertisementTagGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagGenericproductCollection already has been fetched. Setting this property to false when AdvertisementTagGenericproductCollection has been fetched
		/// will clear the AdvertisementTagGenericproductCollection collection well. Setting this property to true while AdvertisementTagGenericproductCollection hasn't been fetched disables lazy loading for AdvertisementTagGenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagGenericproductCollection
		{
			get { return _alreadyFetchedAdvertisementTagGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagGenericproductCollection && !value && (_advertisementTagGenericproductCollection != null))
				{
					_advertisementTagGenericproductCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagGenericproductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection AdvertisementTagLanguageCollection
		{
			get	{ return GetMultiAdvertisementTagLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagLanguageCollection. When set to true, AdvertisementTagLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagLanguageCollection
		{
			get	{ return _alwaysFetchAdvertisementTagLanguageCollection; }
			set	{ _alwaysFetchAdvertisementTagLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagLanguageCollection already has been fetched. Setting this property to false when AdvertisementTagLanguageCollection has been fetched
		/// will clear the AdvertisementTagLanguageCollection collection well. Setting this property to true while AdvertisementTagLanguageCollection hasn't been fetched disables lazy loading for AdvertisementTagLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagLanguageCollection
		{
			get { return _alreadyFetchedAdvertisementTagLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagLanguageCollection && !value && (_advertisementTagLanguageCollection != null))
				{
					_advertisementTagLanguageCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagProductCollection AdvertisementTagProductCollection
		{
			get	{ return GetMultiAdvertisementTagProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagProductCollection. When set to true, AdvertisementTagProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagProductCollection
		{
			get	{ return _alwaysFetchAdvertisementTagProductCollection; }
			set	{ _alwaysFetchAdvertisementTagProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagProductCollection already has been fetched. Setting this property to false when AdvertisementTagProductCollection has been fetched
		/// will clear the AdvertisementTagProductCollection collection well. Setting this property to true while AdvertisementTagProductCollection hasn't been fetched disables lazy loading for AdvertisementTagProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagProductCollection
		{
			get { return _alreadyFetchedAdvertisementTagProductCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagProductCollection && !value && (_advertisementTagProductCollection != null))
				{
					_advertisementTagProductCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagProductCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AdvertisementTagEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
