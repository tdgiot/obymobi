﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Vattariff'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class VattariffEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "VattariffEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.GenericproductCollection	_genericproductCollection;
		private bool	_alwaysFetchGenericproductCollection, _alreadyFetchedGenericproductCollection;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaProduct;
		private bool	_alwaysFetchGenericproductCollectionViaProduct, _alreadyFetchedGenericproductCollectionViaProduct;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaProduct;
		private bool	_alwaysFetchRouteCollectionViaProduct, _alreadyFetchedRouteCollectionViaProduct;
		private CountryEntity _countryEntity;
		private bool	_alwaysFetchCountryEntity, _alreadyFetchedCountryEntity, _countryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CountryEntity</summary>
			public static readonly string CountryEntity = "CountryEntity";
			/// <summary>Member name GenericproductCollection</summary>
			public static readonly string GenericproductCollection = "GenericproductCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name GenericproductCollectionViaProduct</summary>
			public static readonly string GenericproductCollectionViaProduct = "GenericproductCollectionViaProduct";
			/// <summary>Member name RouteCollectionViaProduct</summary>
			public static readonly string RouteCollectionViaProduct = "RouteCollectionViaProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VattariffEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected VattariffEntityBase() :base("VattariffEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		protected VattariffEntityBase(System.Int32 vattariffId):base("VattariffEntity")
		{
			InitClassFetch(vattariffId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected VattariffEntityBase(System.Int32 vattariffId, IPrefetchPath prefetchPathToUse): base("VattariffEntity")
		{
			InitClassFetch(vattariffId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="validator">The custom validator object for this VattariffEntity</param>
		protected VattariffEntityBase(System.Int32 vattariffId, IValidator validator):base("VattariffEntity")
		{
			InitClassFetch(vattariffId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VattariffEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_genericproductCollection = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollection = info.GetBoolean("_alwaysFetchGenericproductCollection");
			_alreadyFetchedGenericproductCollection = info.GetBoolean("_alreadyFetchedGenericproductCollection");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");
			_genericproductCollectionViaProduct = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaProduct = info.GetBoolean("_alwaysFetchGenericproductCollectionViaProduct");
			_alreadyFetchedGenericproductCollectionViaProduct = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaProduct");

			_routeCollectionViaProduct = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaProduct = info.GetBoolean("_alwaysFetchRouteCollectionViaProduct");
			_alreadyFetchedRouteCollectionViaProduct = info.GetBoolean("_alreadyFetchedRouteCollectionViaProduct");
			_countryEntity = (CountryEntity)info.GetValue("_countryEntity", typeof(CountryEntity));
			if(_countryEntity!=null)
			{
				_countryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_countryEntityReturnsNewIfNotFound = info.GetBoolean("_countryEntityReturnsNewIfNotFound");
			_alwaysFetchCountryEntity = info.GetBoolean("_alwaysFetchCountryEntity");
			_alreadyFetchedCountryEntity = info.GetBoolean("_alreadyFetchedCountryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VattariffFieldIndex)fieldIndex)
			{
				case VattariffFieldIndex.CountryId:
					DesetupSyncCountryEntity(true, false);
					_alreadyFetchedCountryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedGenericproductCollection = (_genericproductCollection.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedGenericproductCollectionViaProduct = (_genericproductCollectionViaProduct.Count > 0);
			_alreadyFetchedRouteCollectionViaProduct = (_routeCollectionViaProduct.Count > 0);
			_alreadyFetchedCountryEntity = (_countryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CountryEntity":
					toReturn.Add(Relations.CountryEntityUsingCountryId);
					break;
				case "GenericproductCollection":
					toReturn.Add(Relations.GenericproductEntityUsingVattariffId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingVattariffId);
					break;
				case "GenericproductCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingVattariffId, "VattariffEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.GenericproductEntityUsingGenericproductId, "Product_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingVattariffId, "VattariffEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.RouteEntityUsingRouteId, "Product_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_genericproductCollection", (!this.MarkedForDeletion?_genericproductCollection:null));
			info.AddValue("_alwaysFetchGenericproductCollection", _alwaysFetchGenericproductCollection);
			info.AddValue("_alreadyFetchedGenericproductCollection", _alreadyFetchedGenericproductCollection);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_genericproductCollectionViaProduct", (!this.MarkedForDeletion?_genericproductCollectionViaProduct:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaProduct", _alwaysFetchGenericproductCollectionViaProduct);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaProduct", _alreadyFetchedGenericproductCollectionViaProduct);
			info.AddValue("_routeCollectionViaProduct", (!this.MarkedForDeletion?_routeCollectionViaProduct:null));
			info.AddValue("_alwaysFetchRouteCollectionViaProduct", _alwaysFetchRouteCollectionViaProduct);
			info.AddValue("_alreadyFetchedRouteCollectionViaProduct", _alreadyFetchedRouteCollectionViaProduct);
			info.AddValue("_countryEntity", (!this.MarkedForDeletion?_countryEntity:null));
			info.AddValue("_countryEntityReturnsNewIfNotFound", _countryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCountryEntity", _alwaysFetchCountryEntity);
			info.AddValue("_alreadyFetchedCountryEntity", _alreadyFetchedCountryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CountryEntity":
					_alreadyFetchedCountryEntity = true;
					this.CountryEntity = (CountryEntity)entity;
					break;
				case "GenericproductCollection":
					_alreadyFetchedGenericproductCollection = true;
					if(entity!=null)
					{
						this.GenericproductCollection.Add((GenericproductEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "GenericproductCollectionViaProduct":
					_alreadyFetchedGenericproductCollectionViaProduct = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaProduct.Add((GenericproductEntity)entity);
					}
					break;
				case "RouteCollectionViaProduct":
					_alreadyFetchedRouteCollectionViaProduct = true;
					if(entity!=null)
					{
						this.RouteCollectionViaProduct.Add((RouteEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CountryEntity":
					SetupSyncCountryEntity(relatedEntity);
					break;
				case "GenericproductCollection":
					_genericproductCollection.Add((GenericproductEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CountryEntity":
					DesetupSyncCountryEntity(false, true);
					break;
				case "GenericproductCollection":
					this.PerformRelatedEntityRemoval(_genericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_countryEntity!=null)
			{
				toReturn.Add(_countryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_genericproductCollection);
			toReturn.Add(_productCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 vattariffId)
		{
			return FetchUsingPK(vattariffId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 vattariffId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(vattariffId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 vattariffId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(vattariffId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 vattariffId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(vattariffId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.VattariffId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VattariffRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductCollection || forceFetch || _alwaysFetchGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollection);
				_genericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollection.GetMultiManyToOne(null, null, null, this, filter);
				_genericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollection = true;
			}
			return _genericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollection'. These settings will be taken into account
		/// when the property GenericproductCollection is requested or GetMultiGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollection.SortClauses=sortClauses;
			_genericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaProduct(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaProduct(forceFetch, _genericproductCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaProduct || forceFetch || _alwaysFetchGenericproductCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(VattariffFields.VattariffId, ComparisonOperator.Equal, this.VattariffId, "VattariffEntity__"));
				_genericproductCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaProduct.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaProduct"));
				_genericproductCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaProduct = true;
			}
			return _genericproductCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaProduct'. These settings will be taken into account
		/// when the property GenericproductCollectionViaProduct is requested or GetMultiGenericproductCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaProduct.SortClauses=sortClauses;
			_genericproductCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch)
		{
			return GetMultiRouteCollectionViaProduct(forceFetch, _routeCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaProduct || forceFetch || _alwaysFetchRouteCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(VattariffFields.VattariffId, ComparisonOperator.Equal, this.VattariffId, "VattariffEntity__"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaProduct.GetMulti(filter, GetRelationsForField("RouteCollectionViaProduct"));
				_routeCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaProduct = true;
			}
			return _routeCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaProduct'. These settings will be taken into account
		/// when the property RouteCollectionViaProduct is requested or GetMultiRouteCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaProduct.SortClauses=sortClauses;
			_routeCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CountryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CountryEntity' which is related to this entity.</returns>
		public CountryEntity GetSingleCountryEntity()
		{
			return GetSingleCountryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CountryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CountryEntity' which is related to this entity.</returns>
		public virtual CountryEntity GetSingleCountryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCountryEntity || forceFetch || _alwaysFetchCountryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CountryEntityUsingCountryId);
				CountryEntity newEntity = new CountryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CountryId);
				}
				if(fetchResult)
				{
					newEntity = (CountryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_countryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CountryEntity = newEntity;
				_alreadyFetchedCountryEntity = fetchResult;
			}
			return _countryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CountryEntity", _countryEntity);
			toReturn.Add("GenericproductCollection", _genericproductCollection);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("GenericproductCollectionViaProduct", _genericproductCollectionViaProduct);
			toReturn.Add("RouteCollectionViaProduct", _routeCollectionViaProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="validator">The validator object for this VattariffEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 vattariffId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(vattariffId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_genericproductCollection = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollection.SetContainingEntityInfo(this, "VattariffEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "VattariffEntity");
			_genericproductCollectionViaProduct = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_routeCollectionViaProduct = new Obymobi.Data.CollectionClasses.RouteCollection();
			_countryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VattariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Percentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _countryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCountryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _countryEntity, new PropertyChangedEventHandler( OnCountryEntityPropertyChanged ), "CountryEntity", Obymobi.Data.RelationClasses.StaticVattariffRelations.CountryEntityUsingCountryIdStatic, true, signalRelatedEntity, "VattariffCollection", resetFKFields, new int[] { (int)VattariffFieldIndex.CountryId } );		
			_countryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _countryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCountryEntity(IEntityCore relatedEntity)
		{
			if(_countryEntity!=relatedEntity)
			{		
				DesetupSyncCountryEntity(true, true);
				_countryEntity = (CountryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _countryEntity, new PropertyChangedEventHandler( OnCountryEntityPropertyChanged ), "CountryEntity", Obymobi.Data.RelationClasses.StaticVattariffRelations.CountryEntityUsingCountryIdStatic, true, ref _alreadyFetchedCountryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCountryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="vattariffId">PK value for Vattariff which data should be fetched into this Vattariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 vattariffId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VattariffFieldIndex.VattariffId].ForcedCurrentValueWrite(vattariffId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVattariffDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VattariffEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VattariffRelations Relations
		{
			get	{ return new VattariffRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductCollection")[0], (int)Obymobi.Data.EntityType.VattariffEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.VattariffEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingVattariffId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.VattariffEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaProduct"), "GenericproductCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingVattariffId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.VattariffEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaProduct"), "RouteCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), (IEntityRelation)GetRelationsForField("CountryEntity")[0], (int)Obymobi.Data.EntityType.VattariffEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, null, "CountryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The VattariffId property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."VattariffId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 VattariffId
		{
			get { return (System.Int32)GetValue((int)VattariffFieldIndex.VattariffId, true); }
			set	{ SetValue((int)VattariffFieldIndex.VattariffId, value, true); }
		}

		/// <summary> The CountryId property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."CountryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CountryId
		{
			get { return (System.Int32)GetValue((int)VattariffFieldIndex.CountryId, true); }
			set	{ SetValue((int)VattariffFieldIndex.CountryId, value, true); }
		}

		/// <summary> The Name property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)VattariffFieldIndex.Name, true); }
			set	{ SetValue((int)VattariffFieldIndex.Name, value, true); }
		}

		/// <summary> The Percentage property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."Percentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Percentage
		{
			get { return (System.Double)GetValue((int)VattariffFieldIndex.Percentage, true); }
			set	{ SetValue((int)VattariffFieldIndex.Percentage, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)VattariffFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)VattariffFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)VattariffFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)VattariffFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VattariffFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)VattariffFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Vattariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Vattariff"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VattariffFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)VattariffFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollection
		{
			get	{ return GetMultiGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollection. When set to true, GenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollection
		{
			get	{ return _alwaysFetchGenericproductCollection; }
			set	{ _alwaysFetchGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollection already has been fetched. Setting this property to false when GenericproductCollection has been fetched
		/// will clear the GenericproductCollection collection well. Setting this property to true while GenericproductCollection hasn't been fetched disables lazy loading for GenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollection
		{
			get { return _alreadyFetchedGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductCollection && !value && (_genericproductCollection != null))
				{
					_genericproductCollection.Clear();
				}
				_alreadyFetchedGenericproductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaProduct
		{
			get { return GetMultiGenericproductCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaProduct. When set to true, GenericproductCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaProduct
		{
			get	{ return _alwaysFetchGenericproductCollectionViaProduct; }
			set	{ _alwaysFetchGenericproductCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaProduct already has been fetched. Setting this property to false when GenericproductCollectionViaProduct has been fetched
		/// will clear the GenericproductCollectionViaProduct collection well. Setting this property to true while GenericproductCollectionViaProduct hasn't been fetched disables lazy loading for GenericproductCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaProduct
		{
			get { return _alreadyFetchedGenericproductCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaProduct && !value && (_genericproductCollectionViaProduct != null))
				{
					_genericproductCollectionViaProduct.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaProduct
		{
			get { return GetMultiRouteCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaProduct. When set to true, RouteCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaProduct
		{
			get	{ return _alwaysFetchRouteCollectionViaProduct; }
			set	{ _alwaysFetchRouteCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaProduct already has been fetched. Setting this property to false when RouteCollectionViaProduct has been fetched
		/// will clear the RouteCollectionViaProduct collection well. Setting this property to true while RouteCollectionViaProduct hasn't been fetched disables lazy loading for RouteCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaProduct
		{
			get { return _alreadyFetchedRouteCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaProduct && !value && (_routeCollectionViaProduct != null))
				{
					_routeCollectionViaProduct.Clear();
				}
				_alreadyFetchedRouteCollectionViaProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CountryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCountryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CountryEntity CountryEntity
		{
			get	{ return GetSingleCountryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCountryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VattariffCollection", "CountryEntity", _countryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CountryEntity. When set to true, CountryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryEntity is accessed. You can always execute a forced fetch by calling GetSingleCountryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryEntity
		{
			get	{ return _alwaysFetchCountryEntity; }
			set	{ _alwaysFetchCountryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryEntity already has been fetched. Setting this property to false when CountryEntity has been fetched
		/// will set CountryEntity to null as well. Setting this property to true while CountryEntity hasn't been fetched disables lazy loading for CountryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryEntity
		{
			get { return _alreadyFetchedCountryEntity;}
			set 
			{
				if(_alreadyFetchedCountryEntity && !value)
				{
					this.CountryEntity = null;
				}
				_alreadyFetchedCountryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CountryEntity is not found
		/// in the database. When set to true, CountryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CountryEntityReturnsNewIfNotFound
		{
			get	{ return _countryEntityReturnsNewIfNotFound; }
			set { _countryEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.VattariffEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
