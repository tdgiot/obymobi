﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Account'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AccountEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AccountEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AccountCompanyCollection	_accountCompanyCollection;
		private bool	_alwaysFetchAccountCompanyCollection, _alreadyFetchedAccountCompanyCollection;
		private Obymobi.Data.CollectionClasses.UserCollection	_userCollection;
		private bool	_alwaysFetchUserCollection, _alreadyFetchedUserCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccountCompanyCollection</summary>
			public static readonly string AccountCompanyCollection = "AccountCompanyCollection";
			/// <summary>Member name UserCollection</summary>
			public static readonly string UserCollection = "UserCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AccountEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AccountEntityBase() :base("AccountEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		protected AccountEntityBase(System.Int32 accountId):base("AccountEntity")
		{
			InitClassFetch(accountId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AccountEntityBase(System.Int32 accountId, IPrefetchPath prefetchPathToUse): base("AccountEntity")
		{
			InitClassFetch(accountId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="validator">The custom validator object for this AccountEntity</param>
		protected AccountEntityBase(System.Int32 accountId, IValidator validator):base("AccountEntity")
		{
			InitClassFetch(accountId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccountEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountCompanyCollection = (Obymobi.Data.CollectionClasses.AccountCompanyCollection)info.GetValue("_accountCompanyCollection", typeof(Obymobi.Data.CollectionClasses.AccountCompanyCollection));
			_alwaysFetchAccountCompanyCollection = info.GetBoolean("_alwaysFetchAccountCompanyCollection");
			_alreadyFetchedAccountCompanyCollection = info.GetBoolean("_alreadyFetchedAccountCompanyCollection");

			_userCollection = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollection", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollection = info.GetBoolean("_alwaysFetchUserCollection");
			_alreadyFetchedUserCollection = info.GetBoolean("_alreadyFetchedUserCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountCompanyCollection = (_accountCompanyCollection.Count > 0);
			_alreadyFetchedUserCollection = (_userCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccountCompanyCollection":
					toReturn.Add(Relations.AccountCompanyEntityUsingAccountId);
					break;
				case "UserCollection":
					toReturn.Add(Relations.UserEntityUsingAccountId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountCompanyCollection", (!this.MarkedForDeletion?_accountCompanyCollection:null));
			info.AddValue("_alwaysFetchAccountCompanyCollection", _alwaysFetchAccountCompanyCollection);
			info.AddValue("_alreadyFetchedAccountCompanyCollection", _alreadyFetchedAccountCompanyCollection);
			info.AddValue("_userCollection", (!this.MarkedForDeletion?_userCollection:null));
			info.AddValue("_alwaysFetchUserCollection", _alwaysFetchUserCollection);
			info.AddValue("_alreadyFetchedUserCollection", _alreadyFetchedUserCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccountCompanyCollection":
					_alreadyFetchedAccountCompanyCollection = true;
					if(entity!=null)
					{
						this.AccountCompanyCollection.Add((AccountCompanyEntity)entity);
					}
					break;
				case "UserCollection":
					_alreadyFetchedUserCollection = true;
					if(entity!=null)
					{
						this.UserCollection.Add((UserEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccountCompanyCollection":
					_accountCompanyCollection.Add((AccountCompanyEntity)relatedEntity);
					break;
				case "UserCollection":
					_userCollection.Add((UserEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccountCompanyCollection":
					this.PerformRelatedEntityRemoval(_accountCompanyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserCollection":
					this.PerformRelatedEntityRemoval(_userCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountCompanyCollection);
			toReturn.Add(_userCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accountId)
		{
			return FetchUsingPK(accountId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accountId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(accountId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accountId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(accountId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 accountId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(accountId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AccountId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AccountRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccountCompanyCollection GetMultiAccountCompanyCollection(bool forceFetch)
		{
			return GetMultiAccountCompanyCollection(forceFetch, _accountCompanyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountCompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccountCompanyCollection GetMultiAccountCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountCompanyCollection(forceFetch, _accountCompanyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AccountCompanyCollection GetMultiAccountCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountCompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AccountCompanyCollection GetMultiAccountCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountCompanyCollection || forceFetch || _alwaysFetchAccountCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountCompanyCollection);
				_accountCompanyCollection.SuppressClearInGetMulti=!forceFetch;
				_accountCompanyCollection.EntityFactoryToUse = entityFactoryToUse;
				_accountCompanyCollection.GetMultiManyToOne(this, null, filter);
				_accountCompanyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountCompanyCollection = true;
			}
			return _accountCompanyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountCompanyCollection'. These settings will be taken into account
		/// when the property AccountCompanyCollection is requested or GetMultiAccountCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountCompanyCollection.SortClauses=sortClauses;
			_accountCompanyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollection(bool forceFetch)
		{
			return GetMultiUserCollection(forceFetch, _userCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserCollection(forceFetch, _userCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserCollection || forceFetch || _alwaysFetchUserCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollection);
				_userCollection.SuppressClearInGetMulti=!forceFetch;
				_userCollection.EntityFactoryToUse = entityFactoryToUse;
				_userCollection.GetMultiManyToOne(this, null, filter);
				_userCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollection = true;
			}
			return _userCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollection'. These settings will be taken into account
		/// when the property UserCollection is requested or GetMultiUserCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollection.SortClauses=sortClauses;
			_userCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccountCompanyCollection", _accountCompanyCollection);
			toReturn.Add("UserCollection", _userCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="validator">The validator object for this AccountEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 accountId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(accountId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountCompanyCollection = new Obymobi.Data.CollectionClasses.AccountCompanyCollection();
			_accountCompanyCollection.SetContainingEntityInfo(this, "AccountEntity");

			_userCollection = new Obymobi.Data.CollectionClasses.UserCollection();
			_userCollection.SetContainingEntityInfo(this, "AccountEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="accountId">PK value for Account which data should be fetched into this Account object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 accountId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AccountFieldIndex.AccountId].ForcedCurrentValueWrite(accountId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAccountDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AccountEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AccountRelations Relations
		{
			get	{ return new AccountRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountCompany' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AccountCompanyCollection(), (IEntityRelation)GetRelationsForField("AccountCompanyCollection")[0], (int)Obymobi.Data.EntityType.AccountEntity, (int)Obymobi.Data.EntityType.AccountCompanyEntity, 0, null, null, null, "AccountCompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), (IEntityRelation)GetRelationsForField("UserCollection")[0], (int)Obymobi.Data.EntityType.AccountEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, null, "UserCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountId property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."AccountId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AccountId
		{
			get { return (System.Int32)GetValue((int)AccountFieldIndex.AccountId, true); }
			set	{ SetValue((int)AccountFieldIndex.AccountId, value, true); }
		}

		/// <summary> The Name property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AccountFieldIndex.Name, true); }
			set	{ SetValue((int)AccountFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AccountFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AccountFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AccountFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Account<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Account"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AccountFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountCompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AccountCompanyCollection AccountCompanyCollection
		{
			get	{ return GetMultiAccountCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountCompanyCollection. When set to true, AccountCompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountCompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAccountCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountCompanyCollection
		{
			get	{ return _alwaysFetchAccountCompanyCollection; }
			set	{ _alwaysFetchAccountCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountCompanyCollection already has been fetched. Setting this property to false when AccountCompanyCollection has been fetched
		/// will clear the AccountCompanyCollection collection well. Setting this property to true while AccountCompanyCollection hasn't been fetched disables lazy loading for AccountCompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountCompanyCollection
		{
			get { return _alreadyFetchedAccountCompanyCollection;}
			set 
			{
				if(_alreadyFetchedAccountCompanyCollection && !value && (_accountCompanyCollection != null))
				{
					_accountCompanyCollection.Clear();
				}
				_alreadyFetchedAccountCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollection
		{
			get	{ return GetMultiUserCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollection. When set to true, UserCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollection
		{
			get	{ return _alwaysFetchUserCollection; }
			set	{ _alwaysFetchUserCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollection already has been fetched. Setting this property to false when UserCollection has been fetched
		/// will clear the UserCollection collection well. Setting this property to true while UserCollection hasn't been fetched disables lazy loading for UserCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollection
		{
			get { return _alreadyFetchedUserCollection;}
			set 
			{
				if(_alreadyFetchedUserCollection && !value && (_userCollection != null))
				{
					_userCollection.Clear();
				}
				_alreadyFetchedUserCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AccountEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
