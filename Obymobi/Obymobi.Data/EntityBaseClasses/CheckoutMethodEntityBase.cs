﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'CheckoutMethod'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CheckoutMethodEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CheckoutMethodEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection	_checkoutMethodDeliverypointgroupCollection;
		private bool	_alwaysFetchCheckoutMethodDeliverypointgroupCollection, _alreadyFetchedCheckoutMethodDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private OutletEntity _outletEntity;
		private bool	_alwaysFetchOutletEntity, _alreadyFetchedOutletEntity, _outletEntityReturnsNewIfNotFound;
		private PaymentIntegrationConfigurationEntity _paymentIntegrationConfigurationEntity;
		private bool	_alwaysFetchPaymentIntegrationConfigurationEntity, _alreadyFetchedPaymentIntegrationConfigurationEntity, _paymentIntegrationConfigurationEntityReturnsNewIfNotFound;
		private ReceiptTemplateEntity _receiptTemplateEntity;
		private bool	_alwaysFetchReceiptTemplateEntity, _alreadyFetchedReceiptTemplateEntity, _receiptTemplateEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name OutletEntity</summary>
			public static readonly string OutletEntity = "OutletEntity";
			/// <summary>Member name PaymentIntegrationConfigurationEntity</summary>
			public static readonly string PaymentIntegrationConfigurationEntity = "PaymentIntegrationConfigurationEntity";
			/// <summary>Member name ReceiptTemplateEntity</summary>
			public static readonly string ReceiptTemplateEntity = "ReceiptTemplateEntity";
			/// <summary>Member name CheckoutMethodDeliverypointgroupCollection</summary>
			public static readonly string CheckoutMethodDeliverypointgroupCollection = "CheckoutMethodDeliverypointgroupCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CheckoutMethodEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CheckoutMethodEntityBase() :base("CheckoutMethodEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		protected CheckoutMethodEntityBase(System.Int32 checkoutMethodId):base("CheckoutMethodEntity")
		{
			InitClassFetch(checkoutMethodId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CheckoutMethodEntityBase(System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse): base("CheckoutMethodEntity")
		{
			InitClassFetch(checkoutMethodId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="validator">The custom validator object for this CheckoutMethodEntity</param>
		protected CheckoutMethodEntityBase(System.Int32 checkoutMethodId, IValidator validator):base("CheckoutMethodEntity")
		{
			InitClassFetch(checkoutMethodId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CheckoutMethodEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_checkoutMethodDeliverypointgroupCollection = (Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection)info.GetValue("_checkoutMethodDeliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection));
			_alwaysFetchCheckoutMethodDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchCheckoutMethodDeliverypointgroupCollection");
			_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedCheckoutMethodDeliverypointgroupCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_outletEntity = (OutletEntity)info.GetValue("_outletEntity", typeof(OutletEntity));
			if(_outletEntity!=null)
			{
				_outletEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outletEntityReturnsNewIfNotFound = info.GetBoolean("_outletEntityReturnsNewIfNotFound");
			_alwaysFetchOutletEntity = info.GetBoolean("_alwaysFetchOutletEntity");
			_alreadyFetchedOutletEntity = info.GetBoolean("_alreadyFetchedOutletEntity");

			_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)info.GetValue("_paymentIntegrationConfigurationEntity", typeof(PaymentIntegrationConfigurationEntity));
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				_paymentIntegrationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentIntegrationConfigurationEntity = info.GetBoolean("_alwaysFetchPaymentIntegrationConfigurationEntity");
			_alreadyFetchedPaymentIntegrationConfigurationEntity = info.GetBoolean("_alreadyFetchedPaymentIntegrationConfigurationEntity");

			_receiptTemplateEntity = (ReceiptTemplateEntity)info.GetValue("_receiptTemplateEntity", typeof(ReceiptTemplateEntity));
			if(_receiptTemplateEntity!=null)
			{
				_receiptTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiptTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_receiptTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchReceiptTemplateEntity = info.GetBoolean("_alwaysFetchReceiptTemplateEntity");
			_alreadyFetchedReceiptTemplateEntity = info.GetBoolean("_alreadyFetchedReceiptTemplateEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CheckoutMethodFieldIndex)fieldIndex)
			{
				case CheckoutMethodFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case CheckoutMethodFieldIndex.OutletId:
					DesetupSyncOutletEntity(true, false);
					_alreadyFetchedOutletEntity = false;
					break;
				case CheckoutMethodFieldIndex.ReceiptTemplateId:
					DesetupSyncReceiptTemplateEntity(true, false);
					_alreadyFetchedReceiptTemplateEntity = false;
					break;
				case CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId:
					DesetupSyncPaymentIntegrationConfigurationEntity(true, false);
					_alreadyFetchedPaymentIntegrationConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = (_checkoutMethodDeliverypointgroupCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedOutletEntity = (_outletEntity != null);
			_alreadyFetchedPaymentIntegrationConfigurationEntity = (_paymentIntegrationConfigurationEntity != null);
			_alreadyFetchedReceiptTemplateEntity = (_receiptTemplateEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "OutletEntity":
					toReturn.Add(Relations.OutletEntityUsingOutletId);
					break;
				case "PaymentIntegrationConfigurationEntity":
					toReturn.Add(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
					break;
				case "ReceiptTemplateEntity":
					toReturn.Add(Relations.ReceiptTemplateEntityUsingReceiptTemplateId);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					toReturn.Add(Relations.CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingCheckoutMethodId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingCheckoutMethodId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_checkoutMethodDeliverypointgroupCollection", (!this.MarkedForDeletion?_checkoutMethodDeliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchCheckoutMethodDeliverypointgroupCollection", _alwaysFetchCheckoutMethodDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedCheckoutMethodDeliverypointgroupCollection", _alreadyFetchedCheckoutMethodDeliverypointgroupCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_outletEntity", (!this.MarkedForDeletion?_outletEntity:null));
			info.AddValue("_outletEntityReturnsNewIfNotFound", _outletEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutletEntity", _alwaysFetchOutletEntity);
			info.AddValue("_alreadyFetchedOutletEntity", _alreadyFetchedOutletEntity);
			info.AddValue("_paymentIntegrationConfigurationEntity", (!this.MarkedForDeletion?_paymentIntegrationConfigurationEntity:null));
			info.AddValue("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound", _paymentIntegrationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentIntegrationConfigurationEntity", _alwaysFetchPaymentIntegrationConfigurationEntity);
			info.AddValue("_alreadyFetchedPaymentIntegrationConfigurationEntity", _alreadyFetchedPaymentIntegrationConfigurationEntity);
			info.AddValue("_receiptTemplateEntity", (!this.MarkedForDeletion?_receiptTemplateEntity:null));
			info.AddValue("_receiptTemplateEntityReturnsNewIfNotFound", _receiptTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiptTemplateEntity", _alwaysFetchReceiptTemplateEntity);
			info.AddValue("_alreadyFetchedReceiptTemplateEntity", _alreadyFetchedReceiptTemplateEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "OutletEntity":
					_alreadyFetchedOutletEntity = true;
					this.OutletEntity = (OutletEntity)entity;
					break;
				case "PaymentIntegrationConfigurationEntity":
					_alreadyFetchedPaymentIntegrationConfigurationEntity = true;
					this.PaymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)entity;
					break;
				case "ReceiptTemplateEntity":
					_alreadyFetchedReceiptTemplateEntity = true;
					this.ReceiptTemplateEntity = (ReceiptTemplateEntity)entity;
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.CheckoutMethodDeliverypointgroupCollection.Add((CheckoutMethodDeliverypointgroupEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "OutletEntity":
					SetupSyncOutletEntity(relatedEntity);
					break;
				case "PaymentIntegrationConfigurationEntity":
					SetupSyncPaymentIntegrationConfigurationEntity(relatedEntity);
					break;
				case "ReceiptTemplateEntity":
					SetupSyncReceiptTemplateEntity(relatedEntity);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					_checkoutMethodDeliverypointgroupCollection.Add((CheckoutMethodDeliverypointgroupEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "OutletEntity":
					DesetupSyncOutletEntity(false, true);
					break;
				case "PaymentIntegrationConfigurationEntity":
					DesetupSyncPaymentIntegrationConfigurationEntity(false, true);
					break;
				case "ReceiptTemplateEntity":
					DesetupSyncReceiptTemplateEntity(false, true);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_checkoutMethodDeliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_outletEntity!=null)
			{
				toReturn.Add(_outletEntity);
			}
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				toReturn.Add(_paymentIntegrationConfigurationEntity);
			}
			if(_receiptTemplateEntity!=null)
			{
				toReturn.Add(_receiptTemplateEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_checkoutMethodDeliverypointgroupCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_orderCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 checkoutMethodId)
		{
			return FetchUsingPK(checkoutMethodId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(checkoutMethodId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(checkoutMethodId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(checkoutMethodId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CheckoutMethodId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CheckoutMethodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, _checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, _checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCheckoutMethodDeliverypointgroupCollection || forceFetch || _alwaysFetchCheckoutMethodDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_checkoutMethodDeliverypointgroupCollection);
				_checkoutMethodDeliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_checkoutMethodDeliverypointgroupCollection.GetMultiManyToOne(this, null, null, filter);
				_checkoutMethodDeliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = true;
			}
			return _checkoutMethodDeliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CheckoutMethodDeliverypointgroupCollection'. These settings will be taken into account
		/// when the property CheckoutMethodDeliverypointgroupCollection is requested or GetMultiCheckoutMethodDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCheckoutMethodDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_checkoutMethodDeliverypointgroupCollection.SortClauses=sortClauses;
			_checkoutMethodDeliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public OutletEntity GetSingleOutletEntity()
		{
			return GetSingleOutletEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OutletEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutletEntity' which is related to this entity.</returns>
		public virtual OutletEntity GetSingleOutletEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutletEntity || forceFetch || _alwaysFetchOutletEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutletEntityUsingOutletId);
				OutletEntity newEntity = new OutletEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutletId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutletEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outletEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutletEntity = newEntity;
				_alreadyFetchedOutletEntity = fetchResult;
			}
			return _outletEntity;
		}


		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity()
		{
			return GetSinglePaymentIntegrationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public virtual PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentIntegrationConfigurationEntity || forceFetch || _alwaysFetchPaymentIntegrationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
				PaymentIntegrationConfigurationEntity newEntity = new PaymentIntegrationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentIntegrationConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentIntegrationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentIntegrationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentIntegrationConfigurationEntity = newEntity;
				_alreadyFetchedPaymentIntegrationConfigurationEntity = fetchResult;
			}
			return _paymentIntegrationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReceiptTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReceiptTemplateEntity' which is related to this entity.</returns>
		public ReceiptTemplateEntity GetSingleReceiptTemplateEntity()
		{
			return GetSingleReceiptTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReceiptTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReceiptTemplateEntity' which is related to this entity.</returns>
		public virtual ReceiptTemplateEntity GetSingleReceiptTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiptTemplateEntity || forceFetch || _alwaysFetchReceiptTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReceiptTemplateEntityUsingReceiptTemplateId);
				ReceiptTemplateEntity newEntity = new ReceiptTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiptTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReceiptTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiptTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiptTemplateEntity = newEntity;
				_alreadyFetchedReceiptTemplateEntity = fetchResult;
			}
			return _receiptTemplateEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("OutletEntity", _outletEntity);
			toReturn.Add("PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity);
			toReturn.Add("ReceiptTemplateEntity", _receiptTemplateEntity);
			toReturn.Add("CheckoutMethodDeliverypointgroupCollection", _checkoutMethodDeliverypointgroupCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="validator">The validator object for this CheckoutMethodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 checkoutMethodId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(checkoutMethodId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_checkoutMethodDeliverypointgroupCollection = new Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection();
			_checkoutMethodDeliverypointgroupCollection.SetContainingEntityInfo(this, "CheckoutMethodEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "CheckoutMethodEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "CheckoutMethodEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_outletEntityReturnsNewIfNotFound = true;
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = true;
			_receiptTemplateEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Label", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfirmationActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfirmationDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntegrationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderConfirmationNotificationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderPaymentPendingNotificationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderPaymentFailedNotificationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderReceiptNotificationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermsAndConditionsRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermsAndConditionsLabel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCode", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "CheckoutMethodCollection", resetFKFields, new int[] { (int)CheckoutMethodFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outletEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutletEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.OutletEntityUsingOutletIdStatic, true, signalRelatedEntity, "CheckoutMethodCollection", resetFKFields, new int[] { (int)CheckoutMethodFieldIndex.OutletId } );		
			_outletEntity = null;
		}
		
		/// <summary> setups the sync logic for member _outletEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutletEntity(IEntityCore relatedEntity)
		{
			if(_outletEntity!=relatedEntity)
			{		
				DesetupSyncOutletEntity(true, true);
				_outletEntity = (OutletEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outletEntity, new PropertyChangedEventHandler( OnOutletEntityPropertyChanged ), "OutletEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.OutletEntityUsingOutletIdStatic, true, ref _alreadyFetchedOutletEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutletEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentIntegrationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, signalRelatedEntity, "CheckoutMethodCollection", resetFKFields, new int[] { (int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId } );		
			_paymentIntegrationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentIntegrationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_paymentIntegrationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncPaymentIntegrationConfigurationEntity(true, true);
				_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, ref _alreadyFetchedPaymentIntegrationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentIntegrationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiptTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiptTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiptTemplateEntity, new PropertyChangedEventHandler( OnReceiptTemplateEntityPropertyChanged ), "ReceiptTemplateEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, true, signalRelatedEntity, "CheckoutMethodCollection", resetFKFields, new int[] { (int)CheckoutMethodFieldIndex.ReceiptTemplateId } );		
			_receiptTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiptTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiptTemplateEntity(IEntityCore relatedEntity)
		{
			if(_receiptTemplateEntity!=relatedEntity)
			{		
				DesetupSyncReceiptTemplateEntity(true, true);
				_receiptTemplateEntity = (ReceiptTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiptTemplateEntity, new PropertyChangedEventHandler( OnReceiptTemplateEntityPropertyChanged ), "ReceiptTemplateEntity", Obymobi.Data.RelationClasses.StaticCheckoutMethodRelations.ReceiptTemplateEntityUsingReceiptTemplateIdStatic, true, ref _alreadyFetchedReceiptTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiptTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="checkoutMethodId">PK value for CheckoutMethod which data should be fetched into this CheckoutMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CheckoutMethodFieldIndex.CheckoutMethodId].ForcedCurrentValueWrite(checkoutMethodId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCheckoutMethodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CheckoutMethodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CheckoutMethodRelations Relations
		{
			get	{ return new CheckoutMethodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodDeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity, 0, null, null, null, "CheckoutMethodDeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletEntity")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentIntegrationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection(), (IEntityRelation)GetRelationsForField("PaymentIntegrationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, 0, null, null, null, "PaymentIntegrationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReceiptTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReceiptTemplateCollection(), (IEntityRelation)GetRelationsForField("ReceiptTemplateEntity")[0], (int)Obymobi.Data.EntityType.CheckoutMethodEntity, (int)Obymobi.Data.EntityType.ReceiptTemplateEntity, 0, null, null, null, "ReceiptTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CheckoutMethodId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CheckoutMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CheckoutMethodId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CheckoutMethodId, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CheckoutMethodId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CompanyId, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The OutletId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OutletId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OutletId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.OutletId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OutletId, value, true); }
		}

		/// <summary> The Active property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)CheckoutMethodFieldIndex.Active, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Active, value, true); }
		}

		/// <summary> The Name property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Name, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Name, value, true); }
		}

		/// <summary> The Label property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Label"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Label, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Label, value, true); }
		}

		/// <summary> The Description property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.Description, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.Description, value, true); }
		}

		/// <summary> The ConfirmationActive property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ConfirmationActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ConfirmationActive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CheckoutMethodFieldIndex.ConfirmationActive, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ConfirmationActive, value, true); }
		}

		/// <summary> The ConfirmationDescription property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ConfirmationDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ConfirmationDescription
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.ConfirmationDescription, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ConfirmationDescription, value, true); }
		}

		/// <summary> The CheckoutType property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CheckoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.CheckoutType CheckoutType
		{
			get { return (Obymobi.Enums.CheckoutType)GetValue((int)CheckoutMethodFieldIndex.CheckoutType, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CheckoutType, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CheckoutMethodFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CheckoutMethodFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ReceiptTemplateId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."ReceiptTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiptTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.ReceiptTemplateId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.ReceiptTemplateId, value, true); }
		}

		/// <summary> The PaymentIntegrationConfigurationId property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."PaymentIntegrationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentIntegrationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId, false); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.PaymentIntegrationConfigurationId, value, true); }
		}

		/// <summary> The OrderConfirmationNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderConfirmationNotificationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderNotificationMethod OrderConfirmationNotificationMethod
		{
			get { return (Obymobi.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderConfirmationNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderConfirmationNotificationMethod, value, true); }
		}

		/// <summary> The OrderPaymentPendingNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderPaymentPendingNotificationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderNotificationMethod OrderPaymentPendingNotificationMethod
		{
			get { return (Obymobi.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderPaymentPendingNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderPaymentPendingNotificationMethod, value, true); }
		}

		/// <summary> The OrderPaymentFailedNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderPaymentFailedNotificationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderNotificationMethod OrderPaymentFailedNotificationMethod
		{
			get { return (Obymobi.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderPaymentFailedNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderPaymentFailedNotificationMethod, value, true); }
		}

		/// <summary> The OrderReceiptNotificationMethod property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."OrderReceiptNotificationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderNotificationMethod OrderReceiptNotificationMethod
		{
			get { return (Obymobi.Enums.OrderNotificationMethod)GetValue((int)CheckoutMethodFieldIndex.OrderReceiptNotificationMethod, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.OrderReceiptNotificationMethod, value, true); }
		}

		/// <summary> The TermsAndConditionsRequired property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."TermsAndConditionsRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TermsAndConditionsRequired
		{
			get { return (System.Boolean)GetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsRequired, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsRequired, value, true); }
		}

		/// <summary> The TermsAndConditionsLabel property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."TermsAndConditionsLabel"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TermsAndConditionsLabel
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsLabel, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.TermsAndConditionsLabel, value, true); }
		}

		/// <summary> The CurrencyCode property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CurrencyCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CurrencyCode, value, true); }
		}

		/// <summary> The CountryCode property of the Entity CheckoutMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CheckoutMethod"."CountryCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)CheckoutMethodFieldIndex.CountryCode, true); }
			set	{ SetValue((int)CheckoutMethodFieldIndex.CountryCode, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCheckoutMethodDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection CheckoutMethodDeliverypointgroupCollection
		{
			get	{ return GetMultiCheckoutMethodDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodDeliverypointgroupCollection. When set to true, CheckoutMethodDeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodDeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCheckoutMethodDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchCheckoutMethodDeliverypointgroupCollection; }
			set	{ _alwaysFetchCheckoutMethodDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodDeliverypointgroupCollection already has been fetched. Setting this property to false when CheckoutMethodDeliverypointgroupCollection has been fetched
		/// will clear the CheckoutMethodDeliverypointgroupCollection collection well. Setting this property to true while CheckoutMethodDeliverypointgroupCollection hasn't been fetched disables lazy loading for CheckoutMethodDeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodDeliverypointgroupCollection
		{
			get { return _alreadyFetchedCheckoutMethodDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodDeliverypointgroupCollection && !value && (_checkoutMethodDeliverypointgroupCollection != null))
				{
					_checkoutMethodDeliverypointgroupCollection.Clear();
				}
				_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CheckoutMethodCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutletEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutletEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OutletEntity OutletEntity
		{
			get	{ return GetSingleOutletEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutletEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CheckoutMethodCollection", "OutletEntity", _outletEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutletEntity. When set to true, OutletEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletEntity is accessed. You can always execute a forced fetch by calling GetSingleOutletEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletEntity
		{
			get	{ return _alwaysFetchOutletEntity; }
			set	{ _alwaysFetchOutletEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletEntity already has been fetched. Setting this property to false when OutletEntity has been fetched
		/// will set OutletEntity to null as well. Setting this property to true while OutletEntity hasn't been fetched disables lazy loading for OutletEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletEntity
		{
			get { return _alreadyFetchedOutletEntity;}
			set 
			{
				if(_alreadyFetchedOutletEntity && !value)
				{
					this.OutletEntity = null;
				}
				_alreadyFetchedOutletEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutletEntity is not found
		/// in the database. When set to true, OutletEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OutletEntityReturnsNewIfNotFound
		{
			get	{ return _outletEntityReturnsNewIfNotFound; }
			set { _outletEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentIntegrationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentIntegrationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity
		{
			get	{ return GetSinglePaymentIntegrationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentIntegrationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CheckoutMethodCollection", "PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentIntegrationConfigurationEntity. When set to true, PaymentIntegrationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentIntegrationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentIntegrationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentIntegrationConfigurationEntity
		{
			get	{ return _alwaysFetchPaymentIntegrationConfigurationEntity; }
			set	{ _alwaysFetchPaymentIntegrationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentIntegrationConfigurationEntity already has been fetched. Setting this property to false when PaymentIntegrationConfigurationEntity has been fetched
		/// will set PaymentIntegrationConfigurationEntity to null as well. Setting this property to true while PaymentIntegrationConfigurationEntity hasn't been fetched disables lazy loading for PaymentIntegrationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentIntegrationConfigurationEntity
		{
			get { return _alreadyFetchedPaymentIntegrationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedPaymentIntegrationConfigurationEntity && !value)
				{
					this.PaymentIntegrationConfigurationEntity = null;
				}
				_alreadyFetchedPaymentIntegrationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentIntegrationConfigurationEntity is not found
		/// in the database. When set to true, PaymentIntegrationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentIntegrationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _paymentIntegrationConfigurationEntityReturnsNewIfNotFound; }
			set { _paymentIntegrationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReceiptTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiptTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReceiptTemplateEntity ReceiptTemplateEntity
		{
			get	{ return GetSingleReceiptTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiptTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CheckoutMethodCollection", "ReceiptTemplateEntity", _receiptTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptTemplateEntity. When set to true, ReceiptTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiptTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptTemplateEntity
		{
			get	{ return _alwaysFetchReceiptTemplateEntity; }
			set	{ _alwaysFetchReceiptTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptTemplateEntity already has been fetched. Setting this property to false when ReceiptTemplateEntity has been fetched
		/// will set ReceiptTemplateEntity to null as well. Setting this property to true while ReceiptTemplateEntity hasn't been fetched disables lazy loading for ReceiptTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptTemplateEntity
		{
			get { return _alreadyFetchedReceiptTemplateEntity;}
			set 
			{
				if(_alreadyFetchedReceiptTemplateEntity && !value)
				{
					this.ReceiptTemplateEntity = null;
				}
				_alreadyFetchedReceiptTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiptTemplateEntity is not found
		/// in the database. When set to true, ReceiptTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiptTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _receiptTemplateEntityReturnsNewIfNotFound; }
			set { _receiptTemplateEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CheckoutMethodEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
