﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Role'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoleEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoleEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.RoleModuleRightsCollection	_roleModuleRightsCollection;
		private bool	_alwaysFetchRoleModuleRightsCollection, _alreadyFetchedRoleModuleRightsCollection;
		private Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection	_roleUIElementRightsCollection;
		private bool	_alwaysFetchRoleUIElementRightsCollection, _alreadyFetchedRoleUIElementRightsCollection;
		private Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection	_roleUIElementSubPanelRightsCollection;
		private bool	_alwaysFetchRoleUIElementSubPanelRightsCollection, _alreadyFetchedRoleUIElementSubPanelRightsCollection;
		private Obymobi.Data.CollectionClasses.UserRoleCollection	_userRoleCollection;
		private bool	_alwaysFetchUserRoleCollection, _alreadyFetchedUserRoleCollection;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaUserRole;
		private bool	_alwaysFetchUserCollectionViaUserRole, _alreadyFetchedUserCollectionViaUserRole;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RoleModuleRightsCollection</summary>
			public static readonly string RoleModuleRightsCollection = "RoleModuleRightsCollection";
			/// <summary>Member name RoleUIElementRightsCollection</summary>
			public static readonly string RoleUIElementRightsCollection = "RoleUIElementRightsCollection";
			/// <summary>Member name RoleUIElementSubPanelRightsCollection</summary>
			public static readonly string RoleUIElementSubPanelRightsCollection = "RoleUIElementSubPanelRightsCollection";
			/// <summary>Member name UserRoleCollection</summary>
			public static readonly string UserRoleCollection = "UserRoleCollection";
			/// <summary>Member name UserCollectionViaUserRole</summary>
			public static readonly string UserCollectionViaUserRole = "UserCollectionViaUserRole";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoleEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoleEntityBase() :base("RoleEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		protected RoleEntityBase(System.Int32 roleId):base("RoleEntity")
		{
			InitClassFetch(roleId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoleEntityBase(System.Int32 roleId, IPrefetchPath prefetchPathToUse): base("RoleEntity")
		{
			InitClassFetch(roleId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="validator">The custom validator object for this RoleEntity</param>
		protected RoleEntityBase(System.Int32 roleId, IValidator validator):base("RoleEntity")
		{
			InitClassFetch(roleId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoleEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_roleModuleRightsCollection = (Obymobi.Data.CollectionClasses.RoleModuleRightsCollection)info.GetValue("_roleModuleRightsCollection", typeof(Obymobi.Data.CollectionClasses.RoleModuleRightsCollection));
			_alwaysFetchRoleModuleRightsCollection = info.GetBoolean("_alwaysFetchRoleModuleRightsCollection");
			_alreadyFetchedRoleModuleRightsCollection = info.GetBoolean("_alreadyFetchedRoleModuleRightsCollection");

			_roleUIElementRightsCollection = (Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection)info.GetValue("_roleUIElementRightsCollection", typeof(Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection));
			_alwaysFetchRoleUIElementRightsCollection = info.GetBoolean("_alwaysFetchRoleUIElementRightsCollection");
			_alreadyFetchedRoleUIElementRightsCollection = info.GetBoolean("_alreadyFetchedRoleUIElementRightsCollection");

			_roleUIElementSubPanelRightsCollection = (Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection)info.GetValue("_roleUIElementSubPanelRightsCollection", typeof(Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection));
			_alwaysFetchRoleUIElementSubPanelRightsCollection = info.GetBoolean("_alwaysFetchRoleUIElementSubPanelRightsCollection");
			_alreadyFetchedRoleUIElementSubPanelRightsCollection = info.GetBoolean("_alreadyFetchedRoleUIElementSubPanelRightsCollection");

			_userRoleCollection = (Obymobi.Data.CollectionClasses.UserRoleCollection)info.GetValue("_userRoleCollection", typeof(Obymobi.Data.CollectionClasses.UserRoleCollection));
			_alwaysFetchUserRoleCollection = info.GetBoolean("_alwaysFetchUserRoleCollection");
			_alreadyFetchedUserRoleCollection = info.GetBoolean("_alreadyFetchedUserRoleCollection");
			_userCollectionViaUserRole = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaUserRole", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaUserRole = info.GetBoolean("_alwaysFetchUserCollectionViaUserRole");
			_alreadyFetchedUserCollectionViaUserRole = info.GetBoolean("_alreadyFetchedUserCollectionViaUserRole");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRoleModuleRightsCollection = (_roleModuleRightsCollection.Count > 0);
			_alreadyFetchedRoleUIElementRightsCollection = (_roleUIElementRightsCollection.Count > 0);
			_alreadyFetchedRoleUIElementSubPanelRightsCollection = (_roleUIElementSubPanelRightsCollection.Count > 0);
			_alreadyFetchedUserRoleCollection = (_userRoleCollection.Count > 0);
			_alreadyFetchedUserCollectionViaUserRole = (_userCollectionViaUserRole.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RoleModuleRightsCollection":
					toReturn.Add(Relations.RoleModuleRightsEntityUsingRoleId);
					break;
				case "RoleUIElementRightsCollection":
					toReturn.Add(Relations.RoleUIElementRightsEntityUsingRoleId);
					break;
				case "RoleUIElementSubPanelRightsCollection":
					toReturn.Add(Relations.RoleUIElementSubPanelRightsEntityUsingRoleId);
					break;
				case "UserRoleCollection":
					toReturn.Add(Relations.UserRoleEntityUsingRoleId);
					break;
				case "UserCollectionViaUserRole":
					toReturn.Add(Relations.UserRoleEntityUsingRoleId, "RoleEntity__", "UserRole_", JoinHint.None);
					toReturn.Add(UserRoleEntity.Relations.UserEntityUsingUserId, "UserRole_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_roleModuleRightsCollection", (!this.MarkedForDeletion?_roleModuleRightsCollection:null));
			info.AddValue("_alwaysFetchRoleModuleRightsCollection", _alwaysFetchRoleModuleRightsCollection);
			info.AddValue("_alreadyFetchedRoleModuleRightsCollection", _alreadyFetchedRoleModuleRightsCollection);
			info.AddValue("_roleUIElementRightsCollection", (!this.MarkedForDeletion?_roleUIElementRightsCollection:null));
			info.AddValue("_alwaysFetchRoleUIElementRightsCollection", _alwaysFetchRoleUIElementRightsCollection);
			info.AddValue("_alreadyFetchedRoleUIElementRightsCollection", _alreadyFetchedRoleUIElementRightsCollection);
			info.AddValue("_roleUIElementSubPanelRightsCollection", (!this.MarkedForDeletion?_roleUIElementSubPanelRightsCollection:null));
			info.AddValue("_alwaysFetchRoleUIElementSubPanelRightsCollection", _alwaysFetchRoleUIElementSubPanelRightsCollection);
			info.AddValue("_alreadyFetchedRoleUIElementSubPanelRightsCollection", _alreadyFetchedRoleUIElementSubPanelRightsCollection);
			info.AddValue("_userRoleCollection", (!this.MarkedForDeletion?_userRoleCollection:null));
			info.AddValue("_alwaysFetchUserRoleCollection", _alwaysFetchUserRoleCollection);
			info.AddValue("_alreadyFetchedUserRoleCollection", _alreadyFetchedUserRoleCollection);
			info.AddValue("_userCollectionViaUserRole", (!this.MarkedForDeletion?_userCollectionViaUserRole:null));
			info.AddValue("_alwaysFetchUserCollectionViaUserRole", _alwaysFetchUserCollectionViaUserRole);
			info.AddValue("_alreadyFetchedUserCollectionViaUserRole", _alreadyFetchedUserCollectionViaUserRole);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RoleModuleRightsCollection":
					_alreadyFetchedRoleModuleRightsCollection = true;
					if(entity!=null)
					{
						this.RoleModuleRightsCollection.Add((RoleModuleRightsEntity)entity);
					}
					break;
				case "RoleUIElementRightsCollection":
					_alreadyFetchedRoleUIElementRightsCollection = true;
					if(entity!=null)
					{
						this.RoleUIElementRightsCollection.Add((RoleUIElementRightsEntity)entity);
					}
					break;
				case "RoleUIElementSubPanelRightsCollection":
					_alreadyFetchedRoleUIElementSubPanelRightsCollection = true;
					if(entity!=null)
					{
						this.RoleUIElementSubPanelRightsCollection.Add((RoleUIElementSubPanelRightsEntity)entity);
					}
					break;
				case "UserRoleCollection":
					_alreadyFetchedUserRoleCollection = true;
					if(entity!=null)
					{
						this.UserRoleCollection.Add((UserRoleEntity)entity);
					}
					break;
				case "UserCollectionViaUserRole":
					_alreadyFetchedUserCollectionViaUserRole = true;
					if(entity!=null)
					{
						this.UserCollectionViaUserRole.Add((UserEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RoleModuleRightsCollection":
					_roleModuleRightsCollection.Add((RoleModuleRightsEntity)relatedEntity);
					break;
				case "RoleUIElementRightsCollection":
					_roleUIElementRightsCollection.Add((RoleUIElementRightsEntity)relatedEntity);
					break;
				case "RoleUIElementSubPanelRightsCollection":
					_roleUIElementSubPanelRightsCollection.Add((RoleUIElementSubPanelRightsEntity)relatedEntity);
					break;
				case "UserRoleCollection":
					_userRoleCollection.Add((UserRoleEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RoleModuleRightsCollection":
					this.PerformRelatedEntityRemoval(_roleModuleRightsCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoleUIElementRightsCollection":
					this.PerformRelatedEntityRemoval(_roleUIElementRightsCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoleUIElementSubPanelRightsCollection":
					this.PerformRelatedEntityRemoval(_roleUIElementSubPanelRightsCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserRoleCollection":
					this.PerformRelatedEntityRemoval(_userRoleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_roleModuleRightsCollection);
			toReturn.Add(_roleUIElementRightsCollection);
			toReturn.Add(_roleUIElementSubPanelRightsCollection);
			toReturn.Add(_userRoleCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roleId)
		{
			return FetchUsingPK(roleId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roleId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roleId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roleId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roleId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roleId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoleId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoleModuleRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRightsCollection(bool forceFetch)
		{
			return GetMultiRoleModuleRightsCollection(forceFetch, _roleModuleRightsCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoleModuleRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRightsCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoleModuleRightsCollection(forceFetch, _roleModuleRightsCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoleModuleRightsCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoleModuleRightsCollection || forceFetch || _alwaysFetchRoleModuleRightsCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roleModuleRightsCollection);
				_roleModuleRightsCollection.SuppressClearInGetMulti=!forceFetch;
				_roleModuleRightsCollection.EntityFactoryToUse = entityFactoryToUse;
				_roleModuleRightsCollection.GetMultiManyToOne(null, this, filter);
				_roleModuleRightsCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoleModuleRightsCollection = true;
			}
			return _roleModuleRightsCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoleModuleRightsCollection'. These settings will be taken into account
		/// when the property RoleModuleRightsCollection is requested or GetMultiRoleModuleRightsCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoleModuleRightsCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roleModuleRightsCollection.SortClauses=sortClauses;
			_roleModuleRightsCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoleUIElementRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection GetMultiRoleUIElementRightsCollection(bool forceFetch)
		{
			return GetMultiRoleUIElementRightsCollection(forceFetch, _roleUIElementRightsCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoleUIElementRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection GetMultiRoleUIElementRightsCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoleUIElementRightsCollection(forceFetch, _roleUIElementRightsCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection GetMultiRoleUIElementRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoleUIElementRightsCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection GetMultiRoleUIElementRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoleUIElementRightsCollection || forceFetch || _alwaysFetchRoleUIElementRightsCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roleUIElementRightsCollection);
				_roleUIElementRightsCollection.SuppressClearInGetMulti=!forceFetch;
				_roleUIElementRightsCollection.EntityFactoryToUse = entityFactoryToUse;
				_roleUIElementRightsCollection.GetMultiManyToOne(this, filter);
				_roleUIElementRightsCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoleUIElementRightsCollection = true;
			}
			return _roleUIElementRightsCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoleUIElementRightsCollection'. These settings will be taken into account
		/// when the property RoleUIElementRightsCollection is requested or GetMultiRoleUIElementRightsCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoleUIElementRightsCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roleUIElementRightsCollection.SortClauses=sortClauses;
			_roleUIElementRightsCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementSubPanelRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoleUIElementSubPanelRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection GetMultiRoleUIElementSubPanelRightsCollection(bool forceFetch)
		{
			return GetMultiRoleUIElementSubPanelRightsCollection(forceFetch, _roleUIElementSubPanelRightsCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementSubPanelRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoleUIElementSubPanelRightsEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection GetMultiRoleUIElementSubPanelRightsCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoleUIElementSubPanelRightsCollection(forceFetch, _roleUIElementSubPanelRightsCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementSubPanelRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection GetMultiRoleUIElementSubPanelRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoleUIElementSubPanelRightsCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleUIElementSubPanelRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection GetMultiRoleUIElementSubPanelRightsCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoleUIElementSubPanelRightsCollection || forceFetch || _alwaysFetchRoleUIElementSubPanelRightsCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roleUIElementSubPanelRightsCollection);
				_roleUIElementSubPanelRightsCollection.SuppressClearInGetMulti=!forceFetch;
				_roleUIElementSubPanelRightsCollection.EntityFactoryToUse = entityFactoryToUse;
				_roleUIElementSubPanelRightsCollection.GetMultiManyToOne(this, filter);
				_roleUIElementSubPanelRightsCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoleUIElementSubPanelRightsCollection = true;
			}
			return _roleUIElementSubPanelRightsCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoleUIElementSubPanelRightsCollection'. These settings will be taken into account
		/// when the property RoleUIElementSubPanelRightsCollection is requested or GetMultiRoleUIElementSubPanelRightsCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoleUIElementSubPanelRightsCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roleUIElementSubPanelRightsCollection.SortClauses=sortClauses;
			_roleUIElementSubPanelRightsCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserRoleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch)
		{
			return GetMultiUserRoleCollection(forceFetch, _userRoleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserRoleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserRoleCollection(forceFetch, _userRoleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserRoleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserRoleCollection || forceFetch || _alwaysFetchUserRoleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userRoleCollection);
				_userRoleCollection.SuppressClearInGetMulti=!forceFetch;
				_userRoleCollection.EntityFactoryToUse = entityFactoryToUse;
				_userRoleCollection.GetMultiManyToOne(this, null, filter);
				_userRoleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserRoleCollection = true;
			}
			return _userRoleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserRoleCollection'. These settings will be taken into account
		/// when the property UserRoleCollection is requested or GetMultiUserRoleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserRoleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userRoleCollection.SortClauses=sortClauses;
			_userRoleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaUserRole(bool forceFetch)
		{
			return GetMultiUserCollectionViaUserRole(forceFetch, _userCollectionViaUserRole.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaUserRole(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaUserRole || forceFetch || _alwaysFetchUserCollectionViaUserRole) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaUserRole);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RoleFields.RoleId, ComparisonOperator.Equal, this.RoleId, "RoleEntity__"));
				_userCollectionViaUserRole.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaUserRole.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaUserRole.GetMulti(filter, GetRelationsForField("UserCollectionViaUserRole"));
				_userCollectionViaUserRole.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaUserRole = true;
			}
			return _userCollectionViaUserRole;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaUserRole'. These settings will be taken into account
		/// when the property UserCollectionViaUserRole is requested or GetMultiUserCollectionViaUserRole is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaUserRole(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaUserRole.SortClauses=sortClauses;
			_userCollectionViaUserRole.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RoleModuleRightsCollection", _roleModuleRightsCollection);
			toReturn.Add("RoleUIElementRightsCollection", _roleUIElementRightsCollection);
			toReturn.Add("RoleUIElementSubPanelRightsCollection", _roleUIElementSubPanelRightsCollection);
			toReturn.Add("UserRoleCollection", _userRoleCollection);
			toReturn.Add("UserCollectionViaUserRole", _userCollectionViaUserRole);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="validator">The validator object for this RoleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roleId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roleId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_roleModuleRightsCollection = new Obymobi.Data.CollectionClasses.RoleModuleRightsCollection();
			_roleModuleRightsCollection.SetContainingEntityInfo(this, "RoleEntity");

			_roleUIElementRightsCollection = new Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection();
			_roleUIElementRightsCollection.SetContainingEntityInfo(this, "RoleEntity");

			_roleUIElementSubPanelRightsCollection = new Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection();
			_roleUIElementSubPanelRightsCollection.SetContainingEntityInfo(this, "RoleEntity");

			_userRoleCollection = new Obymobi.Data.CollectionClasses.UserRoleCollection();
			_userRoleCollection.SetContainingEntityInfo(this, "RoleEntity");
			_userCollectionViaUserRole = new Obymobi.Data.CollectionClasses.UserCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentRoleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAdministrator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemRole", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roleId">PK value for Role which data should be fetched into this Role object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoleFieldIndex.RoleId].ForcedCurrentValueWrite(roleId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoleRelations Relations
		{
			get	{ return new RoleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoleModuleRights' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoleModuleRightsCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoleModuleRightsCollection(), (IEntityRelation)GetRelationsForField("RoleModuleRightsCollection")[0], (int)Obymobi.Data.EntityType.RoleEntity, (int)Obymobi.Data.EntityType.RoleModuleRightsEntity, 0, null, null, null, "RoleModuleRightsCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoleUIElementRights' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoleUIElementRightsCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection(), (IEntityRelation)GetRelationsForField("RoleUIElementRightsCollection")[0], (int)Obymobi.Data.EntityType.RoleEntity, (int)Obymobi.Data.EntityType.RoleUIElementRightsEntity, 0, null, null, null, "RoleUIElementRightsCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoleUIElementSubPanelRights' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoleUIElementSubPanelRightsCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection(), (IEntityRelation)GetRelationsForField("RoleUIElementSubPanelRightsCollection")[0], (int)Obymobi.Data.EntityType.RoleEntity, (int)Obymobi.Data.EntityType.RoleUIElementSubPanelRightsEntity, 0, null, null, null, "RoleUIElementSubPanelRightsCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserRoleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserRoleCollection(), (IEntityRelation)GetRelationsForField("UserRoleCollection")[0], (int)Obymobi.Data.EntityType.RoleEntity, (int)Obymobi.Data.EntityType.UserRoleEntity, 0, null, null, null, "UserRoleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaUserRole
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UserRoleEntityUsingRoleId;
				intermediateRelation.SetAliases(string.Empty, "UserRole_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RoleEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaUserRole"), "UserCollectionViaUserRole", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoleId property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."RoleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoleId
		{
			get { return (System.Int32)GetValue((int)RoleFieldIndex.RoleId, true); }
			set	{ SetValue((int)RoleFieldIndex.RoleId, value, true); }
		}

		/// <summary> The ParentRoleId property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."ParentRoleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentRoleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoleFieldIndex.ParentRoleId, false); }
			set	{ SetValue((int)RoleFieldIndex.ParentRoleId, value, true); }
		}

		/// <summary> The Name property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RoleFieldIndex.Name, true); }
			set	{ SetValue((int)RoleFieldIndex.Name, value, true); }
		}

		/// <summary> The IsAdministrator property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."IsAdministrator"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAdministrator
		{
			get { return (System.Boolean)GetValue((int)RoleFieldIndex.IsAdministrator, true); }
			set	{ SetValue((int)RoleFieldIndex.IsAdministrator, value, true); }
		}

		/// <summary> The SystemName property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."SystemName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SystemName
		{
			get { return (System.String)GetValue((int)RoleFieldIndex.SystemName, true); }
			set	{ SetValue((int)RoleFieldIndex.SystemName, value, true); }
		}

		/// <summary> The SystemRole property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."SystemRole"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SystemRole
		{
			get { return (System.Boolean)GetValue((int)RoleFieldIndex.SystemRole, true); }
			set	{ SetValue((int)RoleFieldIndex.SystemRole, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoleFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoleFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoleFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoleFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)RoleFieldIndex.Deleted, true); }
			set	{ SetValue((int)RoleFieldIndex.Deleted, value, true); }
		}

		/// <summary> The Archived property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)RoleFieldIndex.Archived, true); }
			set	{ SetValue((int)RoleFieldIndex.Archived, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoleFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Role<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Role"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoleFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoleModuleRightsCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoleModuleRightsCollection RoleModuleRightsCollection
		{
			get	{ return GetMultiRoleModuleRightsCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoleModuleRightsCollection. When set to true, RoleModuleRightsCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoleModuleRightsCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoleModuleRightsCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoleModuleRightsCollection
		{
			get	{ return _alwaysFetchRoleModuleRightsCollection; }
			set	{ _alwaysFetchRoleModuleRightsCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoleModuleRightsCollection already has been fetched. Setting this property to false when RoleModuleRightsCollection has been fetched
		/// will clear the RoleModuleRightsCollection collection well. Setting this property to true while RoleModuleRightsCollection hasn't been fetched disables lazy loading for RoleModuleRightsCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoleModuleRightsCollection
		{
			get { return _alreadyFetchedRoleModuleRightsCollection;}
			set 
			{
				if(_alreadyFetchedRoleModuleRightsCollection && !value && (_roleModuleRightsCollection != null))
				{
					_roleModuleRightsCollection.Clear();
				}
				_alreadyFetchedRoleModuleRightsCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoleUIElementRightsEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoleUIElementRightsCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoleUIElementRightsCollection RoleUIElementRightsCollection
		{
			get	{ return GetMultiRoleUIElementRightsCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoleUIElementRightsCollection. When set to true, RoleUIElementRightsCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoleUIElementRightsCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoleUIElementRightsCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoleUIElementRightsCollection
		{
			get	{ return _alwaysFetchRoleUIElementRightsCollection; }
			set	{ _alwaysFetchRoleUIElementRightsCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoleUIElementRightsCollection already has been fetched. Setting this property to false when RoleUIElementRightsCollection has been fetched
		/// will clear the RoleUIElementRightsCollection collection well. Setting this property to true while RoleUIElementRightsCollection hasn't been fetched disables lazy loading for RoleUIElementRightsCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoleUIElementRightsCollection
		{
			get { return _alreadyFetchedRoleUIElementRightsCollection;}
			set 
			{
				if(_alreadyFetchedRoleUIElementRightsCollection && !value && (_roleUIElementRightsCollection != null))
				{
					_roleUIElementRightsCollection.Clear();
				}
				_alreadyFetchedRoleUIElementRightsCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoleUIElementSubPanelRightsEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoleUIElementSubPanelRightsCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoleUIElementSubPanelRightsCollection RoleUIElementSubPanelRightsCollection
		{
			get	{ return GetMultiRoleUIElementSubPanelRightsCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoleUIElementSubPanelRightsCollection. When set to true, RoleUIElementSubPanelRightsCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoleUIElementSubPanelRightsCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoleUIElementSubPanelRightsCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoleUIElementSubPanelRightsCollection
		{
			get	{ return _alwaysFetchRoleUIElementSubPanelRightsCollection; }
			set	{ _alwaysFetchRoleUIElementSubPanelRightsCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoleUIElementSubPanelRightsCollection already has been fetched. Setting this property to false when RoleUIElementSubPanelRightsCollection has been fetched
		/// will clear the RoleUIElementSubPanelRightsCollection collection well. Setting this property to true while RoleUIElementSubPanelRightsCollection hasn't been fetched disables lazy loading for RoleUIElementSubPanelRightsCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoleUIElementSubPanelRightsCollection
		{
			get { return _alreadyFetchedRoleUIElementSubPanelRightsCollection;}
			set 
			{
				if(_alreadyFetchedRoleUIElementSubPanelRightsCollection && !value && (_roleUIElementSubPanelRightsCollection != null))
				{
					_roleUIElementSubPanelRightsCollection.Clear();
				}
				_alreadyFetchedRoleUIElementSubPanelRightsCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserRoleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserRoleCollection UserRoleCollection
		{
			get	{ return GetMultiUserRoleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserRoleCollection. When set to true, UserRoleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserRoleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserRoleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserRoleCollection
		{
			get	{ return _alwaysFetchUserRoleCollection; }
			set	{ _alwaysFetchUserRoleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserRoleCollection already has been fetched. Setting this property to false when UserRoleCollection has been fetched
		/// will clear the UserRoleCollection collection well. Setting this property to true while UserRoleCollection hasn't been fetched disables lazy loading for UserRoleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserRoleCollection
		{
			get { return _alreadyFetchedUserRoleCollection;}
			set 
			{
				if(_alreadyFetchedUserRoleCollection && !value && (_userRoleCollection != null))
				{
					_userRoleCollection.Clear();
				}
				_alreadyFetchedUserRoleCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaUserRole()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaUserRole
		{
			get { return GetMultiUserCollectionViaUserRole(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaUserRole. When set to true, UserCollectionViaUserRole is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaUserRole is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaUserRole(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaUserRole
		{
			get	{ return _alwaysFetchUserCollectionViaUserRole; }
			set	{ _alwaysFetchUserCollectionViaUserRole = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaUserRole already has been fetched. Setting this property to false when UserCollectionViaUserRole has been fetched
		/// will clear the UserCollectionViaUserRole collection well. Setting this property to true while UserCollectionViaUserRole hasn't been fetched disables lazy loading for UserCollectionViaUserRole</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaUserRole
		{
			get { return _alreadyFetchedUserCollectionViaUserRole;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaUserRole && !value && (_userCollectionViaUserRole != null))
				{
					_userCollectionViaUserRole.Clear();
				}
				_alreadyFetchedUserCollectionViaUserRole = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoleEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
