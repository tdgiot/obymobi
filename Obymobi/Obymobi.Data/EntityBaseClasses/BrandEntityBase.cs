﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Brand'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class BrandEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "BrandEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationCollection	_alterationCollection;
		private bool	_alwaysFetchAlterationCollection, _alreadyFetchedAlterationCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection	_alterationoptionCollection;
		private bool	_alwaysFetchAlterationoptionCollection, _alreadyFetchedAlterationoptionCollection;
		private Obymobi.Data.CollectionClasses.BrandCultureCollection	_brandCultureCollection;
		private bool	_alwaysFetchBrandCultureCollection, _alreadyFetchedBrandCultureCollection;
		private Obymobi.Data.CollectionClasses.CompanyBrandCollection	_companyBrandCollection;
		private bool	_alwaysFetchCompanyBrandCollection, _alreadyFetchedCompanyBrandCollection;
		private Obymobi.Data.CollectionClasses.GenericalterationCollection	_genericalterationCollection;
		private bool	_alwaysFetchGenericalterationCollection, _alreadyFetchedGenericalterationCollection;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection	_genericcategoryCollection;
		private bool	_alwaysFetchGenericcategoryCollection, _alreadyFetchedGenericcategoryCollection;
		private Obymobi.Data.CollectionClasses.GenericproductCollection	_genericproductCollection;
		private bool	_alwaysFetchGenericproductCollection, _alreadyFetchedGenericproductCollection;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.UserBrandCollection	_userBrandCollection;
		private bool	_alwaysFetchUserBrandCollection, _alreadyFetchedUserBrandCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationoptionCollection</summary>
			public static readonly string AlterationoptionCollection = "AlterationoptionCollection";
			/// <summary>Member name BrandCultureCollection</summary>
			public static readonly string BrandCultureCollection = "BrandCultureCollection";
			/// <summary>Member name CompanyBrandCollection</summary>
			public static readonly string CompanyBrandCollection = "CompanyBrandCollection";
			/// <summary>Member name GenericalterationCollection</summary>
			public static readonly string GenericalterationCollection = "GenericalterationCollection";
			/// <summary>Member name GenericcategoryCollection</summary>
			public static readonly string GenericcategoryCollection = "GenericcategoryCollection";
			/// <summary>Member name GenericproductCollection</summary>
			public static readonly string GenericproductCollection = "GenericproductCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name UserBrandCollection</summary>
			public static readonly string UserBrandCollection = "UserBrandCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BrandEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected BrandEntityBase() :base("BrandEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		protected BrandEntityBase(System.Int32 brandId):base("BrandEntity")
		{
			InitClassFetch(brandId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected BrandEntityBase(System.Int32 brandId, IPrefetchPath prefetchPathToUse): base("BrandEntity")
		{
			InitClassFetch(brandId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="validator">The custom validator object for this BrandEntity</param>
		protected BrandEntityBase(System.Int32 brandId, IValidator validator):base("BrandEntity")
		{
			InitClassFetch(brandId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BrandEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationCollection = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollection = info.GetBoolean("_alwaysFetchAlterationCollection");
			_alreadyFetchedAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationCollection");

			_alterationoptionCollection = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollection = info.GetBoolean("_alwaysFetchAlterationoptionCollection");
			_alreadyFetchedAlterationoptionCollection = info.GetBoolean("_alreadyFetchedAlterationoptionCollection");

			_brandCultureCollection = (Obymobi.Data.CollectionClasses.BrandCultureCollection)info.GetValue("_brandCultureCollection", typeof(Obymobi.Data.CollectionClasses.BrandCultureCollection));
			_alwaysFetchBrandCultureCollection = info.GetBoolean("_alwaysFetchBrandCultureCollection");
			_alreadyFetchedBrandCultureCollection = info.GetBoolean("_alreadyFetchedBrandCultureCollection");

			_companyBrandCollection = (Obymobi.Data.CollectionClasses.CompanyBrandCollection)info.GetValue("_companyBrandCollection", typeof(Obymobi.Data.CollectionClasses.CompanyBrandCollection));
			_alwaysFetchCompanyBrandCollection = info.GetBoolean("_alwaysFetchCompanyBrandCollection");
			_alreadyFetchedCompanyBrandCollection = info.GetBoolean("_alreadyFetchedCompanyBrandCollection");

			_genericalterationCollection = (Obymobi.Data.CollectionClasses.GenericalterationCollection)info.GetValue("_genericalterationCollection", typeof(Obymobi.Data.CollectionClasses.GenericalterationCollection));
			_alwaysFetchGenericalterationCollection = info.GetBoolean("_alwaysFetchGenericalterationCollection");
			_alreadyFetchedGenericalterationCollection = info.GetBoolean("_alreadyFetchedGenericalterationCollection");

			_genericcategoryCollection = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollection", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollection = info.GetBoolean("_alwaysFetchGenericcategoryCollection");
			_alreadyFetchedGenericcategoryCollection = info.GetBoolean("_alreadyFetchedGenericcategoryCollection");

			_genericproductCollection = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollection = info.GetBoolean("_alwaysFetchGenericproductCollection");
			_alreadyFetchedGenericproductCollection = info.GetBoolean("_alreadyFetchedGenericproductCollection");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");

			_userBrandCollection = (Obymobi.Data.CollectionClasses.UserBrandCollection)info.GetValue("_userBrandCollection", typeof(Obymobi.Data.CollectionClasses.UserBrandCollection));
			_alwaysFetchUserBrandCollection = info.GetBoolean("_alwaysFetchUserBrandCollection");
			_alreadyFetchedUserBrandCollection = info.GetBoolean("_alreadyFetchedUserBrandCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationCollection = (_alterationCollection.Count > 0);
			_alreadyFetchedAlterationoptionCollection = (_alterationoptionCollection.Count > 0);
			_alreadyFetchedBrandCultureCollection = (_brandCultureCollection.Count > 0);
			_alreadyFetchedCompanyBrandCollection = (_companyBrandCollection.Count > 0);
			_alreadyFetchedGenericalterationCollection = (_genericalterationCollection.Count > 0);
			_alreadyFetchedGenericcategoryCollection = (_genericcategoryCollection.Count > 0);
			_alreadyFetchedGenericproductCollection = (_genericproductCollection.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedUserBrandCollection = (_userBrandCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationCollection":
					toReturn.Add(Relations.AlterationEntityUsingBrandId);
					break;
				case "AlterationoptionCollection":
					toReturn.Add(Relations.AlterationoptionEntityUsingBrandId);
					break;
				case "BrandCultureCollection":
					toReturn.Add(Relations.BrandCultureEntityUsingBrandId);
					break;
				case "CompanyBrandCollection":
					toReturn.Add(Relations.CompanyBrandEntityUsingBrandId);
					break;
				case "GenericalterationCollection":
					toReturn.Add(Relations.GenericalterationEntityUsingBrandId);
					break;
				case "GenericcategoryCollection":
					toReturn.Add(Relations.GenericcategoryEntityUsingBrandId);
					break;
				case "GenericproductCollection":
					toReturn.Add(Relations.GenericproductEntityUsingBrandId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingBrandId);
					break;
				case "UserBrandCollection":
					toReturn.Add(Relations.UserBrandEntityUsingBrandId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationCollection", (!this.MarkedForDeletion?_alterationCollection:null));
			info.AddValue("_alwaysFetchAlterationCollection", _alwaysFetchAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationCollection", _alreadyFetchedAlterationCollection);
			info.AddValue("_alterationoptionCollection", (!this.MarkedForDeletion?_alterationoptionCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionCollection", _alwaysFetchAlterationoptionCollection);
			info.AddValue("_alreadyFetchedAlterationoptionCollection", _alreadyFetchedAlterationoptionCollection);
			info.AddValue("_brandCultureCollection", (!this.MarkedForDeletion?_brandCultureCollection:null));
			info.AddValue("_alwaysFetchBrandCultureCollection", _alwaysFetchBrandCultureCollection);
			info.AddValue("_alreadyFetchedBrandCultureCollection", _alreadyFetchedBrandCultureCollection);
			info.AddValue("_companyBrandCollection", (!this.MarkedForDeletion?_companyBrandCollection:null));
			info.AddValue("_alwaysFetchCompanyBrandCollection", _alwaysFetchCompanyBrandCollection);
			info.AddValue("_alreadyFetchedCompanyBrandCollection", _alreadyFetchedCompanyBrandCollection);
			info.AddValue("_genericalterationCollection", (!this.MarkedForDeletion?_genericalterationCollection:null));
			info.AddValue("_alwaysFetchGenericalterationCollection", _alwaysFetchGenericalterationCollection);
			info.AddValue("_alreadyFetchedGenericalterationCollection", _alreadyFetchedGenericalterationCollection);
			info.AddValue("_genericcategoryCollection", (!this.MarkedForDeletion?_genericcategoryCollection:null));
			info.AddValue("_alwaysFetchGenericcategoryCollection", _alwaysFetchGenericcategoryCollection);
			info.AddValue("_alreadyFetchedGenericcategoryCollection", _alreadyFetchedGenericcategoryCollection);
			info.AddValue("_genericproductCollection", (!this.MarkedForDeletion?_genericproductCollection:null));
			info.AddValue("_alwaysFetchGenericproductCollection", _alwaysFetchGenericproductCollection);
			info.AddValue("_alreadyFetchedGenericproductCollection", _alreadyFetchedGenericproductCollection);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_userBrandCollection", (!this.MarkedForDeletion?_userBrandCollection:null));
			info.AddValue("_alwaysFetchUserBrandCollection", _alwaysFetchUserBrandCollection);
			info.AddValue("_alreadyFetchedUserBrandCollection", _alreadyFetchedUserBrandCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationCollection":
					_alreadyFetchedAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationCollection.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollection":
					_alreadyFetchedAlterationoptionCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionCollection.Add((AlterationoptionEntity)entity);
					}
					break;
				case "BrandCultureCollection":
					_alreadyFetchedBrandCultureCollection = true;
					if(entity!=null)
					{
						this.BrandCultureCollection.Add((BrandCultureEntity)entity);
					}
					break;
				case "CompanyBrandCollection":
					_alreadyFetchedCompanyBrandCollection = true;
					if(entity!=null)
					{
						this.CompanyBrandCollection.Add((CompanyBrandEntity)entity);
					}
					break;
				case "GenericalterationCollection":
					_alreadyFetchedGenericalterationCollection = true;
					if(entity!=null)
					{
						this.GenericalterationCollection.Add((GenericalterationEntity)entity);
					}
					break;
				case "GenericcategoryCollection":
					_alreadyFetchedGenericcategoryCollection = true;
					if(entity!=null)
					{
						this.GenericcategoryCollection.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollection":
					_alreadyFetchedGenericproductCollection = true;
					if(entity!=null)
					{
						this.GenericproductCollection.Add((GenericproductEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "UserBrandCollection":
					_alreadyFetchedUserBrandCollection = true;
					if(entity!=null)
					{
						this.UserBrandCollection.Add((UserBrandEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationCollection":
					_alterationCollection.Add((AlterationEntity)relatedEntity);
					break;
				case "AlterationoptionCollection":
					_alterationoptionCollection.Add((AlterationoptionEntity)relatedEntity);
					break;
				case "BrandCultureCollection":
					_brandCultureCollection.Add((BrandCultureEntity)relatedEntity);
					break;
				case "CompanyBrandCollection":
					_companyBrandCollection.Add((CompanyBrandEntity)relatedEntity);
					break;
				case "GenericalterationCollection":
					_genericalterationCollection.Add((GenericalterationEntity)relatedEntity);
					break;
				case "GenericcategoryCollection":
					_genericcategoryCollection.Add((GenericcategoryEntity)relatedEntity);
					break;
				case "GenericproductCollection":
					_genericproductCollection.Add((GenericproductEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				case "UserBrandCollection":
					_userBrandCollection.Add((UserBrandEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationoptionCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BrandCultureCollection":
					this.PerformRelatedEntityRemoval(_brandCultureCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyBrandCollection":
					this.PerformRelatedEntityRemoval(_companyBrandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericalterationCollection":
					this.PerformRelatedEntityRemoval(_genericalterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericcategoryCollection":
					this.PerformRelatedEntityRemoval(_genericcategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductCollection":
					this.PerformRelatedEntityRemoval(_genericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserBrandCollection":
					this.PerformRelatedEntityRemoval(_userBrandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationCollection);
			toReturn.Add(_alterationoptionCollection);
			toReturn.Add(_brandCultureCollection);
			toReturn.Add(_companyBrandCollection);
			toReturn.Add(_genericalterationCollection);
			toReturn.Add(_genericcategoryCollection);
			toReturn.Add(_genericproductCollection);
			toReturn.Add(_productCollection);
			toReturn.Add(_userBrandCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 brandId)
		{
			return FetchUsingPK(brandId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 brandId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(brandId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 brandId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(brandId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 brandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(brandId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BrandId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BrandRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationCollection || forceFetch || _alwaysFetchAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollection);
				_alterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollection.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_alterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollection = true;
			}
			return _alterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollection'. These settings will be taken into account
		/// when the property AlterationCollection is requested or GetMultiAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollection.SortClauses=sortClauses;
			_alterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionCollection(forceFetch, _alterationoptionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollection || forceFetch || _alwaysFetchAlterationoptionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollection);
				_alterationoptionCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_alterationoptionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollection = true;
			}
			return _alterationoptionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollection'. These settings will be taken into account
		/// when the property AlterationoptionCollection is requested or GetMultiAlterationoptionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollection.SortClauses=sortClauses;
			_alterationoptionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BrandCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BrandCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.BrandCultureCollection GetMultiBrandCultureCollection(bool forceFetch)
		{
			return GetMultiBrandCultureCollection(forceFetch, _brandCultureCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BrandCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BrandCultureEntity'</returns>
		public Obymobi.Data.CollectionClasses.BrandCultureCollection GetMultiBrandCultureCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBrandCultureCollection(forceFetch, _brandCultureCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BrandCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.BrandCultureCollection GetMultiBrandCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBrandCultureCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BrandCultureEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.BrandCultureCollection GetMultiBrandCultureCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBrandCultureCollection || forceFetch || _alwaysFetchBrandCultureCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_brandCultureCollection);
				_brandCultureCollection.SuppressClearInGetMulti=!forceFetch;
				_brandCultureCollection.EntityFactoryToUse = entityFactoryToUse;
				_brandCultureCollection.GetMultiManyToOne(this, filter);
				_brandCultureCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedBrandCultureCollection = true;
			}
			return _brandCultureCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'BrandCultureCollection'. These settings will be taken into account
		/// when the property BrandCultureCollection is requested or GetMultiBrandCultureCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBrandCultureCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_brandCultureCollection.SortClauses=sortClauses;
			_brandCultureCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyBrandCollection GetMultiCompanyBrandCollection(bool forceFetch)
		{
			return GetMultiCompanyBrandCollection(forceFetch, _companyBrandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyBrandCollection GetMultiCompanyBrandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyBrandCollection(forceFetch, _companyBrandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyBrandCollection GetMultiCompanyBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyBrandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyBrandCollection GetMultiCompanyBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyBrandCollection || forceFetch || _alwaysFetchCompanyBrandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyBrandCollection);
				_companyBrandCollection.SuppressClearInGetMulti=!forceFetch;
				_companyBrandCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyBrandCollection.GetMultiManyToOne(this, null, filter);
				_companyBrandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyBrandCollection = true;
			}
			return _companyBrandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyBrandCollection'. These settings will be taken into account
		/// when the property CompanyBrandCollection is requested or GetMultiCompanyBrandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyBrandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyBrandCollection.SortClauses=sortClauses;
			_companyBrandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollection(bool forceFetch)
		{
			return GetMultiGenericalterationCollection(forceFetch, _genericalterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericalterationCollection(forceFetch, _genericalterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericalterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericalterationCollection || forceFetch || _alwaysFetchGenericalterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationCollection);
				_genericalterationCollection.SuppressClearInGetMulti=!forceFetch;
				_genericalterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationCollection.GetMultiManyToOne(this, filter);
				_genericalterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationCollection = true;
			}
			return _genericalterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationCollection'. These settings will be taken into account
		/// when the property GenericalterationCollection is requested or GetMultiGenericalterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationCollection.SortClauses=sortClauses;
			_genericalterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch)
		{
			return GetMultiGenericcategoryCollection(forceFetch, _genericcategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericcategoryCollection(forceFetch, _genericcategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericcategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollection || forceFetch || _alwaysFetchGenericcategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollection);
				_genericcategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollection.GetMultiManyToOne(this, null, filter);
				_genericcategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollection = true;
			}
			return _genericcategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollection'. These settings will be taken into account
		/// when the property GenericcategoryCollection is requested or GetMultiGenericcategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollection.SortClauses=sortClauses;
			_genericcategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductCollection || forceFetch || _alwaysFetchGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollection);
				_genericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollection.GetMultiManyToOne(this, null, null, null, filter);
				_genericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollection = true;
			}
			return _genericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollection'. These settings will be taken into account
		/// when the property GenericproductCollection is requested or GetMultiGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollection.SortClauses=sortClauses;
			_genericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch)
		{
			return GetMultiUserBrandCollection(forceFetch, _userBrandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserBrandCollection(forceFetch, _userBrandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserBrandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserBrandCollection || forceFetch || _alwaysFetchUserBrandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userBrandCollection);
				_userBrandCollection.SuppressClearInGetMulti=!forceFetch;
				_userBrandCollection.EntityFactoryToUse = entityFactoryToUse;
				_userBrandCollection.GetMultiManyToOne(this, null, filter);
				_userBrandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserBrandCollection = true;
			}
			return _userBrandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserBrandCollection'. These settings will be taken into account
		/// when the property UserBrandCollection is requested or GetMultiUserBrandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserBrandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userBrandCollection.SortClauses=sortClauses;
			_userBrandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationCollection", _alterationCollection);
			toReturn.Add("AlterationoptionCollection", _alterationoptionCollection);
			toReturn.Add("BrandCultureCollection", _brandCultureCollection);
			toReturn.Add("CompanyBrandCollection", _companyBrandCollection);
			toReturn.Add("GenericalterationCollection", _genericalterationCollection);
			toReturn.Add("GenericcategoryCollection", _genericcategoryCollection);
			toReturn.Add("GenericproductCollection", _genericproductCollection);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("UserBrandCollection", _userBrandCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="validator">The validator object for this BrandEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 brandId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(brandId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationCollection = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollection.SetContainingEntityInfo(this, "BrandEntity");

			_alterationoptionCollection = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_alterationoptionCollection.SetContainingEntityInfo(this, "BrandEntity");

			_brandCultureCollection = new Obymobi.Data.CollectionClasses.BrandCultureCollection();
			_brandCultureCollection.SetContainingEntityInfo(this, "BrandEntity");

			_companyBrandCollection = new Obymobi.Data.CollectionClasses.CompanyBrandCollection();
			_companyBrandCollection.SetContainingEntityInfo(this, "BrandEntity");

			_genericalterationCollection = new Obymobi.Data.CollectionClasses.GenericalterationCollection();
			_genericalterationCollection.SetContainingEntityInfo(this, "BrandEntity");

			_genericcategoryCollection = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericcategoryCollection.SetContainingEntityInfo(this, "BrandEntity");

			_genericproductCollection = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollection.SetContainingEntityInfo(this, "BrandEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "BrandEntity");

			_userBrandCollection = new Obymobi.Data.CollectionClasses.UserBrandCollection();
			_userBrandCollection.SetContainingEntityInfo(this, "BrandEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="brandId">PK value for Brand which data should be fetched into this Brand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 brandId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BrandFieldIndex.BrandId].ForcedCurrentValueWrite(brandId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBrandDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BrandEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BrandRelations Relations
		{
			get	{ return new BrandRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BrandCulture' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandCultureCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCultureCollection(), (IEntityRelation)GetRelationsForField("BrandCultureCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.BrandCultureEntity, 0, null, null, null, "BrandCultureCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyBrand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyBrandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyBrandCollection(), (IEntityRelation)GetRelationsForField("CompanyBrandCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.CompanyBrandEntity, 0, null, null, null, "CompanyBrandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), (IEntityRelation)GetRelationsForField("GenericalterationCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, null, "GenericalterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserBrand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserBrandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserBrandCollection(), (IEntityRelation)GetRelationsForField("UserBrandCollection")[0], (int)Obymobi.Data.EntityType.BrandEntity, (int)Obymobi.Data.EntityType.UserBrandEntity, 0, null, null, null, "UserBrandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BrandId property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 BrandId
		{
			get { return (System.Int32)GetValue((int)BrandFieldIndex.BrandId, true); }
			set	{ SetValue((int)BrandFieldIndex.BrandId, value, true); }
		}

		/// <summary> The Name property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)BrandFieldIndex.Name, true); }
			set	{ SetValue((int)BrandFieldIndex.Name, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BrandFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)BrandFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)BrandFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)BrandFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)BrandFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)BrandFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)BrandFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)BrandFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CultureCode property of the Entity Brand<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Brand"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)BrandFieldIndex.CultureCode, true); }
			set	{ SetValue((int)BrandFieldIndex.CultureCode, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollection
		{
			get	{ return GetMultiAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollection. When set to true, AlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollection
		{
			get	{ return _alwaysFetchAlterationCollection; }
			set	{ _alwaysFetchAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollection already has been fetched. Setting this property to false when AlterationCollection has been fetched
		/// will clear the AlterationCollection collection well. Setting this property to true while AlterationCollection hasn't been fetched disables lazy loading for AlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollection
		{
			get { return _alreadyFetchedAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationCollection && !value && (_alterationCollection != null))
				{
					_alterationCollection.Clear();
				}
				_alreadyFetchedAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollection
		{
			get	{ return GetMultiAlterationoptionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollection. When set to true, AlterationoptionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollection
		{
			get	{ return _alwaysFetchAlterationoptionCollection; }
			set	{ _alwaysFetchAlterationoptionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollection already has been fetched. Setting this property to false when AlterationoptionCollection has been fetched
		/// will clear the AlterationoptionCollection collection well. Setting this property to true while AlterationoptionCollection hasn't been fetched disables lazy loading for AlterationoptionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollection
		{
			get { return _alreadyFetchedAlterationoptionCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollection && !value && (_alterationoptionCollection != null))
				{
					_alterationoptionCollection.Clear();
				}
				_alreadyFetchedAlterationoptionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BrandCultureEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBrandCultureCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.BrandCultureCollection BrandCultureCollection
		{
			get	{ return GetMultiBrandCultureCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BrandCultureCollection. When set to true, BrandCultureCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandCultureCollection is accessed. You can always execute/ a forced fetch by calling GetMultiBrandCultureCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandCultureCollection
		{
			get	{ return _alwaysFetchBrandCultureCollection; }
			set	{ _alwaysFetchBrandCultureCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandCultureCollection already has been fetched. Setting this property to false when BrandCultureCollection has been fetched
		/// will clear the BrandCultureCollection collection well. Setting this property to true while BrandCultureCollection hasn't been fetched disables lazy loading for BrandCultureCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandCultureCollection
		{
			get { return _alreadyFetchedBrandCultureCollection;}
			set 
			{
				if(_alreadyFetchedBrandCultureCollection && !value && (_brandCultureCollection != null))
				{
					_brandCultureCollection.Clear();
				}
				_alreadyFetchedBrandCultureCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyBrandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyBrandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyBrandCollection CompanyBrandCollection
		{
			get	{ return GetMultiCompanyBrandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyBrandCollection. When set to true, CompanyBrandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyBrandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyBrandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyBrandCollection
		{
			get	{ return _alwaysFetchCompanyBrandCollection; }
			set	{ _alwaysFetchCompanyBrandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyBrandCollection already has been fetched. Setting this property to false when CompanyBrandCollection has been fetched
		/// will clear the CompanyBrandCollection collection well. Setting this property to true while CompanyBrandCollection hasn't been fetched disables lazy loading for CompanyBrandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyBrandCollection
		{
			get { return _alreadyFetchedCompanyBrandCollection;}
			set 
			{
				if(_alreadyFetchedCompanyBrandCollection && !value && (_companyBrandCollection != null))
				{
					_companyBrandCollection.Clear();
				}
				_alreadyFetchedCompanyBrandCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationCollection GenericalterationCollection
		{
			get	{ return GetMultiGenericalterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationCollection. When set to true, GenericalterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericalterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationCollection
		{
			get	{ return _alwaysFetchGenericalterationCollection; }
			set	{ _alwaysFetchGenericalterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationCollection already has been fetched. Setting this property to false when GenericalterationCollection has been fetched
		/// will clear the GenericalterationCollection collection well. Setting this property to true while GenericalterationCollection hasn't been fetched disables lazy loading for GenericalterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationCollection
		{
			get { return _alreadyFetchedGenericalterationCollection;}
			set 
			{
				if(_alreadyFetchedGenericalterationCollection && !value && (_genericalterationCollection != null))
				{
					_genericalterationCollection.Clear();
				}
				_alreadyFetchedGenericalterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollection
		{
			get	{ return GetMultiGenericcategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollection. When set to true, GenericcategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericcategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollection
		{
			get	{ return _alwaysFetchGenericcategoryCollection; }
			set	{ _alwaysFetchGenericcategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollection already has been fetched. Setting this property to false when GenericcategoryCollection has been fetched
		/// will clear the GenericcategoryCollection collection well. Setting this property to true while GenericcategoryCollection hasn't been fetched disables lazy loading for GenericcategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollection
		{
			get { return _alreadyFetchedGenericcategoryCollection;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollection && !value && (_genericcategoryCollection != null))
				{
					_genericcategoryCollection.Clear();
				}
				_alreadyFetchedGenericcategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollection
		{
			get	{ return GetMultiGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollection. When set to true, GenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollection
		{
			get	{ return _alwaysFetchGenericproductCollection; }
			set	{ _alwaysFetchGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollection already has been fetched. Setting this property to false when GenericproductCollection has been fetched
		/// will clear the GenericproductCollection collection well. Setting this property to true while GenericproductCollection hasn't been fetched disables lazy loading for GenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollection
		{
			get { return _alreadyFetchedGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductCollection && !value && (_genericproductCollection != null))
				{
					_genericproductCollection.Clear();
				}
				_alreadyFetchedGenericproductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserBrandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserBrandCollection UserBrandCollection
		{
			get	{ return GetMultiUserBrandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserBrandCollection. When set to true, UserBrandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserBrandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserBrandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserBrandCollection
		{
			get	{ return _alwaysFetchUserBrandCollection; }
			set	{ _alwaysFetchUserBrandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserBrandCollection already has been fetched. Setting this property to false when UserBrandCollection has been fetched
		/// will clear the UserBrandCollection collection well. Setting this property to true while UserBrandCollection hasn't been fetched disables lazy loading for UserBrandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserBrandCollection
		{
			get { return _alreadyFetchedUserBrandCollection;}
			set 
			{
				if(_alreadyFetchedUserBrandCollection && !value && (_userBrandCollection != null))
				{
					_userBrandCollection.Clear();
				}
				_alreadyFetchedUserBrandCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.BrandEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
