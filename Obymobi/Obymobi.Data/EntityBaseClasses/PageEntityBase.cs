﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Page'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PageEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AttachmentCollection	_attachmentCollection;
		private bool	_alwaysFetchAttachmentCollection, _alreadyFetchedAttachmentCollection;
		private Obymobi.Data.CollectionClasses.AvailabilityCollection	_availabilityCollection;
		private bool	_alwaysFetchAvailabilityCollection, _alreadyFetchedAvailabilityCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection_;
		private bool	_alwaysFetchMediaCollection_, _alreadyFetchedMediaCollection_;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageTemplateCollection	_messageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCollection, _alreadyFetchedMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.PageCollection	_pageCollection;
		private bool	_alwaysFetchPageCollection, _alreadyFetchedPageCollection;
		private Obymobi.Data.CollectionClasses.PageElementCollection	_pageElementCollection;
		private bool	_alwaysFetchPageElementCollection, _alreadyFetchedPageElementCollection;
		private Obymobi.Data.CollectionClasses.PageLanguageCollection	_pageLanguageCollection;
		private bool	_alwaysFetchPageLanguageCollection, _alreadyFetchedPageLanguageCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private PageTemplateEntity _pageTemplateEntity;
		private bool	_alwaysFetchPageTemplateEntity, _alreadyFetchedPageTemplateEntity, _pageTemplateEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name PageTemplateEntity</summary>
			public static readonly string PageTemplateEntity = "PageTemplateEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AttachmentCollection</summary>
			public static readonly string AttachmentCollection = "AttachmentCollection";
			/// <summary>Member name AvailabilityCollection</summary>
			public static readonly string AvailabilityCollection = "AvailabilityCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MediaCollection_</summary>
			public static readonly string MediaCollection_ = "MediaCollection_";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageTemplateCollection</summary>
			public static readonly string MessageTemplateCollection = "MessageTemplateCollection";
			/// <summary>Member name PageCollection</summary>
			public static readonly string PageCollection = "PageCollection";
			/// <summary>Member name PageElementCollection</summary>
			public static readonly string PageElementCollection = "PageElementCollection";
			/// <summary>Member name PageLanguageCollection</summary>
			public static readonly string PageLanguageCollection = "PageLanguageCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PageEntityBase() :base("PageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		protected PageEntityBase(System.Int32 pageId):base("PageEntity")
		{
			InitClassFetch(pageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PageEntityBase(System.Int32 pageId, IPrefetchPath prefetchPathToUse): base("PageEntity")
		{
			InitClassFetch(pageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="validator">The custom validator object for this PageEntity</param>
		protected PageEntityBase(System.Int32 pageId, IValidator validator):base("PageEntity")
		{
			InitClassFetch(pageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_attachmentCollection = (Obymobi.Data.CollectionClasses.AttachmentCollection)info.GetValue("_attachmentCollection", typeof(Obymobi.Data.CollectionClasses.AttachmentCollection));
			_alwaysFetchAttachmentCollection = info.GetBoolean("_alwaysFetchAttachmentCollection");
			_alreadyFetchedAttachmentCollection = info.GetBoolean("_alreadyFetchedAttachmentCollection");

			_availabilityCollection = (Obymobi.Data.CollectionClasses.AvailabilityCollection)info.GetValue("_availabilityCollection", typeof(Obymobi.Data.CollectionClasses.AvailabilityCollection));
			_alwaysFetchAvailabilityCollection = info.GetBoolean("_alwaysFetchAvailabilityCollection");
			_alreadyFetchedAvailabilityCollection = info.GetBoolean("_alreadyFetchedAvailabilityCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_mediaCollection_ = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection_", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection_ = info.GetBoolean("_alwaysFetchMediaCollection_");
			_alreadyFetchedMediaCollection_ = info.GetBoolean("_alreadyFetchedMediaCollection_");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCollection)info.GetValue("_messageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCollection));
			_alwaysFetchMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCollection");

			_pageCollection = (Obymobi.Data.CollectionClasses.PageCollection)info.GetValue("_pageCollection", typeof(Obymobi.Data.CollectionClasses.PageCollection));
			_alwaysFetchPageCollection = info.GetBoolean("_alwaysFetchPageCollection");
			_alreadyFetchedPageCollection = info.GetBoolean("_alreadyFetchedPageCollection");

			_pageElementCollection = (Obymobi.Data.CollectionClasses.PageElementCollection)info.GetValue("_pageElementCollection", typeof(Obymobi.Data.CollectionClasses.PageElementCollection));
			_alwaysFetchPageElementCollection = info.GetBoolean("_alwaysFetchPageElementCollection");
			_alreadyFetchedPageElementCollection = info.GetBoolean("_alreadyFetchedPageElementCollection");

			_pageLanguageCollection = (Obymobi.Data.CollectionClasses.PageLanguageCollection)info.GetValue("_pageLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PageLanguageCollection));
			_alwaysFetchPageLanguageCollection = info.GetBoolean("_alwaysFetchPageLanguageCollection");
			_alreadyFetchedPageLanguageCollection = info.GetBoolean("_alreadyFetchedPageLanguageCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_pageTemplateEntity = (PageTemplateEntity)info.GetValue("_pageTemplateEntity", typeof(PageTemplateEntity));
			if(_pageTemplateEntity!=null)
			{
				_pageTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_pageTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchPageTemplateEntity = info.GetBoolean("_alwaysFetchPageTemplateEntity");
			_alreadyFetchedPageTemplateEntity = info.GetBoolean("_alreadyFetchedPageTemplateEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PageFieldIndex)fieldIndex)
			{
				case PageFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case PageFieldIndex.ParentPageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case PageFieldIndex.PageTemplateId:
					DesetupSyncPageTemplateEntity(true, false);
					_alreadyFetchedPageTemplateEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAttachmentCollection = (_attachmentCollection.Count > 0);
			_alreadyFetchedAvailabilityCollection = (_availabilityCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedMediaCollection_ = (_mediaCollection_.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageTemplateCollection = (_messageTemplateCollection.Count > 0);
			_alreadyFetchedPageCollection = (_pageCollection.Count > 0);
			_alreadyFetchedPageElementCollection = (_pageElementCollection.Count > 0);
			_alreadyFetchedPageLanguageCollection = (_pageLanguageCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedPageTemplateEntity = (_pageTemplateEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageIdParentPageId);
					break;
				case "PageTemplateEntity":
					toReturn.Add(Relations.PageTemplateEntityUsingPageTemplateId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingActionPageId);
					break;
				case "AttachmentCollection":
					toReturn.Add(Relations.AttachmentEntityUsingPageId);
					break;
				case "AvailabilityCollection":
					toReturn.Add(Relations.AvailabilityEntityUsingActionPageId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingPageId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingPageId);
					break;
				case "MediaCollection_":
					toReturn.Add(Relations.MediaEntityUsingActionPageId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingPageId);
					break;
				case "MessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateEntityUsingPageId);
					break;
				case "PageCollection":
					toReturn.Add(Relations.PageEntityUsingParentPageId);
					break;
				case "PageElementCollection":
					toReturn.Add(Relations.PageElementEntityUsingPageId);
					break;
				case "PageLanguageCollection":
					toReturn.Add(Relations.PageLanguageEntityUsingPageId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingPageId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingPageId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_attachmentCollection", (!this.MarkedForDeletion?_attachmentCollection:null));
			info.AddValue("_alwaysFetchAttachmentCollection", _alwaysFetchAttachmentCollection);
			info.AddValue("_alreadyFetchedAttachmentCollection", _alreadyFetchedAttachmentCollection);
			info.AddValue("_availabilityCollection", (!this.MarkedForDeletion?_availabilityCollection:null));
			info.AddValue("_alwaysFetchAvailabilityCollection", _alwaysFetchAvailabilityCollection);
			info.AddValue("_alreadyFetchedAvailabilityCollection", _alreadyFetchedAvailabilityCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_mediaCollection_", (!this.MarkedForDeletion?_mediaCollection_:null));
			info.AddValue("_alwaysFetchMediaCollection_", _alwaysFetchMediaCollection_);
			info.AddValue("_alreadyFetchedMediaCollection_", _alreadyFetchedMediaCollection_);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCollection", _alwaysFetchMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCollection", _alreadyFetchedMessageTemplateCollection);
			info.AddValue("_pageCollection", (!this.MarkedForDeletion?_pageCollection:null));
			info.AddValue("_alwaysFetchPageCollection", _alwaysFetchPageCollection);
			info.AddValue("_alreadyFetchedPageCollection", _alreadyFetchedPageCollection);
			info.AddValue("_pageElementCollection", (!this.MarkedForDeletion?_pageElementCollection:null));
			info.AddValue("_alwaysFetchPageElementCollection", _alwaysFetchPageElementCollection);
			info.AddValue("_alreadyFetchedPageElementCollection", _alreadyFetchedPageElementCollection);
			info.AddValue("_pageLanguageCollection", (!this.MarkedForDeletion?_pageLanguageCollection:null));
			info.AddValue("_alwaysFetchPageLanguageCollection", _alwaysFetchPageLanguageCollection);
			info.AddValue("_alreadyFetchedPageLanguageCollection", _alreadyFetchedPageLanguageCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_pageTemplateEntity", (!this.MarkedForDeletion?_pageTemplateEntity:null));
			info.AddValue("_pageTemplateEntityReturnsNewIfNotFound", _pageTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageTemplateEntity", _alwaysFetchPageTemplateEntity);
			info.AddValue("_alreadyFetchedPageTemplateEntity", _alreadyFetchedPageTemplateEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "PageTemplateEntity":
					_alreadyFetchedPageTemplateEntity = true;
					this.PageTemplateEntity = (PageTemplateEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AttachmentCollection":
					_alreadyFetchedAttachmentCollection = true;
					if(entity!=null)
					{
						this.AttachmentCollection.Add((AttachmentEntity)entity);
					}
					break;
				case "AvailabilityCollection":
					_alreadyFetchedAvailabilityCollection = true;
					if(entity!=null)
					{
						this.AvailabilityCollection.Add((AvailabilityEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollection_":
					_alreadyFetchedMediaCollection_ = true;
					if(entity!=null)
					{
						this.MediaCollection_.Add((MediaEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageTemplateCollection":
					_alreadyFetchedMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCollection.Add((MessageTemplateEntity)entity);
					}
					break;
				case "PageCollection":
					_alreadyFetchedPageCollection = true;
					if(entity!=null)
					{
						this.PageCollection.Add((PageEntity)entity);
					}
					break;
				case "PageElementCollection":
					_alreadyFetchedPageElementCollection = true;
					if(entity!=null)
					{
						this.PageElementCollection.Add((PageElementEntity)entity);
					}
					break;
				case "PageLanguageCollection":
					_alreadyFetchedPageLanguageCollection = true;
					if(entity!=null)
					{
						this.PageLanguageCollection.Add((PageLanguageEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "PageTemplateEntity":
					SetupSyncPageTemplateEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AttachmentCollection":
					_attachmentCollection.Add((AttachmentEntity)relatedEntity);
					break;
				case "AvailabilityCollection":
					_availabilityCollection.Add((AvailabilityEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "MediaCollection_":
					_mediaCollection_.Add((MediaEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageTemplateCollection":
					_messageTemplateCollection.Add((MessageTemplateEntity)relatedEntity);
					break;
				case "PageCollection":
					_pageCollection.Add((PageEntity)relatedEntity);
					break;
				case "PageElementCollection":
					_pageElementCollection.Add((PageElementEntity)relatedEntity);
					break;
				case "PageLanguageCollection":
					_pageLanguageCollection.Add((PageLanguageEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "PageTemplateEntity":
					DesetupSyncPageTemplateEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttachmentCollection":
					this.PerformRelatedEntityRemoval(_attachmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AvailabilityCollection":
					this.PerformRelatedEntityRemoval(_availabilityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection_":
					this.PerformRelatedEntityRemoval(_mediaCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageCollection":
					this.PerformRelatedEntityRemoval(_pageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageElementCollection":
					this.PerformRelatedEntityRemoval(_pageElementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageLanguageCollection":
					this.PerformRelatedEntityRemoval(_pageLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_pageTemplateEntity!=null)
			{
				toReturn.Add(_pageTemplateEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_attachmentCollection);
			toReturn.Add(_availabilityCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_mediaCollection_);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageTemplateCollection);
			toReturn.Add(_pageCollection);
			toReturn.Add(_pageElementCollection);
			toReturn.Add(_pageLanguageCollection);
			toReturn.Add(_scheduledMessageCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageId)
		{
			return FetchUsingPK(pageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttachmentCollection(forceFetch, _attachmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttachmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection GetMultiAttachmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttachmentCollection || forceFetch || _alwaysFetchAttachmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attachmentCollection);
				_attachmentCollection.SuppressClearInGetMulti=!forceFetch;
				_attachmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_attachmentCollection.GetMultiManyToOne(null, this, null, null, filter);
				_attachmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAttachmentCollection = true;
			}
			return _attachmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttachmentCollection'. These settings will be taken into account
		/// when the property AttachmentCollection is requested or GetMultiAttachmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttachmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attachmentCollection.SortClauses=sortClauses;
			_attachmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAvailabilityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAvailabilityCollection || forceFetch || _alwaysFetchAvailabilityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_availabilityCollection);
				_availabilityCollection.SuppressClearInGetMulti=!forceFetch;
				_availabilityCollection.EntityFactoryToUse = entityFactoryToUse;
				_availabilityCollection.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_availabilityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAvailabilityCollection = true;
			}
			return _availabilityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AvailabilityCollection'. These settings will be taken into account
		/// when the property AvailabilityCollection is requested or GetMultiAvailabilityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAvailabilityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_availabilityCollection.SortClauses=sortClauses;
			_availabilityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch)
		{
			return GetMultiMediaCollection_(forceFetch, _mediaCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection_(forceFetch, _mediaCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection_ || forceFetch || _alwaysFetchMediaCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection_);
				_mediaCollection_.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection_.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection_.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection_ = true;
			}
			return _mediaCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection_'. These settings will be taken into account
		/// when the property MediaCollection_ is requested or GetMultiMediaCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection_.SortClauses=sortClauses;
			_mediaCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCollection);
				_messageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_messageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCollection = true;
			}
			return _messageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCollection is requested or GetMultiMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageCollection(forceFetch, _pageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageCollection GetMultiPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageCollection || forceFetch || _alwaysFetchPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageCollection);
				_pageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageCollection.GetMultiManyToOne(this, null, null, filter);
				_pageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageCollection = true;
			}
			return _pageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageCollection'. These settings will be taken into account
		/// when the property PageCollection is requested or GetMultiPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageCollection.SortClauses=sortClauses;
			_pageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch)
		{
			return GetMultiPageElementCollection(forceFetch, _pageElementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageElementCollection(forceFetch, _pageElementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageElementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageElementCollection || forceFetch || _alwaysFetchPageElementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageElementCollection);
				_pageElementCollection.SuppressClearInGetMulti=!forceFetch;
				_pageElementCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageElementCollection.GetMultiManyToOne(null, this, filter);
				_pageElementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageElementCollection = true;
			}
			return _pageElementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageElementCollection'. These settings will be taken into account
		/// when the property PageElementCollection is requested or GetMultiPageElementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageElementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageElementCollection.SortClauses=sortClauses;
			_pageElementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch)
		{
			return GetMultiPageLanguageCollection(forceFetch, _pageLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageLanguageCollection(forceFetch, _pageLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageLanguageCollection || forceFetch || _alwaysFetchPageLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageLanguageCollection);
				_pageLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageLanguageCollection.GetMultiManyToOne(null, this, filter);
				_pageLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageLanguageCollection = true;
			}
			return _pageLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageLanguageCollection'. These settings will be taken into account
		/// when the property PageLanguageCollection is requested or GetMultiPageLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageLanguageCollection.SortClauses=sortClauses;
			_pageLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageIdParentPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public PageTemplateEntity GetSinglePageTemplateEntity()
		{
			return GetSinglePageTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageTemplateEntity' which is related to this entity.</returns>
		public virtual PageTemplateEntity GetSinglePageTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageTemplateEntity || forceFetch || _alwaysFetchPageTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageTemplateEntityUsingPageTemplateId);
				PageTemplateEntity newEntity = new PageTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageTemplateEntity = newEntity;
				_alreadyFetchedPageTemplateEntity = fetchResult;
			}
			return _pageTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId);
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("PageTemplateEntity", _pageTemplateEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AttachmentCollection", _attachmentCollection);
			toReturn.Add("AvailabilityCollection", _availabilityCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("MediaCollection_", _mediaCollection_);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageTemplateCollection", _messageTemplateCollection);
			toReturn.Add("PageCollection", _pageCollection);
			toReturn.Add("PageElementCollection", _pageElementCollection);
			toReturn.Add("PageLanguageCollection", _pageLanguageCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="validator">The validator object for this PageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "PageEntity");

			_attachmentCollection = new Obymobi.Data.CollectionClasses.AttachmentCollection();
			_attachmentCollection.SetContainingEntityInfo(this, "PageEntity");

			_availabilityCollection = new Obymobi.Data.CollectionClasses.AvailabilityCollection();
			_availabilityCollection.SetContainingEntityInfo(this, "PageEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "PageEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "PageEntity");

			_mediaCollection_ = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection_.SetContainingEntityInfo(this, "PageEntity_");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "PageEntity");

			_messageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCollection();
			_messageTemplateCollection.SetContainingEntityInfo(this, "PageEntity");

			_pageCollection = new Obymobi.Data.CollectionClasses.PageCollection();
			_pageCollection.SetContainingEntityInfo(this, "PageEntity");

			_pageElementCollection = new Obymobi.Data.CollectionClasses.PageElementCollection();
			_pageElementCollection.SetContainingEntityInfo(this, "PageEntity");

			_pageLanguageCollection = new Obymobi.Data.CollectionClasses.PageLanguageCollection();
			_pageLanguageCollection.SetContainingEntityInfo(this, "PageEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "PageEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "PageEntity");
			_pageEntityReturnsNewIfNotFound = true;
			_pageTemplateEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticPageRelations.PageEntityUsingPageIdParentPageIdStatic, true, signalRelatedEntity, "PageCollection", resetFKFields, new int[] { (int)PageFieldIndex.ParentPageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticPageRelations.PageEntityUsingPageIdParentPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticPageRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, signalRelatedEntity, "PageCollection", resetFKFields, new int[] { (int)PageFieldIndex.PageTemplateId } );		
			_pageTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageTemplateEntity(IEntityCore relatedEntity)
		{
			if(_pageTemplateEntity!=relatedEntity)
			{		
				DesetupSyncPageTemplateEntity(true, true);
				_pageTemplateEntity = (PageTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageTemplateEntity, new PropertyChangedEventHandler( OnPageTemplateEntityPropertyChanged ), "PageTemplateEntity", Obymobi.Data.RelationClasses.StaticPageRelations.PageTemplateEntityUsingPageTemplateIdStatic, true, ref _alreadyFetchedPageTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticPageRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "PageCollection", resetFKFields, new int[] { (int)PageFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticPageRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PageFieldIndex.PageId].ForcedCurrentValueWrite(pageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PageRelations Relations
		{
			get	{ return new PageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attachment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentCollection(), (IEntityRelation)GetRelationsForField("AttachmentCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.AttachmentEntity, 0, null, null, null, "AttachmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection_")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.MessageTemplateEntity, 0, null, null, null, "MessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageElementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageElementCollection(), (IEntityRelation)GetRelationsForField("PageElementCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.PageElementEntity, 0, null, null, null, "PageElementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageLanguageCollection(), (IEntityRelation)GetRelationsForField("PageLanguageCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.PageLanguageEntity, 0, null, null, null, "PageLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateCollection(), (IEntityRelation)GetRelationsForField("PageTemplateEntity")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.PageTemplateEntity, 0, null, null, null, "PageTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.PageEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PageId property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PageId
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.PageId, true); }
			set	{ SetValue((int)PageFieldIndex.PageId, value, true); }
		}

		/// <summary> The Name property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PageFieldIndex.Name, true); }
			set	{ SetValue((int)PageFieldIndex.Name, value, true); }
		}

		/// <summary> The SiteId property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SiteId
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.SiteId, true); }
			set	{ SetValue((int)PageFieldIndex.SiteId, value, true); }
		}

		/// <summary> The ParentPageId property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."ParentPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PageFieldIndex.ParentPageId, false); }
			set	{ SetValue((int)PageFieldIndex.ParentPageId, value, true); }
		}

		/// <summary> The PageType property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."PageType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageType
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.PageType, true); }
			set	{ SetValue((int)PageFieldIndex.PageType, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The PageTemplateId property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."PageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PageFieldIndex.PageTemplateId, false); }
			set	{ SetValue((int)PageFieldIndex.PageTemplateId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)PageFieldIndex.SortOrder, true); }
			set	{ SetValue((int)PageFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The Visible property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)PageFieldIndex.Visible, true); }
			set	{ SetValue((int)PageFieldIndex.Visible, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PageFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PageFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Page<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Page"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttachmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttachmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AttachmentCollection AttachmentCollection
		{
			get	{ return GetMultiAttachmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentCollection. When set to true, AttachmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAttachmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentCollection
		{
			get	{ return _alwaysFetchAttachmentCollection; }
			set	{ _alwaysFetchAttachmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentCollection already has been fetched. Setting this property to false when AttachmentCollection has been fetched
		/// will clear the AttachmentCollection collection well. Setting this property to true while AttachmentCollection hasn't been fetched disables lazy loading for AttachmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentCollection
		{
			get { return _alreadyFetchedAttachmentCollection;}
			set 
			{
				if(_alreadyFetchedAttachmentCollection && !value && (_attachmentCollection != null))
				{
					_attachmentCollection.Clear();
				}
				_alreadyFetchedAttachmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAvailabilityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection AvailabilityCollection
		{
			get	{ return GetMultiAvailabilityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityCollection. When set to true, AvailabilityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAvailabilityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityCollection
		{
			get	{ return _alwaysFetchAvailabilityCollection; }
			set	{ _alwaysFetchAvailabilityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityCollection already has been fetched. Setting this property to false when AvailabilityCollection has been fetched
		/// will clear the AvailabilityCollection collection well. Setting this property to true while AvailabilityCollection hasn't been fetched disables lazy loading for AvailabilityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityCollection
		{
			get { return _alreadyFetchedAvailabilityCollection;}
			set 
			{
				if(_alreadyFetchedAvailabilityCollection && !value && (_availabilityCollection != null))
				{
					_availabilityCollection.Clear();
				}
				_alreadyFetchedAvailabilityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection_
		{
			get	{ return GetMultiMediaCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection_. When set to true, MediaCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection_
		{
			get	{ return _alwaysFetchMediaCollection_; }
			set	{ _alwaysFetchMediaCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection_ already has been fetched. Setting this property to false when MediaCollection_ has been fetched
		/// will clear the MediaCollection_ collection well. Setting this property to true while MediaCollection_ hasn't been fetched disables lazy loading for MediaCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection_
		{
			get { return _alreadyFetchedMediaCollection_;}
			set 
			{
				if(_alreadyFetchedMediaCollection_ && !value && (_mediaCollection_ != null))
				{
					_mediaCollection_.Clear();
				}
				_alreadyFetchedMediaCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection MessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCollection. When set to true, MessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCollection collection well. Setting this property to true while MessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCollection && !value && (_messageTemplateCollection != null))
				{
					_messageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageCollection PageCollection
		{
			get	{ return GetMultiPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageCollection. When set to true, PageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageCollection
		{
			get	{ return _alwaysFetchPageCollection; }
			set	{ _alwaysFetchPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageCollection already has been fetched. Setting this property to false when PageCollection has been fetched
		/// will clear the PageCollection collection well. Setting this property to true while PageCollection hasn't been fetched disables lazy loading for PageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageCollection
		{
			get { return _alreadyFetchedPageCollection;}
			set 
			{
				if(_alreadyFetchedPageCollection && !value && (_pageCollection != null))
				{
					_pageCollection.Clear();
				}
				_alreadyFetchedPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageElementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageElementCollection PageElementCollection
		{
			get	{ return GetMultiPageElementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageElementCollection. When set to true, PageElementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageElementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageElementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageElementCollection
		{
			get	{ return _alwaysFetchPageElementCollection; }
			set	{ _alwaysFetchPageElementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageElementCollection already has been fetched. Setting this property to false when PageElementCollection has been fetched
		/// will clear the PageElementCollection collection well. Setting this property to true while PageElementCollection hasn't been fetched disables lazy loading for PageElementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageElementCollection
		{
			get { return _alreadyFetchedPageElementCollection;}
			set 
			{
				if(_alreadyFetchedPageElementCollection && !value && (_pageElementCollection != null))
				{
					_pageElementCollection.Clear();
				}
				_alreadyFetchedPageElementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageLanguageCollection PageLanguageCollection
		{
			get	{ return GetMultiPageLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageLanguageCollection. When set to true, PageLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageLanguageCollection
		{
			get	{ return _alwaysFetchPageLanguageCollection; }
			set	{ _alwaysFetchPageLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageLanguageCollection already has been fetched. Setting this property to false when PageLanguageCollection has been fetched
		/// will clear the PageLanguageCollection collection well. Setting this property to true while PageLanguageCollection hasn't been fetched disables lazy loading for PageLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageLanguageCollection
		{
			get { return _alreadyFetchedPageLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPageLanguageCollection && !value && (_pageLanguageCollection != null))
				{
					_pageLanguageCollection.Clear();
				}
				_alreadyFetchedPageLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageTemplateEntity PageTemplateEntity
		{
			get	{ return GetSinglePageTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageCollection", "PageTemplateEntity", _pageTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateEntity. When set to true, PageTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateEntity is accessed. You can always execute a forced fetch by calling GetSinglePageTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateEntity
		{
			get	{ return _alwaysFetchPageTemplateEntity; }
			set	{ _alwaysFetchPageTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateEntity already has been fetched. Setting this property to false when PageTemplateEntity has been fetched
		/// will set PageTemplateEntity to null as well. Setting this property to true while PageTemplateEntity hasn't been fetched disables lazy loading for PageTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateEntity
		{
			get { return _alreadyFetchedPageTemplateEntity;}
			set 
			{
				if(_alreadyFetchedPageTemplateEntity && !value)
				{
					this.PageTemplateEntity = null;
				}
				_alreadyFetchedPageTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageTemplateEntity is not found
		/// in the database. When set to true, PageTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _pageTemplateEntityReturnsNewIfNotFound; }
			set { _pageTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
