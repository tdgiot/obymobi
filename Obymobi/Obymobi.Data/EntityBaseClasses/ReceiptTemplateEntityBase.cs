﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ReceiptTemplate'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReceiptTemplateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReceiptTemplateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CheckoutMethodCollection	_checkoutMethodCollection;
		private bool	_alwaysFetchCheckoutMethodCollection, _alreadyFetchedCheckoutMethodCollection;
		private Obymobi.Data.CollectionClasses.ReceiptCollection	_receiptCollection;
		private bool	_alwaysFetchReceiptCollection, _alreadyFetchedReceiptCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name CheckoutMethodCollection</summary>
			public static readonly string CheckoutMethodCollection = "CheckoutMethodCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReceiptTemplateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReceiptTemplateEntityBase() :base("ReceiptTemplateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		protected ReceiptTemplateEntityBase(System.Int32 receiptTemplateId):base("ReceiptTemplateEntity")
		{
			InitClassFetch(receiptTemplateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReceiptTemplateEntityBase(System.Int32 receiptTemplateId, IPrefetchPath prefetchPathToUse): base("ReceiptTemplateEntity")
		{
			InitClassFetch(receiptTemplateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="validator">The custom validator object for this ReceiptTemplateEntity</param>
		protected ReceiptTemplateEntityBase(System.Int32 receiptTemplateId, IValidator validator):base("ReceiptTemplateEntity")
		{
			InitClassFetch(receiptTemplateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceiptTemplateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_checkoutMethodCollection = (Obymobi.Data.CollectionClasses.CheckoutMethodCollection)info.GetValue("_checkoutMethodCollection", typeof(Obymobi.Data.CollectionClasses.CheckoutMethodCollection));
			_alwaysFetchCheckoutMethodCollection = info.GetBoolean("_alwaysFetchCheckoutMethodCollection");
			_alreadyFetchedCheckoutMethodCollection = info.GetBoolean("_alreadyFetchedCheckoutMethodCollection");

			_receiptCollection = (Obymobi.Data.CollectionClasses.ReceiptCollection)info.GetValue("_receiptCollection", typeof(Obymobi.Data.CollectionClasses.ReceiptCollection));
			_alwaysFetchReceiptCollection = info.GetBoolean("_alwaysFetchReceiptCollection");
			_alreadyFetchedReceiptCollection = info.GetBoolean("_alreadyFetchedReceiptCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReceiptTemplateFieldIndex)fieldIndex)
			{
				case ReceiptTemplateFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCheckoutMethodCollection = (_checkoutMethodCollection.Count > 0);
			_alreadyFetchedReceiptCollection = (_receiptCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "CheckoutMethodCollection":
					toReturn.Add(Relations.CheckoutMethodEntityUsingReceiptTemplateId);
					break;
				case "ReceiptCollection":
					toReturn.Add(Relations.ReceiptEntityUsingReceiptTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_checkoutMethodCollection", (!this.MarkedForDeletion?_checkoutMethodCollection:null));
			info.AddValue("_alwaysFetchCheckoutMethodCollection", _alwaysFetchCheckoutMethodCollection);
			info.AddValue("_alreadyFetchedCheckoutMethodCollection", _alreadyFetchedCheckoutMethodCollection);
			info.AddValue("_receiptCollection", (!this.MarkedForDeletion?_receiptCollection:null));
			info.AddValue("_alwaysFetchReceiptCollection", _alwaysFetchReceiptCollection);
			info.AddValue("_alreadyFetchedReceiptCollection", _alreadyFetchedReceiptCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "CheckoutMethodCollection":
					_alreadyFetchedCheckoutMethodCollection = true;
					if(entity!=null)
					{
						this.CheckoutMethodCollection.Add((CheckoutMethodEntity)entity);
					}
					break;
				case "ReceiptCollection":
					_alreadyFetchedReceiptCollection = true;
					if(entity!=null)
					{
						this.ReceiptCollection.Add((ReceiptEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "CheckoutMethodCollection":
					_checkoutMethodCollection.Add((CheckoutMethodEntity)relatedEntity);
					break;
				case "ReceiptCollection":
					_receiptCollection.Add((ReceiptEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "CheckoutMethodCollection":
					this.PerformRelatedEntityRemoval(_checkoutMethodCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceiptCollection":
					this.PerformRelatedEntityRemoval(_receiptCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_checkoutMethodCollection);
			toReturn.Add(_receiptCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptTemplateId)
		{
			return FetchUsingPK(receiptTemplateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptTemplateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(receiptTemplateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(receiptTemplateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 receiptTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(receiptTemplateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReceiptTemplateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReceiptTemplateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, _checkoutMethodCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCheckoutMethodCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection GetMultiCheckoutMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCheckoutMethodCollection || forceFetch || _alwaysFetchCheckoutMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_checkoutMethodCollection);
				_checkoutMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_checkoutMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_checkoutMethodCollection.GetMultiManyToOne(null, null, null, this, filter);
				_checkoutMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCheckoutMethodCollection = true;
			}
			return _checkoutMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CheckoutMethodCollection'. These settings will be taken into account
		/// when the property CheckoutMethodCollection is requested or GetMultiCheckoutMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCheckoutMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_checkoutMethodCollection.SortClauses=sortClauses;
			_checkoutMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceiptCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceiptCollection || forceFetch || _alwaysFetchReceiptCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receiptCollection);
				_receiptCollection.SuppressClearInGetMulti=!forceFetch;
				_receiptCollection.EntityFactoryToUse = entityFactoryToUse;
				_receiptCollection.GetMultiManyToOne(null, null, null, this, filter);
				_receiptCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceiptCollection = true;
			}
			return _receiptCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceiptCollection'. These settings will be taken into account
		/// when the property ReceiptCollection is requested or GetMultiReceiptCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceiptCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receiptCollection.SortClauses=sortClauses;
			_receiptCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("CheckoutMethodCollection", _checkoutMethodCollection);
			toReturn.Add("ReceiptCollection", _receiptCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="validator">The validator object for this ReceiptTemplateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 receiptTemplateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(receiptTemplateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_checkoutMethodCollection = new Obymobi.Data.CollectionClasses.CheckoutMethodCollection();
			_checkoutMethodCollection.SetContainingEntityInfo(this, "ReceiptTemplateEntity");

			_receiptCollection = new Obymobi.Data.CollectionClasses.ReceiptCollection();
			_receiptCollection.SetContainingEntityInfo(this, "ReceiptTemplateEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerContactInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxBreakdown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerContactEmail", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReceiptTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ReceiptTemplateCollection", resetFKFields, new int[] { (int)ReceiptTemplateFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReceiptTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="receiptTemplateId">PK value for ReceiptTemplate which data should be fetched into this ReceiptTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 receiptTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReceiptTemplateFieldIndex.ReceiptTemplateId].ForcedCurrentValueWrite(receiptTemplateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReceiptTemplateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReceiptTemplateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReceiptTemplateRelations Relations
		{
			get	{ return new ReceiptTemplateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodCollection")[0], (int)Obymobi.Data.EntityType.ReceiptTemplateEntity, (int)Obymobi.Data.EntityType.CheckoutMethodEntity, 0, null, null, null, "CheckoutMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReceiptCollection(), (IEntityRelation)GetRelationsForField("ReceiptCollection")[0], (int)Obymobi.Data.EntityType.ReceiptTemplateEntity, (int)Obymobi.Data.EntityType.ReceiptEntity, 0, null, null, null, "ReceiptCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ReceiptTemplateEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReceiptTemplateId property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."ReceiptTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReceiptTemplateId
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.ReceiptTemplateId, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.ReceiptTemplateId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Name, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Name, value, true); }
		}

		/// <summary> The SellerName property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerName, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerName, value, true); }
		}

		/// <summary> The SellerAddress property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerAddress
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerAddress, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerAddress, value, true); }
		}

		/// <summary> The SellerContactInfo property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerContactInfo"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerContactInfo
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerContactInfo, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerContactInfo, value, true); }
		}

		/// <summary> The TaxBreakdown property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."TaxBreakdown"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.TaxBreakdown TaxBreakdown
		{
			get { return (Obymobi.Enums.TaxBreakdown)GetValue((int)ReceiptTemplateFieldIndex.TaxBreakdown, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.TaxBreakdown, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReceiptTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReceiptTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The Email property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Email, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Email, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The VatNumber property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."VatNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.VatNumber, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.VatNumber, value, true); }
		}

		/// <summary> The SellerContactEmail property of the Entity ReceiptTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReceiptTemplate"."SellerContactEmail"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SellerContactEmail
		{
			get { return (System.String)GetValue((int)ReceiptTemplateFieldIndex.SellerContactEmail, true); }
			set	{ SetValue((int)ReceiptTemplateFieldIndex.SellerContactEmail, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCheckoutMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodCollection CheckoutMethodCollection
		{
			get	{ return GetMultiCheckoutMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodCollection. When set to true, CheckoutMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCheckoutMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodCollection
		{
			get	{ return _alwaysFetchCheckoutMethodCollection; }
			set	{ _alwaysFetchCheckoutMethodCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodCollection already has been fetched. Setting this property to false when CheckoutMethodCollection has been fetched
		/// will clear the CheckoutMethodCollection collection well. Setting this property to true while CheckoutMethodCollection hasn't been fetched disables lazy loading for CheckoutMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodCollection
		{
			get { return _alreadyFetchedCheckoutMethodCollection;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodCollection && !value && (_checkoutMethodCollection != null))
				{
					_checkoutMethodCollection.Clear();
				}
				_alreadyFetchedCheckoutMethodCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceiptCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection ReceiptCollection
		{
			get	{ return GetMultiReceiptCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptCollection. When set to true, ReceiptCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceiptCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptCollection
		{
			get	{ return _alwaysFetchReceiptCollection; }
			set	{ _alwaysFetchReceiptCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptCollection already has been fetched. Setting this property to false when ReceiptCollection has been fetched
		/// will clear the ReceiptCollection collection well. Setting this property to true while ReceiptCollection hasn't been fetched disables lazy loading for ReceiptCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptCollection
		{
			get { return _alreadyFetchedReceiptCollection;}
			set 
			{
				if(_alreadyFetchedReceiptCollection && !value && (_receiptCollection != null))
				{
					_receiptCollection.Clear();
				}
				_alreadyFetchedReceiptCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceiptTemplateCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReceiptTemplateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
