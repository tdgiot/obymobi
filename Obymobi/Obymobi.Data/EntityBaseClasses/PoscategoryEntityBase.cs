﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Poscategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PoscategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PoscategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CategoryCollection	_categoryCollection;
		private bool	_alwaysFetchCategoryCollection, _alreadyFetchedCategoryCollection;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaCategory;
		private bool	_alwaysFetchMenuCollectionViaCategory, _alreadyFetchedMenuCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCategory;
		private bool	_alwaysFetchRouteCollectionViaCategory, _alreadyFetchedRouteCollectionViaCategory;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name MenuCollectionViaCategory</summary>
			public static readonly string MenuCollectionViaCategory = "MenuCollectionViaCategory";
			/// <summary>Member name RouteCollectionViaCategory</summary>
			public static readonly string RouteCollectionViaCategory = "RouteCollectionViaCategory";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PoscategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PoscategoryEntityBase() :base("PoscategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		protected PoscategoryEntityBase(System.Int32 poscategoryId):base("PoscategoryEntity")
		{
			InitClassFetch(poscategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PoscategoryEntityBase(System.Int32 poscategoryId, IPrefetchPath prefetchPathToUse): base("PoscategoryEntity")
		{
			InitClassFetch(poscategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="validator">The custom validator object for this PoscategoryEntity</param>
		protected PoscategoryEntityBase(System.Int32 poscategoryId, IValidator validator):base("PoscategoryEntity")
		{
			InitClassFetch(poscategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PoscategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_categoryCollection = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollection", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollection = info.GetBoolean("_alwaysFetchCategoryCollection");
			_alreadyFetchedCategoryCollection = info.GetBoolean("_alreadyFetchedCategoryCollection");
			_menuCollectionViaCategory = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaCategory = info.GetBoolean("_alwaysFetchMenuCollectionViaCategory");
			_alreadyFetchedMenuCollectionViaCategory = info.GetBoolean("_alreadyFetchedMenuCollectionViaCategory");

			_routeCollectionViaCategory = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCategory = info.GetBoolean("_alwaysFetchRouteCollectionViaCategory");
			_alreadyFetchedRouteCollectionViaCategory = info.GetBoolean("_alreadyFetchedRouteCollectionViaCategory");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PoscategoryFieldIndex)fieldIndex)
			{
				case PoscategoryFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCategoryCollection = (_categoryCollection.Count > 0);
			_alreadyFetchedMenuCollectionViaCategory = (_menuCollectionViaCategory.Count > 0);
			_alreadyFetchedRouteCollectionViaCategory = (_routeCollectionViaCategory.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "CategoryCollection":
					toReturn.Add(Relations.CategoryEntityUsingPoscategoryId);
					break;
				case "MenuCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingPoscategoryId, "PoscategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.MenuEntityUsingMenuId, "Category_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingPoscategoryId, "PoscategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.RouteEntityUsingRouteId, "Category_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_categoryCollection", (!this.MarkedForDeletion?_categoryCollection:null));
			info.AddValue("_alwaysFetchCategoryCollection", _alwaysFetchCategoryCollection);
			info.AddValue("_alreadyFetchedCategoryCollection", _alreadyFetchedCategoryCollection);
			info.AddValue("_menuCollectionViaCategory", (!this.MarkedForDeletion?_menuCollectionViaCategory:null));
			info.AddValue("_alwaysFetchMenuCollectionViaCategory", _alwaysFetchMenuCollectionViaCategory);
			info.AddValue("_alreadyFetchedMenuCollectionViaCategory", _alreadyFetchedMenuCollectionViaCategory);
			info.AddValue("_routeCollectionViaCategory", (!this.MarkedForDeletion?_routeCollectionViaCategory:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCategory", _alwaysFetchRouteCollectionViaCategory);
			info.AddValue("_alreadyFetchedRouteCollectionViaCategory", _alreadyFetchedRouteCollectionViaCategory);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "CategoryCollection":
					_alreadyFetchedCategoryCollection = true;
					if(entity!=null)
					{
						this.CategoryCollection.Add((CategoryEntity)entity);
					}
					break;
				case "MenuCollectionViaCategory":
					_alreadyFetchedMenuCollectionViaCategory = true;
					if(entity!=null)
					{
						this.MenuCollectionViaCategory.Add((MenuEntity)entity);
					}
					break;
				case "RouteCollectionViaCategory":
					_alreadyFetchedRouteCollectionViaCategory = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCategory.Add((RouteEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "CategoryCollection":
					_categoryCollection.Add((CategoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "CategoryCollection":
					this.PerformRelatedEntityRemoval(_categoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_categoryCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 poscategoryId)
		{
			return FetchUsingPK(poscategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 poscategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(poscategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 poscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(poscategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 poscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(poscategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PoscategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PoscategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryCollection || forceFetch || _alwaysFetchCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollection);
				_categoryCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_categoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollection = true;
			}
			return _categoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollection'. These settings will be taken into account
		/// when the property CategoryCollection is requested or GetMultiCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollection.SortClauses=sortClauses;
			_categoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch)
		{
			return GetMultiMenuCollectionViaCategory(forceFetch, _menuCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaCategory || forceFetch || _alwaysFetchMenuCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PoscategoryFields.PoscategoryId, ComparisonOperator.Equal, this.PoscategoryId, "PoscategoryEntity__"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaCategory.GetMulti(filter, GetRelationsForField("MenuCollectionViaCategory"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaCategory = true;
			}
			return _menuCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaCategory'. These settings will be taken into account
		/// when the property MenuCollectionViaCategory is requested or GetMultiMenuCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaCategory.SortClauses=sortClauses;
			_menuCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCategory(forceFetch, _routeCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCategory || forceFetch || _alwaysFetchRouteCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PoscategoryFields.PoscategoryId, ComparisonOperator.Equal, this.PoscategoryId, "PoscategoryEntity__"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCategory.GetMulti(filter, GetRelationsForField("RouteCollectionViaCategory"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCategory = true;
			}
			return _routeCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCategory'. These settings will be taken into account
		/// when the property RouteCollectionViaCategory is requested or GetMultiRouteCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCategory.SortClauses=sortClauses;
			_routeCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("CategoryCollection", _categoryCollection);
			toReturn.Add("MenuCollectionViaCategory", _menuCollectionViaCategory);
			toReturn.Add("RouteCollectionViaCategory", _routeCollectionViaCategory);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="validator">The validator object for this PoscategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 poscategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(poscategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_categoryCollection = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollection.SetContainingEntityInfo(this, "PoscategoryEntity");
			_menuCollectionViaCategory = new Obymobi.Data.CollectionClasses.MenuCollection();
			_routeCollectionViaCategory = new Obymobi.Data.CollectionClasses.RouteCollection();
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PoscategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronisationBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueCenter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPoscategoryRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PoscategoryCollection", resetFKFields, new int[] { (int)PoscategoryFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPoscategoryRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="poscategoryId">PK value for Poscategory which data should be fetched into this Poscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 poscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PoscategoryFieldIndex.PoscategoryId].ForcedCurrentValueWrite(poscategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePoscategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PoscategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PoscategoryRelations Relations
		{
			get	{ return new PoscategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryCollection")[0], (int)Obymobi.Data.EntityType.PoscategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingPoscategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PoscategoryEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaCategory"), "MenuCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingPoscategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PoscategoryEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCategory"), "RouteCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PoscategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PoscategoryId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."PoscategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PoscategoryId
		{
			get { return (System.Int32)GetValue((int)PoscategoryFieldIndex.PoscategoryId, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.PoscategoryId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PoscategoryFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalId
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.ExternalId, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The Name property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.Name, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The CreatedInBatchId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."CreatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PoscategoryFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)PoscategoryFieldIndex.CreatedInBatchId, value, true); }
		}

		/// <summary> The UpdatedInBatchId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."UpdatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PoscategoryFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)PoscategoryFieldIndex.UpdatedInBatchId, value, true); }
		}

		/// <summary> The SynchronisationBatchId property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."SynchronisationBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PoscategoryFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)PoscategoryFieldIndex.SynchronisationBatchId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PoscategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PoscategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PoscategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PoscategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PoscategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PoscategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The RevenueCenter property of the Entity Poscategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Poscategory"."RevenueCenter"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RevenueCenter
		{
			get { return (System.String)GetValue((int)PoscategoryFieldIndex.RevenueCenter, true); }
			set	{ SetValue((int)PoscategoryFieldIndex.RevenueCenter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollection
		{
			get	{ return GetMultiCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollection. When set to true, CategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollection
		{
			get	{ return _alwaysFetchCategoryCollection; }
			set	{ _alwaysFetchCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollection already has been fetched. Setting this property to false when CategoryCollection has been fetched
		/// will clear the CategoryCollection collection well. Setting this property to true while CategoryCollection hasn't been fetched disables lazy loading for CategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollection
		{
			get { return _alreadyFetchedCategoryCollection;}
			set 
			{
				if(_alreadyFetchedCategoryCollection && !value && (_categoryCollection != null))
				{
					_categoryCollection.Clear();
				}
				_alreadyFetchedCategoryCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaCategory
		{
			get { return GetMultiMenuCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaCategory. When set to true, MenuCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaCategory
		{
			get	{ return _alwaysFetchMenuCollectionViaCategory; }
			set	{ _alwaysFetchMenuCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaCategory already has been fetched. Setting this property to false when MenuCollectionViaCategory has been fetched
		/// will clear the MenuCollectionViaCategory collection well. Setting this property to true while MenuCollectionViaCategory hasn't been fetched disables lazy loading for MenuCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaCategory
		{
			get { return _alreadyFetchedMenuCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaCategory && !value && (_menuCollectionViaCategory != null))
				{
					_menuCollectionViaCategory.Clear();
				}
				_alreadyFetchedMenuCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCategory
		{
			get { return GetMultiRouteCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCategory. When set to true, RouteCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCategory
		{
			get	{ return _alwaysFetchRouteCollectionViaCategory; }
			set	{ _alwaysFetchRouteCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCategory already has been fetched. Setting this property to false when RouteCollectionViaCategory has been fetched
		/// will clear the RouteCollectionViaCategory collection well. Setting this property to true while RouteCollectionViaCategory hasn't been fetched disables lazy loading for RouteCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCategory
		{
			get { return _alreadyFetchedRouteCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCategory && !value && (_routeCollectionViaCategory != null))
				{
					_routeCollectionViaCategory.Clear();
				}
				_alreadyFetchedRouteCollectionViaCategory = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PoscategoryCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PoscategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
