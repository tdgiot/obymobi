﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AdvertisementConfigurationAdvertisement'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AdvertisementConfigurationAdvertisementEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AdvertisementConfigurationAdvertisementEntity"; }
		}
	
		#region Class Member Declarations
		private AdvertisementEntity _advertisementEntity;
		private bool	_alwaysFetchAdvertisementEntity, _alreadyFetchedAdvertisementEntity, _advertisementEntityReturnsNewIfNotFound;
		private AdvertisementConfigurationEntity _advertisementConfigurationEntity;
		private bool	_alwaysFetchAdvertisementConfigurationEntity, _alreadyFetchedAdvertisementConfigurationEntity, _advertisementConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementEntity</summary>
			public static readonly string AdvertisementEntity = "AdvertisementEntity";
			/// <summary>Member name AdvertisementConfigurationEntity</summary>
			public static readonly string AdvertisementConfigurationEntity = "AdvertisementConfigurationEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AdvertisementConfigurationAdvertisementEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AdvertisementConfigurationAdvertisementEntityBase() :base("AdvertisementConfigurationAdvertisementEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		protected AdvertisementConfigurationAdvertisementEntityBase(System.Int32 advertisementConfigurationAdvertisementId):base("AdvertisementConfigurationAdvertisementEntity")
		{
			InitClassFetch(advertisementConfigurationAdvertisementId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AdvertisementConfigurationAdvertisementEntityBase(System.Int32 advertisementConfigurationAdvertisementId, IPrefetchPath prefetchPathToUse): base("AdvertisementConfigurationAdvertisementEntity")
		{
			InitClassFetch(advertisementConfigurationAdvertisementId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="validator">The custom validator object for this AdvertisementConfigurationAdvertisementEntity</param>
		protected AdvertisementConfigurationAdvertisementEntityBase(System.Int32 advertisementConfigurationAdvertisementId, IValidator validator):base("AdvertisementConfigurationAdvertisementEntity")
		{
			InitClassFetch(advertisementConfigurationAdvertisementId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdvertisementConfigurationAdvertisementEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementEntity = (AdvertisementEntity)info.GetValue("_advertisementEntity", typeof(AdvertisementEntity));
			if(_advertisementEntity!=null)
			{
				_advertisementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementEntity = info.GetBoolean("_alwaysFetchAdvertisementEntity");
			_alreadyFetchedAdvertisementEntity = info.GetBoolean("_alreadyFetchedAdvertisementEntity");

			_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)info.GetValue("_advertisementConfigurationEntity", typeof(AdvertisementConfigurationEntity));
			if(_advertisementConfigurationEntity!=null)
			{
				_advertisementConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementConfigurationEntity = info.GetBoolean("_alwaysFetchAdvertisementConfigurationEntity");
			_alreadyFetchedAdvertisementConfigurationEntity = info.GetBoolean("_alreadyFetchedAdvertisementConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AdvertisementConfigurationAdvertisementFieldIndex)fieldIndex)
			{
				case AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationId:
					DesetupSyncAdvertisementConfigurationEntity(true, false);
					_alreadyFetchedAdvertisementConfigurationEntity = false;
					break;
				case AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementId:
					DesetupSyncAdvertisementEntity(true, false);
					_alreadyFetchedAdvertisementEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementEntity = (_advertisementEntity != null);
			_alreadyFetchedAdvertisementConfigurationEntity = (_advertisementConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementEntity":
					toReturn.Add(Relations.AdvertisementEntityUsingAdvertisementId);
					break;
				case "AdvertisementConfigurationEntity":
					toReturn.Add(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementEntity", (!this.MarkedForDeletion?_advertisementEntity:null));
			info.AddValue("_advertisementEntityReturnsNewIfNotFound", _advertisementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementEntity", _alwaysFetchAdvertisementEntity);
			info.AddValue("_alreadyFetchedAdvertisementEntity", _alreadyFetchedAdvertisementEntity);
			info.AddValue("_advertisementConfigurationEntity", (!this.MarkedForDeletion?_advertisementConfigurationEntity:null));
			info.AddValue("_advertisementConfigurationEntityReturnsNewIfNotFound", _advertisementConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementConfigurationEntity", _alwaysFetchAdvertisementConfigurationEntity);
			info.AddValue("_alreadyFetchedAdvertisementConfigurationEntity", _alreadyFetchedAdvertisementConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementEntity":
					_alreadyFetchedAdvertisementEntity = true;
					this.AdvertisementEntity = (AdvertisementEntity)entity;
					break;
				case "AdvertisementConfigurationEntity":
					_alreadyFetchedAdvertisementConfigurationEntity = true;
					this.AdvertisementConfigurationEntity = (AdvertisementConfigurationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					SetupSyncAdvertisementEntity(relatedEntity);
					break;
				case "AdvertisementConfigurationEntity":
					SetupSyncAdvertisementConfigurationEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementEntity":
					DesetupSyncAdvertisementEntity(false, true);
					break;
				case "AdvertisementConfigurationEntity":
					DesetupSyncAdvertisementConfigurationEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_advertisementEntity!=null)
			{
				toReturn.Add(_advertisementEntity);
			}
			if(_advertisementConfigurationEntity!=null)
			{
				toReturn.Add(_advertisementConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="advertisementId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationIdAdvertisementId(System.Int32 advertisementConfigurationId, System.Int32 advertisementId)
		{
			return FetchUsingUCAdvertisementConfigurationIdAdvertisementId( advertisementConfigurationId,  advertisementId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="advertisementId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationIdAdvertisementId(System.Int32 advertisementConfigurationId, System.Int32 advertisementId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAdvertisementConfigurationIdAdvertisementId( advertisementConfigurationId,  advertisementId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="advertisementId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationIdAdvertisementId(System.Int32 advertisementConfigurationId, System.Int32 advertisementId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAdvertisementConfigurationIdAdvertisementId( advertisementConfigurationId,  advertisementId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="advertisementId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationIdAdvertisementId(System.Int32 advertisementConfigurationId, System.Int32 advertisementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((AdvertisementConfigurationAdvertisementDAO)CreateDAOInstance()).FetchAdvertisementConfigurationAdvertisementUsingUCAdvertisementConfigurationIdAdvertisementId(this, this.Transaction, advertisementConfigurationId, advertisementId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementConfigurationAdvertisementId)
		{
			return FetchUsingPK(advertisementConfigurationAdvertisementId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementConfigurationAdvertisementId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(advertisementConfigurationAdvertisementId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementConfigurationAdvertisementId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(advertisementConfigurationAdvertisementId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementConfigurationAdvertisementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(advertisementConfigurationAdvertisementId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AdvertisementConfigurationAdvertisementId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AdvertisementConfigurationAdvertisementRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public AdvertisementEntity GetSingleAdvertisementEntity()
		{
			return GetSingleAdvertisementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementEntity' which is related to this entity.</returns>
		public virtual AdvertisementEntity GetSingleAdvertisementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementEntity || forceFetch || _alwaysFetchAdvertisementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementEntityUsingAdvertisementId);
				AdvertisementEntity newEntity = new AdvertisementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementId);
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementEntity = newEntity;
				_alreadyFetchedAdvertisementEntity = fetchResult;
			}
			return _advertisementEntity;
		}


		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity()
		{
			return GetSingleAdvertisementConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public virtual AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementConfigurationEntity || forceFetch || _alwaysFetchAdvertisementConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
				AdvertisementConfigurationEntity newEntity = new AdvertisementConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementConfigurationEntity = newEntity;
				_alreadyFetchedAdvertisementConfigurationEntity = fetchResult;
			}
			return _advertisementConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementEntity", _advertisementEntity);
			toReturn.Add("AdvertisementConfigurationEntity", _advertisementConfigurationEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="validator">The validator object for this AdvertisementConfigurationAdvertisementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 advertisementConfigurationAdvertisementId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(advertisementConfigurationAdvertisementId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_advertisementEntityReturnsNewIfNotFound = true;
			_advertisementConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementConfigurationAdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _advertisementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticAdvertisementConfigurationAdvertisementRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, signalRelatedEntity, "AdvertisementConfigurationAdvertisementCollection", resetFKFields, new int[] { (int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementId } );		
			_advertisementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementEntity(IEntityCore relatedEntity)
		{
			if(_advertisementEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementEntity(true, true);
				_advertisementEntity = (AdvertisementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementEntity, new PropertyChangedEventHandler( OnAdvertisementEntityPropertyChanged ), "AdvertisementEntity", Obymobi.Data.RelationClasses.StaticAdvertisementConfigurationAdvertisementRelations.AdvertisementEntityUsingAdvertisementIdStatic, true, ref _alreadyFetchedAdvertisementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticAdvertisementConfigurationAdvertisementRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, signalRelatedEntity, "AdvertisementConfigurationAdvertisementCollection", resetFKFields, new int[] { (int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationId } );		
			_advertisementConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_advertisementConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementConfigurationEntity(true, true);
				_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticAdvertisementConfigurationAdvertisementRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, ref _alreadyFetchedAdvertisementConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="advertisementConfigurationAdvertisementId">PK value for AdvertisementConfigurationAdvertisement which data should be fetched into this AdvertisementConfigurationAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 advertisementConfigurationAdvertisementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationAdvertisementId].ForcedCurrentValueWrite(advertisementConfigurationAdvertisementId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAdvertisementConfigurationAdvertisementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AdvertisementConfigurationAdvertisementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AdvertisementConfigurationAdvertisementRelations Relations
		{
			get	{ return new AdvertisementConfigurationAdvertisementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementEntity")[0], (int)Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementConfigurationCollection(), (IEntityRelation)GetRelationsForField("AdvertisementConfigurationEntity")[0], (int)Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity, (int)Obymobi.Data.EntityType.AdvertisementConfigurationEntity, 0, null, null, null, "AdvertisementConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdvertisementConfigurationAdvertisementId property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."AdvertisementConfigurationAdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AdvertisementConfigurationAdvertisementId
		{
			get { return (System.Int32)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationAdvertisementId, true); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationAdvertisementId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The AdvertisementConfigurationId property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."AdvertisementConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AdvertisementConfigurationId
		{
			get { return (System.Int32)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationId, true); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementConfigurationId, value, true); }
		}

		/// <summary> The AdvertisementId property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."AdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AdvertisementId
		{
			get { return (System.Int32)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementId, true); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.AdvertisementId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity AdvertisementConfigurationAdvertisement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementConfigurationAdvertisement"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AdvertisementConfigurationAdvertisementFieldIndex.SortOrder, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AdvertisementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementEntity AdvertisementEntity
		{
			get	{ return GetSingleAdvertisementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AdvertisementConfigurationAdvertisementCollection", "AdvertisementEntity", _advertisementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementEntity. When set to true, AdvertisementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementEntity
		{
			get	{ return _alwaysFetchAdvertisementEntity; }
			set	{ _alwaysFetchAdvertisementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementEntity already has been fetched. Setting this property to false when AdvertisementEntity has been fetched
		/// will set AdvertisementEntity to null as well. Setting this property to true while AdvertisementEntity hasn't been fetched disables lazy loading for AdvertisementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementEntity
		{
			get { return _alreadyFetchedAdvertisementEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementEntity && !value)
				{
					this.AdvertisementEntity = null;
				}
				_alreadyFetchedAdvertisementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementEntity is not found
		/// in the database. When set to true, AdvertisementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementEntityReturnsNewIfNotFound; }
			set { _advertisementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementConfigurationEntity AdvertisementConfigurationEntity
		{
			get	{ return GetSingleAdvertisementConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AdvertisementConfigurationAdvertisementCollection", "AdvertisementConfigurationEntity", _advertisementConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementConfigurationEntity. When set to true, AdvertisementConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementConfigurationEntity
		{
			get	{ return _alwaysFetchAdvertisementConfigurationEntity; }
			set	{ _alwaysFetchAdvertisementConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementConfigurationEntity already has been fetched. Setting this property to false when AdvertisementConfigurationEntity has been fetched
		/// will set AdvertisementConfigurationEntity to null as well. Setting this property to true while AdvertisementConfigurationEntity hasn't been fetched disables lazy loading for AdvertisementConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementConfigurationEntity
		{
			get { return _alreadyFetchedAdvertisementConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementConfigurationEntity && !value)
				{
					this.AdvertisementConfigurationEntity = null;
				}
				_alreadyFetchedAdvertisementConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementConfigurationEntity is not found
		/// in the database. When set to true, AdvertisementConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementConfigurationEntityReturnsNewIfNotFound; }
			set { _advertisementConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
