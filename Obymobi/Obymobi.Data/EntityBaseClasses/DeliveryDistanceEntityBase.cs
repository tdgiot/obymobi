﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'DeliveryDistance'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliveryDistanceEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeliveryDistanceEntity"; }
		}
	
		#region Class Member Declarations
		private ServiceMethodEntity _serviceMethodEntity;
		private bool	_alwaysFetchServiceMethodEntity, _alreadyFetchedServiceMethodEntity, _serviceMethodEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ServiceMethodEntity</summary>
			public static readonly string ServiceMethodEntity = "ServiceMethodEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliveryDistanceEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeliveryDistanceEntityBase() :base("DeliveryDistanceEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		protected DeliveryDistanceEntityBase(System.Int32 deliveryDistanceId):base("DeliveryDistanceEntity")
		{
			InitClassFetch(deliveryDistanceId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeliveryDistanceEntityBase(System.Int32 deliveryDistanceId, IPrefetchPath prefetchPathToUse): base("DeliveryDistanceEntity")
		{
			InitClassFetch(deliveryDistanceId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="validator">The custom validator object for this DeliveryDistanceEntity</param>
		protected DeliveryDistanceEntityBase(System.Int32 deliveryDistanceId, IValidator validator):base("DeliveryDistanceEntity")
		{
			InitClassFetch(deliveryDistanceId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliveryDistanceEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_serviceMethodEntity = (ServiceMethodEntity)info.GetValue("_serviceMethodEntity", typeof(ServiceMethodEntity));
			if(_serviceMethodEntity!=null)
			{
				_serviceMethodEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_serviceMethodEntityReturnsNewIfNotFound = info.GetBoolean("_serviceMethodEntityReturnsNewIfNotFound");
			_alwaysFetchServiceMethodEntity = info.GetBoolean("_alwaysFetchServiceMethodEntity");
			_alreadyFetchedServiceMethodEntity = info.GetBoolean("_alreadyFetchedServiceMethodEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliveryDistanceFieldIndex)fieldIndex)
			{
				case DeliveryDistanceFieldIndex.ServiceMethodId:
					DesetupSyncServiceMethodEntity(true, false);
					_alreadyFetchedServiceMethodEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedServiceMethodEntity = (_serviceMethodEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ServiceMethodEntity":
					toReturn.Add(Relations.ServiceMethodEntityUsingServiceMethodId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_serviceMethodEntity", (!this.MarkedForDeletion?_serviceMethodEntity:null));
			info.AddValue("_serviceMethodEntityReturnsNewIfNotFound", _serviceMethodEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchServiceMethodEntity", _alwaysFetchServiceMethodEntity);
			info.AddValue("_alreadyFetchedServiceMethodEntity", _alreadyFetchedServiceMethodEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ServiceMethodEntity":
					_alreadyFetchedServiceMethodEntity = true;
					this.ServiceMethodEntity = (ServiceMethodEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ServiceMethodEntity":
					SetupSyncServiceMethodEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ServiceMethodEntity":
					DesetupSyncServiceMethodEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_serviceMethodEntity!=null)
			{
				toReturn.Add(_serviceMethodEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliveryDistanceId)
		{
			return FetchUsingPK(deliveryDistanceId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliveryDistanceId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliveryDistanceId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliveryDistanceId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deliveryDistanceId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliveryDistanceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliveryDistanceId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliveryDistanceId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeliveryDistanceRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public ServiceMethodEntity GetSingleServiceMethodEntity()
		{
			return GetSingleServiceMethodEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ServiceMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ServiceMethodEntity' which is related to this entity.</returns>
		public virtual ServiceMethodEntity GetSingleServiceMethodEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedServiceMethodEntity || forceFetch || _alwaysFetchServiceMethodEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ServiceMethodEntityUsingServiceMethodId);
				ServiceMethodEntity newEntity = new ServiceMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ServiceMethodId);
				}
				if(fetchResult)
				{
					newEntity = (ServiceMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_serviceMethodEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ServiceMethodEntity = newEntity;
				_alreadyFetchedServiceMethodEntity = fetchResult;
			}
			return _serviceMethodEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ServiceMethodEntity", _serviceMethodEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="validator">The validator object for this DeliveryDistanceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deliveryDistanceId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deliveryDistanceId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_serviceMethodEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliveryDistanceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaximumInMetres", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _serviceMethodEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncServiceMethodEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticDeliveryDistanceRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, signalRelatedEntity, "DeliveryDistanceCollection", resetFKFields, new int[] { (int)DeliveryDistanceFieldIndex.ServiceMethodId } );		
			_serviceMethodEntity = null;
		}
		
		/// <summary> setups the sync logic for member _serviceMethodEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncServiceMethodEntity(IEntityCore relatedEntity)
		{
			if(_serviceMethodEntity!=relatedEntity)
			{		
				DesetupSyncServiceMethodEntity(true, true);
				_serviceMethodEntity = (ServiceMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _serviceMethodEntity, new PropertyChangedEventHandler( OnServiceMethodEntityPropertyChanged ), "ServiceMethodEntity", Obymobi.Data.RelationClasses.StaticDeliveryDistanceRelations.ServiceMethodEntityUsingServiceMethodIdStatic, true, ref _alreadyFetchedServiceMethodEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnServiceMethodEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliveryDistanceId">PK value for DeliveryDistance which data should be fetched into this DeliveryDistance object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliveryDistanceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeliveryDistanceFieldIndex.DeliveryDistanceId].ForcedCurrentValueWrite(deliveryDistanceId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliveryDistanceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliveryDistanceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliveryDistanceRelations Relations
		{
			get	{ return new DeliveryDistanceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodEntity")[0], (int)Obymobi.Data.EntityType.DeliveryDistanceEntity, (int)Obymobi.Data.EntityType.ServiceMethodEntity, 0, null, null, null, "ServiceMethodEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeliveryDistanceId property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."DeliveryDistanceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliveryDistanceId
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.DeliveryDistanceId, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.DeliveryDistanceId, value, true); }
		}

		/// <summary> The ServiceMethodId property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."ServiceMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ServiceMethodId
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.ServiceMethodId, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.ServiceMethodId, value, true); }
		}

		/// <summary> The MaximumInMetres property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."MaximumInMetres"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaximumInMetres
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.MaximumInMetres, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.MaximumInMetres, value, true); }
		}

		/// <summary> The Price property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."Price"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Price
		{
			get { return (System.Decimal)GetValue((int)DeliveryDistanceFieldIndex.Price, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.Price, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryDistanceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliveryDistanceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity DeliveryDistance<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliveryDistance"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliveryDistanceFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliveryDistanceFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ServiceMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleServiceMethodEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ServiceMethodEntity ServiceMethodEntity
		{
			get	{ return GetSingleServiceMethodEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncServiceMethodEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliveryDistanceCollection", "ServiceMethodEntity", _serviceMethodEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodEntity. When set to true, ServiceMethodEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodEntity is accessed. You can always execute a forced fetch by calling GetSingleServiceMethodEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodEntity
		{
			get	{ return _alwaysFetchServiceMethodEntity; }
			set	{ _alwaysFetchServiceMethodEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodEntity already has been fetched. Setting this property to false when ServiceMethodEntity has been fetched
		/// will set ServiceMethodEntity to null as well. Setting this property to true while ServiceMethodEntity hasn't been fetched disables lazy loading for ServiceMethodEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodEntity
		{
			get { return _alreadyFetchedServiceMethodEntity;}
			set 
			{
				if(_alreadyFetchedServiceMethodEntity && !value)
				{
					this.ServiceMethodEntity = null;
				}
				_alreadyFetchedServiceMethodEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ServiceMethodEntity is not found
		/// in the database. When set to true, ServiceMethodEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ServiceMethodEntityReturnsNewIfNotFound
		{
			get	{ return _serviceMethodEntityReturnsNewIfNotFound; }
			set { _serviceMethodEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliveryDistanceEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
