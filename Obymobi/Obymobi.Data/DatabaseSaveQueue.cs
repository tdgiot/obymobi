﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Obymobi.Data
{
    public class DatabaseSaveQueue
    {
        #region Fields
        
        private static object databaseLock = new object();
        private static DatabaseSaveQueue databaseSaveQueue;

        private readonly BlockingCollection<CommonEntityBase> queue = new BlockingCollection<CommonEntityBase>();

        #endregion

        #region Ctor

        private DatabaseSaveQueue()
        {
            Task.Factory.StartNew(() => this.ProcessWork(), TaskCreationOptions.LongRunning);
        }

        private static DatabaseSaveQueue Instance
        {
            get
            {
                lock (DatabaseSaveQueue.databaseLock)
                {
                    if (DatabaseSaveQueue.databaseSaveQueue == null)
                    {
                        DatabaseSaveQueue.databaseSaveQueue = new DatabaseSaveQueue();
                    }
                }

                return DatabaseSaveQueue.databaseSaveQueue;
            }
        }

        #endregion

        #region Public Static Wrapper Methods

        public static void Enqueue(CommonEntityBase entity)
        {
            DatabaseSaveQueue.Instance.EnqueueInternal(entity);
        }

        #endregion

        #region Processing

        private void EnqueueInternal(CommonEntityBase entity)
        {
            this.queue.TryAdd(entity);
        }

        private void ProcessWork()
        {
            foreach (CommonEntityBase entity in this.queue.GetConsumingEnumerable())
            {
                try
                {
                    if (entity.IsQueued)
                    {
                        entity.Save(entity.UpdateRestriction, entity.Recurse);
                    }
                }
                catch (Exception ex)
                {
                    // If we get an exception here it's fine because it has already been caught and logged in CommonEntityBase.Save()
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
        }

        #endregion
    }
}
