﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Data
{
    public interface INullableBrandRelatedChildEntity
    {
        int? ParentBrandId { get; }
        CommonEntityBase BrandParent { get; }
        BrandEntity BrandParentBrandEntity { get; }
    }
}