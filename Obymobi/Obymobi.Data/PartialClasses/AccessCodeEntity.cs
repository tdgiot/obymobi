﻿using Dionysos.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Data.EntityClasses
{
    public partial class AccessCodeEntity
    {
        /// <summary>
        /// Gets the text representation of the access code type
        /// </summary>
        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)AccessCodeFieldIndex.Type)]
        public string TypeText
        {
            get
            {
                return this.Type.ToString();
            }
        }

        public delegate void AfterDeleteDelegate(AccessCodeEntity entity);
        public event AfterDeleteDelegate AfterDelete;

        protected override void OnAuditDeleteOfEntity()
        {
            if (AfterDelete != null)
                this.AfterDelete(this);

            base.OnAuditDeleteOfEntity();
        }
    }
}
