﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Data.EntityClasses
{
    public partial class PublishingItemEntity
    {
        public string GetParameterString()
        {
            string parameters = string.Empty;

            if (!this.Parameter1Name.IsNullOrWhiteSpace() && !this.Parameter1Value.IsNullOrWhiteSpace())
                parameters += $"{this.Parameter1Name}-{this.Parameter1Value}";

            if (!this.Parameter2Name.IsNullOrWhiteSpace() && !this.Parameter2Value.IsNullOrWhiteSpace())
                parameters += $"_{this.Parameter2Name}-{this.Parameter2Value}";

            return parameters;
        }
    }
}
