﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Media Entity. It will perform database oriented actions for a entity of type 'MediaEntity'.</summary>
	public partial class MediaDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public MediaDAO() : base(InheritanceHierarchyType.None, "MediaEntity", new MediaEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MediaEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCategoryCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity categoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnNoCategory, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(categoryInstance.Fields[(int)CategoryFieldIndex.CategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCategoryCollectionViaAnnouncement_(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity categoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnYesCategory, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(categoryInstance.Fields[(int)CategoryFieldIndex.CategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCategoryCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity categoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(categoryInstance.Fields[(int)CategoryFieldIndex.CategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCategoryCollectionViaMessageTemplate(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity categoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageTemplateEntityUsingMediaId, "MessageTemplate_");
			relations.Add(MessageTemplateEntity.Relations.CategoryEntityUsingCategoryId, "MessageTemplate_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(categoryInstance.Fields[(int)CategoryFieldIndex.CategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in ClientEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingClientCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity clientInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.ClientEntityUsingClientId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(clientInstance.Fields[(int)ClientFieldIndex.ClientId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CompanyEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCompanyCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity companyInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.CompanyEntityUsingCompanyId, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(companyInstance.Fields[(int)CompanyFieldIndex.CompanyId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CompanyEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCompanyCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity companyInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.CompanyEntityUsingCompanyId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(companyInstance.Fields[(int)CompanyFieldIndex.CompanyId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CompanyEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCompanyCollectionViaMessageTemplate(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity companyInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageTemplateEntityUsingMediaId, "MessageTemplate_");
			relations.Add(MessageTemplateEntity.Relations.CompanyEntityUsingCompanyId, "MessageTemplate_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(companyInstance.Fields[(int)CompanyFieldIndex.CompanyId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in CustomerEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCustomerCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity customerInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.CustomerEntityUsingCustomerId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(customerInstance.Fields[(int)CustomerFieldIndex.CustomerId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in DeliverypointEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingDeliverypointCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(deliverypointInstance.Fields[(int)DeliverypointFieldIndex.DeliverypointId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in DeliverypointgroupEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingDeliverypointgroupCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(deliverypointgroupInstance.Fields[(int)DeliverypointgroupFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in EntertainmentEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingEntertainmentCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entertainmentInstance.Fields[(int)EntertainmentFieldIndex.EntertainmentId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in EntertainmentEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingEntertainmentCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entertainmentInstance.Fields[(int)EntertainmentFieldIndex.EntertainmentId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in EntertainmentEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingEntertainmentCollectionViaMessageTemplate(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageTemplateEntityUsingMediaId, "MessageTemplate_");
			relations.Add(MessageTemplateEntity.Relations.EntertainmentEntityUsingEntertainmentId, "MessageTemplate_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entertainmentInstance.Fields[(int)EntertainmentFieldIndex.EntertainmentId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in EntertainmentcategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entertainmentcategoryInstance.Fields[(int)EntertainmentcategoryFieldIndex.EntertainmentcategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in EntertainmentcategoryEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement_(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entertainmentcategoryInstance.Fields[(int)EntertainmentcategoryFieldIndex.EntertainmentcategoryId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in OrderEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingOrderCollectionViaMessage(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity orderInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageEntityUsingMediaId, "Message_");
			relations.Add(MessageEntity.Relations.OrderEntityUsingOrderId, "Message_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(orderInstance.Fields[(int)OrderFieldIndex.OrderId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in ProductEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingProductCollectionViaAnnouncement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity productInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, "Announcement_");
			relations.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(productInstance.Fields[(int)ProductFieldIndex.ProductId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling MediaCollection object all MediaEntity objects which are related via a relation of type 'm:n' with the passed in ProductEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingProductCollectionViaMessageTemplate(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity productInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(MediaEntity.Relations.MessageTemplateEntityUsingMediaId, "MessageTemplate_");
			relations.Add(MessageTemplateEntity.Relations.ProductEntityUsingProductId, "MessageTemplate_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(productInstance.Fields[(int)ProductFieldIndex.ProductId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Deletes from the persistent storage all 'Media' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MediaEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MediaEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(advertisementEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.AdvertisementId], ComparisonOperator.Equal, ((AdvertisementEntity)advertisementEntityInstance).AdvertisementId));
			}
			if(alterationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.AlterationId], ComparisonOperator.Equal, ((AlterationEntity)alterationEntityInstance).AlterationId));
			}
			if(alterationoptionEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.AlterationoptionId], ComparisonOperator.Equal, ((AlterationoptionEntity)alterationoptionEntityInstance).AlterationoptionId));
			}
			if(applicationConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ApplicationConfigurationId], ComparisonOperator.Equal, ((ApplicationConfigurationEntity)applicationConfigurationEntityInstance).ApplicationConfigurationId));
			}
			if(carouselItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.CarouselItemId], ComparisonOperator.Equal, ((CarouselItemEntity)carouselItemEntityInstance).CarouselItemId));
			}
			if(landingPageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.LandingPageId], ComparisonOperator.Equal, ((LandingPageEntity)landingPageEntityInstance).LandingPageId));
			}
			if(widgetActionBannerEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.WidgetId], ComparisonOperator.Equal, ((WidgetActionBannerEntity)widgetActionBannerEntityInstance).WidgetId));
			}
			if(widgetHeroEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.WidgetId], ComparisonOperator.Equal, ((WidgetHeroEntity)widgetHeroEntityInstance).WidgetId));
			}
			if(attachmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.AttachmentId], ComparisonOperator.Equal, ((AttachmentEntity)attachmentEntityInstance).AttachmentId));
			}
			if(actionCategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionCategoryId], ComparisonOperator.Equal, ((CategoryEntity)actionCategoryEntityInstance).CategoryId));
			}
			if(categoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.CategoryId], ComparisonOperator.Equal, ((CategoryEntity)categoryEntityInstance).CategoryId));
			}
			if(clientConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ClientConfigurationId], ComparisonOperator.Equal, ((ClientConfigurationEntity)clientConfigurationEntityInstance).ClientConfigurationId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(deliverypointgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal, ((DeliverypointgroupEntity)deliverypointgroupEntityInstance).DeliverypointgroupId));
			}
			if(actionEntertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionEntertainmentId], ComparisonOperator.Equal, ((EntertainmentEntity)actionEntertainmentEntityInstance).EntertainmentId));
			}
			if(entertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.EntertainmentId], ComparisonOperator.Equal, ((EntertainmentEntity)entertainmentEntityInstance).EntertainmentId));
			}
			if(actionEntertainmentcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionEntertainmentcategoryId], ComparisonOperator.Equal, ((EntertainmentcategoryEntity)actionEntertainmentcategoryEntityInstance).EntertainmentcategoryId));
			}
			if(genericcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.GenericcategoryId], ComparisonOperator.Equal, ((GenericcategoryEntity)genericcategoryEntityInstance).GenericcategoryId));
			}
			if(genericproductEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.GenericproductId], ComparisonOperator.Equal, ((GenericproductEntity)genericproductEntityInstance).GenericproductId));
			}
			if(mediaEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.AgnosticMediaId], ComparisonOperator.Equal, ((MediaEntity)mediaEntityInstance).MediaId));
			}
			if(pageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.PageId], ComparisonOperator.Equal, ((PageEntity)pageEntityInstance).PageId));
			}
			if(pageEntity_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionPageId], ComparisonOperator.Equal, ((PageEntity)pageEntity_Instance).PageId));
			}
			if(pageElementEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.PageElementId], ComparisonOperator.Equal, ((PageElementEntity)pageElementEntityInstance).PageElementId));
			}
			if(pageTemplateEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.PageTemplateId], ComparisonOperator.Equal, ((PageTemplateEntity)pageTemplateEntityInstance).PageTemplateId));
			}
			if(pageTemplateElementEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.PageTemplateElementId], ComparisonOperator.Equal, ((PageTemplateElementEntity)pageTemplateElementEntityInstance).PageTemplateElementId));
			}
			if(pointOfInterestEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.PointOfInterestId], ComparisonOperator.Equal, ((PointOfInterestEntity)pointOfInterestEntityInstance).PointOfInterestId));
			}
			if(actionProductEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionProductId], ComparisonOperator.Equal, ((ProductEntity)actionProductEntityInstance).ProductId));
			}
			if(productEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ProductId], ComparisonOperator.Equal, ((ProductEntity)productEntityInstance).ProductId));
			}
			if(productgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ProductgroupId], ComparisonOperator.Equal, ((ProductgroupEntity)productgroupEntityInstance).ProductgroupId));
			}
			if(roomControlSectionEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.RoomControlSectionId], ComparisonOperator.Equal, ((RoomControlSectionEntity)roomControlSectionEntityInstance).RoomControlSectionId));
			}
			if(roomControlSectionItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.RoomControlSectionItemId], ComparisonOperator.Equal, ((RoomControlSectionItemEntity)roomControlSectionItemEntityInstance).RoomControlSectionItemId));
			}
			if(routestephandlerEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.RoutestephandlerId], ComparisonOperator.Equal, ((RoutestephandlerEntity)routestephandlerEntityInstance).RoutestephandlerId));
			}
			if(siteEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.SiteId], ComparisonOperator.Equal, ((SiteEntity)siteEntityInstance).SiteId));
			}
			if(siteEntity_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.ActionSiteId], ComparisonOperator.Equal, ((SiteEntity)siteEntity_Instance).SiteId));
			}
			if(stationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.StationId], ComparisonOperator.Equal, ((StationEntity)stationEntityInstance).StationId));
			}
			if(surveyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.SurveyId], ComparisonOperator.Equal, ((SurveyEntity)surveyEntityInstance).SurveyId));
			}
			if(surveyPageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.SurveyPageId], ComparisonOperator.Equal, ((SurveyPageEntity)surveyPageEntityInstance).SurveyPageId));
			}
			if(uIFooterItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.UIFooterItemId], ComparisonOperator.Equal, ((UIFooterItemEntity)uIFooterItemEntityInstance).UIFooterItemId));
			}
			if(uIThemeEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.UIThemeId], ComparisonOperator.Equal, ((UIThemeEntity)uIThemeEntityInstance).UIThemeId));
			}
			if(uIWidgetEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MediaFieldIndex.UIWidgetId], ComparisonOperator.Equal, ((UIWidgetEntity)uIWidgetEntityInstance).UIWidgetId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
