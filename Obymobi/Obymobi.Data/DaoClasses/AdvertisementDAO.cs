﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Advertisement Entity. It will perform database oriented actions for a entity of type 'AdvertisementEntity'.</summary>
	public partial class AdvertisementDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public AdvertisementDAO() : base(InheritanceHierarchyType.None, "AdvertisementEntity", new AdvertisementEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling AdvertisementCollection object all AdvertisementEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the AdvertisementEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity actionCategoryEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity pageEntityInstance, IEntity productEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, IEntity supplierEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AdvertisementEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(actionCategoryEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericproductEntityInstance, pageEntityInstance, productEntityInstance, productCategoryEntityInstance, siteEntityInstance, supplierEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling AdvertisementCollection object all AdvertisementEntity objects which are related via a relation of type 'm:n' with the passed in AlterationoptionEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingAlterationoptionCollectionViaMedium(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity alterationoptionInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AdvertisementEntity.Relations.MediaEntityUsingAdvertisementId, "Media_");
			relations.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(alterationoptionInstance.Fields[(int)AlterationoptionFieldIndex.AlterationoptionId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling AdvertisementCollection object all AdvertisementEntity objects which are related via a relation of type 'm:n' with the passed in DeliverypointgroupEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingDeliverypointgroupCollectionViaDeliverypointgroupAdvertisement(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AdvertisementEntity.Relations.DeliverypointgroupAdvertisementEntityUsingAdvertisementId, "DeliverypointgroupAdvertisement_");
			relations.Add(DeliverypointgroupAdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "DeliverypointgroupAdvertisement_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(deliverypointgroupInstance.Fields[(int)DeliverypointgroupFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}



		/// <summary>Retrieves in the calling AdvertisementCollection object all AdvertisementEntity objects which are related via a relation of type 'm:n' with the passed in PointOfInterestEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingPointOfInterestCollectionViaMedium(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity pointOfInterestInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AdvertisementEntity.Relations.MediaEntityUsingAdvertisementId, "Media_");
			relations.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(pointOfInterestInstance.Fields[(int)PointOfInterestFieldIndex.PointOfInterestId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling AdvertisementCollection object all AdvertisementEntity objects which are related via a relation of type 'm:n' with the passed in SurveyPageEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingSurveyPageCollectionViaMedium(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity surveyPageInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AdvertisementEntity.Relations.MediaEntityUsingAdvertisementId, "Media_");
			relations.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(surveyPageInstance.Fields[(int)SurveyPageFieldIndex.SurveyPageId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Deletes from the persistent storage all 'Advertisement' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the AdvertisementEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity actionCategoryEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity pageEntityInstance, IEntity productEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, IEntity supplierEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AdvertisementEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(actionCategoryEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericproductEntityInstance, pageEntityInstance, productEntityInstance, productCategoryEntityInstance, siteEntityInstance, supplierEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the AdvertisementEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity actionCategoryEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity pageEntityInstance, IEntity productEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, IEntity supplierEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AdvertisementEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(actionCategoryEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericproductEntityInstance, pageEntityInstance, productEntityInstance, productCategoryEntityInstance, siteEntityInstance, supplierEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the AdvertisementEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity actionCategoryEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity pageEntityInstance, IEntity productEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, IEntity supplierEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(actionCategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ActionCategoryId], ComparisonOperator.Equal, ((CategoryEntity)actionCategoryEntityInstance).CategoryId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(deliverypointgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal, ((DeliverypointgroupEntity)deliverypointgroupEntityInstance).DeliverypointgroupId));
			}
			if(actionEntertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ActionEntertainmentId], ComparisonOperator.Equal, ((EntertainmentEntity)actionEntertainmentEntityInstance).EntertainmentId));
			}
			if(entertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.EntertainmentId], ComparisonOperator.Equal, ((EntertainmentEntity)entertainmentEntityInstance).EntertainmentId));
			}
			if(actionEntertainmentcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ActionEntertainmentCategoryId], ComparisonOperator.Equal, ((EntertainmentcategoryEntity)actionEntertainmentcategoryEntityInstance).EntertainmentcategoryId));
			}
			if(genericproductEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.GenericproductId], ComparisonOperator.Equal, ((GenericproductEntity)genericproductEntityInstance).GenericproductId));
			}
			if(pageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ActionPageId], ComparisonOperator.Equal, ((PageEntity)pageEntityInstance).PageId));
			}
			if(productEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ProductId], ComparisonOperator.Equal, ((ProductEntity)productEntityInstance).ProductId));
			}
			if(productCategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ProductCategoryId], ComparisonOperator.Equal, ((ProductCategoryEntity)productCategoryEntityInstance).ProductCategoryId));
			}
			if(siteEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.ActionSiteId], ComparisonOperator.Equal, ((SiteEntity)siteEntityInstance).SiteId));
			}
			if(supplierEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AdvertisementFieldIndex.SupplierId], ComparisonOperator.Equal, ((SupplierEntity)supplierEntityInstance).SupplierId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
