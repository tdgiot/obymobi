﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the ClientConfiguration Entity. It will perform database oriented actions for a entity of type 'ClientConfigurationEntity'.</summary>
	public partial class ClientConfigurationDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public ClientConfigurationDAO() : base(InheritanceHierarchyType.None, "ClientConfigurationEntity", new ClientConfigurationEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling ClientConfigurationCollection object all ClientConfigurationEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="advertisementConfigurationEntityInstance">AdvertisementConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="entertainmentConfigurationEntityInstance">EntertainmentConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="roomControlConfigurationEntityInstance">RoomControlConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="wifiConfigurationEntityInstance">WifiConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity advertisementConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentConfigurationEntityInstance, IEntity menuEntityInstance, IEntity priceScheduleEntityInstance, IEntity roomControlConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, IEntity wifiConfigurationEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.ClientConfigurationEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(advertisementConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentConfigurationEntityInstance, menuEntityInstance, priceScheduleEntityInstance, roomControlConfigurationEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, wifiConfigurationEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'ClientConfiguration' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="advertisementConfigurationEntityInstance">AdvertisementConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="entertainmentConfigurationEntityInstance">EntertainmentConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="roomControlConfigurationEntityInstance">RoomControlConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <param name="wifiConfigurationEntityInstance">WifiConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity advertisementConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentConfigurationEntityInstance, IEntity menuEntityInstance, IEntity priceScheduleEntityInstance, IEntity roomControlConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, IEntity wifiConfigurationEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.ClientConfigurationEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(advertisementConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentConfigurationEntityInstance, menuEntityInstance, priceScheduleEntityInstance, roomControlConfigurationEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, wifiConfigurationEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="advertisementConfigurationEntityInstance">AdvertisementConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="entertainmentConfigurationEntityInstance">EntertainmentConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="roomControlConfigurationEntityInstance">RoomControlConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <param name="wifiConfigurationEntityInstance">WifiConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity advertisementConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentConfigurationEntityInstance, IEntity menuEntityInstance, IEntity priceScheduleEntityInstance, IEntity roomControlConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, IEntity wifiConfigurationEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.ClientConfigurationEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(advertisementConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentConfigurationEntityInstance, menuEntityInstance, priceScheduleEntityInstance, roomControlConfigurationEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, wifiConfigurationEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="advertisementConfigurationEntityInstance">AdvertisementConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="entertainmentConfigurationEntityInstance">EntertainmentConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="roomControlConfigurationEntityInstance">RoomControlConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="wifiConfigurationEntityInstance">WifiConfigurationEntity instance to use as a filter for the ClientConfigurationEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity advertisementConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentConfigurationEntityInstance, IEntity menuEntityInstance, IEntity priceScheduleEntityInstance, IEntity roomControlConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, IEntity wifiConfigurationEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(advertisementConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.AdvertisementConfigurationId], ComparisonOperator.Equal, ((AdvertisementConfigurationEntity)advertisementConfigurationEntityInstance).AdvertisementConfigurationId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(deliverypointgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal, ((DeliverypointgroupEntity)deliverypointgroupEntityInstance).DeliverypointgroupId));
			}
			if(entertainmentConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.EntertainmentConfigurationId], ComparisonOperator.Equal, ((EntertainmentConfigurationEntity)entertainmentConfigurationEntityInstance).EntertainmentConfigurationId));
			}
			if(menuEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.MenuId], ComparisonOperator.Equal, ((MenuEntity)menuEntityInstance).MenuId));
			}
			if(priceScheduleEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.PriceScheduleId], ComparisonOperator.Equal, ((PriceScheduleEntity)priceScheduleEntityInstance).PriceScheduleId));
			}
			if(roomControlConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.RoomControlConfigurationId], ComparisonOperator.Equal, ((RoomControlConfigurationEntity)roomControlConfigurationEntityInstance).RoomControlConfigurationId));
			}
			if(uIModeEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.UIModeId], ComparisonOperator.Equal, ((UIModeEntity)uIModeEntityInstance).UIModeId));
			}
			if(uIScheduleEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.UIScheduleId], ComparisonOperator.Equal, ((UIScheduleEntity)uIScheduleEntityInstance).UIScheduleId));
			}
			if(uIThemeEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.UIThemeId], ComparisonOperator.Equal, ((UIThemeEntity)uIThemeEntityInstance).UIThemeId));
			}
			if(wifiConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)ClientConfigurationFieldIndex.WifiConfigurationId], ComparisonOperator.Equal, ((WifiConfigurationEntity)wifiConfigurationEntityInstance).WifiConfigurationId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
