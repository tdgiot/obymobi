﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the RoomControlWidget Entity. It will perform database oriented actions for a entity of type 'RoomControlWidgetEntity'.</summary>
	public partial class RoomControlWidgetDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public RoomControlWidgetDAO() : base(InheritanceHierarchyType.None, "RoomControlWidgetEntity", new RoomControlWidgetEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling RoomControlWidgetCollection object all RoomControlWidgetEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.RoomControlWidgetEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'RoomControlWidget' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.RoomControlWidgetEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.RoomControlWidgetEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(infraredConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.InfraredConfigurationId], ComparisonOperator.Equal, ((InfraredConfigurationEntity)infraredConfigurationEntityInstance).InfraredConfigurationId));
			}
			if(roomControlComponentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId1], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntityInstance).RoomControlComponentId));
			}
			if(roomControlComponentEntity_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId10], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity_Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity__Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId2], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity__Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity___Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId3], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity___Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity____Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId4], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity____Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity_____Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId5], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity_____Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity______Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId6], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity______Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity_______Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId7], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity_______Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity________Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId8], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity________Instance).RoomControlComponentId));
			}
			if(roomControlComponentEntity_________Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlComponentId9], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntity_________Instance).RoomControlComponentId));
			}
			if(roomControlSectionEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlSectionId], ComparisonOperator.Equal, ((RoomControlSectionEntity)roomControlSectionEntityInstance).RoomControlSectionId));
			}
			if(roomControlSectionItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)RoomControlWidgetFieldIndex.RoomControlSectionItemId], ComparisonOperator.Equal, ((RoomControlSectionItemEntity)roomControlSectionItemEntityInstance).RoomControlSectionItemId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
