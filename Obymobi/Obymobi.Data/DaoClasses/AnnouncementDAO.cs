﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Announcement Entity. It will perform database oriented actions for a entity of type 'AnnouncementEntity'.</summary>
	public partial class AnnouncementDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public AnnouncementDAO() : base(InheritanceHierarchyType.None, "AnnouncementEntity", new AnnouncementEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="categoryEntity_Instance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="entertainmentcategoryEntity_Instance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AnnouncementEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity categoryEntityInstance, IEntity categoryEntity_Instance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity entertainmentcategoryEntity_Instance, IEntity mediaEntityInstance, IEntity productEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AnnouncementEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, categoryEntity_Instance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, entertainmentcategoryEntity_Instance, mediaEntityInstance, productEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in CompanyEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCompanyCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity companyInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(companyInstance.Fields[(int)CompanyFieldIndex.CompanyId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in MenuEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingMenuCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity menuInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(menuInstance.Fields[(int)MenuFieldIndex.MenuId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in PosdeliverypointgroupEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity posdeliverypointgroupInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(posdeliverypointgroupInstance.Fields[(int)PosdeliverypointgroupFieldIndex.PosdeliverypointgroupId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in RouteEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingRouteCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity routeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(routeInstance.Fields[(int)RouteFieldIndex.RouteId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in RouteEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingRouteCollectionViaDeliverypointgroup_(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity routeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(routeInstance.Fields[(int)RouteFieldIndex.RouteId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in TerminalEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingTerminalCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity terminalInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(terminalInstance.Fields[(int)TerminalFieldIndex.TerminalId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in UIModeEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingUIModeCollectionViaDeliverypointgroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity uIModeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(uIModeInstance.Fields[(int)UIModeFieldIndex.UIModeId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in UIModeEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingUIModeCollectionViaDeliverypointgroup_(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity uIModeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(uIModeInstance.Fields[(int)UIModeFieldIndex.UIModeId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling AnnouncementCollection object all AnnouncementEntity objects which are related via a relation of type 'm:n' with the passed in UIModeEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingUIModeCollectionViaDeliverypointgroup__(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity uIModeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_");
			relations.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(uIModeInstance.Fields[(int)UIModeFieldIndex.UIModeId], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Deletes from the persistent storage all 'Announcement' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="categoryEntity_Instance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="entertainmentcategoryEntity_Instance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AnnouncementEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity categoryEntityInstance, IEntity categoryEntity_Instance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity entertainmentcategoryEntity_Instance, IEntity mediaEntityInstance, IEntity productEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AnnouncementEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, categoryEntity_Instance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, entertainmentcategoryEntity_Instance, mediaEntityInstance, productEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="categoryEntity_Instance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="entertainmentcategoryEntity_Instance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AnnouncementEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity categoryEntityInstance, IEntity categoryEntity_Instance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity entertainmentcategoryEntity_Instance, IEntity mediaEntityInstance, IEntity productEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.AnnouncementEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, categoryEntity_Instance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, entertainmentcategoryEntity_Instance, mediaEntityInstance, productEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="categoryEntity_Instance">CategoryEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="entertainmentcategoryEntity_Instance">EntertainmentcategoryEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the AnnouncementEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity categoryEntityInstance, IEntity categoryEntity_Instance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity entertainmentcategoryEntity_Instance, IEntity mediaEntityInstance, IEntity productEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(categoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnNoCategory], ComparisonOperator.Equal, ((CategoryEntity)categoryEntityInstance).CategoryId));
			}
			if(categoryEntity_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnYesCategory], ComparisonOperator.Equal, ((CategoryEntity)categoryEntity_Instance).CategoryId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(deliverypointgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal, ((DeliverypointgroupEntity)deliverypointgroupEntityInstance).DeliverypointgroupId));
			}
			if(entertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnYesEntertainment], ComparisonOperator.Equal, ((EntertainmentEntity)entertainmentEntityInstance).EntertainmentId));
			}
			if(entertainmentcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnYesEntertainmentCategory], ComparisonOperator.Equal, ((EntertainmentcategoryEntity)entertainmentcategoryEntityInstance).EntertainmentcategoryId));
			}
			if(entertainmentcategoryEntity_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnNoEntertainmentCategory], ComparisonOperator.Equal, ((EntertainmentcategoryEntity)entertainmentcategoryEntity_Instance).EntertainmentcategoryId));
			}
			if(mediaEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.MediaId], ComparisonOperator.Equal, ((MediaEntity)mediaEntityInstance).MediaId));
			}
			if(productEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)AnnouncementFieldIndex.OnYesProduct], ComparisonOperator.Equal, ((ProductEntity)productEntityInstance).ProductId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
