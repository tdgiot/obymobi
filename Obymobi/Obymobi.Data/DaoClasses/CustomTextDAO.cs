﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the CustomText Entity. It will perform database oriented actions for a entity of type 'CustomTextEntity'.</summary>
	public partial class CustomTextDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public CustomTextDAO() : base(InheritanceHierarchyType.None, "CustomTextEntity", new CustomTextEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling CustomTextCollection object all CustomTextEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.CustomTextEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'CustomText' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.CustomTextEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.CustomTextEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(actionButtonEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ActionButtonId], ComparisonOperator.Equal, ((ActionButtonEntity)actionButtonEntityInstance).ActionButtonId));
			}
			if(advertisementEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AdvertisementId], ComparisonOperator.Equal, ((AdvertisementEntity)advertisementEntityInstance).AdvertisementId));
			}
			if(alterationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AlterationId], ComparisonOperator.Equal, ((AlterationEntity)alterationEntityInstance).AlterationId));
			}
			if(alterationoptionEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AlterationoptionId], ComparisonOperator.Equal, ((AlterationoptionEntity)alterationoptionEntityInstance).AlterationoptionId));
			}
			if(amenityEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AmenityId], ComparisonOperator.Equal, ((AmenityEntity)amenityEntityInstance).AmenityId));
			}
			if(announcementEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AnnouncementId], ComparisonOperator.Equal, ((AnnouncementEntity)announcementEntityInstance).AnnouncementId));
			}
			if(applicationConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ApplicationConfigurationId], ComparisonOperator.Equal, ((ApplicationConfigurationEntity)applicationConfigurationEntityInstance).ApplicationConfigurationId));
			}
			if(carouselItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.CarouselItemId], ComparisonOperator.Equal, ((CarouselItemEntity)carouselItemEntityInstance).CarouselItemId));
			}
			if(landingPageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.LandingPageId], ComparisonOperator.Equal, ((LandingPageEntity)landingPageEntityInstance).LandingPageId));
			}
			if(navigationMenuItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.NavigationMenuItemId], ComparisonOperator.Equal, ((NavigationMenuItemEntity)navigationMenuItemEntityInstance).NavigationMenuItemId));
			}
			if(widgetEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.WidgetId], ComparisonOperator.Equal, ((WidgetEntity)widgetEntityInstance).WidgetId));
			}
			if(attachmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AttachmentId], ComparisonOperator.Equal, ((AttachmentEntity)attachmentEntityInstance).AttachmentId));
			}
			if(availabilityEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.AvailabilityId], ComparisonOperator.Equal, ((AvailabilityEntity)availabilityEntityInstance).AvailabilityId));
			}
			if(categoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.CategoryId], ComparisonOperator.Equal, ((CategoryEntity)categoryEntityInstance).CategoryId));
			}
			if(checkoutMethodEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.CheckoutMethodId], ComparisonOperator.Equal, ((CheckoutMethodEntity)checkoutMethodEntityInstance).CheckoutMethodId));
			}
			if(clientConfigurationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ClientConfigurationId], ComparisonOperator.Equal, ((ClientConfigurationEntity)clientConfigurationEntityInstance).ClientConfigurationId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(deliverypointgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.DeliverypointgroupId], ComparisonOperator.Equal, ((DeliverypointgroupEntity)deliverypointgroupEntityInstance).DeliverypointgroupId));
			}
			if(entertainmentcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.EntertainmentcategoryId], ComparisonOperator.Equal, ((EntertainmentcategoryEntity)entertainmentcategoryEntityInstance).EntertainmentcategoryId));
			}
			if(genericcategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.GenericcategoryId], ComparisonOperator.Equal, ((GenericcategoryEntity)genericcategoryEntityInstance).GenericcategoryId));
			}
			if(genericproductEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.GenericproductId], ComparisonOperator.Equal, ((GenericproductEntity)genericproductEntityInstance).GenericproductId));
			}
			if(infraredCommandEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.InfraredCommandId], ComparisonOperator.Equal, ((InfraredCommandEntity)infraredCommandEntityInstance).InfraredCommandId));
			}
			if(languageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.LanguageId], ComparisonOperator.Equal, ((LanguageEntity)languageEntityInstance).LanguageId));
			}
			if(outletEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.OutletId], ComparisonOperator.Equal, ((OutletEntity)outletEntityInstance).OutletId));
			}
			if(pageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.PageId], ComparisonOperator.Equal, ((PageEntity)pageEntityInstance).PageId));
			}
			if(pageTemplateEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.PageTemplateId], ComparisonOperator.Equal, ((PageTemplateEntity)pageTemplateEntityInstance).PageTemplateId));
			}
			if(pointOfInterestEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.PointOfInterestId], ComparisonOperator.Equal, ((PointOfInterestEntity)pointOfInterestEntityInstance).PointOfInterestId));
			}
			if(productEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ProductId], ComparisonOperator.Equal, ((ProductEntity)productEntityInstance).ProductId));
			}
			if(productgroupEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ProductgroupId], ComparisonOperator.Equal, ((ProductgroupEntity)productgroupEntityInstance).ProductgroupId));
			}
			if(roomControlAreaEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoomControlAreaId], ComparisonOperator.Equal, ((RoomControlAreaEntity)roomControlAreaEntityInstance).RoomControlAreaId));
			}
			if(roomControlComponentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoomControlComponentId], ComparisonOperator.Equal, ((RoomControlComponentEntity)roomControlComponentEntityInstance).RoomControlComponentId));
			}
			if(roomControlSectionEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoomControlSectionId], ComparisonOperator.Equal, ((RoomControlSectionEntity)roomControlSectionEntityInstance).RoomControlSectionId));
			}
			if(roomControlSectionItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoomControlSectionItemId], ComparisonOperator.Equal, ((RoomControlSectionItemEntity)roomControlSectionItemEntityInstance).RoomControlSectionItemId));
			}
			if(roomControlWidgetEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoomControlWidgetId], ComparisonOperator.Equal, ((RoomControlWidgetEntity)roomControlWidgetEntityInstance).RoomControlWidgetId));
			}
			if(routestephandlerEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.RoutestephandlerId], ComparisonOperator.Equal, ((RoutestephandlerEntity)routestephandlerEntityInstance).RoutestephandlerId));
			}
			if(scheduledMessageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ScheduledMessageId], ComparisonOperator.Equal, ((ScheduledMessageEntity)scheduledMessageEntityInstance).ScheduledMessageId));
			}
			if(serviceMethodEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.ServiceMethodId], ComparisonOperator.Equal, ((ServiceMethodEntity)serviceMethodEntityInstance).ServiceMethodId));
			}
			if(siteEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.SiteId], ComparisonOperator.Equal, ((SiteEntity)siteEntityInstance).SiteId));
			}
			if(siteTemplateEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.SiteTemplateId], ComparisonOperator.Equal, ((SiteTemplateEntity)siteTemplateEntityInstance).SiteTemplateId));
			}
			if(stationEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.StationId], ComparisonOperator.Equal, ((StationEntity)stationEntityInstance).StationId));
			}
			if(uIFooterItemEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.UIFooterItemId], ComparisonOperator.Equal, ((UIFooterItemEntity)uIFooterItemEntityInstance).UIFooterItemId));
			}
			if(uITabEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.UITabId], ComparisonOperator.Equal, ((UITabEntity)uITabEntityInstance).UITabId));
			}
			if(uIWidgetEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.UIWidgetId], ComparisonOperator.Equal, ((UIWidgetEntity)uIWidgetEntityInstance).UIWidgetId));
			}
			if(venueCategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)CustomTextFieldIndex.VenueCategoryId], ComparisonOperator.Equal, ((VenueCategoryEntity)venueCategoryEntityInstance).VenueCategoryId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
