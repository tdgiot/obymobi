﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Message Entity. It will perform database oriented actions for a entity of type 'MessageEntity'.</summary>
	public partial class MessageDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public MessageDAO() : base(InheritanceHierarchyType.None, "MessageEntity", new MessageEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling MessageCollection object all MessageEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MessageEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity categoryEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity customerEntityInstance, IEntity deliverypointEntityInstance, IEntity entertainmentEntityInstance, IEntity mediaEntityInstance, IEntity orderEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MessageEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, clientEntityInstance, companyEntityInstance, customerEntityInstance, deliverypointEntityInstance, entertainmentEntityInstance, mediaEntityInstance, orderEntityInstance, pageEntityInstance, productCategoryEntityInstance, siteEntityInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'Message' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MessageEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity categoryEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity customerEntityInstance, IEntity deliverypointEntityInstance, IEntity entertainmentEntityInstance, IEntity mediaEntityInstance, IEntity orderEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MessageEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, clientEntityInstance, companyEntityInstance, customerEntityInstance, deliverypointEntityInstance, entertainmentEntityInstance, mediaEntityInstance, orderEntityInstance, pageEntityInstance, productCategoryEntityInstance, siteEntityInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MessageEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity categoryEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity customerEntityInstance, IEntity deliverypointEntityInstance, IEntity entertainmentEntityInstance, IEntity mediaEntityInstance, IEntity orderEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.MessageEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(categoryEntityInstance, clientEntityInstance, companyEntityInstance, customerEntityInstance, deliverypointEntityInstance, entertainmentEntityInstance, mediaEntityInstance, orderEntityInstance, pageEntityInstance, productCategoryEntityInstance, siteEntityInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MessageEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity categoryEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity customerEntityInstance, IEntity deliverypointEntityInstance, IEntity entertainmentEntityInstance, IEntity mediaEntityInstance, IEntity orderEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity siteEntityInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(categoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.CategoryId], ComparisonOperator.Equal, ((CategoryEntity)categoryEntityInstance).CategoryId));
			}
			if(clientEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.ClientId], ComparisonOperator.Equal, ((ClientEntity)clientEntityInstance).ClientId));
			}
			if(companyEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.CompanyId], ComparisonOperator.Equal, ((CompanyEntity)companyEntityInstance).CompanyId));
			}
			if(customerEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.CustomerId], ComparisonOperator.Equal, ((CustomerEntity)customerEntityInstance).CustomerId));
			}
			if(deliverypointEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.DeliverypointId], ComparisonOperator.Equal, ((DeliverypointEntity)deliverypointEntityInstance).DeliverypointId));
			}
			if(entertainmentEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.EntertainmentId], ComparisonOperator.Equal, ((EntertainmentEntity)entertainmentEntityInstance).EntertainmentId));
			}
			if(mediaEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.MediaId], ComparisonOperator.Equal, ((MediaEntity)mediaEntityInstance).MediaId));
			}
			if(orderEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.OrderId], ComparisonOperator.Equal, ((OrderEntity)orderEntityInstance).OrderId));
			}
			if(pageEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.PageId], ComparisonOperator.Equal, ((PageEntity)pageEntityInstance).PageId));
			}
			if(productCategoryEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.ProductCategoryId], ComparisonOperator.Equal, ((ProductCategoryEntity)productCategoryEntityInstance).ProductCategoryId));
			}
			if(siteEntityInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)MessageFieldIndex.SiteId], ComparisonOperator.Equal, ((SiteEntity)siteEntityInstance).SiteId));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
