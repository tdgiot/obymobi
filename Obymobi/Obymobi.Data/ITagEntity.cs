﻿using System.Collections.Generic;
using Obymobi.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data
{
    public interface ITagEntity
    {
        ICollection<ITag> GetTags();

        ICollection<ITag> GetInheritedTags();

        IEntityCollection TagCollection { get; }
    }
}
