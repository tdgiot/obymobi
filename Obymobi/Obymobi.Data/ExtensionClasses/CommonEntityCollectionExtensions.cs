﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.ExtensionClasses
{
    public static class CommonEntityCollectionExtensions
    {
        public static void SetValidator<TEntity>(this CommonEntityCollection<TEntity> entityCollection, IValidator validator) where TEntity : EntityBase
        {
            foreach (TEntity entity in entityCollection)
            {
                entity.Validator = validator;
            }
        }
    }
}
