﻿using System.IO;
using Dionysos;

namespace Obymobi.Data.EntityClasses
{
    public static class MediaExtensions
    {
        public static void SetMimeTypeAndExtension(this MediaEntity media, string fileName)
        {
            media.Extension = Path.GetExtension(fileName);
            media.MimeType = MimeTypeHelper.GetMimeType(fileName);
        }
    }
}
