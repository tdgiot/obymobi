﻿using Obymobi.Enums;

namespace Obymobi.Data.EntityClasses
{
    public static class CompanyExtensions
    {
        public static POSConnectorType GetPOSConnectorType(this CompanyEntity company) => company.POSConnectorType;
        public static bool IsTestingCompany(this CompanyEntity company) => company.ReleaseGroup == ReleaseGroup.Test;
        public static bool IsDevelopmentCompany(this CompanyEntity company) => company.ReleaseGroup == ReleaseGroup.Dev;
    }
}
