﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.ExtensionClasses
{
    public static class UIScheduleItemExtensions
    {
        public static bool IsHandledClientSide(this UIScheduleItemEntity scheduledItem) =>
            scheduledItem.UIWidgetId.HasValue ||
            scheduledItem.MediaId.HasValue;
    }
}
