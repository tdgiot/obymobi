﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Obymobi.Data.Extensions
{
    public static class SqlDataReaderExtensions
    {
        public static Dictionary<int, string> GetDataReaderScheme(this SqlDataReader dataReader)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                result.Add(i, dataReader.GetName(i));
            }

            return result;
        }
    }
}
