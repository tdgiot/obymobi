﻿namespace Obymobi.Data
{
    public interface IBrandRelatedEntity
    {
        int BrandId { get; set; }
    }
}