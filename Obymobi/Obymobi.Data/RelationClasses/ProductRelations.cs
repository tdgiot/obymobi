﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Product. </summary>
	public partial class ProductRelations
	{
		/// <summary>CTor</summary>
		public ProductRelations()
		{
		}

		/// <summary>Gets all relations of the ProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingProductId);
			toReturn.Add(this.AdvertisementTagProductEntityUsingProductId);
			toReturn.Add(this.AlterationoptionEntityUsingProductId);
			toReturn.Add(this.AlterationProductEntityUsingProductId);
			toReturn.Add(this.AnnouncementEntityUsingOnYesProduct);
			toReturn.Add(this.ActionEntityUsingProductId);
			toReturn.Add(this.AttachmentEntityUsingProductId);
			toReturn.Add(this.CategoryEntityUsingProductId);
			toReturn.Add(this.CategorySuggestionEntityUsingProductId);
			toReturn.Add(this.CustomTextEntityUsingProductId);
			toReturn.Add(this.DeliverypointgroupEntityUsingHotSOSBatteryLowProductId);
			toReturn.Add(this.DeliverypointgroupProductEntityUsingProductId);
			toReturn.Add(this.MediaEntityUsingActionProductId);
			toReturn.Add(this.MediaEntityUsingProductId);
			toReturn.Add(this.MediaRelationshipEntityUsingProductId);
			toReturn.Add(this.MessageRecipientEntityUsingProductId);
			toReturn.Add(this.MessageTemplateEntityUsingProductId);
			toReturn.Add(this.OrderHourEntityUsingProductId);
			toReturn.Add(this.OrderitemEntityUsingProductId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationoptionProductId);
			toReturn.Add(this.OrderitemTagEntityUsingProductId);
			toReturn.Add(this.OutletEntityUsingDeliveryChargeProductId);
			toReturn.Add(this.OutletEntityUsingServiceChargeProductId);
			toReturn.Add(this.OutletEntityUsingTippingProductId);
			toReturn.Add(this.PriceLevelItemEntityUsingProductId);
			toReturn.Add(this.ProductEntityUsingBrandProductId);
			toReturn.Add(this.ProductAlterationEntityUsingProductId);
			toReturn.Add(this.ProductAttachmentEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductId);
			toReturn.Add(this.ProductgroupItemEntityUsingProductId);
			toReturn.Add(this.ProductLanguageEntityUsingProductId);
			toReturn.Add(this.ProductSuggestionEntityUsingProductId);
			toReturn.Add(this.ProductSuggestionEntityUsingSuggestedProductId);
			toReturn.Add(this.ProductTagEntityUsingProductId);
			toReturn.Add(this.RatingEntityUsingProductId);
			toReturn.Add(this.ServiceMethodEntityUsingServiceChargeProductId);
			toReturn.Add(this.TerminalEntityUsingBatteryLowProductId);
			toReturn.Add(this.TerminalEntityUsingClientDisconnectedProductId);
			toReturn.Add(this.TerminalEntityUsingOrderFailedProductId);
			toReturn.Add(this.TerminalEntityUsingUnlockDeliverypointProductId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.ExternalProductEntityUsingExternalProductId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.PosproductEntityUsingPosproductId);
			toReturn.Add(this.ProductEntityUsingProductIdBrandProductId);
			toReturn.Add(this.ProductgroupEntityUsingProductgroupId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			toReturn.Add(this.ScheduleEntityUsingScheduleId);
			toReturn.Add(this.TaxTariffEntityUsingTaxTariffId);
			toReturn.Add(this.VattariffEntityUsingVattariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Advertisement.ProductId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AdvertisementFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AdvertisementTagProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - AdvertisementTagProduct.ProductId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagProductCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AdvertisementTagProductFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Alterationoption.ProductId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AlterationoptionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AlterationProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - AlterationProduct.ProductId
		/// </summary>
		public virtual IEntityRelation AlterationProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationProductCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AlterationProductFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Announcement.OnYesProduct
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnYesProduct
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AnnouncementFields.OnYesProduct);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ActionEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Action.ProductId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ActionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Attachment.ProductId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, AttachmentFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Category.ProductId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, CategoryFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CategorySuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - CategorySuggestion.ProductId
		/// </summary>
		public virtual IEntityRelation CategorySuggestionEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategorySuggestionCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, CategorySuggestionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - CustomText.ProductId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, CustomTextFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Deliverypointgroup.HotSOSBatteryLowProductId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingHotSOSBatteryLowProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, DeliverypointgroupFields.HotSOSBatteryLowProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and DeliverypointgroupProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - DeliverypointgroupProduct.ProductId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupProductCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, DeliverypointgroupProductFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Media.ActionProductId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionMediaCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, MediaFields.ActionProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Media.ProductId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, MediaFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MediaRelationshipEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - MediaRelationship.ProductId
		/// </summary>
		public virtual IEntityRelation MediaRelationshipEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaRelationshipCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, MediaRelationshipFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRelationshipEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - MessageRecipient.ProductId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, MessageRecipientFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - MessageTemplate.ProductId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, MessageTemplateFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderHourEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - OrderHour.ProductId
		/// </summary>
		public virtual IEntityRelation OrderHourEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderHourCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderHourFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderHourEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Orderitem.ProductId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - OrderitemAlterationitem.AlterationoptionProductId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationoptionProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemAlterationitemFields.AlterationoptionProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - OrderitemTag.ProductId
		/// </summary>
		public virtual IEntityRelation OrderitemTagEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemTagCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemTagFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Outlet.DeliveryChargeProductId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingDeliveryChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliveryChargeOutletCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.DeliveryChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Outlet.ServiceChargeProductId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingServiceChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceChargeOutletCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.ServiceChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Outlet.TippingProductId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingTippingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TippingProductOutletCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.TippingProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - PriceLevelItem.ProductId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceLevelItemCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, PriceLevelItemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Product.BrandProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingBrandProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BrandProductCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductFields.BrandProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductAlteration.ProductId
		/// </summary>
		public virtual IEntityRelation ProductAlterationEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductAlterationCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductAlterationFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductAttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductAttachment.ProductId
		/// </summary>
		public virtual IEntityRelation ProductAttachmentEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductAttachmentCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductAttachmentFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAttachmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductCategory.ProductId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategoryCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductCategoryFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductgroupItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductgroupItem.ProductId
		/// </summary>
		public virtual IEntityRelation ProductgroupItemEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductgroupItemCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductgroupItemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductLanguage.ProductId
		/// </summary>
		public virtual IEntityRelation ProductLanguageEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductLanguageCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductLanguageFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductSuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductSuggestion.ProductId
		/// </summary>
		public virtual IEntityRelation ProductSuggestionEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductSuggestionCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductSuggestionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductSuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductSuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductSuggestion.SuggestedProductId
		/// </summary>
		public virtual IEntityRelation ProductSuggestionEntityUsingSuggestedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductSuggestionFields.SuggestedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductSuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ProductTag.ProductId
		/// </summary>
		public virtual IEntityRelation ProductTagEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTagCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductTagFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and RatingEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Rating.ProductId
		/// </summary>
		public virtual IEntityRelation RatingEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RatingCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, RatingFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RatingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - ServiceMethod.ServiceChargeProductId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, ServiceMethodFields.ServiceChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Terminal.BatteryLowProductId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingBatteryLowProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection_" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.BatteryLowProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Terminal.ClientDisconnectedProductId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingClientDisconnectedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection__" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.ClientDisconnectedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Terminal.OrderFailedProductId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingOrderFailedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection___" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.OrderFailedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductId - Terminal.UnlockDeliverypointProductId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingUnlockDeliverypointProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.UnlockDeliverypointProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProductEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, ProductFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ProductFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ExternalProductId - ExternalProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalProductEntity", false);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ProductFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, ProductFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, ProductFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PosproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.PosproductId - Posproduct.PosproductId
		/// </summary>
		public virtual IEntityRelation PosproductEntityUsingPosproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosproductEntity", false);
				relation.AddEntityFieldPair(PosproductFields.PosproductId, ProductFields.PosproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.BrandProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductIdBrandProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductFields.BrandProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ProductgroupId - Productgroup.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductgroupEntity", false);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, ProductFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ScheduleId - Schedule.ScheduleId
		/// </summary>
		public virtual IEntityRelation ScheduleEntityUsingScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduleEntity", false);
				relation.AddEntityFieldPair(ScheduleFields.ScheduleId, ProductFields.ScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.TaxTariffId - TaxTariff.TaxTariffId
		/// </summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TaxTariffEntity", false);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, ProductFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and VattariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.VattariffId - Vattariff.VattariffId
		/// </summary>
		public virtual IEntityRelation VattariffEntityUsingVattariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VattariffEntity", false);
				relation.AddEntityFieldPair(VattariffFields.VattariffId, ProductFields.VattariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingProductIdStatic = new ProductRelations().AdvertisementEntityUsingProductId;
		internal static readonly IEntityRelation AdvertisementTagProductEntityUsingProductIdStatic = new ProductRelations().AdvertisementTagProductEntityUsingProductId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingProductIdStatic = new ProductRelations().AlterationoptionEntityUsingProductId;
		internal static readonly IEntityRelation AlterationProductEntityUsingProductIdStatic = new ProductRelations().AlterationProductEntityUsingProductId;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnYesProductStatic = new ProductRelations().AnnouncementEntityUsingOnYesProduct;
		internal static readonly IEntityRelation ActionEntityUsingProductIdStatic = new ProductRelations().ActionEntityUsingProductId;
		internal static readonly IEntityRelation AttachmentEntityUsingProductIdStatic = new ProductRelations().AttachmentEntityUsingProductId;
		internal static readonly IEntityRelation CategoryEntityUsingProductIdStatic = new ProductRelations().CategoryEntityUsingProductId;
		internal static readonly IEntityRelation CategorySuggestionEntityUsingProductIdStatic = new ProductRelations().CategorySuggestionEntityUsingProductId;
		internal static readonly IEntityRelation CustomTextEntityUsingProductIdStatic = new ProductRelations().CustomTextEntityUsingProductId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingHotSOSBatteryLowProductIdStatic = new ProductRelations().DeliverypointgroupEntityUsingHotSOSBatteryLowProductId;
		internal static readonly IEntityRelation DeliverypointgroupProductEntityUsingProductIdStatic = new ProductRelations().DeliverypointgroupProductEntityUsingProductId;
		internal static readonly IEntityRelation MediaEntityUsingActionProductIdStatic = new ProductRelations().MediaEntityUsingActionProductId;
		internal static readonly IEntityRelation MediaEntityUsingProductIdStatic = new ProductRelations().MediaEntityUsingProductId;
		internal static readonly IEntityRelation MediaRelationshipEntityUsingProductIdStatic = new ProductRelations().MediaRelationshipEntityUsingProductId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingProductIdStatic = new ProductRelations().MessageRecipientEntityUsingProductId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingProductIdStatic = new ProductRelations().MessageTemplateEntityUsingProductId;
		internal static readonly IEntityRelation OrderHourEntityUsingProductIdStatic = new ProductRelations().OrderHourEntityUsingProductId;
		internal static readonly IEntityRelation OrderitemEntityUsingProductIdStatic = new ProductRelations().OrderitemEntityUsingProductId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationoptionProductIdStatic = new ProductRelations().OrderitemAlterationitemEntityUsingAlterationoptionProductId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingProductIdStatic = new ProductRelations().OrderitemTagEntityUsingProductId;
		internal static readonly IEntityRelation OutletEntityUsingDeliveryChargeProductIdStatic = new ProductRelations().OutletEntityUsingDeliveryChargeProductId;
		internal static readonly IEntityRelation OutletEntityUsingServiceChargeProductIdStatic = new ProductRelations().OutletEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation OutletEntityUsingTippingProductIdStatic = new ProductRelations().OutletEntityUsingTippingProductId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingProductIdStatic = new ProductRelations().PriceLevelItemEntityUsingProductId;
		internal static readonly IEntityRelation ProductEntityUsingBrandProductIdStatic = new ProductRelations().ProductEntityUsingBrandProductId;
		internal static readonly IEntityRelation ProductAlterationEntityUsingProductIdStatic = new ProductRelations().ProductAlterationEntityUsingProductId;
		internal static readonly IEntityRelation ProductAttachmentEntityUsingProductIdStatic = new ProductRelations().ProductAttachmentEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductIdStatic = new ProductRelations().ProductCategoryEntityUsingProductId;
		internal static readonly IEntityRelation ProductgroupItemEntityUsingProductIdStatic = new ProductRelations().ProductgroupItemEntityUsingProductId;
		internal static readonly IEntityRelation ProductLanguageEntityUsingProductIdStatic = new ProductRelations().ProductLanguageEntityUsingProductId;
		internal static readonly IEntityRelation ProductSuggestionEntityUsingProductIdStatic = new ProductRelations().ProductSuggestionEntityUsingProductId;
		internal static readonly IEntityRelation ProductSuggestionEntityUsingSuggestedProductIdStatic = new ProductRelations().ProductSuggestionEntityUsingSuggestedProductId;
		internal static readonly IEntityRelation ProductTagEntityUsingProductIdStatic = new ProductRelations().ProductTagEntityUsingProductId;
		internal static readonly IEntityRelation RatingEntityUsingProductIdStatic = new ProductRelations().RatingEntityUsingProductId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceChargeProductIdStatic = new ProductRelations().ServiceMethodEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation TerminalEntityUsingBatteryLowProductIdStatic = new ProductRelations().TerminalEntityUsingBatteryLowProductId;
		internal static readonly IEntityRelation TerminalEntityUsingClientDisconnectedProductIdStatic = new ProductRelations().TerminalEntityUsingClientDisconnectedProductId;
		internal static readonly IEntityRelation TerminalEntityUsingOrderFailedProductIdStatic = new ProductRelations().TerminalEntityUsingOrderFailedProductId;
		internal static readonly IEntityRelation TerminalEntityUsingUnlockDeliverypointProductIdStatic = new ProductRelations().TerminalEntityUsingUnlockDeliverypointProductId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new ProductRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ProductRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new ProductRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new ProductRelations().GenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new ProductRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation PosproductEntityUsingPosproductIdStatic = new ProductRelations().PosproductEntityUsingPosproductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdBrandProductIdStatic = new ProductRelations().ProductEntityUsingProductIdBrandProductId;
		internal static readonly IEntityRelation ProductgroupEntityUsingProductgroupIdStatic = new ProductRelations().ProductgroupEntityUsingProductgroupId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new ProductRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation ScheduleEntityUsingScheduleIdStatic = new ProductRelations().ScheduleEntityUsingScheduleId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new ProductRelations().TaxTariffEntityUsingTaxTariffId;
		internal static readonly IEntityRelation VattariffEntityUsingVattariffIdStatic = new ProductRelations().VattariffEntityUsingVattariffId;

		/// <summary>CTor</summary>
		static StaticProductRelations()
		{
		}
	}
}
