﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PointOfInterestAmenity. </summary>
	public partial class PointOfInterestAmenityRelations
	{
		/// <summary>CTor</summary>
		public PointOfInterestAmenityRelations()
		{
		}

		/// <summary>Gets all relations of the PointOfInterestAmenityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AmenityEntityUsingAmenityId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PointOfInterestAmenityEntity and AmenityEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterestAmenity.AmenityId - Amenity.AmenityId
		/// </summary>
		public virtual IEntityRelation AmenityEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AmenityEntity", false);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, PointOfInterestAmenityFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestAmenityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PointOfInterestAmenityEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterestAmenity.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, PointOfInterestAmenityFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestAmenityEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPointOfInterestAmenityRelations
	{
		internal static readonly IEntityRelation AmenityEntityUsingAmenityIdStatic = new PointOfInterestAmenityRelations().AmenityEntityUsingAmenityId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new PointOfInterestAmenityRelations().PointOfInterestEntityUsingPointOfInterestId;

		/// <summary>CTor</summary>
		static StaticPointOfInterestAmenityRelations()
		{
		}
	}
}
