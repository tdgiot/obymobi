﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alterationitem. </summary>
	public partial class AlterationitemRelations
	{
		/// <summary>CTor</summary>
		public AlterationitemRelations()
		{
		}

		/// <summary>Gets all relations of the AlterationitemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationitemAlterationEntityUsingAlterationitemId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationitemId);
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.PosalterationitemEntityUsingPosalterationitemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationitemAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationitem.AlterationitemId - AlterationitemAlteration.AlterationitemId
		/// </summary>
		public virtual IEntityRelation AlterationitemAlterationEntityUsingAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationitemAlterationCollection" , true);
				relation.AddEntityFieldPair(AlterationitemFields.AlterationitemId, AlterationitemAlterationFields.AlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemAlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationitem.AlterationitemId - OrderitemAlterationitem.AlterationitemId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(AlterationitemFields.AlterationitemId, OrderitemAlterationitemFields.AlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationitem.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationitemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationitem.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, AlterationitemFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationitemEntity and PosalterationitemEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationitem.PosalterationitemId - Posalterationitem.PosalterationitemId
		/// </summary>
		public virtual IEntityRelation PosalterationitemEntityUsingPosalterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosalterationitemEntity", false);
				relation.AddEntityFieldPair(PosalterationitemFields.PosalterationitemId, AlterationitemFields.PosalterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationitemRelations
	{
		internal static readonly IEntityRelation AlterationitemAlterationEntityUsingAlterationitemIdStatic = new AlterationitemRelations().AlterationitemAlterationEntityUsingAlterationitemId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationitemIdStatic = new AlterationitemRelations().OrderitemAlterationitemEntityUsingAlterationitemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new AlterationitemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new AlterationitemRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation PosalterationitemEntityUsingPosalterationitemIdStatic = new AlterationitemRelations().PosalterationitemEntityUsingPosalterationitemId;

		/// <summary>CTor</summary>
		static StaticAlterationitemRelations()
		{
		}
	}
}
