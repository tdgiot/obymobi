﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Timestamp. </summary>
	public partial class TimestampRelations
	{
		/// <summary>CTor</summary>
		public TimestampRelations()
		{
		}

		/// <summary>Gets all relations of the TimestampEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
			toReturn.Add(this.InfraredConfigurationEntityUsingInfraredConfigurationId);
			toReturn.Add(this.MapEntityUsingMapId);
			toReturn.Add(this.MenuEntityUsingMenuId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.PriceScheduleEntityUsingPriceScheduleId);
			toReturn.Add(this.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			toReturn.Add(this.TerminalConfigurationEntityUsingTerminalConfigurationId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			toReturn.Add(this.UIScheduleEntityUsingUIScheduleId);
			toReturn.Add(this.UIThemeEntityUsingUIThemeId);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and AdvertisementConfigurationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.AdvertisementConfigurationId - AdvertisementConfiguration.AdvertisementConfigurationId
		/// </summary>
		public virtual IEntityRelation AdvertisementConfigurationEntityUsingAdvertisementConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "AdvertisementConfigurationEntity", false);




				relation.AddEntityFieldPair(AdvertisementConfigurationFields.AdvertisementConfigurationId, TimestampFields.AdvertisementConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and ClientEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ClientEntity", false);




				relation.AddEntityFieldPair(ClientFields.ClientId, TimestampFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and CompanyEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CompanyEntity", false);




				relation.AddEntityFieldPair(CompanyFields.CompanyId, TimestampFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and DeliverypointEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "DeliverypointEntity", false);




				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TimestampFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and DeliverypointgroupEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "DeliverypointgroupEntity", false);




				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, TimestampFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and EntertainmentConfigurationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.EntertainmentConfigurationId - EntertainmentConfiguration.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "EntertainmentConfigurationEntity", false);




				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, TimestampFields.EntertainmentConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and InfraredConfigurationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.InfraredConfigurationId - InfraredConfiguration.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "InfraredConfigurationEntity", false);




				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, TimestampFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and MapEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.MapId - Map.MapId
		/// </summary>
		public virtual IEntityRelation MapEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "MapEntity", false);




				relation.AddEntityFieldPair(MapFields.MapId, TimestampFields.MapId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and MenuEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.MenuId - Menu.MenuId
		/// </summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "MenuEntity", false);




				relation.AddEntityFieldPair(MenuFields.MenuId, TimestampFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and PointOfInterestEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PointOfInterestEntity", false);




				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, TimestampFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and PriceScheduleEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.PriceScheduleId - PriceSchedule.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PriceScheduleEntity", false);




				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, TimestampFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and RoomControlConfigurationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.RoomControlConfigurationId - RoomControlConfiguration.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "RoomControlConfigurationEntity", false);




				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, TimestampFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and SiteEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "SiteEntity", false);




				relation.AddEntityFieldPair(SiteFields.SiteId, TimestampFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and TerminalConfigurationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Timestamp.TerminalConfigurationId - TerminalConfiguration.TerminalConfigurationId
		/// </summary>
		public virtual IEntityRelation TerminalConfigurationEntityUsingTerminalConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TerminalConfigurationEntity", false);




				relation.AddEntityFieldPair(TerminalConfigurationFields.TerminalConfigurationId, TimestampFields.TerminalConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Timestamp.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TimestampFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Timestamp.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TimestampFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and UIScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// Timestamp.UIScheduleId - UISchedule.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleEntity", false);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, TimestampFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TimestampEntity and UIThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// Timestamp.UIThemeId - UITheme.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIThemeEntity", false);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, TimestampFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTimestampRelations
	{
		internal static readonly IEntityRelation AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic = new TimestampRelations().AdvertisementConfigurationEntityUsingAdvertisementConfigurationId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new TimestampRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TimestampRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new TimestampRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new TimestampRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic = new TimestampRelations().EntertainmentConfigurationEntityUsingEntertainmentConfigurationId;
		internal static readonly IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationIdStatic = new TimestampRelations().InfraredConfigurationEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation MapEntityUsingMapIdStatic = new TimestampRelations().MapEntityUsingMapId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new TimestampRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new TimestampRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new TimestampRelations().PriceScheduleEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic = new TimestampRelations().RoomControlConfigurationEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new TimestampRelations().SiteEntityUsingSiteId;
		internal static readonly IEntityRelation TerminalConfigurationEntityUsingTerminalConfigurationIdStatic = new TimestampRelations().TerminalConfigurationEntityUsingTerminalConfigurationId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new TimestampRelations().TerminalEntityUsingTerminalId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new TimestampRelations().UIModeEntityUsingUIModeId;
		internal static readonly IEntityRelation UIScheduleEntityUsingUIScheduleIdStatic = new TimestampRelations().UIScheduleEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIThemeEntityUsingUIThemeIdStatic = new TimestampRelations().UIThemeEntityUsingUIThemeId;

		/// <summary>CTor</summary>
		static StaticTimestampRelations()
		{
		}
	}
}
