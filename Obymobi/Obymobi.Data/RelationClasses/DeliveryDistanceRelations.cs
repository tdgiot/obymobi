﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliveryDistance. </summary>
	public partial class DeliveryDistanceRelations
	{
		/// <summary>CTor</summary>
		public DeliveryDistanceRelations()
		{
		}

		/// <summary>Gets all relations of the DeliveryDistanceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ServiceMethodEntityUsingServiceMethodId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeliveryDistanceEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliveryDistance.ServiceMethodId - ServiceMethod.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceMethodEntity", false);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, DeliveryDistanceFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliveryDistanceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliveryDistanceRelations
	{
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new DeliveryDistanceRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticDeliveryDistanceRelations()
		{
		}
	}
}
