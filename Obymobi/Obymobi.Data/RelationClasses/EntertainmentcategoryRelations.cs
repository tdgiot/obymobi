﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Entertainmentcategory. </summary>
	public partial class EntertainmentcategoryRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentcategoryRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentcategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingActionEntertainmentCategoryId);
			toReturn.Add(this.AnnouncementEntityUsingOnYesEntertainmentCategory);
			toReturn.Add(this.AnnouncementEntityUsingOnNoEntertainmentCategory);
			toReturn.Add(this.CustomTextEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.MediaEntityUsingActionEntertainmentcategoryId);
			toReturn.Add(this.UIWidgetEntityUsingEntertainmentcategoryId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - Advertisement.ActionEntertainmentCategoryId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingActionEntertainmentCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AdvertisementFields.ActionEntertainmentCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - Announcement.OnYesEntertainmentCategory
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnYesEntertainmentCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AnnouncementFields.OnYesEntertainmentCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - Announcement.OnNoEntertainmentCategory
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnNoEntertainmentCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection_" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AnnouncementFields.OnNoEntertainmentCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - CustomText.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, CustomTextFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and EntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - Entertainment.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, EntertainmentFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and EntertainmentcategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - EntertainmentcategoryLanguage.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentcategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, EntertainmentcategoryLanguageFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - Media.ActionEntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionMediaCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, MediaFields.ActionEntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentcategoryEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmentcategory.EntertainmentcategoryId - UIWidget.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, UIWidgetFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentcategoryRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingActionEntertainmentCategoryIdStatic = new EntertainmentcategoryRelations().AdvertisementEntityUsingActionEntertainmentCategoryId;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnYesEntertainmentCategoryStatic = new EntertainmentcategoryRelations().AnnouncementEntityUsingOnYesEntertainmentCategory;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnNoEntertainmentCategoryStatic = new EntertainmentcategoryRelations().AnnouncementEntityUsingOnNoEntertainmentCategory;
		internal static readonly IEntityRelation CustomTextEntityUsingEntertainmentcategoryIdStatic = new EntertainmentcategoryRelations().CustomTextEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentcategoryIdStatic = new EntertainmentcategoryRelations().EntertainmentEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryIdStatic = new EntertainmentcategoryRelations().EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation MediaEntityUsingActionEntertainmentcategoryIdStatic = new EntertainmentcategoryRelations().MediaEntityUsingActionEntertainmentcategoryId;
		internal static readonly IEntityRelation UIWidgetEntityUsingEntertainmentcategoryIdStatic = new EntertainmentcategoryRelations().UIWidgetEntityUsingEntertainmentcategoryId;

		/// <summary>CTor</summary>
		static StaticEntertainmentcategoryRelations()
		{
		}
	}
}
