﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Netmessage. </summary>
	public partial class NetmessageRelations
	{
		/// <summary>CTor</summary>
		public NetmessageRelations()
		{
		}

		/// <summary>Gets all relations of the NetmessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingReceiverClientId);
			toReturn.Add(this.ClientEntityUsingSenderClientId);
			toReturn.Add(this.CompanyEntityUsingReceiverCompanyId);
			toReturn.Add(this.CompanyEntityUsingSenderCompanyId);
			toReturn.Add(this.CustomerEntityUsingReceiverCustomerId);
			toReturn.Add(this.CustomerEntityUsingSenderCustomerId);
			toReturn.Add(this.DeliverypointEntityUsingReceiverDeliverypointId);
			toReturn.Add(this.DeliverypointEntityUsingSenderDeliverypointId);
			toReturn.Add(this.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId);
			toReturn.Add(this.TerminalEntityUsingReceiverTerminalId);
			toReturn.Add(this.TerminalEntityUsingSenderTerminalId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingReceiverClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiverClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, NetmessageFields.ReceiverClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.SenderClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingSenderClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SenderClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, NetmessageFields.SenderClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingReceiverCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiverCompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, NetmessageFields.ReceiverCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.SenderCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingSenderCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SenderCompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, NetmessageFields.SenderCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverCustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingReceiverCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RecieverCustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, NetmessageFields.ReceiverCustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.SenderCustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingSenderCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SenderCustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, NetmessageFields.SenderCustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingReceiverDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiverDeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, NetmessageFields.ReceiverDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.SenderDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingSenderDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SenderDeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, NetmessageFields.SenderDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverDeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingReceiverDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, NetmessageFields.ReceiverDeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.ReceiverTerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingReceiverTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiverTerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, NetmessageFields.ReceiverTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NetmessageEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Netmessage.SenderTerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingSenderTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SenderTerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, NetmessageFields.SenderTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNetmessageRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingReceiverClientIdStatic = new NetmessageRelations().ClientEntityUsingReceiverClientId;
		internal static readonly IEntityRelation ClientEntityUsingSenderClientIdStatic = new NetmessageRelations().ClientEntityUsingSenderClientId;
		internal static readonly IEntityRelation CompanyEntityUsingReceiverCompanyIdStatic = new NetmessageRelations().CompanyEntityUsingReceiverCompanyId;
		internal static readonly IEntityRelation CompanyEntityUsingSenderCompanyIdStatic = new NetmessageRelations().CompanyEntityUsingSenderCompanyId;
		internal static readonly IEntityRelation CustomerEntityUsingReceiverCustomerIdStatic = new NetmessageRelations().CustomerEntityUsingReceiverCustomerId;
		internal static readonly IEntityRelation CustomerEntityUsingSenderCustomerIdStatic = new NetmessageRelations().CustomerEntityUsingSenderCustomerId;
		internal static readonly IEntityRelation DeliverypointEntityUsingReceiverDeliverypointIdStatic = new NetmessageRelations().DeliverypointEntityUsingReceiverDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingSenderDeliverypointIdStatic = new NetmessageRelations().DeliverypointEntityUsingSenderDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingReceiverDeliverypointgroupIdStatic = new NetmessageRelations().DeliverypointgroupEntityUsingReceiverDeliverypointgroupId;
		internal static readonly IEntityRelation TerminalEntityUsingReceiverTerminalIdStatic = new NetmessageRelations().TerminalEntityUsingReceiverTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingSenderTerminalIdStatic = new NetmessageRelations().TerminalEntityUsingSenderTerminalId;

		/// <summary>CTor</summary>
		static StaticNetmessageRelations()
		{
		}
	}
}
