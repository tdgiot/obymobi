﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdvertisementConfiguration. </summary>
	public partial class AdvertisementConfigurationRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementConfigurationAdvertisementEntityUsingAdvertisementConfigurationId);
			toReturn.Add(this.ClientConfigurationEntityUsingAdvertisementConfigurationId);
			toReturn.Add(this.TimestampEntityUsingAdvertisementConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AdvertisementConfigurationEntity and AdvertisementConfigurationAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementConfiguration.AdvertisementConfigurationId - AdvertisementConfigurationAdvertisement.AdvertisementConfigurationId
		/// </summary>
		public virtual IEntityRelation AdvertisementConfigurationAdvertisementEntityUsingAdvertisementConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementConfigurationAdvertisementCollection" , true);
				relation.AddEntityFieldPair(AdvertisementConfigurationFields.AdvertisementConfigurationId, AdvertisementConfigurationAdvertisementFields.AdvertisementConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementConfigurationEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementConfiguration.AdvertisementConfigurationId - ClientConfiguration.AdvertisementConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingAdvertisementConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(AdvertisementConfigurationFields.AdvertisementConfigurationId, ClientConfigurationFields.AdvertisementConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementConfigurationEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// AdvertisementConfiguration.AdvertisementConfigurationId - Timestamp.AdvertisementConfigurationId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingAdvertisementConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(AdvertisementConfigurationFields.AdvertisementConfigurationId, TimestampFields.AdvertisementConfigurationId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AdvertisementConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementConfigurationRelations
	{
		internal static readonly IEntityRelation AdvertisementConfigurationAdvertisementEntityUsingAdvertisementConfigurationIdStatic = new AdvertisementConfigurationRelations().AdvertisementConfigurationAdvertisementEntityUsingAdvertisementConfigurationId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingAdvertisementConfigurationIdStatic = new AdvertisementConfigurationRelations().ClientConfigurationEntityUsingAdvertisementConfigurationId;
		internal static readonly IEntityRelation TimestampEntityUsingAdvertisementConfigurationIdStatic = new AdvertisementConfigurationRelations().TimestampEntityUsingAdvertisementConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AdvertisementConfigurationRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticAdvertisementConfigurationRelations()
		{
		}
	}
}
