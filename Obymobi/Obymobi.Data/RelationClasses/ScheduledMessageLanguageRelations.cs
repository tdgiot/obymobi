﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ScheduledMessageLanguage. </summary>
	public partial class ScheduledMessageLanguageRelations
	{
		/// <summary>CTor</summary>
		public ScheduledMessageLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduledMessageLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.ScheduledMessageEntityUsingScheduledMessageId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, ScheduledMessageLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageLanguageEntity and ScheduledMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageLanguage.ScheduledMessageId - ScheduledMessage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledMessageEntity", false);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, ScheduledMessageLanguageFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduledMessageLanguageRelations
	{
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new ScheduledMessageLanguageRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingScheduledMessageIdStatic = new ScheduledMessageLanguageRelations().ScheduledMessageEntityUsingScheduledMessageId;

		/// <summary>CTor</summary>
		static StaticScheduledMessageLanguageRelations()
		{
		}
	}
}
