﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlComponent. </summary>
	public partial class RoomControlComponentRelations
	{
		/// <summary>CTor</summary>
		public RoomControlComponentRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlComponentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingRoomControlComponentId);
			toReturn.Add(this.RoomControlComponentLanguageEntityUsingRoomControlComponentId);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId1);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId10);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId2);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId3);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId4);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId5);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId6);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId7);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId8);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlComponentId9);
			toReturn.Add(this.InfraredConfigurationEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - CustomText.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoomControlComponentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, CustomTextFields.RoomControlComponentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlComponentLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlComponentLanguage.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentLanguageEntityUsingRoomControlComponentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlComponentLanguageCollection" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlComponentLanguageFields.RoomControlComponentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId1
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId10
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId10
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection_" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId10);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId2
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection__" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId3
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId3
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection___" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId3);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId4
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId4
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection____" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId4);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId5
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId5
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection_____" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId5);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId6
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId6
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection______" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId6);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId7
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId7
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection_______" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId7);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId8
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId8
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection________" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId8);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlComponentId - RoomControlWidget.RoomControlComponentId9
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId9
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection_________" , true);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId9);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and InfraredConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlComponent.InfraredConfigurationId - InfraredConfiguration.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InfraredConfigurationEntity", false);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlComponentFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlComponent.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlComponentFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlComponentRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingRoomControlComponentIdStatic = new RoomControlComponentRelations().CustomTextEntityUsingRoomControlComponentId;
		internal static readonly IEntityRelation RoomControlComponentLanguageEntityUsingRoomControlComponentIdStatic = new RoomControlComponentRelations().RoomControlComponentLanguageEntityUsingRoomControlComponentId;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId1Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId1;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId10Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId10;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId2Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId2;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId3Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId3;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId4Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId4;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId5Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId5;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId6Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId6;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId7Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId7;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId8Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId8;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlComponentId9Static = new RoomControlComponentRelations().RoomControlWidgetEntityUsingRoomControlComponentId9;
		internal static readonly IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationIdStatic = new RoomControlComponentRelations().InfraredConfigurationEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new RoomControlComponentRelations().RoomControlSectionEntityUsingRoomControlSectionId;

		/// <summary>CTor</summary>
		static StaticRoomControlComponentRelations()
		{
		}
	}
}
