﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SiteLanguage. </summary>
	public partial class SiteLanguageRelations
	{
		/// <summary>CTor</summary>
		public SiteLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the SiteLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SiteLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// SiteLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SiteLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SiteLanguageEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// SiteLanguage.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, SiteLanguageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSiteLanguageRelations
	{
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new SiteLanguageRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new SiteLanguageRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticSiteLanguageRelations()
		{
		}
	}
}
