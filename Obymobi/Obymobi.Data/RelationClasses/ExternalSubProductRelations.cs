﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSubProduct. </summary>
	public partial class ExternalSubProductRelations
	{
		/// <summary>CTor</summary>
		public ExternalSubProductRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalSubProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalProductEntityUsingExternalProductId);
			toReturn.Add(this.ExternalProductEntityUsingExternalSubProductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ExternalSubProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalSubProduct.ExternalProductId - ExternalProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalProductEntity", false);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSubProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalSubProductEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalSubProduct.ExternalSubProductId - ExternalProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalSubProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalProductEntity1", false);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalSubProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSubProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSubProductRelations
	{
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new ExternalSubProductRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalSubProductIdStatic = new ExternalSubProductRelations().ExternalProductEntityUsingExternalSubProductId;

		/// <summary>CTor</summary>
		static StaticExternalSubProductRelations()
		{
		}
	}
}
