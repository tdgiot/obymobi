﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AffiliateCampaign. </summary>
	public partial class AffiliateCampaignRelations
	{
		/// <summary>CTor</summary>
		public AffiliateCampaignRelations()
		{
		}

		/// <summary>Gets all relations of the AffiliateCampaignEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AffiliateCampaignAffiliatePartnerEntityUsingAffiliateCampaignId);
			toReturn.Add(this.DeliverypointgroupEntityUsingAffiliateCampaignId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AffiliateCampaignEntity and AffiliateCampaignAffiliatePartnerEntity over the 1:n relation they have, using the relation between the fields:
		/// AffiliateCampaign.AffiliateCampaignId - AffiliateCampaignAffiliatePartner.AffiliateCampaignId
		/// </summary>
		public virtual IEntityRelation AffiliateCampaignAffiliatePartnerEntityUsingAffiliateCampaignId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AffiliateCampaignAffiliatePartnerCollection" , true);
				relation.AddEntityFieldPair(AffiliateCampaignFields.AffiliateCampaignId, AffiliateCampaignAffiliatePartnerFields.AffiliateCampaignId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignAffiliatePartnerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AffiliateCampaignEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// AffiliateCampaign.AffiliateCampaignId - Deliverypointgroup.AffiliateCampaignId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingAffiliateCampaignId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(AffiliateCampaignFields.AffiliateCampaignId, DeliverypointgroupFields.AffiliateCampaignId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAffiliateCampaignRelations
	{
		internal static readonly IEntityRelation AffiliateCampaignAffiliatePartnerEntityUsingAffiliateCampaignIdStatic = new AffiliateCampaignRelations().AffiliateCampaignAffiliatePartnerEntityUsingAffiliateCampaignId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingAffiliateCampaignIdStatic = new AffiliateCampaignRelations().DeliverypointgroupEntityUsingAffiliateCampaignId;

		/// <summary>CTor</summary>
		static StaticAffiliateCampaignRelations()
		{
		}
	}
}
