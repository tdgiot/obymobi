﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CompanyBrand. </summary>
	public partial class CompanyBrandRelations
	{
		/// <summary>CTor</summary>
		public CompanyBrandRelations()
		{
		}

		/// <summary>Gets all relations of the CompanyBrandEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CompanyBrandEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// CompanyBrand.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, CompanyBrandFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyBrandEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyBrandEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// CompanyBrand.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyBrandFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyBrandEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompanyBrandRelations
	{
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new CompanyBrandRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CompanyBrandRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticCompanyBrandRelations()
		{
		}
	}
}
