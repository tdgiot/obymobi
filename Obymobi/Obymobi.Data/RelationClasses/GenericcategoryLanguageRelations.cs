﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GenericcategoryLanguage. </summary>
	public partial class GenericcategoryLanguageRelations
	{
		/// <summary>CTor</summary>
		public GenericcategoryLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the GenericcategoryLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between GenericcategoryLanguageEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// GenericcategoryLanguage.GenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericcategoryLanguageFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericcategoryLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// GenericcategoryLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, GenericcategoryLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericcategoryLanguageRelations
	{
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdStatic = new GenericcategoryLanguageRelations().GenericcategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new GenericcategoryLanguageRelations().LanguageEntityUsingLanguageId;

		/// <summary>CTor</summary>
		static StaticGenericcategoryLanguageRelations()
		{
		}
	}
}
