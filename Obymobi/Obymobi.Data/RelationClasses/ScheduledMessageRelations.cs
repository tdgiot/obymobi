﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ScheduledMessage. </summary>
	public partial class ScheduledMessageRelations
	{
		/// <summary>CTor</summary>
		public ScheduledMessageRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduledMessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingScheduledMessageId);
			toReturn.Add(this.PmsActionRuleEntityUsingScheduledMessageId);
			toReturn.Add(this.ScheduledMessageHistoryEntityUsingScheduledMessageId);
			toReturn.Add(this.ScheduledMessageLanguageEntityUsingScheduledMessageId);
			toReturn.Add(this.UIScheduleItemEntityUsingScheduledMessageId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.MediaEntityUsingMediaId);
			toReturn.Add(this.MessageTemplateEntityUsingMessageTemplateId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledMessage.ScheduledMessageId - CustomText.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, CustomTextFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and PmsActionRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledMessage.ScheduledMessageId - PmsActionRule.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation PmsActionRuleEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsActionRuleCollection" , true);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, PmsActionRuleFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and ScheduledMessageHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledMessage.ScheduledMessageId - ScheduledMessageHistory.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageHistoryEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageHistoryCollection" , true);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, ScheduledMessageHistoryFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and ScheduledMessageLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledMessage.ScheduledMessageId - ScheduledMessageLanguage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageLanguageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageLanguageCollection" , true);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, ScheduledMessageLanguageFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and UIScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledMessage.ScheduledMessageId - UIScheduleItem.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemCollection" , true);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, UIScheduleItemFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ScheduledMessageFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduledMessageFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, ScheduledMessageFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, ScheduledMessageFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and MessageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.MessageTemplateId - MessageTemplate.MessageTemplateId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingMessageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageTemplateEntity", false);
				relation.AddEntityFieldPair(MessageTemplateFields.MessageTemplateId, ScheduledMessageFields.MessageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, ScheduledMessageFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ScheduledMessageFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessage.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, ScheduledMessageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduledMessageRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingScheduledMessageIdStatic = new ScheduledMessageRelations().CustomTextEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation PmsActionRuleEntityUsingScheduledMessageIdStatic = new ScheduledMessageRelations().PmsActionRuleEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation ScheduledMessageHistoryEntityUsingScheduledMessageIdStatic = new ScheduledMessageRelations().ScheduledMessageHistoryEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation ScheduledMessageLanguageEntityUsingScheduledMessageIdStatic = new ScheduledMessageRelations().ScheduledMessageLanguageEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation UIScheduleItemEntityUsingScheduledMessageIdStatic = new ScheduledMessageRelations().UIScheduleItemEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ScheduledMessageRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ScheduledMessageRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new ScheduledMessageRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new ScheduledMessageRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingMessageTemplateIdStatic = new ScheduledMessageRelations().MessageTemplateEntityUsingMessageTemplateId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new ScheduledMessageRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new ScheduledMessageRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new ScheduledMessageRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticScheduledMessageRelations()
		{
		}
	}
}
