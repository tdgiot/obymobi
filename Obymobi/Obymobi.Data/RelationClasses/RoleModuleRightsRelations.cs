﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoleModuleRights. </summary>
	public partial class RoleModuleRightsRelations
	{
		/// <summary>CTor</summary>
		public RoleModuleRightsRelations()
		{
		}

		/// <summary>Gets all relations of the RoleModuleRightsEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ModuleEntityUsingModuleId);
			toReturn.Add(this.RoleEntityUsingRoleId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RoleModuleRightsEntity and ModuleEntity over the m:1 relation they have, using the relation between the fields:
		/// RoleModuleRights.ModuleId - Module.ModuleId
		/// </summary>
		public virtual IEntityRelation ModuleEntityUsingModuleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ModuleEntity", false);
				relation.AddEntityFieldPair(ModuleFields.ModuleId, RoleModuleRightsFields.ModuleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ModuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleModuleRightsEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoleModuleRightsEntity and RoleEntity over the m:1 relation they have, using the relation between the fields:
		/// RoleModuleRights.RoleId - Role.RoleId
		/// </summary>
		public virtual IEntityRelation RoleEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoleEntity", false);
				relation.AddEntityFieldPair(RoleFields.RoleId, RoleModuleRightsFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleModuleRightsEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoleModuleRightsRelations
	{
		internal static readonly IEntityRelation ModuleEntityUsingModuleIdStatic = new RoleModuleRightsRelations().ModuleEntityUsingModuleId;
		internal static readonly IEntityRelation RoleEntityUsingRoleIdStatic = new RoleModuleRightsRelations().RoleEntityUsingRoleId;

		/// <summary>CTor</summary>
		static StaticRoleModuleRightsRelations()
		{
		}
	}
}
