﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportProcessingTask. </summary>
	public partial class ReportProcessingTaskRelations
	{
		/// <summary>CTor</summary>
		public ReportProcessingTaskRelations()
		{
		}

		/// <summary>Gets all relations of the ReportProcessingTaskEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskId);
			toReturn.Add(this.ReportProcessingTaskFileEntityUsingReportProcessingTaskId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.TimeZoneEntityUsingTimeZoneId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskEntity and ReportProcessingTaskTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportProcessingTask.ReportProcessingTaskId - ReportProcessingTaskTemplate.ReportProcessingTaskId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportProcessingTaskTemplateCollection" , true);
				relation.AddEntityFieldPair(ReportProcessingTaskFields.ReportProcessingTaskId, ReportProcessingTaskTemplateFields.ReportProcessingTaskId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskEntity and ReportProcessingTaskFileEntity over the 1:1 relation they have, using the relation between the fields:
		/// ReportProcessingTask.ReportProcessingTaskId - ReportProcessingTaskFile.ReportProcessingTaskId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskFileEntityUsingReportProcessingTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ReportProcessingTaskFileEntity", true);


				relation.AddEntityFieldPair(ReportProcessingTaskFields.ReportProcessingTaskId, ReportProcessingTaskFileFields.ReportProcessingTaskId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskFileEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportProcessingTask.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReportProcessingTaskFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskEntity and TimeZoneEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportProcessingTask.TimeZoneId - TimeZone.TimeZoneId
		/// </summary>
		public virtual IEntityRelation TimeZoneEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TimeZoneEntity", false);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, ReportProcessingTaskFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportProcessingTaskRelations
	{
		internal static readonly IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskIdStatic = new ReportProcessingTaskRelations().ReportProcessingTaskTemplateEntityUsingReportProcessingTaskId;
		internal static readonly IEntityRelation ReportProcessingTaskFileEntityUsingReportProcessingTaskIdStatic = new ReportProcessingTaskRelations().ReportProcessingTaskFileEntityUsingReportProcessingTaskId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReportProcessingTaskRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation TimeZoneEntityUsingTimeZoneIdStatic = new ReportProcessingTaskRelations().TimeZoneEntityUsingTimeZoneId;

		/// <summary>CTor</summary>
		static StaticReportProcessingTaskRelations()
		{
		}
	}
}
