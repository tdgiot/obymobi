﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemAlterationitem. </summary>
	public partial class OrderitemAlterationitemRelations
	{
		/// <summary>CTor</summary>
		public OrderitemAlterationitemRelations()
		{
		}

		/// <summary>Gets all relations of the OrderitemAlterationitemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId);
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationitemEntityUsingAlterationitemId);
			toReturn.Add(this.OrderitemEntityUsingOrderitemId);
			toReturn.Add(this.PriceLevelItemEntityUsingPriceLevelItemId);
			toReturn.Add(this.ProductEntityUsingAlterationoptionProductId);
			toReturn.Add(this.TaxTariffEntityUsingTaxTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.OrderitemAlterationitemId - OrderitemAlterationitemTag.OrderitemAlterationitemId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemTagCollection" , true);
				relation.AddEntityFieldPair(OrderitemAlterationitemFields.OrderitemAlterationitemId, OrderitemAlterationitemTagFields.OrderitemAlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, OrderitemAlterationitemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and AlterationitemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.AlterationitemId - Alterationitem.AlterationitemId
		/// </summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationitemEntity", false);
				relation.AddEntityFieldPair(AlterationitemFields.AlterationitemId, OrderitemAlterationitemFields.AlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and OrderitemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.OrderitemId - Orderitem.OrderitemId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderitemEntity", false);
				relation.AddEntityFieldPair(OrderitemFields.OrderitemId, OrderitemAlterationitemFields.OrderitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and PriceLevelItemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.PriceLevelItemId - PriceLevelItem.PriceLevelItemId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceLevelItemEntity", false);
				relation.AddEntityFieldPair(PriceLevelItemFields.PriceLevelItemId, OrderitemAlterationitemFields.PriceLevelItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.AlterationoptionProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingAlterationoptionProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemAlterationitemFields.AlterationoptionProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitem.TaxTariffId - TaxTariff.TaxTariffId
		/// </summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TaxTariffEntity", false);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, OrderitemAlterationitemFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemAlterationitemRelations
	{
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemIdStatic = new OrderitemAlterationitemRelations().OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new OrderitemAlterationitemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationitemIdStatic = new OrderitemAlterationitemRelations().AlterationitemEntityUsingAlterationitemId;
		internal static readonly IEntityRelation OrderitemEntityUsingOrderitemIdStatic = new OrderitemAlterationitemRelations().OrderitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelItemIdStatic = new OrderitemAlterationitemRelations().PriceLevelItemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation ProductEntityUsingAlterationoptionProductIdStatic = new OrderitemAlterationitemRelations().ProductEntityUsingAlterationoptionProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new OrderitemAlterationitemRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticOrderitemAlterationitemRelations()
		{
		}
	}
}
