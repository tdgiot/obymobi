﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Posdeliverypoint. </summary>
	public partial class PosdeliverypointRelations
	{
		/// <summary>CTor</summary>
		public PosdeliverypointRelations()
		{
		}

		/// <summary>Gets all relations of the PosdeliverypointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointEntityUsingPosdeliverypointId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PosdeliverypointEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Posdeliverypoint.PosdeliverypointId - Deliverypoint.PosdeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingPosdeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(PosdeliverypointFields.PosdeliverypointId, DeliverypointFields.PosdeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PosdeliverypointEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Posdeliverypoint.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosdeliverypointFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPosdeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointEntityUsingPosdeliverypointIdStatic = new PosdeliverypointRelations().DeliverypointEntityUsingPosdeliverypointId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PosdeliverypointRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPosdeliverypointRelations()
		{
		}
	}
}
