﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Deliverypointgroup. </summary>
	public partial class DeliverypointgroupRelations
	{
		/// <summary>CTor</summary>
		public DeliverypointgroupRelations()
		{
		}

		/// <summary>Gets all relations of the DeliverypointgroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingDeliverypointgroupId);
			toReturn.Add(this.AnnouncementEntityUsingDeliverypointgroupId);
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.ClientEntityUsingDeliverypointGroupId);
			toReturn.Add(this.ClientConfigurationEntityUsingDeliverypointgroupId);
			toReturn.Add(this.CustomTextEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupAnnouncementEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupLanguageEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupOccupancyEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeliverypointgroupProductEntityUsingDeliverypointgroupId);
			toReturn.Add(this.MediaEntityUsingDeliverypointgroupId);
			toReturn.Add(this.NetmessageEntityUsingReceiverDeliverypointgroupId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.SetupCodeEntityUsingDeliverypointgroupId);
			toReturn.Add(this.TerminalEntityUsingDeliverypointgroupId);
			toReturn.Add(this.UIScheduleEntityUsingDeliverypointgroupId);
			toReturn.Add(this.TimestampEntityUsingDeliverypointgroupId);
			toReturn.Add(this.AddressEntityUsingAddressId);
			toReturn.Add(this.AffiliateCampaignEntityUsingAffiliateCampaignId);
			toReturn.Add(this.AnnouncementEntityUsingReorderNotificationAnnouncementId);
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.MenuEntityUsingMenuId);
			toReturn.Add(this.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);
			toReturn.Add(this.PriceScheduleEntityUsingPriceScheduleId);
			toReturn.Add(this.ProductEntityUsingHotSOSBatteryLowProductId);
			toReturn.Add(this.RouteEntityUsingEmailDocumentRouteId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			toReturn.Add(this.RouteEntityUsingOrderNotesRouteId);
			toReturn.Add(this.RouteEntityUsingSystemMessageRouteId);
			toReturn.Add(this.TerminalEntityUsingXTerminalId);
			toReturn.Add(this.UIModeEntityUsingMobileUIModeId);
			toReturn.Add(this.UIModeEntityUsingTabletUIModeId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			toReturn.Add(this.UIScheduleEntityUsingUIScheduleId);
			toReturn.Add(this.UIThemeEntityUsingUIThemeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Advertisement.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, AdvertisementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Announcement.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, AnnouncementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - CheckoutMethodDeliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, CheckoutMethodDeliverypointgroupFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Client.DeliverypointGroupId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingDeliverypointGroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ClientFields.DeliverypointGroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - ClientConfiguration.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ClientConfigurationFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - CustomText.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, CustomTextFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Deliverypoint.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupAdvertisement.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupAdvertisementCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupAdvertisementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupAnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupAnnouncement.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupAnnouncementEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupAnnouncementCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupAnnouncementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupEntertainment.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupEntertainmentCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupEntertainmentFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupLanguage.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupLanguageEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupLanguageCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupLanguageFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupOccupancyEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupOccupancy.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupOccupancyEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupOccupancyCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupOccupancyFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupOccupancyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and DeliverypointgroupProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - DeliverypointgroupProduct.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupProductEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupProductCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupProductFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Media.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, MediaFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Netmessage.ReceiverDeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NetmessageCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, NetmessageFields.ReceiverDeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - ServiceMethodDeliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ServiceMethodDeliverypointgroupFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and SetupCodeEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - SetupCode.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation SetupCodeEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SetupCodeCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, SetupCodeFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SetupCodeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Terminal.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, TerminalFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIScheduleEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - UISchedule.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleCollection" , true);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, UIScheduleFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.DeliverypointgroupId - Timestamp.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, TimestampFields.DeliverypointgroupId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.AddressId - Address.AddressId
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AddressEntity", false);
				relation.AddEntityFieldPair(AddressFields.AddressId, DeliverypointgroupFields.AddressId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and AffiliateCampaignEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.AffiliateCampaignId - AffiliateCampaign.AffiliateCampaignId
		/// </summary>
		public virtual IEntityRelation AffiliateCampaignEntityUsingAffiliateCampaignId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AffiliateCampaignEntity", false);
				relation.AddEntityFieldPair(AffiliateCampaignFields.AffiliateCampaignId, DeliverypointgroupFields.AffiliateCampaignId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and AnnouncementEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.ReorderNotificationAnnouncementId - Announcement.AnnouncementId
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingReorderNotificationAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AnnouncementEntity", false);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, DeliverypointgroupFields.ReorderNotificationAnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, DeliverypointgroupFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, DeliverypointgroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and MenuEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.MenuId - Menu.MenuId
		/// </summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MenuEntity", false);
				relation.AddEntityFieldPair(MenuFields.MenuId, DeliverypointgroupFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and PosdeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.PosdeliverypointgroupId - Posdeliverypointgroup.PosdeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation PosdeliverypointgroupEntityUsingPosdeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosdeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(PosdeliverypointgroupFields.PosdeliverypointgroupId, DeliverypointgroupFields.PosdeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.PriceScheduleId - PriceSchedule.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceScheduleEntity", false);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, DeliverypointgroupFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.HotSOSBatteryLowProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingHotSOSBatteryLowProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "HotSOSBatteryLowProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, DeliverypointgroupFields.HotSOSBatteryLowProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.EmailDocumentRouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingEmailDocumentRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailDocumentRouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.EmailDocumentRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.OrderNotesRouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingOrderNotesRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity_", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.OrderNotesRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.SystemMessageRouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingSystemMessageRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SystemMessageRouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.SystemMessageRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.XTerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingXTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, DeliverypointgroupFields.XTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.MobileUIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingMobileUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MobileUIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.MobileUIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.TabletUIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingTabletUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TabletUIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.TabletUIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.UIScheduleId - UISchedule.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleEntity", false);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, DeliverypointgroupFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupEntity and UIThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypointgroup.UIThemeId - UITheme.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIThemeEntity", false);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, DeliverypointgroupFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().AdvertisementEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation AnnouncementEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().AnnouncementEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ClientEntityUsingDeliverypointGroupIdStatic = new DeliverypointgroupRelations().ClientEntityUsingDeliverypointGroupId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().ClientConfigurationEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation CustomTextEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().CustomTextEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupAnnouncementEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupAnnouncementEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupLanguageEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupLanguageEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupOccupancyEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupOccupancyEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeliverypointgroupProductEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().DeliverypointgroupProductEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation MediaEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().MediaEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverDeliverypointgroupIdStatic = new DeliverypointgroupRelations().NetmessageEntityUsingReceiverDeliverypointgroupId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation SetupCodeEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().SetupCodeEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation TerminalEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().TerminalEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation UIScheduleEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().UIScheduleEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation TimestampEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupRelations().TimestampEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation AddressEntityUsingAddressIdStatic = new DeliverypointgroupRelations().AddressEntityUsingAddressId;
		internal static readonly IEntityRelation AffiliateCampaignEntityUsingAffiliateCampaignIdStatic = new DeliverypointgroupRelations().AffiliateCampaignEntityUsingAffiliateCampaignId;
		internal static readonly IEntityRelation AnnouncementEntityUsingReorderNotificationAnnouncementIdStatic = new DeliverypointgroupRelations().AnnouncementEntityUsingReorderNotificationAnnouncementId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new DeliverypointgroupRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new DeliverypointgroupRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new DeliverypointgroupRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PosdeliverypointgroupEntityUsingPosdeliverypointgroupIdStatic = new DeliverypointgroupRelations().PosdeliverypointgroupEntityUsingPosdeliverypointgroupId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new DeliverypointgroupRelations().PriceScheduleEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation ProductEntityUsingHotSOSBatteryLowProductIdStatic = new DeliverypointgroupRelations().ProductEntityUsingHotSOSBatteryLowProductId;
		internal static readonly IEntityRelation RouteEntityUsingEmailDocumentRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingEmailDocumentRouteId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation RouteEntityUsingOrderNotesRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingOrderNotesRouteId;
		internal static readonly IEntityRelation RouteEntityUsingSystemMessageRouteIdStatic = new DeliverypointgroupRelations().RouteEntityUsingSystemMessageRouteId;
		internal static readonly IEntityRelation TerminalEntityUsingXTerminalIdStatic = new DeliverypointgroupRelations().TerminalEntityUsingXTerminalId;
		internal static readonly IEntityRelation UIModeEntityUsingMobileUIModeIdStatic = new DeliverypointgroupRelations().UIModeEntityUsingMobileUIModeId;
		internal static readonly IEntityRelation UIModeEntityUsingTabletUIModeIdStatic = new DeliverypointgroupRelations().UIModeEntityUsingTabletUIModeId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new DeliverypointgroupRelations().UIModeEntityUsingUIModeId;
		internal static readonly IEntityRelation UIScheduleEntityUsingUIScheduleIdStatic = new DeliverypointgroupRelations().UIScheduleEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIThemeEntityUsingUIThemeIdStatic = new DeliverypointgroupRelations().UIThemeEntityUsingUIThemeId;

		/// <summary>CTor</summary>
		static StaticDeliverypointgroupRelations()
		{
		}
	}
}
