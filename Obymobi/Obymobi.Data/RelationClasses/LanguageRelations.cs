﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Language. </summary>
	public partial class LanguageRelations
	{
		/// <summary>CTor</summary>
		public LanguageRelations()
		{
		}

		/// <summary>Gets all relations of the LanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ActionButtonLanguageEntityUsingLanguageId);
			toReturn.Add(this.AdvertisementLanguageEntityUsingLanguageId);
			toReturn.Add(this.AdvertisementTagLanguageEntityUsingLanguageId);
			toReturn.Add(this.AlterationLanguageEntityUsingLanguageId);
			toReturn.Add(this.AlterationoptionLanguageEntityUsingLanguageId);
			toReturn.Add(this.AmenityLanguageEntityUsingLanguageId);
			toReturn.Add(this.AnnouncementLanguageEntityUsingLanguageId);
			toReturn.Add(this.AttachmentLanguageEntityUsingLanguageId);
			toReturn.Add(this.CategoryLanguageEntityUsingLanguageId);
			toReturn.Add(this.CompanyEntityUsingLanguageId);
			toReturn.Add(this.CompanyLanguageEntityUsingLanguageId);
			toReturn.Add(this.CustomTextEntityUsingLanguageId);
			toReturn.Add(this.DeliverypointgroupLanguageEntityUsingLanguageId);
			toReturn.Add(this.EntertainmentcategoryLanguageEntityUsingLanguageId);
			toReturn.Add(this.GenericcategoryLanguageEntityUsingLanguageId);
			toReturn.Add(this.GenericproductLanguageEntityUsingLanguageId);
			toReturn.Add(this.MediaLanguageEntityUsingLanguageId);
			toReturn.Add(this.PageElementEntityUsingLanguageId);
			toReturn.Add(this.PageLanguageEntityUsingLanguageId);
			toReturn.Add(this.PageTemplateElementEntityUsingLanguageId);
			toReturn.Add(this.PageTemplateLanguageEntityUsingLanguageId);
			toReturn.Add(this.PointOfInterestLanguageEntityUsingLanguageId);
			toReturn.Add(this.ProductLanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlAreaLanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlComponentLanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlSectionItemLanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlSectionLanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlWidgetLanguageEntityUsingLanguageId);
			toReturn.Add(this.ScheduledMessageLanguageEntityUsingLanguageId);
			toReturn.Add(this.SiteLanguageEntityUsingLanguageId);
			toReturn.Add(this.SiteTemplateLanguageEntityUsingLanguageId);
			toReturn.Add(this.StationLanguageEntityUsingLanguageId);
			toReturn.Add(this.SurveyAnswerLanguageEntityUsingLanguageId);
			toReturn.Add(this.SurveyLanguageEntityUsingLanguageId);
			toReturn.Add(this.SurveyQuestionLanguageEntityUsingLanguageId);
			toReturn.Add(this.UIFooterItemLanguageEntityUsingLanguageId);
			toReturn.Add(this.UITabLanguageEntityUsingLanguageId);
			toReturn.Add(this.UIWidgetLanguageEntityUsingLanguageId);
			toReturn.Add(this.VenueCategoryLanguageEntityUsingLanguageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and ActionButtonLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - ActionButtonLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation ActionButtonLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionButtonLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, ActionButtonLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AdvertisementLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AdvertisementLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AdvertisementLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AdvertisementLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AdvertisementTagLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AdvertisementTagLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AdvertisementTagLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AlterationLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AlterationLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AlterationLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AlterationLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AlterationoptionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AlterationoptionLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AlterationoptionLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AlterationoptionLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AmenityLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AmenityLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AmenityLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AmenityLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AmenityLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AnnouncementLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AnnouncementLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AnnouncementLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AnnouncementLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and AttachmentLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - AttachmentLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation AttachmentLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AttachmentLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and CategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - CategoryLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation CategoryLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CategoryLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - Company.LanguageId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CompanyFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and CompanyLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - CompanyLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation CompanyLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CompanyLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - CustomText.LanguageId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CustomTextFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and DeliverypointgroupLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - DeliverypointgroupLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, DeliverypointgroupLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and EntertainmentcategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - EntertainmentcategoryLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentcategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, EntertainmentcategoryLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and GenericcategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - GenericcategoryLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation GenericcategoryLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericcategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, GenericcategoryLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and GenericproductLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - GenericproductLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation GenericproductLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, GenericproductLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and MediaLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - MediaLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation MediaLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, MediaLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and PageElementEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - PageElement.LanguageId
		/// </summary>
		public virtual IEntityRelation PageElementEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageElementCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageElementFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and PageLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - PageLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation PageLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and PageTemplateElementEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - PageTemplateElement.LanguageId
		/// </summary>
		public virtual IEntityRelation PageTemplateElementEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateElementCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageTemplateElementFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and PageTemplateLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - PageTemplateLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation PageTemplateLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageTemplateLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and PointOfInterestLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - PointOfInterestLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation PointOfInterestLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PointOfInterestLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and ProductLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - ProductLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation ProductLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, ProductLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and RoomControlAreaLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - RoomControlAreaLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlAreaLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlAreaLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and RoomControlComponentLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - RoomControlComponentLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlComponentLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlComponentLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and RoomControlSectionItemLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - RoomControlSectionItemLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionItemLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlSectionItemLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and RoomControlSectionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - RoomControlSectionLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlSectionLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and RoomControlWidgetLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - RoomControlWidgetLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlWidgetLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and ScheduledMessageLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - ScheduledMessageLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, ScheduledMessageLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and SiteLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - SiteLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation SiteLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SiteLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and SiteTemplateLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - SiteTemplateLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation SiteTemplateLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteTemplateLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SiteTemplateLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and StationLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - StationLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation StationLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StationLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, StationLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and SurveyAnswerLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - SurveyAnswerLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation SurveyAnswerLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswerLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SurveyAnswerLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and SurveyLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - SurveyLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation SurveyLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SurveyLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and SurveyQuestionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - SurveyQuestionLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyQuestionLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SurveyQuestionLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and UIFooterItemLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - UIFooterItemLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation UIFooterItemLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIFooterItemLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, UIFooterItemLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and UITabLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - UITabLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation UITabLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, UITabLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and UIWidgetLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - UIWidgetLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation UIWidgetLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, UIWidgetLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and VenueCategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageId - VenueCategoryLanguage.LanguageId
		/// </summary>
		public virtual IEntityRelation VenueCategoryLanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VenueCategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, VenueCategoryLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLanguageRelations
	{
		internal static readonly IEntityRelation ActionButtonLanguageEntityUsingLanguageIdStatic = new LanguageRelations().ActionButtonLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AdvertisementLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AdvertisementLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AdvertisementTagLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AdvertisementTagLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AlterationLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AlterationLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AlterationoptionLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AlterationoptionLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AmenityLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AmenityLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AnnouncementLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AnnouncementLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation AttachmentLanguageEntityUsingLanguageIdStatic = new LanguageRelations().AttachmentLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation CategoryLanguageEntityUsingLanguageIdStatic = new LanguageRelations().CategoryLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation CompanyEntityUsingLanguageIdStatic = new LanguageRelations().CompanyEntityUsingLanguageId;
		internal static readonly IEntityRelation CompanyLanguageEntityUsingLanguageIdStatic = new LanguageRelations().CompanyLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation CustomTextEntityUsingLanguageIdStatic = new LanguageRelations().CustomTextEntityUsingLanguageId;
		internal static readonly IEntityRelation DeliverypointgroupLanguageEntityUsingLanguageIdStatic = new LanguageRelations().DeliverypointgroupLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation EntertainmentcategoryLanguageEntityUsingLanguageIdStatic = new LanguageRelations().EntertainmentcategoryLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation GenericcategoryLanguageEntityUsingLanguageIdStatic = new LanguageRelations().GenericcategoryLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation GenericproductLanguageEntityUsingLanguageIdStatic = new LanguageRelations().GenericproductLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation MediaLanguageEntityUsingLanguageIdStatic = new LanguageRelations().MediaLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation PageElementEntityUsingLanguageIdStatic = new LanguageRelations().PageElementEntityUsingLanguageId;
		internal static readonly IEntityRelation PageLanguageEntityUsingLanguageIdStatic = new LanguageRelations().PageLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation PageTemplateElementEntityUsingLanguageIdStatic = new LanguageRelations().PageTemplateElementEntityUsingLanguageId;
		internal static readonly IEntityRelation PageTemplateLanguageEntityUsingLanguageIdStatic = new LanguageRelations().PageTemplateLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation PointOfInterestLanguageEntityUsingLanguageIdStatic = new LanguageRelations().PointOfInterestLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation ProductLanguageEntityUsingLanguageIdStatic = new LanguageRelations().ProductLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlAreaLanguageEntityUsingLanguageIdStatic = new LanguageRelations().RoomControlAreaLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlComponentLanguageEntityUsingLanguageIdStatic = new LanguageRelations().RoomControlComponentLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlSectionItemLanguageEntityUsingLanguageIdStatic = new LanguageRelations().RoomControlSectionItemLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlSectionLanguageEntityUsingLanguageIdStatic = new LanguageRelations().RoomControlSectionLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlWidgetLanguageEntityUsingLanguageIdStatic = new LanguageRelations().RoomControlWidgetLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation ScheduledMessageLanguageEntityUsingLanguageIdStatic = new LanguageRelations().ScheduledMessageLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SiteLanguageEntityUsingLanguageIdStatic = new LanguageRelations().SiteLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SiteTemplateLanguageEntityUsingLanguageIdStatic = new LanguageRelations().SiteTemplateLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation StationLanguageEntityUsingLanguageIdStatic = new LanguageRelations().StationLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SurveyAnswerLanguageEntityUsingLanguageIdStatic = new LanguageRelations().SurveyAnswerLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SurveyLanguageEntityUsingLanguageIdStatic = new LanguageRelations().SurveyLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SurveyQuestionLanguageEntityUsingLanguageIdStatic = new LanguageRelations().SurveyQuestionLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation UIFooterItemLanguageEntityUsingLanguageIdStatic = new LanguageRelations().UIFooterItemLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation UITabLanguageEntityUsingLanguageIdStatic = new LanguageRelations().UITabLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation UIWidgetLanguageEntityUsingLanguageIdStatic = new LanguageRelations().UIWidgetLanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation VenueCategoryLanguageEntityUsingLanguageIdStatic = new LanguageRelations().VenueCategoryLanguageEntityUsingLanguageId;

		/// <summary>CTor</summary>
		static StaticLanguageRelations()
		{
		}
	}
}
