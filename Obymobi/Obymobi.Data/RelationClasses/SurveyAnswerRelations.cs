﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyAnswer. </summary>
	public partial class SurveyAnswerRelations
	{
		/// <summary>CTor</summary>
		public SurveyAnswerRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyAnswerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyAnswerLanguageEntityUsingSurveyAnswerId);
			toReturn.Add(this.SurveyResultEntityUsingSurveyAnswerId);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionId);
			toReturn.Add(this.SurveyQuestionEntityUsingTargetSurveyQuestionId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyAnswerLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyAnswerId - SurveyAnswerLanguage.SurveyAnswerId
		/// </summary>
		public virtual IEntityRelation SurveyAnswerLanguageEntityUsingSurveyAnswerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswerLanguageCollection" , true);
				relation.AddEntityFieldPair(SurveyAnswerFields.SurveyAnswerId, SurveyAnswerLanguageFields.SurveyAnswerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyResultEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyAnswerId - SurveyResult.SurveyAnswerId
		/// </summary>
		public virtual IEntityRelation SurveyResultEntityUsingSurveyAnswerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResultCollection" , true);
				relation.AddEntityFieldPair(SurveyAnswerFields.SurveyAnswerId, SurveyResultFields.SurveyAnswerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyQuestionId - SurveyQuestion.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyQuestionEntity", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyAnswerFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyAnswer.TargetSurveyQuestionId - SurveyQuestion.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingTargetSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TargetSurveyQuestionEntity", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyAnswerFields.TargetSurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyAnswerRelations
	{
		internal static readonly IEntityRelation SurveyAnswerLanguageEntityUsingSurveyAnswerIdStatic = new SurveyAnswerRelations().SurveyAnswerLanguageEntityUsingSurveyAnswerId;
		internal static readonly IEntityRelation SurveyResultEntityUsingSurveyAnswerIdStatic = new SurveyAnswerRelations().SurveyResultEntityUsingSurveyAnswerId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIdStatic = new SurveyAnswerRelations().SurveyQuestionEntityUsingSurveyQuestionId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingTargetSurveyQuestionIdStatic = new SurveyAnswerRelations().SurveyQuestionEntityUsingTargetSurveyQuestionId;

		/// <summary>CTor</summary>
		static StaticSurveyAnswerRelations()
		{
		}
	}
}
