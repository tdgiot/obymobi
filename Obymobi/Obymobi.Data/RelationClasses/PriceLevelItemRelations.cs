﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceLevelItem. </summary>
	public partial class PriceLevelItemRelations
	{
		/// <summary>CTor</summary>
		public PriceLevelItemRelations()
		{
		}

		/// <summary>Gets all relations of the PriceLevelItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderitemEntityUsingPriceLevelItemId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingPriceLevelItemId);
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.PriceLevelEntityUsingPriceLevelId);
			toReturn.Add(this.ProductEntityUsingProductId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceLevelItem.PriceLevelItemId - Orderitem.PriceLevelItemId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingPriceLevelItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemCollection" , true);
				relation.AddEntityFieldPair(PriceLevelItemFields.PriceLevelItemId, OrderitemFields.PriceLevelItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceLevelItem.PriceLevelItemId - OrderitemAlterationitem.PriceLevelItemId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingPriceLevelItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(PriceLevelItemFields.PriceLevelItemId, OrderitemAlterationitemFields.PriceLevelItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceLevelItem.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, PriceLevelItemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceLevelItem.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, PriceLevelItemFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and PriceLevelEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceLevelItem.PriceLevelId - PriceLevel.PriceLevelId
		/// </summary>
		public virtual IEntityRelation PriceLevelEntityUsingPriceLevelId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceLevelEntity", false);
				relation.AddEntityFieldPair(PriceLevelFields.PriceLevelId, PriceLevelItemFields.PriceLevelId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PriceLevelItemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceLevelItem.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, PriceLevelItemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceLevelItemRelations
	{
		internal static readonly IEntityRelation OrderitemEntityUsingPriceLevelItemIdStatic = new PriceLevelItemRelations().OrderitemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingPriceLevelItemIdStatic = new PriceLevelItemRelations().OrderitemAlterationitemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new PriceLevelItemRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new PriceLevelItemRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation PriceLevelEntityUsingPriceLevelIdStatic = new PriceLevelItemRelations().PriceLevelEntityUsingPriceLevelId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new PriceLevelItemRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticPriceLevelItemRelations()
		{
		}
	}
}
