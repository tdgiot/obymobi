﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Application. </summary>
	public partial class ApplicationRelations
	{
		/// <summary>CTor</summary>
		public ApplicationRelations()
		{
		}

		/// <summary>Gets all relations of the ApplicationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReleaseEntityUsingApplicationId);
			toReturn.Add(this.ReleaseEntityUsingReleaseId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ApplicationEntity and ReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// Application.ApplicationId - Release.ApplicationId
		/// </summary>
		public virtual IEntityRelation ReleaseEntityUsingApplicationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReleaseCollection" , true);
				relation.AddEntityFieldPair(ApplicationFields.ApplicationId, ReleaseFields.ApplicationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ApplicationEntity and ReleaseEntity over the m:1 relation they have, using the relation between the fields:
		/// Application.ReleaseId - Release.ReleaseId
		/// </summary>
		public virtual IEntityRelation ReleaseEntityUsingReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReleaseEntity", false);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, ApplicationFields.ReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticApplicationRelations
	{
		internal static readonly IEntityRelation ReleaseEntityUsingApplicationIdStatic = new ApplicationRelations().ReleaseEntityUsingApplicationId;
		internal static readonly IEntityRelation ReleaseEntityUsingReleaseIdStatic = new ApplicationRelations().ReleaseEntityUsingReleaseId;

		/// <summary>CTor</summary>
		static StaticApplicationRelations()
		{
		}
	}
}
