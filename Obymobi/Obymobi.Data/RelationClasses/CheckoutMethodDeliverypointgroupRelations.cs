﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CheckoutMethodDeliverypointgroup. </summary>
	public partial class CheckoutMethodDeliverypointgroupRelations
	{
		/// <summary>CTor</summary>
		public CheckoutMethodDeliverypointgroupRelations()
		{
		}

		/// <summary>Gets all relations of the CheckoutMethodDeliverypointgroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CheckoutMethodEntityUsingCheckoutMethodId);
			toReturn.Add(this.CompanyEntityUsingParentCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and CheckoutMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethodDeliverypointgroup.CheckoutMethodId - CheckoutMethod.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CheckoutMethodEntity", false);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, CheckoutMethodDeliverypointgroupFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethodDeliverypointgroup.ParentCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CheckoutMethodDeliverypointgroupFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodDeliverypointgroupEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethodDeliverypointgroup.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, CheckoutMethodDeliverypointgroupFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCheckoutMethodDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCheckoutMethodIdStatic = new CheckoutMethodDeliverypointgroupRelations().CheckoutMethodEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new CheckoutMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new CheckoutMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;

		/// <summary>CTor</summary>
		static StaticCheckoutMethodDeliverypointgroupRelations()
		{
		}
	}
}
