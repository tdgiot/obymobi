﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Station. </summary>
	public partial class StationRelations
	{
		/// <summary>CTor</summary>
		public StationRelations()
		{
		}

		/// <summary>Gets all relations of the StationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingStationId);
			toReturn.Add(this.MediaEntityUsingStationId);
			toReturn.Add(this.StationLanguageEntityUsingStationId);
			toReturn.Add(this.StationListEntityUsingStationListId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between StationEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Station.StationId - CustomText.StationId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingStationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(StationFields.StationId, CustomTextFields.StationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Station.StationId - Media.StationId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingStationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(StationFields.StationId, MediaFields.StationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StationEntity and StationLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Station.StationId - StationLanguage.StationId
		/// </summary>
		public virtual IEntityRelation StationLanguageEntityUsingStationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StationLanguageCollection" , true);
				relation.AddEntityFieldPair(StationFields.StationId, StationLanguageFields.StationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between StationEntity and StationListEntity over the m:1 relation they have, using the relation between the fields:
		/// Station.StationListId - StationList.StationListId
		/// </summary>
		public virtual IEntityRelation StationListEntityUsingStationListId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StationListEntity", false);
				relation.AddEntityFieldPair(StationListFields.StationListId, StationFields.StationListId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStationRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingStationIdStatic = new StationRelations().CustomTextEntityUsingStationId;
		internal static readonly IEntityRelation MediaEntityUsingStationIdStatic = new StationRelations().MediaEntityUsingStationId;
		internal static readonly IEntityRelation StationLanguageEntityUsingStationIdStatic = new StationRelations().StationLanguageEntityUsingStationId;
		internal static readonly IEntityRelation StationListEntityUsingStationListIdStatic = new StationRelations().StationListEntityUsingStationListId;

		/// <summary>CTor</summary>
		static StaticStationRelations()
		{
		}
	}
}
