﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Schedule. </summary>
	public partial class ScheduleRelations
	{
		/// <summary>CTor</summary>
		public ScheduleRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingScheduleId);
			toReturn.Add(this.ProductEntityUsingScheduleId);
			toReturn.Add(this.ScheduleitemEntityUsingScheduleId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Schedule.ScheduleId - Category.ScheduleId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(ScheduleFields.ScheduleId, CategoryFields.ScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Schedule.ScheduleId - Product.ScheduleId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(ScheduleFields.ScheduleId, ProductFields.ScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and ScheduleitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Schedule.ScheduleId - Scheduleitem.ScheduleId
		/// </summary>
		public virtual IEntityRelation ScheduleitemEntityUsingScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduleitemCollection" , true);
				relation.AddEntityFieldPair(ScheduleFields.ScheduleId, ScheduleitemFields.ScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleitemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ScheduleEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Schedule.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduleRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingScheduleIdStatic = new ScheduleRelations().CategoryEntityUsingScheduleId;
		internal static readonly IEntityRelation ProductEntityUsingScheduleIdStatic = new ScheduleRelations().ProductEntityUsingScheduleId;
		internal static readonly IEntityRelation ScheduleitemEntityUsingScheduleIdStatic = new ScheduleRelations().ScheduleitemEntityUsingScheduleId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ScheduleRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticScheduleRelations()
		{
		}
	}
}
