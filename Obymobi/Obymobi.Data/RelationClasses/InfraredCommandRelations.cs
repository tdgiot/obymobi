﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InfraredCommand. </summary>
	public partial class InfraredCommandRelations
	{
		/// <summary>CTor</summary>
		public InfraredCommandRelations()
		{
		}

		/// <summary>Gets all relations of the InfraredCommandEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingInfraredCommandId);
			toReturn.Add(this.InfraredConfigurationEntityUsingInfraredConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InfraredCommandEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// InfraredCommand.InfraredCommandId - CustomText.InfraredCommandId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingInfraredCommandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(InfraredCommandFields.InfraredCommandId, CustomTextFields.InfraredCommandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredCommandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between InfraredCommandEntity and InfraredConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// InfraredCommand.InfraredConfigurationId - InfraredConfiguration.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InfraredConfigurationEntity", false);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, InfraredCommandFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredCommandEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInfraredCommandRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingInfraredCommandIdStatic = new InfraredCommandRelations().CustomTextEntityUsingInfraredCommandId;
		internal static readonly IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationIdStatic = new InfraredCommandRelations().InfraredConfigurationEntityUsingInfraredConfigurationId;

		/// <summary>CTor</summary>
		static StaticInfraredCommandRelations()
		{
		}
	}
}
