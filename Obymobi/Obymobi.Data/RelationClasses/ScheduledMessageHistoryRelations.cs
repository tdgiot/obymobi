﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ScheduledMessageHistory. </summary>
	public partial class ScheduledMessageHistoryRelations
	{
		/// <summary>CTor</summary>
		public ScheduledMessageHistoryRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduledMessageHistoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageEntityUsingMessageId);
			toReturn.Add(this.MessagegroupEntityUsingMessagegroupId);
			toReturn.Add(this.ScheduledMessageEntityUsingScheduledMessageId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageHistoryEntity and MessageEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageHistory.MessageId - Message.MessageId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageEntity", false);
				relation.AddEntityFieldPair(MessageFields.MessageId, ScheduledMessageHistoryFields.MessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageHistoryEntity and MessagegroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageHistory.MessagegroupId - Messagegroup.MessagegroupId
		/// </summary>
		public virtual IEntityRelation MessagegroupEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessagegroupEntity", false);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, ScheduledMessageHistoryFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageHistoryEntity and ScheduledMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageHistory.ScheduledMessageId - ScheduledMessage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledMessageEntity", false);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, ScheduledMessageHistoryFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledMessageHistoryEntity and UIScheduleItemOccurrenceEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledMessageHistory.UIScheduleItemOccurrenceId - UIScheduleItemOccurrence.UIScheduleItemOccurrenceId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleItemOccurrenceEntity", false);
				relation.AddEntityFieldPair(UIScheduleItemOccurrenceFields.UIScheduleItemOccurrenceId, ScheduledMessageHistoryFields.UIScheduleItemOccurrenceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduledMessageHistoryRelations
	{
		internal static readonly IEntityRelation MessageEntityUsingMessageIdStatic = new ScheduledMessageHistoryRelations().MessageEntityUsingMessageId;
		internal static readonly IEntityRelation MessagegroupEntityUsingMessagegroupIdStatic = new ScheduledMessageHistoryRelations().MessagegroupEntityUsingMessagegroupId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingScheduledMessageIdStatic = new ScheduledMessageHistoryRelations().ScheduledMessageEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdStatic = new ScheduledMessageHistoryRelations().UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceId;

		/// <summary>CTor</summary>
		static StaticScheduledMessageHistoryRelations()
		{
		}
	}
}
