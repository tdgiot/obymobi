﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageTemplateElement. </summary>
	public partial class PageTemplateElementRelations
	{
		/// <summary>CTor</summary>
		public PageTemplateElementRelations()
		{
		}

		/// <summary>Gets all relations of the PageTemplateElementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MediaEntityUsingPageTemplateElementId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageTemplateElementEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplateElement.PageTemplateElementId - Media.PageTemplateElementId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingPageTemplateElementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(PageTemplateElementFields.PageTemplateElementId, MediaFields.PageTemplateElementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PageTemplateElementEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// PageTemplateElement.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageTemplateElementFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageTemplateElementEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// PageTemplateElement.PageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageTemplateElementFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageTemplateElementRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingPageTemplateElementIdStatic = new PageTemplateElementRelations().MediaEntityUsingPageTemplateElementId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new PageTemplateElementRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdStatic = new PageTemplateElementRelations().PageTemplateEntityUsingPageTemplateId;

		/// <summary>CTor</summary>
		static StaticPageTemplateElementRelations()
		{
		}
	}
}
