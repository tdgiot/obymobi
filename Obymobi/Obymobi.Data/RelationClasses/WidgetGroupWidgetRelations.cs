﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: WidgetGroupWidget. </summary>
	public partial class WidgetGroupWidgetRelations
	{
		/// <summary>CTor</summary>
		public WidgetGroupWidgetRelations()
		{
		}

		/// <summary>Gets all relations of the WidgetGroupWidgetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.WidgetEntityUsingWidgetId);
			toReturn.Add(this.WidgetGroupEntityUsingWidgetGroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between WidgetGroupWidgetEntity and WidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// WidgetGroupWidget.WidgetId - Widget.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentWidgetEntity", false);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetGroupWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetGroupWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetGroupWidgetEntity and WidgetGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// WidgetGroupWidget.WidgetGroupId - WidgetGroup.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetGroupEntityUsingWidgetGroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentWidgetGroupEntity", false);
				relation.AddEntityFieldPair(WidgetGroupFields.WidgetId, WidgetGroupWidgetFields.WidgetGroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetGroupWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWidgetGroupWidgetRelations
	{
		internal static readonly IEntityRelation WidgetEntityUsingWidgetIdStatic = new WidgetGroupWidgetRelations().WidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation WidgetGroupEntityUsingWidgetGroupIdStatic = new WidgetGroupWidgetRelations().WidgetGroupEntityUsingWidgetGroupId;

		/// <summary>CTor</summary>
		static StaticWidgetGroupWidgetRelations()
		{
		}
	}
}
